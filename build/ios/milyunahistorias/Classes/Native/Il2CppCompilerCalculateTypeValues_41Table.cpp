﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TOOLS2D_T1149803378_H
#define TOOLS2D_T1149803378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util.Tools2D
struct  Tools2D_t1149803378  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLS2D_T1149803378_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU3D112_T1514025265_H
#define U24ARRAYTYPEU3D112_T1514025265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=112
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D112_t1514025265 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D112_t1514025265__padding[112];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D112_T1514025265_H
#ifndef U24ARRAYTYPEU3D28_T173484549_H
#define U24ARRAYTYPEU3D28_T173484549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=28
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D28_t173484549 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D28_t173484549__padding[28];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D28_T173484549_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef IMAGEFILTERMODE_T35032559_H
#define IMAGEFILTERMODE_T35032559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util.ImageFilterMode
struct  ImageFilterMode_t35032559 
{
public:
	// System.Int32 Util.ImageFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageFilterMode_t35032559, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEFILTERMODE_T35032559_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-78517443912BB49729313EC23065D9970ABC80E3
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0;
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C
	U24ArrayTypeU3D28_t173484549  ___U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-148E9E3E864CD628C70D3DC1D8309483BD8C0E89
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-AAF72C1002FDBCAE040DAE16A10D82184CE83679
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-5CF7299F6558A8AC3F821B4F2F65F23798D319D3
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-B67A7FB4648C62F6A1337CA473436D0E787E8633
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-FAD52931F5B79811D31566BB18B6E0B5D2E2A164
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7;
	// <PrivateImplementationDetails>/$ArrayType=112 <PrivateImplementationDetails>::$field-50B1635D1FB2907A171B71751E1A3FA79423CA17
	U24ArrayTypeU3D112_t1514025265  ___U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8;

public:
	inline static int32_t get_offset_of_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0() const { return ___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0() { return &___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0; }
	inline void set_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1)); }
	inline U24ArrayTypeU3D28_t173484549  get_U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1() const { return ___U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1; }
	inline U24ArrayTypeU3D28_t173484549 * get_address_of_U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1() { return &___U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1; }
	inline void set_U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1(U24ArrayTypeU3D28_t173484549  value)
	{
		___U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2() const { return ___U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2() { return &___U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2; }
	inline void set_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3() const { return ___U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3() { return &___U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3; }
	inline void set_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4() const { return ___U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4() { return &___U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4; }
	inline void set_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5() const { return ___U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5() { return &___U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5; }
	inline void set_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6() const { return ___U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6() { return &___U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6; }
	inline void set_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7() const { return ___U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7() { return &___U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7; }
	inline void set_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8)); }
	inline U24ArrayTypeU3D112_t1514025265  get_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8() const { return ___U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8; }
	inline U24ArrayTypeU3D112_t1514025265 * get_address_of_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8() { return &___U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8; }
	inline void set_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8(U24ArrayTypeU3D112_t1514025265  value)
	{
		___U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UTILTHREAD_T4012287602_H
#define UTILTHREAD_T4012287602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util.UtilThread
struct  UtilThread_t4012287602  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILTHREAD_T4012287602_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (ImageFilterMode_t35032559)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4100[4] = 
{
	ImageFilterMode_t35032559::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { sizeof (Tools2D_t1149803378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (UtilThread_t4012287602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4103[9] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D78517443912BB49729313EC23065D9970ABC80E3_0(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D3E9BE9C2C9C2670F11178D4936C4FE5AEF3ECD9C_1(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_2(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_3(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_4(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_5(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_6(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_7(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { sizeof (U24ArrayTypeU3D28_t173484549)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D28_t173484549 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (U24ArrayTypeU3D112_t1514025265)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D112_t1514025265 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
