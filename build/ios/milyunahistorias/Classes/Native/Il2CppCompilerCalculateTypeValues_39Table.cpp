﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2571361770;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.Generic.HashSet`1<UnityEngine.Object>
struct HashSet_1_t3490924723;
// Cinemachine.ICinemachineCamera
struct ICinemachineCamera_t1388288076;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_t1327958294;
// Cinemachine.CinemachineBrain
struct CinemachineBrain_t1028500134;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Transform
struct Transform_t3600365921;
// Cinemachine.Utility.GaussianWindow1D_Vector3
struct GaussianWindow1D_Vector3_t926608352;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item[]
struct ItemU5BU5D_t1337847011;
// UnityEngine.Object
struct Object_t631007953;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// Cinemachine.CinemachineCore/AxisInputDelegate
struct AxisInputDelegate_t365085504;
// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain>
struct List_1_t2500574876;
// System.Collections.Generic.List`1<Cinemachine.ICinemachineCamera>
struct List_1_t2860362818;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Cinemachine.ICinemachineCamera>>
struct List_1_t37470264;
// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,Cinemachine.CinemachineCore/UpdateStatus>
struct Dictionary_2_t3701647757;
// Cinemachine.CinemachineBlenderSettings/CustomBlend[]
struct CustomBlendU5BU5D_t2035913683;
// System.Collections.Generic.List`1<Cinemachine.CameraState/CustomBlendable>
struct List_1_t708436666;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Cinemachine.CinemachineOrbitalTransposer
struct CinemachineOrbitalTransposer_t2654619201;
// Cinemachine.CinemachineVirtualCamera
struct CinemachineVirtualCamera_t947248387;
// Cinemachine.CinemachineFreeLook
struct CinemachineFreeLook_t3895466944;
// Cinemachine.NoiseSettings/TransformNoiseParams[]
struct TransformNoiseParamsU5BU5D_t2996701196;
// Cinemachine.CinemachineComponentBase[]
struct CinemachineComponentBaseU5BU5D_t3785922318;
// Cinemachine.CinemachineTargetGroup/Target[]
struct TargetU5BU5D_t3329193756;
// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,System.Object>
struct Dictionary_2_t1893910920;
// Cinemachine.CinemachinePathBase/Appearance
struct Appearance_t243588924;
// System.Action
struct Action_t1264377477;
// System.String[]
struct StringU5BU5D_t1281789340;
// Cinemachine.CinemachineCore/Stage[]
struct StageU5BU5D_t2719076562;
// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate
struct OnPostPipelineStageDelegate_t2437605738;
// Cinemachine.CinemachinePathBase
struct CinemachinePathBase_t1750211330;
// Cinemachine.Utility.PositionPredictor
struct PositionPredictor_t974033707;
// Cinemachine.CinemachineVirtualCameraBase[]
struct CinemachineVirtualCameraBaseU5BU5D_t2980098515;
// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,System.Int32>
struct Dictionary_2_t139543931;
// Cinemachine.CinemachineFreeLook/Orbit[]
struct OrbitU5BU5D_t1434718521;
// Cinemachine.CinemachineVirtualCamera[]
struct CinemachineVirtualCameraU5BU5D_t194901906;
// Cinemachine.CinemachineOrbitalTransposer[]
struct CinemachineOrbitalTransposerU5BU5D_t4090051484;
// Cinemachine.CinemachineBlend
struct CinemachineBlend_t1772488801;
// Cinemachine.CinemachineFreeLook/CreateRigDelegate
struct CreateRigDelegate_t3127201826;
// Cinemachine.CinemachineFreeLook/DestroyRigDelegate
struct DestroyRigDelegate_t2388233918;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.Camera
struct Camera_t4157153871;
// Cinemachine.CinemachinePath/Waypoint[]
struct WaypointU5BU5D_t2291284199;
// Cinemachine.NoiseSettings
struct NoiseSettings_t3019862488;
// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate
struct CreatePipelineDelegate_t3891770455;
// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate
struct DestroyPipelineDelegate_t29728977;
// System.Comparison`1<Cinemachine.CinemachineComponentBase>
struct Comparison_1_t3897468706;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct List_1_t805411711;
// UnityEngine.Animator
struct Animator_t434523843;
// Cinemachine.CinemachineStateDrivenCamera/Instruction[]
struct InstructionU5BU5D_t716093651;
// Cinemachine.CinemachineBlenderSettings
struct CinemachineBlenderSettings_t3646651286;
// Cinemachine.CinemachineStateDrivenCamera/ParentHash[]
struct ParentHashU5BU5D_t1415352068;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t333824601;
// Cinemachine.CinemachineSmoothPath/Waypoint[]
struct WaypointU5BU5D_t280685257;
// Cinemachine.CinemachineOrbitalTransposer/UpdateHeadingDelegate
struct UpdateHeadingDelegate_t3270413085;
// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker
struct HeadingTracker_t85731283;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;

struct Object_t631007953_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GAUSSIANWINDOW1D_1_T1405837570_H
#define GAUSSIANWINDOW1D_1_T1405837570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Vector2>
struct  GaussianWindow1d_1_t1405837570  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	Vector2U5BU5D_t1457185986* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_t1444911251* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1405837570, ___mData_0)); }
	inline Vector2U5BU5D_t1457185986* get_mData_0() const { return ___mData_0; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(Vector2U5BU5D_t1457185986* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((&___mData_0), value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1405837570, ___mKernel_1)); }
	inline SingleU5BU5D_t1444911251* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_t1444911251* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((&___mKernel_1), value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1405837570, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1405837570, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1405837570, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_1_T1405837570_H
#ifndef GAUSSIANWINDOW1D_1_T1551536378_H
#define GAUSSIANWINDOW1D_1_T1551536378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Quaternion>
struct  GaussianWindow1d_1_t1551536378  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	QuaternionU5BU5D_t2571361770* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_t1444911251* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1551536378, ___mData_0)); }
	inline QuaternionU5BU5D_t2571361770* get_mData_0() const { return ___mData_0; }
	inline QuaternionU5BU5D_t2571361770** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(QuaternionU5BU5D_t2571361770* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((&___mData_0), value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1551536378, ___mKernel_1)); }
	inline SingleU5BU5D_t1444911251* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_t1444911251* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((&___mKernel_1), value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1551536378, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1551536378, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t1551536378, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_1_T1551536378_H
#ifndef GAUSSIANWINDOW1D_1_T2971921511_H
#define GAUSSIANWINDOW1D_1_T2971921511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Vector3>
struct  GaussianWindow1d_1_t2971921511  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	Vector3U5BU5D_t1718750761* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_t1444911251* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2971921511, ___mData_0)); }
	inline Vector3U5BU5D_t1718750761* get_mData_0() const { return ___mData_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(Vector3U5BU5D_t1718750761* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((&___mData_0), value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2971921511, ___mKernel_1)); }
	inline SingleU5BU5D_t1444911251* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_t1444911251* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((&___mKernel_1), value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2971921511, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2971921511, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t2971921511, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_1_T2971921511_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef REFLECTIONHELPERS_T20665401_H
#define REFLECTIONHELPERS_T20665401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.ReflectionHelpers
struct  ReflectionHelpers_t20665401  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONHELPERS_T20665401_H
#ifndef DAMPER_T1723357806_H
#define DAMPER_T1723357806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.Damper
struct  Damper_t1723357806  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMPER_T1723357806_H
#ifndef CINEMACHINEGAMEWINDOWDEBUG_T254377641_H
#define CINEMACHINEGAMEWINDOWDEBUG_T254377641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.CinemachineGameWindowDebug
struct  CinemachineGameWindowDebug_t254377641  : public RuntimeObject
{
public:

public:
};

struct CinemachineGameWindowDebug_t254377641_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.Object> Cinemachine.Utility.CinemachineGameWindowDebug::mClients
	HashSet_1_t3490924723 * ___mClients_0;

public:
	inline static int32_t get_offset_of_mClients_0() { return static_cast<int32_t>(offsetof(CinemachineGameWindowDebug_t254377641_StaticFields, ___mClients_0)); }
	inline HashSet_1_t3490924723 * get_mClients_0() const { return ___mClients_0; }
	inline HashSet_1_t3490924723 ** get_address_of_mClients_0() { return &___mClients_0; }
	inline void set_mClients_0(HashSet_1_t3490924723 * value)
	{
		___mClients_0 = value;
		Il2CppCodeGenWriteBarrier((&___mClients_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEGAMEWINDOWDEBUG_T254377641_H
#ifndef PLAYABLEBEHAVIOUR_T4203540982_H
#define PLAYABLEBEHAVIOUR_T4203540982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t4203540982  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T4203540982_H
#ifndef UNITYQUATERNIONEXTENSIONS_T2095813971_H
#define UNITYQUATERNIONEXTENSIONS_T2095813971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.UnityQuaternionExtensions
struct  UnityQuaternionExtensions_t2095813971  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYQUATERNIONEXTENSIONS_T2095813971_H
#ifndef UNITYVECTOREXTENSIONS_T3137382101_H
#define UNITYVECTOREXTENSIONS_T3137382101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.UnityVectorExtensions
struct  UnityVectorExtensions_t3137382101  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVECTOREXTENSIONS_T3137382101_H
#ifndef UNITYRECTEXTENSIONS_T1109636319_H
#define UNITYRECTEXTENSIONS_T1109636319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.UnityRectExtensions
struct  UnityRectExtensions_t1109636319  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRECTEXTENSIONS_T1109636319_H
#ifndef VCAMEXTRASTATE_T2436675054_H
#define VCAMEXTRASTATE_T2436675054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFollowZoom/VcamExtraState
struct  VcamExtraState_t2436675054  : public RuntimeObject
{
public:
	// System.Single Cinemachine.CinemachineFollowZoom/VcamExtraState::m_previousFrameZoom
	float ___m_previousFrameZoom_0;

public:
	inline static int32_t get_offset_of_m_previousFrameZoom_0() { return static_cast<int32_t>(offsetof(VcamExtraState_t2436675054, ___m_previousFrameZoom_0)); }
	inline float get_m_previousFrameZoom_0() const { return ___m_previousFrameZoom_0; }
	inline float* get_address_of_m_previousFrameZoom_0() { return &___m_previousFrameZoom_0; }
	inline void set_m_previousFrameZoom_0(float value)
	{
		___m_previousFrameZoom_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VCAMEXTRASTATE_T2436675054_H
#ifndef CINEMACHINEBLEND_T1772488801_H
#define CINEMACHINEBLEND_T1772488801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlend
struct  CinemachineBlend_t1772488801  : public RuntimeObject
{
public:
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBlend::<CamA>k__BackingField
	RuntimeObject* ___U3CCamAU3Ek__BackingField_0;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBlend::<CamB>k__BackingField
	RuntimeObject* ___U3CCamBU3Ek__BackingField_1;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineBlend::<BlendCurve>k__BackingField
	AnimationCurve_t3046754366 * ___U3CBlendCurveU3Ek__BackingField_2;
	// System.Single Cinemachine.CinemachineBlend::<TimeInBlend>k__BackingField
	float ___U3CTimeInBlendU3Ek__BackingField_3;
	// System.Single Cinemachine.CinemachineBlend::<Duration>k__BackingField
	float ___U3CDurationU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCamAU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CinemachineBlend_t1772488801, ___U3CCamAU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CCamAU3Ek__BackingField_0() const { return ___U3CCamAU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CCamAU3Ek__BackingField_0() { return &___U3CCamAU3Ek__BackingField_0; }
	inline void set_U3CCamAU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CCamAU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCamAU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCamBU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CinemachineBlend_t1772488801, ___U3CCamBU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCamBU3Ek__BackingField_1() const { return ___U3CCamBU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCamBU3Ek__BackingField_1() { return &___U3CCamBU3Ek__BackingField_1; }
	inline void set_U3CCamBU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCamBU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCamBU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBlendCurveU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CinemachineBlend_t1772488801, ___U3CBlendCurveU3Ek__BackingField_2)); }
	inline AnimationCurve_t3046754366 * get_U3CBlendCurveU3Ek__BackingField_2() const { return ___U3CBlendCurveU3Ek__BackingField_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3CBlendCurveU3Ek__BackingField_2() { return &___U3CBlendCurveU3Ek__BackingField_2; }
	inline void set_U3CBlendCurveU3Ek__BackingField_2(AnimationCurve_t3046754366 * value)
	{
		___U3CBlendCurveU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBlendCurveU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTimeInBlendU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CinemachineBlend_t1772488801, ___U3CTimeInBlendU3Ek__BackingField_3)); }
	inline float get_U3CTimeInBlendU3Ek__BackingField_3() const { return ___U3CTimeInBlendU3Ek__BackingField_3; }
	inline float* get_address_of_U3CTimeInBlendU3Ek__BackingField_3() { return &___U3CTimeInBlendU3Ek__BackingField_3; }
	inline void set_U3CTimeInBlendU3Ek__BackingField_3(float value)
	{
		___U3CTimeInBlendU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CinemachineBlend_t1772488801, ___U3CDurationU3Ek__BackingField_4)); }
	inline float get_U3CDurationU3Ek__BackingField_4() const { return ___U3CDurationU3Ek__BackingField_4; }
	inline float* get_address_of_U3CDurationU3Ek__BackingField_4() { return &___U3CDurationU3Ek__BackingField_4; }
	inline void set_U3CDurationU3Ek__BackingField_4(float value)
	{
		___U3CDurationU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLEND_T1772488801_H
#ifndef SPLINEHELPERS_T505011237_H
#define SPLINEHELPERS_T505011237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.SplineHelpers
struct  SplineHelpers_t505011237  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEHELPERS_T505011237_H
#ifndef NOISEPARAMS_T2342455097_H
#define NOISEPARAMS_T2342455097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoiseSettings/NoiseParams
struct  NoiseParams_t2342455097 
{
public:
	// System.Single Cinemachine.NoiseSettings/NoiseParams::Amplitude
	float ___Amplitude_0;
	// System.Single Cinemachine.NoiseSettings/NoiseParams::Frequency
	float ___Frequency_1;

public:
	inline static int32_t get_offset_of_Amplitude_0() { return static_cast<int32_t>(offsetof(NoiseParams_t2342455097, ___Amplitude_0)); }
	inline float get_Amplitude_0() const { return ___Amplitude_0; }
	inline float* get_address_of_Amplitude_0() { return &___Amplitude_0; }
	inline void set_Amplitude_0(float value)
	{
		___Amplitude_0 = value;
	}

	inline static int32_t get_offset_of_Frequency_1() { return static_cast<int32_t>(offsetof(NoiseParams_t2342455097, ___Frequency_1)); }
	inline float get_Frequency_1() const { return ___Frequency_1; }
	inline float* get_address_of_Frequency_1() { return &___Frequency_1; }
	inline void set_Frequency_1(float value)
	{
		___Frequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISEPARAMS_T2342455097_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef CINEMACHINESHOTPLAYABLE_T1408524235_H
#define CINEMACHINESHOTPLAYABLE_T1408524235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineShotPlayable
struct  CinemachineShotPlayable_t1408524235  : public PlayableBehaviour_t4203540982
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.Timeline.CinemachineShotPlayable::VirtualCamera
	CinemachineVirtualCameraBase_t1327958294 * ___VirtualCamera_0;

public:
	inline static int32_t get_offset_of_VirtualCamera_0() { return static_cast<int32_t>(offsetof(CinemachineShotPlayable_t1408524235, ___VirtualCamera_0)); }
	inline CinemachineVirtualCameraBase_t1327958294 * get_VirtualCamera_0() const { return ___VirtualCamera_0; }
	inline CinemachineVirtualCameraBase_t1327958294 ** get_address_of_VirtualCamera_0() { return &___VirtualCamera_0; }
	inline void set_VirtualCamera_0(CinemachineVirtualCameraBase_t1327958294 * value)
	{
		___VirtualCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualCamera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESHOTPLAYABLE_T1408524235_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef LENSSETTINGS_T822254920_H
#define LENSSETTINGS_T822254920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.LensSettings
struct  LensSettings_t822254920 
{
public:
	// System.Single Cinemachine.LensSettings::FieldOfView
	float ___FieldOfView_1;
	// System.Single Cinemachine.LensSettings::OrthographicSize
	float ___OrthographicSize_2;
	// System.Single Cinemachine.LensSettings::NearClipPlane
	float ___NearClipPlane_3;
	// System.Single Cinemachine.LensSettings::FarClipPlane
	float ___FarClipPlane_4;
	// System.Single Cinemachine.LensSettings::Dutch
	float ___Dutch_5;
	// System.Boolean Cinemachine.LensSettings::<Orthographic>k__BackingField
	bool ___U3COrthographicU3Ek__BackingField_6;
	// System.Single Cinemachine.LensSettings::<Aspect>k__BackingField
	float ___U3CAspectU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_FieldOfView_1() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___FieldOfView_1)); }
	inline float get_FieldOfView_1() const { return ___FieldOfView_1; }
	inline float* get_address_of_FieldOfView_1() { return &___FieldOfView_1; }
	inline void set_FieldOfView_1(float value)
	{
		___FieldOfView_1 = value;
	}

	inline static int32_t get_offset_of_OrthographicSize_2() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___OrthographicSize_2)); }
	inline float get_OrthographicSize_2() const { return ___OrthographicSize_2; }
	inline float* get_address_of_OrthographicSize_2() { return &___OrthographicSize_2; }
	inline void set_OrthographicSize_2(float value)
	{
		___OrthographicSize_2 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_3() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___NearClipPlane_3)); }
	inline float get_NearClipPlane_3() const { return ___NearClipPlane_3; }
	inline float* get_address_of_NearClipPlane_3() { return &___NearClipPlane_3; }
	inline void set_NearClipPlane_3(float value)
	{
		___NearClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_4() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___FarClipPlane_4)); }
	inline float get_FarClipPlane_4() const { return ___FarClipPlane_4; }
	inline float* get_address_of_FarClipPlane_4() { return &___FarClipPlane_4; }
	inline void set_FarClipPlane_4(float value)
	{
		___FarClipPlane_4 = value;
	}

	inline static int32_t get_offset_of_Dutch_5() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___Dutch_5)); }
	inline float get_Dutch_5() const { return ___Dutch_5; }
	inline float* get_address_of_Dutch_5() { return &___Dutch_5; }
	inline void set_Dutch_5(float value)
	{
		___Dutch_5 = value;
	}

	inline static int32_t get_offset_of_U3COrthographicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___U3COrthographicU3Ek__BackingField_6)); }
	inline bool get_U3COrthographicU3Ek__BackingField_6() const { return ___U3COrthographicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3COrthographicU3Ek__BackingField_6() { return &___U3COrthographicU3Ek__BackingField_6; }
	inline void set_U3COrthographicU3Ek__BackingField_6(bool value)
	{
		___U3COrthographicU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAspectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LensSettings_t822254920, ___U3CAspectU3Ek__BackingField_7)); }
	inline float get_U3CAspectU3Ek__BackingField_7() const { return ___U3CAspectU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAspectU3Ek__BackingField_7() { return &___U3CAspectU3Ek__BackingField_7; }
	inline void set_U3CAspectU3Ek__BackingField_7(float value)
	{
		___U3CAspectU3Ek__BackingField_7 = value;
	}
};

struct LensSettings_t822254920_StaticFields
{
public:
	// Cinemachine.LensSettings Cinemachine.LensSettings::Default
	LensSettings_t822254920  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LensSettings_t822254920_StaticFields, ___Default_0)); }
	inline LensSettings_t822254920  get_Default_0() const { return ___Default_0; }
	inline LensSettings_t822254920 * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LensSettings_t822254920  value)
	{
		___Default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.LensSettings
struct LensSettings_t822254920_marshaled_pinvoke
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	float ___U3CAspectU3Ek__BackingField_7;
};
// Native definition for COM marshalling of Cinemachine.LensSettings
struct LensSettings_t822254920_marshaled_com
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	float ___U3CAspectU3Ek__BackingField_7;
};
#endif // LENSSETTINGS_T822254920_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef AUTODOLLY_T722089724_H
#define AUTODOLLY_T722089724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTrackedDolly/AutoDolly
struct  AutoDolly_t722089724 
{
public:
	// System.Boolean Cinemachine.CinemachineTrackedDolly/AutoDolly::m_Enabled
	bool ___m_Enabled_0;
	// System.Single Cinemachine.CinemachineTrackedDolly/AutoDolly::m_PositionOffset
	float ___m_PositionOffset_1;
	// System.Int32 Cinemachine.CinemachineTrackedDolly/AutoDolly::m_SearchRadius
	int32_t ___m_SearchRadius_2;
	// System.Int32 Cinemachine.CinemachineTrackedDolly/AutoDolly::m_SearchResolution
	int32_t ___m_SearchResolution_3;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(AutoDolly_t722089724, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}

	inline static int32_t get_offset_of_m_PositionOffset_1() { return static_cast<int32_t>(offsetof(AutoDolly_t722089724, ___m_PositionOffset_1)); }
	inline float get_m_PositionOffset_1() const { return ___m_PositionOffset_1; }
	inline float* get_address_of_m_PositionOffset_1() { return &___m_PositionOffset_1; }
	inline void set_m_PositionOffset_1(float value)
	{
		___m_PositionOffset_1 = value;
	}

	inline static int32_t get_offset_of_m_SearchRadius_2() { return static_cast<int32_t>(offsetof(AutoDolly_t722089724, ___m_SearchRadius_2)); }
	inline int32_t get_m_SearchRadius_2() const { return ___m_SearchRadius_2; }
	inline int32_t* get_address_of_m_SearchRadius_2() { return &___m_SearchRadius_2; }
	inline void set_m_SearchRadius_2(int32_t value)
	{
		___m_SearchRadius_2 = value;
	}

	inline static int32_t get_offset_of_m_SearchResolution_3() { return static_cast<int32_t>(offsetof(AutoDolly_t722089724, ___m_SearchResolution_3)); }
	inline int32_t get_m_SearchResolution_3() const { return ___m_SearchResolution_3; }
	inline int32_t* get_address_of_m_SearchResolution_3() { return &___m_SearchResolution_3; }
	inline void set_m_SearchResolution_3(int32_t value)
	{
		___m_SearchResolution_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineTrackedDolly/AutoDolly
struct AutoDolly_t722089724_marshaled_pinvoke
{
	int32_t ___m_Enabled_0;
	float ___m_PositionOffset_1;
	int32_t ___m_SearchRadius_2;
	int32_t ___m_SearchResolution_3;
};
// Native definition for COM marshalling of Cinemachine.CinemachineTrackedDolly/AutoDolly
struct AutoDolly_t722089724_marshaled_com
{
	int32_t ___m_Enabled_0;
	float ___m_PositionOffset_1;
	int32_t ___m_SearchRadius_2;
	int32_t ___m_SearchResolution_3;
};
#endif // AUTODOLLY_T722089724_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECENTERING_T980373847_H
#define RECENTERING_T980373847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/Recentering
struct  Recentering_t980373847 
{
public:
	// System.Boolean Cinemachine.CinemachineOrbitalTransposer/Recentering::m_enabled
	bool ___m_enabled_0;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/Recentering::m_RecenterWaitTime
	float ___m_RecenterWaitTime_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/Recentering::m_RecenteringTime
	float ___m_RecenteringTime_2;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/Recentering::m_LegacyHeadingDefinition
	int32_t ___m_LegacyHeadingDefinition_3;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/Recentering::m_LegacyVelocityFilterStrength
	int32_t ___m_LegacyVelocityFilterStrength_4;

public:
	inline static int32_t get_offset_of_m_enabled_0() { return static_cast<int32_t>(offsetof(Recentering_t980373847, ___m_enabled_0)); }
	inline bool get_m_enabled_0() const { return ___m_enabled_0; }
	inline bool* get_address_of_m_enabled_0() { return &___m_enabled_0; }
	inline void set_m_enabled_0(bool value)
	{
		___m_enabled_0 = value;
	}

	inline static int32_t get_offset_of_m_RecenterWaitTime_1() { return static_cast<int32_t>(offsetof(Recentering_t980373847, ___m_RecenterWaitTime_1)); }
	inline float get_m_RecenterWaitTime_1() const { return ___m_RecenterWaitTime_1; }
	inline float* get_address_of_m_RecenterWaitTime_1() { return &___m_RecenterWaitTime_1; }
	inline void set_m_RecenterWaitTime_1(float value)
	{
		___m_RecenterWaitTime_1 = value;
	}

	inline static int32_t get_offset_of_m_RecenteringTime_2() { return static_cast<int32_t>(offsetof(Recentering_t980373847, ___m_RecenteringTime_2)); }
	inline float get_m_RecenteringTime_2() const { return ___m_RecenteringTime_2; }
	inline float* get_address_of_m_RecenteringTime_2() { return &___m_RecenteringTime_2; }
	inline void set_m_RecenteringTime_2(float value)
	{
		___m_RecenteringTime_2 = value;
	}

	inline static int32_t get_offset_of_m_LegacyHeadingDefinition_3() { return static_cast<int32_t>(offsetof(Recentering_t980373847, ___m_LegacyHeadingDefinition_3)); }
	inline int32_t get_m_LegacyHeadingDefinition_3() const { return ___m_LegacyHeadingDefinition_3; }
	inline int32_t* get_address_of_m_LegacyHeadingDefinition_3() { return &___m_LegacyHeadingDefinition_3; }
	inline void set_m_LegacyHeadingDefinition_3(int32_t value)
	{
		___m_LegacyHeadingDefinition_3 = value;
	}

	inline static int32_t get_offset_of_m_LegacyVelocityFilterStrength_4() { return static_cast<int32_t>(offsetof(Recentering_t980373847, ___m_LegacyVelocityFilterStrength_4)); }
	inline int32_t get_m_LegacyVelocityFilterStrength_4() const { return ___m_LegacyVelocityFilterStrength_4; }
	inline int32_t* get_address_of_m_LegacyVelocityFilterStrength_4() { return &___m_LegacyVelocityFilterStrength_4; }
	inline void set_m_LegacyVelocityFilterStrength_4(int32_t value)
	{
		___m_LegacyVelocityFilterStrength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineOrbitalTransposer/Recentering
struct Recentering_t980373847_marshaled_pinvoke
{
	int32_t ___m_enabled_0;
	float ___m_RecenterWaitTime_1;
	float ___m_RecenteringTime_2;
	int32_t ___m_LegacyHeadingDefinition_3;
	int32_t ___m_LegacyVelocityFilterStrength_4;
};
// Native definition for COM marshalling of Cinemachine.CinemachineOrbitalTransposer/Recentering
struct Recentering_t980373847_marshaled_com
{
	int32_t ___m_enabled_0;
	float ___m_RecenterWaitTime_1;
	float ___m_RecenteringTime_2;
	int32_t ___m_LegacyHeadingDefinition_3;
	int32_t ___m_LegacyVelocityFilterStrength_4;
};
#endif // RECENTERING_T980373847_H
#ifndef CINEMACHINEMIXER_T2688963259_H
#define CINEMACHINEMIXER_T2688963259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineMixer
struct  CinemachineMixer_t2688963259  : public PlayableBehaviour_t4203540982
{
public:
	// Cinemachine.CinemachineBrain Cinemachine.Timeline.CinemachineMixer::mBrain
	CinemachineBrain_t1028500134 * ___mBrain_0;
	// System.Int32 Cinemachine.Timeline.CinemachineMixer::mBrainOverrideId
	int32_t ___mBrainOverrideId_1;
	// System.Boolean Cinemachine.Timeline.CinemachineMixer::mPlaying
	bool ___mPlaying_2;
	// System.Single Cinemachine.Timeline.CinemachineMixer::mLastOverrideFrame
	float ___mLastOverrideFrame_3;

public:
	inline static int32_t get_offset_of_mBrain_0() { return static_cast<int32_t>(offsetof(CinemachineMixer_t2688963259, ___mBrain_0)); }
	inline CinemachineBrain_t1028500134 * get_mBrain_0() const { return ___mBrain_0; }
	inline CinemachineBrain_t1028500134 ** get_address_of_mBrain_0() { return &___mBrain_0; }
	inline void set_mBrain_0(CinemachineBrain_t1028500134 * value)
	{
		___mBrain_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBrain_0), value);
	}

	inline static int32_t get_offset_of_mBrainOverrideId_1() { return static_cast<int32_t>(offsetof(CinemachineMixer_t2688963259, ___mBrainOverrideId_1)); }
	inline int32_t get_mBrainOverrideId_1() const { return ___mBrainOverrideId_1; }
	inline int32_t* get_address_of_mBrainOverrideId_1() { return &___mBrainOverrideId_1; }
	inline void set_mBrainOverrideId_1(int32_t value)
	{
		___mBrainOverrideId_1 = value;
	}

	inline static int32_t get_offset_of_mPlaying_2() { return static_cast<int32_t>(offsetof(CinemachineMixer_t2688963259, ___mPlaying_2)); }
	inline bool get_mPlaying_2() const { return ___mPlaying_2; }
	inline bool* get_address_of_mPlaying_2() { return &___mPlaying_2; }
	inline void set_mPlaying_2(bool value)
	{
		___mPlaying_2 = value;
	}

	inline static int32_t get_offset_of_mLastOverrideFrame_3() { return static_cast<int32_t>(offsetof(CinemachineMixer_t2688963259, ___mLastOverrideFrame_3)); }
	inline float get_mLastOverrideFrame_3() const { return ___mLastOverrideFrame_3; }
	inline float* get_address_of_mLastOverrideFrame_3() { return &___mLastOverrideFrame_3; }
	inline void set_mLastOverrideFrame_3(float value)
	{
		___mLastOverrideFrame_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEMIXER_T2688963259_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef PARENTHASH_T1970338041_H
#define PARENTHASH_T1970338041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineStateDrivenCamera/ParentHash
struct  ParentHash_t1970338041 
{
public:
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera/ParentHash::m_Hash
	int32_t ___m_Hash_0;
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera/ParentHash::m_ParentHash
	int32_t ___m_ParentHash_1;

public:
	inline static int32_t get_offset_of_m_Hash_0() { return static_cast<int32_t>(offsetof(ParentHash_t1970338041, ___m_Hash_0)); }
	inline int32_t get_m_Hash_0() const { return ___m_Hash_0; }
	inline int32_t* get_address_of_m_Hash_0() { return &___m_Hash_0; }
	inline void set_m_Hash_0(int32_t value)
	{
		___m_Hash_0 = value;
	}

	inline static int32_t get_offset_of_m_ParentHash_1() { return static_cast<int32_t>(offsetof(ParentHash_t1970338041, ___m_ParentHash_1)); }
	inline int32_t get_m_ParentHash_1() const { return ___m_ParentHash_1; }
	inline int32_t* get_address_of_m_ParentHash_1() { return &___m_ParentHash_1; }
	inline void set_m_ParentHash_1(int32_t value)
	{
		___m_ParentHash_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTHASH_T1970338041_H
#ifndef INSTRUCTION_T2121420310_H
#define INSTRUCTION_T2121420310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineStateDrivenCamera/Instruction
struct  Instruction_t2121420310 
{
public:
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera/Instruction::m_FullHash
	int32_t ___m_FullHash_0;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineStateDrivenCamera/Instruction::m_VirtualCamera
	CinemachineVirtualCameraBase_t1327958294 * ___m_VirtualCamera_1;
	// System.Single Cinemachine.CinemachineStateDrivenCamera/Instruction::m_ActivateAfter
	float ___m_ActivateAfter_2;
	// System.Single Cinemachine.CinemachineStateDrivenCamera/Instruction::m_MinDuration
	float ___m_MinDuration_3;

public:
	inline static int32_t get_offset_of_m_FullHash_0() { return static_cast<int32_t>(offsetof(Instruction_t2121420310, ___m_FullHash_0)); }
	inline int32_t get_m_FullHash_0() const { return ___m_FullHash_0; }
	inline int32_t* get_address_of_m_FullHash_0() { return &___m_FullHash_0; }
	inline void set_m_FullHash_0(int32_t value)
	{
		___m_FullHash_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualCamera_1() { return static_cast<int32_t>(offsetof(Instruction_t2121420310, ___m_VirtualCamera_1)); }
	inline CinemachineVirtualCameraBase_t1327958294 * get_m_VirtualCamera_1() const { return ___m_VirtualCamera_1; }
	inline CinemachineVirtualCameraBase_t1327958294 ** get_address_of_m_VirtualCamera_1() { return &___m_VirtualCamera_1; }
	inline void set_m_VirtualCamera_1(CinemachineVirtualCameraBase_t1327958294 * value)
	{
		___m_VirtualCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualCamera_1), value);
	}

	inline static int32_t get_offset_of_m_ActivateAfter_2() { return static_cast<int32_t>(offsetof(Instruction_t2121420310, ___m_ActivateAfter_2)); }
	inline float get_m_ActivateAfter_2() const { return ___m_ActivateAfter_2; }
	inline float* get_address_of_m_ActivateAfter_2() { return &___m_ActivateAfter_2; }
	inline void set_m_ActivateAfter_2(float value)
	{
		___m_ActivateAfter_2 = value;
	}

	inline static int32_t get_offset_of_m_MinDuration_3() { return static_cast<int32_t>(offsetof(Instruction_t2121420310, ___m_MinDuration_3)); }
	inline float get_m_MinDuration_3() const { return ___m_MinDuration_3; }
	inline float* get_address_of_m_MinDuration_3() { return &___m_MinDuration_3; }
	inline void set_m_MinDuration_3(float value)
	{
		___m_MinDuration_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineStateDrivenCamera/Instruction
struct Instruction_t2121420310_marshaled_pinvoke
{
	int32_t ___m_FullHash_0;
	CinemachineVirtualCameraBase_t1327958294 * ___m_VirtualCamera_1;
	float ___m_ActivateAfter_2;
	float ___m_MinDuration_3;
};
// Native definition for COM marshalling of Cinemachine.CinemachineStateDrivenCamera/Instruction
struct Instruction_t2121420310_marshaled_com
{
	int32_t ___m_FullHash_0;
	CinemachineVirtualCameraBase_t1327958294 * ___m_VirtualCamera_1;
	float ___m_ActivateAfter_2;
	float ___m_MinDuration_3;
};
#endif // INSTRUCTION_T2121420310_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef ORBIT_T75009320_H
#define ORBIT_T75009320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook/Orbit
struct  Orbit_t75009320 
{
public:
	// System.Single Cinemachine.CinemachineFreeLook/Orbit::m_Height
	float ___m_Height_0;
	// System.Single Cinemachine.CinemachineFreeLook/Orbit::m_Radius
	float ___m_Radius_1;

public:
	inline static int32_t get_offset_of_m_Height_0() { return static_cast<int32_t>(offsetof(Orbit_t75009320, ___m_Height_0)); }
	inline float get_m_Height_0() const { return ___m_Height_0; }
	inline float* get_address_of_m_Height_0() { return &___m_Height_0; }
	inline void set_m_Height_0(float value)
	{
		___m_Height_0 = value;
	}

	inline static int32_t get_offset_of_m_Radius_1() { return static_cast<int32_t>(offsetof(Orbit_t75009320, ___m_Radius_1)); }
	inline float get_m_Radius_1() const { return ___m_Radius_1; }
	inline float* get_address_of_m_Radius_1() { return &___m_Radius_1; }
	inline void set_m_Radius_1(float value)
	{
		___m_Radius_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORBIT_T75009320_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SAVEDURINGPLAYATTRIBUTE_T77832053_H
#define SAVEDURINGPLAYATTRIBUTE_T77832053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.SaveDuringPlayAttribute
struct  SaveDuringPlayAttribute_t77832053  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDURINGPLAYATTRIBUTE_T77832053_H
#ifndef PROPERTYNAME_T3749835189_H
#define PROPERTYNAME_T3749835189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t3749835189 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t3749835189, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T3749835189_H
#ifndef GAUSSIANWINDOW1D_QUATERNION_T451620234_H
#define GAUSSIANWINDOW1D_QUATERNION_T451620234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1D_Quaternion
struct  GaussianWindow1D_Quaternion_t451620234  : public GaussianWindow1d_1_t1551536378
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_QUATERNION_T451620234_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef GAUSSIANWINDOW1D_CAMERAROTATION_T788833816_H
#define GAUSSIANWINDOW1D_CAMERAROTATION_T788833816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1D_CameraRotation
struct  GaussianWindow1D_CameraRotation_t788833816  : public GaussianWindow1d_1_t1405837570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_CAMERAROTATION_T788833816_H
#ifndef AXISSTATE_T478218302_H
#define AXISSTATE_T478218302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.AxisState
struct  AxisState_t478218302 
{
public:
	// System.Single Cinemachine.AxisState::Value
	float ___Value_0;
	// System.Single Cinemachine.AxisState::m_MaxSpeed
	float ___m_MaxSpeed_1;
	// System.Single Cinemachine.AxisState::m_AccelTime
	float ___m_AccelTime_2;
	// System.Single Cinemachine.AxisState::m_DecelTime
	float ___m_DecelTime_3;
	// System.String Cinemachine.AxisState::m_InputAxisName
	String_t* ___m_InputAxisName_4;
	// System.Single Cinemachine.AxisState::m_InputAxisValue
	float ___m_InputAxisValue_5;
	// System.Boolean Cinemachine.AxisState::m_InvertAxis
	bool ___m_InvertAxis_6;
	// System.Single Cinemachine.AxisState::mCurrentSpeed
	float ___mCurrentSpeed_7;
	// System.Single Cinemachine.AxisState::mMinValue
	float ___mMinValue_8;
	// System.Single Cinemachine.AxisState::mMaxValue
	float ___mMaxValue_9;
	// System.Boolean Cinemachine.AxisState::mWrapAround
	bool ___mWrapAround_10;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___Value_0)); }
	inline float get_Value_0() const { return ___Value_0; }
	inline float* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(float value)
	{
		___Value_0 = value;
	}

	inline static int32_t get_offset_of_m_MaxSpeed_1() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___m_MaxSpeed_1)); }
	inline float get_m_MaxSpeed_1() const { return ___m_MaxSpeed_1; }
	inline float* get_address_of_m_MaxSpeed_1() { return &___m_MaxSpeed_1; }
	inline void set_m_MaxSpeed_1(float value)
	{
		___m_MaxSpeed_1 = value;
	}

	inline static int32_t get_offset_of_m_AccelTime_2() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___m_AccelTime_2)); }
	inline float get_m_AccelTime_2() const { return ___m_AccelTime_2; }
	inline float* get_address_of_m_AccelTime_2() { return &___m_AccelTime_2; }
	inline void set_m_AccelTime_2(float value)
	{
		___m_AccelTime_2 = value;
	}

	inline static int32_t get_offset_of_m_DecelTime_3() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___m_DecelTime_3)); }
	inline float get_m_DecelTime_3() const { return ___m_DecelTime_3; }
	inline float* get_address_of_m_DecelTime_3() { return &___m_DecelTime_3; }
	inline void set_m_DecelTime_3(float value)
	{
		___m_DecelTime_3 = value;
	}

	inline static int32_t get_offset_of_m_InputAxisName_4() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___m_InputAxisName_4)); }
	inline String_t* get_m_InputAxisName_4() const { return ___m_InputAxisName_4; }
	inline String_t** get_address_of_m_InputAxisName_4() { return &___m_InputAxisName_4; }
	inline void set_m_InputAxisName_4(String_t* value)
	{
		___m_InputAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputAxisName_4), value);
	}

	inline static int32_t get_offset_of_m_InputAxisValue_5() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___m_InputAxisValue_5)); }
	inline float get_m_InputAxisValue_5() const { return ___m_InputAxisValue_5; }
	inline float* get_address_of_m_InputAxisValue_5() { return &___m_InputAxisValue_5; }
	inline void set_m_InputAxisValue_5(float value)
	{
		___m_InputAxisValue_5 = value;
	}

	inline static int32_t get_offset_of_m_InvertAxis_6() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___m_InvertAxis_6)); }
	inline bool get_m_InvertAxis_6() const { return ___m_InvertAxis_6; }
	inline bool* get_address_of_m_InvertAxis_6() { return &___m_InvertAxis_6; }
	inline void set_m_InvertAxis_6(bool value)
	{
		___m_InvertAxis_6 = value;
	}

	inline static int32_t get_offset_of_mCurrentSpeed_7() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___mCurrentSpeed_7)); }
	inline float get_mCurrentSpeed_7() const { return ___mCurrentSpeed_7; }
	inline float* get_address_of_mCurrentSpeed_7() { return &___mCurrentSpeed_7; }
	inline void set_mCurrentSpeed_7(float value)
	{
		___mCurrentSpeed_7 = value;
	}

	inline static int32_t get_offset_of_mMinValue_8() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___mMinValue_8)); }
	inline float get_mMinValue_8() const { return ___mMinValue_8; }
	inline float* get_address_of_mMinValue_8() { return &___mMinValue_8; }
	inline void set_mMinValue_8(float value)
	{
		___mMinValue_8 = value;
	}

	inline static int32_t get_offset_of_mMaxValue_9() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___mMaxValue_9)); }
	inline float get_mMaxValue_9() const { return ___mMaxValue_9; }
	inline float* get_address_of_mMaxValue_9() { return &___mMaxValue_9; }
	inline void set_mMaxValue_9(float value)
	{
		___mMaxValue_9 = value;
	}

	inline static int32_t get_offset_of_mWrapAround_10() { return static_cast<int32_t>(offsetof(AxisState_t478218302, ___mWrapAround_10)); }
	inline bool get_mWrapAround_10() const { return ___mWrapAround_10; }
	inline bool* get_address_of_mWrapAround_10() { return &___mWrapAround_10; }
	inline void set_mWrapAround_10(bool value)
	{
		___mWrapAround_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.AxisState
struct AxisState_t478218302_marshaled_pinvoke
{
	float ___Value_0;
	float ___m_MaxSpeed_1;
	float ___m_AccelTime_2;
	float ___m_DecelTime_3;
	char* ___m_InputAxisName_4;
	float ___m_InputAxisValue_5;
	int32_t ___m_InvertAxis_6;
	float ___mCurrentSpeed_7;
	float ___mMinValue_8;
	float ___mMaxValue_9;
	int32_t ___mWrapAround_10;
};
// Native definition for COM marshalling of Cinemachine.AxisState
struct AxisState_t478218302_marshaled_com
{
	float ___Value_0;
	float ___m_MaxSpeed_1;
	float ___m_AccelTime_2;
	float ___m_DecelTime_3;
	Il2CppChar* ___m_InputAxisName_4;
	float ___m_InputAxisValue_5;
	int32_t ___m_InvertAxis_6;
	float ___mCurrentSpeed_7;
	float ___mMinValue_8;
	float ___mMaxValue_9;
	int32_t ___mWrapAround_10;
};
#endif // AXISSTATE_T478218302_H
#ifndef TARGET_T116854977_H
#define TARGET_T116854977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/Target
struct  Target_t116854977 
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineTargetGroup/Target::target
	Transform_t3600365921 * ___target_0;
	// System.Single Cinemachine.CinemachineTargetGroup/Target::weight
	float ___weight_1;
	// System.Single Cinemachine.CinemachineTargetGroup/Target::radius
	float ___radius_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Target_t116854977, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Target_t116854977, ___weight_1)); }
	inline float get_weight_1() const { return ___weight_1; }
	inline float* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(float value)
	{
		___weight_1 = value;
	}

	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(Target_t116854977, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineTargetGroup/Target
struct Target_t116854977_marshaled_pinvoke
{
	Transform_t3600365921 * ___target_0;
	float ___weight_1;
	float ___radius_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineTargetGroup/Target
struct Target_t116854977_marshaled_com
{
	Transform_t3600365921 * ___target_0;
	float ___weight_1;
	float ___radius_2;
};
#endif // TARGET_T116854977_H
#ifndef GAUSSIANWINDOW1D_VECTOR3_T926608352_H
#define GAUSSIANWINDOW1D_VECTOR3_T926608352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.GaussianWindow1D_Vector3
struct  GaussianWindow1D_Vector3_t926608352  : public GaussianWindow1d_1_t2971921511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANWINDOW1D_VECTOR3_T926608352_H
#ifndef TAGFIELDATTRIBUTE_T2187080624_H
#define TAGFIELDATTRIBUTE_T2187080624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.TagFieldAttribute
struct  TagFieldAttribute_t2187080624  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGFIELDATTRIBUTE_T2187080624_H
#ifndef NOSAVEDURINGPLAYATTRIBUTE_T350030686_H
#define NOSAVEDURINGPLAYATTRIBUTE_T350030686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoSaveDuringPlayAttribute
struct  NoSaveDuringPlayAttribute_t350030686  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOSAVEDURINGPLAYATTRIBUTE_T350030686_H
#ifndef TRANSFORMNOISEPARAMS_T763395729_H
#define TRANSFORMNOISEPARAMS_T763395729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoiseSettings/TransformNoiseParams
struct  TransformNoiseParams_t763395729 
{
public:
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::X
	NoiseParams_t2342455097  ___X_0;
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::Y
	NoiseParams_t2342455097  ___Y_1;
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::Z
	NoiseParams_t2342455097  ___Z_2;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t763395729, ___X_0)); }
	inline NoiseParams_t2342455097  get_X_0() const { return ___X_0; }
	inline NoiseParams_t2342455097 * get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(NoiseParams_t2342455097  value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t763395729, ___Y_1)); }
	inline NoiseParams_t2342455097  get_Y_1() const { return ___Y_1; }
	inline NoiseParams_t2342455097 * get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(NoiseParams_t2342455097  value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_Z_2() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t763395729, ___Z_2)); }
	inline NoiseParams_t2342455097  get_Z_2() const { return ___Z_2; }
	inline NoiseParams_t2342455097 * get_address_of_Z_2() { return &___Z_2; }
	inline void set_Z_2(NoiseParams_t2342455097  value)
	{
		___Z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMNOISEPARAMS_T763395729_H
#ifndef STYLE_T1056527879_H
#define STYLE_T1056527879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinition/Style
struct  Style_t1056527879 
{
public:
	// System.Int32 Cinemachine.CinemachineBlendDefinition/Style::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Style_t1056527879, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T1056527879_H
#ifndef UPDATEFILTER_T3852455381_H
#define UPDATEFILTER_T3852455381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/UpdateFilter
struct  UpdateFilter_t3852455381 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/UpdateFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateFilter_t3852455381, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEFILTER_T3852455381_H
#ifndef CINEMACHINEBLENDDEFINITIONPROPERTYATTRIBUTE_T1893815722_H
#define CINEMACHINEBLENDDEFINITIONPROPERTYATTRIBUTE_T1893815722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinitionPropertyAttribute
struct  CinemachineBlendDefinitionPropertyAttribute_t1893815722  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDDEFINITIONPROPERTYATTRIBUTE_T1893815722_H
#ifndef LENSSETTINGSPROPERTYATTRIBUTE_T289899148_H
#define LENSSETTINGSPROPERTYATTRIBUTE_T289899148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.LensSettingsPropertyAttribute
struct  LensSettingsPropertyAttribute_t289899148  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSSETTINGSPROPERTYATTRIBUTE_T289899148_H
#ifndef POSITIONUNITS_T2997231777_H
#define POSITIONUNITS_T2997231777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePathBase/PositionUnits
struct  PositionUnits_t2997231777 
{
public:
	// System.Int32 Cinemachine.CinemachinePathBase/PositionUnits::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PositionUnits_t2997231777, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONUNITS_T2997231777_H
#ifndef STAGE_T3116928195_H
#define STAGE_T3116928195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/Stage
struct  Stage_t3116928195 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/Stage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Stage_t3116928195, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAGE_T3116928195_H
#ifndef APPEARANCE_T243588924_H
#define APPEARANCE_T243588924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePathBase/Appearance
struct  Appearance_t243588924  : public RuntimeObject
{
public:
	// UnityEngine.Color Cinemachine.CinemachinePathBase/Appearance::pathColor
	Color_t2555686324  ___pathColor_0;
	// UnityEngine.Color Cinemachine.CinemachinePathBase/Appearance::inactivePathColor
	Color_t2555686324  ___inactivePathColor_1;
	// System.Single Cinemachine.CinemachinePathBase/Appearance::width
	float ___width_2;

public:
	inline static int32_t get_offset_of_pathColor_0() { return static_cast<int32_t>(offsetof(Appearance_t243588924, ___pathColor_0)); }
	inline Color_t2555686324  get_pathColor_0() const { return ___pathColor_0; }
	inline Color_t2555686324 * get_address_of_pathColor_0() { return &___pathColor_0; }
	inline void set_pathColor_0(Color_t2555686324  value)
	{
		___pathColor_0 = value;
	}

	inline static int32_t get_offset_of_inactivePathColor_1() { return static_cast<int32_t>(offsetof(Appearance_t243588924, ___inactivePathColor_1)); }
	inline Color_t2555686324  get_inactivePathColor_1() const { return ___inactivePathColor_1; }
	inline Color_t2555686324 * get_address_of_inactivePathColor_1() { return &___inactivePathColor_1; }
	inline void set_inactivePathColor_1(Color_t2555686324  value)
	{
		___inactivePathColor_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(Appearance_t243588924, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEARANCE_T243588924_H
#ifndef POSITIONPREDICTOR_T974033707_H
#define POSITIONPREDICTOR_T974033707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Utility.PositionPredictor
struct  PositionPredictor_t974033707  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Cinemachine.Utility.PositionPredictor::m_Position
	Vector3_t3722313464  ___m_Position_0;
	// System.Single Cinemachine.Utility.PositionPredictor::mSmoothing
	float ___mSmoothing_2;
	// Cinemachine.Utility.GaussianWindow1D_Vector3 Cinemachine.Utility.PositionPredictor::m_Velocity
	GaussianWindow1D_Vector3_t926608352 * ___m_Velocity_3;
	// Cinemachine.Utility.GaussianWindow1D_Vector3 Cinemachine.Utility.PositionPredictor::m_Accel
	GaussianWindow1D_Vector3_t926608352 * ___m_Accel_4;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(PositionPredictor_t974033707, ___m_Position_0)); }
	inline Vector3_t3722313464  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t3722313464 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t3722313464  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_mSmoothing_2() { return static_cast<int32_t>(offsetof(PositionPredictor_t974033707, ___mSmoothing_2)); }
	inline float get_mSmoothing_2() const { return ___mSmoothing_2; }
	inline float* get_address_of_mSmoothing_2() { return &___mSmoothing_2; }
	inline void set_mSmoothing_2(float value)
	{
		___mSmoothing_2 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_3() { return static_cast<int32_t>(offsetof(PositionPredictor_t974033707, ___m_Velocity_3)); }
	inline GaussianWindow1D_Vector3_t926608352 * get_m_Velocity_3() const { return ___m_Velocity_3; }
	inline GaussianWindow1D_Vector3_t926608352 ** get_address_of_m_Velocity_3() { return &___m_Velocity_3; }
	inline void set_m_Velocity_3(GaussianWindow1D_Vector3_t926608352 * value)
	{
		___m_Velocity_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Velocity_3), value);
	}

	inline static int32_t get_offset_of_m_Accel_4() { return static_cast<int32_t>(offsetof(PositionPredictor_t974033707, ___m_Accel_4)); }
	inline GaussianWindow1D_Vector3_t926608352 * get_m_Accel_4() const { return ___m_Accel_4; }
	inline GaussianWindow1D_Vector3_t926608352 ** get_address_of_m_Accel_4() { return &___m_Accel_4; }
	inline void set_m_Accel_4(GaussianWindow1D_Vector3_t926608352 * value)
	{
		___m_Accel_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Accel_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONPREDICTOR_T974033707_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef UPDATEMETHOD_T1906263433_H
#define UPDATEMETHOD_T1906263433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/UpdateMethod
struct  UpdateMethod_t1906263433 
{
public:
	// System.Int32 Cinemachine.CinemachineTargetGroup/UpdateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateMethod_t1906263433, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMETHOD_T1906263433_H
#ifndef ROTATIONMODE_T1993955844_H
#define ROTATIONMODE_T1993955844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/RotationMode
struct  RotationMode_t1993955844 
{
public:
	// System.Int32 Cinemachine.CinemachineTargetGroup/RotationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationMode_t1993955844, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONMODE_T1993955844_H
#ifndef POSITIONMODE_T2068418555_H
#define POSITIONMODE_T2068418555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup/PositionMode
struct  PositionMode_t2068418555 
{
public:
	// System.Int32 Cinemachine.CinemachineTargetGroup/PositionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PositionMode_t2068418555, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONMODE_T2068418555_H
#ifndef WAYPOINT_T1322198744_H
#define WAYPOINT_T1322198744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineSmoothPath/Waypoint
struct  Waypoint_t1322198744 
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineSmoothPath/Waypoint::position
	Vector3_t3722313464  ___position_0;
	// System.Single Cinemachine.CinemachineSmoothPath/Waypoint::roll
	float ___roll_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Waypoint_t1322198744, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_roll_1() { return static_cast<int32_t>(offsetof(Waypoint_t1322198744, ___roll_1)); }
	inline float get_roll_1() const { return ___roll_1; }
	inline float* get_address_of_roll_1() { return &___roll_1; }
	inline void set_roll_1(float value)
	{
		___roll_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINT_T1322198744_H
#ifndef WAYPOINT_T2405927058_H
#define WAYPOINT_T2405927058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePath/Waypoint
struct  Waypoint_t2405927058 
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachinePath/Waypoint::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 Cinemachine.CinemachinePath/Waypoint::tangent
	Vector3_t3722313464  ___tangent_1;
	// System.Single Cinemachine.CinemachinePath/Waypoint::roll
	float ___roll_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Waypoint_t2405927058, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_tangent_1() { return static_cast<int32_t>(offsetof(Waypoint_t2405927058, ___tangent_1)); }
	inline Vector3_t3722313464  get_tangent_1() const { return ___tangent_1; }
	inline Vector3_t3722313464 * get_address_of_tangent_1() { return &___tangent_1; }
	inline void set_tangent_1(Vector3_t3722313464  value)
	{
		___tangent_1 = value;
	}

	inline static int32_t get_offset_of_roll_2() { return static_cast<int32_t>(offsetof(Waypoint_t2405927058, ___roll_2)); }
	inline float get_roll_2() const { return ___roll_2; }
	inline float* get_address_of_roll_2() { return &___roll_2; }
	inline void set_roll_2(float value)
	{
		___roll_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINT_T2405927058_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef UPDATEMETHOD_T147924144_H
#define UPDATEMETHOD_T147924144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineDollyCart/UpdateMethod
struct  UpdateMethod_t147924144 
{
public:
	// System.Int32 Cinemachine.CinemachineDollyCart/UpdateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateMethod_t147924144, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMETHOD_T147924144_H
#ifndef VCAMEXTRASTATE_T647499534_H
#define VCAMEXTRASTATE_T647499534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineConfiner/VcamExtraState
struct  VcamExtraState_t647499534  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineConfiner/VcamExtraState::m_previousDisplacement
	Vector3_t3722313464  ___m_previousDisplacement_0;
	// System.Single Cinemachine.CinemachineConfiner/VcamExtraState::confinerDisplacement
	float ___confinerDisplacement_1;

public:
	inline static int32_t get_offset_of_m_previousDisplacement_0() { return static_cast<int32_t>(offsetof(VcamExtraState_t647499534, ___m_previousDisplacement_0)); }
	inline Vector3_t3722313464  get_m_previousDisplacement_0() const { return ___m_previousDisplacement_0; }
	inline Vector3_t3722313464 * get_address_of_m_previousDisplacement_0() { return &___m_previousDisplacement_0; }
	inline void set_m_previousDisplacement_0(Vector3_t3722313464  value)
	{
		___m_previousDisplacement_0 = value;
	}

	inline static int32_t get_offset_of_confinerDisplacement_1() { return static_cast<int32_t>(offsetof(VcamExtraState_t647499534, ___confinerDisplacement_1)); }
	inline float get_confinerDisplacement_1() const { return ___confinerDisplacement_1; }
	inline float* get_address_of_confinerDisplacement_1() { return &___confinerDisplacement_1; }
	inline void set_confinerDisplacement_1(float value)
	{
		___confinerDisplacement_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VCAMEXTRASTATE_T647499534_H
#ifndef MODE_T1560670659_H
#define MODE_T1560670659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineConfiner/Mode
struct  Mode_t1560670659 
{
public:
	// System.Int32 Cinemachine.CinemachineConfiner/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1560670659, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1560670659_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef BINDINGMODE_T1806219840_H
#define BINDINGMODE_T1806219840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTransposer/BindingMode
struct  BindingMode_t1806219840 
{
public:
	// System.Int32 Cinemachine.CinemachineTransposer/BindingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingMode_t1806219840, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGMODE_T1806219840_H
#ifndef CAMERAUPMODE_T3833165741_H
#define CAMERAUPMODE_T3833165741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTrackedDolly/CameraUpMode
struct  CameraUpMode_t3833165741 
{
public:
	// System.Int32 Cinemachine.CinemachineTrackedDolly/CameraUpMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraUpMode_t3833165741, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAUPMODE_T3833165741_H
#ifndef ITEM_T2807873350_H
#define ITEM_T2807873350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item
struct  Item_t2807873350 
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item::velocity
	Vector3_t3722313464  ___velocity_0;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item::weight
	float ___weight_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item::time
	float ___time_2;

public:
	inline static int32_t get_offset_of_velocity_0() { return static_cast<int32_t>(offsetof(Item_t2807873350, ___velocity_0)); }
	inline Vector3_t3722313464  get_velocity_0() const { return ___velocity_0; }
	inline Vector3_t3722313464 * get_address_of_velocity_0() { return &___velocity_0; }
	inline void set_velocity_0(Vector3_t3722313464  value)
	{
		___velocity_0 = value;
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Item_t2807873350, ___weight_1)); }
	inline float get_weight_1() const { return ___weight_1; }
	inline float* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(float value)
	{
		___weight_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Item_t2807873350, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T2807873350_H
#ifndef HEADINGTRACKER_T85731283_H
#define HEADINGTRACKER_T85731283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker
struct  HeadingTracker_t85731283  : public RuntimeObject
{
public:
	// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker/Item[] Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mHistory
	ItemU5BU5D_t1337847011* ___mHistory_0;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mTop
	int32_t ___mTop_1;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mBottom
	int32_t ___mBottom_2;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mCount
	int32_t ___mCount_3;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mHeadingSum
	Vector3_t3722313464  ___mHeadingSum_4;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mWeightSum
	float ___mWeightSum_5;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mWeightTime
	float ___mWeightTime_6;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mLastGoodHeading
	Vector3_t3722313464  ___mLastGoodHeading_7;

public:
	inline static int32_t get_offset_of_mHistory_0() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mHistory_0)); }
	inline ItemU5BU5D_t1337847011* get_mHistory_0() const { return ___mHistory_0; }
	inline ItemU5BU5D_t1337847011** get_address_of_mHistory_0() { return &___mHistory_0; }
	inline void set_mHistory_0(ItemU5BU5D_t1337847011* value)
	{
		___mHistory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHistory_0), value);
	}

	inline static int32_t get_offset_of_mTop_1() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mTop_1)); }
	inline int32_t get_mTop_1() const { return ___mTop_1; }
	inline int32_t* get_address_of_mTop_1() { return &___mTop_1; }
	inline void set_mTop_1(int32_t value)
	{
		___mTop_1 = value;
	}

	inline static int32_t get_offset_of_mBottom_2() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mBottom_2)); }
	inline int32_t get_mBottom_2() const { return ___mBottom_2; }
	inline int32_t* get_address_of_mBottom_2() { return &___mBottom_2; }
	inline void set_mBottom_2(int32_t value)
	{
		___mBottom_2 = value;
	}

	inline static int32_t get_offset_of_mCount_3() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mCount_3)); }
	inline int32_t get_mCount_3() const { return ___mCount_3; }
	inline int32_t* get_address_of_mCount_3() { return &___mCount_3; }
	inline void set_mCount_3(int32_t value)
	{
		___mCount_3 = value;
	}

	inline static int32_t get_offset_of_mHeadingSum_4() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mHeadingSum_4)); }
	inline Vector3_t3722313464  get_mHeadingSum_4() const { return ___mHeadingSum_4; }
	inline Vector3_t3722313464 * get_address_of_mHeadingSum_4() { return &___mHeadingSum_4; }
	inline void set_mHeadingSum_4(Vector3_t3722313464  value)
	{
		___mHeadingSum_4 = value;
	}

	inline static int32_t get_offset_of_mWeightSum_5() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mWeightSum_5)); }
	inline float get_mWeightSum_5() const { return ___mWeightSum_5; }
	inline float* get_address_of_mWeightSum_5() { return &___mWeightSum_5; }
	inline void set_mWeightSum_5(float value)
	{
		___mWeightSum_5 = value;
	}

	inline static int32_t get_offset_of_mWeightTime_6() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mWeightTime_6)); }
	inline float get_mWeightTime_6() const { return ___mWeightTime_6; }
	inline float* get_address_of_mWeightTime_6() { return &___mWeightTime_6; }
	inline void set_mWeightTime_6(float value)
	{
		___mWeightTime_6 = value;
	}

	inline static int32_t get_offset_of_mLastGoodHeading_7() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283, ___mLastGoodHeading_7)); }
	inline Vector3_t3722313464  get_mLastGoodHeading_7() const { return ___mLastGoodHeading_7; }
	inline Vector3_t3722313464 * get_address_of_mLastGoodHeading_7() { return &___mLastGoodHeading_7; }
	inline void set_mLastGoodHeading_7(Vector3_t3722313464  value)
	{
		___mLastGoodHeading_7 = value;
	}
};

struct HeadingTracker_t85731283_StaticFields
{
public:
	// System.Single Cinemachine.CinemachineOrbitalTransposer/HeadingTracker::mDecayExponent
	float ___mDecayExponent_8;

public:
	inline static int32_t get_offset_of_mDecayExponent_8() { return static_cast<int32_t>(offsetof(HeadingTracker_t85731283_StaticFields, ___mDecayExponent_8)); }
	inline float get_mDecayExponent_8() const { return ___mDecayExponent_8; }
	inline float* get_address_of_mDecayExponent_8() { return &___mDecayExponent_8; }
	inline void set_mDecayExponent_8(float value)
	{
		___mDecayExponent_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADINGTRACKER_T85731283_H
#ifndef HEADINGDEFINITION_T1970467155_H
#define HEADINGDEFINITION_T1970467155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/Heading/HeadingDefinition
struct  HeadingDefinition_t1970467155 
{
public:
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/Heading/HeadingDefinition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HeadingDefinition_t1970467155, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADINGDEFINITION_T1970467155_H
#ifndef LEVEL_T4265333678_H
#define LEVEL_T4265333678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.DocumentationSortingAttribute/Level
struct  Level_t4265333678 
{
public:
	// System.Int32 Cinemachine.DocumentationSortingAttribute/Level::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Level_t4265333678, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T4265333678_H
#ifndef FRAMINGMODE_T1646599265_H
#define FRAMINGMODE_T1646599265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFramingTransposer/FramingMode
struct  FramingMode_t1646599265 
{
public:
	// System.Int32 Cinemachine.CinemachineFramingTransposer/FramingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramingMode_t1646599265, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMINGMODE_T1646599265_H
#ifndef ADJUSTMENTMODE_T2584965458_H
#define ADJUSTMENTMODE_T2584965458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFramingTransposer/AdjustmentMode
struct  AdjustmentMode_t2584965458 
{
public:
	// System.Int32 Cinemachine.CinemachineFramingTransposer/AdjustmentMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdjustmentMode_t2584965458, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTMENTMODE_T2584965458_H
#ifndef FRAMINGMODE_T2640351704_H
#define FRAMINGMODE_T2640351704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineGroupComposer/FramingMode
struct  FramingMode_t2640351704 
{
public:
	// System.Int32 Cinemachine.CinemachineGroupComposer/FramingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramingMode_t2640351704, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMINGMODE_T2640351704_H
#ifndef ADJUSTMENTMODE_T857859900_H
#define ADJUSTMENTMODE_T857859900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineGroupComposer/AdjustmentMode
struct  AdjustmentMode_t857859900 
{
public:
	// System.Int32 Cinemachine.CinemachineGroupComposer/AdjustmentMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdjustmentMode_t857859900, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTMENTMODE_T857859900_H
#ifndef EXPOSEDREFERENCE_1_T1593012215_H
#define EXPOSEDREFERENCE_1_T1593012215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExposedReference`1<Cinemachine.CinemachineVirtualCameraBase>
struct  ExposedReference_1_t1593012215 
{
public:
	// UnityEngine.PropertyName UnityEngine.ExposedReference`1::exposedName
	PropertyName_t3749835189  ___exposedName_0;
	// UnityEngine.Object UnityEngine.ExposedReference`1::defaultValue
	Object_t631007953 * ___defaultValue_1;

public:
	inline static int32_t get_offset_of_exposedName_0() { return static_cast<int32_t>(offsetof(ExposedReference_1_t1593012215, ___exposedName_0)); }
	inline PropertyName_t3749835189  get_exposedName_0() const { return ___exposedName_0; }
	inline PropertyName_t3749835189 * get_address_of_exposedName_0() { return &___exposedName_0; }
	inline void set_exposedName_0(PropertyName_t3749835189  value)
	{
		___exposedName_0 = value;
	}

	inline static int32_t get_offset_of_defaultValue_1() { return static_cast<int32_t>(offsetof(ExposedReference_1_t1593012215, ___defaultValue_1)); }
	inline Object_t631007953 * get_defaultValue_1() const { return ___defaultValue_1; }
	inline Object_t631007953 ** get_address_of_defaultValue_1() { return &___defaultValue_1; }
	inline void set_defaultValue_1(Object_t631007953 * value)
	{
		___defaultValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ExposedReference`1
#ifndef ExposedReference_1_t2548232636_marshaled_pinvoke_define
#define ExposedReference_1_t2548232636_marshaled_pinvoke_define
struct ExposedReference_1_t2548232636_marshaled_pinvoke
{
	PropertyName_t3749835189  ___exposedName_0;
	Object_t631007953_marshaled_pinvoke ___defaultValue_1;
};
#endif
// Native definition for COM marshalling of UnityEngine.ExposedReference`1
#ifndef ExposedReference_1_t2548232636_marshaled_com_define
#define ExposedReference_1_t2548232636_marshaled_com_define
struct ExposedReference_1_t2548232636_marshaled_com
{
	PropertyName_t3749835189  ___exposedName_0;
	Object_t631007953_marshaled_com* ___defaultValue_1;
};
#endif
#endif // EXPOSEDREFERENCE_1_T1593012215_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef CINEMACHINECORE_T2342901799_H
#define CINEMACHINECORE_T2342901799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore
struct  CinemachineCore_t2342901799  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain> Cinemachine.CinemachineCore::mActiveBrains
	List_1_t2500574876 * ___mActiveBrains_5;
	// System.Collections.Generic.List`1<Cinemachine.ICinemachineCamera> Cinemachine.CinemachineCore::mActiveCameras
	List_1_t2860362818 * ___mActiveCameras_6;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Cinemachine.ICinemachineCamera>> Cinemachine.CinemachineCore::mChildCameras
	List_1_t37470264 * ___mChildCameras_7;
	// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,Cinemachine.CinemachineCore/UpdateStatus> Cinemachine.CinemachineCore::mUpdateStatus
	Dictionary_2_t3701647757 * ___mUpdateStatus_8;
	// Cinemachine.CinemachineCore/UpdateFilter Cinemachine.CinemachineCore::<CurrentUpdateFilter>k__BackingField
	int32_t ___U3CCurrentUpdateFilterU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_mActiveBrains_5() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799, ___mActiveBrains_5)); }
	inline List_1_t2500574876 * get_mActiveBrains_5() const { return ___mActiveBrains_5; }
	inline List_1_t2500574876 ** get_address_of_mActiveBrains_5() { return &___mActiveBrains_5; }
	inline void set_mActiveBrains_5(List_1_t2500574876 * value)
	{
		___mActiveBrains_5 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveBrains_5), value);
	}

	inline static int32_t get_offset_of_mActiveCameras_6() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799, ___mActiveCameras_6)); }
	inline List_1_t2860362818 * get_mActiveCameras_6() const { return ___mActiveCameras_6; }
	inline List_1_t2860362818 ** get_address_of_mActiveCameras_6() { return &___mActiveCameras_6; }
	inline void set_mActiveCameras_6(List_1_t2860362818 * value)
	{
		___mActiveCameras_6 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveCameras_6), value);
	}

	inline static int32_t get_offset_of_mChildCameras_7() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799, ___mChildCameras_7)); }
	inline List_1_t37470264 * get_mChildCameras_7() const { return ___mChildCameras_7; }
	inline List_1_t37470264 ** get_address_of_mChildCameras_7() { return &___mChildCameras_7; }
	inline void set_mChildCameras_7(List_1_t37470264 * value)
	{
		___mChildCameras_7 = value;
		Il2CppCodeGenWriteBarrier((&___mChildCameras_7), value);
	}

	inline static int32_t get_offset_of_mUpdateStatus_8() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799, ___mUpdateStatus_8)); }
	inline Dictionary_2_t3701647757 * get_mUpdateStatus_8() const { return ___mUpdateStatus_8; }
	inline Dictionary_2_t3701647757 ** get_address_of_mUpdateStatus_8() { return &___mUpdateStatus_8; }
	inline void set_mUpdateStatus_8(Dictionary_2_t3701647757 * value)
	{
		___mUpdateStatus_8 = value;
		Il2CppCodeGenWriteBarrier((&___mUpdateStatus_8), value);
	}

	inline static int32_t get_offset_of_U3CCurrentUpdateFilterU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799, ___U3CCurrentUpdateFilterU3Ek__BackingField_9)); }
	inline int32_t get_U3CCurrentUpdateFilterU3Ek__BackingField_9() const { return ___U3CCurrentUpdateFilterU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CCurrentUpdateFilterU3Ek__BackingField_9() { return &___U3CCurrentUpdateFilterU3Ek__BackingField_9; }
	inline void set_U3CCurrentUpdateFilterU3Ek__BackingField_9(int32_t value)
	{
		___U3CCurrentUpdateFilterU3Ek__BackingField_9 = value;
	}
};

struct CinemachineCore_t2342901799_StaticFields
{
public:
	// System.Int32 Cinemachine.CinemachineCore::kStreamingVersion
	int32_t ___kStreamingVersion_0;
	// System.String Cinemachine.CinemachineCore::kVersionString
	String_t* ___kVersionString_1;
	// Cinemachine.CinemachineCore Cinemachine.CinemachineCore::sInstance
	CinemachineCore_t2342901799 * ___sInstance_2;
	// System.Boolean Cinemachine.CinemachineCore::sShowHiddenObjects
	bool ___sShowHiddenObjects_3;
	// Cinemachine.CinemachineCore/AxisInputDelegate Cinemachine.CinemachineCore::GetInputAxis
	AxisInputDelegate_t365085504 * ___GetInputAxis_4;
	// Cinemachine.CinemachineCore/AxisInputDelegate Cinemachine.CinemachineCore::<>f__mg$cache0
	AxisInputDelegate_t365085504 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_kStreamingVersion_0() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799_StaticFields, ___kStreamingVersion_0)); }
	inline int32_t get_kStreamingVersion_0() const { return ___kStreamingVersion_0; }
	inline int32_t* get_address_of_kStreamingVersion_0() { return &___kStreamingVersion_0; }
	inline void set_kStreamingVersion_0(int32_t value)
	{
		___kStreamingVersion_0 = value;
	}

	inline static int32_t get_offset_of_kVersionString_1() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799_StaticFields, ___kVersionString_1)); }
	inline String_t* get_kVersionString_1() const { return ___kVersionString_1; }
	inline String_t** get_address_of_kVersionString_1() { return &___kVersionString_1; }
	inline void set_kVersionString_1(String_t* value)
	{
		___kVersionString_1 = value;
		Il2CppCodeGenWriteBarrier((&___kVersionString_1), value);
	}

	inline static int32_t get_offset_of_sInstance_2() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799_StaticFields, ___sInstance_2)); }
	inline CinemachineCore_t2342901799 * get_sInstance_2() const { return ___sInstance_2; }
	inline CinemachineCore_t2342901799 ** get_address_of_sInstance_2() { return &___sInstance_2; }
	inline void set_sInstance_2(CinemachineCore_t2342901799 * value)
	{
		___sInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_2), value);
	}

	inline static int32_t get_offset_of_sShowHiddenObjects_3() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799_StaticFields, ___sShowHiddenObjects_3)); }
	inline bool get_sShowHiddenObjects_3() const { return ___sShowHiddenObjects_3; }
	inline bool* get_address_of_sShowHiddenObjects_3() { return &___sShowHiddenObjects_3; }
	inline void set_sShowHiddenObjects_3(bool value)
	{
		___sShowHiddenObjects_3 = value;
	}

	inline static int32_t get_offset_of_GetInputAxis_4() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799_StaticFields, ___GetInputAxis_4)); }
	inline AxisInputDelegate_t365085504 * get_GetInputAxis_4() const { return ___GetInputAxis_4; }
	inline AxisInputDelegate_t365085504 ** get_address_of_GetInputAxis_4() { return &___GetInputAxis_4; }
	inline void set_GetInputAxis_4(AxisInputDelegate_t365085504 * value)
	{
		___GetInputAxis_4 = value;
		Il2CppCodeGenWriteBarrier((&___GetInputAxis_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(CinemachineCore_t2342901799_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline AxisInputDelegate_t365085504 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline AxisInputDelegate_t365085504 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(AxisInputDelegate_t365085504 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECORE_T2342901799_H
#ifndef UPDATESTATUS_T592875705_H
#define UPDATESTATUS_T592875705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/UpdateStatus
struct  UpdateStatus_t592875705 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::lastUpdateFrame
	int32_t ___lastUpdateFrame_1;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::lastUpdateSubframe
	int32_t ___lastUpdateSubframe_2;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::windowStart
	int32_t ___windowStart_3;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::numWindowLateUpdateMoves
	int32_t ___numWindowLateUpdateMoves_4;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::numWindowFixedUpdateMoves
	int32_t ___numWindowFixedUpdateMoves_5;
	// System.Int32 Cinemachine.CinemachineCore/UpdateStatus::numWindows
	int32_t ___numWindows_6;
	// Cinemachine.CinemachineCore/UpdateFilter Cinemachine.CinemachineCore/UpdateStatus::preferredUpdate
	int32_t ___preferredUpdate_7;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineCore/UpdateStatus::targetPos
	Matrix4x4_t1817901843  ___targetPos_8;

public:
	inline static int32_t get_offset_of_lastUpdateFrame_1() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___lastUpdateFrame_1)); }
	inline int32_t get_lastUpdateFrame_1() const { return ___lastUpdateFrame_1; }
	inline int32_t* get_address_of_lastUpdateFrame_1() { return &___lastUpdateFrame_1; }
	inline void set_lastUpdateFrame_1(int32_t value)
	{
		___lastUpdateFrame_1 = value;
	}

	inline static int32_t get_offset_of_lastUpdateSubframe_2() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___lastUpdateSubframe_2)); }
	inline int32_t get_lastUpdateSubframe_2() const { return ___lastUpdateSubframe_2; }
	inline int32_t* get_address_of_lastUpdateSubframe_2() { return &___lastUpdateSubframe_2; }
	inline void set_lastUpdateSubframe_2(int32_t value)
	{
		___lastUpdateSubframe_2 = value;
	}

	inline static int32_t get_offset_of_windowStart_3() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___windowStart_3)); }
	inline int32_t get_windowStart_3() const { return ___windowStart_3; }
	inline int32_t* get_address_of_windowStart_3() { return &___windowStart_3; }
	inline void set_windowStart_3(int32_t value)
	{
		___windowStart_3 = value;
	}

	inline static int32_t get_offset_of_numWindowLateUpdateMoves_4() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___numWindowLateUpdateMoves_4)); }
	inline int32_t get_numWindowLateUpdateMoves_4() const { return ___numWindowLateUpdateMoves_4; }
	inline int32_t* get_address_of_numWindowLateUpdateMoves_4() { return &___numWindowLateUpdateMoves_4; }
	inline void set_numWindowLateUpdateMoves_4(int32_t value)
	{
		___numWindowLateUpdateMoves_4 = value;
	}

	inline static int32_t get_offset_of_numWindowFixedUpdateMoves_5() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___numWindowFixedUpdateMoves_5)); }
	inline int32_t get_numWindowFixedUpdateMoves_5() const { return ___numWindowFixedUpdateMoves_5; }
	inline int32_t* get_address_of_numWindowFixedUpdateMoves_5() { return &___numWindowFixedUpdateMoves_5; }
	inline void set_numWindowFixedUpdateMoves_5(int32_t value)
	{
		___numWindowFixedUpdateMoves_5 = value;
	}

	inline static int32_t get_offset_of_numWindows_6() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___numWindows_6)); }
	inline int32_t get_numWindows_6() const { return ___numWindows_6; }
	inline int32_t* get_address_of_numWindows_6() { return &___numWindows_6; }
	inline void set_numWindows_6(int32_t value)
	{
		___numWindows_6 = value;
	}

	inline static int32_t get_offset_of_preferredUpdate_7() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___preferredUpdate_7)); }
	inline int32_t get_preferredUpdate_7() const { return ___preferredUpdate_7; }
	inline int32_t* get_address_of_preferredUpdate_7() { return &___preferredUpdate_7; }
	inline void set_preferredUpdate_7(int32_t value)
	{
		___preferredUpdate_7 = value;
	}

	inline static int32_t get_offset_of_targetPos_8() { return static_cast<int32_t>(offsetof(UpdateStatus_t592875705, ___targetPos_8)); }
	inline Matrix4x4_t1817901843  get_targetPos_8() const { return ___targetPos_8; }
	inline Matrix4x4_t1817901843 * get_address_of_targetPos_8() { return &___targetPos_8; }
	inline void set_targetPos_8(Matrix4x4_t1817901843  value)
	{
		___targetPos_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATUS_T592875705_H
#ifndef CINEMACHINEBLENDDEFINITION_T3511030937_H
#define CINEMACHINEBLENDDEFINITION_T3511030937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlendDefinition
struct  CinemachineBlendDefinition_t3511030937 
{
public:
	// Cinemachine.CinemachineBlendDefinition/Style Cinemachine.CinemachineBlendDefinition::m_Style
	int32_t ___m_Style_0;
	// System.Single Cinemachine.CinemachineBlendDefinition::m_Time
	float ___m_Time_1;

public:
	inline static int32_t get_offset_of_m_Style_0() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t3511030937, ___m_Style_0)); }
	inline int32_t get_m_Style_0() const { return ___m_Style_0; }
	inline int32_t* get_address_of_m_Style_0() { return &___m_Style_0; }
	inline void set_m_Style_0(int32_t value)
	{
		___m_Style_0 = value;
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t3511030937, ___m_Time_1)); }
	inline float get_m_Time_1() const { return ___m_Time_1; }
	inline float* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(float value)
	{
		___m_Time_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDDEFINITION_T3511030937_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef HEADING_T365180425_H
#define HEADING_T365180425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/Heading
struct  Heading_t365180425 
{
public:
	// Cinemachine.CinemachineOrbitalTransposer/Heading/HeadingDefinition Cinemachine.CinemachineOrbitalTransposer/Heading::m_HeadingDefinition
	int32_t ___m_HeadingDefinition_0;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer/Heading::m_VelocityFilterStrength
	int32_t ___m_VelocityFilterStrength_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer/Heading::m_HeadingBias
	float ___m_HeadingBias_2;

public:
	inline static int32_t get_offset_of_m_HeadingDefinition_0() { return static_cast<int32_t>(offsetof(Heading_t365180425, ___m_HeadingDefinition_0)); }
	inline int32_t get_m_HeadingDefinition_0() const { return ___m_HeadingDefinition_0; }
	inline int32_t* get_address_of_m_HeadingDefinition_0() { return &___m_HeadingDefinition_0; }
	inline void set_m_HeadingDefinition_0(int32_t value)
	{
		___m_HeadingDefinition_0 = value;
	}

	inline static int32_t get_offset_of_m_VelocityFilterStrength_1() { return static_cast<int32_t>(offsetof(Heading_t365180425, ___m_VelocityFilterStrength_1)); }
	inline int32_t get_m_VelocityFilterStrength_1() const { return ___m_VelocityFilterStrength_1; }
	inline int32_t* get_address_of_m_VelocityFilterStrength_1() { return &___m_VelocityFilterStrength_1; }
	inline void set_m_VelocityFilterStrength_1(int32_t value)
	{
		___m_VelocityFilterStrength_1 = value;
	}

	inline static int32_t get_offset_of_m_HeadingBias_2() { return static_cast<int32_t>(offsetof(Heading_t365180425, ___m_HeadingBias_2)); }
	inline float get_m_HeadingBias_2() const { return ___m_HeadingBias_2; }
	inline float* get_address_of_m_HeadingBias_2() { return &___m_HeadingBias_2; }
	inline void set_m_HeadingBias_2(float value)
	{
		___m_HeadingBias_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADING_T365180425_H
#ifndef DOCUMENTATIONSORTINGATTRIBUTE_T2474015552_H
#define DOCUMENTATIONSORTINGATTRIBUTE_T2474015552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.DocumentationSortingAttribute
struct  DocumentationSortingAttribute_t2474015552  : public Attribute_t861562559
{
public:
	// System.Single Cinemachine.DocumentationSortingAttribute::<SortOrder>k__BackingField
	float ___U3CSortOrderU3Ek__BackingField_0;
	// Cinemachine.DocumentationSortingAttribute/Level Cinemachine.DocumentationSortingAttribute::<Category>k__BackingField
	int32_t ___U3CCategoryU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CSortOrderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentationSortingAttribute_t2474015552, ___U3CSortOrderU3Ek__BackingField_0)); }
	inline float get_U3CSortOrderU3Ek__BackingField_0() const { return ___U3CSortOrderU3Ek__BackingField_0; }
	inline float* get_address_of_U3CSortOrderU3Ek__BackingField_0() { return &___U3CSortOrderU3Ek__BackingField_0; }
	inline void set_U3CSortOrderU3Ek__BackingField_0(float value)
	{
		___U3CSortOrderU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCategoryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocumentationSortingAttribute_t2474015552, ___U3CCategoryU3Ek__BackingField_1)); }
	inline int32_t get_U3CCategoryU3Ek__BackingField_1() const { return ___U3CCategoryU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCategoryU3Ek__BackingField_1() { return &___U3CCategoryU3Ek__BackingField_1; }
	inline void set_U3CCategoryU3Ek__BackingField_1(int32_t value)
	{
		___U3CCategoryU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTATIONSORTINGATTRIBUTE_T2474015552_H
#ifndef CUSTOMBLENDABLE_T3531329220_H
#define CUSTOMBLENDABLE_T3531329220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CameraState/CustomBlendable
struct  CustomBlendable_t3531329220 
{
public:
	// UnityEngine.Object Cinemachine.CameraState/CustomBlendable::m_Custom
	Object_t631007953 * ___m_Custom_0;
	// System.Single Cinemachine.CameraState/CustomBlendable::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_Custom_0() { return static_cast<int32_t>(offsetof(CustomBlendable_t3531329220, ___m_Custom_0)); }
	inline Object_t631007953 * get_m_Custom_0() const { return ___m_Custom_0; }
	inline Object_t631007953 ** get_address_of_m_Custom_0() { return &___m_Custom_0; }
	inline void set_m_Custom_0(Object_t631007953 * value)
	{
		___m_Custom_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Custom_0), value);
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(CustomBlendable_t3531329220, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CameraState/CustomBlendable
struct CustomBlendable_t3531329220_marshaled_pinvoke
{
	Object_t631007953_marshaled_pinvoke ___m_Custom_0;
	float ___m_Weight_1;
};
// Native definition for COM marshalling of Cinemachine.CameraState/CustomBlendable
struct CustomBlendable_t3531329220_marshaled_com
{
	Object_t631007953_marshaled_com* ___m_Custom_0;
	float ___m_Weight_1;
};
#endif // CUSTOMBLENDABLE_T3531329220_H
#ifndef CINEMACHINEBLENDERSETTINGS_T3646651286_H
#define CINEMACHINEBLENDERSETTINGS_T3646651286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlenderSettings
struct  CinemachineBlenderSettings_t3646651286  : public ScriptableObject_t2528358522
{
public:
	// Cinemachine.CinemachineBlenderSettings/CustomBlend[] Cinemachine.CinemachineBlenderSettings::m_CustomBlends
	CustomBlendU5BU5D_t2035913683* ___m_CustomBlends_2;

public:
	inline static int32_t get_offset_of_m_CustomBlends_2() { return static_cast<int32_t>(offsetof(CinemachineBlenderSettings_t3646651286, ___m_CustomBlends_2)); }
	inline CustomBlendU5BU5D_t2035913683* get_m_CustomBlends_2() const { return ___m_CustomBlends_2; }
	inline CustomBlendU5BU5D_t2035913683** get_address_of_m_CustomBlends_2() { return &___m_CustomBlends_2; }
	inline void set_m_CustomBlends_2(CustomBlendU5BU5D_t2035913683* value)
	{
		___m_CustomBlends_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomBlends_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBLENDERSETTINGS_T3646651286_H
#ifndef CAMERASTATE_T560734301_H
#define CAMERASTATE_T560734301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CameraState
struct  CameraState_t560734301 
{
public:
	// Cinemachine.LensSettings Cinemachine.CameraState::<Lens>k__BackingField
	LensSettings_t822254920  ___U3CLensU3Ek__BackingField_0;
	// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceUp>k__BackingField
	Vector3_t3722313464  ___U3CReferenceUpU3Ek__BackingField_1;
	// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceLookAt>k__BackingField
	Vector3_t3722313464  ___U3CReferenceLookAtU3Ek__BackingField_2;
	// UnityEngine.Vector3 Cinemachine.CameraState::<RawPosition>k__BackingField
	Vector3_t3722313464  ___U3CRawPositionU3Ek__BackingField_4;
	// UnityEngine.Quaternion Cinemachine.CameraState::<RawOrientation>k__BackingField
	Quaternion_t2301928331  ___U3CRawOrientationU3Ek__BackingField_5;
	// UnityEngine.Vector3 Cinemachine.CameraState::<PositionDampingBypass>k__BackingField
	Vector3_t3722313464  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	// System.Single Cinemachine.CameraState::<ShotQuality>k__BackingField
	float ___U3CShotQualityU3Ek__BackingField_7;
	// UnityEngine.Vector3 Cinemachine.CameraState::<PositionCorrection>k__BackingField
	Vector3_t3722313464  ___U3CPositionCorrectionU3Ek__BackingField_8;
	// UnityEngine.Quaternion Cinemachine.CameraState::<OrientationCorrection>k__BackingField
	Quaternion_t2301928331  ___U3COrientationCorrectionU3Ek__BackingField_9;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom0
	CustomBlendable_t3531329220  ___mCustom0_10;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom1
	CustomBlendable_t3531329220  ___mCustom1_11;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom2
	CustomBlendable_t3531329220  ___mCustom2_12;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom3
	CustomBlendable_t3531329220  ___mCustom3_13;
	// System.Collections.Generic.List`1<Cinemachine.CameraState/CustomBlendable> Cinemachine.CameraState::m_CustomOverflow
	List_1_t708436666 * ___m_CustomOverflow_14;
	// System.Int32 Cinemachine.CameraState::<NumCustomBlendables>k__BackingField
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CLensU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CLensU3Ek__BackingField_0)); }
	inline LensSettings_t822254920  get_U3CLensU3Ek__BackingField_0() const { return ___U3CLensU3Ek__BackingField_0; }
	inline LensSettings_t822254920 * get_address_of_U3CLensU3Ek__BackingField_0() { return &___U3CLensU3Ek__BackingField_0; }
	inline void set_U3CLensU3Ek__BackingField_0(LensSettings_t822254920  value)
	{
		___U3CLensU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceUpU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CReferenceUpU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CReferenceUpU3Ek__BackingField_1() const { return ___U3CReferenceUpU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CReferenceUpU3Ek__BackingField_1() { return &___U3CReferenceUpU3Ek__BackingField_1; }
	inline void set_U3CReferenceUpU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CReferenceUpU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CReferenceLookAtU3Ek__BackingField_2)); }
	inline Vector3_t3722313464  get_U3CReferenceLookAtU3Ek__BackingField_2() const { return ___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline Vector3_t3722313464 * get_address_of_U3CReferenceLookAtU3Ek__BackingField_2() { return &___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline void set_U3CReferenceLookAtU3Ek__BackingField_2(Vector3_t3722313464  value)
	{
		___U3CReferenceLookAtU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRawPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CRawPositionU3Ek__BackingField_4)); }
	inline Vector3_t3722313464  get_U3CRawPositionU3Ek__BackingField_4() const { return ___U3CRawPositionU3Ek__BackingField_4; }
	inline Vector3_t3722313464 * get_address_of_U3CRawPositionU3Ek__BackingField_4() { return &___U3CRawPositionU3Ek__BackingField_4; }
	inline void set_U3CRawPositionU3Ek__BackingField_4(Vector3_t3722313464  value)
	{
		___U3CRawPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRawOrientationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CRawOrientationU3Ek__BackingField_5)); }
	inline Quaternion_t2301928331  get_U3CRawOrientationU3Ek__BackingField_5() const { return ___U3CRawOrientationU3Ek__BackingField_5; }
	inline Quaternion_t2301928331 * get_address_of_U3CRawOrientationU3Ek__BackingField_5() { return &___U3CRawOrientationU3Ek__BackingField_5; }
	inline void set_U3CRawOrientationU3Ek__BackingField_5(Quaternion_t2301928331  value)
	{
		___U3CRawOrientationU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPositionDampingBypassU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CPositionDampingBypassU3Ek__BackingField_6)); }
	inline Vector3_t3722313464  get_U3CPositionDampingBypassU3Ek__BackingField_6() const { return ___U3CPositionDampingBypassU3Ek__BackingField_6; }
	inline Vector3_t3722313464 * get_address_of_U3CPositionDampingBypassU3Ek__BackingField_6() { return &___U3CPositionDampingBypassU3Ek__BackingField_6; }
	inline void set_U3CPositionDampingBypassU3Ek__BackingField_6(Vector3_t3722313464  value)
	{
		___U3CPositionDampingBypassU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CShotQualityU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CShotQualityU3Ek__BackingField_7)); }
	inline float get_U3CShotQualityU3Ek__BackingField_7() const { return ___U3CShotQualityU3Ek__BackingField_7; }
	inline float* get_address_of_U3CShotQualityU3Ek__BackingField_7() { return &___U3CShotQualityU3Ek__BackingField_7; }
	inline void set_U3CShotQualityU3Ek__BackingField_7(float value)
	{
		___U3CShotQualityU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CPositionCorrectionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CPositionCorrectionU3Ek__BackingField_8)); }
	inline Vector3_t3722313464  get_U3CPositionCorrectionU3Ek__BackingField_8() const { return ___U3CPositionCorrectionU3Ek__BackingField_8; }
	inline Vector3_t3722313464 * get_address_of_U3CPositionCorrectionU3Ek__BackingField_8() { return &___U3CPositionCorrectionU3Ek__BackingField_8; }
	inline void set_U3CPositionCorrectionU3Ek__BackingField_8(Vector3_t3722313464  value)
	{
		___U3CPositionCorrectionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3COrientationCorrectionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3COrientationCorrectionU3Ek__BackingField_9)); }
	inline Quaternion_t2301928331  get_U3COrientationCorrectionU3Ek__BackingField_9() const { return ___U3COrientationCorrectionU3Ek__BackingField_9; }
	inline Quaternion_t2301928331 * get_address_of_U3COrientationCorrectionU3Ek__BackingField_9() { return &___U3COrientationCorrectionU3Ek__BackingField_9; }
	inline void set_U3COrientationCorrectionU3Ek__BackingField_9(Quaternion_t2301928331  value)
	{
		___U3COrientationCorrectionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_mCustom0_10() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___mCustom0_10)); }
	inline CustomBlendable_t3531329220  get_mCustom0_10() const { return ___mCustom0_10; }
	inline CustomBlendable_t3531329220 * get_address_of_mCustom0_10() { return &___mCustom0_10; }
	inline void set_mCustom0_10(CustomBlendable_t3531329220  value)
	{
		___mCustom0_10 = value;
	}

	inline static int32_t get_offset_of_mCustom1_11() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___mCustom1_11)); }
	inline CustomBlendable_t3531329220  get_mCustom1_11() const { return ___mCustom1_11; }
	inline CustomBlendable_t3531329220 * get_address_of_mCustom1_11() { return &___mCustom1_11; }
	inline void set_mCustom1_11(CustomBlendable_t3531329220  value)
	{
		___mCustom1_11 = value;
	}

	inline static int32_t get_offset_of_mCustom2_12() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___mCustom2_12)); }
	inline CustomBlendable_t3531329220  get_mCustom2_12() const { return ___mCustom2_12; }
	inline CustomBlendable_t3531329220 * get_address_of_mCustom2_12() { return &___mCustom2_12; }
	inline void set_mCustom2_12(CustomBlendable_t3531329220  value)
	{
		___mCustom2_12 = value;
	}

	inline static int32_t get_offset_of_mCustom3_13() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___mCustom3_13)); }
	inline CustomBlendable_t3531329220  get_mCustom3_13() const { return ___mCustom3_13; }
	inline CustomBlendable_t3531329220 * get_address_of_mCustom3_13() { return &___mCustom3_13; }
	inline void set_mCustom3_13(CustomBlendable_t3531329220  value)
	{
		___mCustom3_13 = value;
	}

	inline static int32_t get_offset_of_m_CustomOverflow_14() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___m_CustomOverflow_14)); }
	inline List_1_t708436666 * get_m_CustomOverflow_14() const { return ___m_CustomOverflow_14; }
	inline List_1_t708436666 ** get_address_of_m_CustomOverflow_14() { return &___m_CustomOverflow_14; }
	inline void set_m_CustomOverflow_14(List_1_t708436666 * value)
	{
		___m_CustomOverflow_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomOverflow_14), value);
	}

	inline static int32_t get_offset_of_U3CNumCustomBlendablesU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CameraState_t560734301, ___U3CNumCustomBlendablesU3Ek__BackingField_15)); }
	inline int32_t get_U3CNumCustomBlendablesU3Ek__BackingField_15() const { return ___U3CNumCustomBlendablesU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CNumCustomBlendablesU3Ek__BackingField_15() { return &___U3CNumCustomBlendablesU3Ek__BackingField_15; }
	inline void set_U3CNumCustomBlendablesU3Ek__BackingField_15(int32_t value)
	{
		___U3CNumCustomBlendablesU3Ek__BackingField_15 = value;
	}
};

struct CameraState_t560734301_StaticFields
{
public:
	// UnityEngine.Vector3 Cinemachine.CameraState::kNoPoint
	Vector3_t3722313464  ___kNoPoint_3;

public:
	inline static int32_t get_offset_of_kNoPoint_3() { return static_cast<int32_t>(offsetof(CameraState_t560734301_StaticFields, ___kNoPoint_3)); }
	inline Vector3_t3722313464  get_kNoPoint_3() const { return ___kNoPoint_3; }
	inline Vector3_t3722313464 * get_address_of_kNoPoint_3() { return &___kNoPoint_3; }
	inline void set_kNoPoint_3(Vector3_t3722313464  value)
	{
		___kNoPoint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CameraState
struct CameraState_t560734301_marshaled_pinvoke
{
	LensSettings_t822254920_marshaled_pinvoke ___U3CLensU3Ek__BackingField_0;
	Vector3_t3722313464  ___U3CReferenceUpU3Ek__BackingField_1;
	Vector3_t3722313464  ___U3CReferenceLookAtU3Ek__BackingField_2;
	Vector3_t3722313464  ___U3CRawPositionU3Ek__BackingField_4;
	Quaternion_t2301928331  ___U3CRawOrientationU3Ek__BackingField_5;
	Vector3_t3722313464  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	float ___U3CShotQualityU3Ek__BackingField_7;
	Vector3_t3722313464  ___U3CPositionCorrectionU3Ek__BackingField_8;
	Quaternion_t2301928331  ___U3COrientationCorrectionU3Ek__BackingField_9;
	CustomBlendable_t3531329220_marshaled_pinvoke ___mCustom0_10;
	CustomBlendable_t3531329220_marshaled_pinvoke ___mCustom1_11;
	CustomBlendable_t3531329220_marshaled_pinvoke ___mCustom2_12;
	CustomBlendable_t3531329220_marshaled_pinvoke ___mCustom3_13;
	List_1_t708436666 * ___m_CustomOverflow_14;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_15;
};
// Native definition for COM marshalling of Cinemachine.CameraState
struct CameraState_t560734301_marshaled_com
{
	LensSettings_t822254920_marshaled_com ___U3CLensU3Ek__BackingField_0;
	Vector3_t3722313464  ___U3CReferenceUpU3Ek__BackingField_1;
	Vector3_t3722313464  ___U3CReferenceLookAtU3Ek__BackingField_2;
	Vector3_t3722313464  ___U3CRawPositionU3Ek__BackingField_4;
	Quaternion_t2301928331  ___U3CRawOrientationU3Ek__BackingField_5;
	Vector3_t3722313464  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	float ___U3CShotQualityU3Ek__BackingField_7;
	Vector3_t3722313464  ___U3CPositionCorrectionU3Ek__BackingField_8;
	Quaternion_t2301928331  ___U3COrientationCorrectionU3Ek__BackingField_9;
	CustomBlendable_t3531329220_marshaled_com ___mCustom0_10;
	CustomBlendable_t3531329220_marshaled_com ___mCustom1_11;
	CustomBlendable_t3531329220_marshaled_com ___mCustom2_12;
	CustomBlendable_t3531329220_marshaled_com ___mCustom3_13;
	List_1_t708436666 * ___m_CustomOverflow_14;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_15;
};
#endif // CAMERASTATE_T560734301_H
#ifndef DESTROYPIPELINEDELEGATE_T29728977_H
#define DESTROYPIPELINEDELEGATE_T29728977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate
struct  DestroyPipelineDelegate_t29728977  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYPIPELINEDELEGATE_T29728977_H
#ifndef UPDATEHEADINGDELEGATE_T3270413085_H
#define UPDATEHEADINGDELEGATE_T3270413085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer/UpdateHeadingDelegate
struct  UpdateHeadingDelegate_t3270413085  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEHEADINGDELEGATE_T3270413085_H
#ifndef CUSTOMBLEND_T84561686_H
#define CUSTOMBLEND_T84561686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBlenderSettings/CustomBlend
struct  CustomBlend_t84561686 
{
public:
	// System.String Cinemachine.CinemachineBlenderSettings/CustomBlend::m_From
	String_t* ___m_From_0;
	// System.String Cinemachine.CinemachineBlenderSettings/CustomBlend::m_To
	String_t* ___m_To_1;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineBlenderSettings/CustomBlend::m_Blend
	CinemachineBlendDefinition_t3511030937  ___m_Blend_2;

public:
	inline static int32_t get_offset_of_m_From_0() { return static_cast<int32_t>(offsetof(CustomBlend_t84561686, ___m_From_0)); }
	inline String_t* get_m_From_0() const { return ___m_From_0; }
	inline String_t** get_address_of_m_From_0() { return &___m_From_0; }
	inline void set_m_From_0(String_t* value)
	{
		___m_From_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_From_0), value);
	}

	inline static int32_t get_offset_of_m_To_1() { return static_cast<int32_t>(offsetof(CustomBlend_t84561686, ___m_To_1)); }
	inline String_t* get_m_To_1() const { return ___m_To_1; }
	inline String_t** get_address_of_m_To_1() { return &___m_To_1; }
	inline void set_m_To_1(String_t* value)
	{
		___m_To_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_To_1), value);
	}

	inline static int32_t get_offset_of_m_Blend_2() { return static_cast<int32_t>(offsetof(CustomBlend_t84561686, ___m_Blend_2)); }
	inline CinemachineBlendDefinition_t3511030937  get_m_Blend_2() const { return ___m_Blend_2; }
	inline CinemachineBlendDefinition_t3511030937 * get_address_of_m_Blend_2() { return &___m_Blend_2; }
	inline void set_m_Blend_2(CinemachineBlendDefinition_t3511030937  value)
	{
		___m_Blend_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Cinemachine.CinemachineBlenderSettings/CustomBlend
struct CustomBlend_t84561686_marshaled_pinvoke
{
	char* ___m_From_0;
	char* ___m_To_1;
	CinemachineBlendDefinition_t3511030937  ___m_Blend_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineBlenderSettings/CustomBlend
struct CustomBlend_t84561686_marshaled_com
{
	Il2CppChar* ___m_From_0;
	Il2CppChar* ___m_To_1;
	CinemachineBlendDefinition_t3511030937  ___m_Blend_2;
};
#endif // CUSTOMBLEND_T84561686_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef PLAYABLEASSET_T3219022681_H
#define PLAYABLEASSET_T3219022681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t3219022681  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T3219022681_H
#ifndef CREATERIGDELEGATE_T3127201826_H
#define CREATERIGDELEGATE_T3127201826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook/CreateRigDelegate
struct  CreateRigDelegate_t3127201826  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATERIGDELEGATE_T3127201826_H
#ifndef DESTROYRIGDELEGATE_T2388233918_H
#define DESTROYRIGDELEGATE_T2388233918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook/DestroyRigDelegate
struct  DestroyRigDelegate_t2388233918  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYRIGDELEGATE_T2388233918_H
#ifndef AXISINPUTDELEGATE_T365085504_H
#define AXISINPUTDELEGATE_T365085504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineCore/AxisInputDelegate
struct  AxisInputDelegate_t365085504  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINPUTDELEGATE_T365085504_H
#ifndef NOISESETTINGS_T3019862488_H
#define NOISESETTINGS_T3019862488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.NoiseSettings
struct  NoiseSettings_t3019862488  : public ScriptableObject_t2528358522
{
public:
	// Cinemachine.NoiseSettings/TransformNoiseParams[] Cinemachine.NoiseSettings::m_Position
	TransformNoiseParamsU5BU5D_t2996701196* ___m_Position_2;
	// Cinemachine.NoiseSettings/TransformNoiseParams[] Cinemachine.NoiseSettings::m_Orientation
	TransformNoiseParamsU5BU5D_t2996701196* ___m_Orientation_3;

public:
	inline static int32_t get_offset_of_m_Position_2() { return static_cast<int32_t>(offsetof(NoiseSettings_t3019862488, ___m_Position_2)); }
	inline TransformNoiseParamsU5BU5D_t2996701196* get_m_Position_2() const { return ___m_Position_2; }
	inline TransformNoiseParamsU5BU5D_t2996701196** get_address_of_m_Position_2() { return &___m_Position_2; }
	inline void set_m_Position_2(TransformNoiseParamsU5BU5D_t2996701196* value)
	{
		___m_Position_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Position_2), value);
	}

	inline static int32_t get_offset_of_m_Orientation_3() { return static_cast<int32_t>(offsetof(NoiseSettings_t3019862488, ___m_Orientation_3)); }
	inline TransformNoiseParamsU5BU5D_t2996701196* get_m_Orientation_3() const { return ___m_Orientation_3; }
	inline TransformNoiseParamsU5BU5D_t2996701196** get_address_of_m_Orientation_3() { return &___m_Orientation_3; }
	inline void set_m_Orientation_3(TransformNoiseParamsU5BU5D_t2996701196* value)
	{
		___m_Orientation_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Orientation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISESETTINGS_T3019862488_H
#ifndef CREATEPIPELINEDELEGATE_T3891770455_H
#define CREATEPIPELINEDELEGATE_T3891770455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate
struct  CreatePipelineDelegate_t3891770455  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEPIPELINEDELEGATE_T3891770455_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CINEMACHINESHOT_T973457530_H
#define CINEMACHINESHOT_T973457530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineShot
struct  CinemachineShot_t973457530  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.ExposedReference`1<Cinemachine.CinemachineVirtualCameraBase> Cinemachine.Timeline.CinemachineShot::VirtualCamera
	ExposedReference_1_t1593012215  ___VirtualCamera_2;

public:
	inline static int32_t get_offset_of_VirtualCamera_2() { return static_cast<int32_t>(offsetof(CinemachineShot_t973457530, ___VirtualCamera_2)); }
	inline ExposedReference_1_t1593012215  get_VirtualCamera_2() const { return ___VirtualCamera_2; }
	inline ExposedReference_1_t1593012215 * get_address_of_VirtualCamera_2() { return &___VirtualCamera_2; }
	inline void set_VirtualCamera_2(ExposedReference_1_t1593012215  value)
	{
		___VirtualCamera_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESHOT_T973457530_H
#ifndef ONPOSTPIPELINESTAGEDELEGATE_T2437605738_H
#define ONPOSTPIPELINESTAGEDELEGATE_T2437605738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate
struct  OnPostPipelineStageDelegate_t2437605738  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPOSTPIPELINESTAGEDELEGATE_T2437605738_H
#ifndef CINEMACHINECOMPONENTBASE_T4122537527_H
#define CINEMACHINECOMPONENTBASE_T4122537527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineComponentBase
struct  CinemachineComponentBase_t4122537527  : public MonoBehaviour_t3962482529
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::m_vcamOwner
	CinemachineVirtualCameraBase_t1327958294 * ___m_vcamOwner_3;

public:
	inline static int32_t get_offset_of_m_vcamOwner_3() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_t4122537527, ___m_vcamOwner_3)); }
	inline CinemachineVirtualCameraBase_t1327958294 * get_m_vcamOwner_3() const { return ___m_vcamOwner_3; }
	inline CinemachineVirtualCameraBase_t1327958294 ** get_address_of_m_vcamOwner_3() { return &___m_vcamOwner_3; }
	inline void set_m_vcamOwner_3(CinemachineVirtualCameraBase_t1327958294 * value)
	{
		___m_vcamOwner_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_vcamOwner_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECOMPONENTBASE_T4122537527_H
#ifndef CINEMACHINETARGETGROUP_T1423332932_H
#define CINEMACHINETARGETGROUP_T1423332932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTargetGroup
struct  CinemachineTargetGroup_t1423332932  : public MonoBehaviour_t3962482529
{
public:
	// Cinemachine.CinemachineTargetGroup/PositionMode Cinemachine.CinemachineTargetGroup::m_PositionMode
	int32_t ___m_PositionMode_2;
	// Cinemachine.CinemachineTargetGroup/RotationMode Cinemachine.CinemachineTargetGroup::m_RotationMode
	int32_t ___m_RotationMode_3;
	// Cinemachine.CinemachineTargetGroup/UpdateMethod Cinemachine.CinemachineTargetGroup::m_UpdateMethod
	int32_t ___m_UpdateMethod_4;
	// Cinemachine.CinemachineTargetGroup/Target[] Cinemachine.CinemachineTargetGroup::m_Targets
	TargetU5BU5D_t3329193756* ___m_Targets_5;
	// System.Single Cinemachine.CinemachineTargetGroup::m_lastRadius
	float ___m_lastRadius_6;

public:
	inline static int32_t get_offset_of_m_PositionMode_2() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t1423332932, ___m_PositionMode_2)); }
	inline int32_t get_m_PositionMode_2() const { return ___m_PositionMode_2; }
	inline int32_t* get_address_of_m_PositionMode_2() { return &___m_PositionMode_2; }
	inline void set_m_PositionMode_2(int32_t value)
	{
		___m_PositionMode_2 = value;
	}

	inline static int32_t get_offset_of_m_RotationMode_3() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t1423332932, ___m_RotationMode_3)); }
	inline int32_t get_m_RotationMode_3() const { return ___m_RotationMode_3; }
	inline int32_t* get_address_of_m_RotationMode_3() { return &___m_RotationMode_3; }
	inline void set_m_RotationMode_3(int32_t value)
	{
		___m_RotationMode_3 = value;
	}

	inline static int32_t get_offset_of_m_UpdateMethod_4() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t1423332932, ___m_UpdateMethod_4)); }
	inline int32_t get_m_UpdateMethod_4() const { return ___m_UpdateMethod_4; }
	inline int32_t* get_address_of_m_UpdateMethod_4() { return &___m_UpdateMethod_4; }
	inline void set_m_UpdateMethod_4(int32_t value)
	{
		___m_UpdateMethod_4 = value;
	}

	inline static int32_t get_offset_of_m_Targets_5() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t1423332932, ___m_Targets_5)); }
	inline TargetU5BU5D_t3329193756* get_m_Targets_5() const { return ___m_Targets_5; }
	inline TargetU5BU5D_t3329193756** get_address_of_m_Targets_5() { return &___m_Targets_5; }
	inline void set_m_Targets_5(TargetU5BU5D_t3329193756* value)
	{
		___m_Targets_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Targets_5), value);
	}

	inline static int32_t get_offset_of_m_lastRadius_6() { return static_cast<int32_t>(offsetof(CinemachineTargetGroup_t1423332932, ___m_lastRadius_6)); }
	inline float get_m_lastRadius_6() const { return ___m_lastRadius_6; }
	inline float* get_address_of_m_lastRadius_6() { return &___m_lastRadius_6; }
	inline void set_m_lastRadius_6(float value)
	{
		___m_lastRadius_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETARGETGROUP_T1423332932_H
#ifndef CINEMACHINEPIPELINE_T1706075881_H
#define CINEMACHINEPIPELINE_T1706075881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePipeline
struct  CinemachinePipeline_t1706075881  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPIPELINE_T1706075881_H
#ifndef CINEMACHINEEXTENSION_T890367330_H
#define CINEMACHINEEXTENSION_T890367330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineExtension
struct  CinemachineExtension_t890367330  : public MonoBehaviour_t3962482529
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineExtension::m_vcamOwner
	CinemachineVirtualCameraBase_t1327958294 * ___m_vcamOwner_3;
	// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,System.Object> Cinemachine.CinemachineExtension::mExtraState
	Dictionary_2_t1893910920 * ___mExtraState_4;

public:
	inline static int32_t get_offset_of_m_vcamOwner_3() { return static_cast<int32_t>(offsetof(CinemachineExtension_t890367330, ___m_vcamOwner_3)); }
	inline CinemachineVirtualCameraBase_t1327958294 * get_m_vcamOwner_3() const { return ___m_vcamOwner_3; }
	inline CinemachineVirtualCameraBase_t1327958294 ** get_address_of_m_vcamOwner_3() { return &___m_vcamOwner_3; }
	inline void set_m_vcamOwner_3(CinemachineVirtualCameraBase_t1327958294 * value)
	{
		___m_vcamOwner_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_vcamOwner_3), value);
	}

	inline static int32_t get_offset_of_mExtraState_4() { return static_cast<int32_t>(offsetof(CinemachineExtension_t890367330, ___mExtraState_4)); }
	inline Dictionary_2_t1893910920 * get_mExtraState_4() const { return ___mExtraState_4; }
	inline Dictionary_2_t1893910920 ** get_address_of_mExtraState_4() { return &___mExtraState_4; }
	inline void set_mExtraState_4(Dictionary_2_t1893910920 * value)
	{
		___mExtraState_4 = value;
		Il2CppCodeGenWriteBarrier((&___mExtraState_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEEXTENSION_T890367330_H
#ifndef CINEMACHINEPATHBASE_T1750211330_H
#define CINEMACHINEPATHBASE_T1750211330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePathBase
struct  CinemachinePathBase_t1750211330  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Cinemachine.CinemachinePathBase::m_Resolution
	int32_t ___m_Resolution_2;
	// Cinemachine.CinemachinePathBase/Appearance Cinemachine.CinemachinePathBase::m_Appearance
	Appearance_t243588924 * ___m_Appearance_3;
	// System.Single[] Cinemachine.CinemachinePathBase::m_DistanceToPos
	SingleU5BU5D_t1444911251* ___m_DistanceToPos_4;
	// System.Single[] Cinemachine.CinemachinePathBase::m_PosToDistance
	SingleU5BU5D_t1444911251* ___m_PosToDistance_5;
	// System.Int32 Cinemachine.CinemachinePathBase::m_CachedSampleSteps
	int32_t ___m_CachedSampleSteps_6;
	// System.Single Cinemachine.CinemachinePathBase::m_PathLength
	float ___m_PathLength_7;
	// System.Single Cinemachine.CinemachinePathBase::m_cachedPosStepSize
	float ___m_cachedPosStepSize_8;
	// System.Single Cinemachine.CinemachinePathBase::m_cachedDistanceStepSize
	float ___m_cachedDistanceStepSize_9;

public:
	inline static int32_t get_offset_of_m_Resolution_2() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_Resolution_2)); }
	inline int32_t get_m_Resolution_2() const { return ___m_Resolution_2; }
	inline int32_t* get_address_of_m_Resolution_2() { return &___m_Resolution_2; }
	inline void set_m_Resolution_2(int32_t value)
	{
		___m_Resolution_2 = value;
	}

	inline static int32_t get_offset_of_m_Appearance_3() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_Appearance_3)); }
	inline Appearance_t243588924 * get_m_Appearance_3() const { return ___m_Appearance_3; }
	inline Appearance_t243588924 ** get_address_of_m_Appearance_3() { return &___m_Appearance_3; }
	inline void set_m_Appearance_3(Appearance_t243588924 * value)
	{
		___m_Appearance_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Appearance_3), value);
	}

	inline static int32_t get_offset_of_m_DistanceToPos_4() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_DistanceToPos_4)); }
	inline SingleU5BU5D_t1444911251* get_m_DistanceToPos_4() const { return ___m_DistanceToPos_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_DistanceToPos_4() { return &___m_DistanceToPos_4; }
	inline void set_m_DistanceToPos_4(SingleU5BU5D_t1444911251* value)
	{
		___m_DistanceToPos_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DistanceToPos_4), value);
	}

	inline static int32_t get_offset_of_m_PosToDistance_5() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_PosToDistance_5)); }
	inline SingleU5BU5D_t1444911251* get_m_PosToDistance_5() const { return ___m_PosToDistance_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_PosToDistance_5() { return &___m_PosToDistance_5; }
	inline void set_m_PosToDistance_5(SingleU5BU5D_t1444911251* value)
	{
		___m_PosToDistance_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PosToDistance_5), value);
	}

	inline static int32_t get_offset_of_m_CachedSampleSteps_6() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_CachedSampleSteps_6)); }
	inline int32_t get_m_CachedSampleSteps_6() const { return ___m_CachedSampleSteps_6; }
	inline int32_t* get_address_of_m_CachedSampleSteps_6() { return &___m_CachedSampleSteps_6; }
	inline void set_m_CachedSampleSteps_6(int32_t value)
	{
		___m_CachedSampleSteps_6 = value;
	}

	inline static int32_t get_offset_of_m_PathLength_7() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_PathLength_7)); }
	inline float get_m_PathLength_7() const { return ___m_PathLength_7; }
	inline float* get_address_of_m_PathLength_7() { return &___m_PathLength_7; }
	inline void set_m_PathLength_7(float value)
	{
		___m_PathLength_7 = value;
	}

	inline static int32_t get_offset_of_m_cachedPosStepSize_8() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_cachedPosStepSize_8)); }
	inline float get_m_cachedPosStepSize_8() const { return ___m_cachedPosStepSize_8; }
	inline float* get_address_of_m_cachedPosStepSize_8() { return &___m_cachedPosStepSize_8; }
	inline void set_m_cachedPosStepSize_8(float value)
	{
		___m_cachedPosStepSize_8 = value;
	}

	inline static int32_t get_offset_of_m_cachedDistanceStepSize_9() { return static_cast<int32_t>(offsetof(CinemachinePathBase_t1750211330, ___m_cachedDistanceStepSize_9)); }
	inline float get_m_cachedDistanceStepSize_9() const { return ___m_cachedDistanceStepSize_9; }
	inline float* get_address_of_m_cachedDistanceStepSize_9() { return &___m_cachedDistanceStepSize_9; }
	inline void set_m_cachedDistanceStepSize_9(float value)
	{
		___m_cachedDistanceStepSize_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPATHBASE_T1750211330_H
#ifndef CINEMACHINEVIRTUALCAMERABASE_T1327958294_H
#define CINEMACHINEVIRTUALCAMERABASE_T1327958294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCameraBase
struct  CinemachineVirtualCameraBase_t1327958294  : public MonoBehaviour_t3962482529
{
public:
	// System.Action Cinemachine.CinemachineVirtualCameraBase::CinemachineGUIDebuggerCallback
	Action_t1264377477 * ___CinemachineGUIDebuggerCallback_2;
	// System.String[] Cinemachine.CinemachineVirtualCameraBase::m_ExcludedPropertiesInInspector
	StringU5BU5D_t1281789340* ___m_ExcludedPropertiesInInspector_3;
	// Cinemachine.CinemachineCore/Stage[] Cinemachine.CinemachineVirtualCameraBase::m_LockStageInInspector
	StageU5BU5D_t2719076562* ___m_LockStageInInspector_4;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_ValidatingStreamVersion
	int32_t ___m_ValidatingStreamVersion_5;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_OnValidateCalled
	bool ___m_OnValidateCalled_6;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_StreamingVersion
	int32_t ___m_StreamingVersion_7;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_Priority
	int32_t ___m_Priority_8;
	// Cinemachine.CinemachineVirtualCameraBase/OnPostPipelineStageDelegate Cinemachine.CinemachineVirtualCameraBase::OnPostPipelineStage
	OnPostPipelineStageDelegate_t2437605738 * ___OnPostPipelineStage_9;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_previousStateIsValid
	bool ___m_previousStateIsValid_10;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCameraBase::m_previousLookAtTarget
	Transform_t3600365921 * ___m_previousLookAtTarget_11;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCameraBase::m_previousFollowTarget
	Transform_t3600365921 * ___m_previousFollowTarget_12;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::mSlaveStatusUpdated
	bool ___mSlaveStatusUpdated_13;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCameraBase::m_parentVcam
	CinemachineVirtualCameraBase_t1327958294 * ___m_parentVcam_14;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_QueuePriority
	int32_t ___m_QueuePriority_15;

public:
	inline static int32_t get_offset_of_CinemachineGUIDebuggerCallback_2() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___CinemachineGUIDebuggerCallback_2)); }
	inline Action_t1264377477 * get_CinemachineGUIDebuggerCallback_2() const { return ___CinemachineGUIDebuggerCallback_2; }
	inline Action_t1264377477 ** get_address_of_CinemachineGUIDebuggerCallback_2() { return &___CinemachineGUIDebuggerCallback_2; }
	inline void set_CinemachineGUIDebuggerCallback_2(Action_t1264377477 * value)
	{
		___CinemachineGUIDebuggerCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___CinemachineGUIDebuggerCallback_2), value);
	}

	inline static int32_t get_offset_of_m_ExcludedPropertiesInInspector_3() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_ExcludedPropertiesInInspector_3)); }
	inline StringU5BU5D_t1281789340* get_m_ExcludedPropertiesInInspector_3() const { return ___m_ExcludedPropertiesInInspector_3; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ExcludedPropertiesInInspector_3() { return &___m_ExcludedPropertiesInInspector_3; }
	inline void set_m_ExcludedPropertiesInInspector_3(StringU5BU5D_t1281789340* value)
	{
		___m_ExcludedPropertiesInInspector_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExcludedPropertiesInInspector_3), value);
	}

	inline static int32_t get_offset_of_m_LockStageInInspector_4() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_LockStageInInspector_4)); }
	inline StageU5BU5D_t2719076562* get_m_LockStageInInspector_4() const { return ___m_LockStageInInspector_4; }
	inline StageU5BU5D_t2719076562** get_address_of_m_LockStageInInspector_4() { return &___m_LockStageInInspector_4; }
	inline void set_m_LockStageInInspector_4(StageU5BU5D_t2719076562* value)
	{
		___m_LockStageInInspector_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LockStageInInspector_4), value);
	}

	inline static int32_t get_offset_of_m_ValidatingStreamVersion_5() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_ValidatingStreamVersion_5)); }
	inline int32_t get_m_ValidatingStreamVersion_5() const { return ___m_ValidatingStreamVersion_5; }
	inline int32_t* get_address_of_m_ValidatingStreamVersion_5() { return &___m_ValidatingStreamVersion_5; }
	inline void set_m_ValidatingStreamVersion_5(int32_t value)
	{
		___m_ValidatingStreamVersion_5 = value;
	}

	inline static int32_t get_offset_of_m_OnValidateCalled_6() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_OnValidateCalled_6)); }
	inline bool get_m_OnValidateCalled_6() const { return ___m_OnValidateCalled_6; }
	inline bool* get_address_of_m_OnValidateCalled_6() { return &___m_OnValidateCalled_6; }
	inline void set_m_OnValidateCalled_6(bool value)
	{
		___m_OnValidateCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_StreamingVersion_7() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_StreamingVersion_7)); }
	inline int32_t get_m_StreamingVersion_7() const { return ___m_StreamingVersion_7; }
	inline int32_t* get_address_of_m_StreamingVersion_7() { return &___m_StreamingVersion_7; }
	inline void set_m_StreamingVersion_7(int32_t value)
	{
		___m_StreamingVersion_7 = value;
	}

	inline static int32_t get_offset_of_m_Priority_8() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_Priority_8)); }
	inline int32_t get_m_Priority_8() const { return ___m_Priority_8; }
	inline int32_t* get_address_of_m_Priority_8() { return &___m_Priority_8; }
	inline void set_m_Priority_8(int32_t value)
	{
		___m_Priority_8 = value;
	}

	inline static int32_t get_offset_of_OnPostPipelineStage_9() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___OnPostPipelineStage_9)); }
	inline OnPostPipelineStageDelegate_t2437605738 * get_OnPostPipelineStage_9() const { return ___OnPostPipelineStage_9; }
	inline OnPostPipelineStageDelegate_t2437605738 ** get_address_of_OnPostPipelineStage_9() { return &___OnPostPipelineStage_9; }
	inline void set_OnPostPipelineStage_9(OnPostPipelineStageDelegate_t2437605738 * value)
	{
		___OnPostPipelineStage_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostPipelineStage_9), value);
	}

	inline static int32_t get_offset_of_m_previousStateIsValid_10() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_previousStateIsValid_10)); }
	inline bool get_m_previousStateIsValid_10() const { return ___m_previousStateIsValid_10; }
	inline bool* get_address_of_m_previousStateIsValid_10() { return &___m_previousStateIsValid_10; }
	inline void set_m_previousStateIsValid_10(bool value)
	{
		___m_previousStateIsValid_10 = value;
	}

	inline static int32_t get_offset_of_m_previousLookAtTarget_11() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_previousLookAtTarget_11)); }
	inline Transform_t3600365921 * get_m_previousLookAtTarget_11() const { return ___m_previousLookAtTarget_11; }
	inline Transform_t3600365921 ** get_address_of_m_previousLookAtTarget_11() { return &___m_previousLookAtTarget_11; }
	inline void set_m_previousLookAtTarget_11(Transform_t3600365921 * value)
	{
		___m_previousLookAtTarget_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_previousLookAtTarget_11), value);
	}

	inline static int32_t get_offset_of_m_previousFollowTarget_12() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_previousFollowTarget_12)); }
	inline Transform_t3600365921 * get_m_previousFollowTarget_12() const { return ___m_previousFollowTarget_12; }
	inline Transform_t3600365921 ** get_address_of_m_previousFollowTarget_12() { return &___m_previousFollowTarget_12; }
	inline void set_m_previousFollowTarget_12(Transform_t3600365921 * value)
	{
		___m_previousFollowTarget_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_previousFollowTarget_12), value);
	}

	inline static int32_t get_offset_of_mSlaveStatusUpdated_13() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___mSlaveStatusUpdated_13)); }
	inline bool get_mSlaveStatusUpdated_13() const { return ___mSlaveStatusUpdated_13; }
	inline bool* get_address_of_mSlaveStatusUpdated_13() { return &___mSlaveStatusUpdated_13; }
	inline void set_mSlaveStatusUpdated_13(bool value)
	{
		___mSlaveStatusUpdated_13 = value;
	}

	inline static int32_t get_offset_of_m_parentVcam_14() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_parentVcam_14)); }
	inline CinemachineVirtualCameraBase_t1327958294 * get_m_parentVcam_14() const { return ___m_parentVcam_14; }
	inline CinemachineVirtualCameraBase_t1327958294 ** get_address_of_m_parentVcam_14() { return &___m_parentVcam_14; }
	inline void set_m_parentVcam_14(CinemachineVirtualCameraBase_t1327958294 * value)
	{
		___m_parentVcam_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_parentVcam_14), value);
	}

	inline static int32_t get_offset_of_m_QueuePriority_15() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t1327958294, ___m_QueuePriority_15)); }
	inline int32_t get_m_QueuePriority_15() const { return ___m_QueuePriority_15; }
	inline int32_t* get_address_of_m_QueuePriority_15() { return &___m_QueuePriority_15; }
	inline void set_m_QueuePriority_15(int32_t value)
	{
		___m_QueuePriority_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEVIRTUALCAMERABASE_T1327958294_H
#ifndef CINEMACHINEDOLLYCART_T1139943543_H
#define CINEMACHINEDOLLYCART_T1139943543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineDollyCart
struct  CinemachineDollyCart_t1139943543  : public MonoBehaviour_t3962482529
{
public:
	// Cinemachine.CinemachinePathBase Cinemachine.CinemachineDollyCart::m_Path
	CinemachinePathBase_t1750211330 * ___m_Path_2;
	// Cinemachine.CinemachineDollyCart/UpdateMethod Cinemachine.CinemachineDollyCart::m_UpdateMethod
	int32_t ___m_UpdateMethod_3;
	// Cinemachine.CinemachinePathBase/PositionUnits Cinemachine.CinemachineDollyCart::m_PositionUnits
	int32_t ___m_PositionUnits_4;
	// System.Single Cinemachine.CinemachineDollyCart::m_Speed
	float ___m_Speed_5;
	// System.Single Cinemachine.CinemachineDollyCart::m_Position
	float ___m_Position_6;

public:
	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(CinemachineDollyCart_t1139943543, ___m_Path_2)); }
	inline CinemachinePathBase_t1750211330 * get_m_Path_2() const { return ___m_Path_2; }
	inline CinemachinePathBase_t1750211330 ** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(CinemachinePathBase_t1750211330 * value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_2), value);
	}

	inline static int32_t get_offset_of_m_UpdateMethod_3() { return static_cast<int32_t>(offsetof(CinemachineDollyCart_t1139943543, ___m_UpdateMethod_3)); }
	inline int32_t get_m_UpdateMethod_3() const { return ___m_UpdateMethod_3; }
	inline int32_t* get_address_of_m_UpdateMethod_3() { return &___m_UpdateMethod_3; }
	inline void set_m_UpdateMethod_3(int32_t value)
	{
		___m_UpdateMethod_3 = value;
	}

	inline static int32_t get_offset_of_m_PositionUnits_4() { return static_cast<int32_t>(offsetof(CinemachineDollyCart_t1139943543, ___m_PositionUnits_4)); }
	inline int32_t get_m_PositionUnits_4() const { return ___m_PositionUnits_4; }
	inline int32_t* get_address_of_m_PositionUnits_4() { return &___m_PositionUnits_4; }
	inline void set_m_PositionUnits_4(int32_t value)
	{
		___m_PositionUnits_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(CinemachineDollyCart_t1139943543, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_Position_6() { return static_cast<int32_t>(offsetof(CinemachineDollyCart_t1139943543, ___m_Position_6)); }
	inline float get_m_Position_6() const { return ___m_Position_6; }
	inline float* get_address_of_m_Position_6() { return &___m_Position_6; }
	inline void set_m_Position_6(float value)
	{
		___m_Position_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEDOLLYCART_T1139943543_H
#ifndef CINEMACHINECOMPOSER_T905041075_H
#define CINEMACHINECOMPOSER_T905041075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineComposer
struct  CinemachineComposer_t905041075  : public CinemachineComponentBase_t4122537527
{
public:
	// System.Action Cinemachine.CinemachineComposer::OnGUICallback
	Action_t1264377477 * ___OnGUICallback_4;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_TrackedObjectOffset
	Vector3_t3722313464  ___m_TrackedObjectOffset_5;
	// System.Single Cinemachine.CinemachineComposer::m_LookaheadTime
	float ___m_LookaheadTime_6;
	// System.Single Cinemachine.CinemachineComposer::m_LookaheadSmoothing
	float ___m_LookaheadSmoothing_7;
	// System.Single Cinemachine.CinemachineComposer::m_HorizontalDamping
	float ___m_HorizontalDamping_8;
	// System.Single Cinemachine.CinemachineComposer::m_VerticalDamping
	float ___m_VerticalDamping_9;
	// System.Single Cinemachine.CinemachineComposer::m_ScreenX
	float ___m_ScreenX_10;
	// System.Single Cinemachine.CinemachineComposer::m_ScreenY
	float ___m_ScreenY_11;
	// System.Single Cinemachine.CinemachineComposer::m_DeadZoneWidth
	float ___m_DeadZoneWidth_12;
	// System.Single Cinemachine.CinemachineComposer::m_DeadZoneHeight
	float ___m_DeadZoneHeight_13;
	// System.Single Cinemachine.CinemachineComposer::m_SoftZoneWidth
	float ___m_SoftZoneWidth_14;
	// System.Single Cinemachine.CinemachineComposer::m_SoftZoneHeight
	float ___m_SoftZoneHeight_15;
	// System.Single Cinemachine.CinemachineComposer::m_BiasX
	float ___m_BiasX_16;
	// System.Single Cinemachine.CinemachineComposer::m_BiasY
	float ___m_BiasY_17;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::<TrackedPoint>k__BackingField
	Vector3_t3722313464  ___U3CTrackedPointU3Ek__BackingField_18;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_CameraPosPrevFrame
	Vector3_t3722313464  ___m_CameraPosPrevFrame_19;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_LookAtPrevFrame
	Vector3_t3722313464  ___m_LookAtPrevFrame_20;
	// UnityEngine.Vector2 Cinemachine.CinemachineComposer::m_ScreenOffsetPrevFrame
	Vector2_t2156229523  ___m_ScreenOffsetPrevFrame_21;
	// UnityEngine.Quaternion Cinemachine.CinemachineComposer::m_CameraOrientationPrevFrame
	Quaternion_t2301928331  ___m_CameraOrientationPrevFrame_22;
	// Cinemachine.Utility.PositionPredictor Cinemachine.CinemachineComposer::m_Predictor
	PositionPredictor_t974033707 * ___m_Predictor_23;

public:
	inline static int32_t get_offset_of_OnGUICallback_4() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___OnGUICallback_4)); }
	inline Action_t1264377477 * get_OnGUICallback_4() const { return ___OnGUICallback_4; }
	inline Action_t1264377477 ** get_address_of_OnGUICallback_4() { return &___OnGUICallback_4; }
	inline void set_OnGUICallback_4(Action_t1264377477 * value)
	{
		___OnGUICallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnGUICallback_4), value);
	}

	inline static int32_t get_offset_of_m_TrackedObjectOffset_5() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_TrackedObjectOffset_5)); }
	inline Vector3_t3722313464  get_m_TrackedObjectOffset_5() const { return ___m_TrackedObjectOffset_5; }
	inline Vector3_t3722313464 * get_address_of_m_TrackedObjectOffset_5() { return &___m_TrackedObjectOffset_5; }
	inline void set_m_TrackedObjectOffset_5(Vector3_t3722313464  value)
	{
		___m_TrackedObjectOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadTime_6() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_LookaheadTime_6)); }
	inline float get_m_LookaheadTime_6() const { return ___m_LookaheadTime_6; }
	inline float* get_address_of_m_LookaheadTime_6() { return &___m_LookaheadTime_6; }
	inline void set_m_LookaheadTime_6(float value)
	{
		___m_LookaheadTime_6 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadSmoothing_7() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_LookaheadSmoothing_7)); }
	inline float get_m_LookaheadSmoothing_7() const { return ___m_LookaheadSmoothing_7; }
	inline float* get_address_of_m_LookaheadSmoothing_7() { return &___m_LookaheadSmoothing_7; }
	inline void set_m_LookaheadSmoothing_7(float value)
	{
		___m_LookaheadSmoothing_7 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalDamping_8() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_HorizontalDamping_8)); }
	inline float get_m_HorizontalDamping_8() const { return ___m_HorizontalDamping_8; }
	inline float* get_address_of_m_HorizontalDamping_8() { return &___m_HorizontalDamping_8; }
	inline void set_m_HorizontalDamping_8(float value)
	{
		___m_HorizontalDamping_8 = value;
	}

	inline static int32_t get_offset_of_m_VerticalDamping_9() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_VerticalDamping_9)); }
	inline float get_m_VerticalDamping_9() const { return ___m_VerticalDamping_9; }
	inline float* get_address_of_m_VerticalDamping_9() { return &___m_VerticalDamping_9; }
	inline void set_m_VerticalDamping_9(float value)
	{
		___m_VerticalDamping_9 = value;
	}

	inline static int32_t get_offset_of_m_ScreenX_10() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_ScreenX_10)); }
	inline float get_m_ScreenX_10() const { return ___m_ScreenX_10; }
	inline float* get_address_of_m_ScreenX_10() { return &___m_ScreenX_10; }
	inline void set_m_ScreenX_10(float value)
	{
		___m_ScreenX_10 = value;
	}

	inline static int32_t get_offset_of_m_ScreenY_11() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_ScreenY_11)); }
	inline float get_m_ScreenY_11() const { return ___m_ScreenY_11; }
	inline float* get_address_of_m_ScreenY_11() { return &___m_ScreenY_11; }
	inline void set_m_ScreenY_11(float value)
	{
		___m_ScreenY_11 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneWidth_12() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_DeadZoneWidth_12)); }
	inline float get_m_DeadZoneWidth_12() const { return ___m_DeadZoneWidth_12; }
	inline float* get_address_of_m_DeadZoneWidth_12() { return &___m_DeadZoneWidth_12; }
	inline void set_m_DeadZoneWidth_12(float value)
	{
		___m_DeadZoneWidth_12 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneHeight_13() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_DeadZoneHeight_13)); }
	inline float get_m_DeadZoneHeight_13() const { return ___m_DeadZoneHeight_13; }
	inline float* get_address_of_m_DeadZoneHeight_13() { return &___m_DeadZoneHeight_13; }
	inline void set_m_DeadZoneHeight_13(float value)
	{
		___m_DeadZoneHeight_13 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneWidth_14() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_SoftZoneWidth_14)); }
	inline float get_m_SoftZoneWidth_14() const { return ___m_SoftZoneWidth_14; }
	inline float* get_address_of_m_SoftZoneWidth_14() { return &___m_SoftZoneWidth_14; }
	inline void set_m_SoftZoneWidth_14(float value)
	{
		___m_SoftZoneWidth_14 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneHeight_15() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_SoftZoneHeight_15)); }
	inline float get_m_SoftZoneHeight_15() const { return ___m_SoftZoneHeight_15; }
	inline float* get_address_of_m_SoftZoneHeight_15() { return &___m_SoftZoneHeight_15; }
	inline void set_m_SoftZoneHeight_15(float value)
	{
		___m_SoftZoneHeight_15 = value;
	}

	inline static int32_t get_offset_of_m_BiasX_16() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_BiasX_16)); }
	inline float get_m_BiasX_16() const { return ___m_BiasX_16; }
	inline float* get_address_of_m_BiasX_16() { return &___m_BiasX_16; }
	inline void set_m_BiasX_16(float value)
	{
		___m_BiasX_16 = value;
	}

	inline static int32_t get_offset_of_m_BiasY_17() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_BiasY_17)); }
	inline float get_m_BiasY_17() const { return ___m_BiasY_17; }
	inline float* get_address_of_m_BiasY_17() { return &___m_BiasY_17; }
	inline void set_m_BiasY_17(float value)
	{
		___m_BiasY_17 = value;
	}

	inline static int32_t get_offset_of_U3CTrackedPointU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___U3CTrackedPointU3Ek__BackingField_18)); }
	inline Vector3_t3722313464  get_U3CTrackedPointU3Ek__BackingField_18() const { return ___U3CTrackedPointU3Ek__BackingField_18; }
	inline Vector3_t3722313464 * get_address_of_U3CTrackedPointU3Ek__BackingField_18() { return &___U3CTrackedPointU3Ek__BackingField_18; }
	inline void set_U3CTrackedPointU3Ek__BackingField_18(Vector3_t3722313464  value)
	{
		___U3CTrackedPointU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CameraPosPrevFrame_19() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_CameraPosPrevFrame_19)); }
	inline Vector3_t3722313464  get_m_CameraPosPrevFrame_19() const { return ___m_CameraPosPrevFrame_19; }
	inline Vector3_t3722313464 * get_address_of_m_CameraPosPrevFrame_19() { return &___m_CameraPosPrevFrame_19; }
	inline void set_m_CameraPosPrevFrame_19(Vector3_t3722313464  value)
	{
		___m_CameraPosPrevFrame_19 = value;
	}

	inline static int32_t get_offset_of_m_LookAtPrevFrame_20() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_LookAtPrevFrame_20)); }
	inline Vector3_t3722313464  get_m_LookAtPrevFrame_20() const { return ___m_LookAtPrevFrame_20; }
	inline Vector3_t3722313464 * get_address_of_m_LookAtPrevFrame_20() { return &___m_LookAtPrevFrame_20; }
	inline void set_m_LookAtPrevFrame_20(Vector3_t3722313464  value)
	{
		___m_LookAtPrevFrame_20 = value;
	}

	inline static int32_t get_offset_of_m_ScreenOffsetPrevFrame_21() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_ScreenOffsetPrevFrame_21)); }
	inline Vector2_t2156229523  get_m_ScreenOffsetPrevFrame_21() const { return ___m_ScreenOffsetPrevFrame_21; }
	inline Vector2_t2156229523 * get_address_of_m_ScreenOffsetPrevFrame_21() { return &___m_ScreenOffsetPrevFrame_21; }
	inline void set_m_ScreenOffsetPrevFrame_21(Vector2_t2156229523  value)
	{
		___m_ScreenOffsetPrevFrame_21 = value;
	}

	inline static int32_t get_offset_of_m_CameraOrientationPrevFrame_22() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_CameraOrientationPrevFrame_22)); }
	inline Quaternion_t2301928331  get_m_CameraOrientationPrevFrame_22() const { return ___m_CameraOrientationPrevFrame_22; }
	inline Quaternion_t2301928331 * get_address_of_m_CameraOrientationPrevFrame_22() { return &___m_CameraOrientationPrevFrame_22; }
	inline void set_m_CameraOrientationPrevFrame_22(Quaternion_t2301928331  value)
	{
		___m_CameraOrientationPrevFrame_22 = value;
	}

	inline static int32_t get_offset_of_m_Predictor_23() { return static_cast<int32_t>(offsetof(CinemachineComposer_t905041075, ___m_Predictor_23)); }
	inline PositionPredictor_t974033707 * get_m_Predictor_23() const { return ___m_Predictor_23; }
	inline PositionPredictor_t974033707 ** get_address_of_m_Predictor_23() { return &___m_Predictor_23; }
	inline void set_m_Predictor_23(PositionPredictor_t974033707 * value)
	{
		___m_Predictor_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Predictor_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECOMPOSER_T905041075_H
#ifndef CINEMACHINEMIXINGCAMERA_T2169870503_H
#define CINEMACHINEMIXINGCAMERA_T2169870503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineMixingCamera
struct  CinemachineMixingCamera_t2169870503  : public CinemachineVirtualCameraBase_t1327958294
{
public:
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight0
	float ___m_Weight0_17;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight1
	float ___m_Weight1_18;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight2
	float ___m_Weight2_19;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight3
	float ___m_Weight3_20;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight4
	float ___m_Weight4_21;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight5
	float ___m_Weight5_22;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight6
	float ___m_Weight6_23;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight7
	float ___m_Weight7_24;
	// Cinemachine.CameraState Cinemachine.CinemachineMixingCamera::m_State
	CameraState_t560734301  ___m_State_25;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineMixingCamera::<LiveChild>k__BackingField
	RuntimeObject* ___U3CLiveChildU3Ek__BackingField_26;
	// UnityEngine.Transform Cinemachine.CinemachineMixingCamera::<LookAt>k__BackingField
	Transform_t3600365921 * ___U3CLookAtU3Ek__BackingField_27;
	// UnityEngine.Transform Cinemachine.CinemachineMixingCamera::<Follow>k__BackingField
	Transform_t3600365921 * ___U3CFollowU3Ek__BackingField_28;
	// Cinemachine.CinemachineVirtualCameraBase[] Cinemachine.CinemachineMixingCamera::m_ChildCameras
	CinemachineVirtualCameraBaseU5BU5D_t2980098515* ___m_ChildCameras_29;
	// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,System.Int32> Cinemachine.CinemachineMixingCamera::m_indexMap
	Dictionary_2_t139543931 * ___m_indexMap_30;

public:
	inline static int32_t get_offset_of_m_Weight0_17() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight0_17)); }
	inline float get_m_Weight0_17() const { return ___m_Weight0_17; }
	inline float* get_address_of_m_Weight0_17() { return &___m_Weight0_17; }
	inline void set_m_Weight0_17(float value)
	{
		___m_Weight0_17 = value;
	}

	inline static int32_t get_offset_of_m_Weight1_18() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight1_18)); }
	inline float get_m_Weight1_18() const { return ___m_Weight1_18; }
	inline float* get_address_of_m_Weight1_18() { return &___m_Weight1_18; }
	inline void set_m_Weight1_18(float value)
	{
		___m_Weight1_18 = value;
	}

	inline static int32_t get_offset_of_m_Weight2_19() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight2_19)); }
	inline float get_m_Weight2_19() const { return ___m_Weight2_19; }
	inline float* get_address_of_m_Weight2_19() { return &___m_Weight2_19; }
	inline void set_m_Weight2_19(float value)
	{
		___m_Weight2_19 = value;
	}

	inline static int32_t get_offset_of_m_Weight3_20() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight3_20)); }
	inline float get_m_Weight3_20() const { return ___m_Weight3_20; }
	inline float* get_address_of_m_Weight3_20() { return &___m_Weight3_20; }
	inline void set_m_Weight3_20(float value)
	{
		___m_Weight3_20 = value;
	}

	inline static int32_t get_offset_of_m_Weight4_21() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight4_21)); }
	inline float get_m_Weight4_21() const { return ___m_Weight4_21; }
	inline float* get_address_of_m_Weight4_21() { return &___m_Weight4_21; }
	inline void set_m_Weight4_21(float value)
	{
		___m_Weight4_21 = value;
	}

	inline static int32_t get_offset_of_m_Weight5_22() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight5_22)); }
	inline float get_m_Weight5_22() const { return ___m_Weight5_22; }
	inline float* get_address_of_m_Weight5_22() { return &___m_Weight5_22; }
	inline void set_m_Weight5_22(float value)
	{
		___m_Weight5_22 = value;
	}

	inline static int32_t get_offset_of_m_Weight6_23() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight6_23)); }
	inline float get_m_Weight6_23() const { return ___m_Weight6_23; }
	inline float* get_address_of_m_Weight6_23() { return &___m_Weight6_23; }
	inline void set_m_Weight6_23(float value)
	{
		___m_Weight6_23 = value;
	}

	inline static int32_t get_offset_of_m_Weight7_24() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_Weight7_24)); }
	inline float get_m_Weight7_24() const { return ___m_Weight7_24; }
	inline float* get_address_of_m_Weight7_24() { return &___m_Weight7_24; }
	inline void set_m_Weight7_24(float value)
	{
		___m_Weight7_24 = value;
	}

	inline static int32_t get_offset_of_m_State_25() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_State_25)); }
	inline CameraState_t560734301  get_m_State_25() const { return ___m_State_25; }
	inline CameraState_t560734301 * get_address_of_m_State_25() { return &___m_State_25; }
	inline void set_m_State_25(CameraState_t560734301  value)
	{
		___m_State_25 = value;
	}

	inline static int32_t get_offset_of_U3CLiveChildU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___U3CLiveChildU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CLiveChildU3Ek__BackingField_26() const { return ___U3CLiveChildU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CLiveChildU3Ek__BackingField_26() { return &___U3CLiveChildU3Ek__BackingField_26; }
	inline void set_U3CLiveChildU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CLiveChildU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLiveChildU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CLookAtU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___U3CLookAtU3Ek__BackingField_27)); }
	inline Transform_t3600365921 * get_U3CLookAtU3Ek__BackingField_27() const { return ___U3CLookAtU3Ek__BackingField_27; }
	inline Transform_t3600365921 ** get_address_of_U3CLookAtU3Ek__BackingField_27() { return &___U3CLookAtU3Ek__BackingField_27; }
	inline void set_U3CLookAtU3Ek__BackingField_27(Transform_t3600365921 * value)
	{
		___U3CLookAtU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLookAtU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CFollowU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___U3CFollowU3Ek__BackingField_28)); }
	inline Transform_t3600365921 * get_U3CFollowU3Ek__BackingField_28() const { return ___U3CFollowU3Ek__BackingField_28; }
	inline Transform_t3600365921 ** get_address_of_U3CFollowU3Ek__BackingField_28() { return &___U3CFollowU3Ek__BackingField_28; }
	inline void set_U3CFollowU3Ek__BackingField_28(Transform_t3600365921 * value)
	{
		___U3CFollowU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFollowU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_m_ChildCameras_29() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_ChildCameras_29)); }
	inline CinemachineVirtualCameraBaseU5BU5D_t2980098515* get_m_ChildCameras_29() const { return ___m_ChildCameras_29; }
	inline CinemachineVirtualCameraBaseU5BU5D_t2980098515** get_address_of_m_ChildCameras_29() { return &___m_ChildCameras_29; }
	inline void set_m_ChildCameras_29(CinemachineVirtualCameraBaseU5BU5D_t2980098515* value)
	{
		___m_ChildCameras_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildCameras_29), value);
	}

	inline static int32_t get_offset_of_m_indexMap_30() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t2169870503, ___m_indexMap_30)); }
	inline Dictionary_2_t139543931 * get_m_indexMap_30() const { return ___m_indexMap_30; }
	inline Dictionary_2_t139543931 ** get_address_of_m_indexMap_30() { return &___m_indexMap_30; }
	inline void set_m_indexMap_30(Dictionary_2_t139543931 * value)
	{
		___m_indexMap_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_indexMap_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEMIXINGCAMERA_T2169870503_H
#ifndef CINEMACHINEHARDLOOKAT_T3788634768_H
#define CINEMACHINEHARDLOOKAT_T3788634768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineHardLookAt
struct  CinemachineHardLookAt_t3788634768  : public CinemachineComponentBase_t4122537527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEHARDLOOKAT_T3788634768_H
#ifndef CINEMACHINETRACKEDDOLLY_T847425305_H
#define CINEMACHINETRACKEDDOLLY_T847425305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTrackedDolly
struct  CinemachineTrackedDolly_t847425305  : public CinemachineComponentBase_t4122537527
{
public:
	// Cinemachine.CinemachinePathBase Cinemachine.CinemachineTrackedDolly::m_Path
	CinemachinePathBase_t1750211330 * ___m_Path_4;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_PathPosition
	float ___m_PathPosition_5;
	// Cinemachine.CinemachinePathBase/PositionUnits Cinemachine.CinemachineTrackedDolly::m_PositionUnits
	int32_t ___m_PositionUnits_6;
	// UnityEngine.Vector3 Cinemachine.CinemachineTrackedDolly::m_PathOffset
	Vector3_t3722313464  ___m_PathOffset_7;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_XDamping
	float ___m_XDamping_8;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_YDamping
	float ___m_YDamping_9;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_ZDamping
	float ___m_ZDamping_10;
	// Cinemachine.CinemachineTrackedDolly/CameraUpMode Cinemachine.CinemachineTrackedDolly::m_CameraUp
	int32_t ___m_CameraUp_11;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_PitchDamping
	float ___m_PitchDamping_12;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_YawDamping
	float ___m_YawDamping_13;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_RollDamping
	float ___m_RollDamping_14;
	// Cinemachine.CinemachineTrackedDolly/AutoDolly Cinemachine.CinemachineTrackedDolly::m_AutoDolly
	AutoDolly_t722089724  ___m_AutoDolly_15;
	// System.Single Cinemachine.CinemachineTrackedDolly::m_PreviousPathPosition
	float ___m_PreviousPathPosition_16;
	// UnityEngine.Quaternion Cinemachine.CinemachineTrackedDolly::m_PreviousOrientation
	Quaternion_t2301928331  ___m_PreviousOrientation_17;
	// UnityEngine.Vector3 Cinemachine.CinemachineTrackedDolly::m_PreviousCameraPosition
	Vector3_t3722313464  ___m_PreviousCameraPosition_18;

public:
	inline static int32_t get_offset_of_m_Path_4() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_Path_4)); }
	inline CinemachinePathBase_t1750211330 * get_m_Path_4() const { return ___m_Path_4; }
	inline CinemachinePathBase_t1750211330 ** get_address_of_m_Path_4() { return &___m_Path_4; }
	inline void set_m_Path_4(CinemachinePathBase_t1750211330 * value)
	{
		___m_Path_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_4), value);
	}

	inline static int32_t get_offset_of_m_PathPosition_5() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PathPosition_5)); }
	inline float get_m_PathPosition_5() const { return ___m_PathPosition_5; }
	inline float* get_address_of_m_PathPosition_5() { return &___m_PathPosition_5; }
	inline void set_m_PathPosition_5(float value)
	{
		___m_PathPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_PositionUnits_6() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PositionUnits_6)); }
	inline int32_t get_m_PositionUnits_6() const { return ___m_PositionUnits_6; }
	inline int32_t* get_address_of_m_PositionUnits_6() { return &___m_PositionUnits_6; }
	inline void set_m_PositionUnits_6(int32_t value)
	{
		___m_PositionUnits_6 = value;
	}

	inline static int32_t get_offset_of_m_PathOffset_7() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PathOffset_7)); }
	inline Vector3_t3722313464  get_m_PathOffset_7() const { return ___m_PathOffset_7; }
	inline Vector3_t3722313464 * get_address_of_m_PathOffset_7() { return &___m_PathOffset_7; }
	inline void set_m_PathOffset_7(Vector3_t3722313464  value)
	{
		___m_PathOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_8() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_XDamping_8)); }
	inline float get_m_XDamping_8() const { return ___m_XDamping_8; }
	inline float* get_address_of_m_XDamping_8() { return &___m_XDamping_8; }
	inline void set_m_XDamping_8(float value)
	{
		___m_XDamping_8 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_9() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_YDamping_9)); }
	inline float get_m_YDamping_9() const { return ___m_YDamping_9; }
	inline float* get_address_of_m_YDamping_9() { return &___m_YDamping_9; }
	inline void set_m_YDamping_9(float value)
	{
		___m_YDamping_9 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_10() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_ZDamping_10)); }
	inline float get_m_ZDamping_10() const { return ___m_ZDamping_10; }
	inline float* get_address_of_m_ZDamping_10() { return &___m_ZDamping_10; }
	inline void set_m_ZDamping_10(float value)
	{
		___m_ZDamping_10 = value;
	}

	inline static int32_t get_offset_of_m_CameraUp_11() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_CameraUp_11)); }
	inline int32_t get_m_CameraUp_11() const { return ___m_CameraUp_11; }
	inline int32_t* get_address_of_m_CameraUp_11() { return &___m_CameraUp_11; }
	inline void set_m_CameraUp_11(int32_t value)
	{
		___m_CameraUp_11 = value;
	}

	inline static int32_t get_offset_of_m_PitchDamping_12() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PitchDamping_12)); }
	inline float get_m_PitchDamping_12() const { return ___m_PitchDamping_12; }
	inline float* get_address_of_m_PitchDamping_12() { return &___m_PitchDamping_12; }
	inline void set_m_PitchDamping_12(float value)
	{
		___m_PitchDamping_12 = value;
	}

	inline static int32_t get_offset_of_m_YawDamping_13() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_YawDamping_13)); }
	inline float get_m_YawDamping_13() const { return ___m_YawDamping_13; }
	inline float* get_address_of_m_YawDamping_13() { return &___m_YawDamping_13; }
	inline void set_m_YawDamping_13(float value)
	{
		___m_YawDamping_13 = value;
	}

	inline static int32_t get_offset_of_m_RollDamping_14() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_RollDamping_14)); }
	inline float get_m_RollDamping_14() const { return ___m_RollDamping_14; }
	inline float* get_address_of_m_RollDamping_14() { return &___m_RollDamping_14; }
	inline void set_m_RollDamping_14(float value)
	{
		___m_RollDamping_14 = value;
	}

	inline static int32_t get_offset_of_m_AutoDolly_15() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_AutoDolly_15)); }
	inline AutoDolly_t722089724  get_m_AutoDolly_15() const { return ___m_AutoDolly_15; }
	inline AutoDolly_t722089724 * get_address_of_m_AutoDolly_15() { return &___m_AutoDolly_15; }
	inline void set_m_AutoDolly_15(AutoDolly_t722089724  value)
	{
		___m_AutoDolly_15 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPathPosition_16() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PreviousPathPosition_16)); }
	inline float get_m_PreviousPathPosition_16() const { return ___m_PreviousPathPosition_16; }
	inline float* get_address_of_m_PreviousPathPosition_16() { return &___m_PreviousPathPosition_16; }
	inline void set_m_PreviousPathPosition_16(float value)
	{
		___m_PreviousPathPosition_16 = value;
	}

	inline static int32_t get_offset_of_m_PreviousOrientation_17() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PreviousOrientation_17)); }
	inline Quaternion_t2301928331  get_m_PreviousOrientation_17() const { return ___m_PreviousOrientation_17; }
	inline Quaternion_t2301928331 * get_address_of_m_PreviousOrientation_17() { return &___m_PreviousOrientation_17; }
	inline void set_m_PreviousOrientation_17(Quaternion_t2301928331  value)
	{
		___m_PreviousOrientation_17 = value;
	}

	inline static int32_t get_offset_of_m_PreviousCameraPosition_18() { return static_cast<int32_t>(offsetof(CinemachineTrackedDolly_t847425305, ___m_PreviousCameraPosition_18)); }
	inline Vector3_t3722313464  get_m_PreviousCameraPosition_18() const { return ___m_PreviousCameraPosition_18; }
	inline Vector3_t3722313464 * get_address_of_m_PreviousCameraPosition_18() { return &___m_PreviousCameraPosition_18; }
	inline void set_m_PreviousCameraPosition_18(Vector3_t3722313464  value)
	{
		___m_PreviousCameraPosition_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETRACKEDDOLLY_T847425305_H
#ifndef CINEMACHINEFREELOOK_T3895466944_H
#define CINEMACHINEFREELOOK_T3895466944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFreeLook
struct  CinemachineFreeLook_t3895466944  : public CinemachineVirtualCameraBase_t1327958294
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineFreeLook::m_LookAt
	Transform_t3600365921 * ___m_LookAt_16;
	// UnityEngine.Transform Cinemachine.CinemachineFreeLook::m_Follow
	Transform_t3600365921 * ___m_Follow_17;
	// System.Boolean Cinemachine.CinemachineFreeLook::m_CommonLens
	bool ___m_CommonLens_18;
	// Cinemachine.LensSettings Cinemachine.CinemachineFreeLook::m_Lens
	LensSettings_t822254920  ___m_Lens_19;
	// Cinemachine.AxisState Cinemachine.CinemachineFreeLook::m_YAxis
	AxisState_t478218302  ___m_YAxis_20;
	// Cinemachine.AxisState Cinemachine.CinemachineFreeLook::m_XAxis
	AxisState_t478218302  ___m_XAxis_21;
	// Cinemachine.CinemachineOrbitalTransposer/Heading Cinemachine.CinemachineFreeLook::m_Heading
	Heading_t365180425  ___m_Heading_22;
	// Cinemachine.CinemachineOrbitalTransposer/Recentering Cinemachine.CinemachineFreeLook::m_RecenterToTargetHeading
	Recentering_t980373847  ___m_RecenterToTargetHeading_23;
	// Cinemachine.CinemachineTransposer/BindingMode Cinemachine.CinemachineFreeLook::m_BindingMode
	int32_t ___m_BindingMode_24;
	// System.Single Cinemachine.CinemachineFreeLook::m_SplineCurvature
	float ___m_SplineCurvature_25;
	// Cinemachine.CinemachineFreeLook/Orbit[] Cinemachine.CinemachineFreeLook::m_Orbits
	OrbitU5BU5D_t1434718521* ___m_Orbits_26;
	// System.Single Cinemachine.CinemachineFreeLook::m_LegacyHeadingBias
	float ___m_LegacyHeadingBias_27;
	// System.Boolean Cinemachine.CinemachineFreeLook::mUseLegacyRigDefinitions
	bool ___mUseLegacyRigDefinitions_28;
	// System.Boolean Cinemachine.CinemachineFreeLook::mIsDestroyed
	bool ___mIsDestroyed_29;
	// Cinemachine.CameraState Cinemachine.CinemachineFreeLook::m_State
	CameraState_t560734301  ___m_State_30;
	// Cinemachine.CinemachineVirtualCamera[] Cinemachine.CinemachineFreeLook::m_Rigs
	CinemachineVirtualCameraU5BU5D_t194901906* ___m_Rigs_31;
	// Cinemachine.CinemachineOrbitalTransposer[] Cinemachine.CinemachineFreeLook::mOrbitals
	CinemachineOrbitalTransposerU5BU5D_t4090051484* ___mOrbitals_32;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineFreeLook::mBlendA
	CinemachineBlend_t1772488801 * ___mBlendA_33;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineFreeLook::mBlendB
	CinemachineBlend_t1772488801 * ___mBlendB_34;
	// Cinemachine.CinemachineFreeLook/Orbit[] Cinemachine.CinemachineFreeLook::m_CachedOrbits
	OrbitU5BU5D_t1434718521* ___m_CachedOrbits_37;
	// System.Single Cinemachine.CinemachineFreeLook::m_CachedTension
	float ___m_CachedTension_38;
	// UnityEngine.Vector4[] Cinemachine.CinemachineFreeLook::m_CachedKnots
	Vector4U5BU5D_t934056436* ___m_CachedKnots_39;
	// UnityEngine.Vector4[] Cinemachine.CinemachineFreeLook::m_CachedCtrl1
	Vector4U5BU5D_t934056436* ___m_CachedCtrl1_40;
	// UnityEngine.Vector4[] Cinemachine.CinemachineFreeLook::m_CachedCtrl2
	Vector4U5BU5D_t934056436* ___m_CachedCtrl2_41;

public:
	inline static int32_t get_offset_of_m_LookAt_16() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_LookAt_16)); }
	inline Transform_t3600365921 * get_m_LookAt_16() const { return ___m_LookAt_16; }
	inline Transform_t3600365921 ** get_address_of_m_LookAt_16() { return &___m_LookAt_16; }
	inline void set_m_LookAt_16(Transform_t3600365921 * value)
	{
		___m_LookAt_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_16), value);
	}

	inline static int32_t get_offset_of_m_Follow_17() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_Follow_17)); }
	inline Transform_t3600365921 * get_m_Follow_17() const { return ___m_Follow_17; }
	inline Transform_t3600365921 ** get_address_of_m_Follow_17() { return &___m_Follow_17; }
	inline void set_m_Follow_17(Transform_t3600365921 * value)
	{
		___m_Follow_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_17), value);
	}

	inline static int32_t get_offset_of_m_CommonLens_18() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_CommonLens_18)); }
	inline bool get_m_CommonLens_18() const { return ___m_CommonLens_18; }
	inline bool* get_address_of_m_CommonLens_18() { return &___m_CommonLens_18; }
	inline void set_m_CommonLens_18(bool value)
	{
		___m_CommonLens_18 = value;
	}

	inline static int32_t get_offset_of_m_Lens_19() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_Lens_19)); }
	inline LensSettings_t822254920  get_m_Lens_19() const { return ___m_Lens_19; }
	inline LensSettings_t822254920 * get_address_of_m_Lens_19() { return &___m_Lens_19; }
	inline void set_m_Lens_19(LensSettings_t822254920  value)
	{
		___m_Lens_19 = value;
	}

	inline static int32_t get_offset_of_m_YAxis_20() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_YAxis_20)); }
	inline AxisState_t478218302  get_m_YAxis_20() const { return ___m_YAxis_20; }
	inline AxisState_t478218302 * get_address_of_m_YAxis_20() { return &___m_YAxis_20; }
	inline void set_m_YAxis_20(AxisState_t478218302  value)
	{
		___m_YAxis_20 = value;
	}

	inline static int32_t get_offset_of_m_XAxis_21() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_XAxis_21)); }
	inline AxisState_t478218302  get_m_XAxis_21() const { return ___m_XAxis_21; }
	inline AxisState_t478218302 * get_address_of_m_XAxis_21() { return &___m_XAxis_21; }
	inline void set_m_XAxis_21(AxisState_t478218302  value)
	{
		___m_XAxis_21 = value;
	}

	inline static int32_t get_offset_of_m_Heading_22() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_Heading_22)); }
	inline Heading_t365180425  get_m_Heading_22() const { return ___m_Heading_22; }
	inline Heading_t365180425 * get_address_of_m_Heading_22() { return &___m_Heading_22; }
	inline void set_m_Heading_22(Heading_t365180425  value)
	{
		___m_Heading_22 = value;
	}

	inline static int32_t get_offset_of_m_RecenterToTargetHeading_23() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_RecenterToTargetHeading_23)); }
	inline Recentering_t980373847  get_m_RecenterToTargetHeading_23() const { return ___m_RecenterToTargetHeading_23; }
	inline Recentering_t980373847 * get_address_of_m_RecenterToTargetHeading_23() { return &___m_RecenterToTargetHeading_23; }
	inline void set_m_RecenterToTargetHeading_23(Recentering_t980373847  value)
	{
		___m_RecenterToTargetHeading_23 = value;
	}

	inline static int32_t get_offset_of_m_BindingMode_24() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_BindingMode_24)); }
	inline int32_t get_m_BindingMode_24() const { return ___m_BindingMode_24; }
	inline int32_t* get_address_of_m_BindingMode_24() { return &___m_BindingMode_24; }
	inline void set_m_BindingMode_24(int32_t value)
	{
		___m_BindingMode_24 = value;
	}

	inline static int32_t get_offset_of_m_SplineCurvature_25() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_SplineCurvature_25)); }
	inline float get_m_SplineCurvature_25() const { return ___m_SplineCurvature_25; }
	inline float* get_address_of_m_SplineCurvature_25() { return &___m_SplineCurvature_25; }
	inline void set_m_SplineCurvature_25(float value)
	{
		___m_SplineCurvature_25 = value;
	}

	inline static int32_t get_offset_of_m_Orbits_26() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_Orbits_26)); }
	inline OrbitU5BU5D_t1434718521* get_m_Orbits_26() const { return ___m_Orbits_26; }
	inline OrbitU5BU5D_t1434718521** get_address_of_m_Orbits_26() { return &___m_Orbits_26; }
	inline void set_m_Orbits_26(OrbitU5BU5D_t1434718521* value)
	{
		___m_Orbits_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Orbits_26), value);
	}

	inline static int32_t get_offset_of_m_LegacyHeadingBias_27() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_LegacyHeadingBias_27)); }
	inline float get_m_LegacyHeadingBias_27() const { return ___m_LegacyHeadingBias_27; }
	inline float* get_address_of_m_LegacyHeadingBias_27() { return &___m_LegacyHeadingBias_27; }
	inline void set_m_LegacyHeadingBias_27(float value)
	{
		___m_LegacyHeadingBias_27 = value;
	}

	inline static int32_t get_offset_of_mUseLegacyRigDefinitions_28() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___mUseLegacyRigDefinitions_28)); }
	inline bool get_mUseLegacyRigDefinitions_28() const { return ___mUseLegacyRigDefinitions_28; }
	inline bool* get_address_of_mUseLegacyRigDefinitions_28() { return &___mUseLegacyRigDefinitions_28; }
	inline void set_mUseLegacyRigDefinitions_28(bool value)
	{
		___mUseLegacyRigDefinitions_28 = value;
	}

	inline static int32_t get_offset_of_mIsDestroyed_29() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___mIsDestroyed_29)); }
	inline bool get_mIsDestroyed_29() const { return ___mIsDestroyed_29; }
	inline bool* get_address_of_mIsDestroyed_29() { return &___mIsDestroyed_29; }
	inline void set_mIsDestroyed_29(bool value)
	{
		___mIsDestroyed_29 = value;
	}

	inline static int32_t get_offset_of_m_State_30() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_State_30)); }
	inline CameraState_t560734301  get_m_State_30() const { return ___m_State_30; }
	inline CameraState_t560734301 * get_address_of_m_State_30() { return &___m_State_30; }
	inline void set_m_State_30(CameraState_t560734301  value)
	{
		___m_State_30 = value;
	}

	inline static int32_t get_offset_of_m_Rigs_31() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_Rigs_31)); }
	inline CinemachineVirtualCameraU5BU5D_t194901906* get_m_Rigs_31() const { return ___m_Rigs_31; }
	inline CinemachineVirtualCameraU5BU5D_t194901906** get_address_of_m_Rigs_31() { return &___m_Rigs_31; }
	inline void set_m_Rigs_31(CinemachineVirtualCameraU5BU5D_t194901906* value)
	{
		___m_Rigs_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigs_31), value);
	}

	inline static int32_t get_offset_of_mOrbitals_32() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___mOrbitals_32)); }
	inline CinemachineOrbitalTransposerU5BU5D_t4090051484* get_mOrbitals_32() const { return ___mOrbitals_32; }
	inline CinemachineOrbitalTransposerU5BU5D_t4090051484** get_address_of_mOrbitals_32() { return &___mOrbitals_32; }
	inline void set_mOrbitals_32(CinemachineOrbitalTransposerU5BU5D_t4090051484* value)
	{
		___mOrbitals_32 = value;
		Il2CppCodeGenWriteBarrier((&___mOrbitals_32), value);
	}

	inline static int32_t get_offset_of_mBlendA_33() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___mBlendA_33)); }
	inline CinemachineBlend_t1772488801 * get_mBlendA_33() const { return ___mBlendA_33; }
	inline CinemachineBlend_t1772488801 ** get_address_of_mBlendA_33() { return &___mBlendA_33; }
	inline void set_mBlendA_33(CinemachineBlend_t1772488801 * value)
	{
		___mBlendA_33 = value;
		Il2CppCodeGenWriteBarrier((&___mBlendA_33), value);
	}

	inline static int32_t get_offset_of_mBlendB_34() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___mBlendB_34)); }
	inline CinemachineBlend_t1772488801 * get_mBlendB_34() const { return ___mBlendB_34; }
	inline CinemachineBlend_t1772488801 ** get_address_of_mBlendB_34() { return &___mBlendB_34; }
	inline void set_mBlendB_34(CinemachineBlend_t1772488801 * value)
	{
		___mBlendB_34 = value;
		Il2CppCodeGenWriteBarrier((&___mBlendB_34), value);
	}

	inline static int32_t get_offset_of_m_CachedOrbits_37() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_CachedOrbits_37)); }
	inline OrbitU5BU5D_t1434718521* get_m_CachedOrbits_37() const { return ___m_CachedOrbits_37; }
	inline OrbitU5BU5D_t1434718521** get_address_of_m_CachedOrbits_37() { return &___m_CachedOrbits_37; }
	inline void set_m_CachedOrbits_37(OrbitU5BU5D_t1434718521* value)
	{
		___m_CachedOrbits_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedOrbits_37), value);
	}

	inline static int32_t get_offset_of_m_CachedTension_38() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_CachedTension_38)); }
	inline float get_m_CachedTension_38() const { return ___m_CachedTension_38; }
	inline float* get_address_of_m_CachedTension_38() { return &___m_CachedTension_38; }
	inline void set_m_CachedTension_38(float value)
	{
		___m_CachedTension_38 = value;
	}

	inline static int32_t get_offset_of_m_CachedKnots_39() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_CachedKnots_39)); }
	inline Vector4U5BU5D_t934056436* get_m_CachedKnots_39() const { return ___m_CachedKnots_39; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_CachedKnots_39() { return &___m_CachedKnots_39; }
	inline void set_m_CachedKnots_39(Vector4U5BU5D_t934056436* value)
	{
		___m_CachedKnots_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedKnots_39), value);
	}

	inline static int32_t get_offset_of_m_CachedCtrl1_40() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_CachedCtrl1_40)); }
	inline Vector4U5BU5D_t934056436* get_m_CachedCtrl1_40() const { return ___m_CachedCtrl1_40; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_CachedCtrl1_40() { return &___m_CachedCtrl1_40; }
	inline void set_m_CachedCtrl1_40(Vector4U5BU5D_t934056436* value)
	{
		___m_CachedCtrl1_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedCtrl1_40), value);
	}

	inline static int32_t get_offset_of_m_CachedCtrl2_41() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944, ___m_CachedCtrl2_41)); }
	inline Vector4U5BU5D_t934056436* get_m_CachedCtrl2_41() const { return ___m_CachedCtrl2_41; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_CachedCtrl2_41() { return &___m_CachedCtrl2_41; }
	inline void set_m_CachedCtrl2_41(Vector4U5BU5D_t934056436* value)
	{
		___m_CachedCtrl2_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedCtrl2_41), value);
	}
};

struct CinemachineFreeLook_t3895466944_StaticFields
{
public:
	// Cinemachine.CinemachineFreeLook/CreateRigDelegate Cinemachine.CinemachineFreeLook::CreateRigOverride
	CreateRigDelegate_t3127201826 * ___CreateRigOverride_35;
	// Cinemachine.CinemachineFreeLook/DestroyRigDelegate Cinemachine.CinemachineFreeLook::DestroyRigOverride
	DestroyRigDelegate_t2388233918 * ___DestroyRigOverride_36;

public:
	inline static int32_t get_offset_of_CreateRigOverride_35() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944_StaticFields, ___CreateRigOverride_35)); }
	inline CreateRigDelegate_t3127201826 * get_CreateRigOverride_35() const { return ___CreateRigOverride_35; }
	inline CreateRigDelegate_t3127201826 ** get_address_of_CreateRigOverride_35() { return &___CreateRigOverride_35; }
	inline void set_CreateRigOverride_35(CreateRigDelegate_t3127201826 * value)
	{
		___CreateRigOverride_35 = value;
		Il2CppCodeGenWriteBarrier((&___CreateRigOverride_35), value);
	}

	inline static int32_t get_offset_of_DestroyRigOverride_36() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t3895466944_StaticFields, ___DestroyRigOverride_36)); }
	inline DestroyRigDelegate_t2388233918 * get_DestroyRigOverride_36() const { return ___DestroyRigOverride_36; }
	inline DestroyRigDelegate_t2388233918 ** get_address_of_DestroyRigOverride_36() { return &___DestroyRigOverride_36; }
	inline void set_DestroyRigOverride_36(DestroyRigDelegate_t2388233918 * value)
	{
		___DestroyRigOverride_36 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyRigOverride_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEFREELOOK_T3895466944_H
#ifndef CINEMACHINESAMEASFOLLOWOBJECT_T3628312148_H
#define CINEMACHINESAMEASFOLLOWOBJECT_T3628312148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineSameAsFollowObject
struct  CinemachineSameAsFollowObject_t3628312148  : public CinemachineComponentBase_t4122537527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESAMEASFOLLOWOBJECT_T3628312148_H
#ifndef CINEMACHINEFOLLOWZOOM_T358397207_H
#define CINEMACHINEFOLLOWZOOM_T358397207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFollowZoom
struct  CinemachineFollowZoom_t358397207  : public CinemachineExtension_t890367330
{
public:
	// System.Single Cinemachine.CinemachineFollowZoom::m_Width
	float ___m_Width_5;
	// System.Single Cinemachine.CinemachineFollowZoom::m_Damping
	float ___m_Damping_6;
	// System.Single Cinemachine.CinemachineFollowZoom::m_MinFOV
	float ___m_MinFOV_7;
	// System.Single Cinemachine.CinemachineFollowZoom::m_MaxFOV
	float ___m_MaxFOV_8;

public:
	inline static int32_t get_offset_of_m_Width_5() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t358397207, ___m_Width_5)); }
	inline float get_m_Width_5() const { return ___m_Width_5; }
	inline float* get_address_of_m_Width_5() { return &___m_Width_5; }
	inline void set_m_Width_5(float value)
	{
		___m_Width_5 = value;
	}

	inline static int32_t get_offset_of_m_Damping_6() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t358397207, ___m_Damping_6)); }
	inline float get_m_Damping_6() const { return ___m_Damping_6; }
	inline float* get_address_of_m_Damping_6() { return &___m_Damping_6; }
	inline void set_m_Damping_6(float value)
	{
		___m_Damping_6 = value;
	}

	inline static int32_t get_offset_of_m_MinFOV_7() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t358397207, ___m_MinFOV_7)); }
	inline float get_m_MinFOV_7() const { return ___m_MinFOV_7; }
	inline float* get_address_of_m_MinFOV_7() { return &___m_MinFOV_7; }
	inline void set_m_MinFOV_7(float value)
	{
		___m_MinFOV_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxFOV_8() { return static_cast<int32_t>(offsetof(CinemachineFollowZoom_t358397207, ___m_MaxFOV_8)); }
	inline float get_m_MaxFOV_8() const { return ___m_MaxFOV_8; }
	inline float* get_address_of_m_MaxFOV_8() { return &___m_MaxFOV_8; }
	inline void set_m_MaxFOV_8(float value)
	{
		___m_MaxFOV_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEFOLLOWZOOM_T358397207_H
#ifndef CINEMACHINEEXTERNALCAMERA_T3942460354_H
#define CINEMACHINEEXTERNALCAMERA_T3942460354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineExternalCamera
struct  CinemachineExternalCamera_t3942460354  : public CinemachineVirtualCameraBase_t1327958294
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineExternalCamera::m_LookAt
	Transform_t3600365921 * ___m_LookAt_16;
	// UnityEngine.Camera Cinemachine.CinemachineExternalCamera::m_Camera
	Camera_t4157153871 * ___m_Camera_17;
	// Cinemachine.CameraState Cinemachine.CinemachineExternalCamera::m_State
	CameraState_t560734301  ___m_State_18;
	// UnityEngine.Transform Cinemachine.CinemachineExternalCamera::<Follow>k__BackingField
	Transform_t3600365921 * ___U3CFollowU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_m_LookAt_16() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3942460354, ___m_LookAt_16)); }
	inline Transform_t3600365921 * get_m_LookAt_16() const { return ___m_LookAt_16; }
	inline Transform_t3600365921 ** get_address_of_m_LookAt_16() { return &___m_LookAt_16; }
	inline void set_m_LookAt_16(Transform_t3600365921 * value)
	{
		___m_LookAt_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_16), value);
	}

	inline static int32_t get_offset_of_m_Camera_17() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3942460354, ___m_Camera_17)); }
	inline Camera_t4157153871 * get_m_Camera_17() const { return ___m_Camera_17; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_17() { return &___m_Camera_17; }
	inline void set_m_Camera_17(Camera_t4157153871 * value)
	{
		___m_Camera_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_17), value);
	}

	inline static int32_t get_offset_of_m_State_18() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3942460354, ___m_State_18)); }
	inline CameraState_t560734301  get_m_State_18() const { return ___m_State_18; }
	inline CameraState_t560734301 * get_address_of_m_State_18() { return &___m_State_18; }
	inline void set_m_State_18(CameraState_t560734301  value)
	{
		___m_State_18 = value;
	}

	inline static int32_t get_offset_of_U3CFollowU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(CinemachineExternalCamera_t3942460354, ___U3CFollowU3Ek__BackingField_19)); }
	inline Transform_t3600365921 * get_U3CFollowU3Ek__BackingField_19() const { return ___U3CFollowU3Ek__BackingField_19; }
	inline Transform_t3600365921 ** get_address_of_U3CFollowU3Ek__BackingField_19() { return &___U3CFollowU3Ek__BackingField_19; }
	inline void set_U3CFollowU3Ek__BackingField_19(Transform_t3600365921 * value)
	{
		___U3CFollowU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFollowU3Ek__BackingField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEEXTERNALCAMERA_T3942460354_H
#ifndef CINEMACHINEPOV_T1973872934_H
#define CINEMACHINEPOV_T1973872934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePOV
struct  CinemachinePOV_t1973872934  : public CinemachineComponentBase_t4122537527
{
public:
	// Cinemachine.AxisState Cinemachine.CinemachinePOV::m_VerticalAxis
	AxisState_t478218302  ___m_VerticalAxis_4;
	// Cinemachine.AxisState Cinemachine.CinemachinePOV::m_HorizontalAxis
	AxisState_t478218302  ___m_HorizontalAxis_5;

public:
	inline static int32_t get_offset_of_m_VerticalAxis_4() { return static_cast<int32_t>(offsetof(CinemachinePOV_t1973872934, ___m_VerticalAxis_4)); }
	inline AxisState_t478218302  get_m_VerticalAxis_4() const { return ___m_VerticalAxis_4; }
	inline AxisState_t478218302 * get_address_of_m_VerticalAxis_4() { return &___m_VerticalAxis_4; }
	inline void set_m_VerticalAxis_4(AxisState_t478218302  value)
	{
		___m_VerticalAxis_4 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_5() { return static_cast<int32_t>(offsetof(CinemachinePOV_t1973872934, ___m_HorizontalAxis_5)); }
	inline AxisState_t478218302  get_m_HorizontalAxis_5() const { return ___m_HorizontalAxis_5; }
	inline AxisState_t478218302 * get_address_of_m_HorizontalAxis_5() { return &___m_HorizontalAxis_5; }
	inline void set_m_HorizontalAxis_5(AxisState_t478218302  value)
	{
		___m_HorizontalAxis_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPOV_T1973872934_H
#ifndef CINEMACHINEPATH_T1083711280_H
#define CINEMACHINEPATH_T1083711280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachinePath
struct  CinemachinePath_t1083711280  : public CinemachinePathBase_t1750211330
{
public:
	// System.Boolean Cinemachine.CinemachinePath::m_Looped
	bool ___m_Looped_10;
	// Cinemachine.CinemachinePath/Waypoint[] Cinemachine.CinemachinePath::m_Waypoints
	WaypointU5BU5D_t2291284199* ___m_Waypoints_11;

public:
	inline static int32_t get_offset_of_m_Looped_10() { return static_cast<int32_t>(offsetof(CinemachinePath_t1083711280, ___m_Looped_10)); }
	inline bool get_m_Looped_10() const { return ___m_Looped_10; }
	inline bool* get_address_of_m_Looped_10() { return &___m_Looped_10; }
	inline void set_m_Looped_10(bool value)
	{
		___m_Looped_10 = value;
	}

	inline static int32_t get_offset_of_m_Waypoints_11() { return static_cast<int32_t>(offsetof(CinemachinePath_t1083711280, ___m_Waypoints_11)); }
	inline WaypointU5BU5D_t2291284199* get_m_Waypoints_11() const { return ___m_Waypoints_11; }
	inline WaypointU5BU5D_t2291284199** get_address_of_m_Waypoints_11() { return &___m_Waypoints_11; }
	inline void set_m_Waypoints_11(WaypointU5BU5D_t2291284199* value)
	{
		___m_Waypoints_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Waypoints_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEPATH_T1083711280_H
#ifndef CINEMACHINEBASICMULTICHANNELPERLIN_T1881527403_H
#define CINEMACHINEBASICMULTICHANNELPERLIN_T1881527403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineBasicMultiChannelPerlin
struct  CinemachineBasicMultiChannelPerlin_t1881527403  : public CinemachineComponentBase_t4122537527
{
public:
	// Cinemachine.NoiseSettings Cinemachine.CinemachineBasicMultiChannelPerlin::m_NoiseProfile
	NoiseSettings_t3019862488 * ___m_NoiseProfile_4;
	// System.Single Cinemachine.CinemachineBasicMultiChannelPerlin::m_AmplitudeGain
	float ___m_AmplitudeGain_5;
	// System.Single Cinemachine.CinemachineBasicMultiChannelPerlin::m_FrequencyGain
	float ___m_FrequencyGain_6;
	// System.Boolean Cinemachine.CinemachineBasicMultiChannelPerlin::mInitialized
	bool ___mInitialized_7;
	// System.Single Cinemachine.CinemachineBasicMultiChannelPerlin::mNoiseTime
	float ___mNoiseTime_8;
	// UnityEngine.Vector3 Cinemachine.CinemachineBasicMultiChannelPerlin::mNoiseOffsets
	Vector3_t3722313464  ___mNoiseOffsets_9;

public:
	inline static int32_t get_offset_of_m_NoiseProfile_4() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t1881527403, ___m_NoiseProfile_4)); }
	inline NoiseSettings_t3019862488 * get_m_NoiseProfile_4() const { return ___m_NoiseProfile_4; }
	inline NoiseSettings_t3019862488 ** get_address_of_m_NoiseProfile_4() { return &___m_NoiseProfile_4; }
	inline void set_m_NoiseProfile_4(NoiseSettings_t3019862488 * value)
	{
		___m_NoiseProfile_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_NoiseProfile_4), value);
	}

	inline static int32_t get_offset_of_m_AmplitudeGain_5() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t1881527403, ___m_AmplitudeGain_5)); }
	inline float get_m_AmplitudeGain_5() const { return ___m_AmplitudeGain_5; }
	inline float* get_address_of_m_AmplitudeGain_5() { return &___m_AmplitudeGain_5; }
	inline void set_m_AmplitudeGain_5(float value)
	{
		___m_AmplitudeGain_5 = value;
	}

	inline static int32_t get_offset_of_m_FrequencyGain_6() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t1881527403, ___m_FrequencyGain_6)); }
	inline float get_m_FrequencyGain_6() const { return ___m_FrequencyGain_6; }
	inline float* get_address_of_m_FrequencyGain_6() { return &___m_FrequencyGain_6; }
	inline void set_m_FrequencyGain_6(float value)
	{
		___m_FrequencyGain_6 = value;
	}

	inline static int32_t get_offset_of_mInitialized_7() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t1881527403, ___mInitialized_7)); }
	inline bool get_mInitialized_7() const { return ___mInitialized_7; }
	inline bool* get_address_of_mInitialized_7() { return &___mInitialized_7; }
	inline void set_mInitialized_7(bool value)
	{
		___mInitialized_7 = value;
	}

	inline static int32_t get_offset_of_mNoiseTime_8() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t1881527403, ___mNoiseTime_8)); }
	inline float get_mNoiseTime_8() const { return ___mNoiseTime_8; }
	inline float* get_address_of_mNoiseTime_8() { return &___mNoiseTime_8; }
	inline void set_mNoiseTime_8(float value)
	{
		___mNoiseTime_8 = value;
	}

	inline static int32_t get_offset_of_mNoiseOffsets_9() { return static_cast<int32_t>(offsetof(CinemachineBasicMultiChannelPerlin_t1881527403, ___mNoiseOffsets_9)); }
	inline Vector3_t3722313464  get_mNoiseOffsets_9() const { return ___mNoiseOffsets_9; }
	inline Vector3_t3722313464 * get_address_of_mNoiseOffsets_9() { return &___mNoiseOffsets_9; }
	inline void set_mNoiseOffsets_9(Vector3_t3722313464  value)
	{
		___mNoiseOffsets_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEBASICMULTICHANNELPERLIN_T1881527403_H
#ifndef CINEMACHINETRANSPOSER_T3421585208_H
#define CINEMACHINETRANSPOSER_T3421585208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineTransposer
struct  CinemachineTransposer_t3421585208  : public CinemachineComponentBase_t4122537527
{
public:
	// Cinemachine.CinemachineTransposer/BindingMode Cinemachine.CinemachineTransposer::m_BindingMode
	int32_t ___m_BindingMode_4;
	// UnityEngine.Vector3 Cinemachine.CinemachineTransposer::m_FollowOffset
	Vector3_t3722313464  ___m_FollowOffset_5;
	// System.Single Cinemachine.CinemachineTransposer::m_XDamping
	float ___m_XDamping_6;
	// System.Single Cinemachine.CinemachineTransposer::m_YDamping
	float ___m_YDamping_7;
	// System.Single Cinemachine.CinemachineTransposer::m_ZDamping
	float ___m_ZDamping_8;
	// System.Single Cinemachine.CinemachineTransposer::m_PitchDamping
	float ___m_PitchDamping_9;
	// System.Single Cinemachine.CinemachineTransposer::m_YawDamping
	float ___m_YawDamping_10;
	// System.Single Cinemachine.CinemachineTransposer::m_RollDamping
	float ___m_RollDamping_11;
	// UnityEngine.Vector3 Cinemachine.CinemachineTransposer::m_PreviousTargetPosition
	Vector3_t3722313464  ___m_PreviousTargetPosition_12;
	// UnityEngine.Quaternion Cinemachine.CinemachineTransposer::m_PreviousReferenceOrientation
	Quaternion_t2301928331  ___m_PreviousReferenceOrientation_13;
	// UnityEngine.Quaternion Cinemachine.CinemachineTransposer::m_targetOrientationOnAssign
	Quaternion_t2301928331  ___m_targetOrientationOnAssign_14;
	// UnityEngine.Transform Cinemachine.CinemachineTransposer::m_previousTarget
	Transform_t3600365921 * ___m_previousTarget_15;

public:
	inline static int32_t get_offset_of_m_BindingMode_4() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_BindingMode_4)); }
	inline int32_t get_m_BindingMode_4() const { return ___m_BindingMode_4; }
	inline int32_t* get_address_of_m_BindingMode_4() { return &___m_BindingMode_4; }
	inline void set_m_BindingMode_4(int32_t value)
	{
		___m_BindingMode_4 = value;
	}

	inline static int32_t get_offset_of_m_FollowOffset_5() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_FollowOffset_5)); }
	inline Vector3_t3722313464  get_m_FollowOffset_5() const { return ___m_FollowOffset_5; }
	inline Vector3_t3722313464 * get_address_of_m_FollowOffset_5() { return &___m_FollowOffset_5; }
	inline void set_m_FollowOffset_5(Vector3_t3722313464  value)
	{
		___m_FollowOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_6() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_XDamping_6)); }
	inline float get_m_XDamping_6() const { return ___m_XDamping_6; }
	inline float* get_address_of_m_XDamping_6() { return &___m_XDamping_6; }
	inline void set_m_XDamping_6(float value)
	{
		___m_XDamping_6 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_7() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_YDamping_7)); }
	inline float get_m_YDamping_7() const { return ___m_YDamping_7; }
	inline float* get_address_of_m_YDamping_7() { return &___m_YDamping_7; }
	inline void set_m_YDamping_7(float value)
	{
		___m_YDamping_7 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_8() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_ZDamping_8)); }
	inline float get_m_ZDamping_8() const { return ___m_ZDamping_8; }
	inline float* get_address_of_m_ZDamping_8() { return &___m_ZDamping_8; }
	inline void set_m_ZDamping_8(float value)
	{
		___m_ZDamping_8 = value;
	}

	inline static int32_t get_offset_of_m_PitchDamping_9() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_PitchDamping_9)); }
	inline float get_m_PitchDamping_9() const { return ___m_PitchDamping_9; }
	inline float* get_address_of_m_PitchDamping_9() { return &___m_PitchDamping_9; }
	inline void set_m_PitchDamping_9(float value)
	{
		___m_PitchDamping_9 = value;
	}

	inline static int32_t get_offset_of_m_YawDamping_10() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_YawDamping_10)); }
	inline float get_m_YawDamping_10() const { return ___m_YawDamping_10; }
	inline float* get_address_of_m_YawDamping_10() { return &___m_YawDamping_10; }
	inline void set_m_YawDamping_10(float value)
	{
		___m_YawDamping_10 = value;
	}

	inline static int32_t get_offset_of_m_RollDamping_11() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_RollDamping_11)); }
	inline float get_m_RollDamping_11() const { return ___m_RollDamping_11; }
	inline float* get_address_of_m_RollDamping_11() { return &___m_RollDamping_11; }
	inline void set_m_RollDamping_11(float value)
	{
		___m_RollDamping_11 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTargetPosition_12() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_PreviousTargetPosition_12)); }
	inline Vector3_t3722313464  get_m_PreviousTargetPosition_12() const { return ___m_PreviousTargetPosition_12; }
	inline Vector3_t3722313464 * get_address_of_m_PreviousTargetPosition_12() { return &___m_PreviousTargetPosition_12; }
	inline void set_m_PreviousTargetPosition_12(Vector3_t3722313464  value)
	{
		___m_PreviousTargetPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_PreviousReferenceOrientation_13() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_PreviousReferenceOrientation_13)); }
	inline Quaternion_t2301928331  get_m_PreviousReferenceOrientation_13() const { return ___m_PreviousReferenceOrientation_13; }
	inline Quaternion_t2301928331 * get_address_of_m_PreviousReferenceOrientation_13() { return &___m_PreviousReferenceOrientation_13; }
	inline void set_m_PreviousReferenceOrientation_13(Quaternion_t2301928331  value)
	{
		___m_PreviousReferenceOrientation_13 = value;
	}

	inline static int32_t get_offset_of_m_targetOrientationOnAssign_14() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_targetOrientationOnAssign_14)); }
	inline Quaternion_t2301928331  get_m_targetOrientationOnAssign_14() const { return ___m_targetOrientationOnAssign_14; }
	inline Quaternion_t2301928331 * get_address_of_m_targetOrientationOnAssign_14() { return &___m_targetOrientationOnAssign_14; }
	inline void set_m_targetOrientationOnAssign_14(Quaternion_t2301928331  value)
	{
		___m_targetOrientationOnAssign_14 = value;
	}

	inline static int32_t get_offset_of_m_previousTarget_15() { return static_cast<int32_t>(offsetof(CinemachineTransposer_t3421585208, ___m_previousTarget_15)); }
	inline Transform_t3600365921 * get_m_previousTarget_15() const { return ___m_previousTarget_15; }
	inline Transform_t3600365921 ** get_address_of_m_previousTarget_15() { return &___m_previousTarget_15; }
	inline void set_m_previousTarget_15(Transform_t3600365921 * value)
	{
		___m_previousTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_previousTarget_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETRANSPOSER_T3421585208_H
#ifndef CINEMACHINEFRAMINGTRANSPOSER_T2273839967_H
#define CINEMACHINEFRAMINGTRANSPOSER_T2273839967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineFramingTransposer
struct  CinemachineFramingTransposer_t2273839967  : public CinemachineComponentBase_t4122537527
{
public:
	// System.Action Cinemachine.CinemachineFramingTransposer::OnGUICallback
	Action_t1264377477 * ___OnGUICallback_4;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_LookaheadTime
	float ___m_LookaheadTime_5;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_LookaheadSmoothing
	float ___m_LookaheadSmoothing_6;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_XDamping
	float ___m_XDamping_7;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_YDamping
	float ___m_YDamping_8;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_ZDamping
	float ___m_ZDamping_9;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_ScreenX
	float ___m_ScreenX_10;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_ScreenY
	float ___m_ScreenY_11;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_CameraDistance
	float ___m_CameraDistance_12;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_DeadZoneWidth
	float ___m_DeadZoneWidth_13;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_DeadZoneHeight
	float ___m_DeadZoneHeight_14;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_DeadZoneDepth
	float ___m_DeadZoneDepth_15;
	// System.Boolean Cinemachine.CinemachineFramingTransposer::m_UnlimitedSoftZone
	bool ___m_UnlimitedSoftZone_16;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_SoftZoneWidth
	float ___m_SoftZoneWidth_17;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_SoftZoneHeight
	float ___m_SoftZoneHeight_18;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_BiasX
	float ___m_BiasX_19;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_BiasY
	float ___m_BiasY_20;
	// Cinemachine.CinemachineFramingTransposer/FramingMode Cinemachine.CinemachineFramingTransposer::m_GroupFramingMode
	int32_t ___m_GroupFramingMode_21;
	// Cinemachine.CinemachineFramingTransposer/AdjustmentMode Cinemachine.CinemachineFramingTransposer::m_AdjustmentMode
	int32_t ___m_AdjustmentMode_22;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_GroupFramingSize
	float ___m_GroupFramingSize_23;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaxDollyIn
	float ___m_MaxDollyIn_24;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaxDollyOut
	float ___m_MaxDollyOut_25;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MinimumDistance
	float ___m_MinimumDistance_26;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaximumDistance
	float ___m_MaximumDistance_27;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MinimumFOV
	float ___m_MinimumFOV_28;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaximumFOV
	float ___m_MaximumFOV_29;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MinimumOrthoSize
	float ___m_MinimumOrthoSize_30;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaximumOrthoSize
	float ___m_MaximumOrthoSize_31;
	// UnityEngine.Vector3 Cinemachine.CinemachineFramingTransposer::m_PreviousCameraPosition
	Vector3_t3722313464  ___m_PreviousCameraPosition_33;
	// Cinemachine.Utility.PositionPredictor Cinemachine.CinemachineFramingTransposer::m_Predictor
	PositionPredictor_t974033707 * ___m_Predictor_34;
	// UnityEngine.Vector3 Cinemachine.CinemachineFramingTransposer::<TrackedPoint>k__BackingField
	Vector3_t3722313464  ___U3CTrackedPointU3Ek__BackingField_35;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_prevTargetHeight
	float ___m_prevTargetHeight_36;
	// UnityEngine.Bounds Cinemachine.CinemachineFramingTransposer::<m_LastBounds>k__BackingField
	Bounds_t2266837910  ___U3Cm_LastBoundsU3Ek__BackingField_37;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineFramingTransposer::<m_lastBoundsMatrix>k__BackingField
	Matrix4x4_t1817901843  ___U3Cm_lastBoundsMatrixU3Ek__BackingField_38;

public:
	inline static int32_t get_offset_of_OnGUICallback_4() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___OnGUICallback_4)); }
	inline Action_t1264377477 * get_OnGUICallback_4() const { return ___OnGUICallback_4; }
	inline Action_t1264377477 ** get_address_of_OnGUICallback_4() { return &___OnGUICallback_4; }
	inline void set_OnGUICallback_4(Action_t1264377477 * value)
	{
		___OnGUICallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnGUICallback_4), value);
	}

	inline static int32_t get_offset_of_m_LookaheadTime_5() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_LookaheadTime_5)); }
	inline float get_m_LookaheadTime_5() const { return ___m_LookaheadTime_5; }
	inline float* get_address_of_m_LookaheadTime_5() { return &___m_LookaheadTime_5; }
	inline void set_m_LookaheadTime_5(float value)
	{
		___m_LookaheadTime_5 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadSmoothing_6() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_LookaheadSmoothing_6)); }
	inline float get_m_LookaheadSmoothing_6() const { return ___m_LookaheadSmoothing_6; }
	inline float* get_address_of_m_LookaheadSmoothing_6() { return &___m_LookaheadSmoothing_6; }
	inline void set_m_LookaheadSmoothing_6(float value)
	{
		___m_LookaheadSmoothing_6 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_7() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_XDamping_7)); }
	inline float get_m_XDamping_7() const { return ___m_XDamping_7; }
	inline float* get_address_of_m_XDamping_7() { return &___m_XDamping_7; }
	inline void set_m_XDamping_7(float value)
	{
		___m_XDamping_7 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_8() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_YDamping_8)); }
	inline float get_m_YDamping_8() const { return ___m_YDamping_8; }
	inline float* get_address_of_m_YDamping_8() { return &___m_YDamping_8; }
	inline void set_m_YDamping_8(float value)
	{
		___m_YDamping_8 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_9() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_ZDamping_9)); }
	inline float get_m_ZDamping_9() const { return ___m_ZDamping_9; }
	inline float* get_address_of_m_ZDamping_9() { return &___m_ZDamping_9; }
	inline void set_m_ZDamping_9(float value)
	{
		___m_ZDamping_9 = value;
	}

	inline static int32_t get_offset_of_m_ScreenX_10() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_ScreenX_10)); }
	inline float get_m_ScreenX_10() const { return ___m_ScreenX_10; }
	inline float* get_address_of_m_ScreenX_10() { return &___m_ScreenX_10; }
	inline void set_m_ScreenX_10(float value)
	{
		___m_ScreenX_10 = value;
	}

	inline static int32_t get_offset_of_m_ScreenY_11() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_ScreenY_11)); }
	inline float get_m_ScreenY_11() const { return ___m_ScreenY_11; }
	inline float* get_address_of_m_ScreenY_11() { return &___m_ScreenY_11; }
	inline void set_m_ScreenY_11(float value)
	{
		___m_ScreenY_11 = value;
	}

	inline static int32_t get_offset_of_m_CameraDistance_12() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_CameraDistance_12)); }
	inline float get_m_CameraDistance_12() const { return ___m_CameraDistance_12; }
	inline float* get_address_of_m_CameraDistance_12() { return &___m_CameraDistance_12; }
	inline void set_m_CameraDistance_12(float value)
	{
		___m_CameraDistance_12 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneWidth_13() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_DeadZoneWidth_13)); }
	inline float get_m_DeadZoneWidth_13() const { return ___m_DeadZoneWidth_13; }
	inline float* get_address_of_m_DeadZoneWidth_13() { return &___m_DeadZoneWidth_13; }
	inline void set_m_DeadZoneWidth_13(float value)
	{
		___m_DeadZoneWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneHeight_14() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_DeadZoneHeight_14)); }
	inline float get_m_DeadZoneHeight_14() const { return ___m_DeadZoneHeight_14; }
	inline float* get_address_of_m_DeadZoneHeight_14() { return &___m_DeadZoneHeight_14; }
	inline void set_m_DeadZoneHeight_14(float value)
	{
		___m_DeadZoneHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneDepth_15() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_DeadZoneDepth_15)); }
	inline float get_m_DeadZoneDepth_15() const { return ___m_DeadZoneDepth_15; }
	inline float* get_address_of_m_DeadZoneDepth_15() { return &___m_DeadZoneDepth_15; }
	inline void set_m_DeadZoneDepth_15(float value)
	{
		___m_DeadZoneDepth_15 = value;
	}

	inline static int32_t get_offset_of_m_UnlimitedSoftZone_16() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_UnlimitedSoftZone_16)); }
	inline bool get_m_UnlimitedSoftZone_16() const { return ___m_UnlimitedSoftZone_16; }
	inline bool* get_address_of_m_UnlimitedSoftZone_16() { return &___m_UnlimitedSoftZone_16; }
	inline void set_m_UnlimitedSoftZone_16(bool value)
	{
		___m_UnlimitedSoftZone_16 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneWidth_17() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_SoftZoneWidth_17)); }
	inline float get_m_SoftZoneWidth_17() const { return ___m_SoftZoneWidth_17; }
	inline float* get_address_of_m_SoftZoneWidth_17() { return &___m_SoftZoneWidth_17; }
	inline void set_m_SoftZoneWidth_17(float value)
	{
		___m_SoftZoneWidth_17 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneHeight_18() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_SoftZoneHeight_18)); }
	inline float get_m_SoftZoneHeight_18() const { return ___m_SoftZoneHeight_18; }
	inline float* get_address_of_m_SoftZoneHeight_18() { return &___m_SoftZoneHeight_18; }
	inline void set_m_SoftZoneHeight_18(float value)
	{
		___m_SoftZoneHeight_18 = value;
	}

	inline static int32_t get_offset_of_m_BiasX_19() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_BiasX_19)); }
	inline float get_m_BiasX_19() const { return ___m_BiasX_19; }
	inline float* get_address_of_m_BiasX_19() { return &___m_BiasX_19; }
	inline void set_m_BiasX_19(float value)
	{
		___m_BiasX_19 = value;
	}

	inline static int32_t get_offset_of_m_BiasY_20() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_BiasY_20)); }
	inline float get_m_BiasY_20() const { return ___m_BiasY_20; }
	inline float* get_address_of_m_BiasY_20() { return &___m_BiasY_20; }
	inline void set_m_BiasY_20(float value)
	{
		___m_BiasY_20 = value;
	}

	inline static int32_t get_offset_of_m_GroupFramingMode_21() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_GroupFramingMode_21)); }
	inline int32_t get_m_GroupFramingMode_21() const { return ___m_GroupFramingMode_21; }
	inline int32_t* get_address_of_m_GroupFramingMode_21() { return &___m_GroupFramingMode_21; }
	inline void set_m_GroupFramingMode_21(int32_t value)
	{
		___m_GroupFramingMode_21 = value;
	}

	inline static int32_t get_offset_of_m_AdjustmentMode_22() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_AdjustmentMode_22)); }
	inline int32_t get_m_AdjustmentMode_22() const { return ___m_AdjustmentMode_22; }
	inline int32_t* get_address_of_m_AdjustmentMode_22() { return &___m_AdjustmentMode_22; }
	inline void set_m_AdjustmentMode_22(int32_t value)
	{
		___m_AdjustmentMode_22 = value;
	}

	inline static int32_t get_offset_of_m_GroupFramingSize_23() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_GroupFramingSize_23)); }
	inline float get_m_GroupFramingSize_23() const { return ___m_GroupFramingSize_23; }
	inline float* get_address_of_m_GroupFramingSize_23() { return &___m_GroupFramingSize_23; }
	inline void set_m_GroupFramingSize_23(float value)
	{
		___m_GroupFramingSize_23 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyIn_24() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MaxDollyIn_24)); }
	inline float get_m_MaxDollyIn_24() const { return ___m_MaxDollyIn_24; }
	inline float* get_address_of_m_MaxDollyIn_24() { return &___m_MaxDollyIn_24; }
	inline void set_m_MaxDollyIn_24(float value)
	{
		___m_MaxDollyIn_24 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyOut_25() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MaxDollyOut_25)); }
	inline float get_m_MaxDollyOut_25() const { return ___m_MaxDollyOut_25; }
	inline float* get_address_of_m_MaxDollyOut_25() { return &___m_MaxDollyOut_25; }
	inline void set_m_MaxDollyOut_25(float value)
	{
		___m_MaxDollyOut_25 = value;
	}

	inline static int32_t get_offset_of_m_MinimumDistance_26() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MinimumDistance_26)); }
	inline float get_m_MinimumDistance_26() const { return ___m_MinimumDistance_26; }
	inline float* get_address_of_m_MinimumDistance_26() { return &___m_MinimumDistance_26; }
	inline void set_m_MinimumDistance_26(float value)
	{
		___m_MinimumDistance_26 = value;
	}

	inline static int32_t get_offset_of_m_MaximumDistance_27() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MaximumDistance_27)); }
	inline float get_m_MaximumDistance_27() const { return ___m_MaximumDistance_27; }
	inline float* get_address_of_m_MaximumDistance_27() { return &___m_MaximumDistance_27; }
	inline void set_m_MaximumDistance_27(float value)
	{
		___m_MaximumDistance_27 = value;
	}

	inline static int32_t get_offset_of_m_MinimumFOV_28() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MinimumFOV_28)); }
	inline float get_m_MinimumFOV_28() const { return ___m_MinimumFOV_28; }
	inline float* get_address_of_m_MinimumFOV_28() { return &___m_MinimumFOV_28; }
	inline void set_m_MinimumFOV_28(float value)
	{
		___m_MinimumFOV_28 = value;
	}

	inline static int32_t get_offset_of_m_MaximumFOV_29() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MaximumFOV_29)); }
	inline float get_m_MaximumFOV_29() const { return ___m_MaximumFOV_29; }
	inline float* get_address_of_m_MaximumFOV_29() { return &___m_MaximumFOV_29; }
	inline void set_m_MaximumFOV_29(float value)
	{
		___m_MaximumFOV_29 = value;
	}

	inline static int32_t get_offset_of_m_MinimumOrthoSize_30() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MinimumOrthoSize_30)); }
	inline float get_m_MinimumOrthoSize_30() const { return ___m_MinimumOrthoSize_30; }
	inline float* get_address_of_m_MinimumOrthoSize_30() { return &___m_MinimumOrthoSize_30; }
	inline void set_m_MinimumOrthoSize_30(float value)
	{
		___m_MinimumOrthoSize_30 = value;
	}

	inline static int32_t get_offset_of_m_MaximumOrthoSize_31() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_MaximumOrthoSize_31)); }
	inline float get_m_MaximumOrthoSize_31() const { return ___m_MaximumOrthoSize_31; }
	inline float* get_address_of_m_MaximumOrthoSize_31() { return &___m_MaximumOrthoSize_31; }
	inline void set_m_MaximumOrthoSize_31(float value)
	{
		___m_MaximumOrthoSize_31 = value;
	}

	inline static int32_t get_offset_of_m_PreviousCameraPosition_33() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_PreviousCameraPosition_33)); }
	inline Vector3_t3722313464  get_m_PreviousCameraPosition_33() const { return ___m_PreviousCameraPosition_33; }
	inline Vector3_t3722313464 * get_address_of_m_PreviousCameraPosition_33() { return &___m_PreviousCameraPosition_33; }
	inline void set_m_PreviousCameraPosition_33(Vector3_t3722313464  value)
	{
		___m_PreviousCameraPosition_33 = value;
	}

	inline static int32_t get_offset_of_m_Predictor_34() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_Predictor_34)); }
	inline PositionPredictor_t974033707 * get_m_Predictor_34() const { return ___m_Predictor_34; }
	inline PositionPredictor_t974033707 ** get_address_of_m_Predictor_34() { return &___m_Predictor_34; }
	inline void set_m_Predictor_34(PositionPredictor_t974033707 * value)
	{
		___m_Predictor_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_Predictor_34), value);
	}

	inline static int32_t get_offset_of_U3CTrackedPointU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___U3CTrackedPointU3Ek__BackingField_35)); }
	inline Vector3_t3722313464  get_U3CTrackedPointU3Ek__BackingField_35() const { return ___U3CTrackedPointU3Ek__BackingField_35; }
	inline Vector3_t3722313464 * get_address_of_U3CTrackedPointU3Ek__BackingField_35() { return &___U3CTrackedPointU3Ek__BackingField_35; }
	inline void set_U3CTrackedPointU3Ek__BackingField_35(Vector3_t3722313464  value)
	{
		___U3CTrackedPointU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_m_prevTargetHeight_36() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___m_prevTargetHeight_36)); }
	inline float get_m_prevTargetHeight_36() const { return ___m_prevTargetHeight_36; }
	inline float* get_address_of_m_prevTargetHeight_36() { return &___m_prevTargetHeight_36; }
	inline void set_m_prevTargetHeight_36(float value)
	{
		___m_prevTargetHeight_36 = value;
	}

	inline static int32_t get_offset_of_U3Cm_LastBoundsU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___U3Cm_LastBoundsU3Ek__BackingField_37)); }
	inline Bounds_t2266837910  get_U3Cm_LastBoundsU3Ek__BackingField_37() const { return ___U3Cm_LastBoundsU3Ek__BackingField_37; }
	inline Bounds_t2266837910 * get_address_of_U3Cm_LastBoundsU3Ek__BackingField_37() { return &___U3Cm_LastBoundsU3Ek__BackingField_37; }
	inline void set_U3Cm_LastBoundsU3Ek__BackingField_37(Bounds_t2266837910  value)
	{
		___U3Cm_LastBoundsU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2273839967, ___U3Cm_lastBoundsMatrixU3Ek__BackingField_38)); }
	inline Matrix4x4_t1817901843  get_U3Cm_lastBoundsMatrixU3Ek__BackingField_38() const { return ___U3Cm_lastBoundsMatrixU3Ek__BackingField_38; }
	inline Matrix4x4_t1817901843 * get_address_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_38() { return &___U3Cm_lastBoundsMatrixU3Ek__BackingField_38; }
	inline void set_U3Cm_lastBoundsMatrixU3Ek__BackingField_38(Matrix4x4_t1817901843  value)
	{
		___U3Cm_lastBoundsMatrixU3Ek__BackingField_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEFRAMINGTRANSPOSER_T2273839967_H
#ifndef CINEMACHINEVIRTUALCAMERA_T947248387_H
#define CINEMACHINEVIRTUALCAMERA_T947248387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineVirtualCamera
struct  CinemachineVirtualCamera_t947248387  : public CinemachineVirtualCameraBase_t1327958294
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_LookAt
	Transform_t3600365921 * ___m_LookAt_16;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_Follow
	Transform_t3600365921 * ___m_Follow_17;
	// Cinemachine.LensSettings Cinemachine.CinemachineVirtualCamera::m_Lens
	LensSettings_t822254920  ___m_Lens_18;
	// System.Boolean Cinemachine.CinemachineVirtualCamera::<UserIsDragging>k__BackingField
	bool ___U3CUserIsDraggingU3Ek__BackingField_22;
	// Cinemachine.CameraState Cinemachine.CinemachineVirtualCamera::m_State
	CameraState_t560734301  ___m_State_23;
	// Cinemachine.CinemachineComponentBase[] Cinemachine.CinemachineVirtualCamera::m_ComponentPipeline
	CinemachineComponentBaseU5BU5D_t3785922318* ___m_ComponentPipeline_24;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_ComponentOwner
	Transform_t3600365921 * ___m_ComponentOwner_25;

public:
	inline static int32_t get_offset_of_m_LookAt_16() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___m_LookAt_16)); }
	inline Transform_t3600365921 * get_m_LookAt_16() const { return ___m_LookAt_16; }
	inline Transform_t3600365921 ** get_address_of_m_LookAt_16() { return &___m_LookAt_16; }
	inline void set_m_LookAt_16(Transform_t3600365921 * value)
	{
		___m_LookAt_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_16), value);
	}

	inline static int32_t get_offset_of_m_Follow_17() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___m_Follow_17)); }
	inline Transform_t3600365921 * get_m_Follow_17() const { return ___m_Follow_17; }
	inline Transform_t3600365921 ** get_address_of_m_Follow_17() { return &___m_Follow_17; }
	inline void set_m_Follow_17(Transform_t3600365921 * value)
	{
		___m_Follow_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_17), value);
	}

	inline static int32_t get_offset_of_m_Lens_18() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___m_Lens_18)); }
	inline LensSettings_t822254920  get_m_Lens_18() const { return ___m_Lens_18; }
	inline LensSettings_t822254920 * get_address_of_m_Lens_18() { return &___m_Lens_18; }
	inline void set_m_Lens_18(LensSettings_t822254920  value)
	{
		___m_Lens_18 = value;
	}

	inline static int32_t get_offset_of_U3CUserIsDraggingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___U3CUserIsDraggingU3Ek__BackingField_22)); }
	inline bool get_U3CUserIsDraggingU3Ek__BackingField_22() const { return ___U3CUserIsDraggingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CUserIsDraggingU3Ek__BackingField_22() { return &___U3CUserIsDraggingU3Ek__BackingField_22; }
	inline void set_U3CUserIsDraggingU3Ek__BackingField_22(bool value)
	{
		___U3CUserIsDraggingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_m_State_23() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___m_State_23)); }
	inline CameraState_t560734301  get_m_State_23() const { return ___m_State_23; }
	inline CameraState_t560734301 * get_address_of_m_State_23() { return &___m_State_23; }
	inline void set_m_State_23(CameraState_t560734301  value)
	{
		___m_State_23 = value;
	}

	inline static int32_t get_offset_of_m_ComponentPipeline_24() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___m_ComponentPipeline_24)); }
	inline CinemachineComponentBaseU5BU5D_t3785922318* get_m_ComponentPipeline_24() const { return ___m_ComponentPipeline_24; }
	inline CinemachineComponentBaseU5BU5D_t3785922318** get_address_of_m_ComponentPipeline_24() { return &___m_ComponentPipeline_24; }
	inline void set_m_ComponentPipeline_24(CinemachineComponentBaseU5BU5D_t3785922318* value)
	{
		___m_ComponentPipeline_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentPipeline_24), value);
	}

	inline static int32_t get_offset_of_m_ComponentOwner_25() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387, ___m_ComponentOwner_25)); }
	inline Transform_t3600365921 * get_m_ComponentOwner_25() const { return ___m_ComponentOwner_25; }
	inline Transform_t3600365921 ** get_address_of_m_ComponentOwner_25() { return &___m_ComponentOwner_25; }
	inline void set_m_ComponentOwner_25(Transform_t3600365921 * value)
	{
		___m_ComponentOwner_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentOwner_25), value);
	}
};

struct CinemachineVirtualCamera_t947248387_StaticFields
{
public:
	// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate Cinemachine.CinemachineVirtualCamera::CreatePipelineOverride
	CreatePipelineDelegate_t3891770455 * ___CreatePipelineOverride_20;
	// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate Cinemachine.CinemachineVirtualCamera::DestroyPipelineOverride
	DestroyPipelineDelegate_t29728977 * ___DestroyPipelineOverride_21;
	// System.Comparison`1<Cinemachine.CinemachineComponentBase> Cinemachine.CinemachineVirtualCamera::<>f__am$cache0
	Comparison_1_t3897468706 * ___U3CU3Ef__amU24cache0_26;

public:
	inline static int32_t get_offset_of_CreatePipelineOverride_20() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387_StaticFields, ___CreatePipelineOverride_20)); }
	inline CreatePipelineDelegate_t3891770455 * get_CreatePipelineOverride_20() const { return ___CreatePipelineOverride_20; }
	inline CreatePipelineDelegate_t3891770455 ** get_address_of_CreatePipelineOverride_20() { return &___CreatePipelineOverride_20; }
	inline void set_CreatePipelineOverride_20(CreatePipelineDelegate_t3891770455 * value)
	{
		___CreatePipelineOverride_20 = value;
		Il2CppCodeGenWriteBarrier((&___CreatePipelineOverride_20), value);
	}

	inline static int32_t get_offset_of_DestroyPipelineOverride_21() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387_StaticFields, ___DestroyPipelineOverride_21)); }
	inline DestroyPipelineDelegate_t29728977 * get_DestroyPipelineOverride_21() const { return ___DestroyPipelineOverride_21; }
	inline DestroyPipelineDelegate_t29728977 ** get_address_of_DestroyPipelineOverride_21() { return &___DestroyPipelineOverride_21; }
	inline void set_DestroyPipelineOverride_21(DestroyPipelineDelegate_t29728977 * value)
	{
		___DestroyPipelineOverride_21 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyPipelineOverride_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_26() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t947248387_StaticFields, ___U3CU3Ef__amU24cache0_26)); }
	inline Comparison_1_t3897468706 * get_U3CU3Ef__amU24cache0_26() const { return ___U3CU3Ef__amU24cache0_26; }
	inline Comparison_1_t3897468706 ** get_address_of_U3CU3Ef__amU24cache0_26() { return &___U3CU3Ef__amU24cache0_26; }
	inline void set_U3CU3Ef__amU24cache0_26(Comparison_1_t3897468706 * value)
	{
		___U3CU3Ef__amU24cache0_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEVIRTUALCAMERA_T947248387_H
#ifndef CINEMACHINECONFINER_T3819785210_H
#define CINEMACHINECONFINER_T3819785210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineConfiner
struct  CinemachineConfiner_t3819785210  : public CinemachineExtension_t890367330
{
public:
	// Cinemachine.CinemachineConfiner/Mode Cinemachine.CinemachineConfiner::m_ConfineMode
	int32_t ___m_ConfineMode_5;
	// UnityEngine.Collider Cinemachine.CinemachineConfiner::m_BoundingVolume
	Collider_t1773347010 * ___m_BoundingVolume_6;
	// UnityEngine.Collider2D Cinemachine.CinemachineConfiner::m_BoundingShape2D
	Collider2D_t2806799626 * ___m_BoundingShape2D_7;
	// System.Boolean Cinemachine.CinemachineConfiner::m_ConfineScreenEdges
	bool ___m_ConfineScreenEdges_8;
	// System.Single Cinemachine.CinemachineConfiner::m_Damping
	float ___m_Damping_9;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>> Cinemachine.CinemachineConfiner::m_pathCache
	List_1_t805411711 * ___m_pathCache_10;

public:
	inline static int32_t get_offset_of_m_ConfineMode_5() { return static_cast<int32_t>(offsetof(CinemachineConfiner_t3819785210, ___m_ConfineMode_5)); }
	inline int32_t get_m_ConfineMode_5() const { return ___m_ConfineMode_5; }
	inline int32_t* get_address_of_m_ConfineMode_5() { return &___m_ConfineMode_5; }
	inline void set_m_ConfineMode_5(int32_t value)
	{
		___m_ConfineMode_5 = value;
	}

	inline static int32_t get_offset_of_m_BoundingVolume_6() { return static_cast<int32_t>(offsetof(CinemachineConfiner_t3819785210, ___m_BoundingVolume_6)); }
	inline Collider_t1773347010 * get_m_BoundingVolume_6() const { return ___m_BoundingVolume_6; }
	inline Collider_t1773347010 ** get_address_of_m_BoundingVolume_6() { return &___m_BoundingVolume_6; }
	inline void set_m_BoundingVolume_6(Collider_t1773347010 * value)
	{
		___m_BoundingVolume_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundingVolume_6), value);
	}

	inline static int32_t get_offset_of_m_BoundingShape2D_7() { return static_cast<int32_t>(offsetof(CinemachineConfiner_t3819785210, ___m_BoundingShape2D_7)); }
	inline Collider2D_t2806799626 * get_m_BoundingShape2D_7() const { return ___m_BoundingShape2D_7; }
	inline Collider2D_t2806799626 ** get_address_of_m_BoundingShape2D_7() { return &___m_BoundingShape2D_7; }
	inline void set_m_BoundingShape2D_7(Collider2D_t2806799626 * value)
	{
		___m_BoundingShape2D_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundingShape2D_7), value);
	}

	inline static int32_t get_offset_of_m_ConfineScreenEdges_8() { return static_cast<int32_t>(offsetof(CinemachineConfiner_t3819785210, ___m_ConfineScreenEdges_8)); }
	inline bool get_m_ConfineScreenEdges_8() const { return ___m_ConfineScreenEdges_8; }
	inline bool* get_address_of_m_ConfineScreenEdges_8() { return &___m_ConfineScreenEdges_8; }
	inline void set_m_ConfineScreenEdges_8(bool value)
	{
		___m_ConfineScreenEdges_8 = value;
	}

	inline static int32_t get_offset_of_m_Damping_9() { return static_cast<int32_t>(offsetof(CinemachineConfiner_t3819785210, ___m_Damping_9)); }
	inline float get_m_Damping_9() const { return ___m_Damping_9; }
	inline float* get_address_of_m_Damping_9() { return &___m_Damping_9; }
	inline void set_m_Damping_9(float value)
	{
		___m_Damping_9 = value;
	}

	inline static int32_t get_offset_of_m_pathCache_10() { return static_cast<int32_t>(offsetof(CinemachineConfiner_t3819785210, ___m_pathCache_10)); }
	inline List_1_t805411711 * get_m_pathCache_10() const { return ___m_pathCache_10; }
	inline List_1_t805411711 ** get_address_of_m_pathCache_10() { return &___m_pathCache_10; }
	inline void set_m_pathCache_10(List_1_t805411711 * value)
	{
		___m_pathCache_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_pathCache_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINECONFINER_T3819785210_H
#ifndef CINEMACHINESTATEDRIVENCAMERA_T60529688_H
#define CINEMACHINESTATEDRIVENCAMERA_T60529688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineStateDrivenCamera
struct  CinemachineStateDrivenCamera_t60529688  : public CinemachineVirtualCameraBase_t1327958294
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineStateDrivenCamera::m_LookAt
	Transform_t3600365921 * ___m_LookAt_16;
	// UnityEngine.Transform Cinemachine.CinemachineStateDrivenCamera::m_Follow
	Transform_t3600365921 * ___m_Follow_17;
	// UnityEngine.Animator Cinemachine.CinemachineStateDrivenCamera::m_AnimatedTarget
	Animator_t434523843 * ___m_AnimatedTarget_18;
	// System.Int32 Cinemachine.CinemachineStateDrivenCamera::m_LayerIndex
	int32_t ___m_LayerIndex_19;
	// System.Boolean Cinemachine.CinemachineStateDrivenCamera::m_ShowDebugText
	bool ___m_ShowDebugText_20;
	// System.Boolean Cinemachine.CinemachineStateDrivenCamera::m_EnableAllChildCameras
	bool ___m_EnableAllChildCameras_21;
	// Cinemachine.CinemachineVirtualCameraBase[] Cinemachine.CinemachineStateDrivenCamera::m_ChildCameras
	CinemachineVirtualCameraBaseU5BU5D_t2980098515* ___m_ChildCameras_22;
	// Cinemachine.CinemachineStateDrivenCamera/Instruction[] Cinemachine.CinemachineStateDrivenCamera::m_Instructions
	InstructionU5BU5D_t716093651* ___m_Instructions_23;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineStateDrivenCamera::m_DefaultBlend
	CinemachineBlendDefinition_t3511030937  ___m_DefaultBlend_24;
	// Cinemachine.CinemachineBlenderSettings Cinemachine.CinemachineStateDrivenCamera::m_CustomBlends
	CinemachineBlenderSettings_t3646651286 * ___m_CustomBlends_25;
	// Cinemachine.CinemachineStateDrivenCamera/ParentHash[] Cinemachine.CinemachineStateDrivenCamera::m_ParentHash
	ParentHashU5BU5D_t1415352068* ___m_ParentHash_26;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineStateDrivenCamera::<LiveChild>k__BackingField
	RuntimeObject* ___U3CLiveChildU3Ek__BackingField_27;
	// Cinemachine.CameraState Cinemachine.CinemachineStateDrivenCamera::m_State
	CameraState_t560734301  ___m_State_28;
	// System.Single Cinemachine.CinemachineStateDrivenCamera::mActivationTime
	float ___mActivationTime_29;
	// Cinemachine.CinemachineStateDrivenCamera/Instruction Cinemachine.CinemachineStateDrivenCamera::mActiveInstruction
	Instruction_t2121420310  ___mActiveInstruction_30;
	// System.Single Cinemachine.CinemachineStateDrivenCamera::mPendingActivationTime
	float ___mPendingActivationTime_31;
	// Cinemachine.CinemachineStateDrivenCamera/Instruction Cinemachine.CinemachineStateDrivenCamera::mPendingInstruction
	Instruction_t2121420310  ___mPendingInstruction_32;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineStateDrivenCamera::mActiveBlend
	CinemachineBlend_t1772488801 * ___mActiveBlend_33;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Cinemachine.CinemachineStateDrivenCamera::mInstructionDictionary
	Dictionary_2_t1839659084 * ___mInstructionDictionary_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Cinemachine.CinemachineStateDrivenCamera::mStateParentLookup
	Dictionary_2_t1839659084 * ___mStateParentLookup_35;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Cinemachine.CinemachineStateDrivenCamera::m_clipInfoList
	List_1_t333824601 * ___m_clipInfoList_36;

public:
	inline static int32_t get_offset_of_m_LookAt_16() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_LookAt_16)); }
	inline Transform_t3600365921 * get_m_LookAt_16() const { return ___m_LookAt_16; }
	inline Transform_t3600365921 ** get_address_of_m_LookAt_16() { return &___m_LookAt_16; }
	inline void set_m_LookAt_16(Transform_t3600365921 * value)
	{
		___m_LookAt_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_LookAt_16), value);
	}

	inline static int32_t get_offset_of_m_Follow_17() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_Follow_17)); }
	inline Transform_t3600365921 * get_m_Follow_17() const { return ___m_Follow_17; }
	inline Transform_t3600365921 ** get_address_of_m_Follow_17() { return &___m_Follow_17; }
	inline void set_m_Follow_17(Transform_t3600365921 * value)
	{
		___m_Follow_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Follow_17), value);
	}

	inline static int32_t get_offset_of_m_AnimatedTarget_18() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_AnimatedTarget_18)); }
	inline Animator_t434523843 * get_m_AnimatedTarget_18() const { return ___m_AnimatedTarget_18; }
	inline Animator_t434523843 ** get_address_of_m_AnimatedTarget_18() { return &___m_AnimatedTarget_18; }
	inline void set_m_AnimatedTarget_18(Animator_t434523843 * value)
	{
		___m_AnimatedTarget_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimatedTarget_18), value);
	}

	inline static int32_t get_offset_of_m_LayerIndex_19() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_LayerIndex_19)); }
	inline int32_t get_m_LayerIndex_19() const { return ___m_LayerIndex_19; }
	inline int32_t* get_address_of_m_LayerIndex_19() { return &___m_LayerIndex_19; }
	inline void set_m_LayerIndex_19(int32_t value)
	{
		___m_LayerIndex_19 = value;
	}

	inline static int32_t get_offset_of_m_ShowDebugText_20() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_ShowDebugText_20)); }
	inline bool get_m_ShowDebugText_20() const { return ___m_ShowDebugText_20; }
	inline bool* get_address_of_m_ShowDebugText_20() { return &___m_ShowDebugText_20; }
	inline void set_m_ShowDebugText_20(bool value)
	{
		___m_ShowDebugText_20 = value;
	}

	inline static int32_t get_offset_of_m_EnableAllChildCameras_21() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_EnableAllChildCameras_21)); }
	inline bool get_m_EnableAllChildCameras_21() const { return ___m_EnableAllChildCameras_21; }
	inline bool* get_address_of_m_EnableAllChildCameras_21() { return &___m_EnableAllChildCameras_21; }
	inline void set_m_EnableAllChildCameras_21(bool value)
	{
		___m_EnableAllChildCameras_21 = value;
	}

	inline static int32_t get_offset_of_m_ChildCameras_22() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_ChildCameras_22)); }
	inline CinemachineVirtualCameraBaseU5BU5D_t2980098515* get_m_ChildCameras_22() const { return ___m_ChildCameras_22; }
	inline CinemachineVirtualCameraBaseU5BU5D_t2980098515** get_address_of_m_ChildCameras_22() { return &___m_ChildCameras_22; }
	inline void set_m_ChildCameras_22(CinemachineVirtualCameraBaseU5BU5D_t2980098515* value)
	{
		___m_ChildCameras_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildCameras_22), value);
	}

	inline static int32_t get_offset_of_m_Instructions_23() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_Instructions_23)); }
	inline InstructionU5BU5D_t716093651* get_m_Instructions_23() const { return ___m_Instructions_23; }
	inline InstructionU5BU5D_t716093651** get_address_of_m_Instructions_23() { return &___m_Instructions_23; }
	inline void set_m_Instructions_23(InstructionU5BU5D_t716093651* value)
	{
		___m_Instructions_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instructions_23), value);
	}

	inline static int32_t get_offset_of_m_DefaultBlend_24() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_DefaultBlend_24)); }
	inline CinemachineBlendDefinition_t3511030937  get_m_DefaultBlend_24() const { return ___m_DefaultBlend_24; }
	inline CinemachineBlendDefinition_t3511030937 * get_address_of_m_DefaultBlend_24() { return &___m_DefaultBlend_24; }
	inline void set_m_DefaultBlend_24(CinemachineBlendDefinition_t3511030937  value)
	{
		___m_DefaultBlend_24 = value;
	}

	inline static int32_t get_offset_of_m_CustomBlends_25() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_CustomBlends_25)); }
	inline CinemachineBlenderSettings_t3646651286 * get_m_CustomBlends_25() const { return ___m_CustomBlends_25; }
	inline CinemachineBlenderSettings_t3646651286 ** get_address_of_m_CustomBlends_25() { return &___m_CustomBlends_25; }
	inline void set_m_CustomBlends_25(CinemachineBlenderSettings_t3646651286 * value)
	{
		___m_CustomBlends_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomBlends_25), value);
	}

	inline static int32_t get_offset_of_m_ParentHash_26() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_ParentHash_26)); }
	inline ParentHashU5BU5D_t1415352068* get_m_ParentHash_26() const { return ___m_ParentHash_26; }
	inline ParentHashU5BU5D_t1415352068** get_address_of_m_ParentHash_26() { return &___m_ParentHash_26; }
	inline void set_m_ParentHash_26(ParentHashU5BU5D_t1415352068* value)
	{
		___m_ParentHash_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentHash_26), value);
	}

	inline static int32_t get_offset_of_U3CLiveChildU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___U3CLiveChildU3Ek__BackingField_27)); }
	inline RuntimeObject* get_U3CLiveChildU3Ek__BackingField_27() const { return ___U3CLiveChildU3Ek__BackingField_27; }
	inline RuntimeObject** get_address_of_U3CLiveChildU3Ek__BackingField_27() { return &___U3CLiveChildU3Ek__BackingField_27; }
	inline void set_U3CLiveChildU3Ek__BackingField_27(RuntimeObject* value)
	{
		___U3CLiveChildU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLiveChildU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_m_State_28() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_State_28)); }
	inline CameraState_t560734301  get_m_State_28() const { return ___m_State_28; }
	inline CameraState_t560734301 * get_address_of_m_State_28() { return &___m_State_28; }
	inline void set_m_State_28(CameraState_t560734301  value)
	{
		___m_State_28 = value;
	}

	inline static int32_t get_offset_of_mActivationTime_29() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mActivationTime_29)); }
	inline float get_mActivationTime_29() const { return ___mActivationTime_29; }
	inline float* get_address_of_mActivationTime_29() { return &___mActivationTime_29; }
	inline void set_mActivationTime_29(float value)
	{
		___mActivationTime_29 = value;
	}

	inline static int32_t get_offset_of_mActiveInstruction_30() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mActiveInstruction_30)); }
	inline Instruction_t2121420310  get_mActiveInstruction_30() const { return ___mActiveInstruction_30; }
	inline Instruction_t2121420310 * get_address_of_mActiveInstruction_30() { return &___mActiveInstruction_30; }
	inline void set_mActiveInstruction_30(Instruction_t2121420310  value)
	{
		___mActiveInstruction_30 = value;
	}

	inline static int32_t get_offset_of_mPendingActivationTime_31() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mPendingActivationTime_31)); }
	inline float get_mPendingActivationTime_31() const { return ___mPendingActivationTime_31; }
	inline float* get_address_of_mPendingActivationTime_31() { return &___mPendingActivationTime_31; }
	inline void set_mPendingActivationTime_31(float value)
	{
		___mPendingActivationTime_31 = value;
	}

	inline static int32_t get_offset_of_mPendingInstruction_32() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mPendingInstruction_32)); }
	inline Instruction_t2121420310  get_mPendingInstruction_32() const { return ___mPendingInstruction_32; }
	inline Instruction_t2121420310 * get_address_of_mPendingInstruction_32() { return &___mPendingInstruction_32; }
	inline void set_mPendingInstruction_32(Instruction_t2121420310  value)
	{
		___mPendingInstruction_32 = value;
	}

	inline static int32_t get_offset_of_mActiveBlend_33() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mActiveBlend_33)); }
	inline CinemachineBlend_t1772488801 * get_mActiveBlend_33() const { return ___mActiveBlend_33; }
	inline CinemachineBlend_t1772488801 ** get_address_of_mActiveBlend_33() { return &___mActiveBlend_33; }
	inline void set_mActiveBlend_33(CinemachineBlend_t1772488801 * value)
	{
		___mActiveBlend_33 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveBlend_33), value);
	}

	inline static int32_t get_offset_of_mInstructionDictionary_34() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mInstructionDictionary_34)); }
	inline Dictionary_2_t1839659084 * get_mInstructionDictionary_34() const { return ___mInstructionDictionary_34; }
	inline Dictionary_2_t1839659084 ** get_address_of_mInstructionDictionary_34() { return &___mInstructionDictionary_34; }
	inline void set_mInstructionDictionary_34(Dictionary_2_t1839659084 * value)
	{
		___mInstructionDictionary_34 = value;
		Il2CppCodeGenWriteBarrier((&___mInstructionDictionary_34), value);
	}

	inline static int32_t get_offset_of_mStateParentLookup_35() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___mStateParentLookup_35)); }
	inline Dictionary_2_t1839659084 * get_mStateParentLookup_35() const { return ___mStateParentLookup_35; }
	inline Dictionary_2_t1839659084 ** get_address_of_mStateParentLookup_35() { return &___mStateParentLookup_35; }
	inline void set_mStateParentLookup_35(Dictionary_2_t1839659084 * value)
	{
		___mStateParentLookup_35 = value;
		Il2CppCodeGenWriteBarrier((&___mStateParentLookup_35), value);
	}

	inline static int32_t get_offset_of_m_clipInfoList_36() { return static_cast<int32_t>(offsetof(CinemachineStateDrivenCamera_t60529688, ___m_clipInfoList_36)); }
	inline List_1_t333824601 * get_m_clipInfoList_36() const { return ___m_clipInfoList_36; }
	inline List_1_t333824601 ** get_address_of_m_clipInfoList_36() { return &___m_clipInfoList_36; }
	inline void set_m_clipInfoList_36(List_1_t333824601 * value)
	{
		___m_clipInfoList_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_clipInfoList_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESTATEDRIVENCAMERA_T60529688_H
#ifndef CINEMACHINESMOOTHPATH_T1336093218_H
#define CINEMACHINESMOOTHPATH_T1336093218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineSmoothPath
struct  CinemachineSmoothPath_t1336093218  : public CinemachinePathBase_t1750211330
{
public:
	// System.Boolean Cinemachine.CinemachineSmoothPath::m_Looped
	bool ___m_Looped_10;
	// Cinemachine.CinemachineSmoothPath/Waypoint[] Cinemachine.CinemachineSmoothPath::m_Waypoints
	WaypointU5BU5D_t280685257* ___m_Waypoints_11;
	// Cinemachine.CinemachineSmoothPath/Waypoint[] Cinemachine.CinemachineSmoothPath::m_ControlPoints1
	WaypointU5BU5D_t280685257* ___m_ControlPoints1_12;
	// Cinemachine.CinemachineSmoothPath/Waypoint[] Cinemachine.CinemachineSmoothPath::m_ControlPoints2
	WaypointU5BU5D_t280685257* ___m_ControlPoints2_13;
	// System.Boolean Cinemachine.CinemachineSmoothPath::m_IsLoopedCache
	bool ___m_IsLoopedCache_14;

public:
	inline static int32_t get_offset_of_m_Looped_10() { return static_cast<int32_t>(offsetof(CinemachineSmoothPath_t1336093218, ___m_Looped_10)); }
	inline bool get_m_Looped_10() const { return ___m_Looped_10; }
	inline bool* get_address_of_m_Looped_10() { return &___m_Looped_10; }
	inline void set_m_Looped_10(bool value)
	{
		___m_Looped_10 = value;
	}

	inline static int32_t get_offset_of_m_Waypoints_11() { return static_cast<int32_t>(offsetof(CinemachineSmoothPath_t1336093218, ___m_Waypoints_11)); }
	inline WaypointU5BU5D_t280685257* get_m_Waypoints_11() const { return ___m_Waypoints_11; }
	inline WaypointU5BU5D_t280685257** get_address_of_m_Waypoints_11() { return &___m_Waypoints_11; }
	inline void set_m_Waypoints_11(WaypointU5BU5D_t280685257* value)
	{
		___m_Waypoints_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Waypoints_11), value);
	}

	inline static int32_t get_offset_of_m_ControlPoints1_12() { return static_cast<int32_t>(offsetof(CinemachineSmoothPath_t1336093218, ___m_ControlPoints1_12)); }
	inline WaypointU5BU5D_t280685257* get_m_ControlPoints1_12() const { return ___m_ControlPoints1_12; }
	inline WaypointU5BU5D_t280685257** get_address_of_m_ControlPoints1_12() { return &___m_ControlPoints1_12; }
	inline void set_m_ControlPoints1_12(WaypointU5BU5D_t280685257* value)
	{
		___m_ControlPoints1_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlPoints1_12), value);
	}

	inline static int32_t get_offset_of_m_ControlPoints2_13() { return static_cast<int32_t>(offsetof(CinemachineSmoothPath_t1336093218, ___m_ControlPoints2_13)); }
	inline WaypointU5BU5D_t280685257* get_m_ControlPoints2_13() const { return ___m_ControlPoints2_13; }
	inline WaypointU5BU5D_t280685257** get_address_of_m_ControlPoints2_13() { return &___m_ControlPoints2_13; }
	inline void set_m_ControlPoints2_13(WaypointU5BU5D_t280685257* value)
	{
		___m_ControlPoints2_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlPoints2_13), value);
	}

	inline static int32_t get_offset_of_m_IsLoopedCache_14() { return static_cast<int32_t>(offsetof(CinemachineSmoothPath_t1336093218, ___m_IsLoopedCache_14)); }
	inline bool get_m_IsLoopedCache_14() const { return ___m_IsLoopedCache_14; }
	inline bool* get_address_of_m_IsLoopedCache_14() { return &___m_IsLoopedCache_14; }
	inline void set_m_IsLoopedCache_14(bool value)
	{
		___m_IsLoopedCache_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINESMOOTHPATH_T1336093218_H
#ifndef CINEMACHINEHARDLOCKTOTARGET_T4289260043_H
#define CINEMACHINEHARDLOCKTOTARGET_T4289260043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineHardLockToTarget
struct  CinemachineHardLockToTarget_t4289260043  : public CinemachineComponentBase_t4122537527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEHARDLOCKTOTARGET_T4289260043_H
#ifndef CINEMACHINEORBITALTRANSPOSER_T2654619201_H
#define CINEMACHINEORBITALTRANSPOSER_T2654619201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineOrbitalTransposer
struct  CinemachineOrbitalTransposer_t2654619201  : public CinemachineTransposer_t3421585208
{
public:
	// Cinemachine.CinemachineOrbitalTransposer/Heading Cinemachine.CinemachineOrbitalTransposer::m_Heading
	Heading_t365180425  ___m_Heading_16;
	// Cinemachine.CinemachineOrbitalTransposer/Recentering Cinemachine.CinemachineOrbitalTransposer::m_RecenterToTargetHeading
	Recentering_t980373847  ___m_RecenterToTargetHeading_17;
	// Cinemachine.AxisState Cinemachine.CinemachineOrbitalTransposer::m_XAxis
	AxisState_t478218302  ___m_XAxis_18;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_LegacyRadius
	float ___m_LegacyRadius_19;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_LegacyHeightOffset
	float ___m_LegacyHeightOffset_20;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::m_LegacyHeadingBias
	float ___m_LegacyHeadingBias_21;
	// System.Boolean Cinemachine.CinemachineOrbitalTransposer::m_HeadingIsSlave
	bool ___m_HeadingIsSlave_22;
	// Cinemachine.CinemachineOrbitalTransposer/UpdateHeadingDelegate Cinemachine.CinemachineOrbitalTransposer::HeadingUpdater
	UpdateHeadingDelegate_t3270413085 * ___HeadingUpdater_23;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::mLastHeadingAxisInputTime
	float ___mLastHeadingAxisInputTime_24;
	// System.Single Cinemachine.CinemachineOrbitalTransposer::mHeadingRecenteringVelocity
	float ___mHeadingRecenteringVelocity_25;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer::mLastTargetPosition
	Vector3_t3722313464  ___mLastTargetPosition_26;
	// Cinemachine.CinemachineOrbitalTransposer/HeadingTracker Cinemachine.CinemachineOrbitalTransposer::mHeadingTracker
	HeadingTracker_t85731283 * ___mHeadingTracker_27;
	// UnityEngine.Rigidbody Cinemachine.CinemachineOrbitalTransposer::mTargetRigidBody
	Rigidbody_t3916780224 * ___mTargetRigidBody_28;
	// UnityEngine.Transform Cinemachine.CinemachineOrbitalTransposer::<PreviousTarget>k__BackingField
	Transform_t3600365921 * ___U3CPreviousTargetU3Ek__BackingField_29;
	// UnityEngine.Quaternion Cinemachine.CinemachineOrbitalTransposer::mHeadingPrevFrame
	Quaternion_t2301928331  ___mHeadingPrevFrame_30;
	// UnityEngine.Vector3 Cinemachine.CinemachineOrbitalTransposer::mOffsetPrevFrame
	Vector3_t3722313464  ___mOffsetPrevFrame_31;

public:
	inline static int32_t get_offset_of_m_Heading_16() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_Heading_16)); }
	inline Heading_t365180425  get_m_Heading_16() const { return ___m_Heading_16; }
	inline Heading_t365180425 * get_address_of_m_Heading_16() { return &___m_Heading_16; }
	inline void set_m_Heading_16(Heading_t365180425  value)
	{
		___m_Heading_16 = value;
	}

	inline static int32_t get_offset_of_m_RecenterToTargetHeading_17() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_RecenterToTargetHeading_17)); }
	inline Recentering_t980373847  get_m_RecenterToTargetHeading_17() const { return ___m_RecenterToTargetHeading_17; }
	inline Recentering_t980373847 * get_address_of_m_RecenterToTargetHeading_17() { return &___m_RecenterToTargetHeading_17; }
	inline void set_m_RecenterToTargetHeading_17(Recentering_t980373847  value)
	{
		___m_RecenterToTargetHeading_17 = value;
	}

	inline static int32_t get_offset_of_m_XAxis_18() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_XAxis_18)); }
	inline AxisState_t478218302  get_m_XAxis_18() const { return ___m_XAxis_18; }
	inline AxisState_t478218302 * get_address_of_m_XAxis_18() { return &___m_XAxis_18; }
	inline void set_m_XAxis_18(AxisState_t478218302  value)
	{
		___m_XAxis_18 = value;
	}

	inline static int32_t get_offset_of_m_LegacyRadius_19() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_LegacyRadius_19)); }
	inline float get_m_LegacyRadius_19() const { return ___m_LegacyRadius_19; }
	inline float* get_address_of_m_LegacyRadius_19() { return &___m_LegacyRadius_19; }
	inline void set_m_LegacyRadius_19(float value)
	{
		___m_LegacyRadius_19 = value;
	}

	inline static int32_t get_offset_of_m_LegacyHeightOffset_20() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_LegacyHeightOffset_20)); }
	inline float get_m_LegacyHeightOffset_20() const { return ___m_LegacyHeightOffset_20; }
	inline float* get_address_of_m_LegacyHeightOffset_20() { return &___m_LegacyHeightOffset_20; }
	inline void set_m_LegacyHeightOffset_20(float value)
	{
		___m_LegacyHeightOffset_20 = value;
	}

	inline static int32_t get_offset_of_m_LegacyHeadingBias_21() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_LegacyHeadingBias_21)); }
	inline float get_m_LegacyHeadingBias_21() const { return ___m_LegacyHeadingBias_21; }
	inline float* get_address_of_m_LegacyHeadingBias_21() { return &___m_LegacyHeadingBias_21; }
	inline void set_m_LegacyHeadingBias_21(float value)
	{
		___m_LegacyHeadingBias_21 = value;
	}

	inline static int32_t get_offset_of_m_HeadingIsSlave_22() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___m_HeadingIsSlave_22)); }
	inline bool get_m_HeadingIsSlave_22() const { return ___m_HeadingIsSlave_22; }
	inline bool* get_address_of_m_HeadingIsSlave_22() { return &___m_HeadingIsSlave_22; }
	inline void set_m_HeadingIsSlave_22(bool value)
	{
		___m_HeadingIsSlave_22 = value;
	}

	inline static int32_t get_offset_of_HeadingUpdater_23() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___HeadingUpdater_23)); }
	inline UpdateHeadingDelegate_t3270413085 * get_HeadingUpdater_23() const { return ___HeadingUpdater_23; }
	inline UpdateHeadingDelegate_t3270413085 ** get_address_of_HeadingUpdater_23() { return &___HeadingUpdater_23; }
	inline void set_HeadingUpdater_23(UpdateHeadingDelegate_t3270413085 * value)
	{
		___HeadingUpdater_23 = value;
		Il2CppCodeGenWriteBarrier((&___HeadingUpdater_23), value);
	}

	inline static int32_t get_offset_of_mLastHeadingAxisInputTime_24() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mLastHeadingAxisInputTime_24)); }
	inline float get_mLastHeadingAxisInputTime_24() const { return ___mLastHeadingAxisInputTime_24; }
	inline float* get_address_of_mLastHeadingAxisInputTime_24() { return &___mLastHeadingAxisInputTime_24; }
	inline void set_mLastHeadingAxisInputTime_24(float value)
	{
		___mLastHeadingAxisInputTime_24 = value;
	}

	inline static int32_t get_offset_of_mHeadingRecenteringVelocity_25() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mHeadingRecenteringVelocity_25)); }
	inline float get_mHeadingRecenteringVelocity_25() const { return ___mHeadingRecenteringVelocity_25; }
	inline float* get_address_of_mHeadingRecenteringVelocity_25() { return &___mHeadingRecenteringVelocity_25; }
	inline void set_mHeadingRecenteringVelocity_25(float value)
	{
		___mHeadingRecenteringVelocity_25 = value;
	}

	inline static int32_t get_offset_of_mLastTargetPosition_26() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mLastTargetPosition_26)); }
	inline Vector3_t3722313464  get_mLastTargetPosition_26() const { return ___mLastTargetPosition_26; }
	inline Vector3_t3722313464 * get_address_of_mLastTargetPosition_26() { return &___mLastTargetPosition_26; }
	inline void set_mLastTargetPosition_26(Vector3_t3722313464  value)
	{
		___mLastTargetPosition_26 = value;
	}

	inline static int32_t get_offset_of_mHeadingTracker_27() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mHeadingTracker_27)); }
	inline HeadingTracker_t85731283 * get_mHeadingTracker_27() const { return ___mHeadingTracker_27; }
	inline HeadingTracker_t85731283 ** get_address_of_mHeadingTracker_27() { return &___mHeadingTracker_27; }
	inline void set_mHeadingTracker_27(HeadingTracker_t85731283 * value)
	{
		___mHeadingTracker_27 = value;
		Il2CppCodeGenWriteBarrier((&___mHeadingTracker_27), value);
	}

	inline static int32_t get_offset_of_mTargetRigidBody_28() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mTargetRigidBody_28)); }
	inline Rigidbody_t3916780224 * get_mTargetRigidBody_28() const { return ___mTargetRigidBody_28; }
	inline Rigidbody_t3916780224 ** get_address_of_mTargetRigidBody_28() { return &___mTargetRigidBody_28; }
	inline void set_mTargetRigidBody_28(Rigidbody_t3916780224 * value)
	{
		___mTargetRigidBody_28 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetRigidBody_28), value);
	}

	inline static int32_t get_offset_of_U3CPreviousTargetU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___U3CPreviousTargetU3Ek__BackingField_29)); }
	inline Transform_t3600365921 * get_U3CPreviousTargetU3Ek__BackingField_29() const { return ___U3CPreviousTargetU3Ek__BackingField_29; }
	inline Transform_t3600365921 ** get_address_of_U3CPreviousTargetU3Ek__BackingField_29() { return &___U3CPreviousTargetU3Ek__BackingField_29; }
	inline void set_U3CPreviousTargetU3Ek__BackingField_29(Transform_t3600365921 * value)
	{
		___U3CPreviousTargetU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviousTargetU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_mHeadingPrevFrame_30() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mHeadingPrevFrame_30)); }
	inline Quaternion_t2301928331  get_mHeadingPrevFrame_30() const { return ___mHeadingPrevFrame_30; }
	inline Quaternion_t2301928331 * get_address_of_mHeadingPrevFrame_30() { return &___mHeadingPrevFrame_30; }
	inline void set_mHeadingPrevFrame_30(Quaternion_t2301928331  value)
	{
		___mHeadingPrevFrame_30 = value;
	}

	inline static int32_t get_offset_of_mOffsetPrevFrame_31() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201, ___mOffsetPrevFrame_31)); }
	inline Vector3_t3722313464  get_mOffsetPrevFrame_31() const { return ___mOffsetPrevFrame_31; }
	inline Vector3_t3722313464 * get_address_of_mOffsetPrevFrame_31() { return &___mOffsetPrevFrame_31; }
	inline void set_mOffsetPrevFrame_31(Vector3_t3722313464  value)
	{
		___mOffsetPrevFrame_31 = value;
	}
};

struct CinemachineOrbitalTransposer_t2654619201_StaticFields
{
public:
	// Cinemachine.CinemachineOrbitalTransposer/UpdateHeadingDelegate Cinemachine.CinemachineOrbitalTransposer::<>f__am$cache0
	UpdateHeadingDelegate_t3270413085 * ___U3CU3Ef__amU24cache0_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_32() { return static_cast<int32_t>(offsetof(CinemachineOrbitalTransposer_t2654619201_StaticFields, ___U3CU3Ef__amU24cache0_32)); }
	inline UpdateHeadingDelegate_t3270413085 * get_U3CU3Ef__amU24cache0_32() const { return ___U3CU3Ef__amU24cache0_32; }
	inline UpdateHeadingDelegate_t3270413085 ** get_address_of_U3CU3Ef__amU24cache0_32() { return &___U3CU3Ef__amU24cache0_32; }
	inline void set_U3CU3Ef__amU24cache0_32(UpdateHeadingDelegate_t3270413085 * value)
	{
		___U3CU3Ef__amU24cache0_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEORBITALTRANSPOSER_T2654619201_H
#ifndef CINEMACHINEGROUPCOMPOSER_T3731041519_H
#define CINEMACHINEGROUPCOMPOSER_T3731041519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.CinemachineGroupComposer
struct  CinemachineGroupComposer_t3731041519  : public CinemachineComposer_t905041075
{
public:
	// System.Single Cinemachine.CinemachineGroupComposer::m_GroupFramingSize
	float ___m_GroupFramingSize_24;
	// Cinemachine.CinemachineGroupComposer/FramingMode Cinemachine.CinemachineGroupComposer::m_FramingMode
	int32_t ___m_FramingMode_25;
	// System.Single Cinemachine.CinemachineGroupComposer::m_FrameDamping
	float ___m_FrameDamping_26;
	// Cinemachine.CinemachineGroupComposer/AdjustmentMode Cinemachine.CinemachineGroupComposer::m_AdjustmentMode
	int32_t ___m_AdjustmentMode_27;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaxDollyIn
	float ___m_MaxDollyIn_28;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaxDollyOut
	float ___m_MaxDollyOut_29;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MinimumDistance
	float ___m_MinimumDistance_30;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaximumDistance
	float ___m_MaximumDistance_31;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MinimumFOV
	float ___m_MinimumFOV_32;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaximumFOV
	float ___m_MaximumFOV_33;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MinimumOrthoSize
	float ___m_MinimumOrthoSize_34;
	// System.Single Cinemachine.CinemachineGroupComposer::m_MaximumOrthoSize
	float ___m_MaximumOrthoSize_35;
	// System.Single Cinemachine.CinemachineGroupComposer::m_prevTargetHeight
	float ___m_prevTargetHeight_36;
	// UnityEngine.Bounds Cinemachine.CinemachineGroupComposer::<m_LastBounds>k__BackingField
	Bounds_t2266837910  ___U3Cm_LastBoundsU3Ek__BackingField_37;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineGroupComposer::<m_lastBoundsMatrix>k__BackingField
	Matrix4x4_t1817901843  ___U3Cm_lastBoundsMatrixU3Ek__BackingField_38;

public:
	inline static int32_t get_offset_of_m_GroupFramingSize_24() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_GroupFramingSize_24)); }
	inline float get_m_GroupFramingSize_24() const { return ___m_GroupFramingSize_24; }
	inline float* get_address_of_m_GroupFramingSize_24() { return &___m_GroupFramingSize_24; }
	inline void set_m_GroupFramingSize_24(float value)
	{
		___m_GroupFramingSize_24 = value;
	}

	inline static int32_t get_offset_of_m_FramingMode_25() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_FramingMode_25)); }
	inline int32_t get_m_FramingMode_25() const { return ___m_FramingMode_25; }
	inline int32_t* get_address_of_m_FramingMode_25() { return &___m_FramingMode_25; }
	inline void set_m_FramingMode_25(int32_t value)
	{
		___m_FramingMode_25 = value;
	}

	inline static int32_t get_offset_of_m_FrameDamping_26() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_FrameDamping_26)); }
	inline float get_m_FrameDamping_26() const { return ___m_FrameDamping_26; }
	inline float* get_address_of_m_FrameDamping_26() { return &___m_FrameDamping_26; }
	inline void set_m_FrameDamping_26(float value)
	{
		___m_FrameDamping_26 = value;
	}

	inline static int32_t get_offset_of_m_AdjustmentMode_27() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_AdjustmentMode_27)); }
	inline int32_t get_m_AdjustmentMode_27() const { return ___m_AdjustmentMode_27; }
	inline int32_t* get_address_of_m_AdjustmentMode_27() { return &___m_AdjustmentMode_27; }
	inline void set_m_AdjustmentMode_27(int32_t value)
	{
		___m_AdjustmentMode_27 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyIn_28() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MaxDollyIn_28)); }
	inline float get_m_MaxDollyIn_28() const { return ___m_MaxDollyIn_28; }
	inline float* get_address_of_m_MaxDollyIn_28() { return &___m_MaxDollyIn_28; }
	inline void set_m_MaxDollyIn_28(float value)
	{
		___m_MaxDollyIn_28 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyOut_29() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MaxDollyOut_29)); }
	inline float get_m_MaxDollyOut_29() const { return ___m_MaxDollyOut_29; }
	inline float* get_address_of_m_MaxDollyOut_29() { return &___m_MaxDollyOut_29; }
	inline void set_m_MaxDollyOut_29(float value)
	{
		___m_MaxDollyOut_29 = value;
	}

	inline static int32_t get_offset_of_m_MinimumDistance_30() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MinimumDistance_30)); }
	inline float get_m_MinimumDistance_30() const { return ___m_MinimumDistance_30; }
	inline float* get_address_of_m_MinimumDistance_30() { return &___m_MinimumDistance_30; }
	inline void set_m_MinimumDistance_30(float value)
	{
		___m_MinimumDistance_30 = value;
	}

	inline static int32_t get_offset_of_m_MaximumDistance_31() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MaximumDistance_31)); }
	inline float get_m_MaximumDistance_31() const { return ___m_MaximumDistance_31; }
	inline float* get_address_of_m_MaximumDistance_31() { return &___m_MaximumDistance_31; }
	inline void set_m_MaximumDistance_31(float value)
	{
		___m_MaximumDistance_31 = value;
	}

	inline static int32_t get_offset_of_m_MinimumFOV_32() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MinimumFOV_32)); }
	inline float get_m_MinimumFOV_32() const { return ___m_MinimumFOV_32; }
	inline float* get_address_of_m_MinimumFOV_32() { return &___m_MinimumFOV_32; }
	inline void set_m_MinimumFOV_32(float value)
	{
		___m_MinimumFOV_32 = value;
	}

	inline static int32_t get_offset_of_m_MaximumFOV_33() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MaximumFOV_33)); }
	inline float get_m_MaximumFOV_33() const { return ___m_MaximumFOV_33; }
	inline float* get_address_of_m_MaximumFOV_33() { return &___m_MaximumFOV_33; }
	inline void set_m_MaximumFOV_33(float value)
	{
		___m_MaximumFOV_33 = value;
	}

	inline static int32_t get_offset_of_m_MinimumOrthoSize_34() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MinimumOrthoSize_34)); }
	inline float get_m_MinimumOrthoSize_34() const { return ___m_MinimumOrthoSize_34; }
	inline float* get_address_of_m_MinimumOrthoSize_34() { return &___m_MinimumOrthoSize_34; }
	inline void set_m_MinimumOrthoSize_34(float value)
	{
		___m_MinimumOrthoSize_34 = value;
	}

	inline static int32_t get_offset_of_m_MaximumOrthoSize_35() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_MaximumOrthoSize_35)); }
	inline float get_m_MaximumOrthoSize_35() const { return ___m_MaximumOrthoSize_35; }
	inline float* get_address_of_m_MaximumOrthoSize_35() { return &___m_MaximumOrthoSize_35; }
	inline void set_m_MaximumOrthoSize_35(float value)
	{
		___m_MaximumOrthoSize_35 = value;
	}

	inline static int32_t get_offset_of_m_prevTargetHeight_36() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___m_prevTargetHeight_36)); }
	inline float get_m_prevTargetHeight_36() const { return ___m_prevTargetHeight_36; }
	inline float* get_address_of_m_prevTargetHeight_36() { return &___m_prevTargetHeight_36; }
	inline void set_m_prevTargetHeight_36(float value)
	{
		___m_prevTargetHeight_36 = value;
	}

	inline static int32_t get_offset_of_U3Cm_LastBoundsU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___U3Cm_LastBoundsU3Ek__BackingField_37)); }
	inline Bounds_t2266837910  get_U3Cm_LastBoundsU3Ek__BackingField_37() const { return ___U3Cm_LastBoundsU3Ek__BackingField_37; }
	inline Bounds_t2266837910 * get_address_of_U3Cm_LastBoundsU3Ek__BackingField_37() { return &___U3Cm_LastBoundsU3Ek__BackingField_37; }
	inline void set_U3Cm_LastBoundsU3Ek__BackingField_37(Bounds_t2266837910  value)
	{
		___U3Cm_LastBoundsU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(CinemachineGroupComposer_t3731041519, ___U3Cm_lastBoundsMatrixU3Ek__BackingField_38)); }
	inline Matrix4x4_t1817901843  get_U3Cm_lastBoundsMatrixU3Ek__BackingField_38() const { return ___U3Cm_lastBoundsMatrixU3Ek__BackingField_38; }
	inline Matrix4x4_t1817901843 * get_address_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_38() { return &___U3Cm_lastBoundsMatrixU3Ek__BackingField_38; }
	inline void set_U3Cm_lastBoundsMatrixU3Ek__BackingField_38(Matrix4x4_t1817901843  value)
	{
		___U3Cm_lastBoundsMatrixU3Ek__BackingField_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINEGROUPCOMPOSER_T3731041519_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (CinemachineConfiner_t3819785210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[6] = 
{
	CinemachineConfiner_t3819785210::get_offset_of_m_ConfineMode_5(),
	CinemachineConfiner_t3819785210::get_offset_of_m_BoundingVolume_6(),
	CinemachineConfiner_t3819785210::get_offset_of_m_BoundingShape2D_7(),
	CinemachineConfiner_t3819785210::get_offset_of_m_ConfineScreenEdges_8(),
	CinemachineConfiner_t3819785210::get_offset_of_m_Damping_9(),
	CinemachineConfiner_t3819785210::get_offset_of_m_pathCache_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (Mode_t1560670659)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3901[3] = 
{
	Mode_t1560670659::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (VcamExtraState_t647499534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[2] = 
{
	VcamExtraState_t647499534::get_offset_of_m_previousDisplacement_0(),
	VcamExtraState_t647499534::get_offset_of_confinerDisplacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (CinemachineDollyCart_t1139943543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3903[5] = 
{
	CinemachineDollyCart_t1139943543::get_offset_of_m_Path_2(),
	CinemachineDollyCart_t1139943543::get_offset_of_m_UpdateMethod_3(),
	CinemachineDollyCart_t1139943543::get_offset_of_m_PositionUnits_4(),
	CinemachineDollyCart_t1139943543::get_offset_of_m_Speed_5(),
	CinemachineDollyCart_t1139943543::get_offset_of_m_Position_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (UpdateMethod_t147924144)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3904[3] = 
{
	UpdateMethod_t147924144::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (CinemachineExternalCamera_t3942460354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[4] = 
{
	CinemachineExternalCamera_t3942460354::get_offset_of_m_LookAt_16(),
	CinemachineExternalCamera_t3942460354::get_offset_of_m_Camera_17(),
	CinemachineExternalCamera_t3942460354::get_offset_of_m_State_18(),
	CinemachineExternalCamera_t3942460354::get_offset_of_U3CFollowU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (CinemachineFollowZoom_t358397207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3906[4] = 
{
	CinemachineFollowZoom_t358397207::get_offset_of_m_Width_5(),
	CinemachineFollowZoom_t358397207::get_offset_of_m_Damping_6(),
	CinemachineFollowZoom_t358397207::get_offset_of_m_MinFOV_7(),
	CinemachineFollowZoom_t358397207::get_offset_of_m_MaxFOV_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (VcamExtraState_t2436675054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3907[1] = 
{
	VcamExtraState_t2436675054::get_offset_of_m_previousFrameZoom_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (CinemachineFreeLook_t3895466944), -1, sizeof(CinemachineFreeLook_t3895466944_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3908[26] = 
{
	CinemachineFreeLook_t3895466944::get_offset_of_m_LookAt_16(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_Follow_17(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_CommonLens_18(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_Lens_19(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_YAxis_20(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_XAxis_21(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_Heading_22(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_RecenterToTargetHeading_23(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_BindingMode_24(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_SplineCurvature_25(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_Orbits_26(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_LegacyHeadingBias_27(),
	CinemachineFreeLook_t3895466944::get_offset_of_mUseLegacyRigDefinitions_28(),
	CinemachineFreeLook_t3895466944::get_offset_of_mIsDestroyed_29(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_State_30(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_Rigs_31(),
	CinemachineFreeLook_t3895466944::get_offset_of_mOrbitals_32(),
	CinemachineFreeLook_t3895466944::get_offset_of_mBlendA_33(),
	CinemachineFreeLook_t3895466944::get_offset_of_mBlendB_34(),
	CinemachineFreeLook_t3895466944_StaticFields::get_offset_of_CreateRigOverride_35(),
	CinemachineFreeLook_t3895466944_StaticFields::get_offset_of_DestroyRigOverride_36(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_CachedOrbits_37(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_CachedTension_38(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_CachedKnots_39(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_CachedCtrl1_40(),
	CinemachineFreeLook_t3895466944::get_offset_of_m_CachedCtrl2_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (Orbit_t75009320)+ sizeof (RuntimeObject), sizeof(Orbit_t75009320 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3909[2] = 
{
	Orbit_t75009320::get_offset_of_m_Height_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Orbit_t75009320::get_offset_of_m_Radius_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (CreateRigDelegate_t3127201826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (DestroyRigDelegate_t2388233918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (CinemachineMixingCamera_t2169870503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3912[15] = 
{
	0,
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight0_17(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight1_18(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight2_19(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight3_20(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight4_21(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight5_22(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight6_23(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_Weight7_24(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_State_25(),
	CinemachineMixingCamera_t2169870503::get_offset_of_U3CLiveChildU3Ek__BackingField_26(),
	CinemachineMixingCamera_t2169870503::get_offset_of_U3CLookAtU3Ek__BackingField_27(),
	CinemachineMixingCamera_t2169870503::get_offset_of_U3CFollowU3Ek__BackingField_28(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_ChildCameras_29(),
	CinemachineMixingCamera_t2169870503::get_offset_of_m_indexMap_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (CinemachinePath_t1083711280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3913[2] = 
{
	CinemachinePath_t1083711280::get_offset_of_m_Looped_10(),
	CinemachinePath_t1083711280::get_offset_of_m_Waypoints_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (Waypoint_t2405927058)+ sizeof (RuntimeObject), sizeof(Waypoint_t2405927058 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3914[3] = 
{
	Waypoint_t2405927058::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Waypoint_t2405927058::get_offset_of_tangent_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Waypoint_t2405927058::get_offset_of_roll_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (CinemachinePipeline_t1706075881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (CinemachineSmoothPath_t1336093218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3916[5] = 
{
	CinemachineSmoothPath_t1336093218::get_offset_of_m_Looped_10(),
	CinemachineSmoothPath_t1336093218::get_offset_of_m_Waypoints_11(),
	CinemachineSmoothPath_t1336093218::get_offset_of_m_ControlPoints1_12(),
	CinemachineSmoothPath_t1336093218::get_offset_of_m_ControlPoints2_13(),
	CinemachineSmoothPath_t1336093218::get_offset_of_m_IsLoopedCache_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (Waypoint_t1322198744)+ sizeof (RuntimeObject), sizeof(Waypoint_t1322198744 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3917[2] = 
{
	Waypoint_t1322198744::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Waypoint_t1322198744::get_offset_of_roll_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (CinemachineStateDrivenCamera_t60529688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3918[21] = 
{
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_LookAt_16(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_Follow_17(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_AnimatedTarget_18(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_LayerIndex_19(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_ShowDebugText_20(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_EnableAllChildCameras_21(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_ChildCameras_22(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_Instructions_23(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_DefaultBlend_24(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_CustomBlends_25(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_ParentHash_26(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_U3CLiveChildU3Ek__BackingField_27(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_State_28(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mActivationTime_29(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mActiveInstruction_30(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mPendingActivationTime_31(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mPendingInstruction_32(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mActiveBlend_33(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mInstructionDictionary_34(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_mStateParentLookup_35(),
	CinemachineStateDrivenCamera_t60529688::get_offset_of_m_clipInfoList_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (Instruction_t2121420310)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3919[4] = 
{
	Instruction_t2121420310::get_offset_of_m_FullHash_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Instruction_t2121420310::get_offset_of_m_VirtualCamera_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Instruction_t2121420310::get_offset_of_m_ActivateAfter_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Instruction_t2121420310::get_offset_of_m_MinDuration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (ParentHash_t1970338041)+ sizeof (RuntimeObject), sizeof(ParentHash_t1970338041 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3920[2] = 
{
	ParentHash_t1970338041::get_offset_of_m_Hash_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParentHash_t1970338041::get_offset_of_m_ParentHash_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (CinemachineTargetGroup_t1423332932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[5] = 
{
	CinemachineTargetGroup_t1423332932::get_offset_of_m_PositionMode_2(),
	CinemachineTargetGroup_t1423332932::get_offset_of_m_RotationMode_3(),
	CinemachineTargetGroup_t1423332932::get_offset_of_m_UpdateMethod_4(),
	CinemachineTargetGroup_t1423332932::get_offset_of_m_Targets_5(),
	CinemachineTargetGroup_t1423332932::get_offset_of_m_lastRadius_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (Target_t116854977)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[3] = 
{
	Target_t116854977::get_offset_of_target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Target_t116854977::get_offset_of_weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Target_t116854977::get_offset_of_radius_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (PositionMode_t2068418555)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3923[3] = 
{
	PositionMode_t2068418555::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (RotationMode_t1993955844)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3924[3] = 
{
	RotationMode_t1993955844::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (UpdateMethod_t1906263433)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3925[4] = 
{
	UpdateMethod_t1906263433::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (CinemachineVirtualCamera_t947248387), -1, sizeof(CinemachineVirtualCamera_t947248387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3926[11] = 
{
	CinemachineVirtualCamera_t947248387::get_offset_of_m_LookAt_16(),
	CinemachineVirtualCamera_t947248387::get_offset_of_m_Follow_17(),
	CinemachineVirtualCamera_t947248387::get_offset_of_m_Lens_18(),
	0,
	CinemachineVirtualCamera_t947248387_StaticFields::get_offset_of_CreatePipelineOverride_20(),
	CinemachineVirtualCamera_t947248387_StaticFields::get_offset_of_DestroyPipelineOverride_21(),
	CinemachineVirtualCamera_t947248387::get_offset_of_U3CUserIsDraggingU3Ek__BackingField_22(),
	CinemachineVirtualCamera_t947248387::get_offset_of_m_State_23(),
	CinemachineVirtualCamera_t947248387::get_offset_of_m_ComponentPipeline_24(),
	CinemachineVirtualCamera_t947248387::get_offset_of_m_ComponentOwner_25(),
	CinemachineVirtualCamera_t947248387_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (CreatePipelineDelegate_t3891770455), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (DestroyPipelineDelegate_t29728977), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (CinemachineBasicMultiChannelPerlin_t1881527403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3929[6] = 
{
	CinemachineBasicMultiChannelPerlin_t1881527403::get_offset_of_m_NoiseProfile_4(),
	CinemachineBasicMultiChannelPerlin_t1881527403::get_offset_of_m_AmplitudeGain_5(),
	CinemachineBasicMultiChannelPerlin_t1881527403::get_offset_of_m_FrequencyGain_6(),
	CinemachineBasicMultiChannelPerlin_t1881527403::get_offset_of_mInitialized_7(),
	CinemachineBasicMultiChannelPerlin_t1881527403::get_offset_of_mNoiseTime_8(),
	CinemachineBasicMultiChannelPerlin_t1881527403::get_offset_of_mNoiseOffsets_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (CinemachineComposer_t905041075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3930[20] = 
{
	CinemachineComposer_t905041075::get_offset_of_OnGUICallback_4(),
	CinemachineComposer_t905041075::get_offset_of_m_TrackedObjectOffset_5(),
	CinemachineComposer_t905041075::get_offset_of_m_LookaheadTime_6(),
	CinemachineComposer_t905041075::get_offset_of_m_LookaheadSmoothing_7(),
	CinemachineComposer_t905041075::get_offset_of_m_HorizontalDamping_8(),
	CinemachineComposer_t905041075::get_offset_of_m_VerticalDamping_9(),
	CinemachineComposer_t905041075::get_offset_of_m_ScreenX_10(),
	CinemachineComposer_t905041075::get_offset_of_m_ScreenY_11(),
	CinemachineComposer_t905041075::get_offset_of_m_DeadZoneWidth_12(),
	CinemachineComposer_t905041075::get_offset_of_m_DeadZoneHeight_13(),
	CinemachineComposer_t905041075::get_offset_of_m_SoftZoneWidth_14(),
	CinemachineComposer_t905041075::get_offset_of_m_SoftZoneHeight_15(),
	CinemachineComposer_t905041075::get_offset_of_m_BiasX_16(),
	CinemachineComposer_t905041075::get_offset_of_m_BiasY_17(),
	CinemachineComposer_t905041075::get_offset_of_U3CTrackedPointU3Ek__BackingField_18(),
	CinemachineComposer_t905041075::get_offset_of_m_CameraPosPrevFrame_19(),
	CinemachineComposer_t905041075::get_offset_of_m_LookAtPrevFrame_20(),
	CinemachineComposer_t905041075::get_offset_of_m_ScreenOffsetPrevFrame_21(),
	CinemachineComposer_t905041075::get_offset_of_m_CameraOrientationPrevFrame_22(),
	CinemachineComposer_t905041075::get_offset_of_m_Predictor_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (CinemachineFramingTransposer_t2273839967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3931[35] = 
{
	CinemachineFramingTransposer_t2273839967::get_offset_of_OnGUICallback_4(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_LookaheadTime_5(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_LookaheadSmoothing_6(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_XDamping_7(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_YDamping_8(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_ZDamping_9(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_ScreenX_10(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_ScreenY_11(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_CameraDistance_12(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_DeadZoneWidth_13(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_DeadZoneHeight_14(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_DeadZoneDepth_15(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_UnlimitedSoftZone_16(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_SoftZoneWidth_17(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_SoftZoneHeight_18(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_BiasX_19(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_BiasY_20(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_GroupFramingMode_21(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_AdjustmentMode_22(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_GroupFramingSize_23(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MaxDollyIn_24(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MaxDollyOut_25(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MinimumDistance_26(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MaximumDistance_27(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MinimumFOV_28(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MaximumFOV_29(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MinimumOrthoSize_30(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_MaximumOrthoSize_31(),
	0,
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_PreviousCameraPosition_33(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_Predictor_34(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_U3CTrackedPointU3Ek__BackingField_35(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_m_prevTargetHeight_36(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_U3Cm_LastBoundsU3Ek__BackingField_37(),
	CinemachineFramingTransposer_t2273839967::get_offset_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (FramingMode_t1646599265)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3932[5] = 
{
	FramingMode_t1646599265::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (AdjustmentMode_t2584965458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3933[4] = 
{
	AdjustmentMode_t2584965458::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (CinemachineGroupComposer_t3731041519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3934[15] = 
{
	CinemachineGroupComposer_t3731041519::get_offset_of_m_GroupFramingSize_24(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_FramingMode_25(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_FrameDamping_26(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_AdjustmentMode_27(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MaxDollyIn_28(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MaxDollyOut_29(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MinimumDistance_30(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MaximumDistance_31(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MinimumFOV_32(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MaximumFOV_33(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MinimumOrthoSize_34(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_MaximumOrthoSize_35(),
	CinemachineGroupComposer_t3731041519::get_offset_of_m_prevTargetHeight_36(),
	CinemachineGroupComposer_t3731041519::get_offset_of_U3Cm_LastBoundsU3Ek__BackingField_37(),
	CinemachineGroupComposer_t3731041519::get_offset_of_U3Cm_lastBoundsMatrixU3Ek__BackingField_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (FramingMode_t2640351704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3935[4] = 
{
	FramingMode_t2640351704::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (AdjustmentMode_t857859900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3936[4] = 
{
	AdjustmentMode_t857859900::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (CinemachineHardLockToTarget_t4289260043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (CinemachineHardLookAt_t3788634768), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (CinemachineOrbitalTransposer_t2654619201), -1, sizeof(CinemachineOrbitalTransposer_t2654619201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3939[17] = 
{
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_Heading_16(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_RecenterToTargetHeading_17(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_XAxis_18(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_LegacyRadius_19(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_LegacyHeightOffset_20(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_LegacyHeadingBias_21(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_m_HeadingIsSlave_22(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_HeadingUpdater_23(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mLastHeadingAxisInputTime_24(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mHeadingRecenteringVelocity_25(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mLastTargetPosition_26(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mHeadingTracker_27(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mTargetRigidBody_28(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_U3CPreviousTargetU3Ek__BackingField_29(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mHeadingPrevFrame_30(),
	CinemachineOrbitalTransposer_t2654619201::get_offset_of_mOffsetPrevFrame_31(),
	CinemachineOrbitalTransposer_t2654619201_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (Heading_t365180425)+ sizeof (RuntimeObject), sizeof(Heading_t365180425 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3940[3] = 
{
	Heading_t365180425::get_offset_of_m_HeadingDefinition_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Heading_t365180425::get_offset_of_m_VelocityFilterStrength_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Heading_t365180425::get_offset_of_m_HeadingBias_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (HeadingDefinition_t1970467155)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3941[5] = 
{
	HeadingDefinition_t1970467155::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (Recentering_t980373847)+ sizeof (RuntimeObject), sizeof(Recentering_t980373847_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3942[5] = 
{
	Recentering_t980373847::get_offset_of_m_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t980373847::get_offset_of_m_RecenterWaitTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t980373847::get_offset_of_m_RecenteringTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t980373847::get_offset_of_m_LegacyHeadingDefinition_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Recentering_t980373847::get_offset_of_m_LegacyVelocityFilterStrength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (UpdateHeadingDelegate_t3270413085), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (HeadingTracker_t85731283), -1, sizeof(HeadingTracker_t85731283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3944[9] = 
{
	HeadingTracker_t85731283::get_offset_of_mHistory_0(),
	HeadingTracker_t85731283::get_offset_of_mTop_1(),
	HeadingTracker_t85731283::get_offset_of_mBottom_2(),
	HeadingTracker_t85731283::get_offset_of_mCount_3(),
	HeadingTracker_t85731283::get_offset_of_mHeadingSum_4(),
	HeadingTracker_t85731283::get_offset_of_mWeightSum_5(),
	HeadingTracker_t85731283::get_offset_of_mWeightTime_6(),
	HeadingTracker_t85731283::get_offset_of_mLastGoodHeading_7(),
	HeadingTracker_t85731283_StaticFields::get_offset_of_mDecayExponent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (Item_t2807873350)+ sizeof (RuntimeObject), sizeof(Item_t2807873350 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3945[3] = 
{
	Item_t2807873350::get_offset_of_velocity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Item_t2807873350::get_offset_of_weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Item_t2807873350::get_offset_of_time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (CinemachinePOV_t1973872934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3946[2] = 
{
	CinemachinePOV_t1973872934::get_offset_of_m_VerticalAxis_4(),
	CinemachinePOV_t1973872934::get_offset_of_m_HorizontalAxis_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (CinemachineSameAsFollowObject_t3628312148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (CinemachineTrackedDolly_t847425305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3948[15] = 
{
	CinemachineTrackedDolly_t847425305::get_offset_of_m_Path_4(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PathPosition_5(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PositionUnits_6(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PathOffset_7(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_XDamping_8(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_YDamping_9(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_ZDamping_10(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_CameraUp_11(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PitchDamping_12(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_YawDamping_13(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_RollDamping_14(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_AutoDolly_15(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PreviousPathPosition_16(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PreviousOrientation_17(),
	CinemachineTrackedDolly_t847425305::get_offset_of_m_PreviousCameraPosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (CameraUpMode_t3833165741)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3949[6] = 
{
	CameraUpMode_t3833165741::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (AutoDolly_t722089724)+ sizeof (RuntimeObject), sizeof(AutoDolly_t722089724_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3950[4] = 
{
	AutoDolly_t722089724::get_offset_of_m_Enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoDolly_t722089724::get_offset_of_m_PositionOffset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoDolly_t722089724::get_offset_of_m_SearchRadius_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoDolly_t722089724::get_offset_of_m_SearchResolution_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (CinemachineTransposer_t3421585208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3951[12] = 
{
	CinemachineTransposer_t3421585208::get_offset_of_m_BindingMode_4(),
	CinemachineTransposer_t3421585208::get_offset_of_m_FollowOffset_5(),
	CinemachineTransposer_t3421585208::get_offset_of_m_XDamping_6(),
	CinemachineTransposer_t3421585208::get_offset_of_m_YDamping_7(),
	CinemachineTransposer_t3421585208::get_offset_of_m_ZDamping_8(),
	CinemachineTransposer_t3421585208::get_offset_of_m_PitchDamping_9(),
	CinemachineTransposer_t3421585208::get_offset_of_m_YawDamping_10(),
	CinemachineTransposer_t3421585208::get_offset_of_m_RollDamping_11(),
	CinemachineTransposer_t3421585208::get_offset_of_m_PreviousTargetPosition_12(),
	CinemachineTransposer_t3421585208::get_offset_of_m_PreviousReferenceOrientation_13(),
	CinemachineTransposer_t3421585208::get_offset_of_m_targetOrientationOnAssign_14(),
	CinemachineTransposer_t3421585208::get_offset_of_m_previousTarget_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (BindingMode_t1806219840)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3952[7] = 
{
	BindingMode_t1806219840::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (AxisState_t478218302)+ sizeof (RuntimeObject), sizeof(AxisState_t478218302_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3953[12] = 
{
	AxisState_t478218302::get_offset_of_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_m_MaxSpeed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_m_AccelTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_m_DecelTime_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_m_InputAxisName_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_m_InputAxisValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_m_InvertAxis_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_mCurrentSpeed_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_mMinValue_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_mMaxValue_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisState_t478218302::get_offset_of_mWrapAround_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (CameraState_t560734301)+ sizeof (RuntimeObject), -1, sizeof(CameraState_t560734301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3954[16] = 
{
	CameraState_t560734301::get_offset_of_U3CLensU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CReferenceUpU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301_StaticFields::get_offset_of_kNoPoint_3(),
	CameraState_t560734301::get_offset_of_U3CRawPositionU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CRawOrientationU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CPositionDampingBypassU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CShotQualityU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CPositionCorrectionU3Ek__BackingField_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3COrientationCorrectionU3Ek__BackingField_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_mCustom0_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_mCustom1_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_mCustom2_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_mCustom3_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_m_CustomOverflow_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t560734301::get_offset_of_U3CNumCustomBlendablesU3Ek__BackingField_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (CustomBlendable_t3531329220)+ sizeof (RuntimeObject), sizeof(CustomBlendable_t3531329220_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3955[2] = 
{
	CustomBlendable_t3531329220::get_offset_of_m_Custom_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomBlendable_t3531329220::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (CinemachineBlend_t1772488801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[5] = 
{
	CinemachineBlend_t1772488801::get_offset_of_U3CCamAU3Ek__BackingField_0(),
	CinemachineBlend_t1772488801::get_offset_of_U3CCamBU3Ek__BackingField_1(),
	CinemachineBlend_t1772488801::get_offset_of_U3CBlendCurveU3Ek__BackingField_2(),
	CinemachineBlend_t1772488801::get_offset_of_U3CTimeInBlendU3Ek__BackingField_3(),
	CinemachineBlend_t1772488801::get_offset_of_U3CDurationU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (CinemachineBlendDefinition_t3511030937)+ sizeof (RuntimeObject), sizeof(CinemachineBlendDefinition_t3511030937 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3957[2] = 
{
	CinemachineBlendDefinition_t3511030937::get_offset_of_m_Style_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CinemachineBlendDefinition_t3511030937::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (Style_t1056527879)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3958[8] = 
{
	Style_t1056527879::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (CinemachineBlenderSettings_t3646651286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[2] = 
{
	CinemachineBlenderSettings_t3646651286::get_offset_of_m_CustomBlends_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (CustomBlend_t84561686)+ sizeof (RuntimeObject), sizeof(CustomBlend_t84561686_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3960[3] = 
{
	CustomBlend_t84561686::get_offset_of_m_From_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomBlend_t84561686::get_offset_of_m_To_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomBlend_t84561686::get_offset_of_m_Blend_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (CinemachineComponentBase_t4122537527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3961[2] = 
{
	0,
	CinemachineComponentBase_t4122537527::get_offset_of_m_vcamOwner_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (CinemachineCore_t2342901799), -1, sizeof(CinemachineCore_t2342901799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3962[11] = 
{
	CinemachineCore_t2342901799_StaticFields::get_offset_of_kStreamingVersion_0(),
	CinemachineCore_t2342901799_StaticFields::get_offset_of_kVersionString_1(),
	CinemachineCore_t2342901799_StaticFields::get_offset_of_sInstance_2(),
	CinemachineCore_t2342901799_StaticFields::get_offset_of_sShowHiddenObjects_3(),
	CinemachineCore_t2342901799_StaticFields::get_offset_of_GetInputAxis_4(),
	CinemachineCore_t2342901799::get_offset_of_mActiveBrains_5(),
	CinemachineCore_t2342901799::get_offset_of_mActiveCameras_6(),
	CinemachineCore_t2342901799::get_offset_of_mChildCameras_7(),
	CinemachineCore_t2342901799::get_offset_of_mUpdateStatus_8(),
	CinemachineCore_t2342901799::get_offset_of_U3CCurrentUpdateFilterU3Ek__BackingField_9(),
	CinemachineCore_t2342901799_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (Stage_t3116928195)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3963[4] = 
{
	Stage_t3116928195::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (AxisInputDelegate_t365085504), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (UpdateStatus_t592875705)+ sizeof (RuntimeObject), sizeof(UpdateStatus_t592875705 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3965[9] = 
{
	0,
	UpdateStatus_t592875705::get_offset_of_lastUpdateFrame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_lastUpdateSubframe_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_windowStart_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_numWindowLateUpdateMoves_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_numWindowFixedUpdateMoves_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_numWindows_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_preferredUpdate_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateStatus_t592875705::get_offset_of_targetPos_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (UpdateFilter_t3852455381)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3966[5] = 
{
	UpdateFilter_t3852455381::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (CinemachineGameWindowDebug_t254377641), -1, sizeof(CinemachineGameWindowDebug_t254377641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3967[1] = 
{
	CinemachineGameWindowDebug_t254377641_StaticFields::get_offset_of_mClients_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (CinemachineExtension_t890367330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[3] = 
{
	0,
	CinemachineExtension_t890367330::get_offset_of_m_vcamOwner_3(),
	CinemachineExtension_t890367330::get_offset_of_mExtraState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (CinemachinePathBase_t1750211330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[8] = 
{
	CinemachinePathBase_t1750211330::get_offset_of_m_Resolution_2(),
	CinemachinePathBase_t1750211330::get_offset_of_m_Appearance_3(),
	CinemachinePathBase_t1750211330::get_offset_of_m_DistanceToPos_4(),
	CinemachinePathBase_t1750211330::get_offset_of_m_PosToDistance_5(),
	CinemachinePathBase_t1750211330::get_offset_of_m_CachedSampleSteps_6(),
	CinemachinePathBase_t1750211330::get_offset_of_m_PathLength_7(),
	CinemachinePathBase_t1750211330::get_offset_of_m_cachedPosStepSize_8(),
	CinemachinePathBase_t1750211330::get_offset_of_m_cachedDistanceStepSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (Appearance_t243588924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3970[3] = 
{
	Appearance_t243588924::get_offset_of_pathColor_0(),
	Appearance_t243588924::get_offset_of_inactivePathColor_1(),
	Appearance_t243588924::get_offset_of_width_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (PositionUnits_t2997231777)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3971[3] = 
{
	PositionUnits_t2997231777::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (LensSettingsPropertyAttribute_t289899148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (CinemachineBlendDefinitionPropertyAttribute_t1893815722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (SaveDuringPlayAttribute_t77832053), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (NoSaveDuringPlayAttribute_t350030686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (TagFieldAttribute_t2187080624), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (DocumentationSortingAttribute_t2474015552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3977[2] = 
{
	DocumentationSortingAttribute_t2474015552::get_offset_of_U3CSortOrderU3Ek__BackingField_0(),
	DocumentationSortingAttribute_t2474015552::get_offset_of_U3CCategoryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (Level_t4265333678)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3978[4] = 
{
	Level_t4265333678::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (CinemachineVirtualCameraBase_t1327958294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3979[14] = 
{
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_CinemachineGUIDebuggerCallback_2(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_ExcludedPropertiesInInspector_3(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_LockStageInInspector_4(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_ValidatingStreamVersion_5(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_OnValidateCalled_6(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_StreamingVersion_7(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_Priority_8(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_OnPostPipelineStage_9(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_previousStateIsValid_10(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_previousLookAtTarget_11(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_previousFollowTarget_12(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_mSlaveStatusUpdated_13(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_parentVcam_14(),
	CinemachineVirtualCameraBase_t1327958294::get_offset_of_m_QueuePriority_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (OnPostPipelineStageDelegate_t2437605738), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3981[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (GaussianWindow1D_Vector3_t926608352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (GaussianWindow1D_Quaternion_t451620234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (GaussianWindow1D_CameraRotation_t788833816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (LensSettings_t822254920)+ sizeof (RuntimeObject), sizeof(LensSettings_t822254920_marshaled_pinvoke), sizeof(LensSettings_t822254920_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3986[8] = 
{
	LensSettings_t822254920_StaticFields::get_offset_of_Default_0(),
	LensSettings_t822254920::get_offset_of_FieldOfView_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t822254920::get_offset_of_OrthographicSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t822254920::get_offset_of_NearClipPlane_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t822254920::get_offset_of_FarClipPlane_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t822254920::get_offset_of_Dutch_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t822254920::get_offset_of_U3COrthographicU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_t822254920::get_offset_of_U3CAspectU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (NoiseSettings_t3019862488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3987[2] = 
{
	NoiseSettings_t3019862488::get_offset_of_m_Position_2(),
	NoiseSettings_t3019862488::get_offset_of_m_Orientation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (NoiseParams_t2342455097)+ sizeof (RuntimeObject), sizeof(NoiseParams_t2342455097 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3988[2] = 
{
	NoiseParams_t2342455097::get_offset_of_Amplitude_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NoiseParams_t2342455097::get_offset_of_Frequency_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (TransformNoiseParams_t763395729)+ sizeof (RuntimeObject), sizeof(TransformNoiseParams_t763395729 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3989[3] = 
{
	TransformNoiseParams_t763395729::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformNoiseParams_t763395729::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformNoiseParams_t763395729::get_offset_of_Z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (PositionPredictor_t974033707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3990[5] = 
{
	PositionPredictor_t974033707::get_offset_of_m_Position_0(),
	0,
	PositionPredictor_t974033707::get_offset_of_mSmoothing_2(),
	PositionPredictor_t974033707::get_offset_of_m_Velocity_3(),
	PositionPredictor_t974033707::get_offset_of_m_Accel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (Damper_t1723357806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3991[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (ReflectionHelpers_t20665401), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (SplineHelpers_t505011237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (UnityVectorExtensions_t3137382101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3994[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (UnityQuaternionExtensions_t2095813971), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (UnityRectExtensions_t1109636319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (CinemachineMixer_t2688963259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3997[4] = 
{
	CinemachineMixer_t2688963259::get_offset_of_mBrain_0(),
	CinemachineMixer_t2688963259::get_offset_of_mBrainOverrideId_1(),
	CinemachineMixer_t2688963259::get_offset_of_mPlaying_2(),
	CinemachineMixer_t2688963259::get_offset_of_mLastOverrideFrame_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (CinemachineShotPlayable_t1408524235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[1] = 
{
	CinemachineShotPlayable_t1408524235::get_offset_of_VirtualCamera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (CinemachineShot_t973457530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3999[1] = 
{
	CinemachineShot_t973457530::get_offset_of_VirtualCamera_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
