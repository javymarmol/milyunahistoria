﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AudioRecordManager
struct AudioRecordManager_t3166594816;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// QuestData[]
struct QuestDataU5BU5D_t253853787;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IEnumerator_1_t4140357341;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// ListStories
struct ListStories_t748560288;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// LitJson.WriterContext
struct WriterContext_t1011093999;
// System.Collections.Generic.Stack`1<LitJson.WriterContext>
struct Stack_1_t1854483454;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// LitJson.Lexer
struct Lexer_t1514038666;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>
struct IDictionary_2_t2759509920;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t1152366808;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>
struct IDictionary_2_t1802487394;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct IDictionary_2_t3694023158;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>
struct IDictionary_2_t179515681;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>
struct IDictionary_2_t2155991415;
// LitJson.JsonWriter
struct JsonWriter_t3570089748;
// LitJson.WrapperFactory
struct WrapperFactory_t2158548929;
// LitJson.ExporterFunc
struct ExporterFunc_t1851311465;
// LitJson.ImporterFunc
struct ImporterFunc_t3630937194;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t1323790106;
// LitJson.FsmContext
struct FsmContext_t2331368794;
// System.IO.TextReader
struct TextReader_t283511965;
// LitJson.Lexer/StateHandler
struct StateHandler_t105866779;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// UIPuzzleController
struct UIPuzzleController_t2836468369;
// QuestTrigger
struct QuestTrigger_t3125687900;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_t1976548163;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Guid[]
struct GuidU5BU5D_t545550574;
// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.UInt64>
struct setFunc_1_t3007409591;
// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32>
struct setFunc_1_t1824315252;
// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Single>
struct setFunc_1_t270636273;
// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean>
struct setFunc_1_t3265624760;
// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<NAudio.Lame.DLL.MPEGMode>
struct setFunc_1_t3575232538;
// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<NAudio.Lame.DLL.VBRMode>
struct setFunc_1_t1936349779;
// NAudio.Lame.DLL.LibMp3Lame
struct LibMp3Lame_t153112877;
// NAudio.Wave.WZT.WaveFormat
struct WaveFormat_t2842237185;
// System.IO.Stream
struct Stream_t1273022909;
// NAudio.Lame.LameMP3FileWriter/ArrayUnion
struct ArrayUnion_t2417265785;
// NAudio.Lame.LameMP3FileWriter/delEncode
struct delEncode_t1434347660;
// UnityEngine.Transform
struct Transform_t3600365921;
// CharacterBehavior
struct CharacterBehavior_t2392107484;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Action`2<System.Boolean,UnityEngine.AudioClip>
struct Action_2_t3124271206;
// System.Collections.Generic.IList`1<LitJson.JsonData>
struct IList_1_t3340178190;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>
struct IDictionary_2_t4068933393;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IList_1_t1228139360;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>
struct IDictionary_2_t3680310408;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t3794335208;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Collections.Generic.List`1<NAudio.Wave.WZT.RiffChunk>
struct List_1_t1567347887;
// NAudio.Wave.WZT.WaveFileReader
struct WaveFileReader_t3404544520;
// NAudio.Lame.LameMP3FileWriter
struct LameMP3FileWriter_t44674093;
// System.Action
struct Action_t1264377477;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// LitJson.IJsonWrapper
struct IJsonWrapper_t1028825384;
// UIPuzzleToken
struct UIPuzzleToken_t1662105885;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t3219022681;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_t4000433264;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t140784555;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_t52518008;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerable_1_t1808561134;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t907029268;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_t3950300692;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t2253157129;
// QuestManager
struct QuestManager_t588401851;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// SceneChanger
struct SceneChanger_t1033871796;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// GameSettings/Question[]
struct QuestionU5BU5D_t564637768;
// PlayerData
struct PlayerData_t220878115;
// GameData
struct GameData_t415813024;
// Quest
struct Quest_t3696879532;
// DataController
struct DataController_t353634109;
// GameController
struct GameController_t2330501625;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// System.Collections.Generic.Dictionary`2<UnityEngine.AudioType,EAudioFileExtension>
struct Dictionary_2_t4114977226;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t2743564464;
// NAudio.Example.MP3EncoderWrapper
struct MP3EncoderWrapper_t279410081;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2297175928;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<EPuzzleToken>
struct List_1_t3937307605;
// System.Collections.Generic.List`1<UIPuzzleToken>
struct List_1_t3134180627;
// UIPuzzleController/PuzzleControllerEventHandler
struct PuzzleControllerEventHandler_t963335703;
// UIPuzzleInsert/PuzzleInsertEventHandler
struct PuzzleInsertEventHandler_t3824397729;
// UIPuzzleToken/PuzzleTokenEventHandler
struct PuzzleTokenEventHandler_t3807414068;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Image
struct Image_t2670269651;
// Quest[]
struct QuestU5BU5D_t1937508389;

struct Exception_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3C_AUDIORECORDWITHAUTHORIZATIONU3EC__ITERATOR0_T3774682809_H
#define U3C_AUDIORECORDWITHAUTHORIZATIONU3EC__ITERATOR0_T3774682809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecordManager/<_AudioRecordWithAuthorization>c__Iterator0
struct  U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809  : public RuntimeObject
{
public:
	// AudioRecordManager AudioRecordManager/<_AudioRecordWithAuthorization>c__Iterator0::$this
	AudioRecordManager_t3166594816 * ___U24this_0;
	// System.Object AudioRecordManager/<_AudioRecordWithAuthorization>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AudioRecordManager/<_AudioRecordWithAuthorization>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AudioRecordManager/<_AudioRecordWithAuthorization>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809, ___U24this_0)); }
	inline AudioRecordManager_t3166594816 * get_U24this_0() const { return ___U24this_0; }
	inline AudioRecordManager_t3166594816 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioRecordManager_t3166594816 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_AUDIORECORDWITHAUTHORIZATIONU3EC__ITERATOR0_T3774682809_H
#ifndef GAMEDATA_T415813024_H
#define GAMEDATA_T415813024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameData
struct  GameData_t415813024  : public RuntimeObject
{
public:
	// System.Boolean[] GameData::questCompleted
	BooleanU5BU5D_t2897418192* ___questCompleted_0;
	// QuestData[] GameData::questDatas
	QuestDataU5BU5D_t253853787* ___questDatas_1;

public:
	inline static int32_t get_offset_of_questCompleted_0() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___questCompleted_0)); }
	inline BooleanU5BU5D_t2897418192* get_questCompleted_0() const { return ___questCompleted_0; }
	inline BooleanU5BU5D_t2897418192** get_address_of_questCompleted_0() { return &___questCompleted_0; }
	inline void set_questCompleted_0(BooleanU5BU5D_t2897418192* value)
	{
		___questCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___questCompleted_0), value);
	}

	inline static int32_t get_offset_of_questDatas_1() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___questDatas_1)); }
	inline QuestDataU5BU5D_t253853787* get_questDatas_1() const { return ___questDatas_1; }
	inline QuestDataU5BU5D_t253853787** get_address_of_questDatas_1() { return &___questDatas_1; }
	inline void set_questDatas_1(QuestDataU5BU5D_t253853787* value)
	{
		___questDatas_1 = value;
		Il2CppCodeGenWriteBarrier((&___questDatas_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEDATA_T415813024_H
#ifndef ORDEREDDICTIONARYENUMERATOR_T386339177_H
#define ORDEREDDICTIONARYENUMERATOR_T386339177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.OrderedDictionaryEnumerator
struct  OrderedDictionaryEnumerator_t386339177  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.OrderedDictionaryEnumerator::list_enumerator
	RuntimeObject* ___list_enumerator_0;

public:
	inline static int32_t get_offset_of_list_enumerator_0() { return static_cast<int32_t>(offsetof(OrderedDictionaryEnumerator_t386339177, ___list_enumerator_0)); }
	inline RuntimeObject* get_list_enumerator_0() const { return ___list_enumerator_0; }
	inline RuntimeObject** get_address_of_list_enumerator_0() { return &___list_enumerator_0; }
	inline void set_list_enumerator_0(RuntimeObject* value)
	{
		___list_enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDEREDDICTIONARYENUMERATOR_T386339177_H
#ifndef QUESTDATA_T1216399150_H
#define QUESTDATA_T1216399150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestData
struct  QuestData_t1216399150  : public RuntimeObject
{
public:
	// System.Int32 QuestData::optionSelected
	int32_t ___optionSelected_0;
	// System.Boolean[] QuestData::storiesCompleted
	BooleanU5BU5D_t2897418192* ___storiesCompleted_1;

public:
	inline static int32_t get_offset_of_optionSelected_0() { return static_cast<int32_t>(offsetof(QuestData_t1216399150, ___optionSelected_0)); }
	inline int32_t get_optionSelected_0() const { return ___optionSelected_0; }
	inline int32_t* get_address_of_optionSelected_0() { return &___optionSelected_0; }
	inline void set_optionSelected_0(int32_t value)
	{
		___optionSelected_0 = value;
	}

	inline static int32_t get_offset_of_storiesCompleted_1() { return static_cast<int32_t>(offsetof(QuestData_t1216399150, ___storiesCompleted_1)); }
	inline BooleanU5BU5D_t2897418192* get_storiesCompleted_1() const { return ___storiesCompleted_1; }
	inline BooleanU5BU5D_t2897418192** get_address_of_storiesCompleted_1() { return &___storiesCompleted_1; }
	inline void set_storiesCompleted_1(BooleanU5BU5D_t2897418192* value)
	{
		___storiesCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___storiesCompleted_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTDATA_T1216399150_H
#ifndef QUESTION_T1252140549_H
#define QUESTION_T1252140549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSettings/Question
struct  Question_t1252140549  : public RuntimeObject
{
public:
	// System.String GameSettings/Question::type
	String_t* ___type_0;
	// System.Int32 GameSettings/Question::index
	int32_t ___index_1;
	// System.String GameSettings/Question::text
	String_t* ___text_2;
	// System.String[] GameSettings/Question::options
	StringU5BU5D_t1281789340* ___options_3;
	// System.String GameSettings/Question::answer
	String_t* ___answer_4;
	// System.String GameSettings/Question::responseAnswerWrong
	String_t* ___responseAnswerWrong_5;
	// System.String GameSettings/Question::responseAnsweRight
	String_t* ___responseAnsweRight_6;
	// System.Int32 GameSettings/Question::answerRight
	int32_t ___answerRight_7;
	// System.Boolean GameSettings/Question::completed
	bool ___completed_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___type_0)); }
	inline String_t* get_type_0() const { return ___type_0; }
	inline String_t** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(String_t* value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_options_3() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___options_3)); }
	inline StringU5BU5D_t1281789340* get_options_3() const { return ___options_3; }
	inline StringU5BU5D_t1281789340** get_address_of_options_3() { return &___options_3; }
	inline void set_options_3(StringU5BU5D_t1281789340* value)
	{
		___options_3 = value;
		Il2CppCodeGenWriteBarrier((&___options_3), value);
	}

	inline static int32_t get_offset_of_answer_4() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___answer_4)); }
	inline String_t* get_answer_4() const { return ___answer_4; }
	inline String_t** get_address_of_answer_4() { return &___answer_4; }
	inline void set_answer_4(String_t* value)
	{
		___answer_4 = value;
		Il2CppCodeGenWriteBarrier((&___answer_4), value);
	}

	inline static int32_t get_offset_of_responseAnswerWrong_5() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___responseAnswerWrong_5)); }
	inline String_t* get_responseAnswerWrong_5() const { return ___responseAnswerWrong_5; }
	inline String_t** get_address_of_responseAnswerWrong_5() { return &___responseAnswerWrong_5; }
	inline void set_responseAnswerWrong_5(String_t* value)
	{
		___responseAnswerWrong_5 = value;
		Il2CppCodeGenWriteBarrier((&___responseAnswerWrong_5), value);
	}

	inline static int32_t get_offset_of_responseAnsweRight_6() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___responseAnsweRight_6)); }
	inline String_t* get_responseAnsweRight_6() const { return ___responseAnsweRight_6; }
	inline String_t** get_address_of_responseAnsweRight_6() { return &___responseAnsweRight_6; }
	inline void set_responseAnsweRight_6(String_t* value)
	{
		___responseAnsweRight_6 = value;
		Il2CppCodeGenWriteBarrier((&___responseAnsweRight_6), value);
	}

	inline static int32_t get_offset_of_answerRight_7() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___answerRight_7)); }
	inline int32_t get_answerRight_7() const { return ___answerRight_7; }
	inline int32_t* get_address_of_answerRight_7() { return &___answerRight_7; }
	inline void set_answerRight_7(int32_t value)
	{
		___answerRight_7 = value;
	}

	inline static int32_t get_offset_of_completed_8() { return static_cast<int32_t>(offsetof(Question_t1252140549, ___completed_8)); }
	inline bool get_completed_8() const { return ___completed_8; }
	inline bool* get_address_of_completed_8() { return &___completed_8; }
	inline void set_completed_8(bool value)
	{
		___completed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTION_T1252140549_H
#ifndef SYSTEMHELPERS_T2322995976_H
#define SYSTEMHELPERS_T2322995976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SystemHelpers
struct  SystemHelpers_t2322995976  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMHELPERS_T2322995976_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T2788251243_H
#define U3CSTARTU3EC__ANONSTOREY0_T2788251243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ListStories/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t2788251243  : public RuntimeObject
{
public:
	// System.String ListStories/<Start>c__AnonStorey0::nameStory
	String_t* ___nameStory_0;
	// ListStories ListStories/<Start>c__AnonStorey0::$this
	ListStories_t748560288 * ___U24this_1;

public:
	inline static int32_t get_offset_of_nameStory_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t2788251243, ___nameStory_0)); }
	inline String_t* get_nameStory_0() const { return ___nameStory_0; }
	inline String_t** get_address_of_nameStory_0() { return &___nameStory_0; }
	inline void set_nameStory_0(String_t* value)
	{
		___nameStory_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameStory_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t2788251243, ___U24this_1)); }
	inline ListStories_t748560288 * get_U24this_1() const { return ___U24this_1; }
	inline ListStories_t748560288 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ListStories_t748560288 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T2788251243_H
#ifndef WRITERCONTEXT_T1011093999_H
#define WRITERCONTEXT_T1011093999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.WriterContext
struct  WriterContext_t1011093999  : public RuntimeObject
{
public:
	// System.Int32 LitJson.WriterContext::Count
	int32_t ___Count_0;
	// System.Boolean LitJson.WriterContext::InArray
	bool ___InArray_1;
	// System.Boolean LitJson.WriterContext::InObject
	bool ___InObject_2;
	// System.Boolean LitJson.WriterContext::ExpectingValue
	bool ___ExpectingValue_3;
	// System.Int32 LitJson.WriterContext::Padding
	int32_t ___Padding_4;

public:
	inline static int32_t get_offset_of_Count_0() { return static_cast<int32_t>(offsetof(WriterContext_t1011093999, ___Count_0)); }
	inline int32_t get_Count_0() const { return ___Count_0; }
	inline int32_t* get_address_of_Count_0() { return &___Count_0; }
	inline void set_Count_0(int32_t value)
	{
		___Count_0 = value;
	}

	inline static int32_t get_offset_of_InArray_1() { return static_cast<int32_t>(offsetof(WriterContext_t1011093999, ___InArray_1)); }
	inline bool get_InArray_1() const { return ___InArray_1; }
	inline bool* get_address_of_InArray_1() { return &___InArray_1; }
	inline void set_InArray_1(bool value)
	{
		___InArray_1 = value;
	}

	inline static int32_t get_offset_of_InObject_2() { return static_cast<int32_t>(offsetof(WriterContext_t1011093999, ___InObject_2)); }
	inline bool get_InObject_2() const { return ___InObject_2; }
	inline bool* get_address_of_InObject_2() { return &___InObject_2; }
	inline void set_InObject_2(bool value)
	{
		___InObject_2 = value;
	}

	inline static int32_t get_offset_of_ExpectingValue_3() { return static_cast<int32_t>(offsetof(WriterContext_t1011093999, ___ExpectingValue_3)); }
	inline bool get_ExpectingValue_3() const { return ___ExpectingValue_3; }
	inline bool* get_address_of_ExpectingValue_3() { return &___ExpectingValue_3; }
	inline void set_ExpectingValue_3(bool value)
	{
		___ExpectingValue_3 = value;
	}

	inline static int32_t get_offset_of_Padding_4() { return static_cast<int32_t>(offsetof(WriterContext_t1011093999, ___Padding_4)); }
	inline int32_t get_Padding_4() const { return ___Padding_4; }
	inline int32_t* get_address_of_Padding_4() { return &___Padding_4; }
	inline void set_Padding_4(int32_t value)
	{
		___Padding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITERCONTEXT_T1011093999_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	ServerIdentity_t2342208608 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	ServerIdentity_t2342208608 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef JSONWRITER_T3570089748_H
#define JSONWRITER_T3570089748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonWriter
struct  JsonWriter_t3570089748  : public RuntimeObject
{
public:
	// LitJson.WriterContext LitJson.JsonWriter::context
	WriterContext_t1011093999 * ___context_1;
	// System.Collections.Generic.Stack`1<LitJson.WriterContext> LitJson.JsonWriter::ctx_stack
	Stack_1_t1854483454 * ___ctx_stack_2;
	// System.Boolean LitJson.JsonWriter::has_reached_end
	bool ___has_reached_end_3;
	// System.Char[] LitJson.JsonWriter::hex_seq
	CharU5BU5D_t3528271667* ___hex_seq_4;
	// System.Int32 LitJson.JsonWriter::indentation
	int32_t ___indentation_5;
	// System.Int32 LitJson.JsonWriter::indent_value
	int32_t ___indent_value_6;
	// System.Text.StringBuilder LitJson.JsonWriter::inst_string_builder
	StringBuilder_t * ___inst_string_builder_7;
	// System.Boolean LitJson.JsonWriter::pretty_print
	bool ___pretty_print_8;
	// System.Boolean LitJson.JsonWriter::validate
	bool ___validate_9;
	// System.Boolean LitJson.JsonWriter::lower_case_properties
	bool ___lower_case_properties_10;
	// System.IO.TextWriter LitJson.JsonWriter::writer
	TextWriter_t3478189236 * ___writer_11;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___context_1)); }
	inline WriterContext_t1011093999 * get_context_1() const { return ___context_1; }
	inline WriterContext_t1011093999 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(WriterContext_t1011093999 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_ctx_stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___ctx_stack_2)); }
	inline Stack_1_t1854483454 * get_ctx_stack_2() const { return ___ctx_stack_2; }
	inline Stack_1_t1854483454 ** get_address_of_ctx_stack_2() { return &___ctx_stack_2; }
	inline void set_ctx_stack_2(Stack_1_t1854483454 * value)
	{
		___ctx_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_stack_2), value);
	}

	inline static int32_t get_offset_of_has_reached_end_3() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___has_reached_end_3)); }
	inline bool get_has_reached_end_3() const { return ___has_reached_end_3; }
	inline bool* get_address_of_has_reached_end_3() { return &___has_reached_end_3; }
	inline void set_has_reached_end_3(bool value)
	{
		___has_reached_end_3 = value;
	}

	inline static int32_t get_offset_of_hex_seq_4() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___hex_seq_4)); }
	inline CharU5BU5D_t3528271667* get_hex_seq_4() const { return ___hex_seq_4; }
	inline CharU5BU5D_t3528271667** get_address_of_hex_seq_4() { return &___hex_seq_4; }
	inline void set_hex_seq_4(CharU5BU5D_t3528271667* value)
	{
		___hex_seq_4 = value;
		Il2CppCodeGenWriteBarrier((&___hex_seq_4), value);
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indent_value_6() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___indent_value_6)); }
	inline int32_t get_indent_value_6() const { return ___indent_value_6; }
	inline int32_t* get_address_of_indent_value_6() { return &___indent_value_6; }
	inline void set_indent_value_6(int32_t value)
	{
		___indent_value_6 = value;
	}

	inline static int32_t get_offset_of_inst_string_builder_7() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___inst_string_builder_7)); }
	inline StringBuilder_t * get_inst_string_builder_7() const { return ___inst_string_builder_7; }
	inline StringBuilder_t ** get_address_of_inst_string_builder_7() { return &___inst_string_builder_7; }
	inline void set_inst_string_builder_7(StringBuilder_t * value)
	{
		___inst_string_builder_7 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_builder_7), value);
	}

	inline static int32_t get_offset_of_pretty_print_8() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___pretty_print_8)); }
	inline bool get_pretty_print_8() const { return ___pretty_print_8; }
	inline bool* get_address_of_pretty_print_8() { return &___pretty_print_8; }
	inline void set_pretty_print_8(bool value)
	{
		___pretty_print_8 = value;
	}

	inline static int32_t get_offset_of_validate_9() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___validate_9)); }
	inline bool get_validate_9() const { return ___validate_9; }
	inline bool* get_address_of_validate_9() { return &___validate_9; }
	inline void set_validate_9(bool value)
	{
		___validate_9 = value;
	}

	inline static int32_t get_offset_of_lower_case_properties_10() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___lower_case_properties_10)); }
	inline bool get_lower_case_properties_10() const { return ___lower_case_properties_10; }
	inline bool* get_address_of_lower_case_properties_10() { return &___lower_case_properties_10; }
	inline void set_lower_case_properties_10(bool value)
	{
		___lower_case_properties_10 = value;
	}

	inline static int32_t get_offset_of_writer_11() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748, ___writer_11)); }
	inline TextWriter_t3478189236 * get_writer_11() const { return ___writer_11; }
	inline TextWriter_t3478189236 ** get_address_of_writer_11() { return &___writer_11; }
	inline void set_writer_11(TextWriter_t3478189236 * value)
	{
		___writer_11 = value;
		Il2CppCodeGenWriteBarrier((&___writer_11), value);
	}
};

struct JsonWriter_t3570089748_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo LitJson.JsonWriter::number_format
	NumberFormatInfo_t435877138 * ___number_format_0;

public:
	inline static int32_t get_offset_of_number_format_0() { return static_cast<int32_t>(offsetof(JsonWriter_t3570089748_StaticFields, ___number_format_0)); }
	inline NumberFormatInfo_t435877138 * get_number_format_0() const { return ___number_format_0; }
	inline NumberFormatInfo_t435877138 ** get_address_of_number_format_0() { return &___number_format_0; }
	inline void set_number_format_0(NumberFormatInfo_t435877138 * value)
	{
		___number_format_0 = value;
		Il2CppCodeGenWriteBarrier((&___number_format_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T3570089748_H
#ifndef FSMCONTEXT_T2331368794_H
#define FSMCONTEXT_T2331368794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.FsmContext
struct  FsmContext_t2331368794  : public RuntimeObject
{
public:
	// System.Boolean LitJson.FsmContext::Return
	bool ___Return_0;
	// System.Int32 LitJson.FsmContext::NextState
	int32_t ___NextState_1;
	// LitJson.Lexer LitJson.FsmContext::L
	Lexer_t1514038666 * ___L_2;
	// System.Int32 LitJson.FsmContext::StateStack
	int32_t ___StateStack_3;

public:
	inline static int32_t get_offset_of_Return_0() { return static_cast<int32_t>(offsetof(FsmContext_t2331368794, ___Return_0)); }
	inline bool get_Return_0() const { return ___Return_0; }
	inline bool* get_address_of_Return_0() { return &___Return_0; }
	inline void set_Return_0(bool value)
	{
		___Return_0 = value;
	}

	inline static int32_t get_offset_of_NextState_1() { return static_cast<int32_t>(offsetof(FsmContext_t2331368794, ___NextState_1)); }
	inline int32_t get_NextState_1() const { return ___NextState_1; }
	inline int32_t* get_address_of_NextState_1() { return &___NextState_1; }
	inline void set_NextState_1(int32_t value)
	{
		___NextState_1 = value;
	}

	inline static int32_t get_offset_of_L_2() { return static_cast<int32_t>(offsetof(FsmContext_t2331368794, ___L_2)); }
	inline Lexer_t1514038666 * get_L_2() const { return ___L_2; }
	inline Lexer_t1514038666 ** get_address_of_L_2() { return &___L_2; }
	inline void set_L_2(Lexer_t1514038666 * value)
	{
		___L_2 = value;
		Il2CppCodeGenWriteBarrier((&___L_2), value);
	}

	inline static int32_t get_offset_of_StateStack_3() { return static_cast<int32_t>(offsetof(FsmContext_t2331368794, ___StateStack_3)); }
	inline int32_t get_StateStack_3() const { return ___StateStack_3; }
	inline int32_t* get_address_of_StateStack_3() { return &___StateStack_3; }
	inline void set_StateStack_3(int32_t value)
	{
		___StateStack_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMCONTEXT_T2331368794_H
#ifndef JSONMOCKWRAPPER_T82875095_H
#define JSONMOCKWRAPPER_T82875095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMockWrapper
struct  JsonMockWrapper_t82875095  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONMOCKWRAPPER_T82875095_H
#ifndef JSONMAPPER_T3815285241_H
#define JSONMAPPER_T3815285241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper
struct  JsonMapper_t3815285241  : public RuntimeObject
{
public:

public:
};

struct JsonMapper_t3815285241_StaticFields
{
public:
	// System.Int32 LitJson.JsonMapper::max_nesting_depth
	int32_t ___max_nesting_depth_0;
	// System.IFormatProvider LitJson.JsonMapper::datetime_format
	RuntimeObject* ___datetime_format_1;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::base_exporters_table
	RuntimeObject* ___base_exporters_table_2;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::custom_exporters_table
	RuntimeObject* ___custom_exporters_table_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::base_importers_table
	RuntimeObject* ___base_importers_table_4;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::custom_importers_table
	RuntimeObject* ___custom_importers_table_5;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata> LitJson.JsonMapper::array_metadata
	RuntimeObject* ___array_metadata_6;
	// System.Object LitJson.JsonMapper::array_metadata_lock
	RuntimeObject * ___array_metadata_lock_7;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>> LitJson.JsonMapper::conv_ops
	RuntimeObject* ___conv_ops_8;
	// System.Object LitJson.JsonMapper::conv_ops_lock
	RuntimeObject * ___conv_ops_lock_9;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata> LitJson.JsonMapper::object_metadata
	RuntimeObject* ___object_metadata_10;
	// System.Object LitJson.JsonMapper::object_metadata_lock
	RuntimeObject * ___object_metadata_lock_11;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>> LitJson.JsonMapper::type_properties
	RuntimeObject* ___type_properties_12;
	// System.Object LitJson.JsonMapper::type_properties_lock
	RuntimeObject * ___type_properties_lock_13;
	// LitJson.JsonWriter LitJson.JsonMapper::static_writer
	JsonWriter_t3570089748 * ___static_writer_14;
	// System.Object LitJson.JsonMapper::static_writer_lock
	RuntimeObject * ___static_writer_lock_15;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache0
	WrapperFactory_t2158548929 * ___U3CU3Ef__amU24cache0_16;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache1
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache1_17;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache2
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache2_18;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache3
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache3_19;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache4
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache4_20;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache5
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache5_21;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache6
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache6_22;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache7
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache7_23;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache8
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache8_24;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache9
	ExporterFunc_t1851311465 * ___U3CU3Ef__amU24cache9_25;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cacheA
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cacheA_26;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cacheB
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cacheB_27;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cacheC
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cacheC_28;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cacheD
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cacheD_29;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cacheE
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cacheE_30;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cacheF
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cacheF_31;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache10
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache10_32;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache11
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache11_33;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache12
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache12_34;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache13
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache13_35;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache14
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache14_36;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache15
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache15_37;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache16
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache16_38;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache17
	ImporterFunc_t3630937194 * ___U3CU3Ef__amU24cache17_39;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache18
	WrapperFactory_t2158548929 * ___U3CU3Ef__amU24cache18_40;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache19
	WrapperFactory_t2158548929 * ___U3CU3Ef__amU24cache19_41;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache1A
	WrapperFactory_t2158548929 * ___U3CU3Ef__amU24cache1A_42;

public:
	inline static int32_t get_offset_of_max_nesting_depth_0() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___max_nesting_depth_0)); }
	inline int32_t get_max_nesting_depth_0() const { return ___max_nesting_depth_0; }
	inline int32_t* get_address_of_max_nesting_depth_0() { return &___max_nesting_depth_0; }
	inline void set_max_nesting_depth_0(int32_t value)
	{
		___max_nesting_depth_0 = value;
	}

	inline static int32_t get_offset_of_datetime_format_1() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___datetime_format_1)); }
	inline RuntimeObject* get_datetime_format_1() const { return ___datetime_format_1; }
	inline RuntimeObject** get_address_of_datetime_format_1() { return &___datetime_format_1; }
	inline void set_datetime_format_1(RuntimeObject* value)
	{
		___datetime_format_1 = value;
		Il2CppCodeGenWriteBarrier((&___datetime_format_1), value);
	}

	inline static int32_t get_offset_of_base_exporters_table_2() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___base_exporters_table_2)); }
	inline RuntimeObject* get_base_exporters_table_2() const { return ___base_exporters_table_2; }
	inline RuntimeObject** get_address_of_base_exporters_table_2() { return &___base_exporters_table_2; }
	inline void set_base_exporters_table_2(RuntimeObject* value)
	{
		___base_exporters_table_2 = value;
		Il2CppCodeGenWriteBarrier((&___base_exporters_table_2), value);
	}

	inline static int32_t get_offset_of_custom_exporters_table_3() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___custom_exporters_table_3)); }
	inline RuntimeObject* get_custom_exporters_table_3() const { return ___custom_exporters_table_3; }
	inline RuntimeObject** get_address_of_custom_exporters_table_3() { return &___custom_exporters_table_3; }
	inline void set_custom_exporters_table_3(RuntimeObject* value)
	{
		___custom_exporters_table_3 = value;
		Il2CppCodeGenWriteBarrier((&___custom_exporters_table_3), value);
	}

	inline static int32_t get_offset_of_base_importers_table_4() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___base_importers_table_4)); }
	inline RuntimeObject* get_base_importers_table_4() const { return ___base_importers_table_4; }
	inline RuntimeObject** get_address_of_base_importers_table_4() { return &___base_importers_table_4; }
	inline void set_base_importers_table_4(RuntimeObject* value)
	{
		___base_importers_table_4 = value;
		Il2CppCodeGenWriteBarrier((&___base_importers_table_4), value);
	}

	inline static int32_t get_offset_of_custom_importers_table_5() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___custom_importers_table_5)); }
	inline RuntimeObject* get_custom_importers_table_5() const { return ___custom_importers_table_5; }
	inline RuntimeObject** get_address_of_custom_importers_table_5() { return &___custom_importers_table_5; }
	inline void set_custom_importers_table_5(RuntimeObject* value)
	{
		___custom_importers_table_5 = value;
		Il2CppCodeGenWriteBarrier((&___custom_importers_table_5), value);
	}

	inline static int32_t get_offset_of_array_metadata_6() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___array_metadata_6)); }
	inline RuntimeObject* get_array_metadata_6() const { return ___array_metadata_6; }
	inline RuntimeObject** get_address_of_array_metadata_6() { return &___array_metadata_6; }
	inline void set_array_metadata_6(RuntimeObject* value)
	{
		___array_metadata_6 = value;
		Il2CppCodeGenWriteBarrier((&___array_metadata_6), value);
	}

	inline static int32_t get_offset_of_array_metadata_lock_7() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___array_metadata_lock_7)); }
	inline RuntimeObject * get_array_metadata_lock_7() const { return ___array_metadata_lock_7; }
	inline RuntimeObject ** get_address_of_array_metadata_lock_7() { return &___array_metadata_lock_7; }
	inline void set_array_metadata_lock_7(RuntimeObject * value)
	{
		___array_metadata_lock_7 = value;
		Il2CppCodeGenWriteBarrier((&___array_metadata_lock_7), value);
	}

	inline static int32_t get_offset_of_conv_ops_8() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___conv_ops_8)); }
	inline RuntimeObject* get_conv_ops_8() const { return ___conv_ops_8; }
	inline RuntimeObject** get_address_of_conv_ops_8() { return &___conv_ops_8; }
	inline void set_conv_ops_8(RuntimeObject* value)
	{
		___conv_ops_8 = value;
		Il2CppCodeGenWriteBarrier((&___conv_ops_8), value);
	}

	inline static int32_t get_offset_of_conv_ops_lock_9() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___conv_ops_lock_9)); }
	inline RuntimeObject * get_conv_ops_lock_9() const { return ___conv_ops_lock_9; }
	inline RuntimeObject ** get_address_of_conv_ops_lock_9() { return &___conv_ops_lock_9; }
	inline void set_conv_ops_lock_9(RuntimeObject * value)
	{
		___conv_ops_lock_9 = value;
		Il2CppCodeGenWriteBarrier((&___conv_ops_lock_9), value);
	}

	inline static int32_t get_offset_of_object_metadata_10() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___object_metadata_10)); }
	inline RuntimeObject* get_object_metadata_10() const { return ___object_metadata_10; }
	inline RuntimeObject** get_address_of_object_metadata_10() { return &___object_metadata_10; }
	inline void set_object_metadata_10(RuntimeObject* value)
	{
		___object_metadata_10 = value;
		Il2CppCodeGenWriteBarrier((&___object_metadata_10), value);
	}

	inline static int32_t get_offset_of_object_metadata_lock_11() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___object_metadata_lock_11)); }
	inline RuntimeObject * get_object_metadata_lock_11() const { return ___object_metadata_lock_11; }
	inline RuntimeObject ** get_address_of_object_metadata_lock_11() { return &___object_metadata_lock_11; }
	inline void set_object_metadata_lock_11(RuntimeObject * value)
	{
		___object_metadata_lock_11 = value;
		Il2CppCodeGenWriteBarrier((&___object_metadata_lock_11), value);
	}

	inline static int32_t get_offset_of_type_properties_12() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___type_properties_12)); }
	inline RuntimeObject* get_type_properties_12() const { return ___type_properties_12; }
	inline RuntimeObject** get_address_of_type_properties_12() { return &___type_properties_12; }
	inline void set_type_properties_12(RuntimeObject* value)
	{
		___type_properties_12 = value;
		Il2CppCodeGenWriteBarrier((&___type_properties_12), value);
	}

	inline static int32_t get_offset_of_type_properties_lock_13() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___type_properties_lock_13)); }
	inline RuntimeObject * get_type_properties_lock_13() const { return ___type_properties_lock_13; }
	inline RuntimeObject ** get_address_of_type_properties_lock_13() { return &___type_properties_lock_13; }
	inline void set_type_properties_lock_13(RuntimeObject * value)
	{
		___type_properties_lock_13 = value;
		Il2CppCodeGenWriteBarrier((&___type_properties_lock_13), value);
	}

	inline static int32_t get_offset_of_static_writer_14() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___static_writer_14)); }
	inline JsonWriter_t3570089748 * get_static_writer_14() const { return ___static_writer_14; }
	inline JsonWriter_t3570089748 ** get_address_of_static_writer_14() { return &___static_writer_14; }
	inline void set_static_writer_14(JsonWriter_t3570089748 * value)
	{
		___static_writer_14 = value;
		Il2CppCodeGenWriteBarrier((&___static_writer_14), value);
	}

	inline static int32_t get_offset_of_static_writer_lock_15() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___static_writer_lock_15)); }
	inline RuntimeObject * get_static_writer_lock_15() const { return ___static_writer_lock_15; }
	inline RuntimeObject ** get_address_of_static_writer_lock_15() { return &___static_writer_lock_15; }
	inline void set_static_writer_lock_15(RuntimeObject * value)
	{
		___static_writer_lock_15 = value;
		Il2CppCodeGenWriteBarrier((&___static_writer_lock_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_16() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache0_16)); }
	inline WrapperFactory_t2158548929 * get_U3CU3Ef__amU24cache0_16() const { return ___U3CU3Ef__amU24cache0_16; }
	inline WrapperFactory_t2158548929 ** get_address_of_U3CU3Ef__amU24cache0_16() { return &___U3CU3Ef__amU24cache0_16; }
	inline void set_U3CU3Ef__amU24cache0_16(WrapperFactory_t2158548929 * value)
	{
		___U3CU3Ef__amU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_17() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache1_17)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache1_17() const { return ___U3CU3Ef__amU24cache1_17; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache1_17() { return &___U3CU3Ef__amU24cache1_17; }
	inline void set_U3CU3Ef__amU24cache1_17(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache1_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_18() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache2_18)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache2_18() const { return ___U3CU3Ef__amU24cache2_18; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache2_18() { return &___U3CU3Ef__amU24cache2_18; }
	inline void set_U3CU3Ef__amU24cache2_18(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache2_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_19() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache3_19)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache3_19() const { return ___U3CU3Ef__amU24cache3_19; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache3_19() { return &___U3CU3Ef__amU24cache3_19; }
	inline void set_U3CU3Ef__amU24cache3_19(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache3_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_20() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache4_20)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache4_20() const { return ___U3CU3Ef__amU24cache4_20; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache4_20() { return &___U3CU3Ef__amU24cache4_20; }
	inline void set_U3CU3Ef__amU24cache4_20(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache4_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_21() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache5_21)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache5_21() const { return ___U3CU3Ef__amU24cache5_21; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache5_21() { return &___U3CU3Ef__amU24cache5_21; }
	inline void set_U3CU3Ef__amU24cache5_21(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache5_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_22() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache6_22)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache6_22() const { return ___U3CU3Ef__amU24cache6_22; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache6_22() { return &___U3CU3Ef__amU24cache6_22; }
	inline void set_U3CU3Ef__amU24cache6_22(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache6_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_23() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache7_23)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache7_23() const { return ___U3CU3Ef__amU24cache7_23; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache7_23() { return &___U3CU3Ef__amU24cache7_23; }
	inline void set_U3CU3Ef__amU24cache7_23(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache7_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_24() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache8_24)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache8_24() const { return ___U3CU3Ef__amU24cache8_24; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache8_24() { return &___U3CU3Ef__amU24cache8_24; }
	inline void set_U3CU3Ef__amU24cache8_24(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache8_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache8_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_25() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache9_25)); }
	inline ExporterFunc_t1851311465 * get_U3CU3Ef__amU24cache9_25() const { return ___U3CU3Ef__amU24cache9_25; }
	inline ExporterFunc_t1851311465 ** get_address_of_U3CU3Ef__amU24cache9_25() { return &___U3CU3Ef__amU24cache9_25; }
	inline void set_U3CU3Ef__amU24cache9_25(ExporterFunc_t1851311465 * value)
	{
		___U3CU3Ef__amU24cache9_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache9_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_26() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cacheA_26)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cacheA_26() const { return ___U3CU3Ef__amU24cacheA_26; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cacheA_26() { return &___U3CU3Ef__amU24cacheA_26; }
	inline void set_U3CU3Ef__amU24cacheA_26(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cacheA_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheA_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_27() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cacheB_27)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cacheB_27() const { return ___U3CU3Ef__amU24cacheB_27; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cacheB_27() { return &___U3CU3Ef__amU24cacheB_27; }
	inline void set_U3CU3Ef__amU24cacheB_27(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cacheB_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_28() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cacheC_28)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cacheC_28() const { return ___U3CU3Ef__amU24cacheC_28; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cacheC_28() { return &___U3CU3Ef__amU24cacheC_28; }
	inline void set_U3CU3Ef__amU24cacheC_28(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cacheC_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheC_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_29() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cacheD_29)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cacheD_29() const { return ___U3CU3Ef__amU24cacheD_29; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cacheD_29() { return &___U3CU3Ef__amU24cacheD_29; }
	inline void set_U3CU3Ef__amU24cacheD_29(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cacheD_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheD_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_30() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cacheE_30)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cacheE_30() const { return ___U3CU3Ef__amU24cacheE_30; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cacheE_30() { return &___U3CU3Ef__amU24cacheE_30; }
	inline void set_U3CU3Ef__amU24cacheE_30(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cacheE_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheE_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_31() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cacheF_31)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cacheF_31() const { return ___U3CU3Ef__amU24cacheF_31; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cacheF_31() { return &___U3CU3Ef__amU24cacheF_31; }
	inline void set_U3CU3Ef__amU24cacheF_31(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cacheF_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheF_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_32() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache10_32)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache10_32() const { return ___U3CU3Ef__amU24cache10_32; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache10_32() { return &___U3CU3Ef__amU24cache10_32; }
	inline void set_U3CU3Ef__amU24cache10_32(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache10_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache10_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_33() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache11_33)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache11_33() const { return ___U3CU3Ef__amU24cache11_33; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache11_33() { return &___U3CU3Ef__amU24cache11_33; }
	inline void set_U3CU3Ef__amU24cache11_33(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache11_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache11_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_34() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache12_34)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache12_34() const { return ___U3CU3Ef__amU24cache12_34; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache12_34() { return &___U3CU3Ef__amU24cache12_34; }
	inline void set_U3CU3Ef__amU24cache12_34(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache12_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache12_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_35() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache13_35)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache13_35() const { return ___U3CU3Ef__amU24cache13_35; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache13_35() { return &___U3CU3Ef__amU24cache13_35; }
	inline void set_U3CU3Ef__amU24cache13_35(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache13_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache13_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_36() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache14_36)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache14_36() const { return ___U3CU3Ef__amU24cache14_36; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache14_36() { return &___U3CU3Ef__amU24cache14_36; }
	inline void set_U3CU3Ef__amU24cache14_36(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache14_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache14_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_37() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache15_37)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache15_37() const { return ___U3CU3Ef__amU24cache15_37; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache15_37() { return &___U3CU3Ef__amU24cache15_37; }
	inline void set_U3CU3Ef__amU24cache15_37(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache15_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache15_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_38() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache16_38)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache16_38() const { return ___U3CU3Ef__amU24cache16_38; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache16_38() { return &___U3CU3Ef__amU24cache16_38; }
	inline void set_U3CU3Ef__amU24cache16_38(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache16_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache16_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_39() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache17_39)); }
	inline ImporterFunc_t3630937194 * get_U3CU3Ef__amU24cache17_39() const { return ___U3CU3Ef__amU24cache17_39; }
	inline ImporterFunc_t3630937194 ** get_address_of_U3CU3Ef__amU24cache17_39() { return &___U3CU3Ef__amU24cache17_39; }
	inline void set_U3CU3Ef__amU24cache17_39(ImporterFunc_t3630937194 * value)
	{
		___U3CU3Ef__amU24cache17_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache17_39), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_40() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache18_40)); }
	inline WrapperFactory_t2158548929 * get_U3CU3Ef__amU24cache18_40() const { return ___U3CU3Ef__amU24cache18_40; }
	inline WrapperFactory_t2158548929 ** get_address_of_U3CU3Ef__amU24cache18_40() { return &___U3CU3Ef__amU24cache18_40; }
	inline void set_U3CU3Ef__amU24cache18_40(WrapperFactory_t2158548929 * value)
	{
		___U3CU3Ef__amU24cache18_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache18_40), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_41() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache19_41)); }
	inline WrapperFactory_t2158548929 * get_U3CU3Ef__amU24cache19_41() const { return ___U3CU3Ef__amU24cache19_41; }
	inline WrapperFactory_t2158548929 ** get_address_of_U3CU3Ef__amU24cache19_41() { return &___U3CU3Ef__amU24cache19_41; }
	inline void set_U3CU3Ef__amU24cache19_41(WrapperFactory_t2158548929 * value)
	{
		___U3CU3Ef__amU24cache19_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache19_41), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_42() { return static_cast<int32_t>(offsetof(JsonMapper_t3815285241_StaticFields, ___U3CU3Ef__amU24cache1A_42)); }
	inline WrapperFactory_t2158548929 * get_U3CU3Ef__amU24cache1A_42() const { return ___U3CU3Ef__amU24cache1A_42; }
	inline WrapperFactory_t2158548929 ** get_address_of_U3CU3Ef__amU24cache1A_42() { return &___U3CU3Ef__amU24cache1A_42; }
	inline void set_U3CU3Ef__amU24cache1A_42(WrapperFactory_t2158548929 * value)
	{
		___U3CU3Ef__amU24cache1A_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1A_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONMAPPER_T3815285241_H
#ifndef LEXER_T1514038666_H
#define LEXER_T1514038666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Lexer
struct  Lexer_t1514038666  : public RuntimeObject
{
public:
	// System.Boolean LitJson.Lexer::allow_comments
	bool ___allow_comments_2;
	// System.Boolean LitJson.Lexer::allow_single_quoted_strings
	bool ___allow_single_quoted_strings_3;
	// System.Boolean LitJson.Lexer::end_of_input
	bool ___end_of_input_4;
	// LitJson.FsmContext LitJson.Lexer::fsm_context
	FsmContext_t2331368794 * ___fsm_context_5;
	// System.Int32 LitJson.Lexer::input_buffer
	int32_t ___input_buffer_6;
	// System.Int32 LitJson.Lexer::input_char
	int32_t ___input_char_7;
	// System.IO.TextReader LitJson.Lexer::reader
	TextReader_t283511965 * ___reader_8;
	// System.Int32 LitJson.Lexer::state
	int32_t ___state_9;
	// System.Text.StringBuilder LitJson.Lexer::string_buffer
	StringBuilder_t * ___string_buffer_10;
	// System.String LitJson.Lexer::string_value
	String_t* ___string_value_11;
	// System.Int32 LitJson.Lexer::token
	int32_t ___token_12;
	// System.Int32 LitJson.Lexer::unichar
	int32_t ___unichar_13;

public:
	inline static int32_t get_offset_of_allow_comments_2() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___allow_comments_2)); }
	inline bool get_allow_comments_2() const { return ___allow_comments_2; }
	inline bool* get_address_of_allow_comments_2() { return &___allow_comments_2; }
	inline void set_allow_comments_2(bool value)
	{
		___allow_comments_2 = value;
	}

	inline static int32_t get_offset_of_allow_single_quoted_strings_3() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___allow_single_quoted_strings_3)); }
	inline bool get_allow_single_quoted_strings_3() const { return ___allow_single_quoted_strings_3; }
	inline bool* get_address_of_allow_single_quoted_strings_3() { return &___allow_single_quoted_strings_3; }
	inline void set_allow_single_quoted_strings_3(bool value)
	{
		___allow_single_quoted_strings_3 = value;
	}

	inline static int32_t get_offset_of_end_of_input_4() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___end_of_input_4)); }
	inline bool get_end_of_input_4() const { return ___end_of_input_4; }
	inline bool* get_address_of_end_of_input_4() { return &___end_of_input_4; }
	inline void set_end_of_input_4(bool value)
	{
		___end_of_input_4 = value;
	}

	inline static int32_t get_offset_of_fsm_context_5() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___fsm_context_5)); }
	inline FsmContext_t2331368794 * get_fsm_context_5() const { return ___fsm_context_5; }
	inline FsmContext_t2331368794 ** get_address_of_fsm_context_5() { return &___fsm_context_5; }
	inline void set_fsm_context_5(FsmContext_t2331368794 * value)
	{
		___fsm_context_5 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_context_5), value);
	}

	inline static int32_t get_offset_of_input_buffer_6() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___input_buffer_6)); }
	inline int32_t get_input_buffer_6() const { return ___input_buffer_6; }
	inline int32_t* get_address_of_input_buffer_6() { return &___input_buffer_6; }
	inline void set_input_buffer_6(int32_t value)
	{
		___input_buffer_6 = value;
	}

	inline static int32_t get_offset_of_input_char_7() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___input_char_7)); }
	inline int32_t get_input_char_7() const { return ___input_char_7; }
	inline int32_t* get_address_of_input_char_7() { return &___input_char_7; }
	inline void set_input_char_7(int32_t value)
	{
		___input_char_7 = value;
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___reader_8)); }
	inline TextReader_t283511965 * get_reader_8() const { return ___reader_8; }
	inline TextReader_t283511965 ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(TextReader_t283511965 * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___state_9)); }
	inline int32_t get_state_9() const { return ___state_9; }
	inline int32_t* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(int32_t value)
	{
		___state_9 = value;
	}

	inline static int32_t get_offset_of_string_buffer_10() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___string_buffer_10)); }
	inline StringBuilder_t * get_string_buffer_10() const { return ___string_buffer_10; }
	inline StringBuilder_t ** get_address_of_string_buffer_10() { return &___string_buffer_10; }
	inline void set_string_buffer_10(StringBuilder_t * value)
	{
		___string_buffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___string_buffer_10), value);
	}

	inline static int32_t get_offset_of_string_value_11() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___string_value_11)); }
	inline String_t* get_string_value_11() const { return ___string_value_11; }
	inline String_t** get_address_of_string_value_11() { return &___string_value_11; }
	inline void set_string_value_11(String_t* value)
	{
		___string_value_11 = value;
		Il2CppCodeGenWriteBarrier((&___string_value_11), value);
	}

	inline static int32_t get_offset_of_token_12() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___token_12)); }
	inline int32_t get_token_12() const { return ___token_12; }
	inline int32_t* get_address_of_token_12() { return &___token_12; }
	inline void set_token_12(int32_t value)
	{
		___token_12 = value;
	}

	inline static int32_t get_offset_of_unichar_13() { return static_cast<int32_t>(offsetof(Lexer_t1514038666, ___unichar_13)); }
	inline int32_t get_unichar_13() const { return ___unichar_13; }
	inline int32_t* get_address_of_unichar_13() { return &___unichar_13; }
	inline void set_unichar_13(int32_t value)
	{
		___unichar_13 = value;
	}
};

struct Lexer_t1514038666_StaticFields
{
public:
	// System.Int32[] LitJson.Lexer::fsm_return_table
	Int32U5BU5D_t385246372* ___fsm_return_table_0;
	// LitJson.Lexer/StateHandler[] LitJson.Lexer::fsm_handler_table
	StateHandlerU5BU5D_t1323790106* ___fsm_handler_table_1;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache0
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache0_14;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache1
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache1_15;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache2
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache2_16;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache3
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache3_17;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache4
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache4_18;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache5
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache5_19;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache6
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache6_20;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache7
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache7_21;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache8
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache8_22;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache9
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache9_23;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cacheA
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cacheA_24;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cacheB
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cacheB_25;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cacheC
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cacheC_26;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cacheD
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cacheD_27;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cacheE
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cacheE_28;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cacheF
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cacheF_29;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache10
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache10_30;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache11
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache11_31;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache12
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache12_32;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache13
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache13_33;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache14
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache14_34;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache15
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache15_35;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache16
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache16_36;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache17
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache17_37;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache18
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache18_38;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache19
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache19_39;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache1A
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache1A_40;
	// LitJson.Lexer/StateHandler LitJson.Lexer::<>f__mg$cache1B
	StateHandler_t105866779 * ___U3CU3Ef__mgU24cache1B_41;

public:
	inline static int32_t get_offset_of_fsm_return_table_0() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___fsm_return_table_0)); }
	inline Int32U5BU5D_t385246372* get_fsm_return_table_0() const { return ___fsm_return_table_0; }
	inline Int32U5BU5D_t385246372** get_address_of_fsm_return_table_0() { return &___fsm_return_table_0; }
	inline void set_fsm_return_table_0(Int32U5BU5D_t385246372* value)
	{
		___fsm_return_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_return_table_0), value);
	}

	inline static int32_t get_offset_of_fsm_handler_table_1() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___fsm_handler_table_1)); }
	inline StateHandlerU5BU5D_t1323790106* get_fsm_handler_table_1() const { return ___fsm_handler_table_1; }
	inline StateHandlerU5BU5D_t1323790106** get_address_of_fsm_handler_table_1() { return &___fsm_handler_table_1; }
	inline void set_fsm_handler_table_1(StateHandlerU5BU5D_t1323790106* value)
	{
		___fsm_handler_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_handler_table_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_14() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache0_14)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache0_14() const { return ___U3CU3Ef__mgU24cache0_14; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache0_14() { return &___U3CU3Ef__mgU24cache0_14; }
	inline void set_U3CU3Ef__mgU24cache0_14(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_15() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache1_15)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache1_15() const { return ___U3CU3Ef__mgU24cache1_15; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache1_15() { return &___U3CU3Ef__mgU24cache1_15; }
	inline void set_U3CU3Ef__mgU24cache1_15(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_16() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache2_16)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache2_16() const { return ___U3CU3Ef__mgU24cache2_16; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache2_16() { return &___U3CU3Ef__mgU24cache2_16; }
	inline void set_U3CU3Ef__mgU24cache2_16(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_17() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache3_17)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache3_17() const { return ___U3CU3Ef__mgU24cache3_17; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache3_17() { return &___U3CU3Ef__mgU24cache3_17; }
	inline void set_U3CU3Ef__mgU24cache3_17(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache3_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_18() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache4_18)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache4_18() const { return ___U3CU3Ef__mgU24cache4_18; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache4_18() { return &___U3CU3Ef__mgU24cache4_18; }
	inline void set_U3CU3Ef__mgU24cache4_18(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache4_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_19() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache5_19)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache5_19() const { return ___U3CU3Ef__mgU24cache5_19; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache5_19() { return &___U3CU3Ef__mgU24cache5_19; }
	inline void set_U3CU3Ef__mgU24cache5_19(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache5_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_20() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache6_20)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache6_20() const { return ___U3CU3Ef__mgU24cache6_20; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache6_20() { return &___U3CU3Ef__mgU24cache6_20; }
	inline void set_U3CU3Ef__mgU24cache6_20(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache6_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_21() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache7_21)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache7_21() const { return ___U3CU3Ef__mgU24cache7_21; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache7_21() { return &___U3CU3Ef__mgU24cache7_21; }
	inline void set_U3CU3Ef__mgU24cache7_21(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache7_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_22() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache8_22)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache8_22() const { return ___U3CU3Ef__mgU24cache8_22; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache8_22() { return &___U3CU3Ef__mgU24cache8_22; }
	inline void set_U3CU3Ef__mgU24cache8_22(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache8_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_23() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache9_23)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache9_23() const { return ___U3CU3Ef__mgU24cache9_23; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache9_23() { return &___U3CU3Ef__mgU24cache9_23; }
	inline void set_U3CU3Ef__mgU24cache9_23(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache9_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_24() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cacheA_24)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cacheA_24() const { return ___U3CU3Ef__mgU24cacheA_24; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cacheA_24() { return &___U3CU3Ef__mgU24cacheA_24; }
	inline void set_U3CU3Ef__mgU24cacheA_24(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cacheA_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_25() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cacheB_25)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cacheB_25() const { return ___U3CU3Ef__mgU24cacheB_25; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cacheB_25() { return &___U3CU3Ef__mgU24cacheB_25; }
	inline void set_U3CU3Ef__mgU24cacheB_25(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cacheB_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_26() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cacheC_26)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cacheC_26() const { return ___U3CU3Ef__mgU24cacheC_26; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cacheC_26() { return &___U3CU3Ef__mgU24cacheC_26; }
	inline void set_U3CU3Ef__mgU24cacheC_26(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cacheC_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_27() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cacheD_27)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cacheD_27() const { return ___U3CU3Ef__mgU24cacheD_27; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cacheD_27() { return &___U3CU3Ef__mgU24cacheD_27; }
	inline void set_U3CU3Ef__mgU24cacheD_27(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cacheD_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_28() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cacheE_28)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cacheE_28() const { return ___U3CU3Ef__mgU24cacheE_28; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cacheE_28() { return &___U3CU3Ef__mgU24cacheE_28; }
	inline void set_U3CU3Ef__mgU24cacheE_28(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cacheE_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_29() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cacheF_29)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cacheF_29() const { return ___U3CU3Ef__mgU24cacheF_29; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cacheF_29() { return &___U3CU3Ef__mgU24cacheF_29; }
	inline void set_U3CU3Ef__mgU24cacheF_29(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cacheF_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_30() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache10_30)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache10_30() const { return ___U3CU3Ef__mgU24cache10_30; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache10_30() { return &___U3CU3Ef__mgU24cache10_30; }
	inline void set_U3CU3Ef__mgU24cache10_30(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache10_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_31() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache11_31)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache11_31() const { return ___U3CU3Ef__mgU24cache11_31; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache11_31() { return &___U3CU3Ef__mgU24cache11_31; }
	inline void set_U3CU3Ef__mgU24cache11_31(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache11_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_32() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache12_32)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache12_32() const { return ___U3CU3Ef__mgU24cache12_32; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache12_32() { return &___U3CU3Ef__mgU24cache12_32; }
	inline void set_U3CU3Ef__mgU24cache12_32(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache12_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache13_33() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache13_33)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache13_33() const { return ___U3CU3Ef__mgU24cache13_33; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache13_33() { return &___U3CU3Ef__mgU24cache13_33; }
	inline void set_U3CU3Ef__mgU24cache13_33(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache13_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache13_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache14_34() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache14_34)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache14_34() const { return ___U3CU3Ef__mgU24cache14_34; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache14_34() { return &___U3CU3Ef__mgU24cache14_34; }
	inline void set_U3CU3Ef__mgU24cache14_34(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache14_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache14_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache15_35() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache15_35)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache15_35() const { return ___U3CU3Ef__mgU24cache15_35; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache15_35() { return &___U3CU3Ef__mgU24cache15_35; }
	inline void set_U3CU3Ef__mgU24cache15_35(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache15_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache15_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache16_36() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache16_36)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache16_36() const { return ___U3CU3Ef__mgU24cache16_36; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache16_36() { return &___U3CU3Ef__mgU24cache16_36; }
	inline void set_U3CU3Ef__mgU24cache16_36(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache16_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache16_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache17_37() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache17_37)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache17_37() const { return ___U3CU3Ef__mgU24cache17_37; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache17_37() { return &___U3CU3Ef__mgU24cache17_37; }
	inline void set_U3CU3Ef__mgU24cache17_37(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache17_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache17_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache18_38() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache18_38)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache18_38() const { return ___U3CU3Ef__mgU24cache18_38; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache18_38() { return &___U3CU3Ef__mgU24cache18_38; }
	inline void set_U3CU3Ef__mgU24cache18_38(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache18_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache18_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache19_39() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache19_39)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache19_39() const { return ___U3CU3Ef__mgU24cache19_39; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache19_39() { return &___U3CU3Ef__mgU24cache19_39; }
	inline void set_U3CU3Ef__mgU24cache19_39(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache19_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache19_39), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1A_40() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache1A_40)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache1A_40() const { return ___U3CU3Ef__mgU24cache1A_40; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache1A_40() { return &___U3CU3Ef__mgU24cache1A_40; }
	inline void set_U3CU3Ef__mgU24cache1A_40(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache1A_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1A_40), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1B_41() { return static_cast<int32_t>(offsetof(Lexer_t1514038666_StaticFields, ___U3CU3Ef__mgU24cache1B_41)); }
	inline StateHandler_t105866779 * get_U3CU3Ef__mgU24cache1B_41() const { return ___U3CU3Ef__mgU24cache1B_41; }
	inline StateHandler_t105866779 ** get_address_of_U3CU3Ef__mgU24cache1B_41() { return &___U3CU3Ef__mgU24cache1B_41; }
	inline void set_U3CU3Ef__mgU24cache1B_41(StateHandler_t105866779 * value)
	{
		___U3CU3Ef__mgU24cache1B_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1B_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T1514038666_H
#ifndef U3C_MP3FILEENCONDINGU3EC__ITERATOR1_T1291331674_H
#define U3C_MP3FILEENCONDINGU3EC__ITERATOR1_T1291331674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecordManager/<_Mp3FileEnconding>c__Iterator1
struct  U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674  : public RuntimeObject
{
public:
	// System.String AudioRecordManager/<_Mp3FileEnconding>c__Iterator1::<input>__0
	String_t* ___U3CinputU3E__0_0;
	// System.String AudioRecordManager/<_Mp3FileEnconding>c__Iterator1::<output>__0
	String_t* ___U3CoutputU3E__0_1;
	// AudioRecordManager AudioRecordManager/<_Mp3FileEnconding>c__Iterator1::$this
	AudioRecordManager_t3166594816 * ___U24this_2;
	// System.Object AudioRecordManager/<_Mp3FileEnconding>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean AudioRecordManager/<_Mp3FileEnconding>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 AudioRecordManager/<_Mp3FileEnconding>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CinputU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674, ___U3CinputU3E__0_0)); }
	inline String_t* get_U3CinputU3E__0_0() const { return ___U3CinputU3E__0_0; }
	inline String_t** get_address_of_U3CinputU3E__0_0() { return &___U3CinputU3E__0_0; }
	inline void set_U3CinputU3E__0_0(String_t* value)
	{
		___U3CinputU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3E__0_1() { return static_cast<int32_t>(offsetof(U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674, ___U3CoutputU3E__0_1)); }
	inline String_t* get_U3CoutputU3E__0_1() const { return ___U3CoutputU3E__0_1; }
	inline String_t** get_address_of_U3CoutputU3E__0_1() { return &___U3CoutputU3E__0_1; }
	inline void set_U3CoutputU3E__0_1(String_t* value)
	{
		___U3CoutputU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674, ___U24this_2)); }
	inline AudioRecordManager_t3166594816 * get_U24this_2() const { return ___U24this_2; }
	inline AudioRecordManager_t3166594816 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AudioRecordManager_t3166594816 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_MP3FILEENCONDINGU3EC__ITERATOR1_T1291331674_H
#ifndef U3CDELAYEDSHAREU3EC__ITERATOR3_T529057701_H
#define U3CDELAYEDSHAREU3EC__ITERATOR3_T529057701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecordManager/<delayedShare>c__Iterator3
struct  U3CdelayedShareU3Ec__Iterator3_t529057701  : public RuntimeObject
{
public:
	// System.String AudioRecordManager/<delayedShare>c__Iterator3::screenShotPath
	String_t* ___screenShotPath_0;
	// System.String AudioRecordManager/<delayedShare>c__Iterator3::text
	String_t* ___text_1;
	// System.Object AudioRecordManager/<delayedShare>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AudioRecordManager/<delayedShare>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 AudioRecordManager/<delayedShare>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_screenShotPath_0() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator3_t529057701, ___screenShotPath_0)); }
	inline String_t* get_screenShotPath_0() const { return ___screenShotPath_0; }
	inline String_t** get_address_of_screenShotPath_0() { return &___screenShotPath_0; }
	inline void set_screenShotPath_0(String_t* value)
	{
		___screenShotPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotPath_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator3_t529057701, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator3_t529057701, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator3_t529057701, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator3_t529057701, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSHAREU3EC__ITERATOR3_T529057701_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef RIFFCHUNK_T95273145_H
#define RIFFCHUNK_T95273145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.RiffChunk
struct  RiffChunk_t95273145  : public RuntimeObject
{
public:
	// System.Int32 NAudio.Wave.WZT.RiffChunk::identifier
	int32_t ___identifier_0;
	// System.Int32 NAudio.Wave.WZT.RiffChunk::length
	int32_t ___length_1;
	// System.Int64 NAudio.Wave.WZT.RiffChunk::streamPosition
	int64_t ___streamPosition_2;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(RiffChunk_t95273145, ___identifier_0)); }
	inline int32_t get_identifier_0() const { return ___identifier_0; }
	inline int32_t* get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(int32_t value)
	{
		___identifier_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(RiffChunk_t95273145, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_streamPosition_2() { return static_cast<int32_t>(offsetof(RiffChunk_t95273145, ___streamPosition_2)); }
	inline int64_t get_streamPosition_2() const { return ___streamPosition_2; }
	inline int64_t* get_address_of_streamPosition_2() { return &___streamPosition_2; }
	inline void set_streamPosition_2(int64_t value)
	{
		___streamPosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIFFCHUNK_T95273145_H
#ifndef NATIVEMETHODS_T3997983053_H
#define NATIVEMETHODS_T3997983053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.LibMp3Lame/NativeMethods
struct  NativeMethods_t3997983053  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEMETHODS_T3997983053_H
#ifndef QUESTDATAOLD_T372650824_H
#define QUESTDATAOLD_T372650824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestDataOld
struct  QuestDataOld_t372650824  : public RuntimeObject
{
public:
	// System.Int32 QuestDataOld::questID
	int32_t ___questID_0;
	// System.Boolean QuestDataOld::isActivity
	bool ___isActivity_1;
	// System.Boolean QuestDataOld::activityCompleted
	bool ___activityCompleted_2;
	// System.String[] QuestDataOld::textActivity
	StringU5BU5D_t1281789340* ___textActivity_3;
	// System.Boolean QuestDataOld::isSelectMultiple
	bool ___isSelectMultiple_4;
	// System.Int32 QuestDataOld::numberOptions
	int32_t ___numberOptions_5;
	// System.String[] QuestDataOld::options
	StringU5BU5D_t1281789340* ___options_6;
	// System.Int32 QuestDataOld::rigthOption
	int32_t ___rigthOption_7;
	// System.Boolean[] QuestDataOld::optionsSelected
	BooleanU5BU5D_t2897418192* ___optionsSelected_8;
	// System.Boolean QuestDataOld::isStory
	bool ___isStory_9;
	// System.Int32 QuestDataOld::numberOfStories
	int32_t ___numberOfStories_10;
	// System.String[] QuestDataOld::stories
	StringU5BU5D_t1281789340* ___stories_11;
	// System.Boolean[] QuestDataOld::storiesCompleted
	BooleanU5BU5D_t2897418192* ___storiesCompleted_12;
	// System.String QuestDataOld::sceneName
	String_t* ___sceneName_13;
	// System.String QuestDataOld::startText
	String_t* ___startText_14;
	// System.String QuestDataOld::completedText
	String_t* ___completedText_15;
	// System.String QuestDataOld::wrongText
	String_t* ___wrongText_16;

public:
	inline static int32_t get_offset_of_questID_0() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___questID_0)); }
	inline int32_t get_questID_0() const { return ___questID_0; }
	inline int32_t* get_address_of_questID_0() { return &___questID_0; }
	inline void set_questID_0(int32_t value)
	{
		___questID_0 = value;
	}

	inline static int32_t get_offset_of_isActivity_1() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___isActivity_1)); }
	inline bool get_isActivity_1() const { return ___isActivity_1; }
	inline bool* get_address_of_isActivity_1() { return &___isActivity_1; }
	inline void set_isActivity_1(bool value)
	{
		___isActivity_1 = value;
	}

	inline static int32_t get_offset_of_activityCompleted_2() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___activityCompleted_2)); }
	inline bool get_activityCompleted_2() const { return ___activityCompleted_2; }
	inline bool* get_address_of_activityCompleted_2() { return &___activityCompleted_2; }
	inline void set_activityCompleted_2(bool value)
	{
		___activityCompleted_2 = value;
	}

	inline static int32_t get_offset_of_textActivity_3() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___textActivity_3)); }
	inline StringU5BU5D_t1281789340* get_textActivity_3() const { return ___textActivity_3; }
	inline StringU5BU5D_t1281789340** get_address_of_textActivity_3() { return &___textActivity_3; }
	inline void set_textActivity_3(StringU5BU5D_t1281789340* value)
	{
		___textActivity_3 = value;
		Il2CppCodeGenWriteBarrier((&___textActivity_3), value);
	}

	inline static int32_t get_offset_of_isSelectMultiple_4() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___isSelectMultiple_4)); }
	inline bool get_isSelectMultiple_4() const { return ___isSelectMultiple_4; }
	inline bool* get_address_of_isSelectMultiple_4() { return &___isSelectMultiple_4; }
	inline void set_isSelectMultiple_4(bool value)
	{
		___isSelectMultiple_4 = value;
	}

	inline static int32_t get_offset_of_numberOptions_5() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___numberOptions_5)); }
	inline int32_t get_numberOptions_5() const { return ___numberOptions_5; }
	inline int32_t* get_address_of_numberOptions_5() { return &___numberOptions_5; }
	inline void set_numberOptions_5(int32_t value)
	{
		___numberOptions_5 = value;
	}

	inline static int32_t get_offset_of_options_6() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___options_6)); }
	inline StringU5BU5D_t1281789340* get_options_6() const { return ___options_6; }
	inline StringU5BU5D_t1281789340** get_address_of_options_6() { return &___options_6; }
	inline void set_options_6(StringU5BU5D_t1281789340* value)
	{
		___options_6 = value;
		Il2CppCodeGenWriteBarrier((&___options_6), value);
	}

	inline static int32_t get_offset_of_rigthOption_7() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___rigthOption_7)); }
	inline int32_t get_rigthOption_7() const { return ___rigthOption_7; }
	inline int32_t* get_address_of_rigthOption_7() { return &___rigthOption_7; }
	inline void set_rigthOption_7(int32_t value)
	{
		___rigthOption_7 = value;
	}

	inline static int32_t get_offset_of_optionsSelected_8() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___optionsSelected_8)); }
	inline BooleanU5BU5D_t2897418192* get_optionsSelected_8() const { return ___optionsSelected_8; }
	inline BooleanU5BU5D_t2897418192** get_address_of_optionsSelected_8() { return &___optionsSelected_8; }
	inline void set_optionsSelected_8(BooleanU5BU5D_t2897418192* value)
	{
		___optionsSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___optionsSelected_8), value);
	}

	inline static int32_t get_offset_of_isStory_9() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___isStory_9)); }
	inline bool get_isStory_9() const { return ___isStory_9; }
	inline bool* get_address_of_isStory_9() { return &___isStory_9; }
	inline void set_isStory_9(bool value)
	{
		___isStory_9 = value;
	}

	inline static int32_t get_offset_of_numberOfStories_10() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___numberOfStories_10)); }
	inline int32_t get_numberOfStories_10() const { return ___numberOfStories_10; }
	inline int32_t* get_address_of_numberOfStories_10() { return &___numberOfStories_10; }
	inline void set_numberOfStories_10(int32_t value)
	{
		___numberOfStories_10 = value;
	}

	inline static int32_t get_offset_of_stories_11() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___stories_11)); }
	inline StringU5BU5D_t1281789340* get_stories_11() const { return ___stories_11; }
	inline StringU5BU5D_t1281789340** get_address_of_stories_11() { return &___stories_11; }
	inline void set_stories_11(StringU5BU5D_t1281789340* value)
	{
		___stories_11 = value;
		Il2CppCodeGenWriteBarrier((&___stories_11), value);
	}

	inline static int32_t get_offset_of_storiesCompleted_12() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___storiesCompleted_12)); }
	inline BooleanU5BU5D_t2897418192* get_storiesCompleted_12() const { return ___storiesCompleted_12; }
	inline BooleanU5BU5D_t2897418192** get_address_of_storiesCompleted_12() { return &___storiesCompleted_12; }
	inline void set_storiesCompleted_12(BooleanU5BU5D_t2897418192* value)
	{
		___storiesCompleted_12 = value;
		Il2CppCodeGenWriteBarrier((&___storiesCompleted_12), value);
	}

	inline static int32_t get_offset_of_sceneName_13() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___sceneName_13)); }
	inline String_t* get_sceneName_13() const { return ___sceneName_13; }
	inline String_t** get_address_of_sceneName_13() { return &___sceneName_13; }
	inline void set_sceneName_13(String_t* value)
	{
		___sceneName_13 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_13), value);
	}

	inline static int32_t get_offset_of_startText_14() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___startText_14)); }
	inline String_t* get_startText_14() const { return ___startText_14; }
	inline String_t** get_address_of_startText_14() { return &___startText_14; }
	inline void set_startText_14(String_t* value)
	{
		___startText_14 = value;
		Il2CppCodeGenWriteBarrier((&___startText_14), value);
	}

	inline static int32_t get_offset_of_completedText_15() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___completedText_15)); }
	inline String_t* get_completedText_15() const { return ___completedText_15; }
	inline String_t** get_address_of_completedText_15() { return &___completedText_15; }
	inline void set_completedText_15(String_t* value)
	{
		___completedText_15 = value;
		Il2CppCodeGenWriteBarrier((&___completedText_15), value);
	}

	inline static int32_t get_offset_of_wrongText_16() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___wrongText_16)); }
	inline String_t* get_wrongText_16() const { return ___wrongText_16; }
	inline String_t** get_address_of_wrongText_16() { return &___wrongText_16; }
	inline void set_wrongText_16(String_t* value)
	{
		___wrongText_16 = value;
		Il2CppCodeGenWriteBarrier((&___wrongText_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTDATAOLD_T372650824_H
#ifndef SAVWAV_T2455245258_H
#define SAVWAV_T2455245258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavWav
struct  SavWav_t2455245258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVWAV_T2455245258_H
#ifndef ARRAYUNION_T2417265785_H
#define ARRAYUNION_T2417265785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.LameMP3FileWriter/ArrayUnion
struct  ArrayUnion_t2417265785  : public RuntimeObject
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 NAudio.Lame.LameMP3FileWriter/ArrayUnion::nBytes
			int32_t ___nBytes_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___nBytes_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___bytes_1_OffsetPadding[16];
			// System.Byte[] NAudio.Lame.LameMP3FileWriter/ArrayUnion::bytes
			ByteU5BU5D_t4116647657* ___bytes_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___bytes_1_OffsetPadding_forAlignmentOnly[16];
			ByteU5BU5D_t4116647657* ___bytes_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___shorts_2_OffsetPadding[16];
			// System.Int16[] NAudio.Lame.LameMP3FileWriter/ArrayUnion::shorts
			Int16U5BU5D_t3686840178* ___shorts_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___shorts_2_OffsetPadding_forAlignmentOnly[16];
			Int16U5BU5D_t3686840178* ___shorts_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ints_3_OffsetPadding[16];
			// System.Int32[] NAudio.Lame.LameMP3FileWriter/ArrayUnion::ints
			Int32U5BU5D_t385246372* ___ints_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ints_3_OffsetPadding_forAlignmentOnly[16];
			Int32U5BU5D_t385246372* ___ints_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___longs_4_OffsetPadding[16];
			// System.Int64[] NAudio.Lame.LameMP3FileWriter/ArrayUnion::longs
			Int64U5BU5D_t2559172825* ___longs_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___longs_4_OffsetPadding_forAlignmentOnly[16];
			Int64U5BU5D_t2559172825* ___longs_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___floats_5_OffsetPadding[16];
			// System.Single[] NAudio.Lame.LameMP3FileWriter/ArrayUnion::floats
			SingleU5BU5D_t1444911251* ___floats_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___floats_5_OffsetPadding_forAlignmentOnly[16];
			SingleU5BU5D_t1444911251* ___floats_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___doubles_6_OffsetPadding[16];
			// System.Double[] NAudio.Lame.LameMP3FileWriter/ArrayUnion::doubles
			DoubleU5BU5D_t3413330114* ___doubles_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___doubles_6_OffsetPadding_forAlignmentOnly[16];
			DoubleU5BU5D_t3413330114* ___doubles_6_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_nBytes_0() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___nBytes_0)); }
	inline int32_t get_nBytes_0() const { return ___nBytes_0; }
	inline int32_t* get_address_of_nBytes_0() { return &___nBytes_0; }
	inline void set_nBytes_0(int32_t value)
	{
		___nBytes_0 = value;
	}

	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___bytes_1)); }
	inline ByteU5BU5D_t4116647657* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t4116647657* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}

	inline static int32_t get_offset_of_shorts_2() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___shorts_2)); }
	inline Int16U5BU5D_t3686840178* get_shorts_2() const { return ___shorts_2; }
	inline Int16U5BU5D_t3686840178** get_address_of_shorts_2() { return &___shorts_2; }
	inline void set_shorts_2(Int16U5BU5D_t3686840178* value)
	{
		___shorts_2 = value;
		Il2CppCodeGenWriteBarrier((&___shorts_2), value);
	}

	inline static int32_t get_offset_of_ints_3() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___ints_3)); }
	inline Int32U5BU5D_t385246372* get_ints_3() const { return ___ints_3; }
	inline Int32U5BU5D_t385246372** get_address_of_ints_3() { return &___ints_3; }
	inline void set_ints_3(Int32U5BU5D_t385246372* value)
	{
		___ints_3 = value;
		Il2CppCodeGenWriteBarrier((&___ints_3), value);
	}

	inline static int32_t get_offset_of_longs_4() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___longs_4)); }
	inline Int64U5BU5D_t2559172825* get_longs_4() const { return ___longs_4; }
	inline Int64U5BU5D_t2559172825** get_address_of_longs_4() { return &___longs_4; }
	inline void set_longs_4(Int64U5BU5D_t2559172825* value)
	{
		___longs_4 = value;
		Il2CppCodeGenWriteBarrier((&___longs_4), value);
	}

	inline static int32_t get_offset_of_floats_5() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___floats_5)); }
	inline SingleU5BU5D_t1444911251* get_floats_5() const { return ___floats_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_floats_5() { return &___floats_5; }
	inline void set_floats_5(SingleU5BU5D_t1444911251* value)
	{
		___floats_5 = value;
		Il2CppCodeGenWriteBarrier((&___floats_5), value);
	}

	inline static int32_t get_offset_of_doubles_6() { return static_cast<int32_t>(offsetof(ArrayUnion_t2417265785, ___doubles_6)); }
	inline DoubleU5BU5D_t3413330114* get_doubles_6() const { return ___doubles_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_doubles_6() { return &___doubles_6; }
	inline void set_doubles_6(DoubleU5BU5D_t3413330114* value)
	{
		___doubles_6 = value;
		Il2CppCodeGenWriteBarrier((&___doubles_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Lame.LameMP3FileWriter/ArrayUnion
struct ArrayUnion_t2417265785_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___nBytes_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___nBytes_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___bytes_1_OffsetPadding[16];
			uint8_t* ___bytes_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___bytes_1_OffsetPadding_forAlignmentOnly[16];
			uint8_t* ___bytes_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___shorts_2_OffsetPadding[16];
			int16_t* ___shorts_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___shorts_2_OffsetPadding_forAlignmentOnly[16];
			int16_t* ___shorts_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ints_3_OffsetPadding[16];
			int32_t* ___ints_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ints_3_OffsetPadding_forAlignmentOnly[16];
			int32_t* ___ints_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___longs_4_OffsetPadding[16];
			int64_t* ___longs_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___longs_4_OffsetPadding_forAlignmentOnly[16];
			int64_t* ___longs_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___floats_5_OffsetPadding[16];
			float* ___floats_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___floats_5_OffsetPadding_forAlignmentOnly[16];
			float* ___floats_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___doubles_6_OffsetPadding[16];
			double* ___doubles_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___doubles_6_OffsetPadding_forAlignmentOnly[16];
			double* ___doubles_6_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of NAudio.Lame.LameMP3FileWriter/ArrayUnion
struct ArrayUnion_t2417265785_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___nBytes_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___nBytes_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___bytes_1_OffsetPadding[16];
			uint8_t* ___bytes_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___bytes_1_OffsetPadding_forAlignmentOnly[16];
			uint8_t* ___bytes_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___shorts_2_OffsetPadding[16];
			int16_t* ___shorts_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___shorts_2_OffsetPadding_forAlignmentOnly[16];
			int16_t* ___shorts_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ints_3_OffsetPadding[16];
			int32_t* ___ints_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ints_3_OffsetPadding_forAlignmentOnly[16];
			int32_t* ___ints_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___longs_4_OffsetPadding[16];
			int64_t* ___longs_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___longs_4_OffsetPadding_forAlignmentOnly[16];
			int64_t* ___longs_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___floats_5_OffsetPadding[16];
			float* ___floats_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___floats_5_OffsetPadding_forAlignmentOnly[16];
			float* ___floats_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___doubles_6_OffsetPadding[16];
			double* ___doubles_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___doubles_6_OffsetPadding_forAlignmentOnly[16];
			double* ___doubles_6_forAlignmentOnly;
		};
	};
};
#endif // ARRAYUNION_T2417265785_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3C_STARTLATEU3EC__ITERATOR0_T3966377835_H
#define U3C_STARTLATEU3EC__ITERATOR0_T3966377835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController/<_StartLate>c__Iterator0
struct  U3C_StartLateU3Ec__Iterator0_t3966377835  : public RuntimeObject
{
public:
	// System.Int32 UIPuzzleController/<_StartLate>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Int32 UIPuzzleController/<_StartLate>c__Iterator0::<j>__2
	int32_t ___U3CjU3E__2_1;
	// UIPuzzleController UIPuzzleController/<_StartLate>c__Iterator0::$this
	UIPuzzleController_t2836468369 * ___U24this_2;
	// System.Object UIPuzzleController/<_StartLate>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UIPuzzleController/<_StartLate>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UIPuzzleController/<_StartLate>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E__2_1() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U3CjU3E__2_1)); }
	inline int32_t get_U3CjU3E__2_1() const { return ___U3CjU3E__2_1; }
	inline int32_t* get_address_of_U3CjU3E__2_1() { return &___U3CjU3E__2_1; }
	inline void set_U3CjU3E__2_1(int32_t value)
	{
		___U3CjU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24this_2)); }
	inline UIPuzzleController_t2836468369 * get_U24this_2() const { return ___U24this_2; }
	inline UIPuzzleController_t2836468369 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UIPuzzleController_t2836468369 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_STARTLATEU3EC__ITERATOR0_T3966377835_H
#ifndef PLAYERDATA_T220878115_H
#define PLAYERDATA_T220878115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t220878115  : public RuntimeObject
{
public:
	// System.Int32 PlayerData::isFirstTime
	int32_t ___isFirstTime_0;
	// System.Int32 PlayerData::currentLevel
	int32_t ___currentLevel_1;
	// System.Int32 PlayerData::posLevelCharacter
	int32_t ___posLevelCharacter_2;
	// System.String PlayerData::characterName
	String_t* ___characterName_3;
	// System.Single PlayerData::posX
	float ___posX_4;
	// System.Single PlayerData::posY
	float ___posY_5;

public:
	inline static int32_t get_offset_of_isFirstTime_0() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___isFirstTime_0)); }
	inline int32_t get_isFirstTime_0() const { return ___isFirstTime_0; }
	inline int32_t* get_address_of_isFirstTime_0() { return &___isFirstTime_0; }
	inline void set_isFirstTime_0(int32_t value)
	{
		___isFirstTime_0 = value;
	}

	inline static int32_t get_offset_of_currentLevel_1() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___currentLevel_1)); }
	inline int32_t get_currentLevel_1() const { return ___currentLevel_1; }
	inline int32_t* get_address_of_currentLevel_1() { return &___currentLevel_1; }
	inline void set_currentLevel_1(int32_t value)
	{
		___currentLevel_1 = value;
	}

	inline static int32_t get_offset_of_posLevelCharacter_2() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___posLevelCharacter_2)); }
	inline int32_t get_posLevelCharacter_2() const { return ___posLevelCharacter_2; }
	inline int32_t* get_address_of_posLevelCharacter_2() { return &___posLevelCharacter_2; }
	inline void set_posLevelCharacter_2(int32_t value)
	{
		___posLevelCharacter_2 = value;
	}

	inline static int32_t get_offset_of_characterName_3() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___characterName_3)); }
	inline String_t* get_characterName_3() const { return ___characterName_3; }
	inline String_t** get_address_of_characterName_3() { return &___characterName_3; }
	inline void set_characterName_3(String_t* value)
	{
		___characterName_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_3), value);
	}

	inline static int32_t get_offset_of_posX_4() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___posX_4)); }
	inline float get_posX_4() const { return ___posX_4; }
	inline float* get_address_of_posX_4() { return &___posX_4; }
	inline void set_posX_4(float value)
	{
		___posX_4 = value;
	}

	inline static int32_t get_offset_of_posY_5() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___posY_5)); }
	inline float get_posY_5() const { return ___posY_5; }
	inline float* get_address_of_posY_5() { return &___posY_5; }
	inline void set_posY_5(float value)
	{
		___posY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T220878115_H
#ifndef U3CDELAYSTARTQUESTU3EC__ITERATOR0_T39561174_H
#define U3CDELAYSTARTQUESTU3EC__ITERATOR0_T39561174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestTrigger/<DelayStartQuest>c__Iterator0
struct  U3CDelayStartQuestU3Ec__Iterator0_t39561174  : public RuntimeObject
{
public:
	// QuestTrigger QuestTrigger/<DelayStartQuest>c__Iterator0::$this
	QuestTrigger_t3125687900 * ___U24this_0;
	// System.Object QuestTrigger/<DelayStartQuest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean QuestTrigger/<DelayStartQuest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 QuestTrigger/<DelayStartQuest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24this_0)); }
	inline QuestTrigger_t3125687900 * get_U24this_0() const { return ___U24this_0; }
	inline QuestTrigger_t3125687900 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(QuestTrigger_t3125687900 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYSTARTQUESTU3EC__ITERATOR0_T39561174_H
#ifndef EXTENDUTILITY_T1436226533_H
#define EXTENDUTILITY_T1436226533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtendUtility
struct  ExtendUtility_t1436226533  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDUTILITY_T1436226533_H
#ifndef U3CDELAYCOMPLETEQUESTU3EC__ITERATOR1_T479202429_H
#define U3CDELAYCOMPLETEQUESTU3EC__ITERATOR1_T479202429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController/<DelayCompleteQuest>c__Iterator1
struct  U3CDelayCompleteQuestU3Ec__Iterator1_t479202429  : public RuntimeObject
{
public:
	// UIPuzzleController UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$this
	UIPuzzleController_t2836468369 * ___U24this_0;
	// System.Object UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24this_0)); }
	inline UIPuzzleController_t2836468369 * get_U24this_0() const { return ___U24this_0; }
	inline UIPuzzleController_t2836468369 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UIPuzzleController_t2836468369 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYCOMPLETEQUESTU3EC__ITERATOR1_T479202429_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DISCRETETIME_T924799574_H
#define DISCRETETIME_T924799574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t924799574 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t924799574, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t924799574_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t924799574  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t924799574_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t924799574  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t924799574 * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t924799574  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T924799574_H
#ifndef APPLICATIONEXCEPTION_T2339761290_H
#define APPLICATIONEXCEPTION_T2339761290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ApplicationException
struct  ApplicationException_t2339761290  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXCEPTION_T2339761290_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT16_T2552820387_H
#define INT16_T2552820387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t2552820387 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t2552820387, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T2552820387_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef OBJECTMETADATA_T3566284522_H
#define OBJECTMETADATA_T3566284522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ObjectMetadata
struct  ObjectMetadata_t3566284522 
{
public:
	// System.Type LitJson.ObjectMetadata::element_type
	Type_t * ___element_type_0;
	// System.Boolean LitJson.ObjectMetadata::is_dictionary
	bool ___is_dictionary_1;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::properties
	RuntimeObject* ___properties_2;

public:
	inline static int32_t get_offset_of_element_type_0() { return static_cast<int32_t>(offsetof(ObjectMetadata_t3566284522, ___element_type_0)); }
	inline Type_t * get_element_type_0() const { return ___element_type_0; }
	inline Type_t ** get_address_of_element_type_0() { return &___element_type_0; }
	inline void set_element_type_0(Type_t * value)
	{
		___element_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_type_0), value);
	}

	inline static int32_t get_offset_of_is_dictionary_1() { return static_cast<int32_t>(offsetof(ObjectMetadata_t3566284522, ___is_dictionary_1)); }
	inline bool get_is_dictionary_1() const { return ___is_dictionary_1; }
	inline bool* get_address_of_is_dictionary_1() { return &___is_dictionary_1; }
	inline void set_is_dictionary_1(bool value)
	{
		___is_dictionary_1 = value;
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ObjectMetadata_t3566284522, ___properties_2)); }
	inline RuntimeObject* get_properties_2() const { return ___properties_2; }
	inline RuntimeObject** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(RuntimeObject* value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LitJson.ObjectMetadata
struct ObjectMetadata_t3566284522_marshaled_pinvoke
{
	Type_t * ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
// Native definition for COM marshalling of LitJson.ObjectMetadata
struct ObjectMetadata_t3566284522_marshaled_com
{
	Type_t * ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
#endif // OBJECTMETADATA_T3566284522_H
#ifndef PROPERTYMETADATA_T3727440473_H
#define PROPERTYMETADATA_T3727440473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.PropertyMetadata
struct  PropertyMetadata_t3727440473 
{
public:
	// System.Reflection.MemberInfo LitJson.PropertyMetadata::Info
	MemberInfo_t * ___Info_0;
	// System.Boolean LitJson.PropertyMetadata::IsField
	bool ___IsField_1;
	// System.Type LitJson.PropertyMetadata::Type
	Type_t * ___Type_2;

public:
	inline static int32_t get_offset_of_Info_0() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3727440473, ___Info_0)); }
	inline MemberInfo_t * get_Info_0() const { return ___Info_0; }
	inline MemberInfo_t ** get_address_of_Info_0() { return &___Info_0; }
	inline void set_Info_0(MemberInfo_t * value)
	{
		___Info_0 = value;
		Il2CppCodeGenWriteBarrier((&___Info_0), value);
	}

	inline static int32_t get_offset_of_IsField_1() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3727440473, ___IsField_1)); }
	inline bool get_IsField_1() const { return ___IsField_1; }
	inline bool* get_address_of_IsField_1() { return &___IsField_1; }
	inline void set_IsField_1(bool value)
	{
		___IsField_1 = value;
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3727440473, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LitJson.PropertyMetadata
struct PropertyMetadata_t3727440473_marshaled_pinvoke
{
	MemberInfo_t * ___Info_0;
	int32_t ___IsField_1;
	Type_t * ___Type_2;
};
// Native definition for COM marshalling of LitJson.PropertyMetadata
struct PropertyMetadata_t3727440473_marshaled_com
{
	MemberInfo_t * ___Info_0;
	int32_t ___IsField_1;
	Type_t * ___Type_2;
};
#endif // PROPERTYMETADATA_T3727440473_H
#ifndef ARRAYMETADATA_T894288939_H
#define ARRAYMETADATA_T894288939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ArrayMetadata
struct  ArrayMetadata_t894288939 
{
public:
	// System.Type LitJson.ArrayMetadata::element_type
	Type_t * ___element_type_0;
	// System.Boolean LitJson.ArrayMetadata::is_array
	bool ___is_array_1;
	// System.Boolean LitJson.ArrayMetadata::is_list
	bool ___is_list_2;

public:
	inline static int32_t get_offset_of_element_type_0() { return static_cast<int32_t>(offsetof(ArrayMetadata_t894288939, ___element_type_0)); }
	inline Type_t * get_element_type_0() const { return ___element_type_0; }
	inline Type_t ** get_address_of_element_type_0() { return &___element_type_0; }
	inline void set_element_type_0(Type_t * value)
	{
		___element_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_type_0), value);
	}

	inline static int32_t get_offset_of_is_array_1() { return static_cast<int32_t>(offsetof(ArrayMetadata_t894288939, ___is_array_1)); }
	inline bool get_is_array_1() const { return ___is_array_1; }
	inline bool* get_address_of_is_array_1() { return &___is_array_1; }
	inline void set_is_array_1(bool value)
	{
		___is_array_1 = value;
	}

	inline static int32_t get_offset_of_is_list_2() { return static_cast<int32_t>(offsetof(ArrayMetadata_t894288939, ___is_list_2)); }
	inline bool get_is_list_2() const { return ___is_list_2; }
	inline bool* get_address_of_is_list_2() { return &___is_list_2; }
	inline void set_is_list_2(bool value)
	{
		___is_list_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LitJson.ArrayMetadata
struct ArrayMetadata_t894288939_marshaled_pinvoke
{
	Type_t * ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
// Native definition for COM marshalling of LitJson.ArrayMetadata
struct ArrayMetadata_t894288939_marshaled_com
{
	Type_t * ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
#endif // ARRAYMETADATA_T894288939_H
#ifndef AUDIOMEDIASUBTYPES_T1633211026_H
#define AUDIOMEDIASUBTYPES_T1633211026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Dmo.WZT.AudioMediaSubtypes
struct  AudioMediaSubtypes_t1633211026  : public RuntimeObject
{
public:

public:
};

struct AudioMediaSubtypes_t1633211026_StaticFields
{
public:
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_PCM
	Guid_t  ___MEDIASUBTYPE_PCM_0;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_PCMAudioObsolete
	Guid_t  ___MEDIASUBTYPE_PCMAudioObsolete_1;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_MPEG1Packet
	Guid_t  ___MEDIASUBTYPE_MPEG1Packet_2;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_MPEG1Payload
	Guid_t  ___MEDIASUBTYPE_MPEG1Payload_3;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_MPEG2_AUDIO
	Guid_t  ___MEDIASUBTYPE_MPEG2_AUDIO_4;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_DVD_LPCM_AUDIO
	Guid_t  ___MEDIASUBTYPE_DVD_LPCM_AUDIO_5;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_DRM_Audio
	Guid_t  ___MEDIASUBTYPE_DRM_Audio_6;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_IEEE_FLOAT
	Guid_t  ___MEDIASUBTYPE_IEEE_FLOAT_7;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_DOLBY_AC3
	Guid_t  ___MEDIASUBTYPE_DOLBY_AC3_8;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_DOLBY_AC3_SPDIF
	Guid_t  ___MEDIASUBTYPE_DOLBY_AC3_SPDIF_9;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_RAW_SPORT
	Guid_t  ___MEDIASUBTYPE_RAW_SPORT_10;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_SPDIF_TAG_241h
	Guid_t  ___MEDIASUBTYPE_SPDIF_TAG_241h_11;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::WMMEDIASUBTYPE_MP3
	Guid_t  ___WMMEDIASUBTYPE_MP3_12;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_WAVE
	Guid_t  ___MEDIASUBTYPE_WAVE_13;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_AU
	Guid_t  ___MEDIASUBTYPE_AU_14;
	// System.Guid NAudio.Dmo.WZT.AudioMediaSubtypes::MEDIASUBTYPE_AIFF
	Guid_t  ___MEDIASUBTYPE_AIFF_15;
	// System.Guid[] NAudio.Dmo.WZT.AudioMediaSubtypes::AudioSubTypes
	GuidU5BU5D_t545550574* ___AudioSubTypes_16;
	// System.String[] NAudio.Dmo.WZT.AudioMediaSubtypes::AudioSubTypeNames
	StringU5BU5D_t1281789340* ___AudioSubTypeNames_17;

public:
	inline static int32_t get_offset_of_MEDIASUBTYPE_PCM_0() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_PCM_0)); }
	inline Guid_t  get_MEDIASUBTYPE_PCM_0() const { return ___MEDIASUBTYPE_PCM_0; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_PCM_0() { return &___MEDIASUBTYPE_PCM_0; }
	inline void set_MEDIASUBTYPE_PCM_0(Guid_t  value)
	{
		___MEDIASUBTYPE_PCM_0 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_PCMAudioObsolete_1() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_PCMAudioObsolete_1)); }
	inline Guid_t  get_MEDIASUBTYPE_PCMAudioObsolete_1() const { return ___MEDIASUBTYPE_PCMAudioObsolete_1; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_PCMAudioObsolete_1() { return &___MEDIASUBTYPE_PCMAudioObsolete_1; }
	inline void set_MEDIASUBTYPE_PCMAudioObsolete_1(Guid_t  value)
	{
		___MEDIASUBTYPE_PCMAudioObsolete_1 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_MPEG1Packet_2() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_MPEG1Packet_2)); }
	inline Guid_t  get_MEDIASUBTYPE_MPEG1Packet_2() const { return ___MEDIASUBTYPE_MPEG1Packet_2; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_MPEG1Packet_2() { return &___MEDIASUBTYPE_MPEG1Packet_2; }
	inline void set_MEDIASUBTYPE_MPEG1Packet_2(Guid_t  value)
	{
		___MEDIASUBTYPE_MPEG1Packet_2 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_MPEG1Payload_3() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_MPEG1Payload_3)); }
	inline Guid_t  get_MEDIASUBTYPE_MPEG1Payload_3() const { return ___MEDIASUBTYPE_MPEG1Payload_3; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_MPEG1Payload_3() { return &___MEDIASUBTYPE_MPEG1Payload_3; }
	inline void set_MEDIASUBTYPE_MPEG1Payload_3(Guid_t  value)
	{
		___MEDIASUBTYPE_MPEG1Payload_3 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_MPEG2_AUDIO_4() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_MPEG2_AUDIO_4)); }
	inline Guid_t  get_MEDIASUBTYPE_MPEG2_AUDIO_4() const { return ___MEDIASUBTYPE_MPEG2_AUDIO_4; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_MPEG2_AUDIO_4() { return &___MEDIASUBTYPE_MPEG2_AUDIO_4; }
	inline void set_MEDIASUBTYPE_MPEG2_AUDIO_4(Guid_t  value)
	{
		___MEDIASUBTYPE_MPEG2_AUDIO_4 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_DVD_LPCM_AUDIO_5() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_DVD_LPCM_AUDIO_5)); }
	inline Guid_t  get_MEDIASUBTYPE_DVD_LPCM_AUDIO_5() const { return ___MEDIASUBTYPE_DVD_LPCM_AUDIO_5; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_DVD_LPCM_AUDIO_5() { return &___MEDIASUBTYPE_DVD_LPCM_AUDIO_5; }
	inline void set_MEDIASUBTYPE_DVD_LPCM_AUDIO_5(Guid_t  value)
	{
		___MEDIASUBTYPE_DVD_LPCM_AUDIO_5 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_DRM_Audio_6() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_DRM_Audio_6)); }
	inline Guid_t  get_MEDIASUBTYPE_DRM_Audio_6() const { return ___MEDIASUBTYPE_DRM_Audio_6; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_DRM_Audio_6() { return &___MEDIASUBTYPE_DRM_Audio_6; }
	inline void set_MEDIASUBTYPE_DRM_Audio_6(Guid_t  value)
	{
		___MEDIASUBTYPE_DRM_Audio_6 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_IEEE_FLOAT_7() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_IEEE_FLOAT_7)); }
	inline Guid_t  get_MEDIASUBTYPE_IEEE_FLOAT_7() const { return ___MEDIASUBTYPE_IEEE_FLOAT_7; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_IEEE_FLOAT_7() { return &___MEDIASUBTYPE_IEEE_FLOAT_7; }
	inline void set_MEDIASUBTYPE_IEEE_FLOAT_7(Guid_t  value)
	{
		___MEDIASUBTYPE_IEEE_FLOAT_7 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_DOLBY_AC3_8() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_DOLBY_AC3_8)); }
	inline Guid_t  get_MEDIASUBTYPE_DOLBY_AC3_8() const { return ___MEDIASUBTYPE_DOLBY_AC3_8; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_DOLBY_AC3_8() { return &___MEDIASUBTYPE_DOLBY_AC3_8; }
	inline void set_MEDIASUBTYPE_DOLBY_AC3_8(Guid_t  value)
	{
		___MEDIASUBTYPE_DOLBY_AC3_8 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_DOLBY_AC3_SPDIF_9() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_DOLBY_AC3_SPDIF_9)); }
	inline Guid_t  get_MEDIASUBTYPE_DOLBY_AC3_SPDIF_9() const { return ___MEDIASUBTYPE_DOLBY_AC3_SPDIF_9; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_DOLBY_AC3_SPDIF_9() { return &___MEDIASUBTYPE_DOLBY_AC3_SPDIF_9; }
	inline void set_MEDIASUBTYPE_DOLBY_AC3_SPDIF_9(Guid_t  value)
	{
		___MEDIASUBTYPE_DOLBY_AC3_SPDIF_9 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_RAW_SPORT_10() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_RAW_SPORT_10)); }
	inline Guid_t  get_MEDIASUBTYPE_RAW_SPORT_10() const { return ___MEDIASUBTYPE_RAW_SPORT_10; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_RAW_SPORT_10() { return &___MEDIASUBTYPE_RAW_SPORT_10; }
	inline void set_MEDIASUBTYPE_RAW_SPORT_10(Guid_t  value)
	{
		___MEDIASUBTYPE_RAW_SPORT_10 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_SPDIF_TAG_241h_11() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_SPDIF_TAG_241h_11)); }
	inline Guid_t  get_MEDIASUBTYPE_SPDIF_TAG_241h_11() const { return ___MEDIASUBTYPE_SPDIF_TAG_241h_11; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_SPDIF_TAG_241h_11() { return &___MEDIASUBTYPE_SPDIF_TAG_241h_11; }
	inline void set_MEDIASUBTYPE_SPDIF_TAG_241h_11(Guid_t  value)
	{
		___MEDIASUBTYPE_SPDIF_TAG_241h_11 = value;
	}

	inline static int32_t get_offset_of_WMMEDIASUBTYPE_MP3_12() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___WMMEDIASUBTYPE_MP3_12)); }
	inline Guid_t  get_WMMEDIASUBTYPE_MP3_12() const { return ___WMMEDIASUBTYPE_MP3_12; }
	inline Guid_t * get_address_of_WMMEDIASUBTYPE_MP3_12() { return &___WMMEDIASUBTYPE_MP3_12; }
	inline void set_WMMEDIASUBTYPE_MP3_12(Guid_t  value)
	{
		___WMMEDIASUBTYPE_MP3_12 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_WAVE_13() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_WAVE_13)); }
	inline Guid_t  get_MEDIASUBTYPE_WAVE_13() const { return ___MEDIASUBTYPE_WAVE_13; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_WAVE_13() { return &___MEDIASUBTYPE_WAVE_13; }
	inline void set_MEDIASUBTYPE_WAVE_13(Guid_t  value)
	{
		___MEDIASUBTYPE_WAVE_13 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_AU_14() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_AU_14)); }
	inline Guid_t  get_MEDIASUBTYPE_AU_14() const { return ___MEDIASUBTYPE_AU_14; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_AU_14() { return &___MEDIASUBTYPE_AU_14; }
	inline void set_MEDIASUBTYPE_AU_14(Guid_t  value)
	{
		___MEDIASUBTYPE_AU_14 = value;
	}

	inline static int32_t get_offset_of_MEDIASUBTYPE_AIFF_15() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___MEDIASUBTYPE_AIFF_15)); }
	inline Guid_t  get_MEDIASUBTYPE_AIFF_15() const { return ___MEDIASUBTYPE_AIFF_15; }
	inline Guid_t * get_address_of_MEDIASUBTYPE_AIFF_15() { return &___MEDIASUBTYPE_AIFF_15; }
	inline void set_MEDIASUBTYPE_AIFF_15(Guid_t  value)
	{
		___MEDIASUBTYPE_AIFF_15 = value;
	}

	inline static int32_t get_offset_of_AudioSubTypes_16() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___AudioSubTypes_16)); }
	inline GuidU5BU5D_t545550574* get_AudioSubTypes_16() const { return ___AudioSubTypes_16; }
	inline GuidU5BU5D_t545550574** get_address_of_AudioSubTypes_16() { return &___AudioSubTypes_16; }
	inline void set_AudioSubTypes_16(GuidU5BU5D_t545550574* value)
	{
		___AudioSubTypes_16 = value;
		Il2CppCodeGenWriteBarrier((&___AudioSubTypes_16), value);
	}

	inline static int32_t get_offset_of_AudioSubTypeNames_17() { return static_cast<int32_t>(offsetof(AudioMediaSubtypes_t1633211026_StaticFields, ___AudioSubTypeNames_17)); }
	inline StringU5BU5D_t1281789340* get_AudioSubTypeNames_17() const { return ___AudioSubTypeNames_17; }
	inline StringU5BU5D_t1281789340** get_address_of_AudioSubTypeNames_17() { return &___AudioSubTypeNames_17; }
	inline void set_AudioSubTypeNames_17(StringU5BU5D_t1281789340* value)
	{
		___AudioSubTypeNames_17 = value;
		Il2CppCodeGenWriteBarrier((&___AudioSubTypeNames_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMEDIASUBTYPES_T1633211026_H
#ifndef WAVEFORMATENCODING_T2321404235_H
#define WAVEFORMATENCODING_T2321404235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormatEncoding
struct  WaveFormatEncoding_t2321404235 
{
public:
	// System.UInt16 NAudio.Wave.WZT.WaveFormatEncoding::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaveFormatEncoding_t2321404235, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEFORMATENCODING_T2321404235_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef WAVESTREAM_T91297746_H
#define WAVESTREAM_T91297746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveStream
struct  WaveStream_t91297746  : public Stream_t1273022909
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVESTREAM_T91297746_H
#ifndef JSONTYPE_T2731125707_H
#define JSONTYPE_T2731125707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonType
struct  JsonType_t2731125707 
{
public:
	// System.Int32 LitJson.JsonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonType_t2731125707, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPE_T2731125707_H
#ifndef MEDIATYPE_T4163677969_H
#define MEDIATYPE_T4163677969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/MediaType
struct  MediaType_t4163677969 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/MediaType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MediaType_t4163677969, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPE_T4163677969_H
#ifndef EPUZZLETOKEN_T2465232863_H
#define EPUZZLETOKEN_T2465232863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EPuzzleToken
struct  EPuzzleToken_t2465232863 
{
public:
	// System.Int32 EPuzzleToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EPuzzleToken_t2465232863, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPUZZLETOKEN_T2465232863_H
#ifndef LAMEPRESET_T1074290626_H
#define LAMEPRESET_T1074290626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.LAMEPreset
struct  LAMEPreset_t1074290626 
{
public:
	// System.Int32 NAudio.Lame.LAMEPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LAMEPreset_t1074290626, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAMEPRESET_T1074290626_H
#ifndef MPEGMODE_T406895743_H
#define MPEGMODE_T406895743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.MPEGMode
struct  MPEGMode_t406895743 
{
public:
	// System.UInt32 NAudio.Lame.DLL.MPEGMode::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MPEGMode_t406895743, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MPEGMODE_T406895743_H
#ifndef ASMOPTIMIZATIONS_T3241729094_H
#define ASMOPTIMIZATIONS_T3241729094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.ASMOptimizations
struct  ASMOptimizations_t3241729094 
{
public:
	// System.UInt32 NAudio.Lame.DLL.ASMOptimizations::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ASMOptimizations_t3241729094, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASMOPTIMIZATIONS_T3241729094_H
#ifndef VBRMODE_T3062980280_H
#define VBRMODE_T3062980280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.VBRMode
struct  VBRMode_t3062980280 
{
public:
	// System.UInt32 NAudio.Lame.DLL.VBRMode::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VBRMode_t3062980280, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VBRMODE_T3062980280_H
#ifndef MPEGVERSION_T749196590_H
#define MPEGVERSION_T749196590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.MPEGVersion
struct  MPEGVersion_t749196590 
{
public:
	// System.Int32 NAudio.Lame.DLL.MPEGVersion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MPEGVersion_t749196590, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MPEGVERSION_T749196590_H
#ifndef LAMEVERSION_T651286643_H
#define LAMEVERSION_T651286643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.LAMEVersion
struct  LAMEVersion_t651286643  : public RuntimeObject
{
public:
	// System.Int32 NAudio.Lame.DLL.LAMEVersion::major
	int32_t ___major_0;
	// System.Int32 NAudio.Lame.DLL.LAMEVersion::minor
	int32_t ___minor_1;
	// System.Boolean NAudio.Lame.DLL.LAMEVersion::alpha
	bool ___alpha_2;
	// System.Boolean NAudio.Lame.DLL.LAMEVersion::beta
	bool ___beta_3;
	// System.Int32 NAudio.Lame.DLL.LAMEVersion::psy_major
	int32_t ___psy_major_4;
	// System.Int32 NAudio.Lame.DLL.LAMEVersion::psy_minor
	int32_t ___psy_minor_5;
	// System.Boolean NAudio.Lame.DLL.LAMEVersion::psy_alpha
	bool ___psy_alpha_6;
	// System.Boolean NAudio.Lame.DLL.LAMEVersion::psy_beta
	bool ___psy_beta_7;
	// System.IntPtr NAudio.Lame.DLL.LAMEVersion::features_ptr
	intptr_t ___features_ptr_8;

public:
	inline static int32_t get_offset_of_major_0() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___major_0)); }
	inline int32_t get_major_0() const { return ___major_0; }
	inline int32_t* get_address_of_major_0() { return &___major_0; }
	inline void set_major_0(int32_t value)
	{
		___major_0 = value;
	}

	inline static int32_t get_offset_of_minor_1() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___minor_1)); }
	inline int32_t get_minor_1() const { return ___minor_1; }
	inline int32_t* get_address_of_minor_1() { return &___minor_1; }
	inline void set_minor_1(int32_t value)
	{
		___minor_1 = value;
	}

	inline static int32_t get_offset_of_alpha_2() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___alpha_2)); }
	inline bool get_alpha_2() const { return ___alpha_2; }
	inline bool* get_address_of_alpha_2() { return &___alpha_2; }
	inline void set_alpha_2(bool value)
	{
		___alpha_2 = value;
	}

	inline static int32_t get_offset_of_beta_3() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___beta_3)); }
	inline bool get_beta_3() const { return ___beta_3; }
	inline bool* get_address_of_beta_3() { return &___beta_3; }
	inline void set_beta_3(bool value)
	{
		___beta_3 = value;
	}

	inline static int32_t get_offset_of_psy_major_4() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___psy_major_4)); }
	inline int32_t get_psy_major_4() const { return ___psy_major_4; }
	inline int32_t* get_address_of_psy_major_4() { return &___psy_major_4; }
	inline void set_psy_major_4(int32_t value)
	{
		___psy_major_4 = value;
	}

	inline static int32_t get_offset_of_psy_minor_5() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___psy_minor_5)); }
	inline int32_t get_psy_minor_5() const { return ___psy_minor_5; }
	inline int32_t* get_address_of_psy_minor_5() { return &___psy_minor_5; }
	inline void set_psy_minor_5(int32_t value)
	{
		___psy_minor_5 = value;
	}

	inline static int32_t get_offset_of_psy_alpha_6() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___psy_alpha_6)); }
	inline bool get_psy_alpha_6() const { return ___psy_alpha_6; }
	inline bool* get_address_of_psy_alpha_6() { return &___psy_alpha_6; }
	inline void set_psy_alpha_6(bool value)
	{
		___psy_alpha_6 = value;
	}

	inline static int32_t get_offset_of_psy_beta_7() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___psy_beta_7)); }
	inline bool get_psy_beta_7() const { return ___psy_beta_7; }
	inline bool* get_address_of_psy_beta_7() { return &___psy_beta_7; }
	inline void set_psy_beta_7(bool value)
	{
		___psy_beta_7 = value;
	}

	inline static int32_t get_offset_of_features_ptr_8() { return static_cast<int32_t>(offsetof(LAMEVersion_t651286643, ___features_ptr_8)); }
	inline intptr_t get_features_ptr_8() const { return ___features_ptr_8; }
	inline intptr_t* get_address_of_features_ptr_8() { return &___features_ptr_8; }
	inline void set_features_ptr_8(intptr_t value)
	{
		___features_ptr_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Lame.DLL.LAMEVersion
struct LAMEVersion_t651286643_marshaled_pinvoke
{
	int32_t ___major_0;
	int32_t ___minor_1;
	int32_t ___alpha_2;
	int32_t ___beta_3;
	int32_t ___psy_major_4;
	int32_t ___psy_minor_5;
	int32_t ___psy_alpha_6;
	int32_t ___psy_beta_7;
	intptr_t ___features_ptr_8;
};
// Native definition for COM marshalling of NAudio.Lame.DLL.LAMEVersion
struct LAMEVersion_t651286643_marshaled_com
{
	int32_t ___major_0;
	int32_t ___minor_1;
	int32_t ___alpha_2;
	int32_t ___beta_3;
	int32_t ___psy_major_4;
	int32_t ___psy_minor_5;
	int32_t ___psy_alpha_6;
	int32_t ___psy_beta_7;
	intptr_t ___features_ptr_8;
};
#endif // LAMEVERSION_T651286643_H
#ifndef LIBMP3LAME_T153112877_H
#define LIBMP3LAME_T153112877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.LibMp3Lame
struct  LibMp3Lame_t153112877  : public RuntimeObject
{
public:
	// System.IntPtr NAudio.Lame.DLL.LibMp3Lame::context
	intptr_t ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877, ___context_0)); }
	inline intptr_t get_context_0() const { return ___context_0; }
	inline intptr_t* get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(intptr_t value)
	{
		___context_0 = value;
	}
};

struct LibMp3Lame_t153112877_StaticFields
{
public:
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.UInt64> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache0
	setFunc_1_t3007409591 * ___U3CU3Ef__mgU24cache0_1;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1_2;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache2
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache2_3;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Single> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache3
	setFunc_1_t270636273 * ___U3CU3Ef__mgU24cache3_4;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Single> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache4
	setFunc_1_t270636273 * ___U3CU3Ef__mgU24cache4_5;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Single> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache5
	setFunc_1_t270636273 * ___U3CU3Ef__mgU24cache5_6;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache6
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache6_7;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache7
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache7_8;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache8
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache8_9;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache9
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache9_10;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cacheA
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cacheA_11;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<NAudio.Lame.DLL.MPEGMode> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cacheB
	setFunc_1_t3575232538 * ___U3CU3Ef__mgU24cacheB_12;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cacheC
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cacheC_13;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cacheD
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cacheD_14;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cacheE
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cacheE_15;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cacheF
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cacheF_16;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache10
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache10_17;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache11
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache11_18;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache12
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache12_19;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Single> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache13
	setFunc_1_t270636273 * ___U3CU3Ef__mgU24cache13_20;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache14
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache14_21;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache15
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache15_22;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache16
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache16_23;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache17
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache17_24;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache18
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache18_25;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache19
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache19_26;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1A
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1A_27;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1B
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1B_28;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1C
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1C_29;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1D
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1D_30;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1E
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1E_31;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache1F
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache1F_32;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache20
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache20_33;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<NAudio.Lame.DLL.VBRMode> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache21
	setFunc_1_t1936349779 * ___U3CU3Ef__mgU24cache21_34;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache22
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache22_35;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Single> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache23
	setFunc_1_t270636273 * ___U3CU3Ef__mgU24cache23_36;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache24
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache24_37;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache25
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache25_38;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache26
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache26_39;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Boolean> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache27
	setFunc_1_t3265624760 * ___U3CU3Ef__mgU24cache27_40;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache28
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache28_41;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache29
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache29_42;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache2A
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache2A_43;
	// NAudio.Lame.DLL.LibMp3Lame/setFunc`1<System.Int32> NAudio.Lame.DLL.LibMp3Lame::<>f__mg$cache2B
	setFunc_1_t1824315252 * ___U3CU3Ef__mgU24cache2B_44;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline setFunc_1_t3007409591 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline setFunc_1_t3007409591 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(setFunc_1_t3007409591 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_2() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1_2)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1_2() const { return ___U3CU3Ef__mgU24cache1_2; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1_2() { return &___U3CU3Ef__mgU24cache1_2; }
	inline void set_U3CU3Ef__mgU24cache1_2(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_3() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache2_3)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache2_3() const { return ___U3CU3Ef__mgU24cache2_3; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache2_3() { return &___U3CU3Ef__mgU24cache2_3; }
	inline void set_U3CU3Ef__mgU24cache2_3(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_4() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache3_4)); }
	inline setFunc_1_t270636273 * get_U3CU3Ef__mgU24cache3_4() const { return ___U3CU3Ef__mgU24cache3_4; }
	inline setFunc_1_t270636273 ** get_address_of_U3CU3Ef__mgU24cache3_4() { return &___U3CU3Ef__mgU24cache3_4; }
	inline void set_U3CU3Ef__mgU24cache3_4(setFunc_1_t270636273 * value)
	{
		___U3CU3Ef__mgU24cache3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_5() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache4_5)); }
	inline setFunc_1_t270636273 * get_U3CU3Ef__mgU24cache4_5() const { return ___U3CU3Ef__mgU24cache4_5; }
	inline setFunc_1_t270636273 ** get_address_of_U3CU3Ef__mgU24cache4_5() { return &___U3CU3Ef__mgU24cache4_5; }
	inline void set_U3CU3Ef__mgU24cache4_5(setFunc_1_t270636273 * value)
	{
		___U3CU3Ef__mgU24cache4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_6() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache5_6)); }
	inline setFunc_1_t270636273 * get_U3CU3Ef__mgU24cache5_6() const { return ___U3CU3Ef__mgU24cache5_6; }
	inline setFunc_1_t270636273 ** get_address_of_U3CU3Ef__mgU24cache5_6() { return &___U3CU3Ef__mgU24cache5_6; }
	inline void set_U3CU3Ef__mgU24cache5_6(setFunc_1_t270636273 * value)
	{
		___U3CU3Ef__mgU24cache5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_7() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache6_7)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache6_7() const { return ___U3CU3Ef__mgU24cache6_7; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache6_7() { return &___U3CU3Ef__mgU24cache6_7; }
	inline void set_U3CU3Ef__mgU24cache6_7(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_8() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache7_8)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache7_8() const { return ___U3CU3Ef__mgU24cache7_8; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache7_8() { return &___U3CU3Ef__mgU24cache7_8; }
	inline void set_U3CU3Ef__mgU24cache7_8(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache7_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_9() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache8_9)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache8_9() const { return ___U3CU3Ef__mgU24cache8_9; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache8_9() { return &___U3CU3Ef__mgU24cache8_9; }
	inline void set_U3CU3Ef__mgU24cache8_9(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache8_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_10() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache9_10)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache9_10() const { return ___U3CU3Ef__mgU24cache9_10; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache9_10() { return &___U3CU3Ef__mgU24cache9_10; }
	inline void set_U3CU3Ef__mgU24cache9_10(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache9_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_11() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cacheA_11)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cacheA_11() const { return ___U3CU3Ef__mgU24cacheA_11; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cacheA_11() { return &___U3CU3Ef__mgU24cacheA_11; }
	inline void set_U3CU3Ef__mgU24cacheA_11(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cacheA_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_12() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cacheB_12)); }
	inline setFunc_1_t3575232538 * get_U3CU3Ef__mgU24cacheB_12() const { return ___U3CU3Ef__mgU24cacheB_12; }
	inline setFunc_1_t3575232538 ** get_address_of_U3CU3Ef__mgU24cacheB_12() { return &___U3CU3Ef__mgU24cacheB_12; }
	inline void set_U3CU3Ef__mgU24cacheB_12(setFunc_1_t3575232538 * value)
	{
		___U3CU3Ef__mgU24cacheB_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_13() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cacheC_13)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cacheC_13() const { return ___U3CU3Ef__mgU24cacheC_13; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cacheC_13() { return &___U3CU3Ef__mgU24cacheC_13; }
	inline void set_U3CU3Ef__mgU24cacheC_13(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cacheC_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_14() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cacheD_14)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cacheD_14() const { return ___U3CU3Ef__mgU24cacheD_14; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cacheD_14() { return &___U3CU3Ef__mgU24cacheD_14; }
	inline void set_U3CU3Ef__mgU24cacheD_14(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cacheD_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_15() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cacheE_15)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cacheE_15() const { return ___U3CU3Ef__mgU24cacheE_15; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cacheE_15() { return &___U3CU3Ef__mgU24cacheE_15; }
	inline void set_U3CU3Ef__mgU24cacheE_15(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cacheE_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_16() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cacheF_16)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cacheF_16() const { return ___U3CU3Ef__mgU24cacheF_16; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cacheF_16() { return &___U3CU3Ef__mgU24cacheF_16; }
	inline void set_U3CU3Ef__mgU24cacheF_16(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cacheF_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_17() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache10_17)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache10_17() const { return ___U3CU3Ef__mgU24cache10_17; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache10_17() { return &___U3CU3Ef__mgU24cache10_17; }
	inline void set_U3CU3Ef__mgU24cache10_17(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache10_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_18() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache11_18)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache11_18() const { return ___U3CU3Ef__mgU24cache11_18; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache11_18() { return &___U3CU3Ef__mgU24cache11_18; }
	inline void set_U3CU3Ef__mgU24cache11_18(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache11_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_19() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache12_19)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache12_19() const { return ___U3CU3Ef__mgU24cache12_19; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache12_19() { return &___U3CU3Ef__mgU24cache12_19; }
	inline void set_U3CU3Ef__mgU24cache12_19(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache12_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache13_20() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache13_20)); }
	inline setFunc_1_t270636273 * get_U3CU3Ef__mgU24cache13_20() const { return ___U3CU3Ef__mgU24cache13_20; }
	inline setFunc_1_t270636273 ** get_address_of_U3CU3Ef__mgU24cache13_20() { return &___U3CU3Ef__mgU24cache13_20; }
	inline void set_U3CU3Ef__mgU24cache13_20(setFunc_1_t270636273 * value)
	{
		___U3CU3Ef__mgU24cache13_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache13_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache14_21() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache14_21)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache14_21() const { return ___U3CU3Ef__mgU24cache14_21; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache14_21() { return &___U3CU3Ef__mgU24cache14_21; }
	inline void set_U3CU3Ef__mgU24cache14_21(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache14_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache14_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache15_22() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache15_22)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache15_22() const { return ___U3CU3Ef__mgU24cache15_22; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache15_22() { return &___U3CU3Ef__mgU24cache15_22; }
	inline void set_U3CU3Ef__mgU24cache15_22(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache15_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache15_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache16_23() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache16_23)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache16_23() const { return ___U3CU3Ef__mgU24cache16_23; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache16_23() { return &___U3CU3Ef__mgU24cache16_23; }
	inline void set_U3CU3Ef__mgU24cache16_23(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache16_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache16_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache17_24() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache17_24)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache17_24() const { return ___U3CU3Ef__mgU24cache17_24; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache17_24() { return &___U3CU3Ef__mgU24cache17_24; }
	inline void set_U3CU3Ef__mgU24cache17_24(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache17_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache17_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache18_25() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache18_25)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache18_25() const { return ___U3CU3Ef__mgU24cache18_25; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache18_25() { return &___U3CU3Ef__mgU24cache18_25; }
	inline void set_U3CU3Ef__mgU24cache18_25(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache18_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache18_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache19_26() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache19_26)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache19_26() const { return ___U3CU3Ef__mgU24cache19_26; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache19_26() { return &___U3CU3Ef__mgU24cache19_26; }
	inline void set_U3CU3Ef__mgU24cache19_26(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache19_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache19_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1A_27() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1A_27)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1A_27() const { return ___U3CU3Ef__mgU24cache1A_27; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1A_27() { return &___U3CU3Ef__mgU24cache1A_27; }
	inline void set_U3CU3Ef__mgU24cache1A_27(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1A_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1A_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1B_28() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1B_28)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1B_28() const { return ___U3CU3Ef__mgU24cache1B_28; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1B_28() { return &___U3CU3Ef__mgU24cache1B_28; }
	inline void set_U3CU3Ef__mgU24cache1B_28(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1B_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1B_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1C_29() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1C_29)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1C_29() const { return ___U3CU3Ef__mgU24cache1C_29; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1C_29() { return &___U3CU3Ef__mgU24cache1C_29; }
	inline void set_U3CU3Ef__mgU24cache1C_29(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1C_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1C_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1D_30() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1D_30)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1D_30() const { return ___U3CU3Ef__mgU24cache1D_30; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1D_30() { return &___U3CU3Ef__mgU24cache1D_30; }
	inline void set_U3CU3Ef__mgU24cache1D_30(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1D_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1D_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1E_31() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1E_31)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1E_31() const { return ___U3CU3Ef__mgU24cache1E_31; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1E_31() { return &___U3CU3Ef__mgU24cache1E_31; }
	inline void set_U3CU3Ef__mgU24cache1E_31(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1E_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1E_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1F_32() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache1F_32)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache1F_32() const { return ___U3CU3Ef__mgU24cache1F_32; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache1F_32() { return &___U3CU3Ef__mgU24cache1F_32; }
	inline void set_U3CU3Ef__mgU24cache1F_32(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache1F_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1F_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache20_33() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache20_33)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache20_33() const { return ___U3CU3Ef__mgU24cache20_33; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache20_33() { return &___U3CU3Ef__mgU24cache20_33; }
	inline void set_U3CU3Ef__mgU24cache20_33(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache20_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache20_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache21_34() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache21_34)); }
	inline setFunc_1_t1936349779 * get_U3CU3Ef__mgU24cache21_34() const { return ___U3CU3Ef__mgU24cache21_34; }
	inline setFunc_1_t1936349779 ** get_address_of_U3CU3Ef__mgU24cache21_34() { return &___U3CU3Ef__mgU24cache21_34; }
	inline void set_U3CU3Ef__mgU24cache21_34(setFunc_1_t1936349779 * value)
	{
		___U3CU3Ef__mgU24cache21_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache21_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache22_35() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache22_35)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache22_35() const { return ___U3CU3Ef__mgU24cache22_35; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache22_35() { return &___U3CU3Ef__mgU24cache22_35; }
	inline void set_U3CU3Ef__mgU24cache22_35(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache22_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache22_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache23_36() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache23_36)); }
	inline setFunc_1_t270636273 * get_U3CU3Ef__mgU24cache23_36() const { return ___U3CU3Ef__mgU24cache23_36; }
	inline setFunc_1_t270636273 ** get_address_of_U3CU3Ef__mgU24cache23_36() { return &___U3CU3Ef__mgU24cache23_36; }
	inline void set_U3CU3Ef__mgU24cache23_36(setFunc_1_t270636273 * value)
	{
		___U3CU3Ef__mgU24cache23_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache23_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache24_37() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache24_37)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache24_37() const { return ___U3CU3Ef__mgU24cache24_37; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache24_37() { return &___U3CU3Ef__mgU24cache24_37; }
	inline void set_U3CU3Ef__mgU24cache24_37(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache24_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache24_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache25_38() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache25_38)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache25_38() const { return ___U3CU3Ef__mgU24cache25_38; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache25_38() { return &___U3CU3Ef__mgU24cache25_38; }
	inline void set_U3CU3Ef__mgU24cache25_38(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache25_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache25_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache26_39() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache26_39)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache26_39() const { return ___U3CU3Ef__mgU24cache26_39; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache26_39() { return &___U3CU3Ef__mgU24cache26_39; }
	inline void set_U3CU3Ef__mgU24cache26_39(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache26_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache26_39), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache27_40() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache27_40)); }
	inline setFunc_1_t3265624760 * get_U3CU3Ef__mgU24cache27_40() const { return ___U3CU3Ef__mgU24cache27_40; }
	inline setFunc_1_t3265624760 ** get_address_of_U3CU3Ef__mgU24cache27_40() { return &___U3CU3Ef__mgU24cache27_40; }
	inline void set_U3CU3Ef__mgU24cache27_40(setFunc_1_t3265624760 * value)
	{
		___U3CU3Ef__mgU24cache27_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache27_40), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache28_41() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache28_41)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache28_41() const { return ___U3CU3Ef__mgU24cache28_41; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache28_41() { return &___U3CU3Ef__mgU24cache28_41; }
	inline void set_U3CU3Ef__mgU24cache28_41(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache28_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache28_41), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache29_42() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache29_42)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache29_42() const { return ___U3CU3Ef__mgU24cache29_42; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache29_42() { return &___U3CU3Ef__mgU24cache29_42; }
	inline void set_U3CU3Ef__mgU24cache29_42(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache29_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache29_42), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2A_43() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache2A_43)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache2A_43() const { return ___U3CU3Ef__mgU24cache2A_43; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache2A_43() { return &___U3CU3Ef__mgU24cache2A_43; }
	inline void set_U3CU3Ef__mgU24cache2A_43(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache2A_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2A_43), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2B_44() { return static_cast<int32_t>(offsetof(LibMp3Lame_t153112877_StaticFields, ___U3CU3Ef__mgU24cache2B_44)); }
	inline setFunc_1_t1824315252 * get_U3CU3Ef__mgU24cache2B_44() const { return ___U3CU3Ef__mgU24cache2B_44; }
	inline setFunc_1_t1824315252 ** get_address_of_U3CU3Ef__mgU24cache2B_44() { return &___U3CU3Ef__mgU24cache2B_44; }
	inline void set_U3CU3Ef__mgU24cache2B_44(setFunc_1_t1824315252 * value)
	{
		___U3CU3Ef__mgU24cache2B_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2B_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIBMP3LAME_T153112877_H
#ifndef LAMEMP3FILEWRITER_T44674093_H
#define LAMEMP3FILEWRITER_T44674093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.LameMP3FileWriter
struct  LameMP3FileWriter_t44674093  : public Stream_t1273022909
{
public:
	// NAudio.Lame.DLL.LibMp3Lame NAudio.Lame.LameMP3FileWriter::_lame
	LibMp3Lame_t153112877 * ____lame_4;
	// NAudio.Wave.WZT.WaveFormat NAudio.Lame.LameMP3FileWriter::inputFormat
	WaveFormat_t2842237185 * ___inputFormat_5;
	// System.IO.Stream NAudio.Lame.LameMP3FileWriter::outStream
	Stream_t1273022909 * ___outStream_6;
	// System.Boolean NAudio.Lame.LameMP3FileWriter::disposeOutput
	bool ___disposeOutput_7;
	// NAudio.Lame.LameMP3FileWriter/ArrayUnion NAudio.Lame.LameMP3FileWriter::inBuffer
	ArrayUnion_t2417265785 * ___inBuffer_8;
	// System.Int32 NAudio.Lame.LameMP3FileWriter::inPosition
	int32_t ___inPosition_9;
	// System.Byte[] NAudio.Lame.LameMP3FileWriter::outBuffer
	ByteU5BU5D_t4116647657* ___outBuffer_10;
	// System.Int64 NAudio.Lame.LameMP3FileWriter::InputByteCount
	int64_t ___InputByteCount_11;
	// System.Int64 NAudio.Lame.LameMP3FileWriter::OutputByteCount
	int64_t ___OutputByteCount_12;
	// NAudio.Lame.LameMP3FileWriter/delEncode NAudio.Lame.LameMP3FileWriter::_encode
	delEncode_t1434347660 * ____encode_13;

public:
	inline static int32_t get_offset_of__lame_4() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ____lame_4)); }
	inline LibMp3Lame_t153112877 * get__lame_4() const { return ____lame_4; }
	inline LibMp3Lame_t153112877 ** get_address_of__lame_4() { return &____lame_4; }
	inline void set__lame_4(LibMp3Lame_t153112877 * value)
	{
		____lame_4 = value;
		Il2CppCodeGenWriteBarrier((&____lame_4), value);
	}

	inline static int32_t get_offset_of_inputFormat_5() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___inputFormat_5)); }
	inline WaveFormat_t2842237185 * get_inputFormat_5() const { return ___inputFormat_5; }
	inline WaveFormat_t2842237185 ** get_address_of_inputFormat_5() { return &___inputFormat_5; }
	inline void set_inputFormat_5(WaveFormat_t2842237185 * value)
	{
		___inputFormat_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputFormat_5), value);
	}

	inline static int32_t get_offset_of_outStream_6() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___outStream_6)); }
	inline Stream_t1273022909 * get_outStream_6() const { return ___outStream_6; }
	inline Stream_t1273022909 ** get_address_of_outStream_6() { return &___outStream_6; }
	inline void set_outStream_6(Stream_t1273022909 * value)
	{
		___outStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___outStream_6), value);
	}

	inline static int32_t get_offset_of_disposeOutput_7() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___disposeOutput_7)); }
	inline bool get_disposeOutput_7() const { return ___disposeOutput_7; }
	inline bool* get_address_of_disposeOutput_7() { return &___disposeOutput_7; }
	inline void set_disposeOutput_7(bool value)
	{
		___disposeOutput_7 = value;
	}

	inline static int32_t get_offset_of_inBuffer_8() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___inBuffer_8)); }
	inline ArrayUnion_t2417265785 * get_inBuffer_8() const { return ___inBuffer_8; }
	inline ArrayUnion_t2417265785 ** get_address_of_inBuffer_8() { return &___inBuffer_8; }
	inline void set_inBuffer_8(ArrayUnion_t2417265785 * value)
	{
		___inBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___inBuffer_8), value);
	}

	inline static int32_t get_offset_of_inPosition_9() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___inPosition_9)); }
	inline int32_t get_inPosition_9() const { return ___inPosition_9; }
	inline int32_t* get_address_of_inPosition_9() { return &___inPosition_9; }
	inline void set_inPosition_9(int32_t value)
	{
		___inPosition_9 = value;
	}

	inline static int32_t get_offset_of_outBuffer_10() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___outBuffer_10)); }
	inline ByteU5BU5D_t4116647657* get_outBuffer_10() const { return ___outBuffer_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_outBuffer_10() { return &___outBuffer_10; }
	inline void set_outBuffer_10(ByteU5BU5D_t4116647657* value)
	{
		___outBuffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___outBuffer_10), value);
	}

	inline static int32_t get_offset_of_InputByteCount_11() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___InputByteCount_11)); }
	inline int64_t get_InputByteCount_11() const { return ___InputByteCount_11; }
	inline int64_t* get_address_of_InputByteCount_11() { return &___InputByteCount_11; }
	inline void set_InputByteCount_11(int64_t value)
	{
		___InputByteCount_11 = value;
	}

	inline static int32_t get_offset_of_OutputByteCount_12() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ___OutputByteCount_12)); }
	inline int64_t get_OutputByteCount_12() const { return ___OutputByteCount_12; }
	inline int64_t* get_address_of_OutputByteCount_12() { return &___OutputByteCount_12; }
	inline void set_OutputByteCount_12(int64_t value)
	{
		___OutputByteCount_12 = value;
	}

	inline static int32_t get_offset_of__encode_13() { return static_cast<int32_t>(offsetof(LameMP3FileWriter_t44674093, ____encode_13)); }
	inline delEncode_t1434347660 * get__encode_13() const { return ____encode_13; }
	inline delEncode_t1434347660 ** get_address_of__encode_13() { return &____encode_13; }
	inline void set__encode_13(delEncode_t1434347660 * value)
	{
		____encode_13 = value;
		Il2CppCodeGenWriteBarrier((&____encode_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAMEMP3FILEWRITER_T44674093_H
#ifndef AUDIOTYPE_T3170162477_H
#define AUDIOTYPE_T3170162477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioType
struct  AudioType_t3170162477 
{
public:
	// System.Int32 UnityEngine.AudioType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioType_t3170162477, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTYPE_T3170162477_H
#ifndef JSONEXCEPTION_T3682484112_H
#define JSONEXCEPTION_T3682484112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonException
struct  JsonException_t3682484112  : public ApplicationException_t2339761290
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T3682484112_H
#ifndef EAUDIOSAMPLINGRATE_T3810640270_H
#define EAUDIOSAMPLINGRATE_T3810640270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EAudioSamplingRate
struct  EAudioSamplingRate_t3810640270 
{
public:
	// System.Int32 EAudioSamplingRate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EAudioSamplingRate_t3810640270, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAUDIOSAMPLINGRATE_T3810640270_H
#ifndef EAUDIOCHANNEL_T4054551443_H
#define EAUDIOCHANNEL_T4054551443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EAudioChannel
struct  EAudioChannel_t4054551443 
{
public:
	// System.Int32 EAudioChannel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EAudioChannel_t4054551443, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAUDIOCHANNEL_T4054551443_H
#ifndef PARSERTOKEN_T2380208742_H
#define PARSERTOKEN_T2380208742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ParserToken
struct  ParserToken_t2380208742 
{
public:
	// System.Int32 LitJson.ParserToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParserToken_t2380208742, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTOKEN_T2380208742_H
#ifndef EAUDIOFILEEXTENSION_T2867350651_H
#define EAUDIOFILEEXTENSION_T2867350651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EAudioFileExtension
struct  EAudioFileExtension_t2867350651 
{
public:
	// System.Int32 EAudioFileExtension::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EAudioFileExtension_t2867350651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAUDIOFILEEXTENSION_T2867350651_H
#ifndef AUDIOSPEAKERMODE_T101565544_H
#define AUDIOSPEAKERMODE_T101565544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSpeakerMode
struct  AudioSpeakerMode_t101565544 
{
public:
	// System.Int32 UnityEngine.AudioSpeakerMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioSpeakerMode_t101565544, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSPEAKERMODE_T101565544_H
#ifndef U3CMOVETOPOSU3EC__ITERATOR0_T951185036_H
#define U3CMOVETOPOSU3EC__ITERATOR0_T951185036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterBehavior/<MoveToPos>c__Iterator0
struct  U3CMoveToPosU3Ec__Iterator0_t951185036  : public RuntimeObject
{
public:
	// System.Single CharacterBehavior/<MoveToPos>c__Iterator0::<counter>__0
	float ___U3CcounterU3E__0_0;
	// UnityEngine.Transform CharacterBehavior/<MoveToPos>c__Iterator0::fromPosition
	Transform_t3600365921 * ___fromPosition_1;
	// UnityEngine.Vector3 CharacterBehavior/<MoveToPos>c__Iterator0::<startPos>__0
	Vector3_t3722313464  ___U3CstartPosU3E__0_2;
	// System.Single CharacterBehavior/<MoveToPos>c__Iterator0::duration
	float ___duration_3;
	// UnityEngine.Vector3 CharacterBehavior/<MoveToPos>c__Iterator0::toPosition
	Vector3_t3722313464  ___toPosition_4;
	// CharacterBehavior CharacterBehavior/<MoveToPos>c__Iterator0::$this
	CharacterBehavior_t2392107484 * ___U24this_5;
	// System.Object CharacterBehavior/<MoveToPos>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean CharacterBehavior/<MoveToPos>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 CharacterBehavior/<MoveToPos>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CcounterU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___U3CcounterU3E__0_0)); }
	inline float get_U3CcounterU3E__0_0() const { return ___U3CcounterU3E__0_0; }
	inline float* get_address_of_U3CcounterU3E__0_0() { return &___U3CcounterU3E__0_0; }
	inline void set_U3CcounterU3E__0_0(float value)
	{
		___U3CcounterU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_fromPosition_1() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___fromPosition_1)); }
	inline Transform_t3600365921 * get_fromPosition_1() const { return ___fromPosition_1; }
	inline Transform_t3600365921 ** get_address_of_fromPosition_1() { return &___fromPosition_1; }
	inline void set_fromPosition_1(Transform_t3600365921 * value)
	{
		___fromPosition_1 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_1), value);
	}

	inline static int32_t get_offset_of_U3CstartPosU3E__0_2() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___U3CstartPosU3E__0_2)); }
	inline Vector3_t3722313464  get_U3CstartPosU3E__0_2() const { return ___U3CstartPosU3E__0_2; }
	inline Vector3_t3722313464 * get_address_of_U3CstartPosU3E__0_2() { return &___U3CstartPosU3E__0_2; }
	inline void set_U3CstartPosU3E__0_2(Vector3_t3722313464  value)
	{
		___U3CstartPosU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_toPosition_4() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___toPosition_4)); }
	inline Vector3_t3722313464  get_toPosition_4() const { return ___toPosition_4; }
	inline Vector3_t3722313464 * get_address_of_toPosition_4() { return &___toPosition_4; }
	inline void set_toPosition_4(Vector3_t3722313464  value)
	{
		___toPosition_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___U24this_5)); }
	inline CharacterBehavior_t2392107484 * get_U24this_5() const { return ___U24this_5; }
	inline CharacterBehavior_t2392107484 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CharacterBehavior_t2392107484 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CMoveToPosU3Ec__Iterator0_t951185036, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVETOPOSU3EC__ITERATOR0_T951185036_H
#ifndef CONDITION_T3240125930_H
#define CONDITION_T3240125930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Condition
struct  Condition_t3240125930 
{
public:
	// System.Int32 LitJson.Condition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Condition_t3240125930, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITION_T3240125930_H
#ifndef JSONTOKEN_T3605538862_H
#define JSONTOKEN_T3605538862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonToken
struct  JsonToken_t3605538862 
{
public:
	// System.Int32 LitJson.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_t3605538862, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T3605538862_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef U3CLOADAUDIOU3EC__ITERATOR2_T22865955_H
#define U3CLOADAUDIOU3EC__ITERATOR2_T22865955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecordManager/<LoadAudio>c__Iterator2
struct  U3CLoadAudioU3Ec__Iterator2_t22865955  : public RuntimeObject
{
public:
	// UnityEngine.AudioType AudioRecordManager/<LoadAudio>c__Iterator2::audioType
	int32_t ___audioType_0;
	// System.String AudioRecordManager/<LoadAudio>c__Iterator2::file
	String_t* ___file_1;
	// System.String AudioRecordManager/<LoadAudio>c__Iterator2::<path>__0
	String_t* ___U3CpathU3E__0_2;
	// UnityEngine.Networking.UnityWebRequest AudioRecordManager/<LoadAudio>c__Iterator2::<www>__1
	UnityWebRequest_t463507806 * ___U3CwwwU3E__1_3;
	// System.Action`2<System.Boolean,UnityEngine.AudioClip> AudioRecordManager/<LoadAudio>c__Iterator2::OnComplete
	Action_2_t3124271206 * ___OnComplete_4;
	// AudioRecordManager AudioRecordManager/<LoadAudio>c__Iterator2::$this
	AudioRecordManager_t3166594816 * ___U24this_5;
	// System.Object AudioRecordManager/<LoadAudio>c__Iterator2::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean AudioRecordManager/<LoadAudio>c__Iterator2::$disposing
	bool ___U24disposing_7;
	// System.Int32 AudioRecordManager/<LoadAudio>c__Iterator2::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_audioType_0() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___audioType_0)); }
	inline int32_t get_audioType_0() const { return ___audioType_0; }
	inline int32_t* get_address_of_audioType_0() { return &___audioType_0; }
	inline void set_audioType_0(int32_t value)
	{
		___audioType_0 = value;
	}

	inline static int32_t get_offset_of_file_1() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___file_1)); }
	inline String_t* get_file_1() const { return ___file_1; }
	inline String_t** get_address_of_file_1() { return &___file_1; }
	inline void set_file_1(String_t* value)
	{
		___file_1 = value;
		Il2CppCodeGenWriteBarrier((&___file_1), value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___U3CpathU3E__0_2)); }
	inline String_t* get_U3CpathU3E__0_2() const { return ___U3CpathU3E__0_2; }
	inline String_t** get_address_of_U3CpathU3E__0_2() { return &___U3CpathU3E__0_2; }
	inline void set_U3CpathU3E__0_2(String_t* value)
	{
		___U3CpathU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___U3CwwwU3E__1_3)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__1_3() const { return ___U3CwwwU3E__1_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__1_3() { return &___U3CwwwU3E__1_3; }
	inline void set_U3CwwwU3E__1_3(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_3), value);
	}

	inline static int32_t get_offset_of_OnComplete_4() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___OnComplete_4)); }
	inline Action_2_t3124271206 * get_OnComplete_4() const { return ___OnComplete_4; }
	inline Action_2_t3124271206 ** get_address_of_OnComplete_4() { return &___OnComplete_4; }
	inline void set_OnComplete_4(Action_2_t3124271206 * value)
	{
		___OnComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___U24this_5)); }
	inline AudioRecordManager_t3166594816 * get_U24this_5() const { return ___U24this_5; }
	inline AudioRecordManager_t3166594816 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(AudioRecordManager_t3166594816 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLoadAudioU3Ec__Iterator2_t22865955, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADAUDIOU3EC__ITERATOR2_T22865955_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AUDIOCONFIGURATION_T4040042187_H
#define AUDIOCONFIGURATION_T4040042187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioConfiguration
struct  AudioConfiguration_t4040042187 
{
public:
	// UnityEngine.AudioSpeakerMode UnityEngine.AudioConfiguration::speakerMode
	int32_t ___speakerMode_0;
	// System.Int32 UnityEngine.AudioConfiguration::dspBufferSize
	int32_t ___dspBufferSize_1;
	// System.Int32 UnityEngine.AudioConfiguration::sampleRate
	int32_t ___sampleRate_2;
	// System.Int32 UnityEngine.AudioConfiguration::numRealVoices
	int32_t ___numRealVoices_3;
	// System.Int32 UnityEngine.AudioConfiguration::numVirtualVoices
	int32_t ___numVirtualVoices_4;

public:
	inline static int32_t get_offset_of_speakerMode_0() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___speakerMode_0)); }
	inline int32_t get_speakerMode_0() const { return ___speakerMode_0; }
	inline int32_t* get_address_of_speakerMode_0() { return &___speakerMode_0; }
	inline void set_speakerMode_0(int32_t value)
	{
		___speakerMode_0 = value;
	}

	inline static int32_t get_offset_of_dspBufferSize_1() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___dspBufferSize_1)); }
	inline int32_t get_dspBufferSize_1() const { return ___dspBufferSize_1; }
	inline int32_t* get_address_of_dspBufferSize_1() { return &___dspBufferSize_1; }
	inline void set_dspBufferSize_1(int32_t value)
	{
		___dspBufferSize_1 = value;
	}

	inline static int32_t get_offset_of_sampleRate_2() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___sampleRate_2)); }
	inline int32_t get_sampleRate_2() const { return ___sampleRate_2; }
	inline int32_t* get_address_of_sampleRate_2() { return &___sampleRate_2; }
	inline void set_sampleRate_2(int32_t value)
	{
		___sampleRate_2 = value;
	}

	inline static int32_t get_offset_of_numRealVoices_3() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___numRealVoices_3)); }
	inline int32_t get_numRealVoices_3() const { return ___numRealVoices_3; }
	inline int32_t* get_address_of_numRealVoices_3() { return &___numRealVoices_3; }
	inline void set_numRealVoices_3(int32_t value)
	{
		___numRealVoices_3 = value;
	}

	inline static int32_t get_offset_of_numVirtualVoices_4() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___numVirtualVoices_4)); }
	inline int32_t get_numVirtualVoices_4() const { return ___numVirtualVoices_4; }
	inline int32_t* get_address_of_numVirtualVoices_4() { return &___numVirtualVoices_4; }
	inline void set_numVirtualVoices_4(int32_t value)
	{
		___numVirtualVoices_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONFIGURATION_T4040042187_H
#ifndef JSONDATA_T1524858407_H
#define JSONDATA_T1524858407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonData
struct  JsonData_t1524858407  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<LitJson.JsonData> LitJson.JsonData::inst_array
	RuntimeObject* ___inst_array_0;
	// System.Boolean LitJson.JsonData::inst_boolean
	bool ___inst_boolean_1;
	// System.Double LitJson.JsonData::inst_double
	double ___inst_double_2;
	// System.Int32 LitJson.JsonData::inst_int
	int32_t ___inst_int_3;
	// System.Int64 LitJson.JsonData::inst_long
	int64_t ___inst_long_4;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData> LitJson.JsonData::inst_object
	RuntimeObject* ___inst_object_5;
	// System.String LitJson.JsonData::inst_string
	String_t* ___inst_string_6;
	// System.String LitJson.JsonData::json
	String_t* ___json_7;
	// LitJson.JsonType LitJson.JsonData::type
	int32_t ___type_8;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.JsonData::object_list
	RuntimeObject* ___object_list_9;

public:
	inline static int32_t get_offset_of_inst_array_0() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_array_0)); }
	inline RuntimeObject* get_inst_array_0() const { return ___inst_array_0; }
	inline RuntimeObject** get_address_of_inst_array_0() { return &___inst_array_0; }
	inline void set_inst_array_0(RuntimeObject* value)
	{
		___inst_array_0 = value;
		Il2CppCodeGenWriteBarrier((&___inst_array_0), value);
	}

	inline static int32_t get_offset_of_inst_boolean_1() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_boolean_1)); }
	inline bool get_inst_boolean_1() const { return ___inst_boolean_1; }
	inline bool* get_address_of_inst_boolean_1() { return &___inst_boolean_1; }
	inline void set_inst_boolean_1(bool value)
	{
		___inst_boolean_1 = value;
	}

	inline static int32_t get_offset_of_inst_double_2() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_double_2)); }
	inline double get_inst_double_2() const { return ___inst_double_2; }
	inline double* get_address_of_inst_double_2() { return &___inst_double_2; }
	inline void set_inst_double_2(double value)
	{
		___inst_double_2 = value;
	}

	inline static int32_t get_offset_of_inst_int_3() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_int_3)); }
	inline int32_t get_inst_int_3() const { return ___inst_int_3; }
	inline int32_t* get_address_of_inst_int_3() { return &___inst_int_3; }
	inline void set_inst_int_3(int32_t value)
	{
		___inst_int_3 = value;
	}

	inline static int32_t get_offset_of_inst_long_4() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_long_4)); }
	inline int64_t get_inst_long_4() const { return ___inst_long_4; }
	inline int64_t* get_address_of_inst_long_4() { return &___inst_long_4; }
	inline void set_inst_long_4(int64_t value)
	{
		___inst_long_4 = value;
	}

	inline static int32_t get_offset_of_inst_object_5() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_object_5)); }
	inline RuntimeObject* get_inst_object_5() const { return ___inst_object_5; }
	inline RuntimeObject** get_address_of_inst_object_5() { return &___inst_object_5; }
	inline void set_inst_object_5(RuntimeObject* value)
	{
		___inst_object_5 = value;
		Il2CppCodeGenWriteBarrier((&___inst_object_5), value);
	}

	inline static int32_t get_offset_of_inst_string_6() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_string_6)); }
	inline String_t* get_inst_string_6() const { return ___inst_string_6; }
	inline String_t** get_address_of_inst_string_6() { return &___inst_string_6; }
	inline void set_inst_string_6(String_t* value)
	{
		___inst_string_6 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_6), value);
	}

	inline static int32_t get_offset_of_json_7() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___json_7)); }
	inline String_t* get_json_7() const { return ___json_7; }
	inline String_t** get_address_of_json_7() { return &___json_7; }
	inline void set_json_7(String_t* value)
	{
		___json_7 = value;
		Il2CppCodeGenWriteBarrier((&___json_7), value);
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___type_8)); }
	inline int32_t get_type_8() const { return ___type_8; }
	inline int32_t* get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(int32_t value)
	{
		___type_8 = value;
	}

	inline static int32_t get_offset_of_object_list_9() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___object_list_9)); }
	inline RuntimeObject* get_object_list_9() const { return ___object_list_9; }
	inline RuntimeObject** get_address_of_object_list_9() { return &___object_list_9; }
	inline void set_object_list_9(RuntimeObject* value)
	{
		___object_list_9 = value;
		Il2CppCodeGenWriteBarrier((&___object_list_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATA_T1524858407_H
#ifndef JSONREADER_T836887441_H
#define JSONREADER_T836887441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonReader
struct  JsonReader_t836887441  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<System.Int32> LitJson.JsonReader::automaton_stack
	Stack_1_t3794335208 * ___automaton_stack_1;
	// System.Int32 LitJson.JsonReader::current_input
	int32_t ___current_input_2;
	// System.Int32 LitJson.JsonReader::current_symbol
	int32_t ___current_symbol_3;
	// System.Boolean LitJson.JsonReader::end_of_json
	bool ___end_of_json_4;
	// System.Boolean LitJson.JsonReader::end_of_input
	bool ___end_of_input_5;
	// LitJson.Lexer LitJson.JsonReader::lexer
	Lexer_t1514038666 * ___lexer_6;
	// System.Boolean LitJson.JsonReader::parser_in_string
	bool ___parser_in_string_7;
	// System.Boolean LitJson.JsonReader::parser_return
	bool ___parser_return_8;
	// System.Boolean LitJson.JsonReader::read_started
	bool ___read_started_9;
	// System.IO.TextReader LitJson.JsonReader::reader
	TextReader_t283511965 * ___reader_10;
	// System.Boolean LitJson.JsonReader::reader_is_owned
	bool ___reader_is_owned_11;
	// System.Boolean LitJson.JsonReader::skip_non_members
	bool ___skip_non_members_12;
	// System.Object LitJson.JsonReader::token_value
	RuntimeObject * ___token_value_13;
	// LitJson.JsonToken LitJson.JsonReader::token
	int32_t ___token_14;

public:
	inline static int32_t get_offset_of_automaton_stack_1() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___automaton_stack_1)); }
	inline Stack_1_t3794335208 * get_automaton_stack_1() const { return ___automaton_stack_1; }
	inline Stack_1_t3794335208 ** get_address_of_automaton_stack_1() { return &___automaton_stack_1; }
	inline void set_automaton_stack_1(Stack_1_t3794335208 * value)
	{
		___automaton_stack_1 = value;
		Il2CppCodeGenWriteBarrier((&___automaton_stack_1), value);
	}

	inline static int32_t get_offset_of_current_input_2() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___current_input_2)); }
	inline int32_t get_current_input_2() const { return ___current_input_2; }
	inline int32_t* get_address_of_current_input_2() { return &___current_input_2; }
	inline void set_current_input_2(int32_t value)
	{
		___current_input_2 = value;
	}

	inline static int32_t get_offset_of_current_symbol_3() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___current_symbol_3)); }
	inline int32_t get_current_symbol_3() const { return ___current_symbol_3; }
	inline int32_t* get_address_of_current_symbol_3() { return &___current_symbol_3; }
	inline void set_current_symbol_3(int32_t value)
	{
		___current_symbol_3 = value;
	}

	inline static int32_t get_offset_of_end_of_json_4() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___end_of_json_4)); }
	inline bool get_end_of_json_4() const { return ___end_of_json_4; }
	inline bool* get_address_of_end_of_json_4() { return &___end_of_json_4; }
	inline void set_end_of_json_4(bool value)
	{
		___end_of_json_4 = value;
	}

	inline static int32_t get_offset_of_end_of_input_5() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___end_of_input_5)); }
	inline bool get_end_of_input_5() const { return ___end_of_input_5; }
	inline bool* get_address_of_end_of_input_5() { return &___end_of_input_5; }
	inline void set_end_of_input_5(bool value)
	{
		___end_of_input_5 = value;
	}

	inline static int32_t get_offset_of_lexer_6() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___lexer_6)); }
	inline Lexer_t1514038666 * get_lexer_6() const { return ___lexer_6; }
	inline Lexer_t1514038666 ** get_address_of_lexer_6() { return &___lexer_6; }
	inline void set_lexer_6(Lexer_t1514038666 * value)
	{
		___lexer_6 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_6), value);
	}

	inline static int32_t get_offset_of_parser_in_string_7() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___parser_in_string_7)); }
	inline bool get_parser_in_string_7() const { return ___parser_in_string_7; }
	inline bool* get_address_of_parser_in_string_7() { return &___parser_in_string_7; }
	inline void set_parser_in_string_7(bool value)
	{
		___parser_in_string_7 = value;
	}

	inline static int32_t get_offset_of_parser_return_8() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___parser_return_8)); }
	inline bool get_parser_return_8() const { return ___parser_return_8; }
	inline bool* get_address_of_parser_return_8() { return &___parser_return_8; }
	inline void set_parser_return_8(bool value)
	{
		___parser_return_8 = value;
	}

	inline static int32_t get_offset_of_read_started_9() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___read_started_9)); }
	inline bool get_read_started_9() const { return ___read_started_9; }
	inline bool* get_address_of_read_started_9() { return &___read_started_9; }
	inline void set_read_started_9(bool value)
	{
		___read_started_9 = value;
	}

	inline static int32_t get_offset_of_reader_10() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___reader_10)); }
	inline TextReader_t283511965 * get_reader_10() const { return ___reader_10; }
	inline TextReader_t283511965 ** get_address_of_reader_10() { return &___reader_10; }
	inline void set_reader_10(TextReader_t283511965 * value)
	{
		___reader_10 = value;
		Il2CppCodeGenWriteBarrier((&___reader_10), value);
	}

	inline static int32_t get_offset_of_reader_is_owned_11() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___reader_is_owned_11)); }
	inline bool get_reader_is_owned_11() const { return ___reader_is_owned_11; }
	inline bool* get_address_of_reader_is_owned_11() { return &___reader_is_owned_11; }
	inline void set_reader_is_owned_11(bool value)
	{
		___reader_is_owned_11 = value;
	}

	inline static int32_t get_offset_of_skip_non_members_12() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___skip_non_members_12)); }
	inline bool get_skip_non_members_12() const { return ___skip_non_members_12; }
	inline bool* get_address_of_skip_non_members_12() { return &___skip_non_members_12; }
	inline void set_skip_non_members_12(bool value)
	{
		___skip_non_members_12 = value;
	}

	inline static int32_t get_offset_of_token_value_13() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___token_value_13)); }
	inline RuntimeObject * get_token_value_13() const { return ___token_value_13; }
	inline RuntimeObject ** get_address_of_token_value_13() { return &___token_value_13; }
	inline void set_token_value_13(RuntimeObject * value)
	{
		___token_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___token_value_13), value);
	}

	inline static int32_t get_offset_of_token_14() { return static_cast<int32_t>(offsetof(JsonReader_t836887441, ___token_14)); }
	inline int32_t get_token_14() const { return ___token_14; }
	inline int32_t* get_address_of_token_14() { return &___token_14; }
	inline void set_token_14(int32_t value)
	{
		___token_14 = value;
	}
};

struct JsonReader_t836887441_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>> LitJson.JsonReader::parse_table
	RuntimeObject* ___parse_table_0;

public:
	inline static int32_t get_offset_of_parse_table_0() { return static_cast<int32_t>(offsetof(JsonReader_t836887441_StaticFields, ___parse_table_0)); }
	inline RuntimeObject* get_parse_table_0() const { return ___parse_table_0; }
	inline RuntimeObject** get_address_of_parse_table_0() { return &___parse_table_0; }
	inline void set_parse_table_0(RuntimeObject* value)
	{
		___parse_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___parse_table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T836887441_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef WAVEFORMAT_T2842237185_H
#define WAVEFORMAT_T2842237185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormat
#pragma pack(push, tp, 2)
struct  WaveFormat_t2842237185  : public RuntimeObject
{
public:
	// NAudio.Wave.WZT.WaveFormatEncoding NAudio.Wave.WZT.WaveFormat::waveFormatTag
	uint16_t ___waveFormatTag_0;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::channels
	int16_t ___channels_1;
	// System.Int32 NAudio.Wave.WZT.WaveFormat::sampleRate
	int32_t ___sampleRate_2;
	// System.Int32 NAudio.Wave.WZT.WaveFormat::averageBytesPerSecond
	int32_t ___averageBytesPerSecond_3;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::blockAlign
	int16_t ___blockAlign_4;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::bitsPerSample
	int16_t ___bitsPerSample_5;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::extraSize
	int16_t ___extraSize_6;

public:
	inline static int32_t get_offset_of_waveFormatTag_0() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___waveFormatTag_0)); }
	inline uint16_t get_waveFormatTag_0() const { return ___waveFormatTag_0; }
	inline uint16_t* get_address_of_waveFormatTag_0() { return &___waveFormatTag_0; }
	inline void set_waveFormatTag_0(uint16_t value)
	{
		___waveFormatTag_0 = value;
	}

	inline static int32_t get_offset_of_channels_1() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___channels_1)); }
	inline int16_t get_channels_1() const { return ___channels_1; }
	inline int16_t* get_address_of_channels_1() { return &___channels_1; }
	inline void set_channels_1(int16_t value)
	{
		___channels_1 = value;
	}

	inline static int32_t get_offset_of_sampleRate_2() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___sampleRate_2)); }
	inline int32_t get_sampleRate_2() const { return ___sampleRate_2; }
	inline int32_t* get_address_of_sampleRate_2() { return &___sampleRate_2; }
	inline void set_sampleRate_2(int32_t value)
	{
		___sampleRate_2 = value;
	}

	inline static int32_t get_offset_of_averageBytesPerSecond_3() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___averageBytesPerSecond_3)); }
	inline int32_t get_averageBytesPerSecond_3() const { return ___averageBytesPerSecond_3; }
	inline int32_t* get_address_of_averageBytesPerSecond_3() { return &___averageBytesPerSecond_3; }
	inline void set_averageBytesPerSecond_3(int32_t value)
	{
		___averageBytesPerSecond_3 = value;
	}

	inline static int32_t get_offset_of_blockAlign_4() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___blockAlign_4)); }
	inline int16_t get_blockAlign_4() const { return ___blockAlign_4; }
	inline int16_t* get_address_of_blockAlign_4() { return &___blockAlign_4; }
	inline void set_blockAlign_4(int16_t value)
	{
		___blockAlign_4 = value;
	}

	inline static int32_t get_offset_of_bitsPerSample_5() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___bitsPerSample_5)); }
	inline int16_t get_bitsPerSample_5() const { return ___bitsPerSample_5; }
	inline int16_t* get_address_of_bitsPerSample_5() { return &___bitsPerSample_5; }
	inline void set_bitsPerSample_5(int16_t value)
	{
		___bitsPerSample_5 = value;
	}

	inline static int32_t get_offset_of_extraSize_6() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___extraSize_6)); }
	inline int16_t get_extraSize_6() const { return ___extraSize_6; }
	inline int16_t* get_address_of_extraSize_6() { return &___extraSize_6; }
	inline void set_extraSize_6(int16_t value)
	{
		___extraSize_6 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.WaveFormat
#pragma pack(push, tp, 2)
struct WaveFormat_t2842237185_marshaled_pinvoke
{
	uint16_t ___waveFormatTag_0;
	int16_t ___channels_1;
	int32_t ___sampleRate_2;
	int32_t ___averageBytesPerSecond_3;
	int16_t ___blockAlign_4;
	int16_t ___bitsPerSample_5;
	int16_t ___extraSize_6;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.WaveFormat
#pragma pack(push, tp, 2)
struct WaveFormat_t2842237185_marshaled_com
{
	uint16_t ___waveFormatTag_0;
	int16_t ___channels_1;
	int32_t ___sampleRate_2;
	int32_t ___averageBytesPerSecond_3;
	int16_t ___blockAlign_4;
	int16_t ___bitsPerSample_5;
	int16_t ___extraSize_6;
};
#pragma pack(pop, tp)
#endif // WAVEFORMAT_T2842237185_H
#ifndef WAVEFILEREADER_T3404544520_H
#define WAVEFILEREADER_T3404544520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFileReader
struct  WaveFileReader_t3404544520  : public WaveStream_t91297746
{
public:
	// NAudio.Wave.WZT.WaveFormat NAudio.Wave.WZT.WaveFileReader::waveFormat
	WaveFormat_t2842237185 * ___waveFormat_4;
	// System.IO.Stream NAudio.Wave.WZT.WaveFileReader::waveStream
	Stream_t1273022909 * ___waveStream_5;
	// System.Boolean NAudio.Wave.WZT.WaveFileReader::ownInput
	bool ___ownInput_6;
	// System.Int64 NAudio.Wave.WZT.WaveFileReader::dataPosition
	int64_t ___dataPosition_7;
	// System.Int32 NAudio.Wave.WZT.WaveFileReader::dataChunkLength
	int32_t ___dataChunkLength_8;
	// System.Collections.Generic.List`1<NAudio.Wave.WZT.RiffChunk> NAudio.Wave.WZT.WaveFileReader::chunks
	List_1_t1567347887 * ___chunks_9;

public:
	inline static int32_t get_offset_of_waveFormat_4() { return static_cast<int32_t>(offsetof(WaveFileReader_t3404544520, ___waveFormat_4)); }
	inline WaveFormat_t2842237185 * get_waveFormat_4() const { return ___waveFormat_4; }
	inline WaveFormat_t2842237185 ** get_address_of_waveFormat_4() { return &___waveFormat_4; }
	inline void set_waveFormat_4(WaveFormat_t2842237185 * value)
	{
		___waveFormat_4 = value;
		Il2CppCodeGenWriteBarrier((&___waveFormat_4), value);
	}

	inline static int32_t get_offset_of_waveStream_5() { return static_cast<int32_t>(offsetof(WaveFileReader_t3404544520, ___waveStream_5)); }
	inline Stream_t1273022909 * get_waveStream_5() const { return ___waveStream_5; }
	inline Stream_t1273022909 ** get_address_of_waveStream_5() { return &___waveStream_5; }
	inline void set_waveStream_5(Stream_t1273022909 * value)
	{
		___waveStream_5 = value;
		Il2CppCodeGenWriteBarrier((&___waveStream_5), value);
	}

	inline static int32_t get_offset_of_ownInput_6() { return static_cast<int32_t>(offsetof(WaveFileReader_t3404544520, ___ownInput_6)); }
	inline bool get_ownInput_6() const { return ___ownInput_6; }
	inline bool* get_address_of_ownInput_6() { return &___ownInput_6; }
	inline void set_ownInput_6(bool value)
	{
		___ownInput_6 = value;
	}

	inline static int32_t get_offset_of_dataPosition_7() { return static_cast<int32_t>(offsetof(WaveFileReader_t3404544520, ___dataPosition_7)); }
	inline int64_t get_dataPosition_7() const { return ___dataPosition_7; }
	inline int64_t* get_address_of_dataPosition_7() { return &___dataPosition_7; }
	inline void set_dataPosition_7(int64_t value)
	{
		___dataPosition_7 = value;
	}

	inline static int32_t get_offset_of_dataChunkLength_8() { return static_cast<int32_t>(offsetof(WaveFileReader_t3404544520, ___dataChunkLength_8)); }
	inline int32_t get_dataChunkLength_8() const { return ___dataChunkLength_8; }
	inline int32_t* get_address_of_dataChunkLength_8() { return &___dataChunkLength_8; }
	inline void set_dataChunkLength_8(int32_t value)
	{
		___dataChunkLength_8 = value;
	}

	inline static int32_t get_offset_of_chunks_9() { return static_cast<int32_t>(offsetof(WaveFileReader_t3404544520, ___chunks_9)); }
	inline List_1_t1567347887 * get_chunks_9() const { return ___chunks_9; }
	inline List_1_t1567347887 ** get_address_of_chunks_9() { return &___chunks_9; }
	inline void set_chunks_9(List_1_t1567347887 * value)
	{
		___chunks_9 = value;
		Il2CppCodeGenWriteBarrier((&___chunks_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEFILEREADER_T3404544520_H
#ifndef U3C_WAVETOMP3U3EC__ITERATOR0_T3429961031_H
#define U3C_WAVETOMP3U3EC__ITERATOR0_T3429961031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0
struct  U3C_WaveToMP3U3Ec__Iterator0_t3429961031  : public RuntimeObject
{
public:
	// System.String NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::waveFileName
	String_t* ___waveFileName_0;
	// NAudio.Wave.WZT.WaveFileReader NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::<reader>__1
	WaveFileReader_t3404544520 * ___U3CreaderU3E__1_1;
	// System.String NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::mp3FileName
	String_t* ___mp3FileName_2;
	// NAudio.Lame.LAMEPreset NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::bitRate
	int32_t ___bitRate_3;
	// NAudio.Lame.LameMP3FileWriter NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::<writer>__2
	LameMP3FileWriter_t44674093 * ___U3CwriterU3E__2_4;
	// System.Action NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::OnComplete
	Action_t1264377477 * ___OnComplete_5;
	// System.Object NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 NAudio.Example.MP3EncoderWrapper/<_WaveToMP3>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_waveFileName_0() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___waveFileName_0)); }
	inline String_t* get_waveFileName_0() const { return ___waveFileName_0; }
	inline String_t** get_address_of_waveFileName_0() { return &___waveFileName_0; }
	inline void set_waveFileName_0(String_t* value)
	{
		___waveFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___waveFileName_0), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E__1_1() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___U3CreaderU3E__1_1)); }
	inline WaveFileReader_t3404544520 * get_U3CreaderU3E__1_1() const { return ___U3CreaderU3E__1_1; }
	inline WaveFileReader_t3404544520 ** get_address_of_U3CreaderU3E__1_1() { return &___U3CreaderU3E__1_1; }
	inline void set_U3CreaderU3E__1_1(WaveFileReader_t3404544520 * value)
	{
		___U3CreaderU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E__1_1), value);
	}

	inline static int32_t get_offset_of_mp3FileName_2() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___mp3FileName_2)); }
	inline String_t* get_mp3FileName_2() const { return ___mp3FileName_2; }
	inline String_t** get_address_of_mp3FileName_2() { return &___mp3FileName_2; }
	inline void set_mp3FileName_2(String_t* value)
	{
		___mp3FileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mp3FileName_2), value);
	}

	inline static int32_t get_offset_of_bitRate_3() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___bitRate_3)); }
	inline int32_t get_bitRate_3() const { return ___bitRate_3; }
	inline int32_t* get_address_of_bitRate_3() { return &___bitRate_3; }
	inline void set_bitRate_3(int32_t value)
	{
		___bitRate_3 = value;
	}

	inline static int32_t get_offset_of_U3CwriterU3E__2_4() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___U3CwriterU3E__2_4)); }
	inline LameMP3FileWriter_t44674093 * get_U3CwriterU3E__2_4() const { return ___U3CwriterU3E__2_4; }
	inline LameMP3FileWriter_t44674093 ** get_address_of_U3CwriterU3E__2_4() { return &___U3CwriterU3E__2_4; }
	inline void set_U3CwriterU3E__2_4(LameMP3FileWriter_t44674093 * value)
	{
		___U3CwriterU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwriterU3E__2_4), value);
	}

	inline static int32_t get_offset_of_OnComplete_5() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___OnComplete_5)); }
	inline Action_t1264377477 * get_OnComplete_5() const { return ___OnComplete_5; }
	inline Action_t1264377477 ** get_address_of_OnComplete_5() { return &___OnComplete_5; }
	inline void set_OnComplete_5(Action_t1264377477 * value)
	{
		___OnComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3C_WaveToMP3U3Ec__Iterator0_t3429961031, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_WAVETOMP3U3EC__ITERATOR0_T3429961031_H
#ifndef STATEHANDLER_T105866779_H
#define STATEHANDLER_T105866779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Lexer/StateHandler
struct  StateHandler_t105866779  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEHANDLER_T105866779_H
#ifndef DELREPORTFUNCTION_T2317660497_H
#define DELREPORTFUNCTION_T2317660497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.DLL.LibMp3Lame/NativeMethods/delReportFunction
struct  delReportFunction_t2317660497  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELREPORTFUNCTION_T2317660497_H
#ifndef WRAPPERFACTORY_T2158548929_H
#define WRAPPERFACTORY_T2158548929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.WrapperFactory
struct  WrapperFactory_t2158548929  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERFACTORY_T2158548929_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef WAVEFORMATEXTRADATA_T1489494699_H
#define WAVEFORMATEXTRADATA_T1489494699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormatExtraData
struct  WaveFormatExtraData_t1489494699  : public WaveFormat_t2842237185
{
public:
	// System.Byte[] NAudio.Wave.WZT.WaveFormatExtraData::extraData
	ByteU5BU5D_t4116647657* ___extraData_7;

public:
	inline static int32_t get_offset_of_extraData_7() { return static_cast<int32_t>(offsetof(WaveFormatExtraData_t1489494699, ___extraData_7)); }
	inline ByteU5BU5D_t4116647657* get_extraData_7() const { return ___extraData_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_extraData_7() { return &___extraData_7; }
	inline void set_extraData_7(ByteU5BU5D_t4116647657* value)
	{
		___extraData_7 = value;
		Il2CppCodeGenWriteBarrier((&___extraData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.WaveFormatExtraData
#pragma pack(push, tp, 2)
struct WaveFormatExtraData_t1489494699_marshaled_pinvoke : public WaveFormat_t2842237185_marshaled_pinvoke
{
	uint8_t ___extraData_7[100];
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.WaveFormatExtraData
#pragma pack(push, tp, 2)
struct WaveFormatExtraData_t1489494699_marshaled_com : public WaveFormat_t2842237185_marshaled_com
{
	uint8_t ___extraData_7[100];
};
#pragma pack(pop, tp)
#endif // WAVEFORMATEXTRADATA_T1489494699_H
#ifndef PLAYABLEASSET_T3219022681_H
#define PLAYABLEASSET_T3219022681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t3219022681  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T3219022681_H
#ifndef GSM610WAVEFORMAT_T3021274509_H
#define GSM610WAVEFORMAT_T3021274509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.Gsm610WaveFormat
#pragma pack(push, tp, 2)
struct  Gsm610WaveFormat_t3021274509  : public WaveFormat_t2842237185
{
public:
	// System.Int16 NAudio.Wave.WZT.Gsm610WaveFormat::samplesPerBlock
	int16_t ___samplesPerBlock_7;

public:
	inline static int32_t get_offset_of_samplesPerBlock_7() { return static_cast<int32_t>(offsetof(Gsm610WaveFormat_t3021274509, ___samplesPerBlock_7)); }
	inline int16_t get_samplesPerBlock_7() const { return ___samplesPerBlock_7; }
	inline int16_t* get_address_of_samplesPerBlock_7() { return &___samplesPerBlock_7; }
	inline void set_samplesPerBlock_7(int16_t value)
	{
		___samplesPerBlock_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.Gsm610WaveFormat
#pragma pack(push, tp, 2)
struct Gsm610WaveFormat_t3021274509_marshaled_pinvoke : public WaveFormat_t2842237185_marshaled_pinvoke
{
	int16_t ___samplesPerBlock_7;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.Gsm610WaveFormat
#pragma pack(push, tp, 2)
struct Gsm610WaveFormat_t3021274509_marshaled_com : public WaveFormat_t2842237185_marshaled_com
{
	int16_t ___samplesPerBlock_7;
};
#pragma pack(pop, tp)
#endif // GSM610WAVEFORMAT_T3021274509_H
#ifndef PUZZLECONTROLLEREVENTHANDLER_T963335703_H
#define PUZZLECONTROLLEREVENTHANDLER_T963335703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController/PuzzleControllerEventHandler
struct  PuzzleControllerEventHandler_t963335703  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLECONTROLLEREVENTHANDLER_T963335703_H
#ifndef ADPCMWAVEFORMAT_T3207843846_H
#define ADPCMWAVEFORMAT_T3207843846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.AdpcmWaveFormat
struct  AdpcmWaveFormat_t3207843846  : public WaveFormat_t2842237185
{
public:
	// System.Int16 NAudio.Wave.WZT.AdpcmWaveFormat::samplesPerBlock
	int16_t ___samplesPerBlock_7;
	// System.Int16 NAudio.Wave.WZT.AdpcmWaveFormat::numCoeff
	int16_t ___numCoeff_8;
	// System.Int16[] NAudio.Wave.WZT.AdpcmWaveFormat::coefficients
	Int16U5BU5D_t3686840178* ___coefficients_9;

public:
	inline static int32_t get_offset_of_samplesPerBlock_7() { return static_cast<int32_t>(offsetof(AdpcmWaveFormat_t3207843846, ___samplesPerBlock_7)); }
	inline int16_t get_samplesPerBlock_7() const { return ___samplesPerBlock_7; }
	inline int16_t* get_address_of_samplesPerBlock_7() { return &___samplesPerBlock_7; }
	inline void set_samplesPerBlock_7(int16_t value)
	{
		___samplesPerBlock_7 = value;
	}

	inline static int32_t get_offset_of_numCoeff_8() { return static_cast<int32_t>(offsetof(AdpcmWaveFormat_t3207843846, ___numCoeff_8)); }
	inline int16_t get_numCoeff_8() const { return ___numCoeff_8; }
	inline int16_t* get_address_of_numCoeff_8() { return &___numCoeff_8; }
	inline void set_numCoeff_8(int16_t value)
	{
		___numCoeff_8 = value;
	}

	inline static int32_t get_offset_of_coefficients_9() { return static_cast<int32_t>(offsetof(AdpcmWaveFormat_t3207843846, ___coefficients_9)); }
	inline Int16U5BU5D_t3686840178* get_coefficients_9() const { return ___coefficients_9; }
	inline Int16U5BU5D_t3686840178** get_address_of_coefficients_9() { return &___coefficients_9; }
	inline void set_coefficients_9(Int16U5BU5D_t3686840178* value)
	{
		___coefficients_9 = value;
		Il2CppCodeGenWriteBarrier((&___coefficients_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.AdpcmWaveFormat
#pragma pack(push, tp, 2)
struct AdpcmWaveFormat_t3207843846_marshaled_pinvoke : public WaveFormat_t2842237185_marshaled_pinvoke
{
	int16_t ___samplesPerBlock_7;
	int16_t ___numCoeff_8;
	int16_t ___coefficients_9[14];
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.AdpcmWaveFormat
#pragma pack(push, tp, 2)
struct AdpcmWaveFormat_t3207843846_marshaled_com : public WaveFormat_t2842237185_marshaled_com
{
	int16_t ___samplesPerBlock_7;
	int16_t ___numCoeff_8;
	int16_t ___coefficients_9[14];
};
#pragma pack(pop, tp)
#endif // ADPCMWAVEFORMAT_T3207843846_H
#ifndef IMPORTERFUNC_T3630937194_H
#define IMPORTERFUNC_T3630937194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ImporterFunc
struct  ImporterFunc_t3630937194  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTERFUNC_T3630937194_H
#ifndef PUZZLETOKENEVENTHANDLER_T3807414068_H
#define PUZZLETOKENEVENTHANDLER_T3807414068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleToken/PuzzleTokenEventHandler
struct  PuzzleTokenEventHandler_t3807414068  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLETOKENEVENTHANDLER_T3807414068_H
#ifndef DELENCODE_T1434347660_H
#define DELENCODE_T1434347660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Lame.LameMP3FileWriter/delEncode
struct  delEncode_t1434347660  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELENCODE_T1434347660_H
#ifndef PUZZLEINSERTEVENTHANDLER_T3824397729_H
#define PUZZLEINSERTEVENTHANDLER_T3824397729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleInsert/PuzzleInsertEventHandler
struct  PuzzleInsertEventHandler_t3824397729  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEINSERTEVENTHANDLER_T3824397729_H
#ifndef EXPORTERFUNC_T1851311465_H
#define EXPORTERFUNC_T1851311465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ExporterFunc
struct  ExporterFunc_t1851311465  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPORTERFUNC_T1851311465_H
#ifndef WAVEFORMATEXTENSIBLE_T1054910509_H
#define WAVEFORMATEXTENSIBLE_T1054910509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormatExtensible
#pragma pack(push, tp, 2)
struct  WaveFormatExtensible_t1054910509  : public WaveFormat_t2842237185
{
public:
	// System.Int16 NAudio.Wave.WZT.WaveFormatExtensible::wValidBitsPerSample
	int16_t ___wValidBitsPerSample_7;
	// System.Int32 NAudio.Wave.WZT.WaveFormatExtensible::dwChannelMask
	int32_t ___dwChannelMask_8;
	// System.Guid NAudio.Wave.WZT.WaveFormatExtensible::subFormat
	Guid_t  ___subFormat_9;

public:
	inline static int32_t get_offset_of_wValidBitsPerSample_7() { return static_cast<int32_t>(offsetof(WaveFormatExtensible_t1054910509, ___wValidBitsPerSample_7)); }
	inline int16_t get_wValidBitsPerSample_7() const { return ___wValidBitsPerSample_7; }
	inline int16_t* get_address_of_wValidBitsPerSample_7() { return &___wValidBitsPerSample_7; }
	inline void set_wValidBitsPerSample_7(int16_t value)
	{
		___wValidBitsPerSample_7 = value;
	}

	inline static int32_t get_offset_of_dwChannelMask_8() { return static_cast<int32_t>(offsetof(WaveFormatExtensible_t1054910509, ___dwChannelMask_8)); }
	inline int32_t get_dwChannelMask_8() const { return ___dwChannelMask_8; }
	inline int32_t* get_address_of_dwChannelMask_8() { return &___dwChannelMask_8; }
	inline void set_dwChannelMask_8(int32_t value)
	{
		___dwChannelMask_8 = value;
	}

	inline static int32_t get_offset_of_subFormat_9() { return static_cast<int32_t>(offsetof(WaveFormatExtensible_t1054910509, ___subFormat_9)); }
	inline Guid_t  get_subFormat_9() const { return ___subFormat_9; }
	inline Guid_t * get_address_of_subFormat_9() { return &___subFormat_9; }
	inline void set_subFormat_9(Guid_t  value)
	{
		___subFormat_9 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.WaveFormatExtensible
#pragma pack(push, tp, 2)
struct WaveFormatExtensible_t1054910509_marshaled_pinvoke : public WaveFormat_t2842237185_marshaled_pinvoke
{
	int16_t ___wValidBitsPerSample_7;
	int32_t ___dwChannelMask_8;
	Guid_t  ___subFormat_9;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.WaveFormatExtensible
#pragma pack(push, tp, 2)
struct WaveFormatExtensible_t1054910509_marshaled_com : public WaveFormat_t2842237185_marshaled_com
{
	int16_t ___wValidBitsPerSample_7;
	int32_t ___dwChannelMask_8;
	Guid_t  ___subFormat_9;
};
#pragma pack(pop, tp)
#endif // WAVEFORMATEXTENSIBLE_T1054910509_H
#ifndef TRACKASSET_T2828708245_H
#define TRACKASSET_T2828708245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t2828708245  : public PlayableAsset_t3219022681
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_2;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_3;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_4;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t2318505987 * ___m_AnimClip_5;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t3219022681 * ___m_Parent_6;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t4000433264 * ___m_Children_7;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_8;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t140784555* ___m_ClipsCache_9;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t924799574  ___m_Start_10;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t924799574  ___m_End_11;
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackAsset::m_MediaType
	int32_t ___m_MediaType_12;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_ChildTrackCache
	RuntimeObject* ___m_ChildTrackCache_14;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_t3950300692 * ___m_Clips_16;

public:
	inline static int32_t get_offset_of_m_Locked_2() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Locked_2)); }
	inline bool get_m_Locked_2() const { return ___m_Locked_2; }
	inline bool* get_address_of_m_Locked_2() { return &___m_Locked_2; }
	inline void set_m_Locked_2(bool value)
	{
		___m_Locked_2 = value;
	}

	inline static int32_t get_offset_of_m_Muted_3() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Muted_3)); }
	inline bool get_m_Muted_3() const { return ___m_Muted_3; }
	inline bool* get_address_of_m_Muted_3() { return &___m_Muted_3; }
	inline void set_m_Muted_3(bool value)
	{
		___m_Muted_3 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_4() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_CustomPlayableFullTypename_4)); }
	inline String_t* get_m_CustomPlayableFullTypename_4() const { return ___m_CustomPlayableFullTypename_4; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_4() { return &___m_CustomPlayableFullTypename_4; }
	inline void set_m_CustomPlayableFullTypename_4(String_t* value)
	{
		___m_CustomPlayableFullTypename_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_4), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_5() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_AnimClip_5)); }
	inline AnimationClip_t2318505987 * get_m_AnimClip_5() const { return ___m_AnimClip_5; }
	inline AnimationClip_t2318505987 ** get_address_of_m_AnimClip_5() { return &___m_AnimClip_5; }
	inline void set_m_AnimClip_5(AnimationClip_t2318505987 * value)
	{
		___m_AnimClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_5), value);
	}

	inline static int32_t get_offset_of_m_Parent_6() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Parent_6)); }
	inline PlayableAsset_t3219022681 * get_m_Parent_6() const { return ___m_Parent_6; }
	inline PlayableAsset_t3219022681 ** get_address_of_m_Parent_6() { return &___m_Parent_6; }
	inline void set_m_Parent_6(PlayableAsset_t3219022681 * value)
	{
		___m_Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_6), value);
	}

	inline static int32_t get_offset_of_m_Children_7() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Children_7)); }
	inline List_1_t4000433264 * get_m_Children_7() const { return ___m_Children_7; }
	inline List_1_t4000433264 ** get_address_of_m_Children_7() { return &___m_Children_7; }
	inline void set_m_Children_7(List_1_t4000433264 * value)
	{
		___m_Children_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_7), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_8() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ItemsHash_8)); }
	inline int32_t get_m_ItemsHash_8() const { return ___m_ItemsHash_8; }
	inline int32_t* get_address_of_m_ItemsHash_8() { return &___m_ItemsHash_8; }
	inline void set_m_ItemsHash_8(int32_t value)
	{
		___m_ItemsHash_8 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_9() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ClipsCache_9)); }
	inline TimelineClipU5BU5D_t140784555* get_m_ClipsCache_9() const { return ___m_ClipsCache_9; }
	inline TimelineClipU5BU5D_t140784555** get_address_of_m_ClipsCache_9() { return &___m_ClipsCache_9; }
	inline void set_m_ClipsCache_9(TimelineClipU5BU5D_t140784555* value)
	{
		___m_ClipsCache_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_9), value);
	}

	inline static int32_t get_offset_of_m_Start_10() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Start_10)); }
	inline DiscreteTime_t924799574  get_m_Start_10() const { return ___m_Start_10; }
	inline DiscreteTime_t924799574 * get_address_of_m_Start_10() { return &___m_Start_10; }
	inline void set_m_Start_10(DiscreteTime_t924799574  value)
	{
		___m_Start_10 = value;
	}

	inline static int32_t get_offset_of_m_End_11() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_End_11)); }
	inline DiscreteTime_t924799574  get_m_End_11() const { return ___m_End_11; }
	inline DiscreteTime_t924799574 * get_address_of_m_End_11() { return &___m_End_11; }
	inline void set_m_End_11(DiscreteTime_t924799574  value)
	{
		___m_End_11 = value;
	}

	inline static int32_t get_offset_of_m_MediaType_12() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_MediaType_12)); }
	inline int32_t get_m_MediaType_12() const { return ___m_MediaType_12; }
	inline int32_t* get_address_of_m_MediaType_12() { return &___m_MediaType_12; }
	inline void set_m_MediaType_12(int32_t value)
	{
		___m_MediaType_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildTrackCache_14() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ChildTrackCache_14)); }
	inline RuntimeObject* get_m_ChildTrackCache_14() const { return ___m_ChildTrackCache_14; }
	inline RuntimeObject** get_address_of_m_ChildTrackCache_14() { return &___m_ChildTrackCache_14; }
	inline void set_m_ChildTrackCache_14(RuntimeObject* value)
	{
		___m_ChildTrackCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildTrackCache_14), value);
	}

	inline static int32_t get_offset_of_m_Clips_16() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Clips_16)); }
	inline List_1_t3950300692 * get_m_Clips_16() const { return ___m_Clips_16; }
	inline List_1_t3950300692 ** get_address_of_m_Clips_16() { return &___m_Clips_16; }
	inline void set_m_Clips_16(List_1_t3950300692 * value)
	{
		___m_Clips_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_16), value);
	}
};

struct TrackAsset_t2828708245_StaticFields
{
public:
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TrackAsset::s_EmptyCache
	TrackAssetU5BU5D_t52518008* ___s_EmptyCache_13;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t907029268 * ___s_TrackBindingTypeAttributeCache_15;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t2253157129 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_s_EmptyCache_13() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___s_EmptyCache_13)); }
	inline TrackAssetU5BU5D_t52518008* get_s_EmptyCache_13() const { return ___s_EmptyCache_13; }
	inline TrackAssetU5BU5D_t52518008** get_address_of_s_EmptyCache_13() { return &___s_EmptyCache_13; }
	inline void set_s_EmptyCache_13(TrackAssetU5BU5D_t52518008* value)
	{
		___s_EmptyCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyCache_13), value);
	}

	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_15() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___s_TrackBindingTypeAttributeCache_15)); }
	inline Dictionary_2_t907029268 * get_s_TrackBindingTypeAttributeCache_15() const { return ___s_TrackBindingTypeAttributeCache_15; }
	inline Dictionary_2_t907029268 ** get_address_of_s_TrackBindingTypeAttributeCache_15() { return &___s_TrackBindingTypeAttributeCache_15; }
	inline void set_s_TrackBindingTypeAttributeCache_15(Dictionary_2_t907029268 * value)
	{
		___s_TrackBindingTypeAttributeCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Comparison_1_t2253157129 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Comparison_1_t2253157129 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Comparison_1_t2253157129 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T2828708245_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef LISTSTORIES_T748560288_H
#define LISTSTORIES_T748560288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ListStories
struct  ListStories_t748560288  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager ListStories::manager
	QuestManager_t588401851 * ___manager_2;
	// AudioRecordManager ListStories::audioRecordManager
	AudioRecordManager_t3166594816 * ___audioRecordManager_3;
	// UnityEngine.Transform ListStories::contentPanel
	Transform_t3600365921 * ___contentPanel_4;
	// UnityEngine.GameObject ListStories::panelPrefab
	GameObject_t1113636619 * ___panelPrefab_5;
	// UnityEngine.GameObject ListStories::panel
	GameObject_t1113636619 * ___panel_6;
	// System.Int32 ListStories::count
	int32_t ___count_7;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(ListStories_t748560288, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_audioRecordManager_3() { return static_cast<int32_t>(offsetof(ListStories_t748560288, ___audioRecordManager_3)); }
	inline AudioRecordManager_t3166594816 * get_audioRecordManager_3() const { return ___audioRecordManager_3; }
	inline AudioRecordManager_t3166594816 ** get_address_of_audioRecordManager_3() { return &___audioRecordManager_3; }
	inline void set_audioRecordManager_3(AudioRecordManager_t3166594816 * value)
	{
		___audioRecordManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___audioRecordManager_3), value);
	}

	inline static int32_t get_offset_of_contentPanel_4() { return static_cast<int32_t>(offsetof(ListStories_t748560288, ___contentPanel_4)); }
	inline Transform_t3600365921 * get_contentPanel_4() const { return ___contentPanel_4; }
	inline Transform_t3600365921 ** get_address_of_contentPanel_4() { return &___contentPanel_4; }
	inline void set_contentPanel_4(Transform_t3600365921 * value)
	{
		___contentPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___contentPanel_4), value);
	}

	inline static int32_t get_offset_of_panelPrefab_5() { return static_cast<int32_t>(offsetof(ListStories_t748560288, ___panelPrefab_5)); }
	inline GameObject_t1113636619 * get_panelPrefab_5() const { return ___panelPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_panelPrefab_5() { return &___panelPrefab_5; }
	inline void set_panelPrefab_5(GameObject_t1113636619 * value)
	{
		___panelPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___panelPrefab_5), value);
	}

	inline static int32_t get_offset_of_panel_6() { return static_cast<int32_t>(offsetof(ListStories_t748560288, ___panel_6)); }
	inline GameObject_t1113636619 * get_panel_6() const { return ___panel_6; }
	inline GameObject_t1113636619 ** get_address_of_panel_6() { return &___panel_6; }
	inline void set_panel_6(GameObject_t1113636619 * value)
	{
		___panel_6 = value;
		Il2CppCodeGenWriteBarrier((&___panel_6), value);
	}

	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(ListStories_t748560288, ___count_7)); }
	inline int32_t get_count_7() const { return ___count_7; }
	inline int32_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(int32_t value)
	{
		___count_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTSTORIES_T748560288_H
#ifndef LEVELINTERACTIONS_T1896269609_H
#define LEVELINTERACTIONS_T1896269609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelInteractions
struct  LevelInteractions_t1896269609  : public MonoBehaviour_t3962482529
{
public:
	// SceneChanger LevelInteractions::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_2;
	// UnityEngine.GameObject LevelInteractions::person
	GameObject_t1113636619 * ___person_3;

public:
	inline static int32_t get_offset_of_sceneChanger_2() { return static_cast<int32_t>(offsetof(LevelInteractions_t1896269609, ___sceneChanger_2)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_2() const { return ___sceneChanger_2; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_2() { return &___sceneChanger_2; }
	inline void set_sceneChanger_2(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_2 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_2), value);
	}

	inline static int32_t get_offset_of_person_3() { return static_cast<int32_t>(offsetof(LevelInteractions_t1896269609, ___person_3)); }
	inline GameObject_t1113636619 * get_person_3() const { return ___person_3; }
	inline GameObject_t1113636619 ** get_address_of_person_3() { return &___person_3; }
	inline void set_person_3(GameObject_t1113636619 * value)
	{
		___person_3 = value;
		Il2CppCodeGenWriteBarrier((&___person_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELINTERACTIONS_T1896269609_H
#ifndef LEVELBEHAVIOR_T1802778136_H
#define LEVELBEHAVIOR_T1802778136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelBehavior
struct  LevelBehavior_t1802778136  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 LevelBehavior::index
	int32_t ___index_2;
	// UnityEngine.GameObject LevelBehavior::person
	GameObject_t1113636619 * ___person_3;
	// System.Boolean LevelBehavior::completed
	bool ___completed_4;
	// System.Boolean LevelBehavior::active
	bool ___active_5;
	// UnityEngine.Sprite[] LevelBehavior::buttons
	SpriteU5BU5D_t2581906349* ___buttons_6;
	// UnityEngine.SpriteRenderer LevelBehavior::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_7;
	// QuestManager LevelBehavior::questManager
	QuestManager_t588401851 * ___questManager_8;
	// UnityEngine.Transform LevelBehavior::level
	Transform_t3600365921 * ___level_9;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_person_3() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___person_3)); }
	inline GameObject_t1113636619 * get_person_3() const { return ___person_3; }
	inline GameObject_t1113636619 ** get_address_of_person_3() { return &___person_3; }
	inline void set_person_3(GameObject_t1113636619 * value)
	{
		___person_3 = value;
		Il2CppCodeGenWriteBarrier((&___person_3), value);
	}

	inline static int32_t get_offset_of_completed_4() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___completed_4)); }
	inline bool get_completed_4() const { return ___completed_4; }
	inline bool* get_address_of_completed_4() { return &___completed_4; }
	inline void set_completed_4(bool value)
	{
		___completed_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_buttons_6() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___buttons_6)); }
	inline SpriteU5BU5D_t2581906349* get_buttons_6() const { return ___buttons_6; }
	inline SpriteU5BU5D_t2581906349** get_address_of_buttons_6() { return &___buttons_6; }
	inline void set_buttons_6(SpriteU5BU5D_t2581906349* value)
	{
		___buttons_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_6), value);
	}

	inline static int32_t get_offset_of_spriteRenderer_7() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___spriteRenderer_7)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_7() const { return ___spriteRenderer_7; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_7() { return &___spriteRenderer_7; }
	inline void set_spriteRenderer_7(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_7), value);
	}

	inline static int32_t get_offset_of_questManager_8() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___questManager_8)); }
	inline QuestManager_t588401851 * get_questManager_8() const { return ___questManager_8; }
	inline QuestManager_t588401851 ** get_address_of_questManager_8() { return &___questManager_8; }
	inline void set_questManager_8(QuestManager_t588401851 * value)
	{
		___questManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___questManager_8), value);
	}

	inline static int32_t get_offset_of_level_9() { return static_cast<int32_t>(offsetof(LevelBehavior_t1802778136, ___level_9)); }
	inline Transform_t3600365921 * get_level_9() const { return ___level_9; }
	inline Transform_t3600365921 ** get_address_of_level_9() { return &___level_9; }
	inline void set_level_9(Transform_t3600365921 * value)
	{
		___level_9 = value;
		Il2CppCodeGenWriteBarrier((&___level_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELBEHAVIOR_T1802778136_H
#ifndef GAMESETTINGS_T2345380323_H
#define GAMESETTINGS_T2345380323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSettings
struct  GameSettings_t2345380323  : public MonoBehaviour_t3962482529
{
public:
	// GameSettings/Question[] GameSettings::questions
	QuestionU5BU5D_t564637768* ___questions_2;

public:
	inline static int32_t get_offset_of_questions_2() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___questions_2)); }
	inline QuestionU5BU5D_t564637768* get_questions_2() const { return ___questions_2; }
	inline QuestionU5BU5D_t564637768** get_address_of_questions_2() { return &___questions_2; }
	inline void set_questions_2(QuestionU5BU5D_t564637768* value)
	{
		___questions_2 = value;
		Il2CppCodeGenWriteBarrier((&___questions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESETTINGS_T2345380323_H
#ifndef CINEMACHINETRACK_T2126039464_H
#define CINEMACHINETRACK_T2126039464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cinemachine.Timeline.CinemachineTrack
struct  CinemachineTrack_t2126039464  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CINEMACHINETRACK_T2126039464_H
#ifndef DONTDESTROYONLOAD_T1456007215_H
#define DONTDESTROYONLOAD_T1456007215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyOnLoad
struct  DontDestroyOnLoad_t1456007215  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_T1456007215_H
#ifndef DATACONTROLLER_T353634109_H
#define DATACONTROLLER_T353634109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataController
struct  DataController_t353634109  : public MonoBehaviour_t3962482529
{
public:
	// PlayerData DataController::playerData
	PlayerData_t220878115 * ___playerData_2;
	// GameData DataController::gameData
	GameData_t415813024 * ___gameData_3;
	// System.String DataController::gameDataFileName
	String_t* ___gameDataFileName_4;
	// System.String DataController::playerDataFileName
	String_t* ___playerDataFileName_5;
	// System.Int32 DataController::isFirstTime
	int32_t ___isFirstTime_6;
	// System.Int32 DataController::currentLevel
	int32_t ___currentLevel_7;
	// QuestManager DataController::manager
	QuestManager_t588401851 * ___manager_8;
	// Quest DataController::quest
	Quest_t3696879532 * ___quest_9;

public:
	inline static int32_t get_offset_of_playerData_2() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___playerData_2)); }
	inline PlayerData_t220878115 * get_playerData_2() const { return ___playerData_2; }
	inline PlayerData_t220878115 ** get_address_of_playerData_2() { return &___playerData_2; }
	inline void set_playerData_2(PlayerData_t220878115 * value)
	{
		___playerData_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerData_2), value);
	}

	inline static int32_t get_offset_of_gameData_3() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___gameData_3)); }
	inline GameData_t415813024 * get_gameData_3() const { return ___gameData_3; }
	inline GameData_t415813024 ** get_address_of_gameData_3() { return &___gameData_3; }
	inline void set_gameData_3(GameData_t415813024 * value)
	{
		___gameData_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameData_3), value);
	}

	inline static int32_t get_offset_of_gameDataFileName_4() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___gameDataFileName_4)); }
	inline String_t* get_gameDataFileName_4() const { return ___gameDataFileName_4; }
	inline String_t** get_address_of_gameDataFileName_4() { return &___gameDataFileName_4; }
	inline void set_gameDataFileName_4(String_t* value)
	{
		___gameDataFileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameDataFileName_4), value);
	}

	inline static int32_t get_offset_of_playerDataFileName_5() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___playerDataFileName_5)); }
	inline String_t* get_playerDataFileName_5() const { return ___playerDataFileName_5; }
	inline String_t** get_address_of_playerDataFileName_5() { return &___playerDataFileName_5; }
	inline void set_playerDataFileName_5(String_t* value)
	{
		___playerDataFileName_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerDataFileName_5), value);
	}

	inline static int32_t get_offset_of_isFirstTime_6() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___isFirstTime_6)); }
	inline int32_t get_isFirstTime_6() const { return ___isFirstTime_6; }
	inline int32_t* get_address_of_isFirstTime_6() { return &___isFirstTime_6; }
	inline void set_isFirstTime_6(int32_t value)
	{
		___isFirstTime_6 = value;
	}

	inline static int32_t get_offset_of_currentLevel_7() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___currentLevel_7)); }
	inline int32_t get_currentLevel_7() const { return ___currentLevel_7; }
	inline int32_t* get_address_of_currentLevel_7() { return &___currentLevel_7; }
	inline void set_currentLevel_7(int32_t value)
	{
		___currentLevel_7 = value;
	}

	inline static int32_t get_offset_of_manager_8() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___manager_8)); }
	inline QuestManager_t588401851 * get_manager_8() const { return ___manager_8; }
	inline QuestManager_t588401851 ** get_address_of_manager_8() { return &___manager_8; }
	inline void set_manager_8(QuestManager_t588401851 * value)
	{
		___manager_8 = value;
		Il2CppCodeGenWriteBarrier((&___manager_8), value);
	}

	inline static int32_t get_offset_of_quest_9() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___quest_9)); }
	inline Quest_t3696879532 * get_quest_9() const { return ___quest_9; }
	inline Quest_t3696879532 ** get_address_of_quest_9() { return &___quest_9; }
	inline void set_quest_9(Quest_t3696879532 * value)
	{
		___quest_9 = value;
		Il2CppCodeGenWriteBarrier((&___quest_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACONTROLLER_T353634109_H
#ifndef CHARACTERMOVI_T1536215638_H
#define CHARACTERMOVI_T1536215638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMovi
struct  CharacterMovi_t1536215638  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CharacterMovi::position
	int32_t ___position_2;
	// System.Single CharacterMovi::speed
	float ___speed_3;

public:
	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(CharacterMovi_t1536215638, ___position_2)); }
	inline int32_t get_position_2() const { return ___position_2; }
	inline int32_t* get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(int32_t value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(CharacterMovi_t1536215638, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMOVI_T1536215638_H
#ifndef CHARACTERBEHAVIOR_T2392107484_H
#define CHARACTERBEHAVIOR_T2392107484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterBehavior
struct  CharacterBehavior_t2392107484  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CharacterBehavior::gameStarted
	bool ___gameStarted_2;
	// System.Int32 CharacterBehavior::posLevelCharacter
	int32_t ___posLevelCharacter_3;
	// System.Int32 CharacterBehavior::level
	int32_t ___level_4;
	// System.Single CharacterBehavior::speed
	float ___speed_5;
	// System.Boolean CharacterBehavior::isMoving
	bool ___isMoving_6;
	// UnityEngine.Vector3 CharacterBehavior::initPos
	Vector3_t3722313464  ___initPos_8;
	// UnityEngine.Vector3 CharacterBehavior::lastPos
	Vector3_t3722313464  ___lastPos_9;
	// UnityEngine.Sprite[] CharacterBehavior::characteres
	SpriteU5BU5D_t2581906349* ___characteres_10;
	// System.String CharacterBehavior::characterSelected
	String_t* ___characterSelected_11;
	// DataController CharacterBehavior::dataController
	DataController_t353634109 * ___dataController_12;
	// GameController CharacterBehavior::gameController
	GameController_t2330501625 * ___gameController_13;

public:
	inline static int32_t get_offset_of_gameStarted_2() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___gameStarted_2)); }
	inline bool get_gameStarted_2() const { return ___gameStarted_2; }
	inline bool* get_address_of_gameStarted_2() { return &___gameStarted_2; }
	inline void set_gameStarted_2(bool value)
	{
		___gameStarted_2 = value;
	}

	inline static int32_t get_offset_of_posLevelCharacter_3() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___posLevelCharacter_3)); }
	inline int32_t get_posLevelCharacter_3() const { return ___posLevelCharacter_3; }
	inline int32_t* get_address_of_posLevelCharacter_3() { return &___posLevelCharacter_3; }
	inline void set_posLevelCharacter_3(int32_t value)
	{
		___posLevelCharacter_3 = value;
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___level_4)); }
	inline int32_t get_level_4() const { return ___level_4; }
	inline int32_t* get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(int32_t value)
	{
		___level_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_isMoving_6() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___isMoving_6)); }
	inline bool get_isMoving_6() const { return ___isMoving_6; }
	inline bool* get_address_of_isMoving_6() { return &___isMoving_6; }
	inline void set_isMoving_6(bool value)
	{
		___isMoving_6 = value;
	}

	inline static int32_t get_offset_of_initPos_8() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___initPos_8)); }
	inline Vector3_t3722313464  get_initPos_8() const { return ___initPos_8; }
	inline Vector3_t3722313464 * get_address_of_initPos_8() { return &___initPos_8; }
	inline void set_initPos_8(Vector3_t3722313464  value)
	{
		___initPos_8 = value;
	}

	inline static int32_t get_offset_of_lastPos_9() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___lastPos_9)); }
	inline Vector3_t3722313464  get_lastPos_9() const { return ___lastPos_9; }
	inline Vector3_t3722313464 * get_address_of_lastPos_9() { return &___lastPos_9; }
	inline void set_lastPos_9(Vector3_t3722313464  value)
	{
		___lastPos_9 = value;
	}

	inline static int32_t get_offset_of_characteres_10() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___characteres_10)); }
	inline SpriteU5BU5D_t2581906349* get_characteres_10() const { return ___characteres_10; }
	inline SpriteU5BU5D_t2581906349** get_address_of_characteres_10() { return &___characteres_10; }
	inline void set_characteres_10(SpriteU5BU5D_t2581906349* value)
	{
		___characteres_10 = value;
		Il2CppCodeGenWriteBarrier((&___characteres_10), value);
	}

	inline static int32_t get_offset_of_characterSelected_11() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___characterSelected_11)); }
	inline String_t* get_characterSelected_11() const { return ___characterSelected_11; }
	inline String_t** get_address_of_characterSelected_11() { return &___characterSelected_11; }
	inline void set_characterSelected_11(String_t* value)
	{
		___characterSelected_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterSelected_11), value);
	}

	inline static int32_t get_offset_of_dataController_12() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___dataController_12)); }
	inline DataController_t353634109 * get_dataController_12() const { return ___dataController_12; }
	inline DataController_t353634109 ** get_address_of_dataController_12() { return &___dataController_12; }
	inline void set_dataController_12(DataController_t353634109 * value)
	{
		___dataController_12 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_12), value);
	}

	inline static int32_t get_offset_of_gameController_13() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___gameController_13)); }
	inline GameController_t2330501625 * get_gameController_13() const { return ___gameController_13; }
	inline GameController_t2330501625 ** get_address_of_gameController_13() { return &___gameController_13; }
	inline void set_gameController_13(GameController_t2330501625 * value)
	{
		___gameController_13 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_13), value);
	}
};

struct CharacterBehavior_t2392107484_StaticFields
{
public:
	// System.Boolean CharacterBehavior::playerCreated
	bool ___playerCreated_7;

public:
	inline static int32_t get_offset_of_playerCreated_7() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484_StaticFields, ___playerCreated_7)); }
	inline bool get_playerCreated_7() const { return ___playerCreated_7; }
	inline bool* get_address_of_playerCreated_7() { return &___playerCreated_7; }
	inline void set_playerCreated_7(bool value)
	{
		___playerCreated_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERBEHAVIOR_T2392107484_H
#ifndef AUDIORECORDMANAGER_T3166594816_H
#define AUDIORECORDMANAGER_T3166594816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecordManager
struct  AudioRecordManager_t3166594816  : public MonoBehaviour_t3962482529
{
public:
	// EAudioSamplingRate AudioRecordManager::samplingRate
	int32_t ___samplingRate_2;
	// System.Int32 AudioRecordManager::sampleRate
	int32_t ___sampleRate_3;
	// UnityEngine.AudioSource AudioRecordManager::_audioSource
	AudioSource_t3935305588 * ____audioSource_4;
	// System.Int32 AudioRecordManager::audioLength
	int32_t ___audioLength_5;
	// UnityEngine.AudioClip AudioRecordManager::_audioClip
	AudioClip_t3680889665 * ____audioClip_6;
	// System.Boolean AudioRecordManager::useMicrophone
	bool ___useMicrophone_7;
	// System.String AudioRecordManager::_selectDevice
	String_t* ____selectDevice_8;
	// System.String AudioRecordManager::fileName
	String_t* ___fileName_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.AudioType,EAudioFileExtension> AudioRecordManager::audioExtensions
	Dictionary_2_t4114977226 * ___audioExtensions_10;
	// UnityEngine.AudioType AudioRecordManager::selectedAudioType
	int32_t ___selectedAudioType_11;
	// System.String AudioRecordManager::_persistentDataPath
	String_t* ____persistentDataPath_12;
	// UnityEngine.Audio.AudioMixerGroup AudioRecordManager::_mixerGroupMicrophone
	AudioMixerGroup_t2743564464 * ____mixerGroupMicrophone_13;
	// UnityEngine.Audio.AudioMixerGroup AudioRecordManager::_mixerGroupMaster
	AudioMixerGroup_t2743564464 * ____mixerGroupMaster_14;
	// UnityEngine.AudioConfiguration AudioRecordManager::_audioConfiguration
	AudioConfiguration_t4040042187  ____audioConfiguration_15;
	// NAudio.Example.MP3EncoderWrapper AudioRecordManager::_mp3EncoderWrapper
	MP3EncoderWrapper_t279410081 * ____mp3EncoderWrapper_16;
	// EAudioChannel AudioRecordManager::channel
	int32_t ___channel_17;
	// System.Int32 AudioRecordManager::min
	int32_t ___min_18;
	// System.Int32 AudioRecordManager::max
	int32_t ___max_19;
	// System.Single AudioRecordManager::elapsedTime
	float ___elapsedTime_20;
	// System.Single AudioRecordManager::timer
	float ___timer_21;
	// System.Int32 AudioRecordManager::maxRecordTime
	int32_t ___maxRecordTime_22;
	// System.Boolean AudioRecordManager::isRecording
	bool ___isRecording_23;
	// System.Single AudioRecordManager::startRecordTime
	float ___startRecordTime_24;
	// System.Int32 AudioRecordManager::timeSpan
	int32_t ___timeSpan_25;
	// System.Action AudioRecordManager::OnStartEncoding
	Action_t1264377477 * ___OnStartEncoding_26;
	// System.Action AudioRecordManager::OnStopEncoding
	Action_t1264377477 * ___OnStopEncoding_27;
	// QuestManager AudioRecordManager::manager
	QuestManager_t588401851 * ___manager_28;
	// Quest AudioRecordManager::quest
	Quest_t3696879532 * ___quest_29;
	// UnityEngine.UI.Text AudioRecordManager::HistoryText
	Text_t1901882714 * ___HistoryText_30;
	// System.String AudioRecordManager::<ElapsedTimeTextWithFormat>k__BackingField
	String_t* ___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31;
	// System.String AudioRecordManager::<TimerTextWithFormat>k__BackingField
	String_t* ___U3CTimerTextWithFormatU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_samplingRate_2() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___samplingRate_2)); }
	inline int32_t get_samplingRate_2() const { return ___samplingRate_2; }
	inline int32_t* get_address_of_samplingRate_2() { return &___samplingRate_2; }
	inline void set_samplingRate_2(int32_t value)
	{
		___samplingRate_2 = value;
	}

	inline static int32_t get_offset_of_sampleRate_3() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___sampleRate_3)); }
	inline int32_t get_sampleRate_3() const { return ___sampleRate_3; }
	inline int32_t* get_address_of_sampleRate_3() { return &___sampleRate_3; }
	inline void set_sampleRate_3(int32_t value)
	{
		___sampleRate_3 = value;
	}

	inline static int32_t get_offset_of__audioSource_4() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____audioSource_4)); }
	inline AudioSource_t3935305588 * get__audioSource_4() const { return ____audioSource_4; }
	inline AudioSource_t3935305588 ** get_address_of__audioSource_4() { return &____audioSource_4; }
	inline void set__audioSource_4(AudioSource_t3935305588 * value)
	{
		____audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&____audioSource_4), value);
	}

	inline static int32_t get_offset_of_audioLength_5() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___audioLength_5)); }
	inline int32_t get_audioLength_5() const { return ___audioLength_5; }
	inline int32_t* get_address_of_audioLength_5() { return &___audioLength_5; }
	inline void set_audioLength_5(int32_t value)
	{
		___audioLength_5 = value;
	}

	inline static int32_t get_offset_of__audioClip_6() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____audioClip_6)); }
	inline AudioClip_t3680889665 * get__audioClip_6() const { return ____audioClip_6; }
	inline AudioClip_t3680889665 ** get_address_of__audioClip_6() { return &____audioClip_6; }
	inline void set__audioClip_6(AudioClip_t3680889665 * value)
	{
		____audioClip_6 = value;
		Il2CppCodeGenWriteBarrier((&____audioClip_6), value);
	}

	inline static int32_t get_offset_of_useMicrophone_7() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___useMicrophone_7)); }
	inline bool get_useMicrophone_7() const { return ___useMicrophone_7; }
	inline bool* get_address_of_useMicrophone_7() { return &___useMicrophone_7; }
	inline void set_useMicrophone_7(bool value)
	{
		___useMicrophone_7 = value;
	}

	inline static int32_t get_offset_of__selectDevice_8() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____selectDevice_8)); }
	inline String_t* get__selectDevice_8() const { return ____selectDevice_8; }
	inline String_t** get_address_of__selectDevice_8() { return &____selectDevice_8; }
	inline void set__selectDevice_8(String_t* value)
	{
		____selectDevice_8 = value;
		Il2CppCodeGenWriteBarrier((&____selectDevice_8), value);
	}

	inline static int32_t get_offset_of_fileName_9() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___fileName_9)); }
	inline String_t* get_fileName_9() const { return ___fileName_9; }
	inline String_t** get_address_of_fileName_9() { return &___fileName_9; }
	inline void set_fileName_9(String_t* value)
	{
		___fileName_9 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_9), value);
	}

	inline static int32_t get_offset_of_audioExtensions_10() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___audioExtensions_10)); }
	inline Dictionary_2_t4114977226 * get_audioExtensions_10() const { return ___audioExtensions_10; }
	inline Dictionary_2_t4114977226 ** get_address_of_audioExtensions_10() { return &___audioExtensions_10; }
	inline void set_audioExtensions_10(Dictionary_2_t4114977226 * value)
	{
		___audioExtensions_10 = value;
		Il2CppCodeGenWriteBarrier((&___audioExtensions_10), value);
	}

	inline static int32_t get_offset_of_selectedAudioType_11() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___selectedAudioType_11)); }
	inline int32_t get_selectedAudioType_11() const { return ___selectedAudioType_11; }
	inline int32_t* get_address_of_selectedAudioType_11() { return &___selectedAudioType_11; }
	inline void set_selectedAudioType_11(int32_t value)
	{
		___selectedAudioType_11 = value;
	}

	inline static int32_t get_offset_of__persistentDataPath_12() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____persistentDataPath_12)); }
	inline String_t* get__persistentDataPath_12() const { return ____persistentDataPath_12; }
	inline String_t** get_address_of__persistentDataPath_12() { return &____persistentDataPath_12; }
	inline void set__persistentDataPath_12(String_t* value)
	{
		____persistentDataPath_12 = value;
		Il2CppCodeGenWriteBarrier((&____persistentDataPath_12), value);
	}

	inline static int32_t get_offset_of__mixerGroupMicrophone_13() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____mixerGroupMicrophone_13)); }
	inline AudioMixerGroup_t2743564464 * get__mixerGroupMicrophone_13() const { return ____mixerGroupMicrophone_13; }
	inline AudioMixerGroup_t2743564464 ** get_address_of__mixerGroupMicrophone_13() { return &____mixerGroupMicrophone_13; }
	inline void set__mixerGroupMicrophone_13(AudioMixerGroup_t2743564464 * value)
	{
		____mixerGroupMicrophone_13 = value;
		Il2CppCodeGenWriteBarrier((&____mixerGroupMicrophone_13), value);
	}

	inline static int32_t get_offset_of__mixerGroupMaster_14() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____mixerGroupMaster_14)); }
	inline AudioMixerGroup_t2743564464 * get__mixerGroupMaster_14() const { return ____mixerGroupMaster_14; }
	inline AudioMixerGroup_t2743564464 ** get_address_of__mixerGroupMaster_14() { return &____mixerGroupMaster_14; }
	inline void set__mixerGroupMaster_14(AudioMixerGroup_t2743564464 * value)
	{
		____mixerGroupMaster_14 = value;
		Il2CppCodeGenWriteBarrier((&____mixerGroupMaster_14), value);
	}

	inline static int32_t get_offset_of__audioConfiguration_15() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____audioConfiguration_15)); }
	inline AudioConfiguration_t4040042187  get__audioConfiguration_15() const { return ____audioConfiguration_15; }
	inline AudioConfiguration_t4040042187 * get_address_of__audioConfiguration_15() { return &____audioConfiguration_15; }
	inline void set__audioConfiguration_15(AudioConfiguration_t4040042187  value)
	{
		____audioConfiguration_15 = value;
	}

	inline static int32_t get_offset_of__mp3EncoderWrapper_16() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____mp3EncoderWrapper_16)); }
	inline MP3EncoderWrapper_t279410081 * get__mp3EncoderWrapper_16() const { return ____mp3EncoderWrapper_16; }
	inline MP3EncoderWrapper_t279410081 ** get_address_of__mp3EncoderWrapper_16() { return &____mp3EncoderWrapper_16; }
	inline void set__mp3EncoderWrapper_16(MP3EncoderWrapper_t279410081 * value)
	{
		____mp3EncoderWrapper_16 = value;
		Il2CppCodeGenWriteBarrier((&____mp3EncoderWrapper_16), value);
	}

	inline static int32_t get_offset_of_channel_17() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___channel_17)); }
	inline int32_t get_channel_17() const { return ___channel_17; }
	inline int32_t* get_address_of_channel_17() { return &___channel_17; }
	inline void set_channel_17(int32_t value)
	{
		___channel_17 = value;
	}

	inline static int32_t get_offset_of_min_18() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___min_18)); }
	inline int32_t get_min_18() const { return ___min_18; }
	inline int32_t* get_address_of_min_18() { return &___min_18; }
	inline void set_min_18(int32_t value)
	{
		___min_18 = value;
	}

	inline static int32_t get_offset_of_max_19() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___max_19)); }
	inline int32_t get_max_19() const { return ___max_19; }
	inline int32_t* get_address_of_max_19() { return &___max_19; }
	inline void set_max_19(int32_t value)
	{
		___max_19 = value;
	}

	inline static int32_t get_offset_of_elapsedTime_20() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___elapsedTime_20)); }
	inline float get_elapsedTime_20() const { return ___elapsedTime_20; }
	inline float* get_address_of_elapsedTime_20() { return &___elapsedTime_20; }
	inline void set_elapsedTime_20(float value)
	{
		___elapsedTime_20 = value;
	}

	inline static int32_t get_offset_of_timer_21() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___timer_21)); }
	inline float get_timer_21() const { return ___timer_21; }
	inline float* get_address_of_timer_21() { return &___timer_21; }
	inline void set_timer_21(float value)
	{
		___timer_21 = value;
	}

	inline static int32_t get_offset_of_maxRecordTime_22() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___maxRecordTime_22)); }
	inline int32_t get_maxRecordTime_22() const { return ___maxRecordTime_22; }
	inline int32_t* get_address_of_maxRecordTime_22() { return &___maxRecordTime_22; }
	inline void set_maxRecordTime_22(int32_t value)
	{
		___maxRecordTime_22 = value;
	}

	inline static int32_t get_offset_of_isRecording_23() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___isRecording_23)); }
	inline bool get_isRecording_23() const { return ___isRecording_23; }
	inline bool* get_address_of_isRecording_23() { return &___isRecording_23; }
	inline void set_isRecording_23(bool value)
	{
		___isRecording_23 = value;
	}

	inline static int32_t get_offset_of_startRecordTime_24() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___startRecordTime_24)); }
	inline float get_startRecordTime_24() const { return ___startRecordTime_24; }
	inline float* get_address_of_startRecordTime_24() { return &___startRecordTime_24; }
	inline void set_startRecordTime_24(float value)
	{
		___startRecordTime_24 = value;
	}

	inline static int32_t get_offset_of_timeSpan_25() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___timeSpan_25)); }
	inline int32_t get_timeSpan_25() const { return ___timeSpan_25; }
	inline int32_t* get_address_of_timeSpan_25() { return &___timeSpan_25; }
	inline void set_timeSpan_25(int32_t value)
	{
		___timeSpan_25 = value;
	}

	inline static int32_t get_offset_of_OnStartEncoding_26() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___OnStartEncoding_26)); }
	inline Action_t1264377477 * get_OnStartEncoding_26() const { return ___OnStartEncoding_26; }
	inline Action_t1264377477 ** get_address_of_OnStartEncoding_26() { return &___OnStartEncoding_26; }
	inline void set_OnStartEncoding_26(Action_t1264377477 * value)
	{
		___OnStartEncoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnStartEncoding_26), value);
	}

	inline static int32_t get_offset_of_OnStopEncoding_27() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___OnStopEncoding_27)); }
	inline Action_t1264377477 * get_OnStopEncoding_27() const { return ___OnStopEncoding_27; }
	inline Action_t1264377477 ** get_address_of_OnStopEncoding_27() { return &___OnStopEncoding_27; }
	inline void set_OnStopEncoding_27(Action_t1264377477 * value)
	{
		___OnStopEncoding_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnStopEncoding_27), value);
	}

	inline static int32_t get_offset_of_manager_28() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___manager_28)); }
	inline QuestManager_t588401851 * get_manager_28() const { return ___manager_28; }
	inline QuestManager_t588401851 ** get_address_of_manager_28() { return &___manager_28; }
	inline void set_manager_28(QuestManager_t588401851 * value)
	{
		___manager_28 = value;
		Il2CppCodeGenWriteBarrier((&___manager_28), value);
	}

	inline static int32_t get_offset_of_quest_29() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___quest_29)); }
	inline Quest_t3696879532 * get_quest_29() const { return ___quest_29; }
	inline Quest_t3696879532 ** get_address_of_quest_29() { return &___quest_29; }
	inline void set_quest_29(Quest_t3696879532 * value)
	{
		___quest_29 = value;
		Il2CppCodeGenWriteBarrier((&___quest_29), value);
	}

	inline static int32_t get_offset_of_HistoryText_30() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___HistoryText_30)); }
	inline Text_t1901882714 * get_HistoryText_30() const { return ___HistoryText_30; }
	inline Text_t1901882714 ** get_address_of_HistoryText_30() { return &___HistoryText_30; }
	inline void set_HistoryText_30(Text_t1901882714 * value)
	{
		___HistoryText_30 = value;
		Il2CppCodeGenWriteBarrier((&___HistoryText_30), value);
	}

	inline static int32_t get_offset_of_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31)); }
	inline String_t* get_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31() const { return ___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31; }
	inline String_t** get_address_of_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31() { return &___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31; }
	inline void set_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31(String_t* value)
	{
		___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CTimerTextWithFormatU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___U3CTimerTextWithFormatU3Ek__BackingField_32)); }
	inline String_t* get_U3CTimerTextWithFormatU3Ek__BackingField_32() const { return ___U3CTimerTextWithFormatU3Ek__BackingField_32; }
	inline String_t** get_address_of_U3CTimerTextWithFormatU3Ek__BackingField_32() { return &___U3CTimerTextWithFormatU3Ek__BackingField_32; }
	inline void set_U3CTimerTextWithFormatU3Ek__BackingField_32(String_t* value)
	{
		___U3CTimerTextWithFormatU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTimerTextWithFormatU3Ek__BackingField_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIORECORDMANAGER_T3166594816_H
#ifndef MP3ENCODERWRAPPER_T279410081_H
#define MP3ENCODERWRAPPER_T279410081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Example.MP3EncoderWrapper
struct  MP3EncoderWrapper_t279410081  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MP3ENCODERWRAPPER_T279410081_H
#ifndef MANAGERGAME_T32073082_H
#define MANAGERGAME_T32073082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManagerGame
struct  ManagerGame_t32073082  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGERGAME_T32073082_H
#ifndef UIDISPLAYCONTROLLER_T1173465452_H
#define UIDISPLAYCONTROLLER_T1173465452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDisplayController
struct  UIDisplayController_t1173465452  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIDisplayController::encodingMessagePanel
	GameObject_t1113636619 * ___encodingMessagePanel_2;
	// UnityEngine.UI.Text UIDisplayController::elapsedTimeText
	Text_t1901882714 * ___elapsedTimeText_3;
	// UnityEngine.UI.Text UIDisplayController::timerText
	Text_t1901882714 * ___timerText_4;
	// UnityEngine.UI.Button UIDisplayController::stop
	Button_t4055032469 * ___stop_5;
	// UnityEngine.UI.Button UIDisplayController::start
	Button_t4055032469 * ___start_6;
	// AudioRecordManager UIDisplayController::recordManager
	AudioRecordManager_t3166594816 * ___recordManager_7;
	// SceneChanger UIDisplayController::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_8;
	// QuestManager UIDisplayController::manager
	QuestManager_t588401851 * ___manager_9;
	// Quest UIDisplayController::quest
	Quest_t3696879532 * ___quest_10;

public:
	inline static int32_t get_offset_of_encodingMessagePanel_2() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___encodingMessagePanel_2)); }
	inline GameObject_t1113636619 * get_encodingMessagePanel_2() const { return ___encodingMessagePanel_2; }
	inline GameObject_t1113636619 ** get_address_of_encodingMessagePanel_2() { return &___encodingMessagePanel_2; }
	inline void set_encodingMessagePanel_2(GameObject_t1113636619 * value)
	{
		___encodingMessagePanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___encodingMessagePanel_2), value);
	}

	inline static int32_t get_offset_of_elapsedTimeText_3() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___elapsedTimeText_3)); }
	inline Text_t1901882714 * get_elapsedTimeText_3() const { return ___elapsedTimeText_3; }
	inline Text_t1901882714 ** get_address_of_elapsedTimeText_3() { return &___elapsedTimeText_3; }
	inline void set_elapsedTimeText_3(Text_t1901882714 * value)
	{
		___elapsedTimeText_3 = value;
		Il2CppCodeGenWriteBarrier((&___elapsedTimeText_3), value);
	}

	inline static int32_t get_offset_of_timerText_4() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___timerText_4)); }
	inline Text_t1901882714 * get_timerText_4() const { return ___timerText_4; }
	inline Text_t1901882714 ** get_address_of_timerText_4() { return &___timerText_4; }
	inline void set_timerText_4(Text_t1901882714 * value)
	{
		___timerText_4 = value;
		Il2CppCodeGenWriteBarrier((&___timerText_4), value);
	}

	inline static int32_t get_offset_of_stop_5() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___stop_5)); }
	inline Button_t4055032469 * get_stop_5() const { return ___stop_5; }
	inline Button_t4055032469 ** get_address_of_stop_5() { return &___stop_5; }
	inline void set_stop_5(Button_t4055032469 * value)
	{
		___stop_5 = value;
		Il2CppCodeGenWriteBarrier((&___stop_5), value);
	}

	inline static int32_t get_offset_of_start_6() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___start_6)); }
	inline Button_t4055032469 * get_start_6() const { return ___start_6; }
	inline Button_t4055032469 ** get_address_of_start_6() { return &___start_6; }
	inline void set_start_6(Button_t4055032469 * value)
	{
		___start_6 = value;
		Il2CppCodeGenWriteBarrier((&___start_6), value);
	}

	inline static int32_t get_offset_of_recordManager_7() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___recordManager_7)); }
	inline AudioRecordManager_t3166594816 * get_recordManager_7() const { return ___recordManager_7; }
	inline AudioRecordManager_t3166594816 ** get_address_of_recordManager_7() { return &___recordManager_7; }
	inline void set_recordManager_7(AudioRecordManager_t3166594816 * value)
	{
		___recordManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___recordManager_7), value);
	}

	inline static int32_t get_offset_of_sceneChanger_8() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___sceneChanger_8)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_8() const { return ___sceneChanger_8; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_8() { return &___sceneChanger_8; }
	inline void set_sceneChanger_8(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_8 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_8), value);
	}

	inline static int32_t get_offset_of_manager_9() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___manager_9)); }
	inline QuestManager_t588401851 * get_manager_9() const { return ___manager_9; }
	inline QuestManager_t588401851 ** get_address_of_manager_9() { return &___manager_9; }
	inline void set_manager_9(QuestManager_t588401851 * value)
	{
		___manager_9 = value;
		Il2CppCodeGenWriteBarrier((&___manager_9), value);
	}

	inline static int32_t get_offset_of_quest_10() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___quest_10)); }
	inline Quest_t3696879532 * get_quest_10() const { return ___quest_10; }
	inline Quest_t3696879532 ** get_address_of_quest_10() { return &___quest_10; }
	inline void set_quest_10(Quest_t3696879532 * value)
	{
		___quest_10 = value;
		Il2CppCodeGenWriteBarrier((&___quest_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDISPLAYCONTROLLER_T1173465452_H
#ifndef SETTINGSCONTROLLER_T1943457452_H
#define SETTINGSCONTROLLER_T1943457452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsController
struct  SettingsController_t1943457452  : public MonoBehaviour_t3962482529
{
public:
	// GameController SettingsController::gameController
	GameController_t2330501625 * ___gameController_2;

public:
	inline static int32_t get_offset_of_gameController_2() { return static_cast<int32_t>(offsetof(SettingsController_t1943457452, ___gameController_2)); }
	inline GameController_t2330501625 * get_gameController_2() const { return ___gameController_2; }
	inline GameController_t2330501625 ** get_address_of_gameController_2() { return &___gameController_2; }
	inline void set_gameController_2(GameController_t2330501625 * value)
	{
		___gameController_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSCONTROLLER_T1943457452_H
#ifndef SCENECHANGER_T1033871796_H
#define SCENECHANGER_T1033871796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneChanger
struct  SceneChanger_t1033871796  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SceneChanger::buttonOk
	Button_t4055032469 * ___buttonOk_2;
	// UnityEngine.UI.Button SceneChanger::buttonWrong
	Button_t4055032469 * ___buttonWrong_3;
	// GameController SceneChanger::gameController
	GameController_t2330501625 * ___gameController_4;
	// DataController SceneChanger::dataController
	DataController_t353634109 * ___dataController_5;

public:
	inline static int32_t get_offset_of_buttonOk_2() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___buttonOk_2)); }
	inline Button_t4055032469 * get_buttonOk_2() const { return ___buttonOk_2; }
	inline Button_t4055032469 ** get_address_of_buttonOk_2() { return &___buttonOk_2; }
	inline void set_buttonOk_2(Button_t4055032469 * value)
	{
		___buttonOk_2 = value;
		Il2CppCodeGenWriteBarrier((&___buttonOk_2), value);
	}

	inline static int32_t get_offset_of_buttonWrong_3() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___buttonWrong_3)); }
	inline Button_t4055032469 * get_buttonWrong_3() const { return ___buttonWrong_3; }
	inline Button_t4055032469 ** get_address_of_buttonWrong_3() { return &___buttonWrong_3; }
	inline void set_buttonWrong_3(Button_t4055032469 * value)
	{
		___buttonWrong_3 = value;
		Il2CppCodeGenWriteBarrier((&___buttonWrong_3), value);
	}

	inline static int32_t get_offset_of_gameController_4() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___gameController_4)); }
	inline GameController_t2330501625 * get_gameController_4() const { return ___gameController_4; }
	inline GameController_t2330501625 ** get_address_of_gameController_4() { return &___gameController_4; }
	inline void set_gameController_4(GameController_t2330501625 * value)
	{
		___gameController_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_4), value);
	}

	inline static int32_t get_offset_of_dataController_5() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___dataController_5)); }
	inline DataController_t353634109 * get_dataController_5() const { return ___dataController_5; }
	inline DataController_t353634109 ** get_address_of_dataController_5() { return &___dataController_5; }
	inline void set_dataController_5(DataController_t353634109 * value)
	{
		___dataController_5 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECHANGER_T1033871796_H
#ifndef RESULTQUESTSCENE_T315437199_H
#define RESULTQUESTSCENE_T315437199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultQuestScene
struct  ResultQuestScene_t315437199  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager ResultQuestScene::manager
	QuestManager_t588401851 * ___manager_2;
	// Quest ResultQuestScene::quest
	Quest_t3696879532 * ___quest_3;
	// UnityEngine.UI.Text ResultQuestScene::text
	Text_t1901882714 * ___text_4;
	// SceneChanger ResultQuestScene::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_5;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_quest_3() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___quest_3)); }
	inline Quest_t3696879532 * get_quest_3() const { return ___quest_3; }
	inline Quest_t3696879532 ** get_address_of_quest_3() { return &___quest_3; }
	inline void set_quest_3(Quest_t3696879532 * value)
	{
		___quest_3 = value;
		Il2CppCodeGenWriteBarrier((&___quest_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_sceneChanger_5() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___sceneChanger_5)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_5() const { return ___sceneChanger_5; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_5() { return &___sceneChanger_5; }
	inline void set_sceneChanger_5(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_5 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTQUESTSCENE_T315437199_H
#ifndef QUESTTRIGGER_T3125687900_H
#define QUESTTRIGGER_T3125687900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestTrigger
struct  QuestTrigger_t3125687900  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager QuestTrigger::manager
	QuestManager_t588401851 * ___manager_2;
	// System.Int32 QuestTrigger::questID
	int32_t ___questID_3;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(QuestTrigger_t3125687900, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_questID_3() { return static_cast<int32_t>(offsetof(QuestTrigger_t3125687900, ___questID_3)); }
	inline int32_t get_questID_3() const { return ___questID_3; }
	inline int32_t* get_address_of_questID_3() { return &___questID_3; }
	inline void set_questID_3(int32_t value)
	{
		___questID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTTRIGGER_T3125687900_H
#ifndef QUESTSCENE_T3272400377_H
#define QUESTSCENE_T3272400377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestScene
struct  QuestScene_t3272400377  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager QuestScene::manager
	QuestManager_t588401851 * ___manager_2;
	// Quest QuestScene::quest
	Quest_t3696879532 * ___quest_3;
	// UnityEngine.UI.Text QuestScene::text
	Text_t1901882714 * ___text_4;
	// UnityEngine.UI.Button[] QuestScene::options
	ButtonU5BU5D_t2297175928* ___options_5;
	// UnityEngine.UI.Text[] QuestScene::optionsText
	TextU5BU5D_t422084607* ___optionsText_6;
	// AudioRecordManager QuestScene::audioManager
	AudioRecordManager_t3166594816 * ___audioManager_7;
	// System.Int32 QuestScene::idStory
	int32_t ___idStory_8;
	// UnityEngine.GameObject QuestScene::buttonStartRecord
	GameObject_t1113636619 * ___buttonStartRecord_9;
	// UnityEngine.GameObject QuestScene::buttonStopRecord
	GameObject_t1113636619 * ___buttonStopRecord_10;
	// System.Single QuestScene::elapsedTime
	float ___elapsedTime_11;
	// System.Single QuestScene::countdownTimeDefault
	float ___countdownTimeDefault_12;
	// System.Single QuestScene::countdownTime
	float ___countdownTime_13;
	// System.Boolean QuestScene::isactivityStarted
	bool ___isactivityStarted_14;
	// UnityEngine.UI.Text QuestScene::countDownText
	Text_t1901882714 * ___countDownText_15;
	// UnityEngine.GameObject QuestScene::startActivity
	GameObject_t1113636619 * ___startActivity_16;
	// UnityEngine.GameObject QuestScene::completeActivity
	GameObject_t1113636619 * ___completeActivity_17;
	// UnityEngine.GameObject QuestScene::incompleteActivity
	GameObject_t1113636619 * ___incompleteActivity_18;
	// SceneChanger QuestScene::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_19;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_quest_3() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___quest_3)); }
	inline Quest_t3696879532 * get_quest_3() const { return ___quest_3; }
	inline Quest_t3696879532 ** get_address_of_quest_3() { return &___quest_3; }
	inline void set_quest_3(Quest_t3696879532 * value)
	{
		___quest_3 = value;
		Il2CppCodeGenWriteBarrier((&___quest_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___options_5)); }
	inline ButtonU5BU5D_t2297175928* get_options_5() const { return ___options_5; }
	inline ButtonU5BU5D_t2297175928** get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(ButtonU5BU5D_t2297175928* value)
	{
		___options_5 = value;
		Il2CppCodeGenWriteBarrier((&___options_5), value);
	}

	inline static int32_t get_offset_of_optionsText_6() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___optionsText_6)); }
	inline TextU5BU5D_t422084607* get_optionsText_6() const { return ___optionsText_6; }
	inline TextU5BU5D_t422084607** get_address_of_optionsText_6() { return &___optionsText_6; }
	inline void set_optionsText_6(TextU5BU5D_t422084607* value)
	{
		___optionsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___optionsText_6), value);
	}

	inline static int32_t get_offset_of_audioManager_7() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___audioManager_7)); }
	inline AudioRecordManager_t3166594816 * get_audioManager_7() const { return ___audioManager_7; }
	inline AudioRecordManager_t3166594816 ** get_address_of_audioManager_7() { return &___audioManager_7; }
	inline void set_audioManager_7(AudioRecordManager_t3166594816 * value)
	{
		___audioManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioManager_7), value);
	}

	inline static int32_t get_offset_of_idStory_8() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___idStory_8)); }
	inline int32_t get_idStory_8() const { return ___idStory_8; }
	inline int32_t* get_address_of_idStory_8() { return &___idStory_8; }
	inline void set_idStory_8(int32_t value)
	{
		___idStory_8 = value;
	}

	inline static int32_t get_offset_of_buttonStartRecord_9() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___buttonStartRecord_9)); }
	inline GameObject_t1113636619 * get_buttonStartRecord_9() const { return ___buttonStartRecord_9; }
	inline GameObject_t1113636619 ** get_address_of_buttonStartRecord_9() { return &___buttonStartRecord_9; }
	inline void set_buttonStartRecord_9(GameObject_t1113636619 * value)
	{
		___buttonStartRecord_9 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStartRecord_9), value);
	}

	inline static int32_t get_offset_of_buttonStopRecord_10() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___buttonStopRecord_10)); }
	inline GameObject_t1113636619 * get_buttonStopRecord_10() const { return ___buttonStopRecord_10; }
	inline GameObject_t1113636619 ** get_address_of_buttonStopRecord_10() { return &___buttonStopRecord_10; }
	inline void set_buttonStopRecord_10(GameObject_t1113636619 * value)
	{
		___buttonStopRecord_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStopRecord_10), value);
	}

	inline static int32_t get_offset_of_elapsedTime_11() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___elapsedTime_11)); }
	inline float get_elapsedTime_11() const { return ___elapsedTime_11; }
	inline float* get_address_of_elapsedTime_11() { return &___elapsedTime_11; }
	inline void set_elapsedTime_11(float value)
	{
		___elapsedTime_11 = value;
	}

	inline static int32_t get_offset_of_countdownTimeDefault_12() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___countdownTimeDefault_12)); }
	inline float get_countdownTimeDefault_12() const { return ___countdownTimeDefault_12; }
	inline float* get_address_of_countdownTimeDefault_12() { return &___countdownTimeDefault_12; }
	inline void set_countdownTimeDefault_12(float value)
	{
		___countdownTimeDefault_12 = value;
	}

	inline static int32_t get_offset_of_countdownTime_13() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___countdownTime_13)); }
	inline float get_countdownTime_13() const { return ___countdownTime_13; }
	inline float* get_address_of_countdownTime_13() { return &___countdownTime_13; }
	inline void set_countdownTime_13(float value)
	{
		___countdownTime_13 = value;
	}

	inline static int32_t get_offset_of_isactivityStarted_14() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___isactivityStarted_14)); }
	inline bool get_isactivityStarted_14() const { return ___isactivityStarted_14; }
	inline bool* get_address_of_isactivityStarted_14() { return &___isactivityStarted_14; }
	inline void set_isactivityStarted_14(bool value)
	{
		___isactivityStarted_14 = value;
	}

	inline static int32_t get_offset_of_countDownText_15() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___countDownText_15)); }
	inline Text_t1901882714 * get_countDownText_15() const { return ___countDownText_15; }
	inline Text_t1901882714 ** get_address_of_countDownText_15() { return &___countDownText_15; }
	inline void set_countDownText_15(Text_t1901882714 * value)
	{
		___countDownText_15 = value;
		Il2CppCodeGenWriteBarrier((&___countDownText_15), value);
	}

	inline static int32_t get_offset_of_startActivity_16() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___startActivity_16)); }
	inline GameObject_t1113636619 * get_startActivity_16() const { return ___startActivity_16; }
	inline GameObject_t1113636619 ** get_address_of_startActivity_16() { return &___startActivity_16; }
	inline void set_startActivity_16(GameObject_t1113636619 * value)
	{
		___startActivity_16 = value;
		Il2CppCodeGenWriteBarrier((&___startActivity_16), value);
	}

	inline static int32_t get_offset_of_completeActivity_17() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___completeActivity_17)); }
	inline GameObject_t1113636619 * get_completeActivity_17() const { return ___completeActivity_17; }
	inline GameObject_t1113636619 ** get_address_of_completeActivity_17() { return &___completeActivity_17; }
	inline void set_completeActivity_17(GameObject_t1113636619 * value)
	{
		___completeActivity_17 = value;
		Il2CppCodeGenWriteBarrier((&___completeActivity_17), value);
	}

	inline static int32_t get_offset_of_incompleteActivity_18() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___incompleteActivity_18)); }
	inline GameObject_t1113636619 * get_incompleteActivity_18() const { return ___incompleteActivity_18; }
	inline GameObject_t1113636619 ** get_address_of_incompleteActivity_18() { return &___incompleteActivity_18; }
	inline void set_incompleteActivity_18(GameObject_t1113636619 * value)
	{
		___incompleteActivity_18 = value;
		Il2CppCodeGenWriteBarrier((&___incompleteActivity_18), value);
	}

	inline static int32_t get_offset_of_sceneChanger_19() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___sceneChanger_19)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_19() const { return ___sceneChanger_19; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_19() { return &___sceneChanger_19; }
	inline void set_sceneChanger_19(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_19 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTSCENE_T3272400377_H
#ifndef MANAGERSETTINGS_T530119282_H
#define MANAGERSETTINGS_T530119282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManagerSettings
struct  ManagerSettings_t530119282  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGERSETTINGS_T530119282_H
#ifndef UIPUZZLECONTROLLER_T2836468369_H
#define UIPUZZLECONTROLLER_T2836468369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController
struct  UIPuzzleController_t2836468369  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UIPuzzleController::images
	List_1_t4142344393 * ___images_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIPuzzleController::imagePositions
	List_1_t899420910 * ___imagePositions_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIPuzzleController::startImgPositions
	List_1_t899420910 * ___startImgPositions_5;
	// System.Collections.Generic.List`1<EPuzzleToken> UIPuzzleController::tokenOrder
	List_1_t3937307605 * ___tokenOrder_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UIPuzzleController::tokenImages
	List_1_t4142344393 * ___tokenImages_7;
	// System.Collections.Generic.List`1<UIPuzzleToken> UIPuzzleController::tokens
	List_1_t3134180627 * ___tokens_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIPuzzleController::tokenPositions
	List_1_t899420910 * ___tokenPositions_9;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UIPuzzleController::insertImages
	List_1_t4142344393 * ___insertImages_10;
	// System.Single UIPuzzleController::showTime
	float ___showTime_11;
	// System.Single UIPuzzleController::startTime
	float ___startTime_12;
	// System.Single UIPuzzleController::countdownTime
	float ___countdownTime_13;
	// UnityEngine.GameObject UIPuzzleController::introPanel
	GameObject_t1113636619 * ___introPanel_14;
	// UnityEngine.GameObject UIPuzzleController::puzzlePanel
	GameObject_t1113636619 * ___puzzlePanel_15;
	// UnityEngine.GameObject UIPuzzleController::congratsMessage
	GameObject_t1113636619 * ___congratsMessage_16;
	// UnityEngine.UI.Text UIPuzzleController::countdownText
	Text_t1901882714 * ___countdownText_17;
	// System.Boolean UIPuzzleController::bInit
	bool ___bInit_18;
	// System.Boolean UIPuzzleController::bTokenMatched
	bool ___bTokenMatched_19;
	// UnityEngine.Vector2 UIPuzzleController::matchedPosition
	Vector2_t2156229523  ___matchedPosition_20;
	// UIPuzzleToken UIPuzzleController::CurrentToken
	UIPuzzleToken_t1662105885 * ___CurrentToken_21;
	// System.Int32 UIPuzzleController::matchCount
	int32_t ___matchCount_22;
	// QuestManager UIPuzzleController::manager
	QuestManager_t588401851 * ___manager_24;

public:
	inline static int32_t get_offset_of_images_3() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___images_3)); }
	inline List_1_t4142344393 * get_images_3() const { return ___images_3; }
	inline List_1_t4142344393 ** get_address_of_images_3() { return &___images_3; }
	inline void set_images_3(List_1_t4142344393 * value)
	{
		___images_3 = value;
		Il2CppCodeGenWriteBarrier((&___images_3), value);
	}

	inline static int32_t get_offset_of_imagePositions_4() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___imagePositions_4)); }
	inline List_1_t899420910 * get_imagePositions_4() const { return ___imagePositions_4; }
	inline List_1_t899420910 ** get_address_of_imagePositions_4() { return &___imagePositions_4; }
	inline void set_imagePositions_4(List_1_t899420910 * value)
	{
		___imagePositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___imagePositions_4), value);
	}

	inline static int32_t get_offset_of_startImgPositions_5() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___startImgPositions_5)); }
	inline List_1_t899420910 * get_startImgPositions_5() const { return ___startImgPositions_5; }
	inline List_1_t899420910 ** get_address_of_startImgPositions_5() { return &___startImgPositions_5; }
	inline void set_startImgPositions_5(List_1_t899420910 * value)
	{
		___startImgPositions_5 = value;
		Il2CppCodeGenWriteBarrier((&___startImgPositions_5), value);
	}

	inline static int32_t get_offset_of_tokenOrder_6() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokenOrder_6)); }
	inline List_1_t3937307605 * get_tokenOrder_6() const { return ___tokenOrder_6; }
	inline List_1_t3937307605 ** get_address_of_tokenOrder_6() { return &___tokenOrder_6; }
	inline void set_tokenOrder_6(List_1_t3937307605 * value)
	{
		___tokenOrder_6 = value;
		Il2CppCodeGenWriteBarrier((&___tokenOrder_6), value);
	}

	inline static int32_t get_offset_of_tokenImages_7() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokenImages_7)); }
	inline List_1_t4142344393 * get_tokenImages_7() const { return ___tokenImages_7; }
	inline List_1_t4142344393 ** get_address_of_tokenImages_7() { return &___tokenImages_7; }
	inline void set_tokenImages_7(List_1_t4142344393 * value)
	{
		___tokenImages_7 = value;
		Il2CppCodeGenWriteBarrier((&___tokenImages_7), value);
	}

	inline static int32_t get_offset_of_tokens_8() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokens_8)); }
	inline List_1_t3134180627 * get_tokens_8() const { return ___tokens_8; }
	inline List_1_t3134180627 ** get_address_of_tokens_8() { return &___tokens_8; }
	inline void set_tokens_8(List_1_t3134180627 * value)
	{
		___tokens_8 = value;
		Il2CppCodeGenWriteBarrier((&___tokens_8), value);
	}

	inline static int32_t get_offset_of_tokenPositions_9() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokenPositions_9)); }
	inline List_1_t899420910 * get_tokenPositions_9() const { return ___tokenPositions_9; }
	inline List_1_t899420910 ** get_address_of_tokenPositions_9() { return &___tokenPositions_9; }
	inline void set_tokenPositions_9(List_1_t899420910 * value)
	{
		___tokenPositions_9 = value;
		Il2CppCodeGenWriteBarrier((&___tokenPositions_9), value);
	}

	inline static int32_t get_offset_of_insertImages_10() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___insertImages_10)); }
	inline List_1_t4142344393 * get_insertImages_10() const { return ___insertImages_10; }
	inline List_1_t4142344393 ** get_address_of_insertImages_10() { return &___insertImages_10; }
	inline void set_insertImages_10(List_1_t4142344393 * value)
	{
		___insertImages_10 = value;
		Il2CppCodeGenWriteBarrier((&___insertImages_10), value);
	}

	inline static int32_t get_offset_of_showTime_11() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___showTime_11)); }
	inline float get_showTime_11() const { return ___showTime_11; }
	inline float* get_address_of_showTime_11() { return &___showTime_11; }
	inline void set_showTime_11(float value)
	{
		___showTime_11 = value;
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___startTime_12)); }
	inline float get_startTime_12() const { return ___startTime_12; }
	inline float* get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(float value)
	{
		___startTime_12 = value;
	}

	inline static int32_t get_offset_of_countdownTime_13() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___countdownTime_13)); }
	inline float get_countdownTime_13() const { return ___countdownTime_13; }
	inline float* get_address_of_countdownTime_13() { return &___countdownTime_13; }
	inline void set_countdownTime_13(float value)
	{
		___countdownTime_13 = value;
	}

	inline static int32_t get_offset_of_introPanel_14() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___introPanel_14)); }
	inline GameObject_t1113636619 * get_introPanel_14() const { return ___introPanel_14; }
	inline GameObject_t1113636619 ** get_address_of_introPanel_14() { return &___introPanel_14; }
	inline void set_introPanel_14(GameObject_t1113636619 * value)
	{
		___introPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___introPanel_14), value);
	}

	inline static int32_t get_offset_of_puzzlePanel_15() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___puzzlePanel_15)); }
	inline GameObject_t1113636619 * get_puzzlePanel_15() const { return ___puzzlePanel_15; }
	inline GameObject_t1113636619 ** get_address_of_puzzlePanel_15() { return &___puzzlePanel_15; }
	inline void set_puzzlePanel_15(GameObject_t1113636619 * value)
	{
		___puzzlePanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___puzzlePanel_15), value);
	}

	inline static int32_t get_offset_of_congratsMessage_16() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___congratsMessage_16)); }
	inline GameObject_t1113636619 * get_congratsMessage_16() const { return ___congratsMessage_16; }
	inline GameObject_t1113636619 ** get_address_of_congratsMessage_16() { return &___congratsMessage_16; }
	inline void set_congratsMessage_16(GameObject_t1113636619 * value)
	{
		___congratsMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___congratsMessage_16), value);
	}

	inline static int32_t get_offset_of_countdownText_17() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___countdownText_17)); }
	inline Text_t1901882714 * get_countdownText_17() const { return ___countdownText_17; }
	inline Text_t1901882714 ** get_address_of_countdownText_17() { return &___countdownText_17; }
	inline void set_countdownText_17(Text_t1901882714 * value)
	{
		___countdownText_17 = value;
		Il2CppCodeGenWriteBarrier((&___countdownText_17), value);
	}

	inline static int32_t get_offset_of_bInit_18() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___bInit_18)); }
	inline bool get_bInit_18() const { return ___bInit_18; }
	inline bool* get_address_of_bInit_18() { return &___bInit_18; }
	inline void set_bInit_18(bool value)
	{
		___bInit_18 = value;
	}

	inline static int32_t get_offset_of_bTokenMatched_19() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___bTokenMatched_19)); }
	inline bool get_bTokenMatched_19() const { return ___bTokenMatched_19; }
	inline bool* get_address_of_bTokenMatched_19() { return &___bTokenMatched_19; }
	inline void set_bTokenMatched_19(bool value)
	{
		___bTokenMatched_19 = value;
	}

	inline static int32_t get_offset_of_matchedPosition_20() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___matchedPosition_20)); }
	inline Vector2_t2156229523  get_matchedPosition_20() const { return ___matchedPosition_20; }
	inline Vector2_t2156229523 * get_address_of_matchedPosition_20() { return &___matchedPosition_20; }
	inline void set_matchedPosition_20(Vector2_t2156229523  value)
	{
		___matchedPosition_20 = value;
	}

	inline static int32_t get_offset_of_CurrentToken_21() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___CurrentToken_21)); }
	inline UIPuzzleToken_t1662105885 * get_CurrentToken_21() const { return ___CurrentToken_21; }
	inline UIPuzzleToken_t1662105885 ** get_address_of_CurrentToken_21() { return &___CurrentToken_21; }
	inline void set_CurrentToken_21(UIPuzzleToken_t1662105885 * value)
	{
		___CurrentToken_21 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentToken_21), value);
	}

	inline static int32_t get_offset_of_matchCount_22() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___matchCount_22)); }
	inline int32_t get_matchCount_22() const { return ___matchCount_22; }
	inline int32_t* get_address_of_matchCount_22() { return &___matchCount_22; }
	inline void set_matchCount_22(int32_t value)
	{
		___matchCount_22 = value;
	}

	inline static int32_t get_offset_of_manager_24() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___manager_24)); }
	inline QuestManager_t588401851 * get_manager_24() const { return ___manager_24; }
	inline QuestManager_t588401851 ** get_address_of_manager_24() { return &___manager_24; }
	inline void set_manager_24(QuestManager_t588401851 * value)
	{
		___manager_24 = value;
		Il2CppCodeGenWriteBarrier((&___manager_24), value);
	}
};

struct UIPuzzleController_t2836468369_StaticFields
{
public:
	// System.Int32 UIPuzzleController::TOTAL_TOKENS
	int32_t ___TOTAL_TOKENS_2;
	// UIPuzzleController/PuzzleControllerEventHandler UIPuzzleController::OnEndPuzzle
	PuzzleControllerEventHandler_t963335703 * ___OnEndPuzzle_23;

public:
	inline static int32_t get_offset_of_TOTAL_TOKENS_2() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369_StaticFields, ___TOTAL_TOKENS_2)); }
	inline int32_t get_TOTAL_TOKENS_2() const { return ___TOTAL_TOKENS_2; }
	inline int32_t* get_address_of_TOTAL_TOKENS_2() { return &___TOTAL_TOKENS_2; }
	inline void set_TOTAL_TOKENS_2(int32_t value)
	{
		___TOTAL_TOKENS_2 = value;
	}

	inline static int32_t get_offset_of_OnEndPuzzle_23() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369_StaticFields, ___OnEndPuzzle_23)); }
	inline PuzzleControllerEventHandler_t963335703 * get_OnEndPuzzle_23() const { return ___OnEndPuzzle_23; }
	inline PuzzleControllerEventHandler_t963335703 ** get_address_of_OnEndPuzzle_23() { return &___OnEndPuzzle_23; }
	inline void set_OnEndPuzzle_23(PuzzleControllerEventHandler_t963335703 * value)
	{
		___OnEndPuzzle_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnEndPuzzle_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPUZZLECONTROLLER_T2836468369_H
#ifndef UIPUZZLEINSERT_T594790890_H
#define UIPUZZLEINSERT_T594790890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleInsert
struct  UIPuzzleInsert_t594790890  : public MonoBehaviour_t3962482529
{
public:
	// UIPuzzleController UIPuzzleInsert::puzzleController
	UIPuzzleController_t2836468369 * ___puzzleController_2;
	// EPuzzleToken UIPuzzleInsert::Token
	int32_t ___Token_3;
	// System.Boolean UIPuzzleInsert::debugLog
	bool ___debugLog_5;
	// System.Boolean UIPuzzleInsert::match
	bool ___match_6;

public:
	inline static int32_t get_offset_of_puzzleController_2() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___puzzleController_2)); }
	inline UIPuzzleController_t2836468369 * get_puzzleController_2() const { return ___puzzleController_2; }
	inline UIPuzzleController_t2836468369 ** get_address_of_puzzleController_2() { return &___puzzleController_2; }
	inline void set_puzzleController_2(UIPuzzleController_t2836468369 * value)
	{
		___puzzleController_2 = value;
		Il2CppCodeGenWriteBarrier((&___puzzleController_2), value);
	}

	inline static int32_t get_offset_of_Token_3() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___Token_3)); }
	inline int32_t get_Token_3() const { return ___Token_3; }
	inline int32_t* get_address_of_Token_3() { return &___Token_3; }
	inline void set_Token_3(int32_t value)
	{
		___Token_3 = value;
	}

	inline static int32_t get_offset_of_debugLog_5() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___debugLog_5)); }
	inline bool get_debugLog_5() const { return ___debugLog_5; }
	inline bool* get_address_of_debugLog_5() { return &___debugLog_5; }
	inline void set_debugLog_5(bool value)
	{
		___debugLog_5 = value;
	}

	inline static int32_t get_offset_of_match_6() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___match_6)); }
	inline bool get_match_6() const { return ___match_6; }
	inline bool* get_address_of_match_6() { return &___match_6; }
	inline void set_match_6(bool value)
	{
		___match_6 = value;
	}
};

struct UIPuzzleInsert_t594790890_StaticFields
{
public:
	// UIPuzzleInsert/PuzzleInsertEventHandler UIPuzzleInsert::OnPointerEvent
	PuzzleInsertEventHandler_t3824397729 * ___OnPointerEvent_4;

public:
	inline static int32_t get_offset_of_OnPointerEvent_4() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890_StaticFields, ___OnPointerEvent_4)); }
	inline PuzzleInsertEventHandler_t3824397729 * get_OnPointerEvent_4() const { return ___OnPointerEvent_4; }
	inline PuzzleInsertEventHandler_t3824397729 ** get_address_of_OnPointerEvent_4() { return &___OnPointerEvent_4; }
	inline void set_OnPointerEvent_4(PuzzleInsertEventHandler_t3824397729 * value)
	{
		___OnPointerEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPUZZLEINSERT_T594790890_H
#ifndef UIPUZZLETOKEN_T1662105885_H
#define UIPUZZLETOKEN_T1662105885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleToken
struct  UIPuzzleToken_t1662105885  : public MonoBehaviour_t3962482529
{
public:
	// EPuzzleToken UIPuzzleToken::token
	int32_t ___token_2;
	// System.Single UIPuzzleToken::moveSpeed
	float ___moveSpeed_3;
	// System.Boolean UIPuzzleToken::<IsGrabbable>k__BackingField
	bool ___U3CIsGrabbableU3Ek__BackingField_4;
	// System.Boolean UIPuzzleToken::matched
	bool ___matched_5;
	// UnityEngine.Vector3 UIPuzzleToken::<StartPosition>k__BackingField
	Vector3_t3722313464  ___U3CStartPositionU3Ek__BackingField_6;
	// UnityEngine.RectTransform UIPuzzleToken::rectTransform
	RectTransform_t3704657025 * ___rectTransform_8;
	// UnityEngine.Vector2 UIPuzzleToken::mousePosition
	Vector2_t2156229523  ___mousePosition_9;
	// System.Boolean UIPuzzleToken::restart
	bool ___restart_10;
	// System.Single UIPuzzleToken::deltaTime
	float ___deltaTime_11;
	// UnityEngine.Vector3 UIPuzzleToken::position
	Vector3_t3722313464  ___position_12;
	// System.Boolean UIPuzzleToken::dragged
	bool ___dragged_13;
	// UnityEngine.UI.Image UIPuzzleToken::image
	Image_t2670269651 * ___image_14;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___token_2)); }
	inline int32_t get_token_2() const { return ___token_2; }
	inline int32_t* get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(int32_t value)
	{
		___token_2 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_3() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___moveSpeed_3)); }
	inline float get_moveSpeed_3() const { return ___moveSpeed_3; }
	inline float* get_address_of_moveSpeed_3() { return &___moveSpeed_3; }
	inline void set_moveSpeed_3(float value)
	{
		___moveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsGrabbableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___U3CIsGrabbableU3Ek__BackingField_4)); }
	inline bool get_U3CIsGrabbableU3Ek__BackingField_4() const { return ___U3CIsGrabbableU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsGrabbableU3Ek__BackingField_4() { return &___U3CIsGrabbableU3Ek__BackingField_4; }
	inline void set_U3CIsGrabbableU3Ek__BackingField_4(bool value)
	{
		___U3CIsGrabbableU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_matched_5() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___matched_5)); }
	inline bool get_matched_5() const { return ___matched_5; }
	inline bool* get_address_of_matched_5() { return &___matched_5; }
	inline void set_matched_5(bool value)
	{
		___matched_5 = value;
	}

	inline static int32_t get_offset_of_U3CStartPositionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___U3CStartPositionU3Ek__BackingField_6)); }
	inline Vector3_t3722313464  get_U3CStartPositionU3Ek__BackingField_6() const { return ___U3CStartPositionU3Ek__BackingField_6; }
	inline Vector3_t3722313464 * get_address_of_U3CStartPositionU3Ek__BackingField_6() { return &___U3CStartPositionU3Ek__BackingField_6; }
	inline void set_U3CStartPositionU3Ek__BackingField_6(Vector3_t3722313464  value)
	{
		___U3CStartPositionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_rectTransform_8() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___rectTransform_8)); }
	inline RectTransform_t3704657025 * get_rectTransform_8() const { return ___rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_8() { return &___rectTransform_8; }
	inline void set_rectTransform_8(RectTransform_t3704657025 * value)
	{
		___rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_8), value);
	}

	inline static int32_t get_offset_of_mousePosition_9() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___mousePosition_9)); }
	inline Vector2_t2156229523  get_mousePosition_9() const { return ___mousePosition_9; }
	inline Vector2_t2156229523 * get_address_of_mousePosition_9() { return &___mousePosition_9; }
	inline void set_mousePosition_9(Vector2_t2156229523  value)
	{
		___mousePosition_9 = value;
	}

	inline static int32_t get_offset_of_restart_10() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___restart_10)); }
	inline bool get_restart_10() const { return ___restart_10; }
	inline bool* get_address_of_restart_10() { return &___restart_10; }
	inline void set_restart_10(bool value)
	{
		___restart_10 = value;
	}

	inline static int32_t get_offset_of_deltaTime_11() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___deltaTime_11)); }
	inline float get_deltaTime_11() const { return ___deltaTime_11; }
	inline float* get_address_of_deltaTime_11() { return &___deltaTime_11; }
	inline void set_deltaTime_11(float value)
	{
		___deltaTime_11 = value;
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___position_12)); }
	inline Vector3_t3722313464  get_position_12() const { return ___position_12; }
	inline Vector3_t3722313464 * get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(Vector3_t3722313464  value)
	{
		___position_12 = value;
	}

	inline static int32_t get_offset_of_dragged_13() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___dragged_13)); }
	inline bool get_dragged_13() const { return ___dragged_13; }
	inline bool* get_address_of_dragged_13() { return &___dragged_13; }
	inline void set_dragged_13(bool value)
	{
		___dragged_13 = value;
	}

	inline static int32_t get_offset_of_image_14() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___image_14)); }
	inline Image_t2670269651 * get_image_14() const { return ___image_14; }
	inline Image_t2670269651 ** get_address_of_image_14() { return &___image_14; }
	inline void set_image_14(Image_t2670269651 * value)
	{
		___image_14 = value;
		Il2CppCodeGenWriteBarrier((&___image_14), value);
	}
};

struct UIPuzzleToken_t1662105885_StaticFields
{
public:
	// UIPuzzleToken/PuzzleTokenEventHandler UIPuzzleToken::OnDragEvent
	PuzzleTokenEventHandler_t3807414068 * ___OnDragEvent_7;

public:
	inline static int32_t get_offset_of_OnDragEvent_7() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885_StaticFields, ___OnDragEvent_7)); }
	inline PuzzleTokenEventHandler_t3807414068 * get_OnDragEvent_7() const { return ___OnDragEvent_7; }
	inline PuzzleTokenEventHandler_t3807414068 ** get_address_of_OnDragEvent_7() { return &___OnDragEvent_7; }
	inline void set_OnDragEvent_7(PuzzleTokenEventHandler_t3807414068 * value)
	{
		___OnDragEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnDragEvent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPUZZLETOKEN_T1662105885_H
#ifndef QUEST_T3696879532_H
#define QUEST_T3696879532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest
struct  Quest_t3696879532  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Quest::questID
	int32_t ___questID_2;
	// QuestManager Quest::manager
	QuestManager_t588401851 * ___manager_3;
	// System.Boolean Quest::isActivity
	bool ___isActivity_4;
	// System.String[] Quest::textActivity
	StringU5BU5D_t1281789340* ___textActivity_5;
	// System.Boolean Quest::isSelectMultiple
	bool ___isSelectMultiple_6;
	// System.Int32 Quest::numberOptions
	int32_t ___numberOptions_7;
	// System.String[] Quest::options
	StringU5BU5D_t1281789340* ___options_8;
	// System.Int32 Quest::rigthOption
	int32_t ___rigthOption_9;
	// System.Int32 Quest::optionsSelected
	int32_t ___optionsSelected_10;
	// System.Boolean Quest::isStory
	bool ___isStory_11;
	// System.Int32 Quest::numberOfStories
	int32_t ___numberOfStories_12;
	// System.String[] Quest::stories
	StringU5BU5D_t1281789340* ___stories_13;
	// System.Boolean[] Quest::storiesCompleted
	BooleanU5BU5D_t2897418192* ___storiesCompleted_14;
	// System.Boolean Quest::isFinal
	bool ___isFinal_15;
	// CharacterBehavior Quest::characterBehavior
	CharacterBehavior_t2392107484 * ___characterBehavior_16;
	// SceneChanger Quest::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_17;
	// System.String Quest::sceneName
	String_t* ___sceneName_18;
	// System.String Quest::startText
	String_t* ___startText_19;
	// System.String Quest::completedText
	String_t* ___completedText_20;
	// System.String Quest::wrongText
	String_t* ___wrongText_21;

public:
	inline static int32_t get_offset_of_questID_2() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___questID_2)); }
	inline int32_t get_questID_2() const { return ___questID_2; }
	inline int32_t* get_address_of_questID_2() { return &___questID_2; }
	inline void set_questID_2(int32_t value)
	{
		___questID_2 = value;
	}

	inline static int32_t get_offset_of_manager_3() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___manager_3)); }
	inline QuestManager_t588401851 * get_manager_3() const { return ___manager_3; }
	inline QuestManager_t588401851 ** get_address_of_manager_3() { return &___manager_3; }
	inline void set_manager_3(QuestManager_t588401851 * value)
	{
		___manager_3 = value;
		Il2CppCodeGenWriteBarrier((&___manager_3), value);
	}

	inline static int32_t get_offset_of_isActivity_4() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isActivity_4)); }
	inline bool get_isActivity_4() const { return ___isActivity_4; }
	inline bool* get_address_of_isActivity_4() { return &___isActivity_4; }
	inline void set_isActivity_4(bool value)
	{
		___isActivity_4 = value;
	}

	inline static int32_t get_offset_of_textActivity_5() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___textActivity_5)); }
	inline StringU5BU5D_t1281789340* get_textActivity_5() const { return ___textActivity_5; }
	inline StringU5BU5D_t1281789340** get_address_of_textActivity_5() { return &___textActivity_5; }
	inline void set_textActivity_5(StringU5BU5D_t1281789340* value)
	{
		___textActivity_5 = value;
		Il2CppCodeGenWriteBarrier((&___textActivity_5), value);
	}

	inline static int32_t get_offset_of_isSelectMultiple_6() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isSelectMultiple_6)); }
	inline bool get_isSelectMultiple_6() const { return ___isSelectMultiple_6; }
	inline bool* get_address_of_isSelectMultiple_6() { return &___isSelectMultiple_6; }
	inline void set_isSelectMultiple_6(bool value)
	{
		___isSelectMultiple_6 = value;
	}

	inline static int32_t get_offset_of_numberOptions_7() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___numberOptions_7)); }
	inline int32_t get_numberOptions_7() const { return ___numberOptions_7; }
	inline int32_t* get_address_of_numberOptions_7() { return &___numberOptions_7; }
	inline void set_numberOptions_7(int32_t value)
	{
		___numberOptions_7 = value;
	}

	inline static int32_t get_offset_of_options_8() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___options_8)); }
	inline StringU5BU5D_t1281789340* get_options_8() const { return ___options_8; }
	inline StringU5BU5D_t1281789340** get_address_of_options_8() { return &___options_8; }
	inline void set_options_8(StringU5BU5D_t1281789340* value)
	{
		___options_8 = value;
		Il2CppCodeGenWriteBarrier((&___options_8), value);
	}

	inline static int32_t get_offset_of_rigthOption_9() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___rigthOption_9)); }
	inline int32_t get_rigthOption_9() const { return ___rigthOption_9; }
	inline int32_t* get_address_of_rigthOption_9() { return &___rigthOption_9; }
	inline void set_rigthOption_9(int32_t value)
	{
		___rigthOption_9 = value;
	}

	inline static int32_t get_offset_of_optionsSelected_10() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___optionsSelected_10)); }
	inline int32_t get_optionsSelected_10() const { return ___optionsSelected_10; }
	inline int32_t* get_address_of_optionsSelected_10() { return &___optionsSelected_10; }
	inline void set_optionsSelected_10(int32_t value)
	{
		___optionsSelected_10 = value;
	}

	inline static int32_t get_offset_of_isStory_11() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isStory_11)); }
	inline bool get_isStory_11() const { return ___isStory_11; }
	inline bool* get_address_of_isStory_11() { return &___isStory_11; }
	inline void set_isStory_11(bool value)
	{
		___isStory_11 = value;
	}

	inline static int32_t get_offset_of_numberOfStories_12() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___numberOfStories_12)); }
	inline int32_t get_numberOfStories_12() const { return ___numberOfStories_12; }
	inline int32_t* get_address_of_numberOfStories_12() { return &___numberOfStories_12; }
	inline void set_numberOfStories_12(int32_t value)
	{
		___numberOfStories_12 = value;
	}

	inline static int32_t get_offset_of_stories_13() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___stories_13)); }
	inline StringU5BU5D_t1281789340* get_stories_13() const { return ___stories_13; }
	inline StringU5BU5D_t1281789340** get_address_of_stories_13() { return &___stories_13; }
	inline void set_stories_13(StringU5BU5D_t1281789340* value)
	{
		___stories_13 = value;
		Il2CppCodeGenWriteBarrier((&___stories_13), value);
	}

	inline static int32_t get_offset_of_storiesCompleted_14() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___storiesCompleted_14)); }
	inline BooleanU5BU5D_t2897418192* get_storiesCompleted_14() const { return ___storiesCompleted_14; }
	inline BooleanU5BU5D_t2897418192** get_address_of_storiesCompleted_14() { return &___storiesCompleted_14; }
	inline void set_storiesCompleted_14(BooleanU5BU5D_t2897418192* value)
	{
		___storiesCompleted_14 = value;
		Il2CppCodeGenWriteBarrier((&___storiesCompleted_14), value);
	}

	inline static int32_t get_offset_of_isFinal_15() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isFinal_15)); }
	inline bool get_isFinal_15() const { return ___isFinal_15; }
	inline bool* get_address_of_isFinal_15() { return &___isFinal_15; }
	inline void set_isFinal_15(bool value)
	{
		___isFinal_15 = value;
	}

	inline static int32_t get_offset_of_characterBehavior_16() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___characterBehavior_16)); }
	inline CharacterBehavior_t2392107484 * get_characterBehavior_16() const { return ___characterBehavior_16; }
	inline CharacterBehavior_t2392107484 ** get_address_of_characterBehavior_16() { return &___characterBehavior_16; }
	inline void set_characterBehavior_16(CharacterBehavior_t2392107484 * value)
	{
		___characterBehavior_16 = value;
		Il2CppCodeGenWriteBarrier((&___characterBehavior_16), value);
	}

	inline static int32_t get_offset_of_sceneChanger_17() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___sceneChanger_17)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_17() const { return ___sceneChanger_17; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_17() { return &___sceneChanger_17; }
	inline void set_sceneChanger_17(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_17), value);
	}

	inline static int32_t get_offset_of_sceneName_18() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___sceneName_18)); }
	inline String_t* get_sceneName_18() const { return ___sceneName_18; }
	inline String_t** get_address_of_sceneName_18() { return &___sceneName_18; }
	inline void set_sceneName_18(String_t* value)
	{
		___sceneName_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_18), value);
	}

	inline static int32_t get_offset_of_startText_19() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___startText_19)); }
	inline String_t* get_startText_19() const { return ___startText_19; }
	inline String_t** get_address_of_startText_19() { return &___startText_19; }
	inline void set_startText_19(String_t* value)
	{
		___startText_19 = value;
		Il2CppCodeGenWriteBarrier((&___startText_19), value);
	}

	inline static int32_t get_offset_of_completedText_20() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___completedText_20)); }
	inline String_t* get_completedText_20() const { return ___completedText_20; }
	inline String_t** get_address_of_completedText_20() { return &___completedText_20; }
	inline void set_completedText_20(String_t* value)
	{
		___completedText_20 = value;
		Il2CppCodeGenWriteBarrier((&___completedText_20), value);
	}

	inline static int32_t get_offset_of_wrongText_21() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___wrongText_21)); }
	inline String_t* get_wrongText_21() const { return ___wrongText_21; }
	inline String_t** get_address_of_wrongText_21() { return &___wrongText_21; }
	inline void set_wrongText_21(String_t* value)
	{
		___wrongText_21 = value;
		Il2CppCodeGenWriteBarrier((&___wrongText_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEST_T3696879532_H
#ifndef QUESTMANAGER_T588401851_H
#define QUESTMANAGER_T588401851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestManager
struct  QuestManager_t588401851  : public MonoBehaviour_t3962482529
{
public:
	// Quest[] QuestManager::quests
	QuestU5BU5D_t1937508389* ___quests_2;
	// System.Boolean[] QuestManager::questCompleted
	BooleanU5BU5D_t2897418192* ___questCompleted_3;
	// System.Boolean QuestManager::isCompleted
	bool ___isCompleted_4;
	// System.Int32 QuestManager::questID
	int32_t ___questID_5;
	// System.Int32 QuestManager::storySelected
	int32_t ___storySelected_6;
	// GameController QuestManager::gameController
	GameController_t2330501625 * ___gameController_7;

public:
	inline static int32_t get_offset_of_quests_2() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___quests_2)); }
	inline QuestU5BU5D_t1937508389* get_quests_2() const { return ___quests_2; }
	inline QuestU5BU5D_t1937508389** get_address_of_quests_2() { return &___quests_2; }
	inline void set_quests_2(QuestU5BU5D_t1937508389* value)
	{
		___quests_2 = value;
		Il2CppCodeGenWriteBarrier((&___quests_2), value);
	}

	inline static int32_t get_offset_of_questCompleted_3() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___questCompleted_3)); }
	inline BooleanU5BU5D_t2897418192* get_questCompleted_3() const { return ___questCompleted_3; }
	inline BooleanU5BU5D_t2897418192** get_address_of_questCompleted_3() { return &___questCompleted_3; }
	inline void set_questCompleted_3(BooleanU5BU5D_t2897418192* value)
	{
		___questCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___questCompleted_3), value);
	}

	inline static int32_t get_offset_of_isCompleted_4() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___isCompleted_4)); }
	inline bool get_isCompleted_4() const { return ___isCompleted_4; }
	inline bool* get_address_of_isCompleted_4() { return &___isCompleted_4; }
	inline void set_isCompleted_4(bool value)
	{
		___isCompleted_4 = value;
	}

	inline static int32_t get_offset_of_questID_5() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___questID_5)); }
	inline int32_t get_questID_5() const { return ___questID_5; }
	inline int32_t* get_address_of_questID_5() { return &___questID_5; }
	inline void set_questID_5(int32_t value)
	{
		___questID_5 = value;
	}

	inline static int32_t get_offset_of_storySelected_6() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___storySelected_6)); }
	inline int32_t get_storySelected_6() const { return ___storySelected_6; }
	inline int32_t* get_address_of_storySelected_6() { return &___storySelected_6; }
	inline void set_storySelected_6(int32_t value)
	{
		___storySelected_6 = value;
	}

	inline static int32_t get_offset_of_gameController_7() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___gameController_7)); }
	inline GameController_t2330501625 * get_gameController_7() const { return ___gameController_7; }
	inline GameController_t2330501625 ** get_address_of_gameController_7() { return &___gameController_7; }
	inline void set_gameController_7(GameController_t2330501625 * value)
	{
		___gameController_7 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_7), value);
	}
};

struct QuestManager_t588401851_StaticFields
{
public:
	// QuestManager QuestManager::_instance
	QuestManager_t588401851 * ____instance_8;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(QuestManager_t588401851_StaticFields, ____instance_8)); }
	inline QuestManager_t588401851 * get__instance_8() const { return ____instance_8; }
	inline QuestManager_t588401851 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(QuestManager_t588401851 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTMANAGER_T588401851_H
#ifndef GAMECONTROLLER_T2330501625_H
#define GAMECONTROLLER_T2330501625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t2330501625  : public MonoBehaviour_t3962482529
{
public:
	// DataController GameController::dataController
	DataController_t353634109 * ___dataController_2;
	// SceneChanger GameController::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_3;
	// QuestManager GameController::manager
	QuestManager_t588401851 * ___manager_4;
	// CharacterBehavior GameController::character
	CharacterBehavior_t2392107484 * ___character_5;

public:
	inline static int32_t get_offset_of_dataController_2() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___dataController_2)); }
	inline DataController_t353634109 * get_dataController_2() const { return ___dataController_2; }
	inline DataController_t353634109 ** get_address_of_dataController_2() { return &___dataController_2; }
	inline void set_dataController_2(DataController_t353634109 * value)
	{
		___dataController_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_2), value);
	}

	inline static int32_t get_offset_of_sceneChanger_3() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___sceneChanger_3)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_3() const { return ___sceneChanger_3; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_3() { return &___sceneChanger_3; }
	inline void set_sceneChanger_3(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_3 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_3), value);
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___manager_4)); }
	inline QuestManager_t588401851 * get_manager_4() const { return ___manager_4; }
	inline QuestManager_t588401851 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(QuestManager_t588401851 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}

	inline static int32_t get_offset_of_character_5() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___character_5)); }
	inline CharacterBehavior_t2392107484 * get_character_5() const { return ___character_5; }
	inline CharacterBehavior_t2392107484 ** get_address_of_character_5() { return &___character_5; }
	inline void set_character_5(CharacterBehavior_t2392107484 * value)
	{
		___character_5 = value;
		Il2CppCodeGenWriteBarrier((&___character_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROLLER_T2330501625_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (CinemachineTrack_t2126039464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (ExtendUtility_t1436226533), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (LAMEPreset_t1074290626)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4002[39] = 
{
	LAMEPreset_t1074290626::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (MPEGMode_t406895743)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4003[5] = 
{
	MPEGMode_t406895743::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (ASMOptimizations_t3241729094)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4004[4] = 
{
	ASMOptimizations_t3241729094::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (VBRMode_t3062980280)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4005[7] = 
{
	VBRMode_t3062980280::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (MPEGVersion_t749196590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4006[4] = 
{
	MPEGVersion_t749196590::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (LAMEVersion_t651286643), sizeof(LAMEVersion_t651286643_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4007[9] = 
{
	LAMEVersion_t651286643::get_offset_of_major_0(),
	LAMEVersion_t651286643::get_offset_of_minor_1(),
	LAMEVersion_t651286643::get_offset_of_alpha_2(),
	LAMEVersion_t651286643::get_offset_of_beta_3(),
	LAMEVersion_t651286643::get_offset_of_psy_major_4(),
	LAMEVersion_t651286643::get_offset_of_psy_minor_5(),
	LAMEVersion_t651286643::get_offset_of_psy_alpha_6(),
	LAMEVersion_t651286643::get_offset_of_psy_beta_7(),
	LAMEVersion_t651286643::get_offset_of_features_ptr_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (LibMp3Lame_t153112877), -1, sizeof(LibMp3Lame_t153112877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4008[45] = 
{
	LibMp3Lame_t153112877::get_offset_of_context_0(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_3(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_4(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_5(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_6(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_7(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_8(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_9(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_10(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_11(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_12(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_13(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_14(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_15(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_16(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_17(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_18(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_19(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_20(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache14_21(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache15_22(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache16_23(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache17_24(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache18_25(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache19_26(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1A_27(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1B_28(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1C_29(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1D_30(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1E_31(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1F_32(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache20_33(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache21_34(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache22_35(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache23_36(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache24_37(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache25_38(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache26_39(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache27_40(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache28_41(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache29_42(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2A_43(),
	LibMp3Lame_t153112877_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2B_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (NativeMethods_t3997983053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (delReportFunction_t2317660497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (MP3EncoderWrapper_t279410081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (U3C_WaveToMP3U3Ec__Iterator0_t3429961031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4013[9] = 
{
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_waveFileName_0(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_U3CreaderU3E__1_1(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_mp3FileName_2(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_bitRate_3(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_U3CwriterU3E__2_4(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_OnComplete_5(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_U24current_6(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_U24disposing_7(),
	U3C_WaveToMP3U3Ec__Iterator0_t3429961031::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (LameMP3FileWriter_t44674093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4014[10] = 
{
	LameMP3FileWriter_t44674093::get_offset_of__lame_4(),
	LameMP3FileWriter_t44674093::get_offset_of_inputFormat_5(),
	LameMP3FileWriter_t44674093::get_offset_of_outStream_6(),
	LameMP3FileWriter_t44674093::get_offset_of_disposeOutput_7(),
	LameMP3FileWriter_t44674093::get_offset_of_inBuffer_8(),
	LameMP3FileWriter_t44674093::get_offset_of_inPosition_9(),
	LameMP3FileWriter_t44674093::get_offset_of_outBuffer_10(),
	LameMP3FileWriter_t44674093::get_offset_of_InputByteCount_11(),
	LameMP3FileWriter_t44674093::get_offset_of_OutputByteCount_12(),
	LameMP3FileWriter_t44674093::get_offset_of__encode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (ArrayUnion_t2417265785), sizeof(ArrayUnion_t2417265785_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4015[7] = 
{
	ArrayUnion_t2417265785::get_offset_of_nBytes_0(),
	ArrayUnion_t2417265785::get_offset_of_bytes_1(),
	ArrayUnion_t2417265785::get_offset_of_shorts_2(),
	ArrayUnion_t2417265785::get_offset_of_ints_3(),
	ArrayUnion_t2417265785::get_offset_of_longs_4(),
	ArrayUnion_t2417265785::get_offset_of_floats_5(),
	ArrayUnion_t2417265785::get_offset_of_doubles_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (delEncode_t1434347660), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (AdpcmWaveFormat_t3207843846), sizeof(AdpcmWaveFormat_t3207843846_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4017[3] = 
{
	AdpcmWaveFormat_t3207843846::get_offset_of_samplesPerBlock_7(),
	AdpcmWaveFormat_t3207843846::get_offset_of_numCoeff_8(),
	AdpcmWaveFormat_t3207843846::get_offset_of_coefficients_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (AudioMediaSubtypes_t1633211026), -1, sizeof(AudioMediaSubtypes_t1633211026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4018[18] = 
{
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_PCM_0(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_PCMAudioObsolete_1(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_MPEG1Packet_2(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_MPEG1Payload_3(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_MPEG2_AUDIO_4(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_DVD_LPCM_AUDIO_5(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_DRM_Audio_6(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_IEEE_FLOAT_7(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_DOLBY_AC3_8(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_DOLBY_AC3_SPDIF_9(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_RAW_SPORT_10(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_SPDIF_TAG_241h_11(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_WMMEDIASUBTYPE_MP3_12(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_WAVE_13(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_AU_14(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_MEDIASUBTYPE_AIFF_15(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_AudioSubTypes_16(),
	AudioMediaSubtypes_t1633211026_StaticFields::get_offset_of_AudioSubTypeNames_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (Gsm610WaveFormat_t3021274509), sizeof(Gsm610WaveFormat_t3021274509_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4019[1] = 
{
	Gsm610WaveFormat_t3021274509::get_offset_of_samplesPerBlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (RiffChunk_t95273145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4021[3] = 
{
	RiffChunk_t95273145::get_offset_of_identifier_0(),
	RiffChunk_t95273145::get_offset_of_length_1(),
	RiffChunk_t95273145::get_offset_of_streamPosition_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (WaveFileReader_t3404544520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4022[6] = 
{
	WaveFileReader_t3404544520::get_offset_of_waveFormat_4(),
	WaveFileReader_t3404544520::get_offset_of_waveStream_5(),
	WaveFileReader_t3404544520::get_offset_of_ownInput_6(),
	WaveFileReader_t3404544520::get_offset_of_dataPosition_7(),
	WaveFileReader_t3404544520::get_offset_of_dataChunkLength_8(),
	WaveFileReader_t3404544520::get_offset_of_chunks_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (WaveFormat_t2842237185), sizeof(WaveFormat_t2842237185_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4023[7] = 
{
	WaveFormat_t2842237185::get_offset_of_waveFormatTag_0(),
	WaveFormat_t2842237185::get_offset_of_channels_1(),
	WaveFormat_t2842237185::get_offset_of_sampleRate_2(),
	WaveFormat_t2842237185::get_offset_of_averageBytesPerSecond_3(),
	WaveFormat_t2842237185::get_offset_of_blockAlign_4(),
	WaveFormat_t2842237185::get_offset_of_bitsPerSample_5(),
	WaveFormat_t2842237185::get_offset_of_extraSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (WaveFormatEncoding_t2321404235)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4024[149] = 
{
	WaveFormatEncoding_t2321404235::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (WaveFormatExtensible_t1054910509), sizeof(WaveFormatExtensible_t1054910509_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4025[3] = 
{
	WaveFormatExtensible_t1054910509::get_offset_of_wValidBitsPerSample_7(),
	WaveFormatExtensible_t1054910509::get_offset_of_dwChannelMask_8(),
	WaveFormatExtensible_t1054910509::get_offset_of_subFormat_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (WaveFormatExtraData_t1489494699), sizeof(WaveFormatExtraData_t1489494699_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4026[1] = 
{
	WaveFormatExtraData_t1489494699::get_offset_of_extraData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (WaveStream_t91297746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (JsonType_t2731125707)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4028[9] = 
{
	JsonType_t2731125707::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (JsonData_t1524858407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4030[10] = 
{
	JsonData_t1524858407::get_offset_of_inst_array_0(),
	JsonData_t1524858407::get_offset_of_inst_boolean_1(),
	JsonData_t1524858407::get_offset_of_inst_double_2(),
	JsonData_t1524858407::get_offset_of_inst_int_3(),
	JsonData_t1524858407::get_offset_of_inst_long_4(),
	JsonData_t1524858407::get_offset_of_inst_object_5(),
	JsonData_t1524858407::get_offset_of_inst_string_6(),
	JsonData_t1524858407::get_offset_of_json_7(),
	JsonData_t1524858407::get_offset_of_type_8(),
	JsonData_t1524858407::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (OrderedDictionaryEnumerator_t386339177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[1] = 
{
	OrderedDictionaryEnumerator_t386339177::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (JsonException_t3682484112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (PropertyMetadata_t3727440473)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4033[3] = 
{
	PropertyMetadata_t3727440473::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropertyMetadata_t3727440473::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropertyMetadata_t3727440473::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (ArrayMetadata_t894288939)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[3] = 
{
	ArrayMetadata_t894288939::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArrayMetadata_t894288939::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArrayMetadata_t894288939::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (ObjectMetadata_t3566284522)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[3] = 
{
	ObjectMetadata_t3566284522::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectMetadata_t3566284522::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectMetadata_t3566284522::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (ExporterFunc_t1851311465), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (ImporterFunc_t3630937194), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (WrapperFactory_t2158548929), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (JsonMapper_t3815285241), -1, sizeof(JsonMapper_t3815285241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4041[43] = 
{
	JsonMapper_t3815285241_StaticFields::get_offset_of_max_nesting_depth_0(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_datetime_format_1(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_base_exporters_table_2(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_custom_exporters_table_3(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_base_importers_table_4(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_custom_importers_table_5(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_array_metadata_6(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_array_metadata_lock_7(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_conv_ops_8(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_conv_ops_lock_9(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_object_metadata_10(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_object_metadata_lock_11(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_type_properties_12(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_type_properties_lock_13(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_static_writer_14(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_static_writer_lock_15(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_17(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_18(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_19(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_20(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_21(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_22(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_23(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_24(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_25(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_26(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_27(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_28(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_29(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_30(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_31(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_32(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_33(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_34(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_35(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_36(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_37(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_38(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_39(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_40(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache19_41(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4043[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (JsonMockWrapper_t82875095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (JsonToken_t3605538862)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4045[13] = 
{
	JsonToken_t3605538862::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (JsonReader_t836887441), -1, sizeof(JsonReader_t836887441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4046[15] = 
{
	JsonReader_t836887441_StaticFields::get_offset_of_parse_table_0(),
	JsonReader_t836887441::get_offset_of_automaton_stack_1(),
	JsonReader_t836887441::get_offset_of_current_input_2(),
	JsonReader_t836887441::get_offset_of_current_symbol_3(),
	JsonReader_t836887441::get_offset_of_end_of_json_4(),
	JsonReader_t836887441::get_offset_of_end_of_input_5(),
	JsonReader_t836887441::get_offset_of_lexer_6(),
	JsonReader_t836887441::get_offset_of_parser_in_string_7(),
	JsonReader_t836887441::get_offset_of_parser_return_8(),
	JsonReader_t836887441::get_offset_of_read_started_9(),
	JsonReader_t836887441::get_offset_of_reader_10(),
	JsonReader_t836887441::get_offset_of_reader_is_owned_11(),
	JsonReader_t836887441::get_offset_of_skip_non_members_12(),
	JsonReader_t836887441::get_offset_of_token_value_13(),
	JsonReader_t836887441::get_offset_of_token_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (Condition_t3240125930)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4047[6] = 
{
	Condition_t3240125930::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (WriterContext_t1011093999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[5] = 
{
	WriterContext_t1011093999::get_offset_of_Count_0(),
	WriterContext_t1011093999::get_offset_of_InArray_1(),
	WriterContext_t1011093999::get_offset_of_InObject_2(),
	WriterContext_t1011093999::get_offset_of_ExpectingValue_3(),
	WriterContext_t1011093999::get_offset_of_Padding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (JsonWriter_t3570089748), -1, sizeof(JsonWriter_t3570089748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4049[12] = 
{
	JsonWriter_t3570089748_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t3570089748::get_offset_of_context_1(),
	JsonWriter_t3570089748::get_offset_of_ctx_stack_2(),
	JsonWriter_t3570089748::get_offset_of_has_reached_end_3(),
	JsonWriter_t3570089748::get_offset_of_hex_seq_4(),
	JsonWriter_t3570089748::get_offset_of_indentation_5(),
	JsonWriter_t3570089748::get_offset_of_indent_value_6(),
	JsonWriter_t3570089748::get_offset_of_inst_string_builder_7(),
	JsonWriter_t3570089748::get_offset_of_pretty_print_8(),
	JsonWriter_t3570089748::get_offset_of_validate_9(),
	JsonWriter_t3570089748::get_offset_of_lower_case_properties_10(),
	JsonWriter_t3570089748::get_offset_of_writer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (FsmContext_t2331368794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4050[4] = 
{
	FsmContext_t2331368794::get_offset_of_Return_0(),
	FsmContext_t2331368794::get_offset_of_NextState_1(),
	FsmContext_t2331368794::get_offset_of_L_2(),
	FsmContext_t2331368794::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (Lexer_t1514038666), -1, sizeof(Lexer_t1514038666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4051[42] = 
{
	Lexer_t1514038666_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_t1514038666_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_t1514038666::get_offset_of_allow_comments_2(),
	Lexer_t1514038666::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_t1514038666::get_offset_of_end_of_input_4(),
	Lexer_t1514038666::get_offset_of_fsm_context_5(),
	Lexer_t1514038666::get_offset_of_input_buffer_6(),
	Lexer_t1514038666::get_offset_of_input_char_7(),
	Lexer_t1514038666::get_offset_of_reader_8(),
	Lexer_t1514038666::get_offset_of_state_9(),
	Lexer_t1514038666::get_offset_of_string_buffer_10(),
	Lexer_t1514038666::get_offset_of_string_value_11(),
	Lexer_t1514038666::get_offset_of_token_12(),
	Lexer_t1514038666::get_offset_of_unichar_13(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_15(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_16(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_17(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_18(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_19(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_20(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_21(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_22(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_23(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_24(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_25(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_26(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_27(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_28(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_29(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_30(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_31(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_32(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_33(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache14_34(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache15_35(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache16_36(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache17_37(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache18_38(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache19_39(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1A_40(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1B_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (StateHandler_t105866779), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (ParserToken_t2380208742)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4053[20] = 
{
	ParserToken_t2380208742::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (AudioRecordManager_t3166594816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4054[31] = 
{
	AudioRecordManager_t3166594816::get_offset_of_samplingRate_2(),
	AudioRecordManager_t3166594816::get_offset_of_sampleRate_3(),
	AudioRecordManager_t3166594816::get_offset_of__audioSource_4(),
	AudioRecordManager_t3166594816::get_offset_of_audioLength_5(),
	AudioRecordManager_t3166594816::get_offset_of__audioClip_6(),
	AudioRecordManager_t3166594816::get_offset_of_useMicrophone_7(),
	AudioRecordManager_t3166594816::get_offset_of__selectDevice_8(),
	AudioRecordManager_t3166594816::get_offset_of_fileName_9(),
	AudioRecordManager_t3166594816::get_offset_of_audioExtensions_10(),
	AudioRecordManager_t3166594816::get_offset_of_selectedAudioType_11(),
	AudioRecordManager_t3166594816::get_offset_of__persistentDataPath_12(),
	AudioRecordManager_t3166594816::get_offset_of__mixerGroupMicrophone_13(),
	AudioRecordManager_t3166594816::get_offset_of__mixerGroupMaster_14(),
	AudioRecordManager_t3166594816::get_offset_of__audioConfiguration_15(),
	AudioRecordManager_t3166594816::get_offset_of__mp3EncoderWrapper_16(),
	AudioRecordManager_t3166594816::get_offset_of_channel_17(),
	AudioRecordManager_t3166594816::get_offset_of_min_18(),
	AudioRecordManager_t3166594816::get_offset_of_max_19(),
	AudioRecordManager_t3166594816::get_offset_of_elapsedTime_20(),
	AudioRecordManager_t3166594816::get_offset_of_timer_21(),
	AudioRecordManager_t3166594816::get_offset_of_maxRecordTime_22(),
	AudioRecordManager_t3166594816::get_offset_of_isRecording_23(),
	AudioRecordManager_t3166594816::get_offset_of_startRecordTime_24(),
	AudioRecordManager_t3166594816::get_offset_of_timeSpan_25(),
	AudioRecordManager_t3166594816::get_offset_of_OnStartEncoding_26(),
	AudioRecordManager_t3166594816::get_offset_of_OnStopEncoding_27(),
	AudioRecordManager_t3166594816::get_offset_of_manager_28(),
	AudioRecordManager_t3166594816::get_offset_of_quest_29(),
	AudioRecordManager_t3166594816::get_offset_of_HistoryText_30(),
	AudioRecordManager_t3166594816::get_offset_of_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31(),
	AudioRecordManager_t3166594816::get_offset_of_U3CTimerTextWithFormatU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4055[4] = 
{
	U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809::get_offset_of_U24this_0(),
	U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809::get_offset_of_U24current_1(),
	U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809::get_offset_of_U24disposing_2(),
	U3C_AudioRecordWithAuthorizationU3Ec__Iterator0_t3774682809::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4056[6] = 
{
	U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674::get_offset_of_U3CinputU3E__0_0(),
	U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674::get_offset_of_U3CoutputU3E__0_1(),
	U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674::get_offset_of_U24this_2(),
	U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674::get_offset_of_U24current_3(),
	U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674::get_offset_of_U24disposing_4(),
	U3C_Mp3FileEncondingU3Ec__Iterator1_t1291331674::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (U3CLoadAudioU3Ec__Iterator2_t22865955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4057[9] = 
{
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_audioType_0(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_file_1(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_U3CpathU3E__0_2(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_U3CwwwU3E__1_3(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_OnComplete_4(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_U24this_5(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_U24current_6(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_U24disposing_7(),
	U3CLoadAudioU3Ec__Iterator2_t22865955::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (U3CdelayedShareU3Ec__Iterator3_t529057701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4058[5] = 
{
	U3CdelayedShareU3Ec__Iterator3_t529057701::get_offset_of_screenShotPath_0(),
	U3CdelayedShareU3Ec__Iterator3_t529057701::get_offset_of_text_1(),
	U3CdelayedShareU3Ec__Iterator3_t529057701::get_offset_of_U24current_2(),
	U3CdelayedShareU3Ec__Iterator3_t529057701::get_offset_of_U24disposing_3(),
	U3CdelayedShareU3Ec__Iterator3_t529057701::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (EAudioFileExtension_t2867350651)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4059[4] = 
{
	EAudioFileExtension_t2867350651::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (EAudioSamplingRate_t3810640270)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4060[5] = 
{
	EAudioSamplingRate_t3810640270::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (EAudioChannel_t4054551443)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4061[4] = 
{
	EAudioChannel_t4054551443::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (CharacterBehavior_t2392107484), -1, sizeof(CharacterBehavior_t2392107484_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4062[12] = 
{
	CharacterBehavior_t2392107484::get_offset_of_gameStarted_2(),
	CharacterBehavior_t2392107484::get_offset_of_posLevelCharacter_3(),
	CharacterBehavior_t2392107484::get_offset_of_level_4(),
	CharacterBehavior_t2392107484::get_offset_of_speed_5(),
	CharacterBehavior_t2392107484::get_offset_of_isMoving_6(),
	CharacterBehavior_t2392107484_StaticFields::get_offset_of_playerCreated_7(),
	CharacterBehavior_t2392107484::get_offset_of_initPos_8(),
	CharacterBehavior_t2392107484::get_offset_of_lastPos_9(),
	CharacterBehavior_t2392107484::get_offset_of_characteres_10(),
	CharacterBehavior_t2392107484::get_offset_of_characterSelected_11(),
	CharacterBehavior_t2392107484::get_offset_of_dataController_12(),
	CharacterBehavior_t2392107484::get_offset_of_gameController_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (U3CMoveToPosU3Ec__Iterator0_t951185036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[9] = 
{
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_U3CcounterU3E__0_0(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_fromPosition_1(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_U3CstartPosU3E__0_2(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_duration_3(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_toPosition_4(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_U24this_5(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_U24current_6(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_U24disposing_7(),
	U3CMoveToPosU3Ec__Iterator0_t951185036::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (CharacterMovi_t1536215638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[2] = 
{
	CharacterMovi_t1536215638::get_offset_of_position_2(),
	CharacterMovi_t1536215638::get_offset_of_speed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (DataController_t353634109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4065[8] = 
{
	DataController_t353634109::get_offset_of_playerData_2(),
	DataController_t353634109::get_offset_of_gameData_3(),
	DataController_t353634109::get_offset_of_gameDataFileName_4(),
	DataController_t353634109::get_offset_of_playerDataFileName_5(),
	DataController_t353634109::get_offset_of_isFirstTime_6(),
	DataController_t353634109::get_offset_of_currentLevel_7(),
	DataController_t353634109::get_offset_of_manager_8(),
	DataController_t353634109::get_offset_of_quest_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (DontDestroyOnLoad_t1456007215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (GameController_t2330501625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4067[4] = 
{
	GameController_t2330501625::get_offset_of_dataController_2(),
	GameController_t2330501625::get_offset_of_sceneChanger_3(),
	GameController_t2330501625::get_offset_of_manager_4(),
	GameController_t2330501625::get_offset_of_character_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (GameData_t415813024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4068[2] = 
{
	GameData_t415813024::get_offset_of_questCompleted_0(),
	GameData_t415813024::get_offset_of_questDatas_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (QuestData_t1216399150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4069[2] = 
{
	QuestData_t1216399150::get_offset_of_optionSelected_0(),
	QuestData_t1216399150::get_offset_of_storiesCompleted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (GameSettings_t2345380323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4070[1] = 
{
	GameSettings_t2345380323::get_offset_of_questions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (Question_t1252140549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4071[9] = 
{
	Question_t1252140549::get_offset_of_type_0(),
	Question_t1252140549::get_offset_of_index_1(),
	Question_t1252140549::get_offset_of_text_2(),
	Question_t1252140549::get_offset_of_options_3(),
	Question_t1252140549::get_offset_of_answer_4(),
	Question_t1252140549::get_offset_of_responseAnswerWrong_5(),
	Question_t1252140549::get_offset_of_responseAnsweRight_6(),
	Question_t1252140549::get_offset_of_answerRight_7(),
	Question_t1252140549::get_offset_of_completed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (SystemHelpers_t2322995976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (LevelBehavior_t1802778136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4073[8] = 
{
	LevelBehavior_t1802778136::get_offset_of_index_2(),
	LevelBehavior_t1802778136::get_offset_of_person_3(),
	LevelBehavior_t1802778136::get_offset_of_completed_4(),
	LevelBehavior_t1802778136::get_offset_of_active_5(),
	LevelBehavior_t1802778136::get_offset_of_buttons_6(),
	LevelBehavior_t1802778136::get_offset_of_spriteRenderer_7(),
	LevelBehavior_t1802778136::get_offset_of_questManager_8(),
	LevelBehavior_t1802778136::get_offset_of_level_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (LevelInteractions_t1896269609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4074[2] = 
{
	LevelInteractions_t1896269609::get_offset_of_sceneChanger_2(),
	LevelInteractions_t1896269609::get_offset_of_person_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (ListStories_t748560288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4075[6] = 
{
	ListStories_t748560288::get_offset_of_manager_2(),
	ListStories_t748560288::get_offset_of_audioRecordManager_3(),
	ListStories_t748560288::get_offset_of_contentPanel_4(),
	ListStories_t748560288::get_offset_of_panelPrefab_5(),
	ListStories_t748560288::get_offset_of_panel_6(),
	ListStories_t748560288::get_offset_of_count_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (U3CStartU3Ec__AnonStorey0_t2788251243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4076[2] = 
{
	U3CStartU3Ec__AnonStorey0_t2788251243::get_offset_of_nameStory_0(),
	U3CStartU3Ec__AnonStorey0_t2788251243::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (ManagerGame_t32073082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (ManagerSettings_t530119282), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (PlayerData_t220878115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4079[6] = 
{
	PlayerData_t220878115::get_offset_of_isFirstTime_0(),
	PlayerData_t220878115::get_offset_of_currentLevel_1(),
	PlayerData_t220878115::get_offset_of_posLevelCharacter_2(),
	PlayerData_t220878115::get_offset_of_characterName_3(),
	PlayerData_t220878115::get_offset_of_posX_4(),
	PlayerData_t220878115::get_offset_of_posY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (UIPuzzleController_t2836468369), -1, sizeof(UIPuzzleController_t2836468369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4080[23] = 
{
	UIPuzzleController_t2836468369_StaticFields::get_offset_of_TOTAL_TOKENS_2(),
	UIPuzzleController_t2836468369::get_offset_of_images_3(),
	UIPuzzleController_t2836468369::get_offset_of_imagePositions_4(),
	UIPuzzleController_t2836468369::get_offset_of_startImgPositions_5(),
	UIPuzzleController_t2836468369::get_offset_of_tokenOrder_6(),
	UIPuzzleController_t2836468369::get_offset_of_tokenImages_7(),
	UIPuzzleController_t2836468369::get_offset_of_tokens_8(),
	UIPuzzleController_t2836468369::get_offset_of_tokenPositions_9(),
	UIPuzzleController_t2836468369::get_offset_of_insertImages_10(),
	UIPuzzleController_t2836468369::get_offset_of_showTime_11(),
	UIPuzzleController_t2836468369::get_offset_of_startTime_12(),
	UIPuzzleController_t2836468369::get_offset_of_countdownTime_13(),
	UIPuzzleController_t2836468369::get_offset_of_introPanel_14(),
	UIPuzzleController_t2836468369::get_offset_of_puzzlePanel_15(),
	UIPuzzleController_t2836468369::get_offset_of_congratsMessage_16(),
	UIPuzzleController_t2836468369::get_offset_of_countdownText_17(),
	UIPuzzleController_t2836468369::get_offset_of_bInit_18(),
	UIPuzzleController_t2836468369::get_offset_of_bTokenMatched_19(),
	UIPuzzleController_t2836468369::get_offset_of_matchedPosition_20(),
	UIPuzzleController_t2836468369::get_offset_of_CurrentToken_21(),
	UIPuzzleController_t2836468369::get_offset_of_matchCount_22(),
	UIPuzzleController_t2836468369_StaticFields::get_offset_of_OnEndPuzzle_23(),
	UIPuzzleController_t2836468369::get_offset_of_manager_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (PuzzleControllerEventHandler_t963335703), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (U3C_StartLateU3Ec__Iterator0_t3966377835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4082[6] = 
{
	U3C_StartLateU3Ec__Iterator0_t3966377835::get_offset_of_U3CiU3E__1_0(),
	U3C_StartLateU3Ec__Iterator0_t3966377835::get_offset_of_U3CjU3E__2_1(),
	U3C_StartLateU3Ec__Iterator0_t3966377835::get_offset_of_U24this_2(),
	U3C_StartLateU3Ec__Iterator0_t3966377835::get_offset_of_U24current_3(),
	U3C_StartLateU3Ec__Iterator0_t3966377835::get_offset_of_U24disposing_4(),
	U3C_StartLateU3Ec__Iterator0_t3966377835::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4083[4] = 
{
	U3CDelayCompleteQuestU3Ec__Iterator1_t479202429::get_offset_of_U24this_0(),
	U3CDelayCompleteQuestU3Ec__Iterator1_t479202429::get_offset_of_U24current_1(),
	U3CDelayCompleteQuestU3Ec__Iterator1_t479202429::get_offset_of_U24disposing_2(),
	U3CDelayCompleteQuestU3Ec__Iterator1_t479202429::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (EPuzzleToken_t2465232863)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4084[7] = 
{
	EPuzzleToken_t2465232863::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (UIPuzzleInsert_t594790890), -1, sizeof(UIPuzzleInsert_t594790890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4085[5] = 
{
	UIPuzzleInsert_t594790890::get_offset_of_puzzleController_2(),
	UIPuzzleInsert_t594790890::get_offset_of_Token_3(),
	UIPuzzleInsert_t594790890_StaticFields::get_offset_of_OnPointerEvent_4(),
	UIPuzzleInsert_t594790890::get_offset_of_debugLog_5(),
	UIPuzzleInsert_t594790890::get_offset_of_match_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (PuzzleInsertEventHandler_t3824397729), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (UIPuzzleToken_t1662105885), -1, sizeof(UIPuzzleToken_t1662105885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4087[13] = 
{
	UIPuzzleToken_t1662105885::get_offset_of_token_2(),
	UIPuzzleToken_t1662105885::get_offset_of_moveSpeed_3(),
	UIPuzzleToken_t1662105885::get_offset_of_U3CIsGrabbableU3Ek__BackingField_4(),
	UIPuzzleToken_t1662105885::get_offset_of_matched_5(),
	UIPuzzleToken_t1662105885::get_offset_of_U3CStartPositionU3Ek__BackingField_6(),
	UIPuzzleToken_t1662105885_StaticFields::get_offset_of_OnDragEvent_7(),
	UIPuzzleToken_t1662105885::get_offset_of_rectTransform_8(),
	UIPuzzleToken_t1662105885::get_offset_of_mousePosition_9(),
	UIPuzzleToken_t1662105885::get_offset_of_restart_10(),
	UIPuzzleToken_t1662105885::get_offset_of_deltaTime_11(),
	UIPuzzleToken_t1662105885::get_offset_of_position_12(),
	UIPuzzleToken_t1662105885::get_offset_of_dragged_13(),
	UIPuzzleToken_t1662105885::get_offset_of_image_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (PuzzleTokenEventHandler_t3807414068), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (Quest_t3696879532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4089[20] = 
{
	Quest_t3696879532::get_offset_of_questID_2(),
	Quest_t3696879532::get_offset_of_manager_3(),
	Quest_t3696879532::get_offset_of_isActivity_4(),
	Quest_t3696879532::get_offset_of_textActivity_5(),
	Quest_t3696879532::get_offset_of_isSelectMultiple_6(),
	Quest_t3696879532::get_offset_of_numberOptions_7(),
	Quest_t3696879532::get_offset_of_options_8(),
	Quest_t3696879532::get_offset_of_rigthOption_9(),
	Quest_t3696879532::get_offset_of_optionsSelected_10(),
	Quest_t3696879532::get_offset_of_isStory_11(),
	Quest_t3696879532::get_offset_of_numberOfStories_12(),
	Quest_t3696879532::get_offset_of_stories_13(),
	Quest_t3696879532::get_offset_of_storiesCompleted_14(),
	Quest_t3696879532::get_offset_of_isFinal_15(),
	Quest_t3696879532::get_offset_of_characterBehavior_16(),
	Quest_t3696879532::get_offset_of_sceneChanger_17(),
	Quest_t3696879532::get_offset_of_sceneName_18(),
	Quest_t3696879532::get_offset_of_startText_19(),
	Quest_t3696879532::get_offset_of_completedText_20(),
	Quest_t3696879532::get_offset_of_wrongText_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (QuestDataOld_t372650824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4090[17] = 
{
	QuestDataOld_t372650824::get_offset_of_questID_0(),
	QuestDataOld_t372650824::get_offset_of_isActivity_1(),
	QuestDataOld_t372650824::get_offset_of_activityCompleted_2(),
	QuestDataOld_t372650824::get_offset_of_textActivity_3(),
	QuestDataOld_t372650824::get_offset_of_isSelectMultiple_4(),
	QuestDataOld_t372650824::get_offset_of_numberOptions_5(),
	QuestDataOld_t372650824::get_offset_of_options_6(),
	QuestDataOld_t372650824::get_offset_of_rigthOption_7(),
	QuestDataOld_t372650824::get_offset_of_optionsSelected_8(),
	QuestDataOld_t372650824::get_offset_of_isStory_9(),
	QuestDataOld_t372650824::get_offset_of_numberOfStories_10(),
	QuestDataOld_t372650824::get_offset_of_stories_11(),
	QuestDataOld_t372650824::get_offset_of_storiesCompleted_12(),
	QuestDataOld_t372650824::get_offset_of_sceneName_13(),
	QuestDataOld_t372650824::get_offset_of_startText_14(),
	QuestDataOld_t372650824::get_offset_of_completedText_15(),
	QuestDataOld_t372650824::get_offset_of_wrongText_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (QuestManager_t588401851), -1, sizeof(QuestManager_t588401851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4091[7] = 
{
	QuestManager_t588401851::get_offset_of_quests_2(),
	QuestManager_t588401851::get_offset_of_questCompleted_3(),
	QuestManager_t588401851::get_offset_of_isCompleted_4(),
	QuestManager_t588401851::get_offset_of_questID_5(),
	QuestManager_t588401851::get_offset_of_storySelected_6(),
	QuestManager_t588401851::get_offset_of_gameController_7(),
	QuestManager_t588401851_StaticFields::get_offset_of__instance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (QuestScene_t3272400377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4092[18] = 
{
	QuestScene_t3272400377::get_offset_of_manager_2(),
	QuestScene_t3272400377::get_offset_of_quest_3(),
	QuestScene_t3272400377::get_offset_of_text_4(),
	QuestScene_t3272400377::get_offset_of_options_5(),
	QuestScene_t3272400377::get_offset_of_optionsText_6(),
	QuestScene_t3272400377::get_offset_of_audioManager_7(),
	QuestScene_t3272400377::get_offset_of_idStory_8(),
	QuestScene_t3272400377::get_offset_of_buttonStartRecord_9(),
	QuestScene_t3272400377::get_offset_of_buttonStopRecord_10(),
	QuestScene_t3272400377::get_offset_of_elapsedTime_11(),
	QuestScene_t3272400377::get_offset_of_countdownTimeDefault_12(),
	QuestScene_t3272400377::get_offset_of_countdownTime_13(),
	QuestScene_t3272400377::get_offset_of_isactivityStarted_14(),
	QuestScene_t3272400377::get_offset_of_countDownText_15(),
	QuestScene_t3272400377::get_offset_of_startActivity_16(),
	QuestScene_t3272400377::get_offset_of_completeActivity_17(),
	QuestScene_t3272400377::get_offset_of_incompleteActivity_18(),
	QuestScene_t3272400377::get_offset_of_sceneChanger_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (QuestTrigger_t3125687900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4093[2] = 
{
	QuestTrigger_t3125687900::get_offset_of_manager_2(),
	QuestTrigger_t3125687900::get_offset_of_questID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (U3CDelayStartQuestU3Ec__Iterator0_t39561174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4094[4] = 
{
	U3CDelayStartQuestU3Ec__Iterator0_t39561174::get_offset_of_U24this_0(),
	U3CDelayStartQuestU3Ec__Iterator0_t39561174::get_offset_of_U24current_1(),
	U3CDelayStartQuestU3Ec__Iterator0_t39561174::get_offset_of_U24disposing_2(),
	U3CDelayStartQuestU3Ec__Iterator0_t39561174::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (ResultQuestScene_t315437199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4095[4] = 
{
	ResultQuestScene_t315437199::get_offset_of_manager_2(),
	ResultQuestScene_t315437199::get_offset_of_quest_3(),
	ResultQuestScene_t315437199::get_offset_of_text_4(),
	ResultQuestScene_t315437199::get_offset_of_sceneChanger_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { sizeof (SavWav_t2455245258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4096[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { sizeof (SceneChanger_t1033871796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4097[4] = 
{
	SceneChanger_t1033871796::get_offset_of_buttonOk_2(),
	SceneChanger_t1033871796::get_offset_of_buttonWrong_3(),
	SceneChanger_t1033871796::get_offset_of_gameController_4(),
	SceneChanger_t1033871796::get_offset_of_dataController_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (SettingsController_t1943457452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4098[1] = 
{
	SettingsController_t1943457452::get_offset_of_gameController_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (UIDisplayController_t1173465452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4099[9] = 
{
	UIDisplayController_t1173465452::get_offset_of_encodingMessagePanel_2(),
	UIDisplayController_t1173465452::get_offset_of_elapsedTimeText_3(),
	UIDisplayController_t1173465452::get_offset_of_timerText_4(),
	UIDisplayController_t1173465452::get_offset_of_stop_5(),
	UIDisplayController_t1173465452::get_offset_of_start_6(),
	UIDisplayController_t1173465452::get_offset_of_recordManager_7(),
	UIDisplayController_t1173465452::get_offset_of_sceneChanger_8(),
	UIDisplayController_t1173465452::get_offset_of_manager_9(),
	UIDisplayController_t1173465452::get_offset_of_quest_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
