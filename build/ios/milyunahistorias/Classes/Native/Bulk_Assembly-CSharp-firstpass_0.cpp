﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// NativeShare/ConfigStruct
struct ConfigStruct_t1540812723;
// NativeShare/SocialSharingStruct
struct SocialSharingStruct_t2558757706;

extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern const uint32_t NativeShare_Share_m2843852134_MetadataUsageId;
struct ConfigStruct_t1540812723_marshaled_pinvoke;
struct ConfigStruct_t1540812723;;
struct ConfigStruct_t1540812723_marshaled_pinvoke;;
struct SocialSharingStruct_t2558757706_marshaled_pinvoke;
struct SocialSharingStruct_t2558757706;;
struct SocialSharingStruct_t2558757706_marshaled_pinvoke;;
extern String_t* _stringLiteral3452614549;
extern const uint32_t NativeShare_ShareIOS_m4209002812_MetadataUsageId;

struct StringU5BU5D_t1281789340;


#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef NATIVESHARE_T4027546635_H
#define NATIVESHARE_T4027546635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare
struct  NativeShare_t4027546635  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVESHARE_T4027546635_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef CONFIGSTRUCT_T1540812723_H
#define CONFIGSTRUCT_T1540812723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/ConfigStruct
struct  ConfigStruct_t1540812723 
{
public:
	// System.String NativeShare/ConfigStruct::title
	String_t* ___title_0;
	// System.String NativeShare/ConfigStruct::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(ConfigStruct_t1540812723, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(ConfigStruct_t1540812723, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NativeShare/ConfigStruct
struct ConfigStruct_t1540812723_marshaled_pinvoke
{
	char* ___title_0;
	char* ___message_1;
};
// Native definition for COM marshalling of NativeShare/ConfigStruct
struct ConfigStruct_t1540812723_marshaled_com
{
	Il2CppChar* ___title_0;
	Il2CppChar* ___message_1;
};
#endif // CONFIGSTRUCT_T1540812723_H
#ifndef SOCIALSHARINGSTRUCT_T2558757706_H
#define SOCIALSHARINGSTRUCT_T2558757706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/SocialSharingStruct
struct  SocialSharingStruct_t2558757706 
{
public:
	// System.String NativeShare/SocialSharingStruct::text
	String_t* ___text_0;
	// System.String NativeShare/SocialSharingStruct::subject
	String_t* ___subject_1;
	// System.String NativeShare/SocialSharingStruct::filePaths
	String_t* ___filePaths_2;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t2558757706, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_subject_1() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t2558757706, ___subject_1)); }
	inline String_t* get_subject_1() const { return ___subject_1; }
	inline String_t** get_address_of_subject_1() { return &___subject_1; }
	inline void set_subject_1(String_t* value)
	{
		___subject_1 = value;
		Il2CppCodeGenWriteBarrier((&___subject_1), value);
	}

	inline static int32_t get_offset_of_filePaths_2() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t2558757706, ___filePaths_2)); }
	inline String_t* get_filePaths_2() const { return ___filePaths_2; }
	inline String_t** get_address_of_filePaths_2() { return &___filePaths_2; }
	inline void set_filePaths_2(String_t* value)
	{
		___filePaths_2 = value;
		Il2CppCodeGenWriteBarrier((&___filePaths_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NativeShare/SocialSharingStruct
struct SocialSharingStruct_t2558757706_marshaled_pinvoke
{
	char* ___text_0;
	char* ___subject_1;
	char* ___filePaths_2;
};
// Native definition for COM marshalling of NativeShare/SocialSharingStruct
struct SocialSharingStruct_t2558757706_marshaled_com
{
	Il2CppChar* ___text_0;
	Il2CppChar* ___subject_1;
	Il2CppChar* ___filePaths_2;
};
#endif // SOCIALSHARINGSTRUCT_T2558757706_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void ConfigStruct_t1540812723_marshal_pinvoke(const ConfigStruct_t1540812723& unmarshaled, ConfigStruct_t1540812723_marshaled_pinvoke& marshaled);
extern "C" void ConfigStruct_t1540812723_marshal_pinvoke_back(const ConfigStruct_t1540812723_marshaled_pinvoke& marshaled, ConfigStruct_t1540812723& unmarshaled);
extern "C" void ConfigStruct_t1540812723_marshal_pinvoke_cleanup(ConfigStruct_t1540812723_marshaled_pinvoke& marshaled);
extern "C" void SocialSharingStruct_t2558757706_marshal_pinvoke(const SocialSharingStruct_t2558757706& unmarshaled, SocialSharingStruct_t2558757706_marshaled_pinvoke& marshaled);
extern "C" void SocialSharingStruct_t2558757706_marshal_pinvoke_back(const SocialSharingStruct_t2558757706_marshaled_pinvoke& marshaled, SocialSharingStruct_t2558757706& unmarshaled);
extern "C" void SocialSharingStruct_t2558757706_marshal_pinvoke_cleanup(SocialSharingStruct_t2558757706_marshaled_pinvoke& marshaled);


// System.Void NativeShare::ShareMultiple(System.String,System.String[],System.String,System.String,System.String,System.Boolean,System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_ShareMultiple_m2670148025 (RuntimeObject * __this /* static, unused */, String_t* ___body0, StringU5BU5D_t1281789340* ___filePaths1, String_t* ___url2, String_t* ___subject3, String_t* ___mimeType4, bool ___chooser5, String_t* ___chooserText6, const RuntimeMethod* method);
// System.Void NativeShare::ShareIOS(System.String,System.String,System.String,System.String[])
extern "C" IL2CPP_METHOD_ATTR void NativeShare_ShareIOS_m4209002812 (RuntimeObject * __this /* static, unused */, String_t* ___body0, String_t* ___subject1, String_t* ___url2, StringU5BU5D_t1281789340* ___filePaths3, const RuntimeMethod* method);
// System.Void NativeShare::showAlertMessage(NativeShare/ConfigStruct&)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_showAlertMessage_m4091591288 (RuntimeObject * __this /* static, unused */, ConfigStruct_t1540812723 * ___conf0, const RuntimeMethod* method);
// System.String System.String::Join(System.String,System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Join_m2050845953 (RuntimeObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1281789340* p1, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void NativeShare::showSocialSharing(NativeShare/SocialSharingStruct&)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_showSocialSharing_m2036589503 (RuntimeObject * __this /* static, unused */, SocialSharingStruct_t2558757706 * ___conf0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeShare::Share(System.String,System.String,System.String,System.String,System.String,System.Boolean,System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_Share_m2843852134 (RuntimeObject * __this /* static, unused */, String_t* ___body0, String_t* ___filePath1, String_t* ___url2, String_t* ___subject3, String_t* ___mimeType4, bool ___chooser5, String_t* ___chooserText6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeShare_Share_m2843852134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ShareMultiple(body, new string[] { filePath }, url, subject, mimeType, chooser, chooserText);
		String_t* L_0 = ___body0;
		StringU5BU5D_t1281789340* L_1 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_2 = ___filePath1;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_2);
		String_t* L_3 = ___url2;
		String_t* L_4 = ___subject3;
		String_t* L_5 = ___mimeType4;
		bool L_6 = ___chooser5;
		String_t* L_7 = ___chooserText6;
		// ShareMultiple(body, new string[] { filePath }, url, subject, mimeType, chooser, chooserText);
		NativeShare_ShareMultiple_m2670148025(NULL /*static, unused*/, L_0, L_1, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NativeShare::ShareMultiple(System.String,System.String[],System.String,System.String,System.String,System.Boolean,System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_ShareMultiple_m2670148025 (RuntimeObject * __this /* static, unused */, String_t* ___body0, StringU5BU5D_t1281789340* ___filePaths1, String_t* ___url2, String_t* ___subject3, String_t* ___mimeType4, bool ___chooser5, String_t* ___chooserText6, const RuntimeMethod* method)
{
	{
		// ShareIOS(body, subject, url, filePaths);
		String_t* L_0 = ___body0;
		String_t* L_1 = ___subject3;
		String_t* L_2 = ___url2;
		StringU5BU5D_t1281789340* L_3 = ___filePaths1;
		// ShareIOS(body, subject, url, filePaths);
		NativeShare_ShareIOS_m4209002812(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
extern "C" void DEFAULT_CALL showAlertMessage(ConfigStruct_t1540812723_marshaled_pinvoke*);
// System.Void NativeShare::showAlertMessage(NativeShare/ConfigStruct&)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_showAlertMessage_m4091591288 (RuntimeObject * __this /* static, unused */, ConfigStruct_t1540812723 * ___conf0, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (ConfigStruct_t1540812723_marshaled_pinvoke*);

	// Marshaling of parameter '___conf0' to native representation
	ConfigStruct_t1540812723_marshaled_pinvoke* ____conf0_marshaled = NULL;
	ConfigStruct_t1540812723_marshaled_pinvoke ____conf0_marshaled_dereferenced = {};
	ConfigStruct_t1540812723_marshal_pinvoke(*___conf0, ____conf0_marshaled_dereferenced);
	____conf0_marshaled = &____conf0_marshaled_dereferenced;

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(showAlertMessage)(____conf0_marshaled);

	// Marshaling of parameter '___conf0' back from native representation
	ConfigStruct_t1540812723  _____conf0_marshaled_unmarshaled_dereferenced;
	memset(&_____conf0_marshaled_unmarshaled_dereferenced, 0, sizeof(_____conf0_marshaled_unmarshaled_dereferenced));
	ConfigStruct_t1540812723_marshal_pinvoke_back(*____conf0_marshaled, _____conf0_marshaled_unmarshaled_dereferenced);
	*___conf0 = _____conf0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___conf0' native representation
	ConfigStruct_t1540812723_marshal_pinvoke_cleanup(*____conf0_marshaled);

}
extern "C" void DEFAULT_CALL showSocialSharing(SocialSharingStruct_t2558757706_marshaled_pinvoke*);
// System.Void NativeShare::showSocialSharing(NativeShare/SocialSharingStruct&)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_showSocialSharing_m2036589503 (RuntimeObject * __this /* static, unused */, SocialSharingStruct_t2558757706 * ___conf0, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (SocialSharingStruct_t2558757706_marshaled_pinvoke*);

	// Marshaling of parameter '___conf0' to native representation
	SocialSharingStruct_t2558757706_marshaled_pinvoke* ____conf0_marshaled = NULL;
	SocialSharingStruct_t2558757706_marshaled_pinvoke ____conf0_marshaled_dereferenced = {};
	SocialSharingStruct_t2558757706_marshal_pinvoke(*___conf0, ____conf0_marshaled_dereferenced);
	____conf0_marshaled = &____conf0_marshaled_dereferenced;

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(showSocialSharing)(____conf0_marshaled);

	// Marshaling of parameter '___conf0' back from native representation
	SocialSharingStruct_t2558757706  _____conf0_marshaled_unmarshaled_dereferenced;
	memset(&_____conf0_marshaled_unmarshaled_dereferenced, 0, sizeof(_____conf0_marshaled_unmarshaled_dereferenced));
	SocialSharingStruct_t2558757706_marshal_pinvoke_back(*____conf0_marshaled, _____conf0_marshaled_unmarshaled_dereferenced);
	*___conf0 = _____conf0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___conf0' native representation
	SocialSharingStruct_t2558757706_marshal_pinvoke_cleanup(*____conf0_marshaled);

}
// System.Void NativeShare::ShareIOS(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeShare_ShareIOS_m2353303123 (RuntimeObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const RuntimeMethod* method)
{
	ConfigStruct_t1540812723  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// ConfigStruct conf = new ConfigStruct();
		il2cpp_codegen_initobj((&V_0), sizeof(ConfigStruct_t1540812723 ));
		// conf.title  = title;
		String_t* L_0 = ___title0;
		(&V_0)->set_title_0(L_0);
		// conf.message = message;
		String_t* L_1 = ___message1;
		(&V_0)->set_message_1(L_1);
		// showAlertMessage(ref conf);
		// showAlertMessage(ref conf);
		NativeShare_showAlertMessage_m4091591288(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NativeShare::ShareIOS(System.String,System.String,System.String,System.String[])
extern "C" IL2CPP_METHOD_ATTR void NativeShare_ShareIOS_m4209002812 (RuntimeObject * __this /* static, unused */, String_t* ___body0, String_t* ___subject1, String_t* ___url2, StringU5BU5D_t1281789340* ___filePaths3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeShare_ShareIOS_m4209002812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SocialSharingStruct_t2558757706  V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	{
		// SocialSharingStruct conf = new SocialSharingStruct();
		il2cpp_codegen_initobj((&V_0), sizeof(SocialSharingStruct_t2558757706 ));
		// conf.text = body;
		String_t* L_0 = ___body0;
		(&V_0)->set_text_0(L_0);
		// string paths = string.Join(";", filePaths);
		StringU5BU5D_t1281789340* L_1 = ___filePaths3;
		// string paths = string.Join(";", filePaths);
		String_t* L_2 = String_Join_m2050845953(NULL /*static, unused*/, _stringLiteral3452614549, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		// if (string.IsNullOrEmpty(paths))
		String_t* L_3 = V_1;
		// if (string.IsNullOrEmpty(paths))
		bool L_4 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		// paths = url;
		String_t* L_5 = ___url2;
		V_1 = L_5;
		goto IL_0047;
	}

IL_002f:
	{
		// else if (!string.IsNullOrEmpty(url))
		String_t* L_6 = ___url2;
		// else if (!string.IsNullOrEmpty(url))
		bool L_7 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		// paths += ";" + url;
		String_t* L_8 = V_1;
		String_t* L_9 = ___url2;
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, L_8, _stringLiteral3452614549, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_0047:
	{
		// conf.filePaths = paths;
		String_t* L_11 = V_1;
		(&V_0)->set_filePaths_2(L_11);
		// conf.subject = subject;
		String_t* L_12 = ___subject1;
		(&V_0)->set_subject_1(L_12);
		// showSocialSharing(ref conf);
		// showSocialSharing(ref conf);
		NativeShare_showSocialSharing_m2036589503(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: NativeShare/ConfigStruct
extern "C" void ConfigStruct_t1540812723_marshal_pinvoke(const ConfigStruct_t1540812723& unmarshaled, ConfigStruct_t1540812723_marshaled_pinvoke& marshaled)
{
	marshaled.___title_0 = il2cpp_codegen_marshal_string(unmarshaled.get_title_0());
	marshaled.___message_1 = il2cpp_codegen_marshal_string(unmarshaled.get_message_1());
}
extern "C" void ConfigStruct_t1540812723_marshal_pinvoke_back(const ConfigStruct_t1540812723_marshaled_pinvoke& marshaled, ConfigStruct_t1540812723& unmarshaled)
{
	unmarshaled.set_title_0(il2cpp_codegen_marshal_string_result(marshaled.___title_0));
	unmarshaled.set_message_1(il2cpp_codegen_marshal_string_result(marshaled.___message_1));
}
// Conversion method for clean up from marshalling of: NativeShare/ConfigStruct
extern "C" void ConfigStruct_t1540812723_marshal_pinvoke_cleanup(ConfigStruct_t1540812723_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___title_0);
	marshaled.___title_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___message_1);
	marshaled.___message_1 = NULL;
}
// Conversion methods for marshalling of: NativeShare/ConfigStruct
extern "C" void ConfigStruct_t1540812723_marshal_com(const ConfigStruct_t1540812723& unmarshaled, ConfigStruct_t1540812723_marshaled_com& marshaled)
{
	marshaled.___title_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_title_0());
	marshaled.___message_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_message_1());
}
extern "C" void ConfigStruct_t1540812723_marshal_com_back(const ConfigStruct_t1540812723_marshaled_com& marshaled, ConfigStruct_t1540812723& unmarshaled)
{
	unmarshaled.set_title_0(il2cpp_codegen_marshal_bstring_result(marshaled.___title_0));
	unmarshaled.set_message_1(il2cpp_codegen_marshal_bstring_result(marshaled.___message_1));
}
// Conversion method for clean up from marshalling of: NativeShare/ConfigStruct
extern "C" void ConfigStruct_t1540812723_marshal_com_cleanup(ConfigStruct_t1540812723_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___title_0);
	marshaled.___title_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___message_1);
	marshaled.___message_1 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: NativeShare/SocialSharingStruct
extern "C" void SocialSharingStruct_t2558757706_marshal_pinvoke(const SocialSharingStruct_t2558757706& unmarshaled, SocialSharingStruct_t2558757706_marshaled_pinvoke& marshaled)
{
	marshaled.___text_0 = il2cpp_codegen_marshal_string(unmarshaled.get_text_0());
	marshaled.___subject_1 = il2cpp_codegen_marshal_string(unmarshaled.get_subject_1());
	marshaled.___filePaths_2 = il2cpp_codegen_marshal_string(unmarshaled.get_filePaths_2());
}
extern "C" void SocialSharingStruct_t2558757706_marshal_pinvoke_back(const SocialSharingStruct_t2558757706_marshaled_pinvoke& marshaled, SocialSharingStruct_t2558757706& unmarshaled)
{
	unmarshaled.set_text_0(il2cpp_codegen_marshal_string_result(marshaled.___text_0));
	unmarshaled.set_subject_1(il2cpp_codegen_marshal_string_result(marshaled.___subject_1));
	unmarshaled.set_filePaths_2(il2cpp_codegen_marshal_string_result(marshaled.___filePaths_2));
}
// Conversion method for clean up from marshalling of: NativeShare/SocialSharingStruct
extern "C" void SocialSharingStruct_t2558757706_marshal_pinvoke_cleanup(SocialSharingStruct_t2558757706_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___text_0);
	marshaled.___text_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___subject_1);
	marshaled.___subject_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___filePaths_2);
	marshaled.___filePaths_2 = NULL;
}
// Conversion methods for marshalling of: NativeShare/SocialSharingStruct
extern "C" void SocialSharingStruct_t2558757706_marshal_com(const SocialSharingStruct_t2558757706& unmarshaled, SocialSharingStruct_t2558757706_marshaled_com& marshaled)
{
	marshaled.___text_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_text_0());
	marshaled.___subject_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_subject_1());
	marshaled.___filePaths_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_filePaths_2());
}
extern "C" void SocialSharingStruct_t2558757706_marshal_com_back(const SocialSharingStruct_t2558757706_marshaled_com& marshaled, SocialSharingStruct_t2558757706& unmarshaled)
{
	unmarshaled.set_text_0(il2cpp_codegen_marshal_bstring_result(marshaled.___text_0));
	unmarshaled.set_subject_1(il2cpp_codegen_marshal_bstring_result(marshaled.___subject_1));
	unmarshaled.set_filePaths_2(il2cpp_codegen_marshal_bstring_result(marshaled.___filePaths_2));
}
// Conversion method for clean up from marshalling of: NativeShare/SocialSharingStruct
extern "C" void SocialSharingStruct_t2558757706_marshal_com_cleanup(SocialSharingStruct_t2558757706_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___text_0);
	marshaled.___text_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___subject_1);
	marshaled.___subject_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___filePaths_2);
	marshaled.___filePaths_2 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
