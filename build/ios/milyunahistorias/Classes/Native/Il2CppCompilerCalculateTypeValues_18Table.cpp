﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t2521872312;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t1375878923;
// Mono.Net.Security.IMonoTlsProvider
struct IMonoTlsProvider_t2843178290;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t3666008581;
// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_t3152003291;
// System.Net.ServerCertValidationCallback
struct ServerCertValidationCallback_t1488468298;
// System.Net.Security.LocalCertSelectionCallback
struct LocalCertSelectionCallback_t1988113036;
// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct ServerCertValidationCallbackWrapper_t1850490691;
// Mono.Net.Security.MonoTlsStream
struct MonoTlsStream_t1980138907;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1669436515;
// Mono.Net.CFProxy[]
struct CFProxyU5BU5D_t3470009405;
// Mono.Net.CFRunLoop
struct CFRunLoop_t2893954052;
// System.Collections.Generic.Queue`1<Mono.Net.CFNetwork/GetProxyData>
struct Queue_1_t4214142888;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Net.ICredentials
struct ICredentials_t725721261;
// System.String
struct String_t;
// Mono.Security.X509.X509Store
struct X509Store_t2777415284;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t3399372417;
// Mono.Net.Security.Private.LegacySslStream
struct LegacySslStream_t1814096354;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t722666473;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Mono.Security.ASN1
struct ASN1_t2114160833;
// Mono.Net.Security.IMonoSslStream
struct IMonoSslStream_t2316850126;
// Mono.Security.Interface.IMonoSslStream
struct IMonoSslStream_t1819859871;
// Mono.Net.Dns.DnsHeader
struct DnsHeader_t376095124;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t596328627;
// System.Net.IPEndPoint[]
struct IPEndPointU5BU5D_t3871874695;
// System.Net.Sockets.Socket
struct Socket_t1119025450;
// System.Collections.Generic.Dictionary`2<System.Int32,Mono.Net.Dns.SimpleResolverEventArgs>
struct Dictionary_2_t897658676;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Threading.TimerCallback
struct TimerCallback_t1438585625;
// System.Security.Cryptography.CspParameters
struct CspParameters_t239852639;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// Mono.Math.BigInteger
struct BigInteger_t2902905090;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1432317219;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord>
struct ReadOnlyCollection_1_t180386464;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion>
struct ReadOnlyCollection_1_t878248125;
// Mono.Security.X509.Extensions.GeneralNames
struct GeneralNames_t2702294159;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// System.IO.Stream
struct Stream_t1273022909;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct KeyGeneratedEventHandler_t3064139578;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// Mono.Net.CFDictionary
struct CFDictionary_t2669341669;
// Mono.Security.Protocol.Tls.SslStreamBase
struct SslStreamBase_t1667413407;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t849923962;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t4071955934;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t713131622;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t194917408;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.EventArgs
struct EventArgs_t3591816995;
// System.Net.IPAddress
struct IPAddress_t241777590;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745528_H
#define U3CMODULEU3E_T692745528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745528 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745528_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T2247204716_H
#define U3CU3EC__DISPLAYCLASS5_0_T2247204716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t2247204716  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass5_0::callback
	MonoRemoteCertificateValidationCallback_t2521872312 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2247204716, ___callback_0)); }
	inline MonoRemoteCertificateValidationCallback_t2521872312 * get_callback_0() const { return ___callback_0; }
	inline MonoRemoteCertificateValidationCallback_t2521872312 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoRemoteCertificateValidationCallback_t2521872312 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T2247204716_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T3821182828_H
#define U3CU3EC__DISPLAYCLASS8_0_T3821182828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t3821182828  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass8_0::callback
	MonoLocalCertificateSelectionCallback_t1375878923 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t3821182828, ___callback_0)); }
	inline MonoLocalCertificateSelectionCallback_t1375878923 * get_callback_0() const { return ___callback_0; }
	inline MonoLocalCertificateSelectionCallback_t1375878923 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoLocalCertificateSelectionCallback_t1375878923 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T3821182828_H
#ifndef CALLBACKHELPERS_T1924962770_H
#define CALLBACKHELPERS_T1924962770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers
struct  CallbackHelpers_t1924962770  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKHELPERS_T1924962770_H
#ifndef MONOTLSPROVIDERFACTORY_T4084560240_H
#define MONOTLSPROVIDERFACTORY_T4084560240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsProviderFactory
struct  MonoTlsProviderFactory_t4084560240  : public RuntimeObject
{
public:

public:
};

struct MonoTlsProviderFactory_t4084560240_StaticFields
{
public:
	// System.Object Mono.Net.Security.MonoTlsProviderFactory::locker
	RuntimeObject * ___locker_0;
	// System.Boolean Mono.Net.Security.MonoTlsProviderFactory::initialized
	bool ___initialized_1;
	// Mono.Net.Security.IMonoTlsProvider Mono.Net.Security.MonoTlsProviderFactory::defaultProvider
	RuntimeObject* ___defaultProvider_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mono.Net.Security.MonoTlsProviderFactory::providerRegistration
	Dictionary_2_t1632706988 * ___providerRegistration_3;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_defaultProvider_2() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___defaultProvider_2)); }
	inline RuntimeObject* get_defaultProvider_2() const { return ___defaultProvider_2; }
	inline RuntimeObject** get_address_of_defaultProvider_2() { return &___defaultProvider_2; }
	inline void set_defaultProvider_2(RuntimeObject* value)
	{
		___defaultProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultProvider_2), value);
	}

	inline static int32_t get_offset_of_providerRegistration_3() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___providerRegistration_3)); }
	inline Dictionary_2_t1632706988 * get_providerRegistration_3() const { return ___providerRegistration_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_providerRegistration_3() { return &___providerRegistration_3; }
	inline void set_providerRegistration_3(Dictionary_2_t1632706988 * value)
	{
		___providerRegistration_3 = value;
		Il2CppCodeGenWriteBarrier((&___providerRegistration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDERFACTORY_T4084560240_H
#ifndef CHAINVALIDATIONHELPER_T669322361_H
#define CHAINVALIDATIONHELPER_T669322361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ChainValidationHelper
struct  ChainValidationHelper_t669322361  : public RuntimeObject
{
public:
	// System.Object Mono.Net.Security.ChainValidationHelper::sender
	RuntimeObject * ___sender_0;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.ChainValidationHelper::settings
	MonoTlsSettings_t3666008581 * ___settings_1;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.ChainValidationHelper::provider
	MonoTlsProvider_t3152003291 * ___provider_2;
	// System.Net.ServerCertValidationCallback Mono.Net.Security.ChainValidationHelper::certValidationCallback
	ServerCertValidationCallback_t1488468298 * ___certValidationCallback_3;
	// System.Net.Security.LocalCertSelectionCallback Mono.Net.Security.ChainValidationHelper::certSelectionCallback
	LocalCertSelectionCallback_t1988113036 * ___certSelectionCallback_4;
	// Mono.Net.Security.ServerCertValidationCallbackWrapper Mono.Net.Security.ChainValidationHelper::callbackWrapper
	ServerCertValidationCallbackWrapper_t1850490691 * ___callbackWrapper_5;
	// Mono.Net.Security.MonoTlsStream Mono.Net.Security.ChainValidationHelper::tlsStream
	MonoTlsStream_t1980138907 * ___tlsStream_6;
	// System.Net.HttpWebRequest Mono.Net.Security.ChainValidationHelper::request
	HttpWebRequest_t1669436515 * ___request_7;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___settings_1)); }
	inline MonoTlsSettings_t3666008581 * get_settings_1() const { return ___settings_1; }
	inline MonoTlsSettings_t3666008581 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(MonoTlsSettings_t3666008581 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}

	inline static int32_t get_offset_of_provider_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___provider_2)); }
	inline MonoTlsProvider_t3152003291 * get_provider_2() const { return ___provider_2; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_provider_2() { return &___provider_2; }
	inline void set_provider_2(MonoTlsProvider_t3152003291 * value)
	{
		___provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___provider_2), value);
	}

	inline static int32_t get_offset_of_certValidationCallback_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___certValidationCallback_3)); }
	inline ServerCertValidationCallback_t1488468298 * get_certValidationCallback_3() const { return ___certValidationCallback_3; }
	inline ServerCertValidationCallback_t1488468298 ** get_address_of_certValidationCallback_3() { return &___certValidationCallback_3; }
	inline void set_certValidationCallback_3(ServerCertValidationCallback_t1488468298 * value)
	{
		___certValidationCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___certValidationCallback_3), value);
	}

	inline static int32_t get_offset_of_certSelectionCallback_4() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___certSelectionCallback_4)); }
	inline LocalCertSelectionCallback_t1988113036 * get_certSelectionCallback_4() const { return ___certSelectionCallback_4; }
	inline LocalCertSelectionCallback_t1988113036 ** get_address_of_certSelectionCallback_4() { return &___certSelectionCallback_4; }
	inline void set_certSelectionCallback_4(LocalCertSelectionCallback_t1988113036 * value)
	{
		___certSelectionCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___certSelectionCallback_4), value);
	}

	inline static int32_t get_offset_of_callbackWrapper_5() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___callbackWrapper_5)); }
	inline ServerCertValidationCallbackWrapper_t1850490691 * get_callbackWrapper_5() const { return ___callbackWrapper_5; }
	inline ServerCertValidationCallbackWrapper_t1850490691 ** get_address_of_callbackWrapper_5() { return &___callbackWrapper_5; }
	inline void set_callbackWrapper_5(ServerCertValidationCallbackWrapper_t1850490691 * value)
	{
		___callbackWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___callbackWrapper_5), value);
	}

	inline static int32_t get_offset_of_tlsStream_6() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___tlsStream_6)); }
	inline MonoTlsStream_t1980138907 * get_tlsStream_6() const { return ___tlsStream_6; }
	inline MonoTlsStream_t1980138907 ** get_address_of_tlsStream_6() { return &___tlsStream_6; }
	inline void set_tlsStream_6(MonoTlsStream_t1980138907 * value)
	{
		___tlsStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___tlsStream_6), value);
	}

	inline static int32_t get_offset_of_request_7() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___request_7)); }
	inline HttpWebRequest_t1669436515 * get_request_7() const { return ___request_7; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_7() { return &___request_7; }
	inline void set_request_7(HttpWebRequest_t1669436515 * value)
	{
		___request_7 = value;
		Il2CppCodeGenWriteBarrier((&___request_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAINVALIDATIONHELPER_T669322361_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T2390936616_H
#define U3CU3EC__DISPLAYCLASS13_0_T2390936616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t2390936616  : public RuntimeObject
{
public:
	// Mono.Net.CFProxy[] Mono.Net.CFNetwork/<>c__DisplayClass13_0::proxies
	CFProxyU5BU5D_t3470009405* ___proxies_0;
	// Mono.Net.CFRunLoop Mono.Net.CFNetwork/<>c__DisplayClass13_0::runLoop
	CFRunLoop_t2893954052 * ___runLoop_1;

public:
	inline static int32_t get_offset_of_proxies_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2390936616, ___proxies_0)); }
	inline CFProxyU5BU5D_t3470009405* get_proxies_0() const { return ___proxies_0; }
	inline CFProxyU5BU5D_t3470009405** get_address_of_proxies_0() { return &___proxies_0; }
	inline void set_proxies_0(CFProxyU5BU5D_t3470009405* value)
	{
		___proxies_0 = value;
		Il2CppCodeGenWriteBarrier((&___proxies_0), value);
	}

	inline static int32_t get_offset_of_runLoop_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2390936616, ___runLoop_1)); }
	inline CFRunLoop_t2893954052 * get_runLoop_1() const { return ___runLoop_1; }
	inline CFRunLoop_t2893954052 ** get_address_of_runLoop_1() { return &___runLoop_1; }
	inline void set_runLoop_1(CFRunLoop_t2893954052 * value)
	{
		___runLoop_1 = value;
		Il2CppCodeGenWriteBarrier((&___runLoop_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T2390936616_H
#ifndef PRIMALITYTESTS_T1538473976_H
#define PRIMALITYTESTS_T1538473976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.PrimalityTests
struct  PrimalityTests_t1538473976  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMALITYTESTS_T1538473976_H
#ifndef PRIMEGENERATORBASE_T446028867_H
#define PRIMEGENERATORBASE_T446028867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.Generator.PrimeGeneratorBase
struct  PrimeGeneratorBase_t446028867  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMEGENERATORBASE_T446028867_H
#ifndef SR_T167583544_H
#define SR_T167583544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t167583544  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T167583544_H
#ifndef CFNETWORK_T1041854236_H
#define CFNETWORK_T1041854236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork
struct  CFNetwork_t1041854236  : public RuntimeObject
{
public:

public:
};

struct CFNetwork_t1041854236_StaticFields
{
public:
	// System.Object Mono.Net.CFNetwork::lock_obj
	RuntimeObject * ___lock_obj_0;
	// System.Collections.Generic.Queue`1<Mono.Net.CFNetwork/GetProxyData> Mono.Net.CFNetwork::get_proxy_queue
	Queue_1_t4214142888 * ___get_proxy_queue_1;
	// System.Threading.AutoResetEvent Mono.Net.CFNetwork::proxy_event
	AutoResetEvent_t1333520283 * ___proxy_event_2;

public:
	inline static int32_t get_offset_of_lock_obj_0() { return static_cast<int32_t>(offsetof(CFNetwork_t1041854236_StaticFields, ___lock_obj_0)); }
	inline RuntimeObject * get_lock_obj_0() const { return ___lock_obj_0; }
	inline RuntimeObject ** get_address_of_lock_obj_0() { return &___lock_obj_0; }
	inline void set_lock_obj_0(RuntimeObject * value)
	{
		___lock_obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___lock_obj_0), value);
	}

	inline static int32_t get_offset_of_get_proxy_queue_1() { return static_cast<int32_t>(offsetof(CFNetwork_t1041854236_StaticFields, ___get_proxy_queue_1)); }
	inline Queue_1_t4214142888 * get_get_proxy_queue_1() const { return ___get_proxy_queue_1; }
	inline Queue_1_t4214142888 ** get_address_of_get_proxy_queue_1() { return &___get_proxy_queue_1; }
	inline void set_get_proxy_queue_1(Queue_1_t4214142888 * value)
	{
		___get_proxy_queue_1 = value;
		Il2CppCodeGenWriteBarrier((&___get_proxy_queue_1), value);
	}

	inline static int32_t get_offset_of_proxy_event_2() { return static_cast<int32_t>(offsetof(CFNetwork_t1041854236_StaticFields, ___proxy_event_2)); }
	inline AutoResetEvent_t1333520283 * get_proxy_event_2() const { return ___proxy_event_2; }
	inline AutoResetEvent_t1333520283 ** get_address_of_proxy_event_2() { return &___proxy_event_2; }
	inline void set_proxy_event_2(AutoResetEvent_t1333520283 * value)
	{
		___proxy_event_2 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_event_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFNETWORK_T1041854236_H
#ifndef CFWEBPROXY_T1756538142_H
#define CFWEBPROXY_T1756538142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/CFWebProxy
struct  CFWebProxy_t1756538142  : public RuntimeObject
{
public:
	// System.Net.ICredentials Mono.Net.CFNetwork/CFWebProxy::credentials
	RuntimeObject* ___credentials_0;
	// System.Boolean Mono.Net.CFNetwork/CFWebProxy::userSpecified
	bool ___userSpecified_1;

public:
	inline static int32_t get_offset_of_credentials_0() { return static_cast<int32_t>(offsetof(CFWebProxy_t1756538142, ___credentials_0)); }
	inline RuntimeObject* get_credentials_0() const { return ___credentials_0; }
	inline RuntimeObject** get_address_of_credentials_0() { return &___credentials_0; }
	inline void set_credentials_0(RuntimeObject* value)
	{
		___credentials_0 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_0), value);
	}

	inline static int32_t get_offset_of_userSpecified_1() { return static_cast<int32_t>(offsetof(CFWebProxy_t1756538142, ___userSpecified_1)); }
	inline bool get_userSpecified_1() const { return ___userSpecified_1; }
	inline bool* get_address_of_userSpecified_1() { return &___userSpecified_1; }
	inline void set_userSpecified_1(bool value)
	{
		___userSpecified_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFWEBPROXY_T1756538142_H
#ifndef X509STORES_T1373936238_H
#define X509STORES_T1373936238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Stores
struct  X509Stores_t1373936238  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Stores::_storePath
	String_t* ____storePath_0;
	// System.Boolean Mono.Security.X509.X509Stores::_newFormat
	bool ____newFormat_1;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_trusted
	X509Store_t2777415284 * ____trusted_2;

public:
	inline static int32_t get_offset_of__storePath_0() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____storePath_0)); }
	inline String_t* get__storePath_0() const { return ____storePath_0; }
	inline String_t** get_address_of__storePath_0() { return &____storePath_0; }
	inline void set__storePath_0(String_t* value)
	{
		____storePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____storePath_0), value);
	}

	inline static int32_t get_offset_of__newFormat_1() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____newFormat_1)); }
	inline bool get__newFormat_1() const { return ____newFormat_1; }
	inline bool* get_address_of__newFormat_1() { return &____newFormat_1; }
	inline void set__newFormat_1(bool value)
	{
		____newFormat_1 = value;
	}

	inline static int32_t get_offset_of__trusted_2() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____trusted_2)); }
	inline X509Store_t2777415284 * get__trusted_2() const { return ____trusted_2; }
	inline X509Store_t2777415284 ** get_address_of__trusted_2() { return &____trusted_2; }
	inline void set__trusted_2(X509Store_t2777415284 * value)
	{
		____trusted_2 = value;
		Il2CppCodeGenWriteBarrier((&____trusted_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORES_T1373936238_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T3818911613_H
#define U3CU3EC__DISPLAYCLASS57_0_T3818911613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t3818911613  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0::clientCertificates
	X509CertificateCollection_t3399372417 * ___clientCertificates_0;
	// System.String Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0::targetHost
	String_t* ___targetHost_1;
	// Mono.Net.Security.Private.LegacySslStream Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0::<>4__this
	LegacySslStream_t1814096354 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_clientCertificates_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t3818911613, ___clientCertificates_0)); }
	inline X509CertificateCollection_t3399372417 * get_clientCertificates_0() const { return ___clientCertificates_0; }
	inline X509CertificateCollection_t3399372417 ** get_address_of_clientCertificates_0() { return &___clientCertificates_0; }
	inline void set_clientCertificates_0(X509CertificateCollection_t3399372417 * value)
	{
		___clientCertificates_0 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificates_0), value);
	}

	inline static int32_t get_offset_of_targetHost_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t3818911613, ___targetHost_1)); }
	inline String_t* get_targetHost_1() const { return ___targetHost_1; }
	inline String_t** get_address_of_targetHost_1() { return &___targetHost_1; }
	inline void set_targetHost_1(String_t* value)
	{
		___targetHost_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetHost_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t3818911613, ___U3CU3E4__this_2)); }
	inline LegacySslStream_t1814096354 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LegacySslStream_t1814096354 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LegacySslStream_t1814096354 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T3818911613_H
#ifndef ASYMMETRICALGORITHM_T932037087_H
#define ASYMMETRICALGORITHM_T932037087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricAlgorithm
struct  AsymmetricAlgorithm_t932037087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_0;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_1;

public:
	inline static int32_t get_offset_of_KeySizeValue_0() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___KeySizeValue_0)); }
	inline int32_t get_KeySizeValue_0() const { return ___KeySizeValue_0; }
	inline int32_t* get_address_of_KeySizeValue_0() { return &___KeySizeValue_0; }
	inline void set_KeySizeValue_0(int32_t value)
	{
		___KeySizeValue_0 = value;
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_1() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___LegalKeySizesValue_1)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_1() const { return ___LegalKeySizesValue_1; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_1() { return &___LegalKeySizesValue_1; }
	inline void set_LegalKeySizesValue_1(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICALGORITHM_T932037087_H
#ifndef MONOTLSPROVIDER_T3152003291_H
#define MONOTLSPROVIDER_T3152003291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsProvider
struct  MonoTlsProvider_t3152003291  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDER_T3152003291_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef HASHALGORITHM_T1432317219_H
#define HASHALGORITHM_T1432317219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t1432317219  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t4116647657* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashValue_1)); }
	inline ByteU5BU5D_t4116647657* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_t4116647657* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_1), value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T1432317219_H
#ifndef X509EXTENSION_T3173393653_H
#define X509EXTENSION_T3173393653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Extension
struct  X509Extension_t3173393653  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Extension::extnOid
	String_t* ___extnOid_0;
	// System.Boolean Mono.Security.X509.X509Extension::extnCritical
	bool ___extnCritical_1;
	// Mono.Security.ASN1 Mono.Security.X509.X509Extension::extnValue
	ASN1_t2114160833 * ___extnValue_2;

public:
	inline static int32_t get_offset_of_extnOid_0() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnOid_0)); }
	inline String_t* get_extnOid_0() const { return ___extnOid_0; }
	inline String_t** get_address_of_extnOid_0() { return &___extnOid_0; }
	inline void set_extnOid_0(String_t* value)
	{
		___extnOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___extnOid_0), value);
	}

	inline static int32_t get_offset_of_extnCritical_1() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnCritical_1)); }
	inline bool get_extnCritical_1() const { return ___extnCritical_1; }
	inline bool* get_address_of_extnCritical_1() { return &___extnCritical_1; }
	inline void set_extnCritical_1(bool value)
	{
		___extnCritical_1 = value;
	}

	inline static int32_t get_offset_of_extnValue_2() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnValue_2)); }
	inline ASN1_t2114160833 * get_extnValue_2() const { return ___extnValue_2; }
	inline ASN1_t2114160833 ** get_address_of_extnValue_2() { return &___extnValue_2; }
	inline void set_extnValue_2(ASN1_t2114160833 * value)
	{
		___extnValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___extnValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T3173393653_H
#ifndef MONOSSLSTREAMIMPL_T1585747555_H
#define MONOSSLSTREAMIMPL_T1585747555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoSslStreamImpl
struct  MonoSslStreamImpl_t1585747555  : public RuntimeObject
{
public:
	// Mono.Net.Security.IMonoSslStream Mono.Net.Security.Private.MonoSslStreamImpl::impl
	RuntimeObject* ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(MonoSslStreamImpl_t1585747555, ___impl_0)); }
	inline RuntimeObject* get_impl_0() const { return ___impl_0; }
	inline RuntimeObject** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(RuntimeObject* value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLSTREAMIMPL_T1585747555_H
#ifndef MONOSSLSTREAMWRAPPER_T1708650488_H
#define MONOSSLSTREAMWRAPPER_T1708650488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoSslStreamWrapper
struct  MonoSslStreamWrapper_t1708650488  : public RuntimeObject
{
public:
	// Mono.Security.Interface.IMonoSslStream Mono.Net.Security.Private.MonoSslStreamWrapper::impl
	RuntimeObject* ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(MonoSslStreamWrapper_t1708650488, ___impl_0)); }
	inline RuntimeObject* get_impl_0() const { return ___impl_0; }
	inline RuntimeObject** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(RuntimeObject* value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLSTREAMWRAPPER_T1708650488_H
#ifndef MONOTLSPROVIDERWRAPPER_T803812000_H
#define MONOTLSPROVIDERWRAPPER_T803812000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoTlsProviderWrapper
struct  MonoTlsProviderWrapper_t803812000  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.Private.MonoTlsProviderWrapper::provider
	MonoTlsProvider_t3152003291 * ___provider_0;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderWrapper_t803812000, ___provider_0)); }
	inline MonoTlsProvider_t3152003291 * get_provider_0() const { return ___provider_0; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(MonoTlsProvider_t3152003291 * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___provider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDERWRAPPER_T803812000_H
#ifndef DNSPACKET_T1927600022_H
#define DNSPACKET_T1927600022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsPacket
struct  DnsPacket_t1927600022  : public RuntimeObject
{
public:
	// System.Byte[] Mono.Net.Dns.DnsPacket::packet
	ByteU5BU5D_t4116647657* ___packet_0;
	// System.Int32 Mono.Net.Dns.DnsPacket::position
	int32_t ___position_1;
	// Mono.Net.Dns.DnsHeader Mono.Net.Dns.DnsPacket::header
	DnsHeader_t376095124 * ___header_2;

public:
	inline static int32_t get_offset_of_packet_0() { return static_cast<int32_t>(offsetof(DnsPacket_t1927600022, ___packet_0)); }
	inline ByteU5BU5D_t4116647657* get_packet_0() const { return ___packet_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_packet_0() { return &___packet_0; }
	inline void set_packet_0(ByteU5BU5D_t4116647657* value)
	{
		___packet_0 = value;
		Il2CppCodeGenWriteBarrier((&___packet_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(DnsPacket_t1927600022, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_header_2() { return static_cast<int32_t>(offsetof(DnsPacket_t1927600022, ___header_2)); }
	inline DnsHeader_t376095124 * get_header_2() const { return ___header_2; }
	inline DnsHeader_t376095124 ** get_address_of_header_2() { return &___header_2; }
	inline void set_header_2(DnsHeader_t376095124 * value)
	{
		___header_2 = value;
		Il2CppCodeGenWriteBarrier((&___header_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSPACKET_T1927600022_H
#ifndef DNSUTIL_T2851216272_H
#define DNSUTIL_T2851216272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsUtil
struct  DnsUtil_t2851216272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSUTIL_T2851216272_H
#ifndef SIMPLERESOLVER_T688826539_H
#define SIMPLERESOLVER_T688826539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.SimpleResolver
struct  SimpleResolver_t688826539  : public RuntimeObject
{
public:
	// System.Net.IPEndPoint[] Mono.Net.Dns.SimpleResolver::endpoints
	IPEndPointU5BU5D_t3871874695* ___endpoints_2;
	// System.Net.Sockets.Socket Mono.Net.Dns.SimpleResolver::client
	Socket_t1119025450 * ___client_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,Mono.Net.Dns.SimpleResolverEventArgs> Mono.Net.Dns.SimpleResolver::queries
	Dictionary_2_t897658676 * ___queries_4;
	// System.AsyncCallback Mono.Net.Dns.SimpleResolver::receive_cb
	AsyncCallback_t3962456242 * ___receive_cb_5;
	// System.Threading.TimerCallback Mono.Net.Dns.SimpleResolver::timeout_cb
	TimerCallback_t1438585625 * ___timeout_cb_6;
	// System.Boolean Mono.Net.Dns.SimpleResolver::disposed
	bool ___disposed_7;

public:
	inline static int32_t get_offset_of_endpoints_2() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539, ___endpoints_2)); }
	inline IPEndPointU5BU5D_t3871874695* get_endpoints_2() const { return ___endpoints_2; }
	inline IPEndPointU5BU5D_t3871874695** get_address_of_endpoints_2() { return &___endpoints_2; }
	inline void set_endpoints_2(IPEndPointU5BU5D_t3871874695* value)
	{
		___endpoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___endpoints_2), value);
	}

	inline static int32_t get_offset_of_client_3() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539, ___client_3)); }
	inline Socket_t1119025450 * get_client_3() const { return ___client_3; }
	inline Socket_t1119025450 ** get_address_of_client_3() { return &___client_3; }
	inline void set_client_3(Socket_t1119025450 * value)
	{
		___client_3 = value;
		Il2CppCodeGenWriteBarrier((&___client_3), value);
	}

	inline static int32_t get_offset_of_queries_4() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539, ___queries_4)); }
	inline Dictionary_2_t897658676 * get_queries_4() const { return ___queries_4; }
	inline Dictionary_2_t897658676 ** get_address_of_queries_4() { return &___queries_4; }
	inline void set_queries_4(Dictionary_2_t897658676 * value)
	{
		___queries_4 = value;
		Il2CppCodeGenWriteBarrier((&___queries_4), value);
	}

	inline static int32_t get_offset_of_receive_cb_5() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539, ___receive_cb_5)); }
	inline AsyncCallback_t3962456242 * get_receive_cb_5() const { return ___receive_cb_5; }
	inline AsyncCallback_t3962456242 ** get_address_of_receive_cb_5() { return &___receive_cb_5; }
	inline void set_receive_cb_5(AsyncCallback_t3962456242 * value)
	{
		___receive_cb_5 = value;
		Il2CppCodeGenWriteBarrier((&___receive_cb_5), value);
	}

	inline static int32_t get_offset_of_timeout_cb_6() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539, ___timeout_cb_6)); }
	inline TimerCallback_t1438585625 * get_timeout_cb_6() const { return ___timeout_cb_6; }
	inline TimerCallback_t1438585625 ** get_address_of_timeout_cb_6() { return &___timeout_cb_6; }
	inline void set_timeout_cb_6(TimerCallback_t1438585625 * value)
	{
		___timeout_cb_6 = value;
		Il2CppCodeGenWriteBarrier((&___timeout_cb_6), value);
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}
};

struct SimpleResolver_t688826539_StaticFields
{
public:
	// System.String[] Mono.Net.Dns.SimpleResolver::EmptyStrings
	StringU5BU5D_t1281789340* ___EmptyStrings_0;
	// System.Net.IPAddress[] Mono.Net.Dns.SimpleResolver::EmptyAddresses
	IPAddressU5BU5D_t596328627* ___EmptyAddresses_1;

public:
	inline static int32_t get_offset_of_EmptyStrings_0() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539_StaticFields, ___EmptyStrings_0)); }
	inline StringU5BU5D_t1281789340* get_EmptyStrings_0() const { return ___EmptyStrings_0; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyStrings_0() { return &___EmptyStrings_0; }
	inline void set_EmptyStrings_0(StringU5BU5D_t1281789340* value)
	{
		___EmptyStrings_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyStrings_0), value);
	}

	inline static int32_t get_offset_of_EmptyAddresses_1() { return static_cast<int32_t>(offsetof(SimpleResolver_t688826539_StaticFields, ___EmptyAddresses_1)); }
	inline IPAddressU5BU5D_t596328627* get_EmptyAddresses_1() const { return ___EmptyAddresses_1; }
	inline IPAddressU5BU5D_t596328627** get_address_of_EmptyAddresses_1() { return &___EmptyAddresses_1; }
	inline void set_EmptyAddresses_1(IPAddressU5BU5D_t596328627* value)
	{
		___EmptyAddresses_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAddresses_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLERESOLVER_T688826539_H
#ifndef KEYPAIRPERSISTENCE_T2094547462_H
#define KEYPAIRPERSISTENCE_T2094547462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyPairPersistence
struct  KeyPairPersistence_t2094547462  : public RuntimeObject
{
public:
	// System.Security.Cryptography.CspParameters Mono.Security.Cryptography.KeyPairPersistence::_params
	CspParameters_t239852639 * ____params_4;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_keyvalue
	String_t* ____keyvalue_5;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_filename
	String_t* ____filename_6;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_container
	String_t* ____container_7;

public:
	inline static int32_t get_offset_of__params_4() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____params_4)); }
	inline CspParameters_t239852639 * get__params_4() const { return ____params_4; }
	inline CspParameters_t239852639 ** get_address_of__params_4() { return &____params_4; }
	inline void set__params_4(CspParameters_t239852639 * value)
	{
		____params_4 = value;
		Il2CppCodeGenWriteBarrier((&____params_4), value);
	}

	inline static int32_t get_offset_of__keyvalue_5() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____keyvalue_5)); }
	inline String_t* get__keyvalue_5() const { return ____keyvalue_5; }
	inline String_t** get_address_of__keyvalue_5() { return &____keyvalue_5; }
	inline void set__keyvalue_5(String_t* value)
	{
		____keyvalue_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyvalue_5), value);
	}

	inline static int32_t get_offset_of__filename_6() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____filename_6)); }
	inline String_t* get__filename_6() const { return ____filename_6; }
	inline String_t** get_address_of__filename_6() { return &____filename_6; }
	inline void set__filename_6(String_t* value)
	{
		____filename_6 = value;
		Il2CppCodeGenWriteBarrier((&____filename_6), value);
	}

	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____container_7)); }
	inline String_t* get__container_7() const { return ____container_7; }
	inline String_t** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(String_t* value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}
};

struct KeyPairPersistence_t2094547462_StaticFields
{
public:
	// System.Boolean Mono.Security.Cryptography.KeyPairPersistence::_userPathExists
	bool ____userPathExists_0;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_userPath
	String_t* ____userPath_1;
	// System.Boolean Mono.Security.Cryptography.KeyPairPersistence::_machinePathExists
	bool ____machinePathExists_2;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_machinePath
	String_t* ____machinePath_3;
	// System.Object Mono.Security.Cryptography.KeyPairPersistence::lockobj
	RuntimeObject * ___lockobj_8;

public:
	inline static int32_t get_offset_of__userPathExists_0() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____userPathExists_0)); }
	inline bool get__userPathExists_0() const { return ____userPathExists_0; }
	inline bool* get_address_of__userPathExists_0() { return &____userPathExists_0; }
	inline void set__userPathExists_0(bool value)
	{
		____userPathExists_0 = value;
	}

	inline static int32_t get_offset_of__userPath_1() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____userPath_1)); }
	inline String_t* get__userPath_1() const { return ____userPath_1; }
	inline String_t** get_address_of__userPath_1() { return &____userPath_1; }
	inline void set__userPath_1(String_t* value)
	{
		____userPath_1 = value;
		Il2CppCodeGenWriteBarrier((&____userPath_1), value);
	}

	inline static int32_t get_offset_of__machinePathExists_2() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____machinePathExists_2)); }
	inline bool get__machinePathExists_2() const { return ____machinePathExists_2; }
	inline bool* get_address_of__machinePathExists_2() { return &____machinePathExists_2; }
	inline void set__machinePathExists_2(bool value)
	{
		____machinePathExists_2 = value;
	}

	inline static int32_t get_offset_of__machinePath_3() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____machinePath_3)); }
	inline String_t* get__machinePath_3() const { return ____machinePath_3; }
	inline String_t** get_address_of__machinePath_3() { return &____machinePath_3; }
	inline void set__machinePath_3(String_t* value)
	{
		____machinePath_3 = value;
		Il2CppCodeGenWriteBarrier((&____machinePath_3), value);
	}

	inline static int32_t get_offset_of_lockobj_8() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ___lockobj_8)); }
	inline RuntimeObject * get_lockobj_8() const { return ___lockobj_8; }
	inline RuntimeObject ** get_address_of_lockobj_8() { return &___lockobj_8; }
	inline void set_lockobj_8(RuntimeObject * value)
	{
		___lockobj_8 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYPAIRPERSISTENCE_T2094547462_H
#ifndef BIGINTEGER_T2902905090_H
#define BIGINTEGER_T2902905090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger
struct  BigInteger_t2902905090  : public RuntimeObject
{
public:
	// System.UInt32 Mono.Math.BigInteger::length
	uint32_t ___length_0;
	// System.UInt32[] Mono.Math.BigInteger::data
	UInt32U5BU5D_t2770800703* ___data_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090, ___length_0)); }
	inline uint32_t get_length_0() const { return ___length_0; }
	inline uint32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(uint32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090, ___data_1)); }
	inline UInt32U5BU5D_t2770800703* get_data_1() const { return ___data_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(UInt32U5BU5D_t2770800703* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

struct BigInteger_t2902905090_StaticFields
{
public:
	// System.UInt32[] Mono.Math.BigInteger::smallPrimes
	UInt32U5BU5D_t2770800703* ___smallPrimes_2;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Math.BigInteger::rng
	RandomNumberGenerator_t386037858 * ___rng_3;

public:
	inline static int32_t get_offset_of_smallPrimes_2() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090_StaticFields, ___smallPrimes_2)); }
	inline UInt32U5BU5D_t2770800703* get_smallPrimes_2() const { return ___smallPrimes_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of_smallPrimes_2() { return &___smallPrimes_2; }
	inline void set_smallPrimes_2(UInt32U5BU5D_t2770800703* value)
	{
		___smallPrimes_2 = value;
		Il2CppCodeGenWriteBarrier((&___smallPrimes_2), value);
	}

	inline static int32_t get_offset_of_rng_3() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090_StaticFields, ___rng_3)); }
	inline RandomNumberGenerator_t386037858 * get_rng_3() const { return ___rng_3; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_3() { return &___rng_3; }
	inline void set_rng_3(RandomNumberGenerator_t386037858 * value)
	{
		___rng_3 = value;
		Il2CppCodeGenWriteBarrier((&___rng_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T2902905090_H
#ifndef ENCRYPTEDPRIVATEKEYINFO_T862116836_H
#define ENCRYPTEDPRIVATEKEYINFO_T862116836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
struct  EncryptedPrivateKeyInfo_t862116836  : public RuntimeObject
{
public:
	// System.String Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_algorithm
	String_t* ____algorithm_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_salt
	ByteU5BU5D_t4116647657* ____salt_1;
	// System.Int32 Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_iterations
	int32_t ____iterations_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_data
	ByteU5BU5D_t4116647657* ____data_3;

public:
	inline static int32_t get_offset_of__algorithm_0() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____algorithm_0)); }
	inline String_t* get__algorithm_0() const { return ____algorithm_0; }
	inline String_t** get_address_of__algorithm_0() { return &____algorithm_0; }
	inline void set__algorithm_0(String_t* value)
	{
		____algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_0), value);
	}

	inline static int32_t get_offset_of__salt_1() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____salt_1)); }
	inline ByteU5BU5D_t4116647657* get__salt_1() const { return ____salt_1; }
	inline ByteU5BU5D_t4116647657** get_address_of__salt_1() { return &____salt_1; }
	inline void set__salt_1(ByteU5BU5D_t4116647657* value)
	{
		____salt_1 = value;
		Il2CppCodeGenWriteBarrier((&____salt_1), value);
	}

	inline static int32_t get_offset_of__iterations_2() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____iterations_2)); }
	inline int32_t get__iterations_2() const { return ____iterations_2; }
	inline int32_t* get_address_of__iterations_2() { return &____iterations_2; }
	inline void set__iterations_2(int32_t value)
	{
		____iterations_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____data_3)); }
	inline ByteU5BU5D_t4116647657* get__data_3() const { return ____data_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(ByteU5BU5D_t4116647657* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDPRIVATEKEYINFO_T862116836_H
#ifndef PRIVATEKEYINFO_T668027993_H
#define PRIVATEKEYINFO_T668027993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct  PrivateKeyInfo_t668027993  : public RuntimeObject
{
public:
	// System.Int32 Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_version
	int32_t ____version_0;
	// System.String Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_algorithm
	String_t* ____algorithm_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_key
	ByteU5BU5D_t4116647657* ____key_2;
	// System.Collections.ArrayList Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_list
	ArrayList_t2718874744 * ____list_3;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____version_0)); }
	inline int32_t get__version_0() const { return ____version_0; }
	inline int32_t* get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(int32_t value)
	{
		____version_0 = value;
	}

	inline static int32_t get_offset_of__algorithm_1() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____algorithm_1)); }
	inline String_t* get__algorithm_1() const { return ____algorithm_1; }
	inline String_t** get_address_of__algorithm_1() { return &____algorithm_1; }
	inline void set__algorithm_1(String_t* value)
	{
		____algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_1), value);
	}

	inline static int32_t get_offset_of__key_2() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____key_2)); }
	inline ByteU5BU5D_t4116647657* get__key_2() const { return ____key_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__key_2() { return &____key_2; }
	inline void set__key_2(ByteU5BU5D_t4116647657* value)
	{
		____key_2 = value;
		Il2CppCodeGenWriteBarrier((&____key_2), value);
	}

	inline static int32_t get_offset_of__list_3() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____list_3)); }
	inline ArrayList_t2718874744 * get__list_3() const { return ____list_3; }
	inline ArrayList_t2718874744 ** get_address_of__list_3() { return &____list_3; }
	inline void set__list_3(ArrayList_t2718874744 * value)
	{
		____list_3 = value;
		Il2CppCodeGenWriteBarrier((&____list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYINFO_T668027993_H
#ifndef PKCS8_T696280613_H
#define PKCS8_T696280613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8
struct  PKCS8_t696280613  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8_T696280613_H
#ifndef PKCS1_T1505584677_H
#define PKCS1_T1505584677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS1
struct  PKCS1_t1505584677  : public RuntimeObject
{
public:

public:
};

struct PKCS1_t1505584677_StaticFields
{
public:
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA1
	ByteU5BU5D_t4116647657* ___emptySHA1_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA256
	ByteU5BU5D_t4116647657* ___emptySHA256_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA384
	ByteU5BU5D_t4116647657* ___emptySHA384_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA512
	ByteU5BU5D_t4116647657* ___emptySHA512_3;

public:
	inline static int32_t get_offset_of_emptySHA1_0() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA1_0)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA1_0() const { return ___emptySHA1_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA1_0() { return &___emptySHA1_0; }
	inline void set_emptySHA1_0(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA1_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA1_0), value);
	}

	inline static int32_t get_offset_of_emptySHA256_1() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA256_1)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA256_1() const { return ___emptySHA256_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA256_1() { return &___emptySHA256_1; }
	inline void set_emptySHA256_1(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA256_1 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA256_1), value);
	}

	inline static int32_t get_offset_of_emptySHA384_2() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA384_2)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA384_2() const { return ___emptySHA384_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA384_2() { return &___emptySHA384_2; }
	inline void set_emptySHA384_2(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA384_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA384_2), value);
	}

	inline static int32_t get_offset_of_emptySHA512_3() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA512_3)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA512_3() const { return ___emptySHA512_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA512_3() { return &___emptySHA512_3; }
	inline void set_emptySHA512_3(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA512_3 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA512_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS1_T1505584677_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	ServerIdentity_t2342208608 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	ServerIdentity_t2342208608 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef KEYBUILDER_T2049230355_H
#define KEYBUILDER_T2049230355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t2049230355  : public RuntimeObject
{
public:

public:
};

struct KeyBuilder_t2049230355_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t386037858 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(KeyBuilder_t2049230355_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t386037858 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t386037858 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUILDER_T2049230355_H
#ifndef CRYPTOCONVERT_T610933157_H
#define CRYPTOCONVERT_T610933157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.CryptoConvert
struct  CryptoConvert_t610933157  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOCONVERT_T610933157_H
#ifndef GENERALNAMES_T2702294159_H
#define GENERALNAMES_T2702294159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.GeneralNames
struct  GeneralNames_t2702294159  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::rfc822Name
	ArrayList_t2718874744 * ___rfc822Name_0;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::dnsName
	ArrayList_t2718874744 * ___dnsName_1;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::directoryNames
	ArrayList_t2718874744 * ___directoryNames_2;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::uris
	ArrayList_t2718874744 * ___uris_3;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::ipAddr
	ArrayList_t2718874744 * ___ipAddr_4;

public:
	inline static int32_t get_offset_of_rfc822Name_0() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___rfc822Name_0)); }
	inline ArrayList_t2718874744 * get_rfc822Name_0() const { return ___rfc822Name_0; }
	inline ArrayList_t2718874744 ** get_address_of_rfc822Name_0() { return &___rfc822Name_0; }
	inline void set_rfc822Name_0(ArrayList_t2718874744 * value)
	{
		___rfc822Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___rfc822Name_0), value);
	}

	inline static int32_t get_offset_of_dnsName_1() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___dnsName_1)); }
	inline ArrayList_t2718874744 * get_dnsName_1() const { return ___dnsName_1; }
	inline ArrayList_t2718874744 ** get_address_of_dnsName_1() { return &___dnsName_1; }
	inline void set_dnsName_1(ArrayList_t2718874744 * value)
	{
		___dnsName_1 = value;
		Il2CppCodeGenWriteBarrier((&___dnsName_1), value);
	}

	inline static int32_t get_offset_of_directoryNames_2() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___directoryNames_2)); }
	inline ArrayList_t2718874744 * get_directoryNames_2() const { return ___directoryNames_2; }
	inline ArrayList_t2718874744 ** get_address_of_directoryNames_2() { return &___directoryNames_2; }
	inline void set_directoryNames_2(ArrayList_t2718874744 * value)
	{
		___directoryNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___directoryNames_2), value);
	}

	inline static int32_t get_offset_of_uris_3() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___uris_3)); }
	inline ArrayList_t2718874744 * get_uris_3() const { return ___uris_3; }
	inline ArrayList_t2718874744 ** get_address_of_uris_3() { return &___uris_3; }
	inline void set_uris_3(ArrayList_t2718874744 * value)
	{
		___uris_3 = value;
		Il2CppCodeGenWriteBarrier((&___uris_3), value);
	}

	inline static int32_t get_offset_of_ipAddr_4() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___ipAddr_4)); }
	inline ArrayList_t2718874744 * get_ipAddr_4() const { return ___ipAddr_4; }
	inline ArrayList_t2718874744 ** get_address_of_ipAddr_4() { return &___ipAddr_4; }
	inline void set_ipAddr_4(ArrayList_t2718874744 * value)
	{
		___ipAddr_4 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALNAMES_T2702294159_H
#ifndef KERNEL_T1402667220_H
#define KERNEL_T1402667220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/Kernel
struct  Kernel_t1402667220  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNEL_T1402667220_H
#ifndef MODULUSRING_T596511505_H
#define MODULUSRING_T596511505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/ModulusRing
struct  ModulusRing_t596511505  : public RuntimeObject
{
public:
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::mod
	BigInteger_t2902905090 * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::constant
	BigInteger_t2902905090 * ___constant_1;

public:
	inline static int32_t get_offset_of_mod_0() { return static_cast<int32_t>(offsetof(ModulusRing_t596511505, ___mod_0)); }
	inline BigInteger_t2902905090 * get_mod_0() const { return ___mod_0; }
	inline BigInteger_t2902905090 ** get_address_of_mod_0() { return &___mod_0; }
	inline void set_mod_0(BigInteger_t2902905090 * value)
	{
		___mod_0 = value;
		Il2CppCodeGenWriteBarrier((&___mod_0), value);
	}

	inline static int32_t get_offset_of_constant_1() { return static_cast<int32_t>(offsetof(ModulusRing_t596511505, ___constant_1)); }
	inline BigInteger_t2902905090 * get_constant_1() const { return ___constant_1; }
	inline BigInteger_t2902905090 ** get_address_of_constant_1() { return &___constant_1; }
	inline void set_constant_1(BigInteger_t2902905090 * value)
	{
		___constant_1 = value;
		Il2CppCodeGenWriteBarrier((&___constant_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULUSRING_T596511505_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T1548391512_H
#define __STATICARRAYINITTYPESIZEU3D20_T1548391512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t1548391512 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t1548391512__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T1548391512_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef RSA_T2385438082_H
#define RSA_T2385438082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSA
struct  RSA_t2385438082  : public AsymmetricAlgorithm_t932037087
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSA_T2385438082_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef KEYEDHASHALGORITHM_T112861511_H
#define KEYEDHASHALGORITHM_T112861511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t112861511  : public HashAlgorithm_t1432317219
{
public:
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_4;

public:
	inline static int32_t get_offset_of_KeyValue_4() { return static_cast<int32_t>(offsetof(KeyedHashAlgorithm_t112861511, ___KeyValue_4)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_4() const { return ___KeyValue_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_4() { return &___KeyValue_4; }
	inline void set_KeyValue_4(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDHASHALGORITHM_T112861511_H
#ifndef EXTENDEDKEYUSAGEEXTENSION_T3929363080_H
#define EXTENDEDKEYUSAGEEXTENSION_T3929363080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
struct  ExtendedKeyUsageExtension_t3929363080  : public X509Extension_t3173393653
{
public:
	// System.Collections.ArrayList Mono.Security.X509.Extensions.ExtendedKeyUsageExtension::keyPurpose
	ArrayList_t2718874744 * ___keyPurpose_3;

public:
	inline static int32_t get_offset_of_keyPurpose_3() { return static_cast<int32_t>(offsetof(ExtendedKeyUsageExtension_t3929363080, ___keyPurpose_3)); }
	inline ArrayList_t2718874744 * get_keyPurpose_3() const { return ___keyPurpose_3; }
	inline ArrayList_t2718874744 ** get_address_of_keyPurpose_3() { return &___keyPurpose_3; }
	inline void set_keyPurpose_3(ArrayList_t2718874744 * value)
	{
		___keyPurpose_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyPurpose_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDKEYUSAGEEXTENSION_T3929363080_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MD5SHA1_T723838944_H
#define MD5SHA1_T723838944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD5SHA1
struct  MD5SHA1_t723838944  : public HashAlgorithm_t1432317219
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::md5
	HashAlgorithm_t1432317219 * ___md5_4;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::sha
	HashAlgorithm_t1432317219 * ___sha_5;
	// System.Boolean Mono.Security.Cryptography.MD5SHA1::hashing
	bool ___hashing_6;

public:
	inline static int32_t get_offset_of_md5_4() { return static_cast<int32_t>(offsetof(MD5SHA1_t723838944, ___md5_4)); }
	inline HashAlgorithm_t1432317219 * get_md5_4() const { return ___md5_4; }
	inline HashAlgorithm_t1432317219 ** get_address_of_md5_4() { return &___md5_4; }
	inline void set_md5_4(HashAlgorithm_t1432317219 * value)
	{
		___md5_4 = value;
		Il2CppCodeGenWriteBarrier((&___md5_4), value);
	}

	inline static int32_t get_offset_of_sha_5() { return static_cast<int32_t>(offsetof(MD5SHA1_t723838944, ___sha_5)); }
	inline HashAlgorithm_t1432317219 * get_sha_5() const { return ___sha_5; }
	inline HashAlgorithm_t1432317219 ** get_address_of_sha_5() { return &___sha_5; }
	inline void set_sha_5(HashAlgorithm_t1432317219 * value)
	{
		___sha_5 = value;
		Il2CppCodeGenWriteBarrier((&___sha_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(MD5SHA1_t723838944, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5SHA1_T723838944_H
#ifndef KEYUSAGEEXTENSION_T1795615912_H
#define KEYUSAGEEXTENSION_T1795615912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsageExtension
struct  KeyUsageExtension_t1795615912  : public X509Extension_t3173393653
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsageExtension::kubits
	int32_t ___kubits_3;

public:
	inline static int32_t get_offset_of_kubits_3() { return static_cast<int32_t>(offsetof(KeyUsageExtension_t1795615912, ___kubits_3)); }
	inline int32_t get_kubits_3() const { return ___kubits_3; }
	inline int32_t* get_address_of_kubits_3() { return &___kubits_3; }
	inline void set_kubits_3(int32_t value)
	{
		___kubits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGEEXTENSION_T1795615912_H
#ifndef DNSQUERY_T4288736869_H
#define DNSQUERY_T4288736869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQuery
struct  DnsQuery_t4288736869  : public DnsPacket_t1927600022
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQUERY_T4288736869_H
#ifndef DNSRESPONSE_T3976690788_H
#define DNSRESPONSE_T3976690788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResponse
struct  DnsResponse_t3976690788  : public DnsPacket_t1927600022
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion> Mono.Net.Dns.DnsResponse::question
	ReadOnlyCollection_1_t878248125 * ___question_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::answer
	ReadOnlyCollection_1_t180386464 * ___answer_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::authority
	ReadOnlyCollection_1_t180386464 * ___authority_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::additional
	ReadOnlyCollection_1_t180386464 * ___additional_8;
	// System.Int32 Mono.Net.Dns.DnsResponse::offset
	int32_t ___offset_9;

public:
	inline static int32_t get_offset_of_question_5() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788, ___question_5)); }
	inline ReadOnlyCollection_1_t878248125 * get_question_5() const { return ___question_5; }
	inline ReadOnlyCollection_1_t878248125 ** get_address_of_question_5() { return &___question_5; }
	inline void set_question_5(ReadOnlyCollection_1_t878248125 * value)
	{
		___question_5 = value;
		Il2CppCodeGenWriteBarrier((&___question_5), value);
	}

	inline static int32_t get_offset_of_answer_6() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788, ___answer_6)); }
	inline ReadOnlyCollection_1_t180386464 * get_answer_6() const { return ___answer_6; }
	inline ReadOnlyCollection_1_t180386464 ** get_address_of_answer_6() { return &___answer_6; }
	inline void set_answer_6(ReadOnlyCollection_1_t180386464 * value)
	{
		___answer_6 = value;
		Il2CppCodeGenWriteBarrier((&___answer_6), value);
	}

	inline static int32_t get_offset_of_authority_7() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788, ___authority_7)); }
	inline ReadOnlyCollection_1_t180386464 * get_authority_7() const { return ___authority_7; }
	inline ReadOnlyCollection_1_t180386464 ** get_address_of_authority_7() { return &___authority_7; }
	inline void set_authority_7(ReadOnlyCollection_1_t180386464 * value)
	{
		___authority_7 = value;
		Il2CppCodeGenWriteBarrier((&___authority_7), value);
	}

	inline static int32_t get_offset_of_additional_8() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788, ___additional_8)); }
	inline ReadOnlyCollection_1_t180386464 * get_additional_8() const { return ___additional_8; }
	inline ReadOnlyCollection_1_t180386464 ** get_address_of_additional_8() { return &___additional_8; }
	inline void set_additional_8(ReadOnlyCollection_1_t180386464 * value)
	{
		___additional_8 = value;
		Il2CppCodeGenWriteBarrier((&___additional_8), value);
	}

	inline static int32_t get_offset_of_offset_9() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788, ___offset_9)); }
	inline int32_t get_offset_9() const { return ___offset_9; }
	inline int32_t* get_address_of_offset_9() { return &___offset_9; }
	inline void set_offset_9(int32_t value)
	{
		___offset_9 = value;
	}
};

struct DnsResponse_t3976690788_StaticFields
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::EmptyRR
	ReadOnlyCollection_1_t180386464 * ___EmptyRR_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion> Mono.Net.Dns.DnsResponse::EmptyQS
	ReadOnlyCollection_1_t878248125 * ___EmptyQS_4;

public:
	inline static int32_t get_offset_of_EmptyRR_3() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788_StaticFields, ___EmptyRR_3)); }
	inline ReadOnlyCollection_1_t180386464 * get_EmptyRR_3() const { return ___EmptyRR_3; }
	inline ReadOnlyCollection_1_t180386464 ** get_address_of_EmptyRR_3() { return &___EmptyRR_3; }
	inline void set_EmptyRR_3(ReadOnlyCollection_1_t180386464 * value)
	{
		___EmptyRR_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyRR_3), value);
	}

	inline static int32_t get_offset_of_EmptyQS_4() { return static_cast<int32_t>(offsetof(DnsResponse_t3976690788_StaticFields, ___EmptyQS_4)); }
	inline ReadOnlyCollection_1_t878248125 * get_EmptyQS_4() const { return ___EmptyQS_4; }
	inline ReadOnlyCollection_1_t878248125 ** get_address_of_EmptyQS_4() { return &___EmptyQS_4; }
	inline void set_EmptyQS_4(ReadOnlyCollection_1_t878248125 * value)
	{
		___EmptyQS_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyQS_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESPONSE_T3976690788_H
#ifndef SEQUENTIALSEARCHPRIMEGENERATORBASE_T2996090509_H
#define SEQUENTIALSEARCHPRIMEGENERATORBASE_T2996090509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
struct  SequentialSearchPrimeGeneratorBase_t2996090509  : public PrimeGeneratorBase_t446028867
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENTIALSEARCHPRIMEGENERATORBASE_T2996090509_H
#ifndef SUBJECTALTNAMEEXTENSION_T1536937677_H
#define SUBJECTALTNAMEEXTENSION_T1536937677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.SubjectAltNameExtension
struct  SubjectAltNameExtension_t1536937677  : public X509Extension_t3173393653
{
public:
	// Mono.Security.X509.Extensions.GeneralNames Mono.Security.X509.Extensions.SubjectAltNameExtension::_names
	GeneralNames_t2702294159 * ____names_3;

public:
	inline static int32_t get_offset_of__names_3() { return static_cast<int32_t>(offsetof(SubjectAltNameExtension_t1536937677, ____names_3)); }
	inline GeneralNames_t2702294159 * get__names_3() const { return ____names_3; }
	inline GeneralNames_t2702294159 ** get_address_of__names_3() { return &____names_3; }
	inline void set__names_3(GeneralNames_t2702294159 * value)
	{
		____names_3 = value;
		Il2CppCodeGenWriteBarrier((&____names_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTALTNAMEEXTENSION_T1536937677_H
#ifndef __STATICARRAYINITTYPESIZEU3D3_T3217885683_H
#define __STATICARRAYINITTYPESIZEU3D3_T3217885683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3
struct  __StaticArrayInitTypeSizeU3D3_t3217885683 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_t3217885683__padding[3];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3_T3217885683_H
#ifndef __STATICARRAYINITTYPESIZEU3D9_T3218278899_H
#define __STATICARRAYINITTYPESIZEU3D9_T3218278899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9
struct  __StaticArrayInitTypeSizeU3D9_t3218278899 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D9_t3218278899__padding[9];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D9_T3218278899_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T1548194904_H
#define __STATICARRAYINITTYPESIZEU3D10_T1548194904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_t1548194904 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t1548194904__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T1548194904_H
#ifndef NETSCAPECERTTYPEEXTENSION_T1524296876_H
#define NETSCAPECERTTYPEEXTENSION_T1524296876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension
struct  NetscapeCertTypeExtension_t1524296876  : public X509Extension_t3173393653
{
public:
	// System.Int32 Mono.Security.X509.Extensions.NetscapeCertTypeExtension::ctbits
	int32_t ___ctbits_3;

public:
	inline static int32_t get_offset_of_ctbits_3() { return static_cast<int32_t>(offsetof(NetscapeCertTypeExtension_t1524296876, ___ctbits_3)); }
	inline int32_t get_ctbits_3() const { return ___ctbits_3; }
	inline int32_t* get_address_of_ctbits_3() { return &___ctbits_3; }
	inline void set_ctbits_3(int32_t value)
	{
		___ctbits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPECERTTYPEEXTENSION_T1524296876_H
#ifndef BASICCONSTRAINTSEXTENSION_T2462195279_H
#define BASICCONSTRAINTSEXTENSION_T2462195279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct  BasicConstraintsExtension_t2462195279  : public X509Extension_t3173393653
{
public:
	// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::cA
	bool ___cA_3;
	// System.Int32 Mono.Security.X509.Extensions.BasicConstraintsExtension::pathLenConstraint
	int32_t ___pathLenConstraint_4;

public:
	inline static int32_t get_offset_of_cA_3() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t2462195279, ___cA_3)); }
	inline bool get_cA_3() const { return ___cA_3; }
	inline bool* get_address_of_cA_3() { return &___cA_3; }
	inline void set_cA_3(bool value)
	{
		___cA_3 = value;
	}

	inline static int32_t get_offset_of_pathLenConstraint_4() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t2462195279, ___pathLenConstraint_4)); }
	inline int32_t get_pathLenConstraint_4() const { return ___pathLenConstraint_4; }
	inline int32_t* get_address_of_pathLenConstraint_4() { return &___pathLenConstraint_4; }
	inline void set_pathLenConstraint_4(int32_t value)
	{
		___pathLenConstraint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCONSTRAINTSEXTENSION_T2462195279_H
#ifndef __STATICARRAYINITTYPESIZEU3D14_T3517563372_H
#define __STATICARRAYINITTYPESIZEU3D14_T3517563372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14
struct  __StaticArrayInitTypeSizeU3D14_t3517563372 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t3517563372__padding[14];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D14_T3517563372_H
#ifndef AUTHORITYKEYIDENTIFIEREXTENSION_T1122691429_H
#define AUTHORITYKEYIDENTIFIEREXTENSION_T1122691429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct  AuthorityKeyIdentifierExtension_t1122691429  : public X509Extension_t3173393653
{
public:
	// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::aki
	ByteU5BU5D_t4116647657* ___aki_3;

public:
	inline static int32_t get_offset_of_aki_3() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifierExtension_t1122691429, ___aki_3)); }
	inline ByteU5BU5D_t4116647657* get_aki_3() const { return ___aki_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_aki_3() { return &___aki_3; }
	inline void set_aki_3(ByteU5BU5D_t4116647657* value)
	{
		___aki_3 = value;
		Il2CppCodeGenWriteBarrier((&___aki_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYKEYIDENTIFIEREXTENSION_T1122691429_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef __STATICARRAYINITTYPESIZEU3D3132_T3825993976_H
#define __STATICARRAYINITTYPESIZEU3D3132_T3825993976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3132
struct  __StaticArrayInitTypeSizeU3D3132_t3825993976 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3132_t3825993976__padding[3132];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3132_T3825993976_H
#ifndef ARRAYSEGMENT_1_T283560987_H
#define ARRAYSEGMENT_1_T283560987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArraySegment`1<System.Byte>
struct  ArraySegment_1_t283560987 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_t4116647657* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t283560987, ____array_0)); }
	inline ByteU5BU5D_t4116647657* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_t4116647657* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t283560987, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t283560987, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENT_1_T283560987_H
#ifndef __STATICARRAYINITTYPESIZEU3D524_T4061560205_H
#define __STATICARRAYINITTYPESIZEU3D524_T4061560205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=524
struct  __StaticArrayInitTypeSizeU3D524_t4061560205 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D524_t4061560205__padding[524];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D524_T4061560205_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T2711125390_H
#define __STATICARRAYINITTYPESIZEU3D32_T2711125390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t2711125390 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t2711125390__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T2711125390_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T2047459750_H
#define MONOPINVOKECALLBACKATTRIBUTE_T2047459750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Util.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t2047459750  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T2047459750_H
#ifndef __STATICARRAYINITTYPESIZEU3D48_T1904228656_H
#define __STATICARRAYINITTYPESIZEU3D48_T1904228656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48
struct  __StaticArrayInitTypeSizeU3D48_t1904228656 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D48_t1904228656__padding[48];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D48_T1904228656_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_T3517497836_H
#define __STATICARRAYINITTYPESIZEU3D64_T3517497836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t3517497836 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t3517497836__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_T3517497836_H
#ifndef DNSTYPE_T1288195022_H
#define DNSTYPE_T1288195022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsType
struct  DnsType_t1288195022 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsType_t1288195022, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSTYPE_T1288195022_H
#ifndef RESOLVERERROR_T779281070_H
#define RESOLVERERROR_T779281070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.ResolverError
struct  ResolverError_t779281070 
{
public:
	// System.Int32 Mono.Net.Dns.ResolverError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResolverError_t779281070, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVERERROR_T779281070_H
#ifndef DNSRCODE_T1462348096_H
#define DNSRCODE_T1462348096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsRCode
struct  DnsRCode_t1462348096 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsRCode::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsRCode_t1462348096, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRCODE_T1462348096_H
#ifndef MONOSSLPOLICYERRORS_T2590217945_H
#define MONOSSLPOLICYERRORS_T2590217945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoSslPolicyErrors
struct  MonoSslPolicyErrors_t2590217945 
{
public:
	// System.Int32 Mono.Security.Interface.MonoSslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonoSslPolicyErrors_t2590217945, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLPOLICYERRORS_T2590217945_H
#ifndef WEBEXCEPTIONSTATUS_T1731416715_H
#define WEBEXCEPTIONSTATUS_T1731416715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t1731416715 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t1731416715, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T1731416715_H
#ifndef X509REVOCATIONMODE_T2571829933_H
#define X509REVOCATIONMODE_T2571829933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t2571829933 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509RevocationMode_t2571829933, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T2571829933_H
#ifndef KEYUSAGES_T820456313_H
#define KEYUSAGES_T820456313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsages
struct  KeyUsages_t820456313 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsages::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyUsages_t820456313, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGES_T820456313_H
#ifndef X509KEYUSAGEFLAGS_T1431795504_H
#define X509KEYUSAGEFLAGS_T1431795504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t1431795504 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t1431795504, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T1431795504_H
#ifndef AUTHENTICATEDSTREAM_T3415418016_H
#define AUTHENTICATEDSTREAM_T3415418016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticatedStream
struct  AuthenticatedStream_t3415418016  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.Net.Security.AuthenticatedStream::_InnerStream
	Stream_t1273022909 * ____InnerStream_4;
	// System.Boolean System.Net.Security.AuthenticatedStream::_LeaveStreamOpen
	bool ____LeaveStreamOpen_5;

public:
	inline static int32_t get_offset_of__InnerStream_4() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t3415418016, ____InnerStream_4)); }
	inline Stream_t1273022909 * get__InnerStream_4() const { return ____InnerStream_4; }
	inline Stream_t1273022909 ** get_address_of__InnerStream_4() { return &____InnerStream_4; }
	inline void set__InnerStream_4(Stream_t1273022909 * value)
	{
		____InnerStream_4 = value;
		Il2CppCodeGenWriteBarrier((&____InnerStream_4), value);
	}

	inline static int32_t get_offset_of__LeaveStreamOpen_5() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t3415418016, ____LeaveStreamOpen_5)); }
	inline bool get__LeaveStreamOpen_5() const { return ____LeaveStreamOpen_5; }
	inline bool* get_address_of__LeaveStreamOpen_5() { return &____LeaveStreamOpen_5; }
	inline void set__LeaveStreamOpen_5(bool value)
	{
		____LeaveStreamOpen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDSTREAM_T3415418016_H
#ifndef CIPHERMODE_T84635067_H
#define CIPHERMODE_T84635067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t84635067 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t84635067, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T84635067_H
#ifndef PADDINGMODE_T2546806710_H
#define PADDINGMODE_T2546806710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t2546806710 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_t2546806710, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T2546806710_H
#ifndef CERTTYPES_T3317701015_H
#define CERTTYPES_T3317701015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
struct  CertTypes_t3317701015 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CertTypes_t3317701015, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTTYPES_T3317701015_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef DNSQTYPE_T3699027829_H
#define DNSQTYPE_T3699027829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQType
struct  DnsQType_t3699027829 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsQType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsQType_t3699027829, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQTYPE_T3699027829_H
#ifndef LEGACYTLSPROVIDER_T3797502067_H
#define LEGACYTLSPROVIDER_T3797502067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.LegacyTlsProvider
struct  LegacyTlsProvider_t3797502067  : public MonoTlsProvider_t3152003291
{
public:

public:
};

struct LegacyTlsProvider_t3797502067_StaticFields
{
public:
	// System.Guid Mono.Net.Security.LegacyTlsProvider::id
	Guid_t  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LegacyTlsProvider_t3797502067_StaticFields, ___id_0)); }
	inline Guid_t  get_id_0() const { return ___id_0; }
	inline Guid_t * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(Guid_t  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYTLSPROVIDER_T3797502067_H
#ifndef RSAMANAGED_T1757093820_H
#define RSAMANAGED_T1757093820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RSAManaged
struct  RSAManaged_t1757093820  : public RSA_t2385438082
{
public:
	// System.Boolean Mono.Security.Cryptography.RSAManaged::isCRTpossible
	bool ___isCRTpossible_2;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::keyBlinding
	bool ___keyBlinding_3;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::keypairGenerated
	bool ___keypairGenerated_4;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::m_disposed
	bool ___m_disposed_5;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::d
	BigInteger_t2902905090 * ___d_6;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::p
	BigInteger_t2902905090 * ___p_7;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::q
	BigInteger_t2902905090 * ___q_8;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::dp
	BigInteger_t2902905090 * ___dp_9;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::dq
	BigInteger_t2902905090 * ___dq_10;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::qInv
	BigInteger_t2902905090 * ___qInv_11;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::n
	BigInteger_t2902905090 * ___n_12;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::e
	BigInteger_t2902905090 * ___e_13;
	// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler Mono.Security.Cryptography.RSAManaged::KeyGenerated
	KeyGeneratedEventHandler_t3064139578 * ___KeyGenerated_14;

public:
	inline static int32_t get_offset_of_isCRTpossible_2() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___isCRTpossible_2)); }
	inline bool get_isCRTpossible_2() const { return ___isCRTpossible_2; }
	inline bool* get_address_of_isCRTpossible_2() { return &___isCRTpossible_2; }
	inline void set_isCRTpossible_2(bool value)
	{
		___isCRTpossible_2 = value;
	}

	inline static int32_t get_offset_of_keyBlinding_3() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___keyBlinding_3)); }
	inline bool get_keyBlinding_3() const { return ___keyBlinding_3; }
	inline bool* get_address_of_keyBlinding_3() { return &___keyBlinding_3; }
	inline void set_keyBlinding_3(bool value)
	{
		___keyBlinding_3 = value;
	}

	inline static int32_t get_offset_of_keypairGenerated_4() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___keypairGenerated_4)); }
	inline bool get_keypairGenerated_4() const { return ___keypairGenerated_4; }
	inline bool* get_address_of_keypairGenerated_4() { return &___keypairGenerated_4; }
	inline void set_keypairGenerated_4(bool value)
	{
		___keypairGenerated_4 = value;
	}

	inline static int32_t get_offset_of_m_disposed_5() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___m_disposed_5)); }
	inline bool get_m_disposed_5() const { return ___m_disposed_5; }
	inline bool* get_address_of_m_disposed_5() { return &___m_disposed_5; }
	inline void set_m_disposed_5(bool value)
	{
		___m_disposed_5 = value;
	}

	inline static int32_t get_offset_of_d_6() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___d_6)); }
	inline BigInteger_t2902905090 * get_d_6() const { return ___d_6; }
	inline BigInteger_t2902905090 ** get_address_of_d_6() { return &___d_6; }
	inline void set_d_6(BigInteger_t2902905090 * value)
	{
		___d_6 = value;
		Il2CppCodeGenWriteBarrier((&___d_6), value);
	}

	inline static int32_t get_offset_of_p_7() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___p_7)); }
	inline BigInteger_t2902905090 * get_p_7() const { return ___p_7; }
	inline BigInteger_t2902905090 ** get_address_of_p_7() { return &___p_7; }
	inline void set_p_7(BigInteger_t2902905090 * value)
	{
		___p_7 = value;
		Il2CppCodeGenWriteBarrier((&___p_7), value);
	}

	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___q_8)); }
	inline BigInteger_t2902905090 * get_q_8() const { return ___q_8; }
	inline BigInteger_t2902905090 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t2902905090 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}

	inline static int32_t get_offset_of_dp_9() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___dp_9)); }
	inline BigInteger_t2902905090 * get_dp_9() const { return ___dp_9; }
	inline BigInteger_t2902905090 ** get_address_of_dp_9() { return &___dp_9; }
	inline void set_dp_9(BigInteger_t2902905090 * value)
	{
		___dp_9 = value;
		Il2CppCodeGenWriteBarrier((&___dp_9), value);
	}

	inline static int32_t get_offset_of_dq_10() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___dq_10)); }
	inline BigInteger_t2902905090 * get_dq_10() const { return ___dq_10; }
	inline BigInteger_t2902905090 ** get_address_of_dq_10() { return &___dq_10; }
	inline void set_dq_10(BigInteger_t2902905090 * value)
	{
		___dq_10 = value;
		Il2CppCodeGenWriteBarrier((&___dq_10), value);
	}

	inline static int32_t get_offset_of_qInv_11() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___qInv_11)); }
	inline BigInteger_t2902905090 * get_qInv_11() const { return ___qInv_11; }
	inline BigInteger_t2902905090 ** get_address_of_qInv_11() { return &___qInv_11; }
	inline void set_qInv_11(BigInteger_t2902905090 * value)
	{
		___qInv_11 = value;
		Il2CppCodeGenWriteBarrier((&___qInv_11), value);
	}

	inline static int32_t get_offset_of_n_12() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___n_12)); }
	inline BigInteger_t2902905090 * get_n_12() const { return ___n_12; }
	inline BigInteger_t2902905090 ** get_address_of_n_12() { return &___n_12; }
	inline void set_n_12(BigInteger_t2902905090 * value)
	{
		___n_12 = value;
		Il2CppCodeGenWriteBarrier((&___n_12), value);
	}

	inline static int32_t get_offset_of_e_13() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___e_13)); }
	inline BigInteger_t2902905090 * get_e_13() const { return ___e_13; }
	inline BigInteger_t2902905090 ** get_address_of_e_13() { return &___e_13; }
	inline void set_e_13(BigInteger_t2902905090 * value)
	{
		___e_13 = value;
		Il2CppCodeGenWriteBarrier((&___e_13), value);
	}

	inline static int32_t get_offset_of_KeyGenerated_14() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___KeyGenerated_14)); }
	inline KeyGeneratedEventHandler_t3064139578 * get_KeyGenerated_14() const { return ___KeyGenerated_14; }
	inline KeyGeneratedEventHandler_t3064139578 ** get_address_of_KeyGenerated_14() { return &___KeyGenerated_14; }
	inline void set_KeyGenerated_14(KeyGeneratedEventHandler_t3064139578 * value)
	{
		___KeyGenerated_14 = value;
		Il2CppCodeGenWriteBarrier((&___KeyGenerated_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAMANAGED_T1757093820_H
#ifndef HMAC_T3689525210_H
#define HMAC_T3689525210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.HMAC
struct  HMAC_t3689525210  : public KeyedHashAlgorithm_t112861511
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.HMAC::hash
	HashAlgorithm_t1432317219 * ___hash_5;
	// System.Boolean Mono.Security.Cryptography.HMAC::hashing
	bool ___hashing_6;
	// System.Byte[] Mono.Security.Cryptography.HMAC::innerPad
	ByteU5BU5D_t4116647657* ___innerPad_7;
	// System.Byte[] Mono.Security.Cryptography.HMAC::outerPad
	ByteU5BU5D_t4116647657* ___outerPad_8;

public:
	inline static int32_t get_offset_of_hash_5() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___hash_5)); }
	inline HashAlgorithm_t1432317219 * get_hash_5() const { return ___hash_5; }
	inline HashAlgorithm_t1432317219 ** get_address_of_hash_5() { return &___hash_5; }
	inline void set_hash_5(HashAlgorithm_t1432317219 * value)
	{
		___hash_5 = value;
		Il2CppCodeGenWriteBarrier((&___hash_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}

	inline static int32_t get_offset_of_innerPad_7() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___innerPad_7)); }
	inline ByteU5BU5D_t4116647657* get_innerPad_7() const { return ___innerPad_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_innerPad_7() { return &___innerPad_7; }
	inline void set_innerPad_7(ByteU5BU5D_t4116647657* value)
	{
		___innerPad_7 = value;
		Il2CppCodeGenWriteBarrier((&___innerPad_7), value);
	}

	inline static int32_t get_offset_of_outerPad_8() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___outerPad_8)); }
	inline ByteU5BU5D_t4116647657* get_outerPad_8() const { return ___outerPad_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_outerPad_8() { return &___outerPad_8; }
	inline void set_outerPad_8(ByteU5BU5D_t4116647657* value)
	{
		___outerPad_8 = value;
		Il2CppCodeGenWriteBarrier((&___outerPad_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_T3689525210_H
#ifndef GETPROXYDATA_T72916098_H
#define GETPROXYDATA_T72916098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/GetProxyData
struct  GetProxyData_t72916098  : public RuntimeObject
{
public:
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::script
	intptr_t ___script_0;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::targetUri
	intptr_t ___targetUri_1;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::error
	intptr_t ___error_2;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::result
	intptr_t ___result_3;
	// System.Threading.ManualResetEvent Mono.Net.CFNetwork/GetProxyData::evt
	ManualResetEvent_t451242010 * ___evt_4;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(GetProxyData_t72916098, ___script_0)); }
	inline intptr_t get_script_0() const { return ___script_0; }
	inline intptr_t* get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(intptr_t value)
	{
		___script_0 = value;
	}

	inline static int32_t get_offset_of_targetUri_1() { return static_cast<int32_t>(offsetof(GetProxyData_t72916098, ___targetUri_1)); }
	inline intptr_t get_targetUri_1() const { return ___targetUri_1; }
	inline intptr_t* get_address_of_targetUri_1() { return &___targetUri_1; }
	inline void set_targetUri_1(intptr_t value)
	{
		___targetUri_1 = value;
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(GetProxyData_t72916098, ___error_2)); }
	inline intptr_t get_error_2() const { return ___error_2; }
	inline intptr_t* get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(intptr_t value)
	{
		___error_2 = value;
	}

	inline static int32_t get_offset_of_result_3() { return static_cast<int32_t>(offsetof(GetProxyData_t72916098, ___result_3)); }
	inline intptr_t get_result_3() const { return ___result_3; }
	inline intptr_t* get_address_of_result_3() { return &___result_3; }
	inline void set_result_3(intptr_t value)
	{
		___result_3 = value;
	}

	inline static int32_t get_offset_of_evt_4() { return static_cast<int32_t>(offsetof(GetProxyData_t72916098, ___evt_4)); }
	inline ManualResetEvent_t451242010 * get_evt_4() const { return ___evt_4; }
	inline ManualResetEvent_t451242010 ** get_address_of_evt_4() { return &___evt_4; }
	inline void set_evt_4(ManualResetEvent_t451242010 * value)
	{
		___evt_4 = value;
		Il2CppCodeGenWriteBarrier((&___evt_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPROXYDATA_T72916098_H
#ifndef CFPROXYSETTINGS_T3399020509_H
#define CFPROXYSETTINGS_T3399020509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxySettings
struct  CFProxySettings_t3399020509  : public RuntimeObject
{
public:
	// Mono.Net.CFDictionary Mono.Net.CFProxySettings::settings
	CFDictionary_t2669341669 * ___settings_6;

public:
	inline static int32_t get_offset_of_settings_6() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509, ___settings_6)); }
	inline CFDictionary_t2669341669 * get_settings_6() const { return ___settings_6; }
	inline CFDictionary_t2669341669 ** get_address_of_settings_6() { return &___settings_6; }
	inline void set_settings_6(CFDictionary_t2669341669 * value)
	{
		___settings_6 = value;
		Il2CppCodeGenWriteBarrier((&___settings_6), value);
	}
};

struct CFProxySettings_t3399020509_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPEnable
	intptr_t ___kCFNetworkProxiesHTTPEnable_0;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPPort
	intptr_t ___kCFNetworkProxiesHTTPPort_1;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPProxy
	intptr_t ___kCFNetworkProxiesHTTPProxy_2;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigEnable
	intptr_t ___kCFNetworkProxiesProxyAutoConfigEnable_3;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigJavaScript
	intptr_t ___kCFNetworkProxiesProxyAutoConfigJavaScript_4;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigURLString
	intptr_t ___kCFNetworkProxiesProxyAutoConfigURLString_5;

public:
	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPEnable_0() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509_StaticFields, ___kCFNetworkProxiesHTTPEnable_0)); }
	inline intptr_t get_kCFNetworkProxiesHTTPEnable_0() const { return ___kCFNetworkProxiesHTTPEnable_0; }
	inline intptr_t* get_address_of_kCFNetworkProxiesHTTPEnable_0() { return &___kCFNetworkProxiesHTTPEnable_0; }
	inline void set_kCFNetworkProxiesHTTPEnable_0(intptr_t value)
	{
		___kCFNetworkProxiesHTTPEnable_0 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPPort_1() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509_StaticFields, ___kCFNetworkProxiesHTTPPort_1)); }
	inline intptr_t get_kCFNetworkProxiesHTTPPort_1() const { return ___kCFNetworkProxiesHTTPPort_1; }
	inline intptr_t* get_address_of_kCFNetworkProxiesHTTPPort_1() { return &___kCFNetworkProxiesHTTPPort_1; }
	inline void set_kCFNetworkProxiesHTTPPort_1(intptr_t value)
	{
		___kCFNetworkProxiesHTTPPort_1 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPProxy_2() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509_StaticFields, ___kCFNetworkProxiesHTTPProxy_2)); }
	inline intptr_t get_kCFNetworkProxiesHTTPProxy_2() const { return ___kCFNetworkProxiesHTTPProxy_2; }
	inline intptr_t* get_address_of_kCFNetworkProxiesHTTPProxy_2() { return &___kCFNetworkProxiesHTTPProxy_2; }
	inline void set_kCFNetworkProxiesHTTPProxy_2(intptr_t value)
	{
		___kCFNetworkProxiesHTTPProxy_2 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigEnable_3() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509_StaticFields, ___kCFNetworkProxiesProxyAutoConfigEnable_3)); }
	inline intptr_t get_kCFNetworkProxiesProxyAutoConfigEnable_3() const { return ___kCFNetworkProxiesProxyAutoConfigEnable_3; }
	inline intptr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigEnable_3() { return &___kCFNetworkProxiesProxyAutoConfigEnable_3; }
	inline void set_kCFNetworkProxiesProxyAutoConfigEnable_3(intptr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigEnable_3 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509_StaticFields, ___kCFNetworkProxiesProxyAutoConfigJavaScript_4)); }
	inline intptr_t get_kCFNetworkProxiesProxyAutoConfigJavaScript_4() const { return ___kCFNetworkProxiesProxyAutoConfigJavaScript_4; }
	inline intptr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4() { return &___kCFNetworkProxiesProxyAutoConfigJavaScript_4; }
	inline void set_kCFNetworkProxiesProxyAutoConfigJavaScript_4(intptr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigJavaScript_4 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigURLString_5() { return static_cast<int32_t>(offsetof(CFProxySettings_t3399020509_StaticFields, ___kCFNetworkProxiesProxyAutoConfigURLString_5)); }
	inline intptr_t get_kCFNetworkProxiesProxyAutoConfigURLString_5() const { return ___kCFNetworkProxiesProxyAutoConfigURLString_5; }
	inline intptr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigURLString_5() { return &___kCFNetworkProxiesProxyAutoConfigURLString_5; }
	inline void set_kCFNetworkProxiesProxyAutoConfigURLString_5(intptr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigURLString_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXYSETTINGS_T3399020509_H
#ifndef CFPROXY_T2349940_H
#define CFPROXY_T2349940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxy
struct  CFProxy_t2349940  : public RuntimeObject
{
public:
	// Mono.Net.CFDictionary Mono.Net.CFProxy::settings
	CFDictionary_t2669341669 * ___settings_13;

public:
	inline static int32_t get_offset_of_settings_13() { return static_cast<int32_t>(offsetof(CFProxy_t2349940, ___settings_13)); }
	inline CFDictionary_t2669341669 * get_settings_13() const { return ___settings_13; }
	inline CFDictionary_t2669341669 ** get_address_of_settings_13() { return &___settings_13; }
	inline void set_settings_13(CFDictionary_t2669341669 * value)
	{
		___settings_13 = value;
		Il2CppCodeGenWriteBarrier((&___settings_13), value);
	}
};

struct CFProxy_t2349940_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFProxy::kCFProxyAutoConfigurationJavaScriptKey
	intptr_t ___kCFProxyAutoConfigurationJavaScriptKey_0;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyAutoConfigurationURLKey
	intptr_t ___kCFProxyAutoConfigurationURLKey_1;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyHostNameKey
	intptr_t ___kCFProxyHostNameKey_2;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyPasswordKey
	intptr_t ___kCFProxyPasswordKey_3;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyPortNumberKey
	intptr_t ___kCFProxyPortNumberKey_4;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeKey
	intptr_t ___kCFProxyTypeKey_5;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyUsernameKey
	intptr_t ___kCFProxyUsernameKey_6;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeAutoConfigurationURL
	intptr_t ___kCFProxyTypeAutoConfigurationURL_7;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeAutoConfigurationJavaScript
	intptr_t ___kCFProxyTypeAutoConfigurationJavaScript_8;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeFTP
	intptr_t ___kCFProxyTypeFTP_9;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeHTTP
	intptr_t ___kCFProxyTypeHTTP_10;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeHTTPS
	intptr_t ___kCFProxyTypeHTTPS_11;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeSOCKS
	intptr_t ___kCFProxyTypeSOCKS_12;

public:
	inline static int32_t get_offset_of_kCFProxyAutoConfigurationJavaScriptKey_0() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyAutoConfigurationJavaScriptKey_0)); }
	inline intptr_t get_kCFProxyAutoConfigurationJavaScriptKey_0() const { return ___kCFProxyAutoConfigurationJavaScriptKey_0; }
	inline intptr_t* get_address_of_kCFProxyAutoConfigurationJavaScriptKey_0() { return &___kCFProxyAutoConfigurationJavaScriptKey_0; }
	inline void set_kCFProxyAutoConfigurationJavaScriptKey_0(intptr_t value)
	{
		___kCFProxyAutoConfigurationJavaScriptKey_0 = value;
	}

	inline static int32_t get_offset_of_kCFProxyAutoConfigurationURLKey_1() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyAutoConfigurationURLKey_1)); }
	inline intptr_t get_kCFProxyAutoConfigurationURLKey_1() const { return ___kCFProxyAutoConfigurationURLKey_1; }
	inline intptr_t* get_address_of_kCFProxyAutoConfigurationURLKey_1() { return &___kCFProxyAutoConfigurationURLKey_1; }
	inline void set_kCFProxyAutoConfigurationURLKey_1(intptr_t value)
	{
		___kCFProxyAutoConfigurationURLKey_1 = value;
	}

	inline static int32_t get_offset_of_kCFProxyHostNameKey_2() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyHostNameKey_2)); }
	inline intptr_t get_kCFProxyHostNameKey_2() const { return ___kCFProxyHostNameKey_2; }
	inline intptr_t* get_address_of_kCFProxyHostNameKey_2() { return &___kCFProxyHostNameKey_2; }
	inline void set_kCFProxyHostNameKey_2(intptr_t value)
	{
		___kCFProxyHostNameKey_2 = value;
	}

	inline static int32_t get_offset_of_kCFProxyPasswordKey_3() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyPasswordKey_3)); }
	inline intptr_t get_kCFProxyPasswordKey_3() const { return ___kCFProxyPasswordKey_3; }
	inline intptr_t* get_address_of_kCFProxyPasswordKey_3() { return &___kCFProxyPasswordKey_3; }
	inline void set_kCFProxyPasswordKey_3(intptr_t value)
	{
		___kCFProxyPasswordKey_3 = value;
	}

	inline static int32_t get_offset_of_kCFProxyPortNumberKey_4() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyPortNumberKey_4)); }
	inline intptr_t get_kCFProxyPortNumberKey_4() const { return ___kCFProxyPortNumberKey_4; }
	inline intptr_t* get_address_of_kCFProxyPortNumberKey_4() { return &___kCFProxyPortNumberKey_4; }
	inline void set_kCFProxyPortNumberKey_4(intptr_t value)
	{
		___kCFProxyPortNumberKey_4 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeKey_5() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeKey_5)); }
	inline intptr_t get_kCFProxyTypeKey_5() const { return ___kCFProxyTypeKey_5; }
	inline intptr_t* get_address_of_kCFProxyTypeKey_5() { return &___kCFProxyTypeKey_5; }
	inline void set_kCFProxyTypeKey_5(intptr_t value)
	{
		___kCFProxyTypeKey_5 = value;
	}

	inline static int32_t get_offset_of_kCFProxyUsernameKey_6() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyUsernameKey_6)); }
	inline intptr_t get_kCFProxyUsernameKey_6() const { return ___kCFProxyUsernameKey_6; }
	inline intptr_t* get_address_of_kCFProxyUsernameKey_6() { return &___kCFProxyUsernameKey_6; }
	inline void set_kCFProxyUsernameKey_6(intptr_t value)
	{
		___kCFProxyUsernameKey_6 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeAutoConfigurationURL_7() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeAutoConfigurationURL_7)); }
	inline intptr_t get_kCFProxyTypeAutoConfigurationURL_7() const { return ___kCFProxyTypeAutoConfigurationURL_7; }
	inline intptr_t* get_address_of_kCFProxyTypeAutoConfigurationURL_7() { return &___kCFProxyTypeAutoConfigurationURL_7; }
	inline void set_kCFProxyTypeAutoConfigurationURL_7(intptr_t value)
	{
		___kCFProxyTypeAutoConfigurationURL_7 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeAutoConfigurationJavaScript_8() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeAutoConfigurationJavaScript_8)); }
	inline intptr_t get_kCFProxyTypeAutoConfigurationJavaScript_8() const { return ___kCFProxyTypeAutoConfigurationJavaScript_8; }
	inline intptr_t* get_address_of_kCFProxyTypeAutoConfigurationJavaScript_8() { return &___kCFProxyTypeAutoConfigurationJavaScript_8; }
	inline void set_kCFProxyTypeAutoConfigurationJavaScript_8(intptr_t value)
	{
		___kCFProxyTypeAutoConfigurationJavaScript_8 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeFTP_9() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeFTP_9)); }
	inline intptr_t get_kCFProxyTypeFTP_9() const { return ___kCFProxyTypeFTP_9; }
	inline intptr_t* get_address_of_kCFProxyTypeFTP_9() { return &___kCFProxyTypeFTP_9; }
	inline void set_kCFProxyTypeFTP_9(intptr_t value)
	{
		___kCFProxyTypeFTP_9 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeHTTP_10() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeHTTP_10)); }
	inline intptr_t get_kCFProxyTypeHTTP_10() const { return ___kCFProxyTypeHTTP_10; }
	inline intptr_t* get_address_of_kCFProxyTypeHTTP_10() { return &___kCFProxyTypeHTTP_10; }
	inline void set_kCFProxyTypeHTTP_10(intptr_t value)
	{
		___kCFProxyTypeHTTP_10 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeHTTPS_11() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeHTTPS_11)); }
	inline intptr_t get_kCFProxyTypeHTTPS_11() const { return ___kCFProxyTypeHTTPS_11; }
	inline intptr_t* get_address_of_kCFProxyTypeHTTPS_11() { return &___kCFProxyTypeHTTPS_11; }
	inline void set_kCFProxyTypeHTTPS_11(intptr_t value)
	{
		___kCFProxyTypeHTTPS_11 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeSOCKS_12() { return static_cast<int32_t>(offsetof(CFProxy_t2349940_StaticFields, ___kCFProxyTypeSOCKS_12)); }
	inline intptr_t get_kCFProxyTypeSOCKS_12() const { return ___kCFProxyTypeSOCKS_12; }
	inline intptr_t* get_address_of_kCFProxyTypeSOCKS_12() { return &___kCFProxyTypeSOCKS_12; }
	inline void set_kCFProxyTypeSOCKS_12(intptr_t value)
	{
		___kCFProxyTypeSOCKS_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXY_T2349940_H
#ifndef CFPROXYTYPE_T1170449121_H
#define CFPROXYTYPE_T1170449121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxyType
struct  CFProxyType_t1170449121 
{
public:
	// System.Int32 Mono.Net.CFProxyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CFProxyType_t1170449121, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXYTYPE_T1170449121_H
#ifndef CFSTREAMCLIENTCONTEXT_T3215013463_H
#define CFSTREAMCLIENTCONTEXT_T3215013463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFStreamClientContext
struct  CFStreamClientContext_t3215013463 
{
public:
	// System.IntPtr Mono.Net.CFStreamClientContext::Version
	intptr_t ___Version_0;
	// System.IntPtr Mono.Net.CFStreamClientContext::Info
	intptr_t ___Info_1;
	// System.IntPtr Mono.Net.CFStreamClientContext::Retain
	intptr_t ___Retain_2;
	// System.IntPtr Mono.Net.CFStreamClientContext::Release
	intptr_t ___Release_3;
	// System.IntPtr Mono.Net.CFStreamClientContext::CopyDescription
	intptr_t ___CopyDescription_4;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3215013463, ___Version_0)); }
	inline intptr_t get_Version_0() const { return ___Version_0; }
	inline intptr_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(intptr_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Info_1() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3215013463, ___Info_1)); }
	inline intptr_t get_Info_1() const { return ___Info_1; }
	inline intptr_t* get_address_of_Info_1() { return &___Info_1; }
	inline void set_Info_1(intptr_t value)
	{
		___Info_1 = value;
	}

	inline static int32_t get_offset_of_Retain_2() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3215013463, ___Retain_2)); }
	inline intptr_t get_Retain_2() const { return ___Retain_2; }
	inline intptr_t* get_address_of_Retain_2() { return &___Retain_2; }
	inline void set_Retain_2(intptr_t value)
	{
		___Retain_2 = value;
	}

	inline static int32_t get_offset_of_Release_3() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3215013463, ___Release_3)); }
	inline intptr_t get_Release_3() const { return ___Release_3; }
	inline intptr_t* get_address_of_Release_3() { return &___Release_3; }
	inline void set_Release_3(intptr_t value)
	{
		___Release_3 = value;
	}

	inline static int32_t get_offset_of_CopyDescription_4() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3215013463, ___CopyDescription_4)); }
	inline intptr_t get_CopyDescription_4() const { return ___CopyDescription_4; }
	inline intptr_t* get_address_of_CopyDescription_4() { return &___CopyDescription_4; }
	inline void set_CopyDescription_4(intptr_t value)
	{
		___CopyDescription_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFSTREAMCLIENTCONTEXT_T3215013463_H
#ifndef CFRANGE_T3090757970_H
#define CFRANGE_T3090757970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFRange
struct  CFRange_t3090757970 
{
public:
	// System.IntPtr Mono.Net.CFRange::Location
	intptr_t ___Location_0;
	// System.IntPtr Mono.Net.CFRange::Length
	intptr_t ___Length_1;

public:
	inline static int32_t get_offset_of_Location_0() { return static_cast<int32_t>(offsetof(CFRange_t3090757970, ___Location_0)); }
	inline intptr_t get_Location_0() const { return ___Location_0; }
	inline intptr_t* get_address_of_Location_0() { return &___Location_0; }
	inline void set_Location_0(intptr_t value)
	{
		___Location_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(CFRange_t3090757970, ___Length_1)); }
	inline intptr_t get_Length_1() const { return ___Length_1; }
	inline intptr_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(intptr_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRANGE_T3090757970_H
#ifndef CFOBJECT_T2662441515_H
#define CFOBJECT_T2662441515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFObject
struct  CFObject_t2662441515  : public RuntimeObject
{
public:
	// System.IntPtr Mono.Net.CFObject::<Handle>k__BackingField
	intptr_t ___U3CHandleU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CHandleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CFObject_t2662441515, ___U3CHandleU3Ek__BackingField_0)); }
	inline intptr_t get_U3CHandleU3Ek__BackingField_0() const { return ___U3CHandleU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CHandleU3Ek__BackingField_0() { return &___U3CHandleU3Ek__BackingField_0; }
	inline void set_U3CHandleU3Ek__BackingField_0(intptr_t value)
	{
		___U3CHandleU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFOBJECT_T2662441515_H
#ifndef SIGN_T3338384039_H
#define SIGN_T3338384039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/Sign
struct  Sign_t3338384039 
{
public:
	// System.Int32 Mono.Math.BigInteger/Sign::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Sign_t3338384039, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGN_T3338384039_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255363_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255363  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::12D04472A8285260EA12FD3813CDFA9F2D2B548C
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0;
	// System.Int32 <PrivateImplementationDetails>::1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C
	int32_t ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::13A35EF1A549297C70E2AD46045BBD2ECA17852D
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::1A84029C80CB5518379F199F53FF08A7B764F8FD
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::235D99572263B22ADFEE10FDA0C25E12F4D94FFC
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14 <PrivateImplementationDetails>::2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130
	__StaticArrayInitTypeSizeU3D14_t3517563372  ___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5;
	// System.Int32 <PrivateImplementationDetails>::31D8729F7377B44017C0A2395A582C9CA4163277
	int32_t ___31D8729F7377B44017C0A2395A582C9CA4163277_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::320B018758ECE3752FFEDBAEB1A6DB67C80B9359
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::3E3442C7396F3F2BB4C7348F4A2074C7DC677D68
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48 <PrivateImplementationDetails>::4E3B533C39447AAEB59A8E48FABD7E15B5B5D195
	__StaticArrayInitTypeSizeU3D48_t1904228656  ___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::56DFA5053B3131883637F53219E7D88CCEF35949
	__StaticArrayInitTypeSizeU3D10_t1548194904  ___56DFA5053B3131883637F53219E7D88CCEF35949_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9 <PrivateImplementationDetails>::6D49C9D487D7AD3491ECE08732D68A593CC2038D
	__StaticArrayInitTypeSizeU3D9_t3218278899  ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3132 <PrivateImplementationDetails>::6E5DC824F803F8565AF31B42199DAE39FE7F4EA9
	__StaticArrayInitTypeSizeU3D3132_t3825993976  ___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::736D39815215889F11249D9958F6ED12D37B9F57
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___736D39815215889F11249D9958F6ED12D37B9F57_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::86F4F563FA2C61798AE6238D789139739428463A
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___86F4F563FA2C61798AE6238D789139739428463A_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::97FB30C84FF4A41CD4625B44B2940BFC8DB43003
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::A323DB0813C4D072957BA6FDA79D9776674CD06B
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___A323DB0813C4D072957BA6FDA79D9776674CD06B_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BE1BDEC0AA74B4DCB079943E70528096CCA985F8
	__StaticArrayInitTypeSizeU3D20_t1548391512  ___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::BF477463CE2F5EF38FC4C644BBBF4DF109E7670A
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::CF0B42666EF5E37EDEA0AB8E173E42C196D03814
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE
	__StaticArrayInitTypeSizeU3D32_t2711125390  ___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=524 <PrivateImplementationDetails>::D693078666967609CB4A2B840921F9E052031847
	__StaticArrayInitTypeSizeU3D524_t4061560205  ___D693078666967609CB4A2B840921F9E052031847_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::E75835D001C843F156FBA01B001DFE1B8029AC17
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___E75835D001C843F156FBA01B001DFE1B8029AC17_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11
	__StaticArrayInitTypeSizeU3D10_t1548194904  ___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::EC83FB16C20052BEE2B4025159BC2ED45C9C70C3
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26;

public:
	inline static int32_t get_offset_of_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0() const { return ___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0() { return &___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0; }
	inline void set_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0 = value;
	}

	inline static int32_t get_offset_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1)); }
	inline int32_t get_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1() const { return ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1; }
	inline int32_t* get_address_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1() { return &___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1; }
	inline void set_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1(int32_t value)
	{
		___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1 = value;
	}

	inline static int32_t get_offset_of_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2() const { return ___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2() { return &___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2; }
	inline void set_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2 = value;
	}

	inline static int32_t get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() const { return ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return &___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline void set_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___1A84029C80CB5518379F199F53FF08A7B764F8FD_3 = value;
	}

	inline static int32_t get_offset_of_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4() const { return ___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4() { return &___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4; }
	inline void set_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4 = value;
	}

	inline static int32_t get_offset_of_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5)); }
	inline __StaticArrayInitTypeSizeU3D14_t3517563372  get_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5() const { return ___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5; }
	inline __StaticArrayInitTypeSizeU3D14_t3517563372 * get_address_of_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5() { return &___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5; }
	inline void set_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5(__StaticArrayInitTypeSizeU3D14_t3517563372  value)
	{
		___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5 = value;
	}

	inline static int32_t get_offset_of_U331D8729F7377B44017C0A2395A582C9CA4163277_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___31D8729F7377B44017C0A2395A582C9CA4163277_6)); }
	inline int32_t get_U331D8729F7377B44017C0A2395A582C9CA4163277_6() const { return ___31D8729F7377B44017C0A2395A582C9CA4163277_6; }
	inline int32_t* get_address_of_U331D8729F7377B44017C0A2395A582C9CA4163277_6() { return &___31D8729F7377B44017C0A2395A582C9CA4163277_6; }
	inline void set_U331D8729F7377B44017C0A2395A582C9CA4163277_6(int32_t value)
	{
		___31D8729F7377B44017C0A2395A582C9CA4163277_6 = value;
	}

	inline static int32_t get_offset_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7() const { return ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7() { return &___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7; }
	inline void set_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7 = value;
	}

	inline static int32_t get_offset_of_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8() const { return ___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8() { return &___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8; }
	inline void set_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8 = value;
	}

	inline static int32_t get_offset_of_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9)); }
	inline __StaticArrayInitTypeSizeU3D48_t1904228656  get_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9() const { return ___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9; }
	inline __StaticArrayInitTypeSizeU3D48_t1904228656 * get_address_of_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9() { return &___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9; }
	inline void set_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9(__StaticArrayInitTypeSizeU3D48_t1904228656  value)
	{
		___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9 = value;
	}

	inline static int32_t get_offset_of_U356DFA5053B3131883637F53219E7D88CCEF35949_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___56DFA5053B3131883637F53219E7D88CCEF35949_10)); }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904  get_U356DFA5053B3131883637F53219E7D88CCEF35949_10() const { return ___56DFA5053B3131883637F53219E7D88CCEF35949_10; }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904 * get_address_of_U356DFA5053B3131883637F53219E7D88CCEF35949_10() { return &___56DFA5053B3131883637F53219E7D88CCEF35949_10; }
	inline void set_U356DFA5053B3131883637F53219E7D88CCEF35949_10(__StaticArrayInitTypeSizeU3D10_t1548194904  value)
	{
		___56DFA5053B3131883637F53219E7D88CCEF35949_10 = value;
	}

	inline static int32_t get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11)); }
	inline __StaticArrayInitTypeSizeU3D9_t3218278899  get_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11() const { return ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11; }
	inline __StaticArrayInitTypeSizeU3D9_t3218278899 * get_address_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11() { return &___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11; }
	inline void set_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11(__StaticArrayInitTypeSizeU3D9_t3218278899  value)
	{
		___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11 = value;
	}

	inline static int32_t get_offset_of_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12)); }
	inline __StaticArrayInitTypeSizeU3D3132_t3825993976  get_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12() const { return ___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12; }
	inline __StaticArrayInitTypeSizeU3D3132_t3825993976 * get_address_of_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12() { return &___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12; }
	inline void set_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12(__StaticArrayInitTypeSizeU3D3132_t3825993976  value)
	{
		___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12 = value;
	}

	inline static int32_t get_offset_of_U3736D39815215889F11249D9958F6ED12D37B9F57_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___736D39815215889F11249D9958F6ED12D37B9F57_13)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U3736D39815215889F11249D9958F6ED12D37B9F57_13() const { return ___736D39815215889F11249D9958F6ED12D37B9F57_13; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U3736D39815215889F11249D9958F6ED12D37B9F57_13() { return &___736D39815215889F11249D9958F6ED12D37B9F57_13; }
	inline void set_U3736D39815215889F11249D9958F6ED12D37B9F57_13(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___736D39815215889F11249D9958F6ED12D37B9F57_13 = value;
	}

	inline static int32_t get_offset_of_U386F4F563FA2C61798AE6238D789139739428463A_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___86F4F563FA2C61798AE6238D789139739428463A_14)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U386F4F563FA2C61798AE6238D789139739428463A_14() const { return ___86F4F563FA2C61798AE6238D789139739428463A_14; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U386F4F563FA2C61798AE6238D789139739428463A_14() { return &___86F4F563FA2C61798AE6238D789139739428463A_14; }
	inline void set_U386F4F563FA2C61798AE6238D789139739428463A_14(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___86F4F563FA2C61798AE6238D789139739428463A_14 = value;
	}

	inline static int32_t get_offset_of_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15() const { return ___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15() { return &___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15; }
	inline void set_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15 = value;
	}

	inline static int32_t get_offset_of_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16() const { return ___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16() { return &___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16; }
	inline void set_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16 = value;
	}

	inline static int32_t get_offset_of_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17() const { return ___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17() { return &___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17; }
	inline void set_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17 = value;
	}

	inline static int32_t get_offset_of_A323DB0813C4D072957BA6FDA79D9776674CD06B_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___A323DB0813C4D072957BA6FDA79D9776674CD06B_18)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_A323DB0813C4D072957BA6FDA79D9776674CD06B_18() const { return ___A323DB0813C4D072957BA6FDA79D9776674CD06B_18; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_A323DB0813C4D072957BA6FDA79D9776674CD06B_18() { return &___A323DB0813C4D072957BA6FDA79D9776674CD06B_18; }
	inline void set_A323DB0813C4D072957BA6FDA79D9776674CD06B_18(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___A323DB0813C4D072957BA6FDA79D9776674CD06B_18 = value;
	}

	inline static int32_t get_offset_of_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19)); }
	inline __StaticArrayInitTypeSizeU3D20_t1548391512  get_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19() const { return ___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19; }
	inline __StaticArrayInitTypeSizeU3D20_t1548391512 * get_address_of_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19() { return &___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19; }
	inline void set_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19(__StaticArrayInitTypeSizeU3D20_t1548391512  value)
	{
		___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19 = value;
	}

	inline static int32_t get_offset_of_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20() const { return ___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20() { return &___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20; }
	inline void set_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20 = value;
	}

	inline static int32_t get_offset_of_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21() const { return ___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21() { return &___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21; }
	inline void set_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21 = value;
	}

	inline static int32_t get_offset_of_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125390  get_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22() const { return ___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125390 * get_address_of_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22() { return &___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22; }
	inline void set_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22(__StaticArrayInitTypeSizeU3D32_t2711125390  value)
	{
		___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22 = value;
	}

	inline static int32_t get_offset_of_D693078666967609CB4A2B840921F9E052031847_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___D693078666967609CB4A2B840921F9E052031847_23)); }
	inline __StaticArrayInitTypeSizeU3D524_t4061560205  get_D693078666967609CB4A2B840921F9E052031847_23() const { return ___D693078666967609CB4A2B840921F9E052031847_23; }
	inline __StaticArrayInitTypeSizeU3D524_t4061560205 * get_address_of_D693078666967609CB4A2B840921F9E052031847_23() { return &___D693078666967609CB4A2B840921F9E052031847_23; }
	inline void set_D693078666967609CB4A2B840921F9E052031847_23(__StaticArrayInitTypeSizeU3D524_t4061560205  value)
	{
		___D693078666967609CB4A2B840921F9E052031847_23 = value;
	}

	inline static int32_t get_offset_of_E75835D001C843F156FBA01B001DFE1B8029AC17_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___E75835D001C843F156FBA01B001DFE1B8029AC17_24)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_E75835D001C843F156FBA01B001DFE1B8029AC17_24() const { return ___E75835D001C843F156FBA01B001DFE1B8029AC17_24; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_E75835D001C843F156FBA01B001DFE1B8029AC17_24() { return &___E75835D001C843F156FBA01B001DFE1B8029AC17_24; }
	inline void set_E75835D001C843F156FBA01B001DFE1B8029AC17_24(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___E75835D001C843F156FBA01B001DFE1B8029AC17_24 = value;
	}

	inline static int32_t get_offset_of_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25)); }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904  get_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25() const { return ___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25; }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904 * get_address_of_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25() { return &___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25; }
	inline void set_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25(__StaticArrayInitTypeSizeU3D10_t1548194904  value)
	{
		___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25 = value;
	}

	inline static int32_t get_offset_of_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26() const { return ___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26() { return &___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26; }
	inline void set_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255363_H
#ifndef DNSQCLASS_T1469259432_H
#define DNSQCLASS_T1469259432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQClass
struct  DnsQClass_t1469259432 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsQClass::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsQClass_t1469259432, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQCLASS_T1469259432_H
#ifndef DNSOPCODE_T4150749904_H
#define DNSOPCODE_T4150749904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsOpCode
struct  DnsOpCode_t4150749904 
{
public:
	// System.Byte Mono.Net.Dns.DnsOpCode::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsOpCode_t4150749904, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSOPCODE_T4150749904_H
#ifndef CONFIDENCEFACTOR_T2516000286_H
#define CONFIDENCEFACTOR_T2516000286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.ConfidenceFactor
struct  ConfidenceFactor_t2516000286 
{
public:
	// System.Int32 Mono.Math.Prime.ConfidenceFactor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConfidenceFactor_t2516000286, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIDENCEFACTOR_T2516000286_H
#ifndef DNSHEADER_T376095124_H
#define DNSHEADER_T376095124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsHeader
struct  DnsHeader_t376095124  : public RuntimeObject
{
public:
	// System.ArraySegment`1<System.Byte> Mono.Net.Dns.DnsHeader::bytes
	ArraySegment_1_t283560987  ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(DnsHeader_t376095124, ___bytes_0)); }
	inline ArraySegment_1_t283560987  get_bytes_0() const { return ___bytes_0; }
	inline ArraySegment_1_t283560987 * get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ArraySegment_1_t283560987  value)
	{
		___bytes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSHEADER_T376095124_H
#ifndef DNSCLASS_T343589385_H
#define DNSCLASS_T343589385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsClass
struct  DnsClass_t343589385 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsClass::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsClass_t343589385, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSCLASS_T343589385_H
#ifndef SYSTEMCERTIFICATEVALIDATOR_T3152079745_H
#define SYSTEMCERTIFICATEVALIDATOR_T3152079745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.SystemCertificateValidator
struct  SystemCertificateValidator_t3152079745  : public RuntimeObject
{
public:

public:
};

struct SystemCertificateValidator_t3152079745_StaticFields
{
public:
	// System.Boolean Mono.Net.Security.SystemCertificateValidator::is_macosx
	bool ___is_macosx_0;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode Mono.Net.Security.SystemCertificateValidator::revocation_mode
	int32_t ___revocation_mode_1;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags Mono.Net.Security.SystemCertificateValidator::s_flags
	int32_t ___s_flags_2;

public:
	inline static int32_t get_offset_of_is_macosx_0() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t3152079745_StaticFields, ___is_macosx_0)); }
	inline bool get_is_macosx_0() const { return ___is_macosx_0; }
	inline bool* get_address_of_is_macosx_0() { return &___is_macosx_0; }
	inline void set_is_macosx_0(bool value)
	{
		___is_macosx_0 = value;
	}

	inline static int32_t get_offset_of_revocation_mode_1() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t3152079745_StaticFields, ___revocation_mode_1)); }
	inline int32_t get_revocation_mode_1() const { return ___revocation_mode_1; }
	inline int32_t* get_address_of_revocation_mode_1() { return &___revocation_mode_1; }
	inline void set_revocation_mode_1(int32_t value)
	{
		___revocation_mode_1 = value;
	}

	inline static int32_t get_offset_of_s_flags_2() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t3152079745_StaticFields, ___s_flags_2)); }
	inline int32_t get_s_flags_2() const { return ___s_flags_2; }
	inline int32_t* get_address_of_s_flags_2() { return &___s_flags_2; }
	inline void set_s_flags_2(int32_t value)
	{
		___s_flags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMCERTIFICATEVALIDATOR_T3152079745_H
#ifndef CFSTRING_T2941785922_H
#define CFSTRING_T2941785922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFString
struct  CFString_t2941785922  : public CFObject_t2662441515
{
public:
	// System.String Mono.Net.CFString::str
	String_t* ___str_1;

public:
	inline static int32_t get_offset_of_str_1() { return static_cast<int32_t>(offsetof(CFString_t2941785922, ___str_1)); }
	inline String_t* get_str_1() const { return ___str_1; }
	inline String_t** get_address_of_str_1() { return &___str_1; }
	inline void set_str_1(String_t* value)
	{
		___str_1 = value;
		Il2CppCodeGenWriteBarrier((&___str_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFSTRING_T2941785922_H
#ifndef CFDICTIONARY_T2669341669_H
#define CFDICTIONARY_T2669341669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFDictionary
struct  CFDictionary_t2669341669  : public CFObject_t2662441515
{
public:

public:
};

struct CFDictionary_t2669341669_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFDictionary::KeyCallbacks
	intptr_t ___KeyCallbacks_1;
	// System.IntPtr Mono.Net.CFDictionary::ValueCallbacks
	intptr_t ___ValueCallbacks_2;

public:
	inline static int32_t get_offset_of_KeyCallbacks_1() { return static_cast<int32_t>(offsetof(CFDictionary_t2669341669_StaticFields, ___KeyCallbacks_1)); }
	inline intptr_t get_KeyCallbacks_1() const { return ___KeyCallbacks_1; }
	inline intptr_t* get_address_of_KeyCallbacks_1() { return &___KeyCallbacks_1; }
	inline void set_KeyCallbacks_1(intptr_t value)
	{
		___KeyCallbacks_1 = value;
	}

	inline static int32_t get_offset_of_ValueCallbacks_2() { return static_cast<int32_t>(offsetof(CFDictionary_t2669341669_StaticFields, ___ValueCallbacks_2)); }
	inline intptr_t get_ValueCallbacks_2() const { return ___ValueCallbacks_2; }
	inline intptr_t* get_address_of_ValueCallbacks_2() { return &___ValueCallbacks_2; }
	inline void set_ValueCallbacks_2(intptr_t value)
	{
		___ValueCallbacks_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFDICTIONARY_T2669341669_H
#ifndef DNSQUESTION_T3960639134_H
#define DNSQUESTION_T3960639134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQuestion
struct  DnsQuestion_t3960639134  : public RuntimeObject
{
public:
	// System.String Mono.Net.Dns.DnsQuestion::name
	String_t* ___name_0;
	// Mono.Net.Dns.DnsQType Mono.Net.Dns.DnsQuestion::type
	uint16_t ___type_1;
	// Mono.Net.Dns.DnsQClass Mono.Net.Dns.DnsQuestion::_class
	uint16_t ____class_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DnsQuestion_t3960639134, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DnsQuestion_t3960639134, ___type_1)); }
	inline uint16_t get_type_1() const { return ___type_1; }
	inline uint16_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint16_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of__class_2() { return static_cast<int32_t>(offsetof(DnsQuestion_t3960639134, ____class_2)); }
	inline uint16_t get__class_2() const { return ____class_2; }
	inline uint16_t* get_address_of__class_2() { return &____class_2; }
	inline void set__class_2(uint16_t value)
	{
		____class_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQUESTION_T3960639134_H
#ifndef CFURL_T920917604_H
#define CFURL_T920917604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFUrl
struct  CFUrl_t920917604  : public CFObject_t2662441515
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFURL_T920917604_H
#ifndef DNSRESOURCERECORD_T3262777473_H
#define DNSRESOURCERECORD_T3262777473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecord
struct  DnsResourceRecord_t3262777473  : public RuntimeObject
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecord::name
	String_t* ___name_0;
	// Mono.Net.Dns.DnsType Mono.Net.Dns.DnsResourceRecord::type
	uint16_t ___type_1;
	// Mono.Net.Dns.DnsClass Mono.Net.Dns.DnsResourceRecord::klass
	uint16_t ___klass_2;
	// System.Int32 Mono.Net.Dns.DnsResourceRecord::ttl
	int32_t ___ttl_3;
	// System.UInt16 Mono.Net.Dns.DnsResourceRecord::rdlength
	uint16_t ___rdlength_4;
	// System.ArraySegment`1<System.Byte> Mono.Net.Dns.DnsResourceRecord::m_rdata
	ArraySegment_1_t283560987  ___m_rdata_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t3262777473, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t3262777473, ___type_1)); }
	inline uint16_t get_type_1() const { return ___type_1; }
	inline uint16_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint16_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_klass_2() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t3262777473, ___klass_2)); }
	inline uint16_t get_klass_2() const { return ___klass_2; }
	inline uint16_t* get_address_of_klass_2() { return &___klass_2; }
	inline void set_klass_2(uint16_t value)
	{
		___klass_2 = value;
	}

	inline static int32_t get_offset_of_ttl_3() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t3262777473, ___ttl_3)); }
	inline int32_t get_ttl_3() const { return ___ttl_3; }
	inline int32_t* get_address_of_ttl_3() { return &___ttl_3; }
	inline void set_ttl_3(int32_t value)
	{
		___ttl_3 = value;
	}

	inline static int32_t get_offset_of_rdlength_4() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t3262777473, ___rdlength_4)); }
	inline uint16_t get_rdlength_4() const { return ___rdlength_4; }
	inline uint16_t* get_address_of_rdlength_4() { return &___rdlength_4; }
	inline void set_rdlength_4(uint16_t value)
	{
		___rdlength_4 = value;
	}

	inline static int32_t get_offset_of_m_rdata_5() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t3262777473, ___m_rdata_5)); }
	inline ArraySegment_1_t283560987  get_m_rdata_5() const { return ___m_rdata_5; }
	inline ArraySegment_1_t283560987 * get_address_of_m_rdata_5() { return &___m_rdata_5; }
	inline void set_m_rdata_5(ArraySegment_1_t283560987  value)
	{
		___m_rdata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORD_T3262777473_H
#ifndef LEGACYSSLSTREAM_T1814096354_H
#define LEGACYSSLSTREAM_T1814096354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.LegacySslStream
struct  LegacySslStream_t1814096354  : public AuthenticatedStream_t3415418016
{
public:
	// Mono.Security.Protocol.Tls.SslStreamBase Mono.Net.Security.Private.LegacySslStream::ssl_stream
	SslStreamBase_t1667413407 * ___ssl_stream_6;
	// Mono.Security.Interface.ICertificateValidator Mono.Net.Security.Private.LegacySslStream::certificateValidator
	RuntimeObject* ___certificateValidator_7;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.Private.LegacySslStream::provider
	MonoTlsProvider_t3152003291 * ___provider_8;

public:
	inline static int32_t get_offset_of_ssl_stream_6() { return static_cast<int32_t>(offsetof(LegacySslStream_t1814096354, ___ssl_stream_6)); }
	inline SslStreamBase_t1667413407 * get_ssl_stream_6() const { return ___ssl_stream_6; }
	inline SslStreamBase_t1667413407 ** get_address_of_ssl_stream_6() { return &___ssl_stream_6; }
	inline void set_ssl_stream_6(SslStreamBase_t1667413407 * value)
	{
		___ssl_stream_6 = value;
		Il2CppCodeGenWriteBarrier((&___ssl_stream_6), value);
	}

	inline static int32_t get_offset_of_certificateValidator_7() { return static_cast<int32_t>(offsetof(LegacySslStream_t1814096354, ___certificateValidator_7)); }
	inline RuntimeObject* get_certificateValidator_7() const { return ___certificateValidator_7; }
	inline RuntimeObject** get_address_of_certificateValidator_7() { return &___certificateValidator_7; }
	inline void set_certificateValidator_7(RuntimeObject* value)
	{
		___certificateValidator_7 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValidator_7), value);
	}

	inline static int32_t get_offset_of_provider_8() { return static_cast<int32_t>(offsetof(LegacySslStream_t1814096354, ___provider_8)); }
	inline MonoTlsProvider_t3152003291 * get_provider_8() const { return ___provider_8; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_provider_8() { return &___provider_8; }
	inline void set_provider_8(MonoTlsProvider_t3152003291 * value)
	{
		___provider_8 = value;
		Il2CppCodeGenWriteBarrier((&___provider_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYSSLSTREAM_T1814096354_H
#ifndef MONOTLSSTREAM_T1980138907_H
#define MONOTLSSTREAM_T1980138907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsStream
struct  MonoTlsStream_t1980138907  : public RuntimeObject
{
public:
	// Mono.Net.Security.IMonoTlsProvider Mono.Net.Security.MonoTlsStream::provider
	RuntimeObject* ___provider_0;
	// System.Net.Sockets.NetworkStream Mono.Net.Security.MonoTlsStream::networkStream
	NetworkStream_t4071955934 * ___networkStream_1;
	// System.Net.HttpWebRequest Mono.Net.Security.MonoTlsStream::request
	HttpWebRequest_t1669436515 * ___request_2;
	// Mono.Net.Security.IMonoSslStream Mono.Net.Security.MonoTlsStream::sslStream
	RuntimeObject* ___sslStream_3;
	// System.Net.WebExceptionStatus Mono.Net.Security.MonoTlsStream::status
	int32_t ___status_4;
	// System.Boolean Mono.Net.Security.MonoTlsStream::<CertificateValidationFailed>k__BackingField
	bool ___U3CCertificateValidationFailedU3Ek__BackingField_5;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MonoTlsStream::settings
	MonoTlsSettings_t3666008581 * ___settings_6;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___provider_0)); }
	inline RuntimeObject* get_provider_0() const { return ___provider_0; }
	inline RuntimeObject** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(RuntimeObject* value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___provider_0), value);
	}

	inline static int32_t get_offset_of_networkStream_1() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___networkStream_1)); }
	inline NetworkStream_t4071955934 * get_networkStream_1() const { return ___networkStream_1; }
	inline NetworkStream_t4071955934 ** get_address_of_networkStream_1() { return &___networkStream_1; }
	inline void set_networkStream_1(NetworkStream_t4071955934 * value)
	{
		___networkStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_1), value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___request_2)); }
	inline HttpWebRequest_t1669436515 * get_request_2() const { return ___request_2; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(HttpWebRequest_t1669436515 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}

	inline static int32_t get_offset_of_sslStream_3() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___sslStream_3)); }
	inline RuntimeObject* get_sslStream_3() const { return ___sslStream_3; }
	inline RuntimeObject** get_address_of_sslStream_3() { return &___sslStream_3; }
	inline void set_sslStream_3(RuntimeObject* value)
	{
		___sslStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___U3CCertificateValidationFailedU3Ek__BackingField_5)); }
	inline bool get_U3CCertificateValidationFailedU3Ek__BackingField_5() const { return ___U3CCertificateValidationFailedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CCertificateValidationFailedU3Ek__BackingField_5() { return &___U3CCertificateValidationFailedU3Ek__BackingField_5; }
	inline void set_U3CCertificateValidationFailedU3Ek__BackingField_5(bool value)
	{
		___U3CCertificateValidationFailedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_settings_6() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___settings_6)); }
	inline MonoTlsSettings_t3666008581 * get_settings_6() const { return ___settings_6; }
	inline MonoTlsSettings_t3666008581 ** get_address_of_settings_6() { return &___settings_6; }
	inline void set_settings_6(MonoTlsSettings_t3666008581 * value)
	{
		___settings_6 = value;
		Il2CppCodeGenWriteBarrier((&___settings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSSTREAM_T1980138907_H
#ifndef SYMMETRICALGORITHM_T4254223087_H
#define SYMMETRICALGORITHM_T4254223087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t4254223087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_t4116647657* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t722666473* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___IVValue_2)); }
	inline ByteU5BU5D_t4116647657* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_t4116647657* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeyValue_3)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t722666473* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t722666473* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T4254223087_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef CFARRAY_T2517955815_H
#define CFARRAY_T2517955815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFArray
struct  CFArray_t2517955815  : public CFObject_t2662441515
{
public:

public:
};

struct CFArray_t2517955815_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFArray::kCFTypeArrayCallbacks
	intptr_t ___kCFTypeArrayCallbacks_1;

public:
	inline static int32_t get_offset_of_kCFTypeArrayCallbacks_1() { return static_cast<int32_t>(offsetof(CFArray_t2517955815_StaticFields, ___kCFTypeArrayCallbacks_1)); }
	inline intptr_t get_kCFTypeArrayCallbacks_1() const { return ___kCFTypeArrayCallbacks_1; }
	inline intptr_t* get_address_of_kCFTypeArrayCallbacks_1() { return &___kCFTypeArrayCallbacks_1; }
	inline void set_kCFTypeArrayCallbacks_1(intptr_t value)
	{
		___kCFTypeArrayCallbacks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFARRAY_T2517955815_H
#ifndef CFNUMBER_T3785561005_H
#define CFNUMBER_T3785561005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNumber
struct  CFNumber_t3785561005  : public CFObject_t2662441515
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFNUMBER_T3785561005_H
#ifndef CFRUNLOOP_T2893954052_H
#define CFRUNLOOP_T2893954052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFRunLoop
struct  CFRunLoop_t2893954052  : public CFObject_t2662441515
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRUNLOOP_T2893954052_H
#ifndef RC4_T2752556436_H
#define RC4_T2752556436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RC4
struct  RC4_t2752556436  : public SymmetricAlgorithm_t4254223087
{
public:

public:
};

struct RC4_t2752556436_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalBlockSizes
	KeySizesU5BU5D_t722666473* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalKeySizes
	KeySizesU5BU5D_t722666473* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(RC4_t2752556436_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_9), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(RC4_t2752556436_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC4_T2752556436_H
#ifndef DNSRESOURCERECORDPTR_T186318047_H
#define DNSRESOURCERECORDPTR_T186318047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordPTR
struct  DnsResourceRecordPTR_t186318047  : public DnsResourceRecord_t3262777473
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecordPTR::dname
	String_t* ___dname_6;

public:
	inline static int32_t get_offset_of_dname_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordPTR_t186318047, ___dname_6)); }
	inline String_t* get_dname_6() const { return ___dname_6; }
	inline String_t** get_address_of_dname_6() { return &___dname_6; }
	inline void set_dname_6(String_t* value)
	{
		___dname_6 = value;
		Il2CppCodeGenWriteBarrier((&___dname_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDPTR_T186318047_H
#ifndef SERVERCERTVALIDATIONCALLBACKWRAPPER_T1850490691_H
#define SERVERCERTVALIDATIONCALLBACKWRAPPER_T1850490691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct  ServerCertValidationCallbackWrapper_t1850490691  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCERTVALIDATIONCALLBACKWRAPPER_T1850490691_H
#ifndef KEYGENERATEDEVENTHANDLER_T3064139578_H
#define KEYGENERATEDEVENTHANDLER_T3064139578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct  KeyGeneratedEventHandler_t3064139578  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATEDEVENTHANDLER_T3064139578_H
#ifndef DNSRESOURCERECORDIPADDRESS_T2288672163_H
#define DNSRESOURCERECORDIPADDRESS_T2288672163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordIPAddress
struct  DnsResourceRecordIPAddress_t2288672163  : public DnsResourceRecord_t3262777473
{
public:
	// System.Net.IPAddress Mono.Net.Dns.DnsResourceRecordIPAddress::address
	IPAddress_t241777590 * ___address_6;

public:
	inline static int32_t get_offset_of_address_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordIPAddress_t2288672163, ___address_6)); }
	inline IPAddress_t241777590 * get_address_6() const { return ___address_6; }
	inline IPAddress_t241777590 ** get_address_of_address_6() { return &___address_6; }
	inline void set_address_6(IPAddress_t241777590 * value)
	{
		___address_6 = value;
		Il2CppCodeGenWriteBarrier((&___address_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDIPADDRESS_T2288672163_H
#ifndef PRIMALITYTEST_T1539325944_H
#define PRIMALITYTEST_T1539325944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.PrimalityTest
struct  PrimalityTest_t1539325944  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMALITYTEST_T1539325944_H
#ifndef DNSRESOURCERECORDCNAME_T586745253_H
#define DNSRESOURCERECORDCNAME_T586745253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordCName
struct  DnsResourceRecordCName_t586745253  : public DnsResourceRecord_t3262777473
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecordCName::cname
	String_t* ___cname_6;

public:
	inline static int32_t get_offset_of_cname_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordCName_t586745253, ___cname_6)); }
	inline String_t* get_cname_6() const { return ___cname_6; }
	inline String_t** get_address_of_cname_6() { return &___cname_6; }
	inline void set_cname_6(String_t* value)
	{
		___cname_6 = value;
		Il2CppCodeGenWriteBarrier((&___cname_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDCNAME_T586745253_H
#ifndef CFPROXYAUTOCONFIGURATIONRESULTCALLBACK_T949084125_H
#define CFPROXYAUTOCONFIGURATIONRESULTCALLBACK_T949084125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/CFProxyAutoConfigurationResultCallback
struct  CFProxyAutoConfigurationResultCallback_t949084125  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXYAUTOCONFIGURATIONRESULTCALLBACK_T949084125_H
#ifndef DNSRESOURCERECORDAAAA_T3668163385_H
#define DNSRESOURCERECORDAAAA_T3668163385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordAAAA
struct  DnsResourceRecordAAAA_t3668163385  : public DnsResourceRecordIPAddress_t2288672163
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDAAAA_T3668163385_H
#ifndef ARC4MANAGED_T2641858452_H
#define ARC4MANAGED_T2641858452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.ARC4Managed
struct  ARC4Managed_t2641858452  : public RC4_t2752556436
{
public:
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::key
	ByteU5BU5D_t4116647657* ___key_11;
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::state
	ByteU5BU5D_t4116647657* ___state_12;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::x
	uint8_t ___x_13;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::y
	uint8_t ___y_14;
	// System.Boolean Mono.Security.Cryptography.ARC4Managed::m_disposed
	bool ___m_disposed_15;

public:
	inline static int32_t get_offset_of_key_11() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___key_11)); }
	inline ByteU5BU5D_t4116647657* get_key_11() const { return ___key_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_key_11() { return &___key_11; }
	inline void set_key_11(ByteU5BU5D_t4116647657* value)
	{
		___key_11 = value;
		Il2CppCodeGenWriteBarrier((&___key_11), value);
	}

	inline static int32_t get_offset_of_state_12() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___state_12)); }
	inline ByteU5BU5D_t4116647657* get_state_12() const { return ___state_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_state_12() { return &___state_12; }
	inline void set_state_12(ByteU5BU5D_t4116647657* value)
	{
		___state_12 = value;
		Il2CppCodeGenWriteBarrier((&___state_12), value);
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___x_13)); }
	inline uint8_t get_x_13() const { return ___x_13; }
	inline uint8_t* get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(uint8_t value)
	{
		___x_13 = value;
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___y_14)); }
	inline uint8_t get_y_14() const { return ___y_14; }
	inline uint8_t* get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(uint8_t value)
	{
		___y_14 = value;
	}

	inline static int32_t get_offset_of_m_disposed_15() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___m_disposed_15)); }
	inline bool get_m_disposed_15() const { return ___m_disposed_15; }
	inline bool* get_address_of_m_disposed_15() { return &___m_disposed_15; }
	inline void set_m_disposed_15(bool value)
	{
		___m_disposed_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARC4MANAGED_T2641858452_H
#ifndef DNSRESOURCERECORDA_T945031563_H
#define DNSRESOURCERECORDA_T945031563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordA
struct  DnsResourceRecordA_t945031563  : public DnsResourceRecordIPAddress_t2288672163
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDA_T945031563_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (X509Stores_t1373936238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[3] = 
{
	X509Stores_t1373936238::get_offset_of__storePath_0(),
	X509Stores_t1373936238::get_offset_of__newFormat_1(),
	X509Stores_t1373936238::get_offset_of__trusted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (AuthorityKeyIdentifierExtension_t1122691429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	AuthorityKeyIdentifierExtension_t1122691429::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (BasicConstraintsExtension_t2462195279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	BasicConstraintsExtension_t2462195279::get_offset_of_cA_3(),
	BasicConstraintsExtension_t2462195279::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (ExtendedKeyUsageExtension_t3929363080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	ExtendedKeyUsageExtension_t3929363080::get_offset_of_keyPurpose_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (GeneralNames_t2702294159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[5] = 
{
	GeneralNames_t2702294159::get_offset_of_rfc822Name_0(),
	GeneralNames_t2702294159::get_offset_of_dnsName_1(),
	GeneralNames_t2702294159::get_offset_of_directoryNames_2(),
	GeneralNames_t2702294159::get_offset_of_uris_3(),
	GeneralNames_t2702294159::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (KeyUsages_t820456313)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[11] = 
{
	KeyUsages_t820456313::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (KeyUsageExtension_t1795615912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	KeyUsageExtension_t1795615912::get_offset_of_kubits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (NetscapeCertTypeExtension_t1524296876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	NetscapeCertTypeExtension_t1524296876::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (CertTypes_t3317701015)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1808[8] = 
{
	CertTypes_t3317701015::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (SubjectAltNameExtension_t1536937677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[1] = 
{
	SubjectAltNameExtension_t1536937677::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (ARC4Managed_t2641858452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[5] = 
{
	ARC4Managed_t2641858452::get_offset_of_key_11(),
	ARC4Managed_t2641858452::get_offset_of_state_12(),
	ARC4Managed_t2641858452::get_offset_of_x_13(),
	ARC4Managed_t2641858452::get_offset_of_y_14(),
	ARC4Managed_t2641858452::get_offset_of_m_disposed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (CryptoConvert_t610933157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (KeyBuilder_t2049230355), -1, sizeof(KeyBuilder_t2049230355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	KeyBuilder_t2049230355_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (KeyPairPersistence_t2094547462), -1, sizeof(KeyPairPersistence_t2094547462_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[9] = 
{
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__userPathExists_0(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__userPath_1(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__machinePathExists_2(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__machinePath_3(),
	KeyPairPersistence_t2094547462::get_offset_of__params_4(),
	KeyPairPersistence_t2094547462::get_offset_of__keyvalue_5(),
	KeyPairPersistence_t2094547462::get_offset_of__filename_6(),
	KeyPairPersistence_t2094547462::get_offset_of__container_7(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of_lockobj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (PKCS1_t1505584677), -1, sizeof(PKCS1_t1505584677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[4] = 
{
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (PKCS8_t696280613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (PrivateKeyInfo_t668027993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[4] = 
{
	PrivateKeyInfo_t668027993::get_offset_of__version_0(),
	PrivateKeyInfo_t668027993::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t668027993::get_offset_of__key_2(),
	PrivateKeyInfo_t668027993::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (EncryptedPrivateKeyInfo_t862116836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[4] = 
{
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (RC4_t2752556436), -1, sizeof(RC4_t2752556436_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1818[2] = 
{
	RC4_t2752556436_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	RC4_t2752556436_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (RSAManaged_t1757093820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[13] = 
{
	RSAManaged_t1757093820::get_offset_of_isCRTpossible_2(),
	RSAManaged_t1757093820::get_offset_of_keyBlinding_3(),
	RSAManaged_t1757093820::get_offset_of_keypairGenerated_4(),
	RSAManaged_t1757093820::get_offset_of_m_disposed_5(),
	RSAManaged_t1757093820::get_offset_of_d_6(),
	RSAManaged_t1757093820::get_offset_of_p_7(),
	RSAManaged_t1757093820::get_offset_of_q_8(),
	RSAManaged_t1757093820::get_offset_of_dp_9(),
	RSAManaged_t1757093820::get_offset_of_dq_10(),
	RSAManaged_t1757093820::get_offset_of_qInv_11(),
	RSAManaged_t1757093820::get_offset_of_n_12(),
	RSAManaged_t1757093820::get_offset_of_e_13(),
	RSAManaged_t1757093820::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (KeyGeneratedEventHandler_t3064139578), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (HMAC_t3689525210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[4] = 
{
	HMAC_t3689525210::get_offset_of_hash_5(),
	HMAC_t3689525210::get_offset_of_hashing_6(),
	HMAC_t3689525210::get_offset_of_innerPad_7(),
	HMAC_t3689525210::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (MD5SHA1_t723838944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[3] = 
{
	MD5SHA1_t723838944::get_offset_of_md5_4(),
	MD5SHA1_t723838944::get_offset_of_sha_5(),
	MD5SHA1_t723838944::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (BigInteger_t2902905090), -1, sizeof(BigInteger_t2902905090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	BigInteger_t2902905090::get_offset_of_length_0(),
	BigInteger_t2902905090::get_offset_of_data_1(),
	BigInteger_t2902905090_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t2902905090_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Sign_t3338384039)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1824[4] = 
{
	Sign_t3338384039::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (ModulusRing_t596511505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[2] = 
{
	ModulusRing_t596511505::get_offset_of_mod_0(),
	ModulusRing_t596511505::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (Kernel_t1402667220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ConfidenceFactor_t2516000286)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[7] = 
{
	ConfidenceFactor_t2516000286::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (PrimalityTest_t1539325944), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (PrimalityTests_t1538473976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (PrimeGeneratorBase_t446028867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (SequentialSearchPrimeGeneratorBase_t2996090509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255363), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[27] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U331D8729F7377B44017C0A2395A582C9CA4163277_6(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U356DFA5053B3131883637F53219E7D88CCEF35949_10(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U3736D39815215889F11249D9958F6ED12D37B9F57_13(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U386F4F563FA2C61798AE6238D789139739428463A_14(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_A323DB0813C4D072957BA6FDA79D9776674CD06B_18(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_D693078666967609CB4A2B840921F9E052031847_23(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_E75835D001C843F156FBA01B001DFE1B8029AC17_24(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (__StaticArrayInitTypeSizeU3D3_t3217885683)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3_t3217885683 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (__StaticArrayInitTypeSizeU3D9_t3218278899)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D9_t3218278899 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (__StaticArrayInitTypeSizeU3D10_t1548194904)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t1548194904 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (__StaticArrayInitTypeSizeU3D14_t3517563372)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D14_t3517563372 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (__StaticArrayInitTypeSizeU3D20_t1548391512)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t1548391512 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2711125390)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2711125390 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (__StaticArrayInitTypeSizeU3D48_t1904228656)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D48_t1904228656 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (__StaticArrayInitTypeSizeU3D64_t3517497836)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_t3517497836 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (__StaticArrayInitTypeSizeU3D524_t4061560205)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D524_t4061560205 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (__StaticArrayInitTypeSizeU3D3132_t3825993976)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3132_t3825993976 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (U3CModuleU3E_t692745528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (SR_t167583544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (MonoPInvokeCallbackAttribute_t2047459750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (CFObject_t2662441515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	CFObject_t2662441515::get_offset_of_U3CHandleU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (CFArray_t2517955815), -1, sizeof(CFArray_t2517955815_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	CFArray_t2517955815_StaticFields::get_offset_of_kCFTypeArrayCallbacks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (CFNumber_t3785561005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (CFRange_t3090757970)+ sizeof (RuntimeObject), sizeof(CFRange_t3090757970 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	CFRange_t3090757970::get_offset_of_Location_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFRange_t3090757970::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (CFStreamClientContext_t3215013463)+ sizeof (RuntimeObject), sizeof(CFStreamClientContext_t3215013463 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1851[5] = 
{
	CFStreamClientContext_t3215013463::get_offset_of_Version_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3215013463::get_offset_of_Info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3215013463::get_offset_of_Retain_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3215013463::get_offset_of_Release_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3215013463::get_offset_of_CopyDescription_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (CFString_t2941785922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	CFString_t2941785922::get_offset_of_str_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (CFDictionary_t2669341669), -1, sizeof(CFDictionary_t2669341669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	CFDictionary_t2669341669_StaticFields::get_offset_of_KeyCallbacks_1(),
	CFDictionary_t2669341669_StaticFields::get_offset_of_ValueCallbacks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (CFUrl_t920917604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (CFRunLoop_t2893954052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (CFProxyType_t1170449121)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[8] = 
{
	CFProxyType_t1170449121::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (CFProxy_t2349940), -1, sizeof(CFProxy_t2349940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1857[14] = 
{
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyAutoConfigurationJavaScriptKey_0(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyAutoConfigurationURLKey_1(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyHostNameKey_2(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyPasswordKey_3(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyPortNumberKey_4(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeKey_5(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyUsernameKey_6(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeAutoConfigurationURL_7(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeAutoConfigurationJavaScript_8(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeFTP_9(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeHTTP_10(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeHTTPS_11(),
	CFProxy_t2349940_StaticFields::get_offset_of_kCFProxyTypeSOCKS_12(),
	CFProxy_t2349940::get_offset_of_settings_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (CFProxySettings_t3399020509), -1, sizeof(CFProxySettings_t3399020509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1858[7] = 
{
	CFProxySettings_t3399020509_StaticFields::get_offset_of_kCFNetworkProxiesHTTPEnable_0(),
	CFProxySettings_t3399020509_StaticFields::get_offset_of_kCFNetworkProxiesHTTPPort_1(),
	CFProxySettings_t3399020509_StaticFields::get_offset_of_kCFNetworkProxiesHTTPProxy_2(),
	CFProxySettings_t3399020509_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigEnable_3(),
	CFProxySettings_t3399020509_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4(),
	CFProxySettings_t3399020509_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigURLString_5(),
	CFProxySettings_t3399020509::get_offset_of_settings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (CFNetwork_t1041854236), -1, sizeof(CFNetwork_t1041854236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1859[3] = 
{
	CFNetwork_t1041854236_StaticFields::get_offset_of_lock_obj_0(),
	CFNetwork_t1041854236_StaticFields::get_offset_of_get_proxy_queue_1(),
	CFNetwork_t1041854236_StaticFields::get_offset_of_proxy_event_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (GetProxyData_t72916098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[5] = 
{
	GetProxyData_t72916098::get_offset_of_script_0(),
	GetProxyData_t72916098::get_offset_of_targetUri_1(),
	GetProxyData_t72916098::get_offset_of_error_2(),
	GetProxyData_t72916098::get_offset_of_result_3(),
	GetProxyData_t72916098::get_offset_of_evt_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (CFProxyAutoConfigurationResultCallback_t949084125), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (CFWebProxy_t1756538142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[2] = 
{
	CFWebProxy_t1756538142::get_offset_of_credentials_0(),
	CFWebProxy_t1756538142::get_offset_of_userSpecified_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (U3CU3Ec__DisplayClass13_0_t2390936616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[2] = 
{
	U3CU3Ec__DisplayClass13_0_t2390936616::get_offset_of_proxies_0(),
	U3CU3Ec__DisplayClass13_0_t2390936616::get_offset_of_runLoop_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (ServerCertValidationCallbackWrapper_t1850490691), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (ChainValidationHelper_t669322361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[8] = 
{
	ChainValidationHelper_t669322361::get_offset_of_sender_0(),
	ChainValidationHelper_t669322361::get_offset_of_settings_1(),
	ChainValidationHelper_t669322361::get_offset_of_provider_2(),
	ChainValidationHelper_t669322361::get_offset_of_certValidationCallback_3(),
	ChainValidationHelper_t669322361::get_offset_of_certSelectionCallback_4(),
	ChainValidationHelper_t669322361::get_offset_of_callbackWrapper_5(),
	ChainValidationHelper_t669322361::get_offset_of_tlsStream_6(),
	ChainValidationHelper_t669322361::get_offset_of_request_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (LegacyTlsProvider_t3797502067), -1, sizeof(LegacyTlsProvider_t3797502067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1868[1] = 
{
	LegacyTlsProvider_t3797502067_StaticFields::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (MonoTlsProviderFactory_t4084560240), -1, sizeof(MonoTlsProviderFactory_t4084560240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1869[4] = 
{
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_locker_0(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_initialized_1(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_defaultProvider_2(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_providerRegistration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (MonoTlsStream_t1980138907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[7] = 
{
	MonoTlsStream_t1980138907::get_offset_of_provider_0(),
	MonoTlsStream_t1980138907::get_offset_of_networkStream_1(),
	MonoTlsStream_t1980138907::get_offset_of_request_2(),
	MonoTlsStream_t1980138907::get_offset_of_sslStream_3(),
	MonoTlsStream_t1980138907::get_offset_of_status_4(),
	MonoTlsStream_t1980138907::get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_5(),
	MonoTlsStream_t1980138907::get_offset_of_settings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (SystemCertificateValidator_t3152079745), -1, sizeof(SystemCertificateValidator_t3152079745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1871[3] = 
{
	SystemCertificateValidator_t3152079745_StaticFields::get_offset_of_is_macosx_0(),
	SystemCertificateValidator_t3152079745_StaticFields::get_offset_of_revocation_mode_1(),
	SystemCertificateValidator_t3152079745_StaticFields::get_offset_of_s_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (CallbackHelpers_t1924962770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (U3CU3Ec__DisplayClass5_0_t2247204716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	U3CU3Ec__DisplayClass5_0_t2247204716::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U3CU3Ec__DisplayClass8_0_t3821182828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[1] = 
{
	U3CU3Ec__DisplayClass8_0_t3821182828::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (LegacySslStream_t1814096354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[3] = 
{
	LegacySslStream_t1814096354::get_offset_of_ssl_stream_6(),
	LegacySslStream_t1814096354::get_offset_of_certificateValidator_7(),
	LegacySslStream_t1814096354::get_offset_of_provider_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (U3CU3Ec__DisplayClass57_0_t3818911613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[3] = 
{
	U3CU3Ec__DisplayClass57_0_t3818911613::get_offset_of_clientCertificates_0(),
	U3CU3Ec__DisplayClass57_0_t3818911613::get_offset_of_targetHost_1(),
	U3CU3Ec__DisplayClass57_0_t3818911613::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (MonoSslStreamImpl_t1585747555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	MonoSslStreamImpl_t1585747555::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (MonoSslStreamWrapper_t1708650488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	MonoSslStreamWrapper_t1708650488::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (MonoTlsProviderWrapper_t803812000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[1] = 
{
	MonoTlsProviderWrapper_t803812000::get_offset_of_provider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (DnsClass_t343589385)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[9] = 
{
	DnsClass_t343589385::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (DnsHeader_t376095124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[1] = 
{
	DnsHeader_t376095124::get_offset_of_bytes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (DnsOpCode_t4150749904)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[6] = 
{
	DnsOpCode_t4150749904::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (DnsPacket_t1927600022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[3] = 
{
	DnsPacket_t1927600022::get_offset_of_packet_0(),
	DnsPacket_t1927600022::get_offset_of_position_1(),
	DnsPacket_t1927600022::get_offset_of_header_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (DnsQClass_t1469259432)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[11] = 
{
	DnsQClass_t1469259432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (DnsQType_t3699027829)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[71] = 
{
	DnsQType_t3699027829::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (DnsQuery_t4288736869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (DnsQuestion_t3960639134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[3] = 
{
	DnsQuestion_t3960639134::get_offset_of_name_0(),
	DnsQuestion_t3960639134::get_offset_of_type_1(),
	DnsQuestion_t3960639134::get_offset_of__class_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (DnsRCode_t1462348096)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1888[20] = 
{
	DnsRCode_t1462348096::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (DnsResourceRecord_t3262777473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[6] = 
{
	DnsResourceRecord_t3262777473::get_offset_of_name_0(),
	DnsResourceRecord_t3262777473::get_offset_of_type_1(),
	DnsResourceRecord_t3262777473::get_offset_of_klass_2(),
	DnsResourceRecord_t3262777473::get_offset_of_ttl_3(),
	DnsResourceRecord_t3262777473::get_offset_of_rdlength_4(),
	DnsResourceRecord_t3262777473::get_offset_of_m_rdata_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (DnsResourceRecordA_t945031563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (DnsResourceRecordAAAA_t3668163385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (DnsResourceRecordCName_t586745253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[1] = 
{
	DnsResourceRecordCName_t586745253::get_offset_of_cname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (DnsResourceRecordIPAddress_t2288672163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[1] = 
{
	DnsResourceRecordIPAddress_t2288672163::get_offset_of_address_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (DnsResourceRecordPTR_t186318047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	DnsResourceRecordPTR_t186318047::get_offset_of_dname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (DnsResponse_t3976690788), -1, sizeof(DnsResponse_t3976690788_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1895[7] = 
{
	DnsResponse_t3976690788_StaticFields::get_offset_of_EmptyRR_3(),
	DnsResponse_t3976690788_StaticFields::get_offset_of_EmptyQS_4(),
	DnsResponse_t3976690788::get_offset_of_question_5(),
	DnsResponse_t3976690788::get_offset_of_answer_6(),
	DnsResponse_t3976690788::get_offset_of_authority_7(),
	DnsResponse_t3976690788::get_offset_of_additional_8(),
	DnsResponse_t3976690788::get_offset_of_offset_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (DnsType_t1288195022)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[70] = 
{
	DnsType_t1288195022::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (DnsUtil_t2851216272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ResolverError_t779281070)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[10] = 
{
	ResolverError_t779281070::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (SimpleResolver_t688826539), -1, sizeof(SimpleResolver_t688826539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1899[8] = 
{
	SimpleResolver_t688826539_StaticFields::get_offset_of_EmptyStrings_0(),
	SimpleResolver_t688826539_StaticFields::get_offset_of_EmptyAddresses_1(),
	SimpleResolver_t688826539::get_offset_of_endpoints_2(),
	SimpleResolver_t688826539::get_offset_of_client_3(),
	SimpleResolver_t688826539::get_offset_of_queries_4(),
	SimpleResolver_t688826539::get_offset_of_receive_cb_5(),
	SimpleResolver_t688826539::get_offset_of_timeout_cb_6(),
	SimpleResolver_t688826539::get_offset_of_disposed_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
