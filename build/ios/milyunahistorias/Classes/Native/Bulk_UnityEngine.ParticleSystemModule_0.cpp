﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;

struct AnimationCurve_t3046754366_marshaled_pinvoke;
struct AnimationCurve_t3046754366;;
struct AnimationCurve_t3046754366_marshaled_pinvoke;;
extern RuntimeClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_t1067599125_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct AnimationCurve_t3046754366_marshaled_com;
struct AnimationCurve_t3046754366_marshaled_com;;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_Evaluate_m690582141_MetadataUsageId;



#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef MAINMODULE_T2320046318_H
#define MAINMODULE_T2320046318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t2320046318 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t2320046318, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2320046318_marshaled_pinvoke
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2320046318_marshaled_com
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T2320046318_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef PARTICLESYSTEMCURVEMODE_T3859704052_H
#define PARTICLESYSTEMCURVEMODE_T3859704052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_t3859704052 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_t3859704052, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMCURVEMODE_T3859704052_H
#ifndef PARTICLESYSTEMSTOPBEHAVIOR_T2808326180_H
#define PARTICLESYSTEMSTOPBEHAVIOR_T2808326180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemStopBehavior
struct  ParticleSystemStopBehavior_t2808326180 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_t2808326180, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSTOPBEHAVIOR_T2808326180_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MINMAXCURVE_T1067599125_H
#define MINMAXCURVE_T1067599125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxCurve
struct  MinMaxCurve_t1067599125 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_t3046754366 * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_t3046754366 * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_CurveMin_2)); }
	inline AnimationCurve_t3046754366 * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_t3046754366 * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMin_2), value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_CurveMax_3)); }
	inline AnimationCurve_t3046754366 * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_t3046754366 * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMax_3), value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t1067599125_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3046754366_marshaled_pinvoke ___m_CurveMin_2;
	AnimationCurve_t3046754366_marshaled_pinvoke ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t1067599125_marshaled_com
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3046754366_marshaled_com* ___m_CurveMin_2;
	AnimationCurve_t3046754366_marshaled_com* ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
#endif // MINMAXCURVE_T1067599125_H
#ifndef PARTICLESYSTEM_T1800779281_H
#define PARTICLESYSTEM_T1800779281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t1800779281  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T1800779281_H

extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(const AnimationCurve_t3046754366_marshaled_pinvoke& marshaled, AnimationCurve_t3046754366& unmarshaled);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(AnimationCurve_t3046754366_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_com& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com_back(const AnimationCurve_t3046754366_marshaled_com& marshaled, AnimationCurve_t3046754366& unmarshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com_cleanup(AnimationCurve_t3046754366_marshaled_com& marshaled);


// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void MainModule__ctor_m1745438521 (MainModule_t2320046318 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m1921398215 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::GetDuration(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetDuration_m471811572 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_duration_m2362826759 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/MainModule::GetLoop(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR bool MainModule_GetLoop_m1744387122 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
extern "C" IL2CPP_METHOD_ATTR bool MainModule_get_loop_m2299304680 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartDelay(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartDelay_m1220620588 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startDelay()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startDelay_m23340732 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationCurve__ctor_m3000526466 (AnimationCurve_t3046754366 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::Evaluate(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_Evaluate_m690582141 (MinMaxCurve_t1067599125 * __this, float ___time0, float ___lerpFactor1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::Evaluate(System.Single)
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_Evaluate_m1494741494 (MinMaxCurve_t1067599125 * __this, float ___time0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Lerp_m1004423579 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C" IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m2125563588 (AnimationCurve_t3046754366 * __this, float p0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.ParticleSystem::get_time()
extern "C" IL2CPP_METHOD_ATTR float ParticleSystem_get_time_m3802336174 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystem_get_time_m3802336174_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_time_m3802336174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_time_m3802336174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_time()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
extern "C" IL2CPP_METHOD_ATTR uint32_t ParticleSystem_get_randomSeed_m3828058209 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	typedef uint32_t (*ParticleSystem_get_randomSeed_m3828058209_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_randomSeed_m3828058209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_randomSeed_m3828058209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_randomSeed()");
	uint32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_set_randomSeed_m1284801121 (ParticleSystem_t1800779281 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_randomSeed_m1284801121_ftn) (ParticleSystem_t1800779281 *, uint32_t);
	static ParticleSystem_set_randomSeed_m1284801121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_randomSeed_m1284801121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_set_useAutoRandomSeed_m1036718603 (ParticleSystem_t1800779281 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_useAutoRandomSeed_m1036718603_ftn) (ParticleSystem_t1800779281 *, bool);
	static ParticleSystem_set_useAutoRandomSeed_m1036718603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_useAutoRandomSeed_m1036718603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C" IL2CPP_METHOD_ATTR MainModule_t2320046318  ParticleSystem_get_main_m3006917117 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t2320046318  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m1745438521((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t2320046318  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m1921398215 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Simulate_m1921398215_ftn) (ParticleSystem_t1800779281 *, float, bool, bool, bool);
	static ParticleSystem_Simulate_m1921398215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Simulate_m1921398215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___t0, ___withChildren1, ___restart2, ___fixedTimeStep3);
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m2186877927 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, bool ___restart2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		bool L_2 = ___restart2;
		bool L_3 = V_0;
		ParticleSystem_Simulate_m1921398215(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m3396581118 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Stop_m3396581118_ftn) (ParticleSystem_t1800779281 *, bool, int32_t);
	static ParticleSystem_Stop_m3396581118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Stop_m3396581118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)");
	_il2cpp_icall_func(__this, ___withChildren0, ___stopBehavior1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_pinvoke(const MainModule_t2320046318& unmarshaled, MainModule_t2320046318_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception,NULL);
}
extern "C" void MainModule_t2320046318_marshal_pinvoke_back(const MainModule_t2320046318_marshaled_pinvoke& marshaled, MainModule_t2320046318& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception,NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_pinvoke_cleanup(MainModule_t2320046318_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_com(const MainModule_t2320046318& unmarshaled, MainModule_t2320046318_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception,NULL);
}
extern "C" void MainModule_t2320046318_marshal_com_back(const MainModule_t2320046318_marshaled_com& marshaled, MainModule_t2320046318& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception,NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_com_cleanup(MainModule_t2320046318_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void MainModule__ctor_m1745438521 (MainModule_t2320046318 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m1745438521_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule__ctor_m1745438521(_thisAdjusted, ___particleSystem0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_duration_m2362826759 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = MainModule_GetDuration_m471811572(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float MainModule_get_duration_m2362826759_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_duration_m2362826759(_thisAdjusted, method);
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
extern "C" IL2CPP_METHOD_ATTR bool MainModule_get_loop_m2299304680 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = MainModule_GetLoop_m1744387122(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool MainModule_get_loop_m2299304680_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_loop_m2299304680(_thisAdjusted, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startDelay()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startDelay_m23340732 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t1067599125  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MinMaxCurve_t1067599125 ));
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartDelay_m1220620588(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t1067599125  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t1067599125  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t1067599125  MainModule_get_startDelay_m23340732_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startDelay_m23340732(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::GetDuration(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetDuration_m471811572 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method)
{
	typedef float (*MainModule_GetDuration_m471811572_ftn) (ParticleSystem_t1800779281 *);
	static MainModule_GetDuration_m471811572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetDuration_m471811572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetDuration(UnityEngine.ParticleSystem)");
	float retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::GetLoop(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR bool MainModule_GetLoop_m1744387122 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method)
{
	typedef bool (*MainModule_GetLoop_m1744387122_ftn) (ParticleSystem_t1800779281 *);
	static MainModule_GetLoop_m1744387122_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetLoop_m1744387122_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetLoop(UnityEngine.ParticleSystem)");
	bool retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartDelay(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartDelay_m1220620588 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartDelay_m1220620588_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static MainModule_GetStartDelay_m1220620588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartDelay_m1220620588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartDelay(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke(const MinMaxCurve_t1067599125& unmarshaled, MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3046754366_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3046754366_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_back(const MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled, MinMaxCurve_t1067599125& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t1067599125_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t3046754366_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t3046754366_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_cleanup(MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t3046754366_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t3046754366_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_com(const MinMaxCurve_t1067599125& unmarshaled, MinMaxCurve_t1067599125_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3046754366_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3046754366_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t1067599125_marshal_com_back(const MinMaxCurve_t1067599125_marshaled_com& marshaled, MinMaxCurve_t1067599125& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t3046754366_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t3046754366_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_com_cleanup(MinMaxCurve_t1067599125_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t3046754366_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t3046754366_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::Evaluate(System.Single)
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_Evaluate_m1494741494 (MinMaxCurve_t1067599125 * __this, float ___time0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___time0;
		float L_1 = MinMaxCurve_Evaluate_m690582141(__this, L_0, (1.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float MinMaxCurve_Evaluate_m1494741494_AdjustorThunk (RuntimeObject * __this, float ___time0, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	return MinMaxCurve_Evaluate_m1494741494(_thisAdjusted, ___time0, method);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::Evaluate(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_Evaluate_m690582141 (MinMaxCurve_t1067599125 * __this, float ___time0, float ___lerpFactor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_Evaluate_m690582141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = ___time0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___time0 = L_1;
		float L_2 = ___lerpFactor1;
		float L_3 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_2, (0.0f), (1.0f), /*hidden argument*/NULL);
		___lerpFactor1 = L_3;
		int32_t L_4 = __this->get_m_Mode_0();
		if (L_4)
		{
			goto IL_003c;
		}
	}
	{
		float L_5 = __this->get_m_ConstantMax_5();
		V_0 = L_5;
		goto IL_00a7;
	}

IL_003c:
	{
		int32_t L_6 = __this->get_m_Mode_0();
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_0060;
		}
	}
	{
		float L_7 = __this->get_m_ConstantMin_4();
		float L_8 = __this->get_m_ConstantMax_5();
		float L_9 = ___lerpFactor1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_00a7;
	}

IL_0060:
	{
		AnimationCurve_t3046754366 * L_11 = __this->get_m_CurveMax_3();
		float L_12 = ___time0;
		NullCheck(L_11);
		float L_13 = AnimationCurve_Evaluate_m2125563588(L_11, L_12, /*hidden argument*/NULL);
		float L_14 = __this->get_m_CurveMultiplier_1();
		V_1 = ((float)il2cpp_codegen_multiply((float)L_13, (float)L_14));
		int32_t L_15 = __this->get_m_Mode_0();
		if ((!(((uint32_t)L_15) == ((uint32_t)2))))
		{
			goto IL_00a0;
		}
	}
	{
		AnimationCurve_t3046754366 * L_16 = __this->get_m_CurveMin_2();
		float L_17 = ___time0;
		NullCheck(L_16);
		float L_18 = AnimationCurve_Evaluate_m2125563588(L_16, L_17, /*hidden argument*/NULL);
		float L_19 = __this->get_m_CurveMultiplier_1();
		float L_20 = V_1;
		float L_21 = ___lerpFactor1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_18, (float)L_19)), L_20, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_00a7;
	}

IL_00a0:
	{
		float L_23 = V_1;
		V_0 = L_23;
		goto IL_00a7;
	}

IL_00a7:
	{
		float L_24 = V_0;
		return L_24;
	}
}
extern "C"  float MinMaxCurve_Evaluate_m690582141_AdjustorThunk (RuntimeObject * __this, float ___time0, float ___lerpFactor1, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	return MinMaxCurve_Evaluate_m690582141(_thisAdjusted, ___time0, ___lerpFactor1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
