﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t2253157129;
// System.String
struct String_t;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t3163629820;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t508516997;
// UnityEngine.Timeline.ITimeControl
struct ITimeControl_t1934324661;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement>
struct IntervalTree_1_t478208184;
// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement>
struct List_1_t3540616959;
// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback>
struct List_1_t2313598679;
// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Timeline.TimelinePlayable/ConnectionCache>
struct Dictionary_2_t3971549929;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Object
struct Object_t631007953;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t2828708245;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t829358056;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t2478225950;
// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_t3872729528;
// UnityEngine.Timeline.AudioTrack
struct AudioTrack_t1549411460;
// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct List_1_t3454752663;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerator_1_t3261278713;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding>
struct IEnumerator_1_t786831177;
// UnityEngine.Timeline.TimelineAsset
struct TimelineAsset_t3776684190;
// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t2757649500;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t3219022681;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_t4000433264;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t140784555;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_t52518008;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerable_1_t1808561134;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t907029268;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_t3950300692;
// System.Collections.Generic.HashSet`1<UnityEngine.Playables.PlayableDirector>
struct HashSet_1_t3368433767;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t3436254912;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_t3211687919;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t445758600;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t3848515759;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset>
struct List_1_t5815691;
// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct EditorSettings_t2594721758;
// UnityEngine.Timeline.ActivationMixerPlayable
struct ActivationMixerPlayable_t2212523045;
// UnityEngine.AvatarMask
struct AvatarMask_t1182482518;

struct Object_t631007953_marshaled_com;



#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745552_H
#define U3CMODULEU3E_T692745552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745552 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745552_H
#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef TIMELINECLIPCAPSEXTENSIONS_T3963340085_H
#define TIMELINECLIPCAPSEXTENSIONS_T3963340085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClipCapsExtensions
struct  TimelineClipCapsExtensions_t3963340085  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECLIPCAPSEXTENSIONS_T3963340085_H
#ifndef EXTRAPOLATION_T625958692_H
#define EXTRAPOLATION_T625958692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.Extrapolation
struct  Extrapolation_t625958692  : public RuntimeObject
{
public:

public:
};

struct Extrapolation_t625958692_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.Extrapolation::kMinExtrapolationTime
	double ___kMinExtrapolationTime_0;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.Extrapolation::<>f__am$cache0
	Comparison_1_t2253157129 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_kMinExtrapolationTime_0() { return static_cast<int32_t>(offsetof(Extrapolation_t625958692_StaticFields, ___kMinExtrapolationTime_0)); }
	inline double get_kMinExtrapolationTime_0() const { return ___kMinExtrapolationTime_0; }
	inline double* get_address_of_kMinExtrapolationTime_0() { return &___kMinExtrapolationTime_0; }
	inline void set_kMinExtrapolationTime_0(double value)
	{
		___kMinExtrapolationTime_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Extrapolation_t625958692_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Comparison_1_t2253157129 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Comparison_1_t2253157129 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Comparison_1_t2253157129 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRAPOLATION_T625958692_H
#ifndef TIMELINECREATEUTILITIES_T4099873628_H
#define TIMELINECREATEUTILITIES_T4099873628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities
struct  TimelineCreateUtilities_t4099873628  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECREATEUTILITIES_T4099873628_H
#ifndef U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T930638780_H
#define U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T930638780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0
struct  U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0::prefix
	String_t* ___prefix_0;
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0::newName
	String_t* ___newName_1;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_newName_1() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780, ___newName_1)); }
	inline String_t* get_newName_1() const { return ___newName_1; }
	inline String_t** get_address_of_newName_1() { return &___newName_1; }
	inline void set_newName_1(String_t* value)
	{
		___newName_1 = value;
		Il2CppCodeGenWriteBarrier((&___newName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T930638780_H
#ifndef TIMELINEUNDO_T898409293_H
#define TIMELINEUNDO_T898409293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineUndo
struct  TimelineUndo_t898409293  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEUNDO_T898409293_H
#ifndef TIMEUTILITY_T877350212_H
#define TIMEUTILITY_T877350212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeUtility
struct  TimeUtility_t877350212  : public RuntimeObject
{
public:

public:
};

struct TimeUtility_t877350212_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.TimeUtility::kTimeEpsilon
	double ___kTimeEpsilon_0;
	// System.Double UnityEngine.Timeline.TimeUtility::kFrameRateEpsilon
	double ___kFrameRateEpsilon_1;
	// System.Double UnityEngine.Timeline.TimeUtility::k_MaxTimelineDurationInSeconds
	double ___k_MaxTimelineDurationInSeconds_2;

public:
	inline static int32_t get_offset_of_kTimeEpsilon_0() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___kTimeEpsilon_0)); }
	inline double get_kTimeEpsilon_0() const { return ___kTimeEpsilon_0; }
	inline double* get_address_of_kTimeEpsilon_0() { return &___kTimeEpsilon_0; }
	inline void set_kTimeEpsilon_0(double value)
	{
		___kTimeEpsilon_0 = value;
	}

	inline static int32_t get_offset_of_kFrameRateEpsilon_1() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___kFrameRateEpsilon_1)); }
	inline double get_kFrameRateEpsilon_1() const { return ___kFrameRateEpsilon_1; }
	inline double* get_address_of_kFrameRateEpsilon_1() { return &___kFrameRateEpsilon_1; }
	inline void set_kFrameRateEpsilon_1(double value)
	{
		___kFrameRateEpsilon_1 = value;
	}

	inline static int32_t get_offset_of_k_MaxTimelineDurationInSeconds_2() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___k_MaxTimelineDurationInSeconds_2)); }
	inline double get_k_MaxTimelineDurationInSeconds_2() const { return ___k_MaxTimelineDurationInSeconds_2; }
	inline double* get_address_of_k_MaxTimelineDurationInSeconds_2() { return &___k_MaxTimelineDurationInSeconds_2; }
	inline void set_k_MaxTimelineDurationInSeconds_2(double value)
	{
		___k_MaxTimelineDurationInSeconds_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILITY_T877350212_H
#ifndef EDITORSETTINGS_T2594721758_H
#define EDITORSETTINGS_T2594721758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct  EditorSettings_t2594721758  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::m_Framerate
	float ___m_Framerate_3;

public:
	inline static int32_t get_offset_of_m_Framerate_3() { return static_cast<int32_t>(offsetof(EditorSettings_t2594721758, ___m_Framerate_3)); }
	inline float get_m_Framerate_3() const { return ___m_Framerate_3; }
	inline float* get_address_of_m_Framerate_3() { return &___m_Framerate_3; }
	inline void set_m_Framerate_3(float value)
	{
		___m_Framerate_3 = value;
	}
};

struct EditorSettings_t2594721758_StaticFields
{
public:
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kMinFps
	float ___kMinFps_0;
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kMaxFps
	float ___kMaxFps_1;
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kDefaultFps
	float ___kDefaultFps_2;

public:
	inline static int32_t get_offset_of_kMinFps_0() { return static_cast<int32_t>(offsetof(EditorSettings_t2594721758_StaticFields, ___kMinFps_0)); }
	inline float get_kMinFps_0() const { return ___kMinFps_0; }
	inline float* get_address_of_kMinFps_0() { return &___kMinFps_0; }
	inline void set_kMinFps_0(float value)
	{
		___kMinFps_0 = value;
	}

	inline static int32_t get_offset_of_kMaxFps_1() { return static_cast<int32_t>(offsetof(EditorSettings_t2594721758_StaticFields, ___kMaxFps_1)); }
	inline float get_kMaxFps_1() const { return ___kMaxFps_1; }
	inline float* get_address_of_kMaxFps_1() { return &___kMaxFps_1; }
	inline void set_kMaxFps_1(float value)
	{
		___kMaxFps_1 = value;
	}

	inline static int32_t get_offset_of_kDefaultFps_2() { return static_cast<int32_t>(offsetof(EditorSettings_t2594721758_StaticFields, ___kDefaultFps_2)); }
	inline float get_kDefaultFps_2() const { return ___kDefaultFps_2; }
	inline float* get_address_of_kDefaultFps_2() { return &___kDefaultFps_2; }
	inline void set_kDefaultFps_2(float value)
	{
		___kDefaultFps_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORSETTINGS_T2594721758_H
#ifndef WEIGHTUTILITY_T2276937658_H
#define WEIGHTUTILITY_T2276937658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.WeightUtility
struct  WeightUtility_t2276937658  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEIGHTUTILITY_T2276937658_H
#ifndef HASHUTILITY_T2883916303_H
#define HASHUTILITY_T2883916303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.HashUtility
struct  HashUtility_t2883916303  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHUTILITY_T2883916303_H
#ifndef REMOTESETTINGS_T1718627291_H
#define REMOTESETTINGS_T1718627291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t1718627291  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t1718627291_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t1027848393 * ___Updated_0;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t1027848393 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t1027848393 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t1027848393 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T1718627291_H
#ifndef U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3018919025_H
#define U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3018919025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0
struct  U3CGetControlableScriptsU3Ec__Iterator0_t3018919025  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::root
	GameObject_t1113636619 * ___root_0;
	// UnityEngine.MonoBehaviour[] UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar0
	MonoBehaviourU5BU5D_t2007329276* ___U24locvar0_1;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::<script>__1
	MonoBehaviour_t3962482529 * ___U3CscriptU3E__1_3;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$current
	MonoBehaviour_t3962482529 * ___U24current_4;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___root_0)); }
	inline GameObject_t1113636619 * get_root_0() const { return ___root_0; }
	inline GameObject_t1113636619 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GameObject_t1113636619 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24locvar0_1)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CscriptU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U3CscriptU3E__1_3)); }
	inline MonoBehaviour_t3962482529 * get_U3CscriptU3E__1_3() const { return ___U3CscriptU3E__1_3; }
	inline MonoBehaviour_t3962482529 ** get_address_of_U3CscriptU3E__1_3() { return &___U3CscriptU3E__1_3; }
	inline void set_U3CscriptU3E__1_3(MonoBehaviour_t3962482529 * value)
	{
		___U3CscriptU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscriptU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24current_4)); }
	inline MonoBehaviour_t3962482529 * get_U24current_4() const { return ___U24current_4; }
	inline MonoBehaviour_t3962482529 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(MonoBehaviour_t3962482529 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3018919025_H
#ifndef ANALYTICSSESSIONINFO_T2322308579_H
#define ANALYTICSSESSIONINFO_T2322308579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_t2322308579  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_t2322308579_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t3163629820 * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_t2322308579_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t3163629820 * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t3163629820 ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t3163629820 * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_T2322308579_H
#ifndef UNITYWEBREQUESTMULTIMEDIA_T3655636800_H
#define UNITYWEBREQUESTMULTIMEDIA_T3655636800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequestMultimedia
struct  UnityWebRequestMultimedia_t3655636800  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTMULTIMEDIA_T3655636800_H
#ifndef TIMELINEMARKER_T2195886951_H
#define TIMELINEMARKER_T2195886951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineMarker
struct  TimelineMarker_t2195886951  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineMarker::m_Key
	String_t* ___m_Key_0;
	// System.Double UnityEngine.Timeline.TimelineMarker::m_Time
	double ___m_Time_1;

public:
	inline static int32_t get_offset_of_m_Key_0() { return static_cast<int32_t>(offsetof(TimelineMarker_t2195886951, ___m_Key_0)); }
	inline String_t* get_m_Key_0() const { return ___m_Key_0; }
	inline String_t** get_address_of_m_Key_0() { return &___m_Key_0; }
	inline void set_m_Key_0(String_t* value)
	{
		___m_Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Key_0), value);
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(TimelineMarker_t2195886951, ___m_Time_1)); }
	inline double get_m_Time_1() const { return ___m_Time_1; }
	inline double* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(double value)
	{
		___m_Time_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEMARKER_T2195886951_H
#ifndef PLAYABLEBEHAVIOUR_T4203540982_H
#define PLAYABLEBEHAVIOUR_T4203540982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t4203540982  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T4203540982_H
#ifndef RUNTIMEELEMENT_T2068542217_H
#define RUNTIMEELEMENT_T2068542217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeElement
struct  RuntimeElement_t2068542217  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Timeline.RuntimeElement::<intervalBit>k__BackingField
	int32_t ___U3CintervalBitU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CintervalBitU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RuntimeElement_t2068542217, ___U3CintervalBitU3Ek__BackingField_0)); }
	inline int32_t get_U3CintervalBitU3Ek__BackingField_0() const { return ___U3CintervalBitU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CintervalBitU3Ek__BackingField_0() { return &___U3CintervalBitU3Ek__BackingField_0; }
	inline void set_U3CintervalBitU3Ek__BackingField_0(int32_t value)
	{
		___U3CintervalBitU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEELEMENT_T2068542217_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef DIRECTORCONTROLPLAYABLE_T2642348650_H
#define DIRECTORCONTROLPLAYABLE_T2642348650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DirectorControlPlayable
struct  DirectorControlPlayable_t2642348650  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.Playables.PlayableDirector UnityEngine.Timeline.DirectorControlPlayable::director
	PlayableDirector_t508516997 * ___director_0;
	// System.Boolean UnityEngine.Timeline.DirectorControlPlayable::m_SyncTime
	bool ___m_SyncTime_1;

public:
	inline static int32_t get_offset_of_director_0() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t2642348650, ___director_0)); }
	inline PlayableDirector_t508516997 * get_director_0() const { return ___director_0; }
	inline PlayableDirector_t508516997 ** get_address_of_director_0() { return &___director_0; }
	inline void set_director_0(PlayableDirector_t508516997 * value)
	{
		___director_0 = value;
		Il2CppCodeGenWriteBarrier((&___director_0), value);
	}

	inline static int32_t get_offset_of_m_SyncTime_1() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t2642348650, ___m_SyncTime_1)); }
	inline bool get_m_SyncTime_1() const { return ___m_SyncTime_1; }
	inline bool* get_address_of_m_SyncTime_1() { return &___m_SyncTime_1; }
	inline void set_m_SyncTime_1(bool value)
	{
		___m_SyncTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORCONTROLPLAYABLE_T2642348650_H
#ifndef PROPERTYNAME_T3749835189_H
#define PROPERTYNAME_T3749835189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t3749835189 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t3749835189, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T3749835189_H
#ifndef PREFABCONTROLPLAYABLE_T3058657425_H
#define PREFABCONTROLPLAYABLE_T3058657425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PrefabControlPlayable
struct  PrefabControlPlayable_t3058657425  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.PrefabControlPlayable::m_Instance
	GameObject_t1113636619 * ___m_Instance_0;

public:
	inline static int32_t get_offset_of_m_Instance_0() { return static_cast<int32_t>(offsetof(PrefabControlPlayable_t3058657425, ___m_Instance_0)); }
	inline GameObject_t1113636619 * get_m_Instance_0() const { return ___m_Instance_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Instance_0() { return &___m_Instance_0; }
	inline void set_m_Instance_0(GameObject_t1113636619 * value)
	{
		___m_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABCONTROLPLAYABLE_T3058657425_H
#ifndef RUNTIMECLIPBASE_T197358283_H
#define RUNTIMECLIPBASE_T197358283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClipBase
struct  RuntimeClipBase_t197358283  : public RuntimeElement_t2068542217
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIPBASE_T197358283_H
#ifndef TIMECONTROLPLAYABLE_T1010398267_H
#define TIMECONTROLPLAYABLE_T1010398267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeControlPlayable
struct  TimeControlPlayable_t1010398267  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.Timeline.ITimeControl UnityEngine.Timeline.TimeControlPlayable::m_timeControl
	RuntimeObject* ___m_timeControl_0;

public:
	inline static int32_t get_offset_of_m_timeControl_0() { return static_cast<int32_t>(offsetof(TimeControlPlayable_t1010398267, ___m_timeControl_0)); }
	inline RuntimeObject* get_m_timeControl_0() const { return ___m_timeControl_0; }
	inline RuntimeObject** get_address_of_m_timeControl_0() { return &___m_timeControl_0; }
	inline void set_m_timeControl_0(RuntimeObject* value)
	{
		___m_timeControl_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_timeControl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECONTROLPLAYABLE_T1010398267_H
#ifndef PARTICLECONTROLPLAYABLE_T3220249377_H
#define PARTICLECONTROLPLAYABLE_T3220249377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ParticleControlPlayable
struct  ParticleControlPlayable_t3220249377  : public PlayableBehaviour_t4203540982
{
public:
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_LastTime
	float ___m_LastTime_0;
	// System.UInt32 UnityEngine.Timeline.ParticleControlPlayable::m_RandomSeed
	uint32_t ___m_RandomSeed_1;
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_SystemTime
	float ___m_SystemTime_2;
	// UnityEngine.ParticleSystem UnityEngine.Timeline.ParticleControlPlayable::<particleSystem>k__BackingField
	ParticleSystem_t1800779281 * ___U3CparticleSystemU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_LastTime_0() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___m_LastTime_0)); }
	inline float get_m_LastTime_0() const { return ___m_LastTime_0; }
	inline float* get_address_of_m_LastTime_0() { return &___m_LastTime_0; }
	inline void set_m_LastTime_0(float value)
	{
		___m_LastTime_0 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_1() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___m_RandomSeed_1)); }
	inline uint32_t get_m_RandomSeed_1() const { return ___m_RandomSeed_1; }
	inline uint32_t* get_address_of_m_RandomSeed_1() { return &___m_RandomSeed_1; }
	inline void set_m_RandomSeed_1(uint32_t value)
	{
		___m_RandomSeed_1 = value;
	}

	inline static int32_t get_offset_of_m_SystemTime_2() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___m_SystemTime_2)); }
	inline float get_m_SystemTime_2() const { return ___m_SystemTime_2; }
	inline float* get_address_of_m_SystemTime_2() { return &___m_SystemTime_2; }
	inline void set_m_SystemTime_2(float value)
	{
		___m_SystemTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CparticleSystemU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___U3CparticleSystemU3Ek__BackingField_3)); }
	inline ParticleSystem_t1800779281 * get_U3CparticleSystemU3Ek__BackingField_3() const { return ___U3CparticleSystemU3Ek__BackingField_3; }
	inline ParticleSystem_t1800779281 ** get_address_of_U3CparticleSystemU3Ek__BackingField_3() { return &___U3CparticleSystemU3Ek__BackingField_3; }
	inline void set_U3CparticleSystemU3Ek__BackingField_3(ParticleSystem_t1800779281 * value)
	{
		___U3CparticleSystemU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparticleSystemU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECONTROLPLAYABLE_T3220249377_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef NOTKEYABLEATTRIBUTE_T1406439677_H
#define NOTKEYABLEATTRIBUTE_T1406439677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.NotKeyableAttribute
struct  NotKeyableAttribute_t1406439677  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTKEYABLEATTRIBUTE_T1406439677_H
#ifndef TIMELINEPLAYABLE_T2938744123_H
#define TIMELINEPLAYABLE_T2938744123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable
struct  TimelinePlayable_t2938744123  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_IntervalTree
	IntervalTree_1_t478208184 * ___m_IntervalTree_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_ActiveClips
	List_1_t3540616959 * ___m_ActiveClips_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_CurrentListOfActiveClips
	List_1_t3540616959 * ___m_CurrentListOfActiveClips_2;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable::m_ActiveBit
	int32_t ___m_ActiveBit_3;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback> UnityEngine.Timeline.TimelinePlayable::m_EvaluateCallbacks
	List_1_t2313598679 * ___m_EvaluateCallbacks_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Timeline.TimelinePlayable/ConnectionCache> UnityEngine.Timeline.TimelinePlayable::m_PlayableCache
	Dictionary_2_t3971549929 * ___m_PlayableCache_5;

public:
	inline static int32_t get_offset_of_m_IntervalTree_0() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_IntervalTree_0)); }
	inline IntervalTree_1_t478208184 * get_m_IntervalTree_0() const { return ___m_IntervalTree_0; }
	inline IntervalTree_1_t478208184 ** get_address_of_m_IntervalTree_0() { return &___m_IntervalTree_0; }
	inline void set_m_IntervalTree_0(IntervalTree_1_t478208184 * value)
	{
		___m_IntervalTree_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntervalTree_0), value);
	}

	inline static int32_t get_offset_of_m_ActiveClips_1() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_ActiveClips_1)); }
	inline List_1_t3540616959 * get_m_ActiveClips_1() const { return ___m_ActiveClips_1; }
	inline List_1_t3540616959 ** get_address_of_m_ActiveClips_1() { return &___m_ActiveClips_1; }
	inline void set_m_ActiveClips_1(List_1_t3540616959 * value)
	{
		___m_ActiveClips_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveClips_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentListOfActiveClips_2() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_CurrentListOfActiveClips_2)); }
	inline List_1_t3540616959 * get_m_CurrentListOfActiveClips_2() const { return ___m_CurrentListOfActiveClips_2; }
	inline List_1_t3540616959 ** get_address_of_m_CurrentListOfActiveClips_2() { return &___m_CurrentListOfActiveClips_2; }
	inline void set_m_CurrentListOfActiveClips_2(List_1_t3540616959 * value)
	{
		___m_CurrentListOfActiveClips_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentListOfActiveClips_2), value);
	}

	inline static int32_t get_offset_of_m_ActiveBit_3() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_ActiveBit_3)); }
	inline int32_t get_m_ActiveBit_3() const { return ___m_ActiveBit_3; }
	inline int32_t* get_address_of_m_ActiveBit_3() { return &___m_ActiveBit_3; }
	inline void set_m_ActiveBit_3(int32_t value)
	{
		___m_ActiveBit_3 = value;
	}

	inline static int32_t get_offset_of_m_EvaluateCallbacks_4() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_EvaluateCallbacks_4)); }
	inline List_1_t2313598679 * get_m_EvaluateCallbacks_4() const { return ___m_EvaluateCallbacks_4; }
	inline List_1_t2313598679 ** get_address_of_m_EvaluateCallbacks_4() { return &___m_EvaluateCallbacks_4; }
	inline void set_m_EvaluateCallbacks_4(List_1_t2313598679 * value)
	{
		___m_EvaluateCallbacks_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EvaluateCallbacks_4), value);
	}

	inline static int32_t get_offset_of_m_PlayableCache_5() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_PlayableCache_5)); }
	inline Dictionary_2_t3971549929 * get_m_PlayableCache_5() const { return ___m_PlayableCache_5; }
	inline Dictionary_2_t3971549929 ** get_address_of_m_PlayableCache_5() { return &___m_PlayableCache_5; }
	inline void set_m_PlayableCache_5(Dictionary_2_t3971549929 * value)
	{
		___m_PlayableCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayableCache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEPLAYABLE_T2938744123_H
#ifndef IGNOREONPLAYABLETRACKATTRIBUTE_T3455747041_H
#define IGNOREONPLAYABLETRACKATTRIBUTE_T3455747041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.IgnoreOnPlayableTrackAttribute
struct  IgnoreOnPlayableTrackAttribute_t3455747041  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNOREONPLAYABLETRACKATTRIBUTE_T3455747041_H
#ifndef SUPPORTSCHILDTRACKSATTRIBUTE_T205780379_H
#define SUPPORTSCHILDTRACKSATTRIBUTE_T205780379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.SupportsChildTracksAttribute
struct  SupportsChildTracksAttribute_t205780379  : public Attribute_t861562559
{
public:
	// System.Type UnityEngine.Timeline.SupportsChildTracksAttribute::childType
	Type_t * ___childType_0;
	// System.Int32 UnityEngine.Timeline.SupportsChildTracksAttribute::levels
	int32_t ___levels_1;

public:
	inline static int32_t get_offset_of_childType_0() { return static_cast<int32_t>(offsetof(SupportsChildTracksAttribute_t205780379, ___childType_0)); }
	inline Type_t * get_childType_0() const { return ___childType_0; }
	inline Type_t ** get_address_of_childType_0() { return &___childType_0; }
	inline void set_childType_0(Type_t * value)
	{
		___childType_0 = value;
		Il2CppCodeGenWriteBarrier((&___childType_0), value);
	}

	inline static int32_t get_offset_of_levels_1() { return static_cast<int32_t>(offsetof(SupportsChildTracksAttribute_t205780379, ___levels_1)); }
	inline int32_t get_levels_1() const { return ___levels_1; }
	inline int32_t* get_address_of_levels_1() { return &___levels_1; }
	inline void set_levels_1(int32_t value)
	{
		___levels_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTSCHILDTRACKSATTRIBUTE_T205780379_H
#ifndef TRACKBINDINGTYPEATTRIBUTE_T2757649500_H
#define TRACKBINDINGTYPEATTRIBUTE_T2757649500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackBindingTypeAttribute
struct  TrackBindingTypeAttribute_t2757649500  : public Attribute_t861562559
{
public:
	// System.Type UnityEngine.Timeline.TrackBindingTypeAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TrackBindingTypeAttribute_t2757649500, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBINDINGTYPEATTRIBUTE_T2757649500_H
#ifndef TRACKCLIPTYPEATTRIBUTE_T4223207965_H
#define TRACKCLIPTYPEATTRIBUTE_T4223207965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackClipTypeAttribute
struct  TrackClipTypeAttribute_t4223207965  : public Attribute_t861562559
{
public:
	// System.Type UnityEngine.Timeline.TrackClipTypeAttribute::inspectedType
	Type_t * ___inspectedType_0;

public:
	inline static int32_t get_offset_of_inspectedType_0() { return static_cast<int32_t>(offsetof(TrackClipTypeAttribute_t4223207965, ___inspectedType_0)); }
	inline Type_t * get_inspectedType_0() const { return ___inspectedType_0; }
	inline Type_t ** get_address_of_inspectedType_0() { return &___inspectedType_0; }
	inline void set_inspectedType_0(Type_t * value)
	{
		___inspectedType_0 = value;
		Il2CppCodeGenWriteBarrier((&___inspectedType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCLIPTYPEATTRIBUTE_T4223207965_H
#ifndef DISCRETETIME_T924799574_H
#define DISCRETETIME_T924799574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t924799574 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t924799574, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t924799574_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t924799574  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t924799574_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t924799574  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t924799574 * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t924799574  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T924799574_H
#ifndef POSTPLAYBACKSTATE_T1986969933_H
#define POSTPLAYBACKSTATE_T1986969933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState
struct  PostPlaybackState_t1986969933 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostPlaybackState_t1986969933, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T1986969933_H
#ifndef DATASTREAMTYPE_T4132467813_H
#define DATASTREAMTYPE_T4132467813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t4132467813 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataStreamType_t4132467813, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T4132467813_H
#ifndef TIMEFIELDATTRIBUTE_T3035049162_H
#define TIMEFIELDATTRIBUTE_T3035049162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeFieldAttribute
struct  TimeFieldAttribute_t3035049162  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEFIELDATTRIBUTE_T3035049162_H
#ifndef CLIPEXTRAPOLATION_T324732067_H
#define CLIPEXTRAPOLATION_T324732067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/ClipExtrapolation
struct  ClipExtrapolation_t324732067 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/ClipExtrapolation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClipExtrapolation_t324732067, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPEXTRAPOLATION_T324732067_H
#ifndef PLAYABLEHANDLE_T1095853803_H
#define PLAYABLEHANDLE_T1095853803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1095853803 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1095853803_H
#ifndef CLIPCAPS_T3444432629_H
#define CLIPCAPS_T3444432629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ClipCaps
struct  ClipCaps_t3444432629 
{
public:
	// System.Int32 UnityEngine.Timeline.ClipCaps::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClipCaps_t3444432629, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPCAPS_T3444432629_H
#ifndef BLENDCURVEMODE_T1741293302_H
#define BLENDCURVEMODE_T1741293302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/BlendCurveMode
struct  BlendCurveMode_t1741293302 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/BlendCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendCurveMode_t1741293302, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDCURVEMODE_T1741293302_H
#ifndef POSTPLAYBACKSTATE_T4203018085_H
#define POSTPLAYBACKSTATE_T4203018085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack/PostPlaybackState
struct  PostPlaybackState_t4203018085 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationTrack/PostPlaybackState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostPlaybackState_t4203018085, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T4203018085_H
#ifndef DOWNLOADHANDLER_T2937767557_H
#define DOWNLOADHANDLER_T2937767557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t2937767557  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t2937767557, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T2937767557_H
#ifndef EVENTHANDLE_T600343995_H
#define EVENTHANDLE_T600343995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t600343995 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventHandle_t600343995, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_T600343995_H
#ifndef MEDIATYPE_T4163677969_H
#define MEDIATYPE_T4163677969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/MediaType
struct  MediaType_t4163677969 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/MediaType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MediaType_t4163677969, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPE_T4163677969_H
#ifndef DURATIONMODE_T2670267584_H
#define DURATIONMODE_T2670267584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/DurationMode
struct  DurationMode_t2670267584 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/DurationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DurationMode_t2670267584, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONMODE_T2670267584_H
#ifndef PLAYABLEOUTPUTHANDLE_T4208053793_H
#define PLAYABLEOUTPUTHANDLE_T4208053793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t4208053793 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t4208053793, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t4208053793, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T4208053793_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef MATCHTARGETFIELDS_T3959953937_H
#define MATCHTARGETFIELDS_T3959953937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFields
struct  MatchTargetFields_t3959953937 
{
public:
	// System.Int32 UnityEngine.Timeline.MatchTargetFields::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MatchTargetFields_t3959953937, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDS_T3959953937_H
#ifndef TRACKCOLORATTRIBUTE_T2898993029_H
#define TRACKCOLORATTRIBUTE_T2898993029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackColorAttribute
struct  TrackColorAttribute_t2898993029  : public Attribute_t861562559
{
public:
	// UnityEngine.Color UnityEngine.Timeline.TrackColorAttribute::m_Color
	Color_t2555686324  ___m_Color_0;

public:
	inline static int32_t get_offset_of_m_Color_0() { return static_cast<int32_t>(offsetof(TrackColorAttribute_t2898993029, ___m_Color_0)); }
	inline Color_t2555686324  get_m_Color_0() const { return ___m_Color_0; }
	inline Color_t2555686324 * get_address_of_m_Color_0() { return &___m_Color_0; }
	inline void set_m_Color_0(Color_t2555686324  value)
	{
		___m_Color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCOLORATTRIBUTE_T2898993029_H
#ifndef MATCHTARGETFIELDCONSTANTS_T2682568617_H
#define MATCHTARGETFIELDCONSTANTS_T2682568617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFieldConstants
struct  MatchTargetFieldConstants_t2682568617  : public RuntimeObject
{
public:

public:
};

struct MatchTargetFieldConstants_t2682568617_StaticFields
{
public:
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::All
	int32_t ___All_0;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::None
	int32_t ___None_1;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Position
	int32_t ___Position_2;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Rotation
	int32_t ___Rotation_3;

public:
	inline static int32_t get_offset_of_All_0() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___All_0)); }
	inline int32_t get_All_0() const { return ___All_0; }
	inline int32_t* get_address_of_All_0() { return &___All_0; }
	inline void set_All_0(int32_t value)
	{
		___All_0 = value;
	}

	inline static int32_t get_offset_of_None_1() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___None_1)); }
	inline int32_t get_None_1() const { return ___None_1; }
	inline int32_t* get_address_of_None_1() { return &___None_1; }
	inline void set_None_1(int32_t value)
	{
		___None_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___Rotation_3)); }
	inline int32_t get_Rotation_3() const { return ___Rotation_3; }
	inline int32_t* get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(int32_t value)
	{
		___Rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDCONSTANTS_T2682568617_H
#ifndef TRACKMEDIATYPE_T482867812_H
#define TRACKMEDIATYPE_T482867812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackMediaType
struct  TrackMediaType_t482867812  : public Attribute_t861562559
{
public:
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackMediaType::m_MediaType
	int32_t ___m_MediaType_0;

public:
	inline static int32_t get_offset_of_m_MediaType_0() { return static_cast<int32_t>(offsetof(TrackMediaType_t482867812, ___m_MediaType_0)); }
	inline int32_t get_m_MediaType_0() const { return ___m_MediaType_0; }
	inline int32_t* get_address_of_m_MediaType_0() { return &___m_MediaType_0; }
	inline void set_m_MediaType_0(int32_t value)
	{
		___m_MediaType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKMEDIATYPE_T482867812_H
#ifndef EXPOSEDREFERENCE_1_T1378690540_H
#define EXPOSEDREFERENCE_1_T1378690540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExposedReference`1<UnityEngine.GameObject>
struct  ExposedReference_1_t1378690540 
{
public:
	// UnityEngine.PropertyName UnityEngine.ExposedReference`1::exposedName
	PropertyName_t3749835189  ___exposedName_0;
	// UnityEngine.Object UnityEngine.ExposedReference`1::defaultValue
	Object_t631007953 * ___defaultValue_1;

public:
	inline static int32_t get_offset_of_exposedName_0() { return static_cast<int32_t>(offsetof(ExposedReference_1_t1378690540, ___exposedName_0)); }
	inline PropertyName_t3749835189  get_exposedName_0() const { return ___exposedName_0; }
	inline PropertyName_t3749835189 * get_address_of_exposedName_0() { return &___exposedName_0; }
	inline void set_exposedName_0(PropertyName_t3749835189  value)
	{
		___exposedName_0 = value;
	}

	inline static int32_t get_offset_of_defaultValue_1() { return static_cast<int32_t>(offsetof(ExposedReference_1_t1378690540, ___defaultValue_1)); }
	inline Object_t631007953 * get_defaultValue_1() const { return ___defaultValue_1; }
	inline Object_t631007953 ** get_address_of_defaultValue_1() { return &___defaultValue_1; }
	inline void set_defaultValue_1(Object_t631007953 * value)
	{
		___defaultValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ExposedReference`1
#ifndef ExposedReference_1_t2548232636_marshaled_pinvoke_define
#define ExposedReference_1_t2548232636_marshaled_pinvoke_define
struct ExposedReference_1_t2548232636_marshaled_pinvoke
{
	PropertyName_t3749835189  ___exposedName_0;
	Object_t631007953_marshaled_pinvoke ___defaultValue_1;
};
#endif
// Native definition for COM marshalling of UnityEngine.ExposedReference`1
#ifndef ExposedReference_1_t2548232636_marshaled_com_define
#define ExposedReference_1_t2548232636_marshaled_com_define
struct ExposedReference_1_t2548232636_marshaled_com
{
	PropertyName_t3749835189  ___exposedName_0;
	Object_t631007953_marshaled_com* ___defaultValue_1;
};
#endif
#endif // EXPOSEDREFERENCE_1_T1378690540_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef DOWNLOADHANDLERAUDIOCLIP_T3167892435_H
#define DOWNLOADHANDLERAUDIOCLIP_T3167892435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerAudioClip
struct  DownloadHandlerAudioClip_t3167892435  : public DownloadHandler_t2937767557
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t3167892435_marshaled_pinvoke : public DownloadHandler_t2937767557_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t3167892435_marshaled_com : public DownloadHandler_t2937767557_marshaled_com
{
};
#endif // DOWNLOADHANDLERAUDIOCLIP_T3167892435_H
#ifndef TIMELINECLIP_T2478225950_H
#define TIMELINECLIP_T2478225950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip
struct  TimelineClip_t2478225950  : public RuntimeObject
{
public:
	// System.Double UnityEngine.Timeline.TimelineClip::m_Start
	double ___m_Start_7;
	// System.Double UnityEngine.Timeline.TimelineClip::m_ClipIn
	double ___m_ClipIn_8;
	// UnityEngine.Object UnityEngine.Timeline.TimelineClip::m_Asset
	Object_t631007953 * ___m_Asset_9;
	// System.Double UnityEngine.Timeline.TimelineClip::m_Duration
	double ___m_Duration_10;
	// System.Double UnityEngine.Timeline.TimelineClip::m_TimeScale
	double ___m_TimeScale_11;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineClip::m_ParentTrack
	TrackAsset_t2828708245 * ___m_ParentTrack_12;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseInDuration
	double ___m_EaseInDuration_13;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseOutDuration
	double ___m_EaseOutDuration_14;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendInDuration
	double ___m_BlendInDuration_15;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendOutDuration
	double ___m_BlendOutDuration_16;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixInCurve
	AnimationCurve_t3046754366 * ___m_MixInCurve_17;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixOutCurve
	AnimationCurve_t3046754366 * ___m_MixOutCurve_18;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendInCurveMode
	int32_t ___m_BlendInCurveMode_19;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendOutCurveMode
	int32_t ___m_BlendOutCurveMode_20;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Timeline.TimelineClip::m_ExposedParameterNames
	List_1_t3319525431 * ___m_ExposedParameterNames_21;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TimelineClip::m_AnimationCurves
	AnimationClip_t2318505987 * ___m_AnimationCurves_22;
	// System.Boolean UnityEngine.Timeline.TimelineClip::m_Recordable
	bool ___m_Recordable_23;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PostExtrapolationMode
	int32_t ___m_PostExtrapolationMode_24;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PreExtrapolationMode
	int32_t ___m_PreExtrapolationMode_25;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PostExtrapolationTime
	double ___m_PostExtrapolationTime_26;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PreExtrapolationTime
	double ___m_PreExtrapolationTime_27;
	// System.String UnityEngine.Timeline.TimelineClip::m_DisplayName
	String_t* ___m_DisplayName_28;
	// System.Int32 UnityEngine.Timeline.TimelineClip::m_Version
	int32_t ___m_Version_29;
	// System.Int32 UnityEngine.Timeline.TimelineClip::<dirtyHash>k__BackingField
	int32_t ___U3CdirtyHashU3Ek__BackingField_30;

public:
	inline static int32_t get_offset_of_m_Start_7() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_Start_7)); }
	inline double get_m_Start_7() const { return ___m_Start_7; }
	inline double* get_address_of_m_Start_7() { return &___m_Start_7; }
	inline void set_m_Start_7(double value)
	{
		___m_Start_7 = value;
	}

	inline static int32_t get_offset_of_m_ClipIn_8() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_ClipIn_8)); }
	inline double get_m_ClipIn_8() const { return ___m_ClipIn_8; }
	inline double* get_address_of_m_ClipIn_8() { return &___m_ClipIn_8; }
	inline void set_m_ClipIn_8(double value)
	{
		___m_ClipIn_8 = value;
	}

	inline static int32_t get_offset_of_m_Asset_9() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_Asset_9)); }
	inline Object_t631007953 * get_m_Asset_9() const { return ___m_Asset_9; }
	inline Object_t631007953 ** get_address_of_m_Asset_9() { return &___m_Asset_9; }
	inline void set_m_Asset_9(Object_t631007953 * value)
	{
		___m_Asset_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Asset_9), value);
	}

	inline static int32_t get_offset_of_m_Duration_10() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_Duration_10)); }
	inline double get_m_Duration_10() const { return ___m_Duration_10; }
	inline double* get_address_of_m_Duration_10() { return &___m_Duration_10; }
	inline void set_m_Duration_10(double value)
	{
		___m_Duration_10 = value;
	}

	inline static int32_t get_offset_of_m_TimeScale_11() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_TimeScale_11)); }
	inline double get_m_TimeScale_11() const { return ___m_TimeScale_11; }
	inline double* get_address_of_m_TimeScale_11() { return &___m_TimeScale_11; }
	inline void set_m_TimeScale_11(double value)
	{
		___m_TimeScale_11 = value;
	}

	inline static int32_t get_offset_of_m_ParentTrack_12() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_ParentTrack_12)); }
	inline TrackAsset_t2828708245 * get_m_ParentTrack_12() const { return ___m_ParentTrack_12; }
	inline TrackAsset_t2828708245 ** get_address_of_m_ParentTrack_12() { return &___m_ParentTrack_12; }
	inline void set_m_ParentTrack_12(TrackAsset_t2828708245 * value)
	{
		___m_ParentTrack_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentTrack_12), value);
	}

	inline static int32_t get_offset_of_m_EaseInDuration_13() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_EaseInDuration_13)); }
	inline double get_m_EaseInDuration_13() const { return ___m_EaseInDuration_13; }
	inline double* get_address_of_m_EaseInDuration_13() { return &___m_EaseInDuration_13; }
	inline void set_m_EaseInDuration_13(double value)
	{
		___m_EaseInDuration_13 = value;
	}

	inline static int32_t get_offset_of_m_EaseOutDuration_14() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_EaseOutDuration_14)); }
	inline double get_m_EaseOutDuration_14() const { return ___m_EaseOutDuration_14; }
	inline double* get_address_of_m_EaseOutDuration_14() { return &___m_EaseOutDuration_14; }
	inline void set_m_EaseOutDuration_14(double value)
	{
		___m_EaseOutDuration_14 = value;
	}

	inline static int32_t get_offset_of_m_BlendInDuration_15() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_BlendInDuration_15)); }
	inline double get_m_BlendInDuration_15() const { return ___m_BlendInDuration_15; }
	inline double* get_address_of_m_BlendInDuration_15() { return &___m_BlendInDuration_15; }
	inline void set_m_BlendInDuration_15(double value)
	{
		___m_BlendInDuration_15 = value;
	}

	inline static int32_t get_offset_of_m_BlendOutDuration_16() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_BlendOutDuration_16)); }
	inline double get_m_BlendOutDuration_16() const { return ___m_BlendOutDuration_16; }
	inline double* get_address_of_m_BlendOutDuration_16() { return &___m_BlendOutDuration_16; }
	inline void set_m_BlendOutDuration_16(double value)
	{
		___m_BlendOutDuration_16 = value;
	}

	inline static int32_t get_offset_of_m_MixInCurve_17() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_MixInCurve_17)); }
	inline AnimationCurve_t3046754366 * get_m_MixInCurve_17() const { return ___m_MixInCurve_17; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_MixInCurve_17() { return &___m_MixInCurve_17; }
	inline void set_m_MixInCurve_17(AnimationCurve_t3046754366 * value)
	{
		___m_MixInCurve_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MixInCurve_17), value);
	}

	inline static int32_t get_offset_of_m_MixOutCurve_18() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_MixOutCurve_18)); }
	inline AnimationCurve_t3046754366 * get_m_MixOutCurve_18() const { return ___m_MixOutCurve_18; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_MixOutCurve_18() { return &___m_MixOutCurve_18; }
	inline void set_m_MixOutCurve_18(AnimationCurve_t3046754366 * value)
	{
		___m_MixOutCurve_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MixOutCurve_18), value);
	}

	inline static int32_t get_offset_of_m_BlendInCurveMode_19() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_BlendInCurveMode_19)); }
	inline int32_t get_m_BlendInCurveMode_19() const { return ___m_BlendInCurveMode_19; }
	inline int32_t* get_address_of_m_BlendInCurveMode_19() { return &___m_BlendInCurveMode_19; }
	inline void set_m_BlendInCurveMode_19(int32_t value)
	{
		___m_BlendInCurveMode_19 = value;
	}

	inline static int32_t get_offset_of_m_BlendOutCurveMode_20() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_BlendOutCurveMode_20)); }
	inline int32_t get_m_BlendOutCurveMode_20() const { return ___m_BlendOutCurveMode_20; }
	inline int32_t* get_address_of_m_BlendOutCurveMode_20() { return &___m_BlendOutCurveMode_20; }
	inline void set_m_BlendOutCurveMode_20(int32_t value)
	{
		___m_BlendOutCurveMode_20 = value;
	}

	inline static int32_t get_offset_of_m_ExposedParameterNames_21() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_ExposedParameterNames_21)); }
	inline List_1_t3319525431 * get_m_ExposedParameterNames_21() const { return ___m_ExposedParameterNames_21; }
	inline List_1_t3319525431 ** get_address_of_m_ExposedParameterNames_21() { return &___m_ExposedParameterNames_21; }
	inline void set_m_ExposedParameterNames_21(List_1_t3319525431 * value)
	{
		___m_ExposedParameterNames_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExposedParameterNames_21), value);
	}

	inline static int32_t get_offset_of_m_AnimationCurves_22() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_AnimationCurves_22)); }
	inline AnimationClip_t2318505987 * get_m_AnimationCurves_22() const { return ___m_AnimationCurves_22; }
	inline AnimationClip_t2318505987 ** get_address_of_m_AnimationCurves_22() { return &___m_AnimationCurves_22; }
	inline void set_m_AnimationCurves_22(AnimationClip_t2318505987 * value)
	{
		___m_AnimationCurves_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationCurves_22), value);
	}

	inline static int32_t get_offset_of_m_Recordable_23() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_Recordable_23)); }
	inline bool get_m_Recordable_23() const { return ___m_Recordable_23; }
	inline bool* get_address_of_m_Recordable_23() { return &___m_Recordable_23; }
	inline void set_m_Recordable_23(bool value)
	{
		___m_Recordable_23 = value;
	}

	inline static int32_t get_offset_of_m_PostExtrapolationMode_24() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_PostExtrapolationMode_24)); }
	inline int32_t get_m_PostExtrapolationMode_24() const { return ___m_PostExtrapolationMode_24; }
	inline int32_t* get_address_of_m_PostExtrapolationMode_24() { return &___m_PostExtrapolationMode_24; }
	inline void set_m_PostExtrapolationMode_24(int32_t value)
	{
		___m_PostExtrapolationMode_24 = value;
	}

	inline static int32_t get_offset_of_m_PreExtrapolationMode_25() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_PreExtrapolationMode_25)); }
	inline int32_t get_m_PreExtrapolationMode_25() const { return ___m_PreExtrapolationMode_25; }
	inline int32_t* get_address_of_m_PreExtrapolationMode_25() { return &___m_PreExtrapolationMode_25; }
	inline void set_m_PreExtrapolationMode_25(int32_t value)
	{
		___m_PreExtrapolationMode_25 = value;
	}

	inline static int32_t get_offset_of_m_PostExtrapolationTime_26() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_PostExtrapolationTime_26)); }
	inline double get_m_PostExtrapolationTime_26() const { return ___m_PostExtrapolationTime_26; }
	inline double* get_address_of_m_PostExtrapolationTime_26() { return &___m_PostExtrapolationTime_26; }
	inline void set_m_PostExtrapolationTime_26(double value)
	{
		___m_PostExtrapolationTime_26 = value;
	}

	inline static int32_t get_offset_of_m_PreExtrapolationTime_27() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_PreExtrapolationTime_27)); }
	inline double get_m_PreExtrapolationTime_27() const { return ___m_PreExtrapolationTime_27; }
	inline double* get_address_of_m_PreExtrapolationTime_27() { return &___m_PreExtrapolationTime_27; }
	inline void set_m_PreExtrapolationTime_27(double value)
	{
		___m_PreExtrapolationTime_27 = value;
	}

	inline static int32_t get_offset_of_m_DisplayName_28() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_DisplayName_28)); }
	inline String_t* get_m_DisplayName_28() const { return ___m_DisplayName_28; }
	inline String_t** get_address_of_m_DisplayName_28() { return &___m_DisplayName_28; }
	inline void set_m_DisplayName_28(String_t* value)
	{
		___m_DisplayName_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisplayName_28), value);
	}

	inline static int32_t get_offset_of_m_Version_29() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___m_Version_29)); }
	inline int32_t get_m_Version_29() const { return ___m_Version_29; }
	inline int32_t* get_address_of_m_Version_29() { return &___m_Version_29; }
	inline void set_m_Version_29(int32_t value)
	{
		___m_Version_29 = value;
	}

	inline static int32_t get_offset_of_U3CdirtyHashU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950, ___U3CdirtyHashU3Ek__BackingField_30)); }
	inline int32_t get_U3CdirtyHashU3Ek__BackingField_30() const { return ___U3CdirtyHashU3Ek__BackingField_30; }
	inline int32_t* get_address_of_U3CdirtyHashU3Ek__BackingField_30() { return &___U3CdirtyHashU3Ek__BackingField_30; }
	inline void set_U3CdirtyHashU3Ek__BackingField_30(int32_t value)
	{
		___U3CdirtyHashU3Ek__BackingField_30 = value;
	}
};

struct TimelineClip_t2478225950_StaticFields
{
public:
	// UnityEngine.Timeline.ClipCaps UnityEngine.Timeline.TimelineClip::kDefaultClipCaps
	int32_t ___kDefaultClipCaps_0;
	// System.Single UnityEngine.Timeline.TimelineClip::kDefaultClipDurationInSeconds
	float ___kDefaultClipDurationInSeconds_1;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMin
	double ___kTimeScaleMin_2;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMax
	double ___kTimeScaleMax_3;
	// System.Double UnityEngine.Timeline.TimelineClip::kMinDuration
	double ___kMinDuration_4;
	// System.Double UnityEngine.Timeline.TimelineClip::kMaxTimeValue
	double ___kMaxTimeValue_6;

public:
	inline static int32_t get_offset_of_kDefaultClipCaps_0() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950_StaticFields, ___kDefaultClipCaps_0)); }
	inline int32_t get_kDefaultClipCaps_0() const { return ___kDefaultClipCaps_0; }
	inline int32_t* get_address_of_kDefaultClipCaps_0() { return &___kDefaultClipCaps_0; }
	inline void set_kDefaultClipCaps_0(int32_t value)
	{
		___kDefaultClipCaps_0 = value;
	}

	inline static int32_t get_offset_of_kDefaultClipDurationInSeconds_1() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950_StaticFields, ___kDefaultClipDurationInSeconds_1)); }
	inline float get_kDefaultClipDurationInSeconds_1() const { return ___kDefaultClipDurationInSeconds_1; }
	inline float* get_address_of_kDefaultClipDurationInSeconds_1() { return &___kDefaultClipDurationInSeconds_1; }
	inline void set_kDefaultClipDurationInSeconds_1(float value)
	{
		___kDefaultClipDurationInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_kTimeScaleMin_2() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950_StaticFields, ___kTimeScaleMin_2)); }
	inline double get_kTimeScaleMin_2() const { return ___kTimeScaleMin_2; }
	inline double* get_address_of_kTimeScaleMin_2() { return &___kTimeScaleMin_2; }
	inline void set_kTimeScaleMin_2(double value)
	{
		___kTimeScaleMin_2 = value;
	}

	inline static int32_t get_offset_of_kTimeScaleMax_3() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950_StaticFields, ___kTimeScaleMax_3)); }
	inline double get_kTimeScaleMax_3() const { return ___kTimeScaleMax_3; }
	inline double* get_address_of_kTimeScaleMax_3() { return &___kTimeScaleMax_3; }
	inline void set_kTimeScaleMax_3(double value)
	{
		___kTimeScaleMax_3 = value;
	}

	inline static int32_t get_offset_of_kMinDuration_4() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950_StaticFields, ___kMinDuration_4)); }
	inline double get_kMinDuration_4() const { return ___kMinDuration_4; }
	inline double* get_address_of_kMinDuration_4() { return &___kMinDuration_4; }
	inline void set_kMinDuration_4(double value)
	{
		___kMinDuration_4 = value;
	}

	inline static int32_t get_offset_of_kMaxTimeValue_6() { return static_cast<int32_t>(offsetof(TimelineClip_t2478225950_StaticFields, ___kMaxTimeValue_6)); }
	inline double get_kMaxTimeValue_6() const { return ___kMaxTimeValue_6; }
	inline double* get_address_of_kMaxTimeValue_6() { return &___kMaxTimeValue_6; }
	inline void set_kMaxTimeValue_6(double value)
	{
		___kMaxTimeValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECLIP_T2478225950_H
#ifndef PLAYABLEBINDING_T354260709_H
#define PLAYABLEBINDING_T354260709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t354260709 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t631007953 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t354260709__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t631007953 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t631007953 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t631007953 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t354260709_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t829358056* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t829358056* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t829358056** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t829358056* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t354260709_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t631007953_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t354260709__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t354260709_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t631007953_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t354260709__padding[1];
	};
};
#endif // PLAYABLEBINDING_T354260709_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ACTIVATIONMIXERPLAYABLE_T2212523045_H
#define ACTIVATIONMIXERPLAYABLE_T2212523045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationMixerPlayable
struct  ActivationMixerPlayable_t2212523045  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationMixerPlayable::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_0;
	// System.Boolean UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObjectInitialStateIsActive
	bool ___m_BoundGameObjectInitialStateIsActive_1;
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObject
	GameObject_t1113636619 * ___m_BoundGameObject_2;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_0() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t2212523045, ___m_PostPlaybackState_0)); }
	inline int32_t get_m_PostPlaybackState_0() const { return ___m_PostPlaybackState_0; }
	inline int32_t* get_address_of_m_PostPlaybackState_0() { return &___m_PostPlaybackState_0; }
	inline void set_m_PostPlaybackState_0(int32_t value)
	{
		___m_PostPlaybackState_0 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObjectInitialStateIsActive_1() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t2212523045, ___m_BoundGameObjectInitialStateIsActive_1)); }
	inline bool get_m_BoundGameObjectInitialStateIsActive_1() const { return ___m_BoundGameObjectInitialStateIsActive_1; }
	inline bool* get_address_of_m_BoundGameObjectInitialStateIsActive_1() { return &___m_BoundGameObjectInitialStateIsActive_1; }
	inline void set_m_BoundGameObjectInitialStateIsActive_1(bool value)
	{
		___m_BoundGameObjectInitialStateIsActive_1 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObject_2() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t2212523045, ___m_BoundGameObject_2)); }
	inline GameObject_t1113636619 * get_m_BoundGameObject_2() const { return ___m_BoundGameObject_2; }
	inline GameObject_t1113636619 ** get_address_of_m_BoundGameObject_2() { return &___m_BoundGameObject_2; }
	inline void set_m_BoundGameObject_2(GameObject_t1113636619 * value)
	{
		___m_BoundGameObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundGameObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONMIXERPLAYABLE_T2212523045_H
#ifndef VIDEOCLIPPLAYABLE_T2598186649_H
#define VIDEOCLIPPLAYABLE_T2598186649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Video.VideoClipPlayable
struct  VideoClipPlayable_t2598186649 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(VideoClipPlayable_t2598186649, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIPPLAYABLE_T2598186649_H
#ifndef ACTIVATIONCONTROLPLAYABLE_T426148305_H
#define ACTIVATIONCONTROLPLAYABLE_T426148305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable
struct  ActivationControlPlayable_t426148305  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationControlPlayable::gameObject
	GameObject_t1113636619 * ___gameObject_0;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ActivationControlPlayable::postPlayback
	int32_t ___postPlayback_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t426148305, ___gameObject_0)); }
	inline GameObject_t1113636619 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1113636619 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_postPlayback_1() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t426148305, ___postPlayback_1)); }
	inline int32_t get_postPlayback_1() const { return ___postPlayback_1; }
	inline int32_t* get_address_of_postPlayback_1() { return &___postPlayback_1; }
	inline void set_postPlayback_1(int32_t value)
	{
		___postPlayback_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONCONTROLPLAYABLE_T426148305_H
#ifndef PLAYABLE_T459825607_H
#define PLAYABLE_T459825607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t459825607 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t459825607, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t459825607_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t459825607  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t459825607_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t459825607  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t459825607 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t459825607  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T459825607_H
#ifndef ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#define ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_t1918618239 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t4208053793  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_t1918618239, ___m_Handle_0)); }
	inline PlayableOutputHandle_t4208053793  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t4208053793 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t4208053793  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#ifndef ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#define ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct  AnimationLayerMixerPlayable_t3631223897 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t3631223897, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationLayerMixerPlayable_t3631223897_StaticFields
{
public:
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_t3631223897  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t3631223897_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationLayerMixerPlayable_t3631223897  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationLayerMixerPlayable_t3631223897 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationLayerMixerPlayable_t3631223897  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef EVENTHANDLER_T3436254912_H
#define EVENTHANDLER_T3436254912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/EventHandler
struct  EventHandler_t3436254912  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T3436254912_H
#ifndef PLAYABLEASSET_T3219022681_H
#define PLAYABLEASSET_T3219022681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t3219022681  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T3219022681_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef FRAMEREADYEVENTHANDLER_T3848515759_H
#define FRAMEREADYEVENTHANDLER_T3848515759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct  FrameReadyEventHandler_t3848515759  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEREADYEVENTHANDLER_T3848515759_H
#ifndef ERROREVENTHANDLER_T3211687919_H
#define ERROREVENTHANDLER_T3211687919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct  ErrorEventHandler_t3211687919  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_T3211687919_H
#ifndef TIMEEVENTHANDLER_T445758600_H
#define TIMEEVENTHANDLER_T445758600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct  TimeEventHandler_t445758600  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEEVENTHANDLER_T445758600_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UPDATEDEVENTHANDLER_T1027848393_H
#define UPDATEDEVENTHANDLER_T1027848393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t1027848393  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T1027848393_H
#ifndef RUNTIMECLIP_T583365242_H
#define RUNTIMECLIP_T583365242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClip
struct  RuntimeClip_t583365242  : public RuntimeClipBase_t197358283
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.RuntimeClip::m_Clip
	TimelineClip_t2478225950 * ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_Playable
	Playable_t459825607  ___m_Playable_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_ParentMixer
	Playable_t459825607  ___m_ParentMixer_3;

public:
	inline static int32_t get_offset_of_m_Clip_1() { return static_cast<int32_t>(offsetof(RuntimeClip_t583365242, ___m_Clip_1)); }
	inline TimelineClip_t2478225950 * get_m_Clip_1() const { return ___m_Clip_1; }
	inline TimelineClip_t2478225950 ** get_address_of_m_Clip_1() { return &___m_Clip_1; }
	inline void set_m_Clip_1(TimelineClip_t2478225950 * value)
	{
		___m_Clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_1), value);
	}

	inline static int32_t get_offset_of_m_Playable_2() { return static_cast<int32_t>(offsetof(RuntimeClip_t583365242, ___m_Playable_2)); }
	inline Playable_t459825607  get_m_Playable_2() const { return ___m_Playable_2; }
	inline Playable_t459825607 * get_address_of_m_Playable_2() { return &___m_Playable_2; }
	inline void set_m_Playable_2(Playable_t459825607  value)
	{
		___m_Playable_2 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_3() { return static_cast<int32_t>(offsetof(RuntimeClip_t583365242, ___m_ParentMixer_3)); }
	inline Playable_t459825607  get_m_ParentMixer_3() const { return ___m_ParentMixer_3; }
	inline Playable_t459825607 * get_address_of_m_ParentMixer_3() { return &___m_ParentMixer_3; }
	inline void set_m_ParentMixer_3(Playable_t459825607  value)
	{
		___m_ParentMixer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIP_T583365242_H
#ifndef INFINITERUNTIMECLIP_T156380358_H
#define INFINITERUNTIMECLIP_T156380358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.InfiniteRuntimeClip
struct  InfiniteRuntimeClip_t156380358  : public RuntimeElement_t2068542217
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.InfiniteRuntimeClip::m_Playable
	Playable_t459825607  ___m_Playable_1;

public:
	inline static int32_t get_offset_of_m_Playable_1() { return static_cast<int32_t>(offsetof(InfiniteRuntimeClip_t156380358, ___m_Playable_1)); }
	inline Playable_t459825607  get_m_Playable_1() const { return ___m_Playable_1; }
	inline Playable_t459825607 * get_address_of_m_Playable_1() { return &___m_Playable_1; }
	inline void set_m_Playable_1(Playable_t459825607  value)
	{
		___m_Playable_1 = value;
	}
};

struct InfiniteRuntimeClip_t156380358_StaticFields
{
public:
	// System.Int64 UnityEngine.Timeline.InfiniteRuntimeClip::kIntervalEnd
	int64_t ___kIntervalEnd_2;

public:
	inline static int32_t get_offset_of_kIntervalEnd_2() { return static_cast<int32_t>(offsetof(InfiniteRuntimeClip_t156380358_StaticFields, ___kIntervalEnd_2)); }
	inline int64_t get_kIntervalEnd_2() const { return ___kIntervalEnd_2; }
	inline int64_t* get_address_of_kIntervalEnd_2() { return &___kIntervalEnd_2; }
	inline void set_kIntervalEnd_2(int64_t value)
	{
		___kIntervalEnd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITERUNTIMECLIP_T156380358_H
#ifndef U3CU3EC__ITERATOR0_T4165699274_H
#define U3CU3EC__ITERATOR0_T4165699274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t4165699274  : public RuntimeObject
{
public:
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_0;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24current_0)); }
	inline PlayableBinding_t354260709  get_U24current_0() const { return ___U24current_0; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(PlayableBinding_t354260709  value)
	{
		___U24current_0 = value;
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T4165699274_H
#ifndef CONNECTIONCACHE_T1120597666_H
#define CONNECTIONCACHE_T1120597666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable/ConnectionCache
struct  ConnectionCache_t1120597666 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimelinePlayable/ConnectionCache::playable
	Playable_t459825607  ___playable_0;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable/ConnectionCache::port
	int32_t ___port_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimelinePlayable/ConnectionCache::parent
	Playable_t459825607  ___parent_2;
	// System.Single UnityEngine.Timeline.TimelinePlayable/ConnectionCache::evalWeight
	float ___evalWeight_3;

public:
	inline static int32_t get_offset_of_playable_0() { return static_cast<int32_t>(offsetof(ConnectionCache_t1120597666, ___playable_0)); }
	inline Playable_t459825607  get_playable_0() const { return ___playable_0; }
	inline Playable_t459825607 * get_address_of_playable_0() { return &___playable_0; }
	inline void set_playable_0(Playable_t459825607  value)
	{
		___playable_0 = value;
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(ConnectionCache_t1120597666, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ConnectionCache_t1120597666, ___parent_2)); }
	inline Playable_t459825607  get_parent_2() const { return ___parent_2; }
	inline Playable_t459825607 * get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Playable_t459825607  value)
	{
		___parent_2 = value;
	}

	inline static int32_t get_offset_of_evalWeight_3() { return static_cast<int32_t>(offsetof(ConnectionCache_t1120597666, ___evalWeight_3)); }
	inline float get_evalWeight_3() const { return ___evalWeight_3; }
	inline float* get_address_of_evalWeight_3() { return &___evalWeight_3; }
	inline void set_evalWeight_3(float value)
	{
		___evalWeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCACHE_T1120597666_H
#ifndef U3CU3EC__ITERATOR0_T1104488729_H
#define U3CU3EC__ITERATOR0_T1104488729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1104488729  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AnimationTrack UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$this
	AnimationTrack_t3872729528 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24this_0)); }
	inline AnimationTrack_t3872729528 * get_U24this_0() const { return ___U24this_0; }
	inline AnimationTrack_t3872729528 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimationTrack_t3872729528 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24current_1)); }
	inline PlayableBinding_t354260709  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t354260709  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1104488729_H
#ifndef U3CU3EC__ITERATOR0_T717920108_H
#define U3CU3EC__ITERATOR0_T717920108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t717920108  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AudioTrack UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$this
	AudioTrack_t1549411460 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24this_0)); }
	inline AudioTrack_t1549411460 * get_U24this_0() const { return ___U24this_0; }
	inline AudioTrack_t1549411460 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioTrack_t1549411460 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24current_1)); }
	inline PlayableBinding_t354260709  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t354260709  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T717920108_H
#ifndef SCHEDULERUNTIMECLIP_T283756268_H
#define SCHEDULERUNTIMECLIP_T283756268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ScheduleRuntimeClip
struct  ScheduleRuntimeClip_t283756268  : public RuntimeClipBase_t197358283
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.ScheduleRuntimeClip::m_Clip
	TimelineClip_t2478225950 * ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_Playable
	Playable_t459825607  ___m_Playable_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_ParentMixer
	Playable_t459825607  ___m_ParentMixer_3;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_StartDelay
	double ___m_StartDelay_4;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_FinishTail
	double ___m_FinishTail_5;
	// System.Boolean UnityEngine.Timeline.ScheduleRuntimeClip::m_Started
	bool ___m_Started_6;

public:
	inline static int32_t get_offset_of_m_Clip_1() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_Clip_1)); }
	inline TimelineClip_t2478225950 * get_m_Clip_1() const { return ___m_Clip_1; }
	inline TimelineClip_t2478225950 ** get_address_of_m_Clip_1() { return &___m_Clip_1; }
	inline void set_m_Clip_1(TimelineClip_t2478225950 * value)
	{
		___m_Clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_1), value);
	}

	inline static int32_t get_offset_of_m_Playable_2() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_Playable_2)); }
	inline Playable_t459825607  get_m_Playable_2() const { return ___m_Playable_2; }
	inline Playable_t459825607 * get_address_of_m_Playable_2() { return &___m_Playable_2; }
	inline void set_m_Playable_2(Playable_t459825607  value)
	{
		___m_Playable_2 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_3() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_ParentMixer_3)); }
	inline Playable_t459825607  get_m_ParentMixer_3() const { return ___m_ParentMixer_3; }
	inline Playable_t459825607 * get_address_of_m_ParentMixer_3() { return &___m_ParentMixer_3; }
	inline void set_m_ParentMixer_3(Playable_t459825607  value)
	{
		___m_ParentMixer_3 = value;
	}

	inline static int32_t get_offset_of_m_StartDelay_4() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_StartDelay_4)); }
	inline double get_m_StartDelay_4() const { return ___m_StartDelay_4; }
	inline double* get_address_of_m_StartDelay_4() { return &___m_StartDelay_4; }
	inline void set_m_StartDelay_4(double value)
	{
		___m_StartDelay_4 = value;
	}

	inline static int32_t get_offset_of_m_FinishTail_5() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_FinishTail_5)); }
	inline double get_m_FinishTail_5() const { return ___m_FinishTail_5; }
	inline double* get_address_of_m_FinishTail_5() { return &___m_FinishTail_5; }
	inline void set_m_FinishTail_5(double value)
	{
		___m_FinishTail_5 = value;
	}

	inline static int32_t get_offset_of_m_Started_6() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_Started_6)); }
	inline bool get_m_Started_6() const { return ___m_Started_6; }
	inline bool* get_address_of_m_Started_6() { return &___m_Started_6; }
	inline void set_m_Started_6(bool value)
	{
		___m_Started_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULERUNTIMECLIP_T283756268_H
#ifndef ANIMATIONOUTPUTWEIGHTPROCESSOR_T2670190175_H
#define ANIMATIONOUTPUTWEIGHTPROCESSOR_T2670190175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor
struct  AnimationOutputWeightProcessor_t2670190175  : public RuntimeObject
{
public:
	// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Output
	AnimationPlayableOutput_t1918618239  ___m_Output_0;
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Timeline.AnimationOutputWeightProcessor::m_LayerMixer
	AnimationLayerMixerPlayable_t3631223897  ___m_LayerMixer_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo> UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Mixers
	List_1_t3454752663 * ___m_Mixers_2;

public:
	inline static int32_t get_offset_of_m_Output_0() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t2670190175, ___m_Output_0)); }
	inline AnimationPlayableOutput_t1918618239  get_m_Output_0() const { return ___m_Output_0; }
	inline AnimationPlayableOutput_t1918618239 * get_address_of_m_Output_0() { return &___m_Output_0; }
	inline void set_m_Output_0(AnimationPlayableOutput_t1918618239  value)
	{
		___m_Output_0 = value;
	}

	inline static int32_t get_offset_of_m_LayerMixer_1() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t2670190175, ___m_LayerMixer_1)); }
	inline AnimationLayerMixerPlayable_t3631223897  get_m_LayerMixer_1() const { return ___m_LayerMixer_1; }
	inline AnimationLayerMixerPlayable_t3631223897 * get_address_of_m_LayerMixer_1() { return &___m_LayerMixer_1; }
	inline void set_m_LayerMixer_1(AnimationLayerMixerPlayable_t3631223897  value)
	{
		___m_LayerMixer_1 = value;
	}

	inline static int32_t get_offset_of_m_Mixers_2() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t2670190175, ___m_Mixers_2)); }
	inline List_1_t3454752663 * get_m_Mixers_2() const { return ___m_Mixers_2; }
	inline List_1_t3454752663 ** get_address_of_m_Mixers_2() { return &___m_Mixers_2; }
	inline void set_m_Mixers_2(List_1_t3454752663 * value)
	{
		___m_Mixers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mixers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONOUTPUTWEIGHTPROCESSOR_T2670190175_H
#ifndef U3CU3EC__ITERATOR0_T647330842_H
#define U3CU3EC__ITERATOR0_T647330842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t647330842  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::<outputTracks>__1
	TrackAsset_t2828708245 * ___U3CoutputTracksU3E__1_1;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::<output>__2
	PlayableBinding_t354260709  ___U3CoutputU3E__2_3;
	// UnityEngine.Timeline.TimelineAsset UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$this
	TimelineAsset_t3776684190 * ___U24this_4;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_5;
	// System.Boolean UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CoutputTracksU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U3CoutputTracksU3E__1_1)); }
	inline TrackAsset_t2828708245 * get_U3CoutputTracksU3E__1_1() const { return ___U3CoutputTracksU3E__1_1; }
	inline TrackAsset_t2828708245 ** get_address_of_U3CoutputTracksU3E__1_1() { return &___U3CoutputTracksU3E__1_1; }
	inline void set_U3CoutputTracksU3E__1_1(TrackAsset_t2828708245 * value)
	{
		___U3CoutputTracksU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputTracksU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U24locvar1_2)); }
	inline RuntimeObject* get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline RuntimeObject** get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(RuntimeObject* value)
	{
		___U24locvar1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_2), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3E__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U3CoutputU3E__2_3)); }
	inline PlayableBinding_t354260709  get_U3CoutputU3E__2_3() const { return ___U3CoutputU3E__2_3; }
	inline PlayableBinding_t354260709 * get_address_of_U3CoutputU3E__2_3() { return &___U3CoutputU3E__2_3; }
	inline void set_U3CoutputU3E__2_3(PlayableBinding_t354260709  value)
	{
		___U3CoutputU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U24this_4)); }
	inline TimelineAsset_t3776684190 * get_U24this_4() const { return ___U24this_4; }
	inline TimelineAsset_t3776684190 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TimelineAsset_t3776684190 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U24current_5)); }
	inline PlayableBinding_t354260709  get_U24current_5() const { return ___U24current_5; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(PlayableBinding_t354260709  value)
	{
		___U24current_5 = value;
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t647330842, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T647330842_H
#ifndef U3CU3EC__ITERATOR0_T3478306766_H
#define U3CU3EC__ITERATOR0_T3478306766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t3478306766  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.TrackBindingTypeAttribute UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<attribute>__0
	TrackBindingTypeAttribute_t2757649500 * ___U3CattributeU3E__0_0;
	// System.Type UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<trackBindingType>__0
	Type_t * ___U3CtrackBindingTypeU3E__0_1;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$this
	TrackAsset_t2828708245 * ___U24this_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_3;
	// System.Boolean UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CattributeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U3CattributeU3E__0_0)); }
	inline TrackBindingTypeAttribute_t2757649500 * get_U3CattributeU3E__0_0() const { return ___U3CattributeU3E__0_0; }
	inline TrackBindingTypeAttribute_t2757649500 ** get_address_of_U3CattributeU3E__0_0() { return &___U3CattributeU3E__0_0; }
	inline void set_U3CattributeU3E__0_0(TrackBindingTypeAttribute_t2757649500 * value)
	{
		___U3CattributeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CtrackBindingTypeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U3CtrackBindingTypeU3E__0_1)); }
	inline Type_t * get_U3CtrackBindingTypeU3E__0_1() const { return ___U3CtrackBindingTypeU3E__0_1; }
	inline Type_t ** get_address_of_U3CtrackBindingTypeU3E__0_1() { return &___U3CtrackBindingTypeU3E__0_1; }
	inline void set_U3CtrackBindingTypeU3E__0_1(Type_t * value)
	{
		___U3CtrackBindingTypeU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtrackBindingTypeU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24this_2)); }
	inline TrackAsset_t2828708245 * get_U24this_2() const { return ___U24this_2; }
	inline TrackAsset_t2828708245 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TrackAsset_t2828708245 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24current_3)); }
	inline PlayableBinding_t354260709  get_U24current_3() const { return ___U24current_3; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(PlayableBinding_t354260709  value)
	{
		___U24current_3 = value;
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T3478306766_H
#ifndef WEIGHTINFO_T1982677921_H
#define WEIGHTINFO_T1982677921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct  WeightInfo_t1982677921 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::mixer
	Playable_t459825607  ___mixer_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::parentMixer
	Playable_t459825607  ___parentMixer_1;
	// System.Int32 UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::port
	int32_t ___port_2;
	// System.Boolean UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::modulate
	bool ___modulate_3;

public:
	inline static int32_t get_offset_of_mixer_0() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___mixer_0)); }
	inline Playable_t459825607  get_mixer_0() const { return ___mixer_0; }
	inline Playable_t459825607 * get_address_of_mixer_0() { return &___mixer_0; }
	inline void set_mixer_0(Playable_t459825607  value)
	{
		___mixer_0 = value;
	}

	inline static int32_t get_offset_of_parentMixer_1() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___parentMixer_1)); }
	inline Playable_t459825607  get_parentMixer_1() const { return ___parentMixer_1; }
	inline Playable_t459825607 * get_address_of_parentMixer_1() { return &___parentMixer_1; }
	inline void set_parentMixer_1(Playable_t459825607  value)
	{
		___parentMixer_1 = value;
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_modulate_3() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___modulate_3)); }
	inline bool get_modulate_3() const { return ___modulate_3; }
	inline bool* get_address_of_modulate_3() { return &___modulate_3; }
	inline void set_modulate_3(bool value)
	{
		___modulate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t1982677921_marshaled_pinvoke
{
	Playable_t459825607  ___mixer_0;
	Playable_t459825607  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t1982677921_marshaled_com
{
	Playable_t459825607  ___mixer_0;
	Playable_t459825607  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
#endif // WEIGHTINFO_T1982677921_H
#ifndef BASICPLAYABLEBEHAVIOUR_T1482445671_H
#define BASICPLAYABLEBEHAVIOUR_T1482445671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.BasicPlayableBehaviour
struct  BasicPlayableBehaviour_t1482445671  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICPLAYABLEBEHAVIOUR_T1482445671_H
#ifndef U3CU3EC__ITERATOR0_T2611944586_H
#define U3CU3EC__ITERATOR0_T2611944586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2611944586  : public RuntimeObject
{
public:
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_0;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24current_0)); }
	inline PlayableBinding_t354260709  get_U24current_0() const { return ___U24current_0; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(PlayableBinding_t354260709  value)
	{
		___U24current_0 = value;
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2611944586_H
#ifndef ANIMATIONPLAYABLEASSET_T734882934_H
#define ANIMATIONPLAYABLEASSET_T734882934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset
struct  AnimationPlayableAsset_t734882934  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.AnimationClip UnityEngine.Timeline.AnimationPlayableAsset::m_Clip
	AnimationClip_t2318505987 * ___m_Clip_2;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_Position
	Vector3_t3722313464  ___m_Position_3;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationPlayableAsset::m_Rotation
	Quaternion_t2301928331  ___m_Rotation_4;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_UseTrackMatchFields
	bool ___m_UseTrackMatchFields_5;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationPlayableAsset::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_6;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_RemoveStartOffset
	bool ___m_RemoveStartOffset_7;

public:
	inline static int32_t get_offset_of_m_Clip_2() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Clip_2)); }
	inline AnimationClip_t2318505987 * get_m_Clip_2() const { return ___m_Clip_2; }
	inline AnimationClip_t2318505987 ** get_address_of_m_Clip_2() { return &___m_Clip_2; }
	inline void set_m_Clip_2(AnimationClip_t2318505987 * value)
	{
		___m_Clip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_2), value);
	}

	inline static int32_t get_offset_of_m_Position_3() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Position_3)); }
	inline Vector3_t3722313464  get_m_Position_3() const { return ___m_Position_3; }
	inline Vector3_t3722313464 * get_address_of_m_Position_3() { return &___m_Position_3; }
	inline void set_m_Position_3(Vector3_t3722313464  value)
	{
		___m_Position_3 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_4() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Rotation_4)); }
	inline Quaternion_t2301928331  get_m_Rotation_4() const { return ___m_Rotation_4; }
	inline Quaternion_t2301928331 * get_address_of_m_Rotation_4() { return &___m_Rotation_4; }
	inline void set_m_Rotation_4(Quaternion_t2301928331  value)
	{
		___m_Rotation_4 = value;
	}

	inline static int32_t get_offset_of_m_UseTrackMatchFields_5() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_UseTrackMatchFields_5)); }
	inline bool get_m_UseTrackMatchFields_5() const { return ___m_UseTrackMatchFields_5; }
	inline bool* get_address_of_m_UseTrackMatchFields_5() { return &___m_UseTrackMatchFields_5; }
	inline void set_m_UseTrackMatchFields_5(bool value)
	{
		___m_UseTrackMatchFields_5 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_6() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_MatchTargetFields_6)); }
	inline int32_t get_m_MatchTargetFields_6() const { return ___m_MatchTargetFields_6; }
	inline int32_t* get_address_of_m_MatchTargetFields_6() { return &___m_MatchTargetFields_6; }
	inline void set_m_MatchTargetFields_6(int32_t value)
	{
		___m_MatchTargetFields_6 = value;
	}

	inline static int32_t get_offset_of_m_RemoveStartOffset_7() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_RemoveStartOffset_7)); }
	inline bool get_m_RemoveStartOffset_7() const { return ___m_RemoveStartOffset_7; }
	inline bool* get_address_of_m_RemoveStartOffset_7() { return &___m_RemoveStartOffset_7; }
	inline void set_m_RemoveStartOffset_7(bool value)
	{
		___m_RemoveStartOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEASSET_T734882934_H
#ifndef ACTIVATIONPLAYABLEASSET_T2332007632_H
#define ACTIVATIONPLAYABLEASSET_T2332007632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationPlayableAsset
struct  ActivationPlayableAsset_t2332007632  : public PlayableAsset_t3219022681
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONPLAYABLEASSET_T2332007632_H
#ifndef AUDIOPLAYABLEASSET_T2575444864_H
#define AUDIOPLAYABLEASSET_T2575444864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset
struct  AudioPlayableAsset_t2575444864  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.AudioClip UnityEngine.Timeline.AudioPlayableAsset::m_Clip
	AudioClip_t3680889665 * ___m_Clip_2;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset::m_Loop
	bool ___m_Loop_3;
	// System.Single UnityEngine.Timeline.AudioPlayableAsset::m_bufferingTime
	float ___m_bufferingTime_4;

public:
	inline static int32_t get_offset_of_m_Clip_2() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t2575444864, ___m_Clip_2)); }
	inline AudioClip_t3680889665 * get_m_Clip_2() const { return ___m_Clip_2; }
	inline AudioClip_t3680889665 ** get_address_of_m_Clip_2() { return &___m_Clip_2; }
	inline void set_m_Clip_2(AudioClip_t3680889665 * value)
	{
		___m_Clip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_2), value);
	}

	inline static int32_t get_offset_of_m_Loop_3() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t2575444864, ___m_Loop_3)); }
	inline bool get_m_Loop_3() const { return ___m_Loop_3; }
	inline bool* get_address_of_m_Loop_3() { return &___m_Loop_3; }
	inline void set_m_Loop_3(bool value)
	{
		___m_Loop_3 = value;
	}

	inline static int32_t get_offset_of_m_bufferingTime_4() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t2575444864, ___m_bufferingTime_4)); }
	inline float get_m_bufferingTime_4() const { return ___m_bufferingTime_4; }
	inline float* get_address_of_m_bufferingTime_4() { return &___m_bufferingTime_4; }
	inline void set_m_bufferingTime_4(float value)
	{
		___m_bufferingTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEASSET_T2575444864_H
#ifndef TRACKASSET_T2828708245_H
#define TRACKASSET_T2828708245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t2828708245  : public PlayableAsset_t3219022681
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_2;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_3;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_4;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t2318505987 * ___m_AnimClip_5;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t3219022681 * ___m_Parent_6;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t4000433264 * ___m_Children_7;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_8;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t140784555* ___m_ClipsCache_9;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t924799574  ___m_Start_10;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t924799574  ___m_End_11;
	// UnityEngine.Timeline.TimelineAsset/MediaType UnityEngine.Timeline.TrackAsset::m_MediaType
	int32_t ___m_MediaType_12;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_ChildTrackCache
	RuntimeObject* ___m_ChildTrackCache_14;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_t3950300692 * ___m_Clips_16;

public:
	inline static int32_t get_offset_of_m_Locked_2() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Locked_2)); }
	inline bool get_m_Locked_2() const { return ___m_Locked_2; }
	inline bool* get_address_of_m_Locked_2() { return &___m_Locked_2; }
	inline void set_m_Locked_2(bool value)
	{
		___m_Locked_2 = value;
	}

	inline static int32_t get_offset_of_m_Muted_3() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Muted_3)); }
	inline bool get_m_Muted_3() const { return ___m_Muted_3; }
	inline bool* get_address_of_m_Muted_3() { return &___m_Muted_3; }
	inline void set_m_Muted_3(bool value)
	{
		___m_Muted_3 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_4() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_CustomPlayableFullTypename_4)); }
	inline String_t* get_m_CustomPlayableFullTypename_4() const { return ___m_CustomPlayableFullTypename_4; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_4() { return &___m_CustomPlayableFullTypename_4; }
	inline void set_m_CustomPlayableFullTypename_4(String_t* value)
	{
		___m_CustomPlayableFullTypename_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_4), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_5() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_AnimClip_5)); }
	inline AnimationClip_t2318505987 * get_m_AnimClip_5() const { return ___m_AnimClip_5; }
	inline AnimationClip_t2318505987 ** get_address_of_m_AnimClip_5() { return &___m_AnimClip_5; }
	inline void set_m_AnimClip_5(AnimationClip_t2318505987 * value)
	{
		___m_AnimClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_5), value);
	}

	inline static int32_t get_offset_of_m_Parent_6() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Parent_6)); }
	inline PlayableAsset_t3219022681 * get_m_Parent_6() const { return ___m_Parent_6; }
	inline PlayableAsset_t3219022681 ** get_address_of_m_Parent_6() { return &___m_Parent_6; }
	inline void set_m_Parent_6(PlayableAsset_t3219022681 * value)
	{
		___m_Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_6), value);
	}

	inline static int32_t get_offset_of_m_Children_7() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Children_7)); }
	inline List_1_t4000433264 * get_m_Children_7() const { return ___m_Children_7; }
	inline List_1_t4000433264 ** get_address_of_m_Children_7() { return &___m_Children_7; }
	inline void set_m_Children_7(List_1_t4000433264 * value)
	{
		___m_Children_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_7), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_8() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ItemsHash_8)); }
	inline int32_t get_m_ItemsHash_8() const { return ___m_ItemsHash_8; }
	inline int32_t* get_address_of_m_ItemsHash_8() { return &___m_ItemsHash_8; }
	inline void set_m_ItemsHash_8(int32_t value)
	{
		___m_ItemsHash_8 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_9() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ClipsCache_9)); }
	inline TimelineClipU5BU5D_t140784555* get_m_ClipsCache_9() const { return ___m_ClipsCache_9; }
	inline TimelineClipU5BU5D_t140784555** get_address_of_m_ClipsCache_9() { return &___m_ClipsCache_9; }
	inline void set_m_ClipsCache_9(TimelineClipU5BU5D_t140784555* value)
	{
		___m_ClipsCache_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_9), value);
	}

	inline static int32_t get_offset_of_m_Start_10() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Start_10)); }
	inline DiscreteTime_t924799574  get_m_Start_10() const { return ___m_Start_10; }
	inline DiscreteTime_t924799574 * get_address_of_m_Start_10() { return &___m_Start_10; }
	inline void set_m_Start_10(DiscreteTime_t924799574  value)
	{
		___m_Start_10 = value;
	}

	inline static int32_t get_offset_of_m_End_11() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_End_11)); }
	inline DiscreteTime_t924799574  get_m_End_11() const { return ___m_End_11; }
	inline DiscreteTime_t924799574 * get_address_of_m_End_11() { return &___m_End_11; }
	inline void set_m_End_11(DiscreteTime_t924799574  value)
	{
		___m_End_11 = value;
	}

	inline static int32_t get_offset_of_m_MediaType_12() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_MediaType_12)); }
	inline int32_t get_m_MediaType_12() const { return ___m_MediaType_12; }
	inline int32_t* get_address_of_m_MediaType_12() { return &___m_MediaType_12; }
	inline void set_m_MediaType_12(int32_t value)
	{
		___m_MediaType_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildTrackCache_14() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ChildTrackCache_14)); }
	inline RuntimeObject* get_m_ChildTrackCache_14() const { return ___m_ChildTrackCache_14; }
	inline RuntimeObject** get_address_of_m_ChildTrackCache_14() { return &___m_ChildTrackCache_14; }
	inline void set_m_ChildTrackCache_14(RuntimeObject* value)
	{
		___m_ChildTrackCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildTrackCache_14), value);
	}

	inline static int32_t get_offset_of_m_Clips_16() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Clips_16)); }
	inline List_1_t3950300692 * get_m_Clips_16() const { return ___m_Clips_16; }
	inline List_1_t3950300692 ** get_address_of_m_Clips_16() { return &___m_Clips_16; }
	inline void set_m_Clips_16(List_1_t3950300692 * value)
	{
		___m_Clips_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_16), value);
	}
};

struct TrackAsset_t2828708245_StaticFields
{
public:
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TrackAsset::s_EmptyCache
	TrackAssetU5BU5D_t52518008* ___s_EmptyCache_13;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t907029268 * ___s_TrackBindingTypeAttributeCache_15;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t2253157129 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_s_EmptyCache_13() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___s_EmptyCache_13)); }
	inline TrackAssetU5BU5D_t52518008* get_s_EmptyCache_13() const { return ___s_EmptyCache_13; }
	inline TrackAssetU5BU5D_t52518008** get_address_of_s_EmptyCache_13() { return &___s_EmptyCache_13; }
	inline void set_s_EmptyCache_13(TrackAssetU5BU5D_t52518008* value)
	{
		___s_EmptyCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyCache_13), value);
	}

	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_15() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___s_TrackBindingTypeAttributeCache_15)); }
	inline Dictionary_2_t907029268 * get_s_TrackBindingTypeAttributeCache_15() const { return ___s_TrackBindingTypeAttributeCache_15; }
	inline Dictionary_2_t907029268 ** get_address_of_s_TrackBindingTypeAttributeCache_15() { return &___s_TrackBindingTypeAttributeCache_15; }
	inline void set_s_TrackBindingTypeAttributeCache_15(Dictionary_2_t907029268 * value)
	{
		___s_TrackBindingTypeAttributeCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Comparison_1_t2253157129 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Comparison_1_t2253157129 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Comparison_1_t2253157129 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T2828708245_H
#ifndef CONTROLPLAYABLEASSET_T3597738600_H
#define CONTROLPLAYABLEASSET_T3597738600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset
struct  ControlPlayableAsset_t3597738600  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.ExposedReference`1<UnityEngine.GameObject> UnityEngine.Timeline.ControlPlayableAsset::sourceGameObject
	ExposedReference_1_t1378690540  ___sourceGameObject_3;
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset::prefabGameObject
	GameObject_t1113636619 * ___prefabGameObject_4;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateParticle
	bool ___updateParticle_5;
	// System.UInt32 UnityEngine.Timeline.ControlPlayableAsset::particleRandomSeed
	uint32_t ___particleRandomSeed_6;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateDirector
	bool ___updateDirector_7;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateITimeControl
	bool ___updateITimeControl_8;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::searchHierarchy
	bool ___searchHierarchy_9;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::active
	bool ___active_10;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ControlPlayableAsset::postPlayback
	int32_t ___postPlayback_11;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.ControlPlayableAsset::m_ControlDirectorAsset
	PlayableAsset_t3219022681 * ___m_ControlDirectorAsset_12;
	// System.Double UnityEngine.Timeline.ControlPlayableAsset::m_Duration
	double ___m_Duration_13;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::m_SupportLoop
	bool ___m_SupportLoop_14;

public:
	inline static int32_t get_offset_of_sourceGameObject_3() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___sourceGameObject_3)); }
	inline ExposedReference_1_t1378690540  get_sourceGameObject_3() const { return ___sourceGameObject_3; }
	inline ExposedReference_1_t1378690540 * get_address_of_sourceGameObject_3() { return &___sourceGameObject_3; }
	inline void set_sourceGameObject_3(ExposedReference_1_t1378690540  value)
	{
		___sourceGameObject_3 = value;
	}

	inline static int32_t get_offset_of_prefabGameObject_4() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___prefabGameObject_4)); }
	inline GameObject_t1113636619 * get_prefabGameObject_4() const { return ___prefabGameObject_4; }
	inline GameObject_t1113636619 ** get_address_of_prefabGameObject_4() { return &___prefabGameObject_4; }
	inline void set_prefabGameObject_4(GameObject_t1113636619 * value)
	{
		___prefabGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefabGameObject_4), value);
	}

	inline static int32_t get_offset_of_updateParticle_5() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___updateParticle_5)); }
	inline bool get_updateParticle_5() const { return ___updateParticle_5; }
	inline bool* get_address_of_updateParticle_5() { return &___updateParticle_5; }
	inline void set_updateParticle_5(bool value)
	{
		___updateParticle_5 = value;
	}

	inline static int32_t get_offset_of_particleRandomSeed_6() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___particleRandomSeed_6)); }
	inline uint32_t get_particleRandomSeed_6() const { return ___particleRandomSeed_6; }
	inline uint32_t* get_address_of_particleRandomSeed_6() { return &___particleRandomSeed_6; }
	inline void set_particleRandomSeed_6(uint32_t value)
	{
		___particleRandomSeed_6 = value;
	}

	inline static int32_t get_offset_of_updateDirector_7() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___updateDirector_7)); }
	inline bool get_updateDirector_7() const { return ___updateDirector_7; }
	inline bool* get_address_of_updateDirector_7() { return &___updateDirector_7; }
	inline void set_updateDirector_7(bool value)
	{
		___updateDirector_7 = value;
	}

	inline static int32_t get_offset_of_updateITimeControl_8() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___updateITimeControl_8)); }
	inline bool get_updateITimeControl_8() const { return ___updateITimeControl_8; }
	inline bool* get_address_of_updateITimeControl_8() { return &___updateITimeControl_8; }
	inline void set_updateITimeControl_8(bool value)
	{
		___updateITimeControl_8 = value;
	}

	inline static int32_t get_offset_of_searchHierarchy_9() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___searchHierarchy_9)); }
	inline bool get_searchHierarchy_9() const { return ___searchHierarchy_9; }
	inline bool* get_address_of_searchHierarchy_9() { return &___searchHierarchy_9; }
	inline void set_searchHierarchy_9(bool value)
	{
		___searchHierarchy_9 = value;
	}

	inline static int32_t get_offset_of_active_10() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___active_10)); }
	inline bool get_active_10() const { return ___active_10; }
	inline bool* get_address_of_active_10() { return &___active_10; }
	inline void set_active_10(bool value)
	{
		___active_10 = value;
	}

	inline static int32_t get_offset_of_postPlayback_11() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___postPlayback_11)); }
	inline int32_t get_postPlayback_11() const { return ___postPlayback_11; }
	inline int32_t* get_address_of_postPlayback_11() { return &___postPlayback_11; }
	inline void set_postPlayback_11(int32_t value)
	{
		___postPlayback_11 = value;
	}

	inline static int32_t get_offset_of_m_ControlDirectorAsset_12() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___m_ControlDirectorAsset_12)); }
	inline PlayableAsset_t3219022681 * get_m_ControlDirectorAsset_12() const { return ___m_ControlDirectorAsset_12; }
	inline PlayableAsset_t3219022681 ** get_address_of_m_ControlDirectorAsset_12() { return &___m_ControlDirectorAsset_12; }
	inline void set_m_ControlDirectorAsset_12(PlayableAsset_t3219022681 * value)
	{
		___m_ControlDirectorAsset_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlDirectorAsset_12), value);
	}

	inline static int32_t get_offset_of_m_Duration_13() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___m_Duration_13)); }
	inline double get_m_Duration_13() const { return ___m_Duration_13; }
	inline double* get_address_of_m_Duration_13() { return &___m_Duration_13; }
	inline void set_m_Duration_13(double value)
	{
		___m_Duration_13 = value;
	}

	inline static int32_t get_offset_of_m_SupportLoop_14() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___m_SupportLoop_14)); }
	inline bool get_m_SupportLoop_14() const { return ___m_SupportLoop_14; }
	inline bool* get_address_of_m_SupportLoop_14() { return &___m_SupportLoop_14; }
	inline void set_m_SupportLoop_14(bool value)
	{
		___m_SupportLoop_14 = value;
	}
};

struct ControlPlayableAsset_t3597738600_StaticFields
{
public:
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset::k_MaxRandInt
	int32_t ___k_MaxRandInt_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Timeline.ControlPlayableAsset::s_ProcessedDirectors
	HashSet_1_t3368433767 * ___s_ProcessedDirectors_15;

public:
	inline static int32_t get_offset_of_k_MaxRandInt_2() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600_StaticFields, ___k_MaxRandInt_2)); }
	inline int32_t get_k_MaxRandInt_2() const { return ___k_MaxRandInt_2; }
	inline int32_t* get_address_of_k_MaxRandInt_2() { return &___k_MaxRandInt_2; }
	inline void set_k_MaxRandInt_2(int32_t value)
	{
		___k_MaxRandInt_2 = value;
	}

	inline static int32_t get_offset_of_s_ProcessedDirectors_15() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600_StaticFields, ___s_ProcessedDirectors_15)); }
	inline HashSet_1_t3368433767 * get_s_ProcessedDirectors_15() const { return ___s_ProcessedDirectors_15; }
	inline HashSet_1_t3368433767 ** get_address_of_s_ProcessedDirectors_15() { return &___s_ProcessedDirectors_15; }
	inline void set_s_ProcessedDirectors_15(HashSet_1_t3368433767 * value)
	{
		___s_ProcessedDirectors_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_ProcessedDirectors_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPLAYABLEASSET_T3597738600_H
#ifndef VIDEOPLAYER_T1683042537_H
#define VIDEOPLAYER_T1683042537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer
struct  VideoPlayer_t1683042537  : public Behaviour_t1437897464
{
public:
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t3436254912 * ___prepareCompleted_2;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t3436254912 * ___loopPointReached_3;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t3436254912 * ___started_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t3436254912 * ___frameDropped_5;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_t3211687919 * ___errorReceived_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t3436254912 * ___seekCompleted_7;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_t445758600 * ___clockResyncOccurred_8;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t3848515759 * ___frameReady_9;

public:
	inline static int32_t get_offset_of_prepareCompleted_2() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___prepareCompleted_2)); }
	inline EventHandler_t3436254912 * get_prepareCompleted_2() const { return ___prepareCompleted_2; }
	inline EventHandler_t3436254912 ** get_address_of_prepareCompleted_2() { return &___prepareCompleted_2; }
	inline void set_prepareCompleted_2(EventHandler_t3436254912 * value)
	{
		___prepareCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___prepareCompleted_2), value);
	}

	inline static int32_t get_offset_of_loopPointReached_3() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___loopPointReached_3)); }
	inline EventHandler_t3436254912 * get_loopPointReached_3() const { return ___loopPointReached_3; }
	inline EventHandler_t3436254912 ** get_address_of_loopPointReached_3() { return &___loopPointReached_3; }
	inline void set_loopPointReached_3(EventHandler_t3436254912 * value)
	{
		___loopPointReached_3 = value;
		Il2CppCodeGenWriteBarrier((&___loopPointReached_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___started_4)); }
	inline EventHandler_t3436254912 * get_started_4() const { return ___started_4; }
	inline EventHandler_t3436254912 ** get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(EventHandler_t3436254912 * value)
	{
		___started_4 = value;
		Il2CppCodeGenWriteBarrier((&___started_4), value);
	}

	inline static int32_t get_offset_of_frameDropped_5() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameDropped_5)); }
	inline EventHandler_t3436254912 * get_frameDropped_5() const { return ___frameDropped_5; }
	inline EventHandler_t3436254912 ** get_address_of_frameDropped_5() { return &___frameDropped_5; }
	inline void set_frameDropped_5(EventHandler_t3436254912 * value)
	{
		___frameDropped_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameDropped_5), value);
	}

	inline static int32_t get_offset_of_errorReceived_6() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___errorReceived_6)); }
	inline ErrorEventHandler_t3211687919 * get_errorReceived_6() const { return ___errorReceived_6; }
	inline ErrorEventHandler_t3211687919 ** get_address_of_errorReceived_6() { return &___errorReceived_6; }
	inline void set_errorReceived_6(ErrorEventHandler_t3211687919 * value)
	{
		___errorReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&___errorReceived_6), value);
	}

	inline static int32_t get_offset_of_seekCompleted_7() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___seekCompleted_7)); }
	inline EventHandler_t3436254912 * get_seekCompleted_7() const { return ___seekCompleted_7; }
	inline EventHandler_t3436254912 ** get_address_of_seekCompleted_7() { return &___seekCompleted_7; }
	inline void set_seekCompleted_7(EventHandler_t3436254912 * value)
	{
		___seekCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___seekCompleted_7), value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_8() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___clockResyncOccurred_8)); }
	inline TimeEventHandler_t445758600 * get_clockResyncOccurred_8() const { return ___clockResyncOccurred_8; }
	inline TimeEventHandler_t445758600 ** get_address_of_clockResyncOccurred_8() { return &___clockResyncOccurred_8; }
	inline void set_clockResyncOccurred_8(TimeEventHandler_t445758600 * value)
	{
		___clockResyncOccurred_8 = value;
		Il2CppCodeGenWriteBarrier((&___clockResyncOccurred_8), value);
	}

	inline static int32_t get_offset_of_frameReady_9() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameReady_9)); }
	inline FrameReadyEventHandler_t3848515759 * get_frameReady_9() const { return ___frameReady_9; }
	inline FrameReadyEventHandler_t3848515759 ** get_address_of_frameReady_9() { return &___frameReady_9; }
	inline void set_frameReady_9(FrameReadyEventHandler_t3848515759 * value)
	{
		___frameReady_9 = value;
		Il2CppCodeGenWriteBarrier((&___frameReady_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYER_T1683042537_H
#ifndef TIMELINEASSET_T3776684190_H
#define TIMELINEASSET_T3776684190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset
struct  TimelineAsset_t3776684190  : public PlayableAsset_t3219022681
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset::m_NextId
	int32_t ___m_NextId_2;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TimelineAsset::m_Tracks
	List_1_t4000433264 * ___m_Tracks_3;
	// System.Double UnityEngine.Timeline.TimelineAsset::m_FixedDuration
	double ___m_FixedDuration_4;
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TimelineAsset::m_CacheOutputTracks
	TrackAssetU5BU5D_t52518008* ___m_CacheOutputTracks_5;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_CacheRootTracks
	List_1_t5815691 * ___m_CacheRootTracks_6;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_CacheFlattenedTracks
	List_1_t5815691 * ___m_CacheFlattenedTracks_7;
	// UnityEngine.Timeline.TimelineAsset/EditorSettings UnityEngine.Timeline.TimelineAsset::m_EditorSettings
	EditorSettings_t2594721758 * ___m_EditorSettings_8;
	// UnityEngine.Timeline.TimelineAsset/DurationMode UnityEngine.Timeline.TimelineAsset::m_DurationMode
	int32_t ___m_DurationMode_9;

public:
	inline static int32_t get_offset_of_m_NextId_2() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_NextId_2)); }
	inline int32_t get_m_NextId_2() const { return ___m_NextId_2; }
	inline int32_t* get_address_of_m_NextId_2() { return &___m_NextId_2; }
	inline void set_m_NextId_2(int32_t value)
	{
		___m_NextId_2 = value;
	}

	inline static int32_t get_offset_of_m_Tracks_3() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_Tracks_3)); }
	inline List_1_t4000433264 * get_m_Tracks_3() const { return ___m_Tracks_3; }
	inline List_1_t4000433264 ** get_address_of_m_Tracks_3() { return &___m_Tracks_3; }
	inline void set_m_Tracks_3(List_1_t4000433264 * value)
	{
		___m_Tracks_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tracks_3), value);
	}

	inline static int32_t get_offset_of_m_FixedDuration_4() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_FixedDuration_4)); }
	inline double get_m_FixedDuration_4() const { return ___m_FixedDuration_4; }
	inline double* get_address_of_m_FixedDuration_4() { return &___m_FixedDuration_4; }
	inline void set_m_FixedDuration_4(double value)
	{
		___m_FixedDuration_4 = value;
	}

	inline static int32_t get_offset_of_m_CacheOutputTracks_5() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_CacheOutputTracks_5)); }
	inline TrackAssetU5BU5D_t52518008* get_m_CacheOutputTracks_5() const { return ___m_CacheOutputTracks_5; }
	inline TrackAssetU5BU5D_t52518008** get_address_of_m_CacheOutputTracks_5() { return &___m_CacheOutputTracks_5; }
	inline void set_m_CacheOutputTracks_5(TrackAssetU5BU5D_t52518008* value)
	{
		___m_CacheOutputTracks_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheOutputTracks_5), value);
	}

	inline static int32_t get_offset_of_m_CacheRootTracks_6() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_CacheRootTracks_6)); }
	inline List_1_t5815691 * get_m_CacheRootTracks_6() const { return ___m_CacheRootTracks_6; }
	inline List_1_t5815691 ** get_address_of_m_CacheRootTracks_6() { return &___m_CacheRootTracks_6; }
	inline void set_m_CacheRootTracks_6(List_1_t5815691 * value)
	{
		___m_CacheRootTracks_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheRootTracks_6), value);
	}

	inline static int32_t get_offset_of_m_CacheFlattenedTracks_7() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_CacheFlattenedTracks_7)); }
	inline List_1_t5815691 * get_m_CacheFlattenedTracks_7() const { return ___m_CacheFlattenedTracks_7; }
	inline List_1_t5815691 ** get_address_of_m_CacheFlattenedTracks_7() { return &___m_CacheFlattenedTracks_7; }
	inline void set_m_CacheFlattenedTracks_7(List_1_t5815691 * value)
	{
		___m_CacheFlattenedTracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheFlattenedTracks_7), value);
	}

	inline static int32_t get_offset_of_m_EditorSettings_8() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_EditorSettings_8)); }
	inline EditorSettings_t2594721758 * get_m_EditorSettings_8() const { return ___m_EditorSettings_8; }
	inline EditorSettings_t2594721758 ** get_address_of_m_EditorSettings_8() { return &___m_EditorSettings_8; }
	inline void set_m_EditorSettings_8(EditorSettings_t2594721758 * value)
	{
		___m_EditorSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_EditorSettings_8), value);
	}

	inline static int32_t get_offset_of_m_DurationMode_9() { return static_cast<int32_t>(offsetof(TimelineAsset_t3776684190, ___m_DurationMode_9)); }
	inline int32_t get_m_DurationMode_9() const { return ___m_DurationMode_9; }
	inline int32_t* get_address_of_m_DurationMode_9() { return &___m_DurationMode_9; }
	inline void set_m_DurationMode_9(int32_t value)
	{
		___m_DurationMode_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEASSET_T3776684190_H
#ifndef GROUPTRACK_T2222291233_H
#define GROUPTRACK_T2222291233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.GroupTrack
struct  GroupTrack_t2222291233  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPTRACK_T2222291233_H
#ifndef ACTIVATIONTRACK_T4003533622_H
#define ACTIVATIONTRACK_T4003533622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack
struct  ActivationTrack_t4003533622  : public TrackAsset_t2828708245
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationTrack::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_18;
	// UnityEngine.Timeline.ActivationMixerPlayable UnityEngine.Timeline.ActivationTrack::m_ActivationMixer
	ActivationMixerPlayable_t2212523045 * ___m_ActivationMixer_19;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_18() { return static_cast<int32_t>(offsetof(ActivationTrack_t4003533622, ___m_PostPlaybackState_18)); }
	inline int32_t get_m_PostPlaybackState_18() const { return ___m_PostPlaybackState_18; }
	inline int32_t* get_address_of_m_PostPlaybackState_18() { return &___m_PostPlaybackState_18; }
	inline void set_m_PostPlaybackState_18(int32_t value)
	{
		___m_PostPlaybackState_18 = value;
	}

	inline static int32_t get_offset_of_m_ActivationMixer_19() { return static_cast<int32_t>(offsetof(ActivationTrack_t4003533622, ___m_ActivationMixer_19)); }
	inline ActivationMixerPlayable_t2212523045 * get_m_ActivationMixer_19() const { return ___m_ActivationMixer_19; }
	inline ActivationMixerPlayable_t2212523045 ** get_address_of_m_ActivationMixer_19() { return &___m_ActivationMixer_19; }
	inline void set_m_ActivationMixer_19(ActivationMixerPlayable_t2212523045 * value)
	{
		___m_ActivationMixer_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivationMixer_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONTRACK_T4003533622_H
#ifndef PLAYABLETRACK_T2508018810_H
#define PLAYABLETRACK_T2508018810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PlayableTrack
struct  PlayableTrack_t2508018810  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLETRACK_T2508018810_H
#ifndef CONTROLTRACK_T3308451304_H
#define CONTROLTRACK_T3308451304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlTrack
struct  ControlTrack_t3308451304  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTRACK_T3308451304_H
#ifndef ANIMATIONTRACK_T3872729528_H
#define ANIMATIONTRACK_T3872729528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack
struct  AnimationTrack_t3872729528  : public TrackAsset_t2828708245
{
public:
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPreExtrapolation
	int32_t ___m_OpenClipPreExtrapolation_18;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPostExtrapolation
	int32_t ___m_OpenClipPostExtrapolation_19;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetPosition
	Vector3_t3722313464  ___m_OpenClipOffsetPosition_20;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetRotation
	Quaternion_t2301928331  ___m_OpenClipOffsetRotation_21;
	// System.Double UnityEngine.Timeline.AnimationTrack::m_OpenClipTimeOffset
	double ___m_OpenClipTimeOffset_22;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationTrack::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_23;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_Position
	Vector3_t3722313464  ___m_Position_24;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_Rotation
	Quaternion_t2301928331  ___m_Rotation_25;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyOffsets
	bool ___m_ApplyOffsets_26;
	// UnityEngine.AvatarMask UnityEngine.Timeline.AnimationTrack::m_AvatarMask
	AvatarMask_t1182482518 * ___m_AvatarMask_27;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyAvatarMask
	bool ___m_ApplyAvatarMask_28;

public:
	inline static int32_t get_offset_of_m_OpenClipPreExtrapolation_18() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipPreExtrapolation_18)); }
	inline int32_t get_m_OpenClipPreExtrapolation_18() const { return ___m_OpenClipPreExtrapolation_18; }
	inline int32_t* get_address_of_m_OpenClipPreExtrapolation_18() { return &___m_OpenClipPreExtrapolation_18; }
	inline void set_m_OpenClipPreExtrapolation_18(int32_t value)
	{
		___m_OpenClipPreExtrapolation_18 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipPostExtrapolation_19() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipPostExtrapolation_19)); }
	inline int32_t get_m_OpenClipPostExtrapolation_19() const { return ___m_OpenClipPostExtrapolation_19; }
	inline int32_t* get_address_of_m_OpenClipPostExtrapolation_19() { return &___m_OpenClipPostExtrapolation_19; }
	inline void set_m_OpenClipPostExtrapolation_19(int32_t value)
	{
		___m_OpenClipPostExtrapolation_19 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetPosition_20() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipOffsetPosition_20)); }
	inline Vector3_t3722313464  get_m_OpenClipOffsetPosition_20() const { return ___m_OpenClipOffsetPosition_20; }
	inline Vector3_t3722313464 * get_address_of_m_OpenClipOffsetPosition_20() { return &___m_OpenClipOffsetPosition_20; }
	inline void set_m_OpenClipOffsetPosition_20(Vector3_t3722313464  value)
	{
		___m_OpenClipOffsetPosition_20 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetRotation_21() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipOffsetRotation_21)); }
	inline Quaternion_t2301928331  get_m_OpenClipOffsetRotation_21() const { return ___m_OpenClipOffsetRotation_21; }
	inline Quaternion_t2301928331 * get_address_of_m_OpenClipOffsetRotation_21() { return &___m_OpenClipOffsetRotation_21; }
	inline void set_m_OpenClipOffsetRotation_21(Quaternion_t2301928331  value)
	{
		___m_OpenClipOffsetRotation_21 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipTimeOffset_22() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipTimeOffset_22)); }
	inline double get_m_OpenClipTimeOffset_22() const { return ___m_OpenClipTimeOffset_22; }
	inline double* get_address_of_m_OpenClipTimeOffset_22() { return &___m_OpenClipTimeOffset_22; }
	inline void set_m_OpenClipTimeOffset_22(double value)
	{
		___m_OpenClipTimeOffset_22 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_23() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_MatchTargetFields_23)); }
	inline int32_t get_m_MatchTargetFields_23() const { return ___m_MatchTargetFields_23; }
	inline int32_t* get_address_of_m_MatchTargetFields_23() { return &___m_MatchTargetFields_23; }
	inline void set_m_MatchTargetFields_23(int32_t value)
	{
		___m_MatchTargetFields_23 = value;
	}

	inline static int32_t get_offset_of_m_Position_24() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_Position_24)); }
	inline Vector3_t3722313464  get_m_Position_24() const { return ___m_Position_24; }
	inline Vector3_t3722313464 * get_address_of_m_Position_24() { return &___m_Position_24; }
	inline void set_m_Position_24(Vector3_t3722313464  value)
	{
		___m_Position_24 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_25() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_Rotation_25)); }
	inline Quaternion_t2301928331  get_m_Rotation_25() const { return ___m_Rotation_25; }
	inline Quaternion_t2301928331 * get_address_of_m_Rotation_25() { return &___m_Rotation_25; }
	inline void set_m_Rotation_25(Quaternion_t2301928331  value)
	{
		___m_Rotation_25 = value;
	}

	inline static int32_t get_offset_of_m_ApplyOffsets_26() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_ApplyOffsets_26)); }
	inline bool get_m_ApplyOffsets_26() const { return ___m_ApplyOffsets_26; }
	inline bool* get_address_of_m_ApplyOffsets_26() { return &___m_ApplyOffsets_26; }
	inline void set_m_ApplyOffsets_26(bool value)
	{
		___m_ApplyOffsets_26 = value;
	}

	inline static int32_t get_offset_of_m_AvatarMask_27() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_AvatarMask_27)); }
	inline AvatarMask_t1182482518 * get_m_AvatarMask_27() const { return ___m_AvatarMask_27; }
	inline AvatarMask_t1182482518 ** get_address_of_m_AvatarMask_27() { return &___m_AvatarMask_27; }
	inline void set_m_AvatarMask_27(AvatarMask_t1182482518 * value)
	{
		___m_AvatarMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AvatarMask_27), value);
	}

	inline static int32_t get_offset_of_m_ApplyAvatarMask_28() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_ApplyAvatarMask_28)); }
	inline bool get_m_ApplyAvatarMask_28() const { return ___m_ApplyAvatarMask_28; }
	inline bool* get_address_of_m_ApplyAvatarMask_28() { return &___m_ApplyAvatarMask_28; }
	inline void set_m_ApplyAvatarMask_28(bool value)
	{
		___m_ApplyAvatarMask_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRACK_T3872729528_H
#ifndef AUDIOTRACK_T1549411460_H
#define AUDIOTRACK_T1549411460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack
struct  AudioTrack_t1549411460  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTRACK_T1549411460_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3601[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (AnalyticsSessionState_t681173134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3603[5] = 
{
	AnalyticsSessionState_t681173134::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (AnalyticsSessionInfo_t2322308579), -1, sizeof(AnalyticsSessionInfo_t2322308579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3604[1] = 
{
	AnalyticsSessionInfo_t2322308579_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (UnityWebRequestMultimedia_t3655636800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (DownloadHandlerAudioClip_t3167892435), sizeof(DownloadHandlerAudioClip_t3167892435_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (VideoPlayer_t1683042537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[8] = 
{
	VideoPlayer_t1683042537::get_offset_of_prepareCompleted_2(),
	VideoPlayer_t1683042537::get_offset_of_loopPointReached_3(),
	VideoPlayer_t1683042537::get_offset_of_started_4(),
	VideoPlayer_t1683042537::get_offset_of_frameDropped_5(),
	VideoPlayer_t1683042537::get_offset_of_errorReceived_6(),
	VideoPlayer_t1683042537::get_offset_of_seekCompleted_7(),
	VideoPlayer_t1683042537::get_offset_of_clockResyncOccurred_8(),
	VideoPlayer_t1683042537::get_offset_of_frameReady_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (EventHandler_t3436254912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (ErrorEventHandler_t3211687919), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (FrameReadyEventHandler_t3848515759), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (TimeEventHandler_t445758600), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (VideoClipPlayable_t2598186649)+ sizeof (RuntimeObject), sizeof(VideoClipPlayable_t2598186649 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3615[1] = 
{
	VideoClipPlayable_t2598186649::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (U3CModuleU3E_t692745551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (ClipCaps_t3444432629)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3618[8] = 
{
	ClipCaps_t3444432629::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (TimelineClipCapsExtensions_t3963340085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (DiscreteTime_t924799574)+ sizeof (RuntimeObject), sizeof(DiscreteTime_t924799574 ), sizeof(DiscreteTime_t924799574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3620[3] = 
{
	0,
	DiscreteTime_t924799574_StaticFields::get_offset_of_kMaxTime_1(),
	DiscreteTime_t924799574::get_offset_of_m_DiscreteTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (GroupTrack_t2222291233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (TimelineAsset_t3776684190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[8] = 
{
	TimelineAsset_t3776684190::get_offset_of_m_NextId_2(),
	TimelineAsset_t3776684190::get_offset_of_m_Tracks_3(),
	TimelineAsset_t3776684190::get_offset_of_m_FixedDuration_4(),
	TimelineAsset_t3776684190::get_offset_of_m_CacheOutputTracks_5(),
	TimelineAsset_t3776684190::get_offset_of_m_CacheRootTracks_6(),
	TimelineAsset_t3776684190::get_offset_of_m_CacheFlattenedTracks_7(),
	TimelineAsset_t3776684190::get_offset_of_m_EditorSettings_8(),
	TimelineAsset_t3776684190::get_offset_of_m_DurationMode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (MediaType_t4163677969)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3623[8] = 
{
	MediaType_t4163677969::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (DurationMode_t2670267584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3624[3] = 
{
	DurationMode_t2670267584::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (EditorSettings_t2594721758), -1, sizeof(EditorSettings_t2594721758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3625[4] = 
{
	EditorSettings_t2594721758_StaticFields::get_offset_of_kMinFps_0(),
	EditorSettings_t2594721758_StaticFields::get_offset_of_kMaxFps_1(),
	EditorSettings_t2594721758_StaticFields::get_offset_of_kDefaultFps_2(),
	EditorSettings_t2594721758::get_offset_of_m_Framerate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (U3CU3Ec__Iterator0_t647330842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[8] = 
{
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U3CoutputTracksU3E__1_1(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U3CoutputU3E__2_3(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t647330842::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (TrackClipTypeAttribute_t4223207965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[1] = 
{
	TrackClipTypeAttribute_t4223207965::get_offset_of_inspectedType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (TrackMediaType_t482867812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[1] = 
{
	TrackMediaType_t482867812::get_offset_of_m_MediaType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (NotKeyableAttribute_t1406439677), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (TrackBindingTypeAttribute_t2757649500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[1] = 
{
	TrackBindingTypeAttribute_t2757649500::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (SupportsChildTracksAttribute_t205780379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[2] = 
{
	SupportsChildTracksAttribute_t205780379::get_offset_of_childType_0(),
	SupportsChildTracksAttribute_t205780379::get_offset_of_levels_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (IgnoreOnPlayableTrackAttribute_t3455747041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (TimeFieldAttribute_t3035049162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (TimelineClip_t2478225950), -1, sizeof(TimelineClip_t2478225950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3636[31] = 
{
	TimelineClip_t2478225950_StaticFields::get_offset_of_kDefaultClipCaps_0(),
	TimelineClip_t2478225950_StaticFields::get_offset_of_kDefaultClipDurationInSeconds_1(),
	TimelineClip_t2478225950_StaticFields::get_offset_of_kTimeScaleMin_2(),
	TimelineClip_t2478225950_StaticFields::get_offset_of_kTimeScaleMax_3(),
	TimelineClip_t2478225950_StaticFields::get_offset_of_kMinDuration_4(),
	0,
	TimelineClip_t2478225950_StaticFields::get_offset_of_kMaxTimeValue_6(),
	TimelineClip_t2478225950::get_offset_of_m_Start_7(),
	TimelineClip_t2478225950::get_offset_of_m_ClipIn_8(),
	TimelineClip_t2478225950::get_offset_of_m_Asset_9(),
	TimelineClip_t2478225950::get_offset_of_m_Duration_10(),
	TimelineClip_t2478225950::get_offset_of_m_TimeScale_11(),
	TimelineClip_t2478225950::get_offset_of_m_ParentTrack_12(),
	TimelineClip_t2478225950::get_offset_of_m_EaseInDuration_13(),
	TimelineClip_t2478225950::get_offset_of_m_EaseOutDuration_14(),
	TimelineClip_t2478225950::get_offset_of_m_BlendInDuration_15(),
	TimelineClip_t2478225950::get_offset_of_m_BlendOutDuration_16(),
	TimelineClip_t2478225950::get_offset_of_m_MixInCurve_17(),
	TimelineClip_t2478225950::get_offset_of_m_MixOutCurve_18(),
	TimelineClip_t2478225950::get_offset_of_m_BlendInCurveMode_19(),
	TimelineClip_t2478225950::get_offset_of_m_BlendOutCurveMode_20(),
	TimelineClip_t2478225950::get_offset_of_m_ExposedParameterNames_21(),
	TimelineClip_t2478225950::get_offset_of_m_AnimationCurves_22(),
	TimelineClip_t2478225950::get_offset_of_m_Recordable_23(),
	TimelineClip_t2478225950::get_offset_of_m_PostExtrapolationMode_24(),
	TimelineClip_t2478225950::get_offset_of_m_PreExtrapolationMode_25(),
	TimelineClip_t2478225950::get_offset_of_m_PostExtrapolationTime_26(),
	TimelineClip_t2478225950::get_offset_of_m_PreExtrapolationTime_27(),
	TimelineClip_t2478225950::get_offset_of_m_DisplayName_28(),
	TimelineClip_t2478225950::get_offset_of_m_Version_29(),
	TimelineClip_t2478225950::get_offset_of_U3CdirtyHashU3Ek__BackingField_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (ClipExtrapolation_t324732067)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3637[6] = 
{
	ClipExtrapolation_t324732067::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (BlendCurveMode_t1741293302)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3638[3] = 
{
	BlendCurveMode_t1741293302::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (TimelineMarker_t2195886951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3639[2] = 
{
	TimelineMarker_t2195886951::get_offset_of_m_Key_0(),
	TimelineMarker_t2195886951::get_offset_of_m_Time_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (TimelinePlayable_t2938744123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[6] = 
{
	TimelinePlayable_t2938744123::get_offset_of_m_IntervalTree_0(),
	TimelinePlayable_t2938744123::get_offset_of_m_ActiveClips_1(),
	TimelinePlayable_t2938744123::get_offset_of_m_CurrentListOfActiveClips_2(),
	TimelinePlayable_t2938744123::get_offset_of_m_ActiveBit_3(),
	TimelinePlayable_t2938744123::get_offset_of_m_EvaluateCallbacks_4(),
	TimelinePlayable_t2938744123::get_offset_of_m_PlayableCache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (ConnectionCache_t1120597666)+ sizeof (RuntimeObject), sizeof(ConnectionCache_t1120597666 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3642[4] = 
{
	ConnectionCache_t1120597666::get_offset_of_playable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionCache_t1120597666::get_offset_of_port_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionCache_t1120597666::get_offset_of_parent_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionCache_t1120597666::get_offset_of_evalWeight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (TrackAsset_t2828708245), -1, sizeof(TrackAsset_t2828708245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3643[16] = 
{
	TrackAsset_t2828708245::get_offset_of_m_Locked_2(),
	TrackAsset_t2828708245::get_offset_of_m_Muted_3(),
	TrackAsset_t2828708245::get_offset_of_m_CustomPlayableFullTypename_4(),
	TrackAsset_t2828708245::get_offset_of_m_AnimClip_5(),
	TrackAsset_t2828708245::get_offset_of_m_Parent_6(),
	TrackAsset_t2828708245::get_offset_of_m_Children_7(),
	TrackAsset_t2828708245::get_offset_of_m_ItemsHash_8(),
	TrackAsset_t2828708245::get_offset_of_m_ClipsCache_9(),
	TrackAsset_t2828708245::get_offset_of_m_Start_10(),
	TrackAsset_t2828708245::get_offset_of_m_End_11(),
	TrackAsset_t2828708245::get_offset_of_m_MediaType_12(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_s_EmptyCache_13(),
	TrackAsset_t2828708245::get_offset_of_m_ChildTrackCache_14(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_s_TrackBindingTypeAttributeCache_15(),
	TrackAsset_t2828708245::get_offset_of_m_Clips_16(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (U3CU3Ec__Iterator0_t3478306766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[6] = 
{
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U3CattributeU3E__0_0(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U3CtrackBindingTypeU3E__0_1(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (ActivationMixerPlayable_t2212523045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[3] = 
{
	ActivationMixerPlayable_t2212523045::get_offset_of_m_PostPlaybackState_0(),
	ActivationMixerPlayable_t2212523045::get_offset_of_m_BoundGameObjectInitialStateIsActive_1(),
	ActivationMixerPlayable_t2212523045::get_offset_of_m_BoundGameObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (ActivationPlayableAsset_t2332007632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (ActivationTrack_t4003533622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[2] = 
{
	ActivationTrack_t4003533622::get_offset_of_m_PostPlaybackState_18(),
	ActivationTrack_t4003533622::get_offset_of_m_ActivationMixer_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (PostPlaybackState_t4203018085)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3648[5] = 
{
	PostPlaybackState_t4203018085::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (AnimationOutputWeightProcessor_t2670190175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[3] = 
{
	AnimationOutputWeightProcessor_t2670190175::get_offset_of_m_Output_0(),
	AnimationOutputWeightProcessor_t2670190175::get_offset_of_m_LayerMixer_1(),
	AnimationOutputWeightProcessor_t2670190175::get_offset_of_m_Mixers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (WeightInfo_t1982677921)+ sizeof (RuntimeObject), sizeof(WeightInfo_t1982677921_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3650[4] = 
{
	WeightInfo_t1982677921::get_offset_of_mixer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t1982677921::get_offset_of_parentMixer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t1982677921::get_offset_of_port_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t1982677921::get_offset_of_modulate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (AnimationPlayableAsset_t734882934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[6] = 
{
	AnimationPlayableAsset_t734882934::get_offset_of_m_Clip_2(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_Position_3(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_Rotation_4(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_UseTrackMatchFields_5(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_MatchTargetFields_6(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_RemoveStartOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (U3CU3Ec__Iterator0_t4165699274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[3] = 
{
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (MatchTargetFields_t3959953937)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3653[7] = 
{
	MatchTargetFields_t3959953937::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (MatchTargetFieldConstants_t2682568617), -1, sizeof(MatchTargetFieldConstants_t2682568617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3654[4] = 
{
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_All_0(),
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_None_1(),
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_Position_2(),
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_Rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (AnimationTrack_t3872729528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[11] = 
{
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipPreExtrapolation_18(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipPostExtrapolation_19(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipOffsetPosition_20(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipOffsetRotation_21(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipTimeOffset_22(),
	AnimationTrack_t3872729528::get_offset_of_m_MatchTargetFields_23(),
	AnimationTrack_t3872729528::get_offset_of_m_Position_24(),
	AnimationTrack_t3872729528::get_offset_of_m_Rotation_25(),
	AnimationTrack_t3872729528::get_offset_of_m_ApplyOffsets_26(),
	AnimationTrack_t3872729528::get_offset_of_m_AvatarMask_27(),
	AnimationTrack_t3872729528::get_offset_of_m_ApplyAvatarMask_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (U3CU3Ec__Iterator0_t1104488729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[4] = 
{
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (TrackColorAttribute_t2898993029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3657[1] = 
{
	TrackColorAttribute_t2898993029::get_offset_of_m_Color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (AudioPlayableAsset_t2575444864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[3] = 
{
	AudioPlayableAsset_t2575444864::get_offset_of_m_Clip_2(),
	AudioPlayableAsset_t2575444864::get_offset_of_m_Loop_3(),
	AudioPlayableAsset_t2575444864::get_offset_of_m_bufferingTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (U3CU3Ec__Iterator0_t2611944586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[3] = 
{
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (AudioTrack_t1549411460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (U3CU3Ec__Iterator0_t717920108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[4] = 
{
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (ControlPlayableAsset_t3597738600), -1, sizeof(ControlPlayableAsset_t3597738600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3662[14] = 
{
	ControlPlayableAsset_t3597738600_StaticFields::get_offset_of_k_MaxRandInt_2(),
	ControlPlayableAsset_t3597738600::get_offset_of_sourceGameObject_3(),
	ControlPlayableAsset_t3597738600::get_offset_of_prefabGameObject_4(),
	ControlPlayableAsset_t3597738600::get_offset_of_updateParticle_5(),
	ControlPlayableAsset_t3597738600::get_offset_of_particleRandomSeed_6(),
	ControlPlayableAsset_t3597738600::get_offset_of_updateDirector_7(),
	ControlPlayableAsset_t3597738600::get_offset_of_updateITimeControl_8(),
	ControlPlayableAsset_t3597738600::get_offset_of_searchHierarchy_9(),
	ControlPlayableAsset_t3597738600::get_offset_of_active_10(),
	ControlPlayableAsset_t3597738600::get_offset_of_postPlayback_11(),
	ControlPlayableAsset_t3597738600::get_offset_of_m_ControlDirectorAsset_12(),
	ControlPlayableAsset_t3597738600::get_offset_of_m_Duration_13(),
	ControlPlayableAsset_t3597738600::get_offset_of_m_SupportLoop_14(),
	ControlPlayableAsset_t3597738600_StaticFields::get_offset_of_s_ProcessedDirectors_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (U3CGetControlableScriptsU3Ec__Iterator0_t3018919025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3663[7] = 
{
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_root_0(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24locvar0_1(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24locvar1_2(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U3CscriptU3E__1_3(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24current_4(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24disposing_5(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (ControlTrack_t3308451304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (InfiniteRuntimeClip_t156380358), -1, sizeof(InfiniteRuntimeClip_t156380358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3665[2] = 
{
	InfiniteRuntimeClip_t156380358::get_offset_of_m_Playable_1(),
	InfiniteRuntimeClip_t156380358_StaticFields::get_offset_of_kIntervalEnd_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (RuntimeClip_t583365242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[3] = 
{
	RuntimeClip_t583365242::get_offset_of_m_Clip_1(),
	RuntimeClip_t583365242::get_offset_of_m_Playable_2(),
	RuntimeClip_t583365242::get_offset_of_m_ParentMixer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (RuntimeClipBase_t197358283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (RuntimeElement_t2068542217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[1] = 
{
	RuntimeElement_t2068542217::get_offset_of_U3CintervalBitU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (ScheduleRuntimeClip_t283756268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[6] = 
{
	ScheduleRuntimeClip_t283756268::get_offset_of_m_Clip_1(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_Playable_2(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_ParentMixer_3(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_StartDelay_4(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_FinishTail_5(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_Started_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (ActivationControlPlayable_t426148305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[2] = 
{
	ActivationControlPlayable_t426148305::get_offset_of_gameObject_0(),
	ActivationControlPlayable_t426148305::get_offset_of_postPlayback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (PostPlaybackState_t1986969933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3674[4] = 
{
	PostPlaybackState_t1986969933::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (BasicPlayableBehaviour_t1482445671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (DirectorControlPlayable_t2642348650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[2] = 
{
	DirectorControlPlayable_t2642348650::get_offset_of_director_0(),
	DirectorControlPlayable_t2642348650::get_offset_of_m_SyncTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (ParticleControlPlayable_t3220249377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[4] = 
{
	ParticleControlPlayable_t3220249377::get_offset_of_m_LastTime_0(),
	ParticleControlPlayable_t3220249377::get_offset_of_m_RandomSeed_1(),
	ParticleControlPlayable_t3220249377::get_offset_of_m_SystemTime_2(),
	ParticleControlPlayable_t3220249377::get_offset_of_U3CparticleSystemU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (PrefabControlPlayable_t3058657425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[1] = 
{
	PrefabControlPlayable_t3058657425::get_offset_of_m_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (TimeControlPlayable_t1010398267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[1] = 
{
	TimeControlPlayable_t1010398267::get_offset_of_m_timeControl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (PlayableTrack_t2508018810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (Extrapolation_t625958692), -1, sizeof(Extrapolation_t625958692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3682[2] = 
{
	Extrapolation_t625958692_StaticFields::get_offset_of_kMinExtrapolationTime_0(),
	Extrapolation_t625958692_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (HashUtility_t2883916303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (TimelineCreateUtilities_t4099873628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[2] = 
{
	U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780::get_offset_of_prefix_0(),
	U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780::get_offset_of_newName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (TimelineUndo_t898409293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (TimeUtility_t877350212), -1, sizeof(TimeUtility_t877350212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3690[3] = 
{
	TimeUtility_t877350212_StaticFields::get_offset_of_kTimeEpsilon_0(),
	TimeUtility_t877350212_StaticFields::get_offset_of_kFrameRateEpsilon_1(),
	TimeUtility_t877350212_StaticFields::get_offset_of_k_MaxTimelineDurationInSeconds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (WeightUtility_t2276937658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (EventHandle_t600343995)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3693[3] = 
{
	EventHandle_t600343995::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
