﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Hashtable
struct Hashtable_t1853889766;
// MS.Internal.Xml.XPath.XPathScanner
struct XPathScanner_t3283201025;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1515527577;
// System.Threading.Thread
struct Thread_t2300836069;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.IO.KqueueMonitor
struct KqueueMonitor_t3818269198;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.IO.SearchPattern2/Op
struct Op_t3134810481;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.IFileWatcher
struct IFileWatcher_t3899097327;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t47339301;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Xml.XPath.XPathDocument
struct XPathDocument_t1673143697;
// MS.Internal.Xml.Cache.XPathNodePageInfo
struct XPathNodePageInfo_t2343388010;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.IO.InotifyData
struct InotifyData_t2533354870;
// System.IO.FileSystemWatcher
struct FileSystemWatcher_t416760199;
// MS.Internal.Xml.XPath.AstNode
struct AstNode_t2514041814;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t1632274355;
// System.Void
struct Void_t1185182177;
// System.Xml.XPath.XPathNavigatorKeyComparer
struct XPathNavigatorKeyComparer_t2518900029;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct XPathNodeInfoAtom_t1760358141;
// System.Exception
struct Exception_t;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IO.Compression.DeflateStream
struct DeflateStream_t4175168077;
// System.IO.kevent[]
struct keventU5BU5D_t21523777;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Collections.Generic.Dictionary`2<System.String,System.IO.PathData>
struct Dictionary_2_t3002451328;
// System.Collections.Generic.Dictionary`2<System.Int32,System.IO.PathData>
struct Dictionary_2_t2105908360;
// System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite
struct UnmanagedReadOrWrite_t1975956110;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.Compression.DeflateStreamNative
struct DeflateStreamNative_t1405046456;
// MS.Internal.Xml.XPath.Operator/Op[]
struct OpU5BU5D_t2837398892;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IO.ErrorEventArgs
struct ErrorEventArgs_t1584858912;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.IO.RenamedEventArgs
struct RenamedEventArgs_t2350765466;
// System.ComponentModel.ISynchronizeInvoke
struct ISynchronizeInvoke_t1357618335;
// System.IO.SearchPattern2
struct SearchPattern2_t2824637351;
// System.IO.FileSystemEventHandler
struct FileSystemEventHandler_t1806121106;
// System.IO.ErrorEventHandler
struct ErrorEventHandler_t2621677363;
// System.IO.RenamedEventHandler
struct RenamedEventHandler_t3047461033;
// System.IO.FileSystemEventArgs
struct FileSystemEventArgs_t1603777841;

struct XPathNode_t2208072876_marshaled_pinvoke;
struct XPathNode_t2208072876_marshaled_com;



#ifndef U3CMODULEU3E_T692745529_H
#define U3CMODULEU3E_T692745529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745529 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745529_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef KEVENTWATCHER_T3355132332_H
#define KEVENTWATCHER_T3355132332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KeventWatcher
struct  KeventWatcher_t3355132332  : public RuntimeObject
{
public:

public:
};

struct KeventWatcher_t3355132332_StaticFields
{
public:
	// System.Boolean System.IO.KeventWatcher::failed
	bool ___failed_0;
	// System.IO.KeventWatcher System.IO.KeventWatcher::instance
	KeventWatcher_t3355132332 * ___instance_1;
	// System.Collections.Hashtable System.IO.KeventWatcher::watches
	Hashtable_t1853889766 * ___watches_2;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___instance_1)); }
	inline KeventWatcher_t3355132332 * get_instance_1() const { return ___instance_1; }
	inline KeventWatcher_t3355132332 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(KeventWatcher_t3355132332 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEVENTWATCHER_T3355132332_H
#ifndef XPATHNODEHELPER_T2230825274_H
#define XPATHNODEHELPER_T2230825274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeHelper
struct  XPathNodeHelper_t2230825274  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEHELPER_T2230825274_H
#ifndef XPATHPARSER_T618394529_H
#define XPATHPARSER_T618394529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser
struct  XPathParser_t618394529  : public RuntimeObject
{
public:
	// MS.Internal.Xml.XPath.XPathScanner MS.Internal.Xml.XPath.XPathParser::scanner
	XPathScanner_t3283201025 * ___scanner_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser::parseDepth
	int32_t ___parseDepth_1;

public:
	inline static int32_t get_offset_of_scanner_0() { return static_cast<int32_t>(offsetof(XPathParser_t618394529, ___scanner_0)); }
	inline XPathScanner_t3283201025 * get_scanner_0() const { return ___scanner_0; }
	inline XPathScanner_t3283201025 ** get_address_of_scanner_0() { return &___scanner_0; }
	inline void set_scanner_0(XPathScanner_t3283201025 * value)
	{
		___scanner_0 = value;
		Il2CppCodeGenWriteBarrier((&___scanner_0), value);
	}

	inline static int32_t get_offset_of_parseDepth_1() { return static_cast<int32_t>(offsetof(XPathParser_t618394529, ___parseDepth_1)); }
	inline int32_t get_parseDepth_1() const { return ___parseDepth_1; }
	inline int32_t* get_address_of_parseDepth_1() { return &___parseDepth_1; }
	inline void set_parseDepth_1(int32_t value)
	{
		___parseDepth_1 = value;
	}
};

struct XPathParser_t618394529_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray1
	XPathResultTypeU5BU5D_t1515527577* ___temparray1_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray2
	XPathResultTypeU5BU5D_t1515527577* ___temparray2_3;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray3
	XPathResultTypeU5BU5D_t1515527577* ___temparray3_4;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray4
	XPathResultTypeU5BU5D_t1515527577* ___temparray4_5;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray5
	XPathResultTypeU5BU5D_t1515527577* ___temparray5_6;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray6
	XPathResultTypeU5BU5D_t1515527577* ___temparray6_7;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray7
	XPathResultTypeU5BU5D_t1515527577* ___temparray7_8;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray8
	XPathResultTypeU5BU5D_t1515527577* ___temparray8_9;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray9
	XPathResultTypeU5BU5D_t1515527577* ___temparray9_10;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::functionTable
	Hashtable_t1853889766 * ___functionTable_11;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::AxesTable
	Hashtable_t1853889766 * ___AxesTable_12;

public:
	inline static int32_t get_offset_of_temparray1_2() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray1_2)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray1_2() const { return ___temparray1_2; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray1_2() { return &___temparray1_2; }
	inline void set_temparray1_2(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray1_2 = value;
		Il2CppCodeGenWriteBarrier((&___temparray1_2), value);
	}

	inline static int32_t get_offset_of_temparray2_3() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray2_3)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray2_3() const { return ___temparray2_3; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray2_3() { return &___temparray2_3; }
	inline void set_temparray2_3(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray2_3 = value;
		Il2CppCodeGenWriteBarrier((&___temparray2_3), value);
	}

	inline static int32_t get_offset_of_temparray3_4() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray3_4)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray3_4() const { return ___temparray3_4; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray3_4() { return &___temparray3_4; }
	inline void set_temparray3_4(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray3_4 = value;
		Il2CppCodeGenWriteBarrier((&___temparray3_4), value);
	}

	inline static int32_t get_offset_of_temparray4_5() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray4_5)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray4_5() const { return ___temparray4_5; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray4_5() { return &___temparray4_5; }
	inline void set_temparray4_5(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray4_5 = value;
		Il2CppCodeGenWriteBarrier((&___temparray4_5), value);
	}

	inline static int32_t get_offset_of_temparray5_6() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray5_6)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray5_6() const { return ___temparray5_6; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray5_6() { return &___temparray5_6; }
	inline void set_temparray5_6(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray5_6 = value;
		Il2CppCodeGenWriteBarrier((&___temparray5_6), value);
	}

	inline static int32_t get_offset_of_temparray6_7() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray6_7)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray6_7() const { return ___temparray6_7; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray6_7() { return &___temparray6_7; }
	inline void set_temparray6_7(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray6_7 = value;
		Il2CppCodeGenWriteBarrier((&___temparray6_7), value);
	}

	inline static int32_t get_offset_of_temparray7_8() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray7_8)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray7_8() const { return ___temparray7_8; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray7_8() { return &___temparray7_8; }
	inline void set_temparray7_8(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray7_8 = value;
		Il2CppCodeGenWriteBarrier((&___temparray7_8), value);
	}

	inline static int32_t get_offset_of_temparray8_9() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray8_9)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray8_9() const { return ___temparray8_9; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray8_9() { return &___temparray8_9; }
	inline void set_temparray8_9(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray8_9 = value;
		Il2CppCodeGenWriteBarrier((&___temparray8_9), value);
	}

	inline static int32_t get_offset_of_temparray9_10() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray9_10)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray9_10() const { return ___temparray9_10; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray9_10() { return &___temparray9_10; }
	inline void set_temparray9_10(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray9_10 = value;
		Il2CppCodeGenWriteBarrier((&___temparray9_10), value);
	}

	inline static int32_t get_offset_of_functionTable_11() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___functionTable_11)); }
	inline Hashtable_t1853889766 * get_functionTable_11() const { return ___functionTable_11; }
	inline Hashtable_t1853889766 ** get_address_of_functionTable_11() { return &___functionTable_11; }
	inline void set_functionTable_11(Hashtable_t1853889766 * value)
	{
		___functionTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___functionTable_11), value);
	}

	inline static int32_t get_offset_of_AxesTable_12() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___AxesTable_12)); }
	inline Hashtable_t1853889766 * get_AxesTable_12() const { return ___AxesTable_12; }
	inline Hashtable_t1853889766 ** get_address_of_AxesTable_12() { return &___AxesTable_12; }
	inline void set_AxesTable_12(Hashtable_t1853889766 * value)
	{
		___AxesTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___AxesTable_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHPARSER_T618394529_H
#ifndef ASTNODE_T2514041814_H
#define ASTNODE_T2514041814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode
struct  AstNode_t2514041814  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTNODE_T2514041814_H
#ifndef SR_T167583545_H
#define SR_T167583545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t167583545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T167583545_H
#ifndef DEFAULTWATCHER_T2229106420_H
#define DEFAULTWATCHER_T2229106420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DefaultWatcher
struct  DefaultWatcher_t2229106420  : public RuntimeObject
{
public:

public:
};

struct DefaultWatcher_t2229106420_StaticFields
{
public:
	// System.IO.DefaultWatcher System.IO.DefaultWatcher::instance
	DefaultWatcher_t2229106420 * ___instance_0;
	// System.Threading.Thread System.IO.DefaultWatcher::thread
	Thread_t2300836069 * ___thread_1;
	// System.Collections.Hashtable System.IO.DefaultWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.String[] System.IO.DefaultWatcher::NoStringsArray
	StringU5BU5D_t1281789340* ___NoStringsArray_3;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___instance_0)); }
	inline DefaultWatcher_t2229106420 * get_instance_0() const { return ___instance_0; }
	inline DefaultWatcher_t2229106420 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(DefaultWatcher_t2229106420 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}

	inline static int32_t get_offset_of_thread_1() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___thread_1)); }
	inline Thread_t2300836069 * get_thread_1() const { return ___thread_1; }
	inline Thread_t2300836069 ** get_address_of_thread_1() { return &___thread_1; }
	inline void set_thread_1(Thread_t2300836069 * value)
	{
		___thread_1 = value;
		Il2CppCodeGenWriteBarrier((&___thread_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}

	inline static int32_t get_offset_of_NoStringsArray_3() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___NoStringsArray_3)); }
	inline StringU5BU5D_t1281789340* get_NoStringsArray_3() const { return ___NoStringsArray_3; }
	inline StringU5BU5D_t1281789340** get_address_of_NoStringsArray_3() { return &___NoStringsArray_3; }
	inline void set_NoStringsArray_3(StringU5BU5D_t1281789340* value)
	{
		___NoStringsArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___NoStringsArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTWATCHER_T2229106420_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T2361320332_H
#define U3CU3EC__DISPLAYCLASS11_0_T2361320332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KqueueMonitor/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t2361320332  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> System.IO.KqueueMonitor/<>c__DisplayClass11_0::newFds
	List_1_t128053199 * ___newFds_0;
	// System.IO.KqueueMonitor System.IO.KqueueMonitor/<>c__DisplayClass11_0::<>4__this
	KqueueMonitor_t3818269198 * ___U3CU3E4__this_1;
	// System.Action`1<System.String> System.IO.KqueueMonitor/<>c__DisplayClass11_0::<>9__0
	Action_1_t2019918284 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_newFds_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2361320332, ___newFds_0)); }
	inline List_1_t128053199 * get_newFds_0() const { return ___newFds_0; }
	inline List_1_t128053199 ** get_address_of_newFds_0() { return &___newFds_0; }
	inline void set_newFds_0(List_1_t128053199 * value)
	{
		___newFds_0 = value;
		Il2CppCodeGenWriteBarrier((&___newFds_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2361320332, ___U3CU3E4__this_1)); }
	inline KqueueMonitor_t3818269198 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline KqueueMonitor_t3818269198 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(KqueueMonitor_t3818269198 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2361320332, ___U3CU3E9__0_2)); }
	inline Action_1_t2019918284 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Action_1_t2019918284 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T2361320332_H
#ifndef WINDOWSWATCHER_T1958179572_H
#define WINDOWSWATCHER_T1958179572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WindowsWatcher
struct  WindowsWatcher_t1958179572  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSWATCHER_T1958179572_H
#ifndef SEARCHPATTERN2_T2824637351_H
#define SEARCHPATTERN2_T2824637351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2
struct  SearchPattern2_t2824637351  : public RuntimeObject
{
public:
	// System.IO.SearchPattern2/Op System.IO.SearchPattern2::ops
	Op_t3134810481 * ___ops_0;
	// System.Boolean System.IO.SearchPattern2::ignore
	bool ___ignore_1;
	// System.Boolean System.IO.SearchPattern2::hasWildcard
	bool ___hasWildcard_2;
	// System.String System.IO.SearchPattern2::pattern
	String_t* ___pattern_3;

public:
	inline static int32_t get_offset_of_ops_0() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___ops_0)); }
	inline Op_t3134810481 * get_ops_0() const { return ___ops_0; }
	inline Op_t3134810481 ** get_address_of_ops_0() { return &___ops_0; }
	inline void set_ops_0(Op_t3134810481 * value)
	{
		___ops_0 = value;
		Il2CppCodeGenWriteBarrier((&___ops_0), value);
	}

	inline static int32_t get_offset_of_ignore_1() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___ignore_1)); }
	inline bool get_ignore_1() const { return ___ignore_1; }
	inline bool* get_address_of_ignore_1() { return &___ignore_1; }
	inline void set_ignore_1(bool value)
	{
		___ignore_1 = value;
	}

	inline static int32_t get_offset_of_hasWildcard_2() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___hasWildcard_2)); }
	inline bool get_hasWildcard_2() const { return ___hasWildcard_2; }
	inline bool* get_address_of_hasWildcard_2() { return &___hasWildcard_2; }
	inline void set_hasWildcard_2(bool value)
	{
		___hasWildcard_2 = value;
	}

	inline static int32_t get_offset_of_pattern_3() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___pattern_3)); }
	inline String_t* get_pattern_3() const { return ___pattern_3; }
	inline String_t** get_address_of_pattern_3() { return &___pattern_3; }
	inline void set_pattern_3(String_t* value)
	{
		___pattern_3 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_3), value);
	}
};

struct SearchPattern2_t2824637351_StaticFields
{
public:
	// System.Char[] System.IO.SearchPattern2::WildcardChars
	CharU5BU5D_t3528271667* ___WildcardChars_4;
	// System.Char[] System.IO.SearchPattern2::InvalidChars
	CharU5BU5D_t3528271667* ___InvalidChars_5;

public:
	inline static int32_t get_offset_of_WildcardChars_4() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351_StaticFields, ___WildcardChars_4)); }
	inline CharU5BU5D_t3528271667* get_WildcardChars_4() const { return ___WildcardChars_4; }
	inline CharU5BU5D_t3528271667** get_address_of_WildcardChars_4() { return &___WildcardChars_4; }
	inline void set_WildcardChars_4(CharU5BU5D_t3528271667* value)
	{
		___WildcardChars_4 = value;
		Il2CppCodeGenWriteBarrier((&___WildcardChars_4), value);
	}

	inline static int32_t get_offset_of_InvalidChars_5() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351_StaticFields, ___InvalidChars_5)); }
	inline CharU5BU5D_t3528271667* get_InvalidChars_5() const { return ___InvalidChars_5; }
	inline CharU5BU5D_t3528271667** get_address_of_InvalidChars_5() { return &___InvalidChars_5; }
	inline void set_InvalidChars_5(CharU5BU5D_t3528271667* value)
	{
		___InvalidChars_5 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidChars_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHPATTERN2_T2824637351_H
#ifndef NULLFILEWATCHER_T1718214284_H
#define NULLFILEWATCHER_T1718214284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NullFileWatcher
struct  NullFileWatcher_t1718214284  : public RuntimeObject
{
public:

public:
};

struct NullFileWatcher_t1718214284_StaticFields
{
public:
	// System.IO.IFileWatcher System.IO.NullFileWatcher::instance
	RuntimeObject* ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(NullFileWatcher_t1718214284_StaticFields, ___instance_0)); }
	inline RuntimeObject* get_instance_0() const { return ___instance_0; }
	inline RuntimeObject** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject* value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLFILEWATCHER_T1718214284_H
#ifndef XPATHNODEPAGEINFO_T2343388010_H
#define XPATHNODEPAGEINFO_T2343388010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodePageInfo
struct  XPathNodePageInfo_t2343388010  : public RuntimeObject
{
public:
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::pageNum
	int32_t ___pageNum_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::nodeCount
	int32_t ___nodeCount_1;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodePageInfo::pageNext
	XPathNodeU5BU5D_t47339301* ___pageNext_2;

public:
	inline static int32_t get_offset_of_pageNum_0() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t2343388010, ___pageNum_0)); }
	inline int32_t get_pageNum_0() const { return ___pageNum_0; }
	inline int32_t* get_address_of_pageNum_0() { return &___pageNum_0; }
	inline void set_pageNum_0(int32_t value)
	{
		___pageNum_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t2343388010, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}

	inline static int32_t get_offset_of_pageNext_2() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t2343388010, ___pageNext_2)); }
	inline XPathNodeU5BU5D_t47339301* get_pageNext_2() const { return ___pageNext_2; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageNext_2() { return &___pageNext_2; }
	inline void set_pageNext_2(XPathNodeU5BU5D_t47339301* value)
	{
		___pageNext_2 = value;
		Il2CppCodeGenWriteBarrier((&___pageNext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEPAGEINFO_T2343388010_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	ServerIdentity_t2342208608 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	ServerIdentity_t2342208608 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef INCREMENTALREADDECODER_T3011954239_H
#define INCREMENTALREADDECODER_T3011954239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.IncrementalReadDecoder
struct  IncrementalReadDecoder_t3011954239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADDECODER_T3011954239_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef BINARYCOMPATIBILITY_T2660327299_H
#define BINARYCOMPATIBILITY_T2660327299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinaryCompatibility
struct  BinaryCompatibility_t2660327299  : public RuntimeObject
{
public:

public:
};

struct BinaryCompatibility_t2660327299_StaticFields
{
public:
	// System.Boolean System.Xml.BinaryCompatibility::_targetsAtLeast_Desktop_V4_5_2
	bool ____targetsAtLeast_Desktop_V4_5_2_0;

public:
	inline static int32_t get_offset_of__targetsAtLeast_Desktop_V4_5_2_0() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t2660327299_StaticFields, ____targetsAtLeast_Desktop_V4_5_2_0)); }
	inline bool get__targetsAtLeast_Desktop_V4_5_2_0() const { return ____targetsAtLeast_Desktop_V4_5_2_0; }
	inline bool* get_address_of__targetsAtLeast_Desktop_V4_5_2_0() { return &____targetsAtLeast_Desktop_V4_5_2_0; }
	inline void set__targetsAtLeast_Desktop_V4_5_2_0(bool value)
	{
		____targetsAtLeast_Desktop_V4_5_2_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCOMPATIBILITY_T2660327299_H
#ifndef BITS_T3566938933_H
#define BITS_T3566938933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Bits
struct  Bits_t3566938933  : public RuntimeObject
{
public:

public:
};

struct Bits_t3566938933_StaticFields
{
public:
	// System.UInt32 System.Xml.Bits::MASK_0101010101010101
	uint32_t ___MASK_0101010101010101_0;
	// System.UInt32 System.Xml.Bits::MASK_0011001100110011
	uint32_t ___MASK_0011001100110011_1;
	// System.UInt32 System.Xml.Bits::MASK_0000111100001111
	uint32_t ___MASK_0000111100001111_2;
	// System.UInt32 System.Xml.Bits::MASK_0000000011111111
	uint32_t ___MASK_0000000011111111_3;
	// System.UInt32 System.Xml.Bits::MASK_1111111111111111
	uint32_t ___MASK_1111111111111111_4;

public:
	inline static int32_t get_offset_of_MASK_0101010101010101_0() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0101010101010101_0)); }
	inline uint32_t get_MASK_0101010101010101_0() const { return ___MASK_0101010101010101_0; }
	inline uint32_t* get_address_of_MASK_0101010101010101_0() { return &___MASK_0101010101010101_0; }
	inline void set_MASK_0101010101010101_0(uint32_t value)
	{
		___MASK_0101010101010101_0 = value;
	}

	inline static int32_t get_offset_of_MASK_0011001100110011_1() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0011001100110011_1)); }
	inline uint32_t get_MASK_0011001100110011_1() const { return ___MASK_0011001100110011_1; }
	inline uint32_t* get_address_of_MASK_0011001100110011_1() { return &___MASK_0011001100110011_1; }
	inline void set_MASK_0011001100110011_1(uint32_t value)
	{
		___MASK_0011001100110011_1 = value;
	}

	inline static int32_t get_offset_of_MASK_0000111100001111_2() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0000111100001111_2)); }
	inline uint32_t get_MASK_0000111100001111_2() const { return ___MASK_0000111100001111_2; }
	inline uint32_t* get_address_of_MASK_0000111100001111_2() { return &___MASK_0000111100001111_2; }
	inline void set_MASK_0000111100001111_2(uint32_t value)
	{
		___MASK_0000111100001111_2 = value;
	}

	inline static int32_t get_offset_of_MASK_0000000011111111_3() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0000000011111111_3)); }
	inline uint32_t get_MASK_0000000011111111_3() const { return ___MASK_0000000011111111_3; }
	inline uint32_t* get_address_of_MASK_0000000011111111_3() { return &___MASK_0000000011111111_3; }
	inline void set_MASK_0000000011111111_3(uint32_t value)
	{
		___MASK_0000000011111111_3 = value;
	}

	inline static int32_t get_offset_of_MASK_1111111111111111_4() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_1111111111111111_4)); }
	inline uint32_t get_MASK_1111111111111111_4() const { return ___MASK_1111111111111111_4; }
	inline uint32_t* get_address_of_MASK_1111111111111111_4() { return &___MASK_1111111111111111_4; }
	inline void set_MASK_1111111111111111_4(uint32_t value)
	{
		___MASK_1111111111111111_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITS_T3566938933_H
#ifndef BINHEXENCODER_T1687308627_H
#define BINHEXENCODER_T1687308627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexEncoder
struct  BinHexEncoder_t1687308627  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXENCODER_T1687308627_H
#ifndef BASE64ENCODER_T3938083961_H
#define BASE64ENCODER_T3938083961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Base64Encoder
struct  Base64Encoder_t3938083961  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.Base64Encoder::leftOverBytes
	ByteU5BU5D_t4116647657* ___leftOverBytes_0;
	// System.Int32 System.Xml.Base64Encoder::leftOverBytesCount
	int32_t ___leftOverBytesCount_1;
	// System.Char[] System.Xml.Base64Encoder::charsLine
	CharU5BU5D_t3528271667* ___charsLine_2;

public:
	inline static int32_t get_offset_of_leftOverBytes_0() { return static_cast<int32_t>(offsetof(Base64Encoder_t3938083961, ___leftOverBytes_0)); }
	inline ByteU5BU5D_t4116647657* get_leftOverBytes_0() const { return ___leftOverBytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_leftOverBytes_0() { return &___leftOverBytes_0; }
	inline void set_leftOverBytes_0(ByteU5BU5D_t4116647657* value)
	{
		___leftOverBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftOverBytes_0), value);
	}

	inline static int32_t get_offset_of_leftOverBytesCount_1() { return static_cast<int32_t>(offsetof(Base64Encoder_t3938083961, ___leftOverBytesCount_1)); }
	inline int32_t get_leftOverBytesCount_1() const { return ___leftOverBytesCount_1; }
	inline int32_t* get_address_of_leftOverBytesCount_1() { return &___leftOverBytesCount_1; }
	inline void set_leftOverBytesCount_1(int32_t value)
	{
		___leftOverBytesCount_1 = value;
	}

	inline static int32_t get_offset_of_charsLine_2() { return static_cast<int32_t>(offsetof(Base64Encoder_t3938083961, ___charsLine_2)); }
	inline CharU5BU5D_t3528271667* get_charsLine_2() const { return ___charsLine_2; }
	inline CharU5BU5D_t3528271667** get_address_of_charsLine_2() { return &___charsLine_2; }
	inline void set_charsLine_2(CharU5BU5D_t3528271667* value)
	{
		___charsLine_2 = value;
		Il2CppCodeGenWriteBarrier((&___charsLine_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_T3938083961_H
#ifndef RES_T3627928856_H
#define RES_T3627928856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Res
struct  Res_t3627928856  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RES_T3627928856_H
#ifndef XPATHNODEINFOATOM_T1760358141_H
#define XPATHNODEINFOATOM_T1760358141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct  XPathNodeInfoAtom_t1760358141  : public RuntimeObject
{
public:
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::localName
	String_t* ___localName_0;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::baseUri
	String_t* ___baseUri_3;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageParent
	XPathNodeU5BU5D_t47339301* ___pageParent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSibling
	XPathNodeU5BU5D_t47339301* ___pageSibling_5;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSimilar
	XPathNodeU5BU5D_t47339301* ___pageSimilar_6;
	// System.Xml.XPath.XPathDocument MS.Internal.Xml.Cache.XPathNodeInfoAtom::doc
	XPathDocument_t1673143697 * ___doc_7;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::lineNumBase
	int32_t ___lineNumBase_8;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::linePosBase
	int32_t ___linePosBase_9;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::hashCode
	int32_t ___hashCode_10;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::localNameHash
	int32_t ___localNameHash_11;
	// MS.Internal.Xml.Cache.XPathNodePageInfo MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageInfo
	XPathNodePageInfo_t2343388010 * ___pageInfo_12;

public:
	inline static int32_t get_offset_of_localName_0() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___localName_0)); }
	inline String_t* get_localName_0() const { return ___localName_0; }
	inline String_t** get_address_of_localName_0() { return &___localName_0; }
	inline void set_localName_0(String_t* value)
	{
		___localName_0 = value;
		Il2CppCodeGenWriteBarrier((&___localName_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_baseUri_3() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___baseUri_3)); }
	inline String_t* get_baseUri_3() const { return ___baseUri_3; }
	inline String_t** get_address_of_baseUri_3() { return &___baseUri_3; }
	inline void set_baseUri_3(String_t* value)
	{
		___baseUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_3), value);
	}

	inline static int32_t get_offset_of_pageParent_4() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageParent_4)); }
	inline XPathNodeU5BU5D_t47339301* get_pageParent_4() const { return ___pageParent_4; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageParent_4() { return &___pageParent_4; }
	inline void set_pageParent_4(XPathNodeU5BU5D_t47339301* value)
	{
		___pageParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageParent_4), value);
	}

	inline static int32_t get_offset_of_pageSibling_5() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageSibling_5)); }
	inline XPathNodeU5BU5D_t47339301* get_pageSibling_5() const { return ___pageSibling_5; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageSibling_5() { return &___pageSibling_5; }
	inline void set_pageSibling_5(XPathNodeU5BU5D_t47339301* value)
	{
		___pageSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageSibling_5), value);
	}

	inline static int32_t get_offset_of_pageSimilar_6() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageSimilar_6)); }
	inline XPathNodeU5BU5D_t47339301* get_pageSimilar_6() const { return ___pageSimilar_6; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageSimilar_6() { return &___pageSimilar_6; }
	inline void set_pageSimilar_6(XPathNodeU5BU5D_t47339301* value)
	{
		___pageSimilar_6 = value;
		Il2CppCodeGenWriteBarrier((&___pageSimilar_6), value);
	}

	inline static int32_t get_offset_of_doc_7() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___doc_7)); }
	inline XPathDocument_t1673143697 * get_doc_7() const { return ___doc_7; }
	inline XPathDocument_t1673143697 ** get_address_of_doc_7() { return &___doc_7; }
	inline void set_doc_7(XPathDocument_t1673143697 * value)
	{
		___doc_7 = value;
		Il2CppCodeGenWriteBarrier((&___doc_7), value);
	}

	inline static int32_t get_offset_of_lineNumBase_8() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___lineNumBase_8)); }
	inline int32_t get_lineNumBase_8() const { return ___lineNumBase_8; }
	inline int32_t* get_address_of_lineNumBase_8() { return &___lineNumBase_8; }
	inline void set_lineNumBase_8(int32_t value)
	{
		___lineNumBase_8 = value;
	}

	inline static int32_t get_offset_of_linePosBase_9() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___linePosBase_9)); }
	inline int32_t get_linePosBase_9() const { return ___linePosBase_9; }
	inline int32_t* get_address_of_linePosBase_9() { return &___linePosBase_9; }
	inline void set_linePosBase_9(int32_t value)
	{
		___linePosBase_9 = value;
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_localNameHash_11() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___localNameHash_11)); }
	inline int32_t get_localNameHash_11() const { return ___localNameHash_11; }
	inline int32_t* get_address_of_localNameHash_11() { return &___localNameHash_11; }
	inline void set_localNameHash_11(int32_t value)
	{
		___localNameHash_11 = value;
	}

	inline static int32_t get_offset_of_pageInfo_12() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageInfo_12)); }
	inline XPathNodePageInfo_t2343388010 * get_pageInfo_12() const { return ___pageInfo_12; }
	inline XPathNodePageInfo_t2343388010 ** get_address_of_pageInfo_12() { return &___pageInfo_12; }
	inline void set_pageInfo_12(XPathNodePageInfo_t2343388010 * value)
	{
		___pageInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEINFOATOM_T1760358141_H
#ifndef PATHDATA_T3217195029_H
#define PATHDATA_T3217195029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.PathData
struct  PathData_t3217195029  : public RuntimeObject
{
public:
	// System.String System.IO.PathData::Path
	String_t* ___Path_0;
	// System.Boolean System.IO.PathData::IsDirectory
	bool ___IsDirectory_1;
	// System.Int32 System.IO.PathData::Fd
	int32_t ___Fd_2;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(PathData_t3217195029, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((&___Path_0), value);
	}

	inline static int32_t get_offset_of_IsDirectory_1() { return static_cast<int32_t>(offsetof(PathData_t3217195029, ___IsDirectory_1)); }
	inline bool get_IsDirectory_1() const { return ___IsDirectory_1; }
	inline bool* get_address_of_IsDirectory_1() { return &___IsDirectory_1; }
	inline void set_IsDirectory_1(bool value)
	{
		___IsDirectory_1 = value;
	}

	inline static int32_t get_offset_of_Fd_2() { return static_cast<int32_t>(offsetof(PathData_t3217195029, ___Fd_2)); }
	inline int32_t get_Fd_2() const { return ___Fd_2; }
	inline int32_t* get_address_of_Fd_2() { return &___Fd_2; }
	inline void set_Fd_2(int32_t value)
	{
		___Fd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHDATA_T3217195029_H
#ifndef XPATHITEM_T4250588140_H
#define XPATHITEM_T4250588140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t4250588140  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T4250588140_H
#ifndef PARENTINOTIFYDATA_T1149622319_H
#define PARENTINOTIFYDATA_T1149622319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ParentInotifyData
struct  ParentInotifyData_t1149622319  : public RuntimeObject
{
public:
	// System.Boolean System.IO.ParentInotifyData::IncludeSubdirs
	bool ___IncludeSubdirs_0;
	// System.Boolean System.IO.ParentInotifyData::Enabled
	bool ___Enabled_1;
	// System.Collections.ArrayList System.IO.ParentInotifyData::children
	ArrayList_t2718874744 * ___children_2;
	// System.IO.InotifyData System.IO.ParentInotifyData::data
	InotifyData_t2533354870 * ___data_3;

public:
	inline static int32_t get_offset_of_IncludeSubdirs_0() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___IncludeSubdirs_0)); }
	inline bool get_IncludeSubdirs_0() const { return ___IncludeSubdirs_0; }
	inline bool* get_address_of_IncludeSubdirs_0() { return &___IncludeSubdirs_0; }
	inline void set_IncludeSubdirs_0(bool value)
	{
		___IncludeSubdirs_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_children_2() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___children_2)); }
	inline ArrayList_t2718874744 * get_children_2() const { return ___children_2; }
	inline ArrayList_t2718874744 ** get_address_of_children_2() { return &___children_2; }
	inline void set_children_2(ArrayList_t2718874744 * value)
	{
		___children_2 = value;
		Il2CppCodeGenWriteBarrier((&___children_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___data_3)); }
	inline InotifyData_t2533354870 * get_data_3() const { return ___data_3; }
	inline InotifyData_t2533354870 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(InotifyData_t2533354870 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTINOTIFYDATA_T1149622319_H
#ifndef INOTIFYDATA_T2533354870_H
#define INOTIFYDATA_T2533354870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyData
struct  InotifyData_t2533354870  : public RuntimeObject
{
public:
	// System.IO.FileSystemWatcher System.IO.InotifyData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.InotifyData::Directory
	String_t* ___Directory_1;
	// System.Int32 System.IO.InotifyData::Watch
	int32_t ___Watch_2;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier((&___FSW_0), value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier((&___Directory_1), value);
	}

	inline static int32_t get_offset_of_Watch_2() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___Watch_2)); }
	inline int32_t get_Watch_2() const { return ___Watch_2; }
	inline int32_t* get_address_of_Watch_2() { return &___Watch_2; }
	inline void set_Watch_2(int32_t value)
	{
		___Watch_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INOTIFYDATA_T2533354870_H
#ifndef U3CGETENUMERATORU3ED__18_T986872067_H
#define U3CGETENUMERATORU3ED__18_T986872067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyWatcher/<GetEnumerator>d__18
struct  U3CGetEnumeratorU3Ed__18_t986872067  : public RuntimeObject
{
public:
	// System.Int32 System.IO.InotifyWatcher/<GetEnumerator>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object System.IO.InotifyWatcher/<GetEnumerator>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 System.IO.InotifyWatcher/<GetEnumerator>d__18::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Object System.IO.InotifyWatcher/<GetEnumerator>d__18::source
	RuntimeObject * ___source_3;
	// System.Object System.IO.InotifyWatcher/<GetEnumerator>d__18::<>3__source
	RuntimeObject * ___U3CU3E3__source_4;
	// System.Collections.ArrayList System.IO.InotifyWatcher/<GetEnumerator>d__18::<list>5__1
	ArrayList_t2718874744 * ___U3ClistU3E5__1_5;
	// System.Int32 System.IO.InotifyWatcher/<GetEnumerator>d__18::<i>5__2
	int32_t ___U3CiU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___source_3)); }
	inline RuntimeObject * get_source_3() const { return ___source_3; }
	inline RuntimeObject ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__source_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3E3__source_4)); }
	inline RuntimeObject * get_U3CU3E3__source_4() const { return ___U3CU3E3__source_4; }
	inline RuntimeObject ** get_address_of_U3CU3E3__source_4() { return &___U3CU3E3__source_4; }
	inline void set_U3CU3E3__source_4(RuntimeObject * value)
	{
		___U3CU3E3__source_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__source_4), value);
	}

	inline static int32_t get_offset_of_U3ClistU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3ClistU3E5__1_5)); }
	inline ArrayList_t2718874744 * get_U3ClistU3E5__1_5() const { return ___U3ClistU3E5__1_5; }
	inline ArrayList_t2718874744 ** get_address_of_U3ClistU3E5__1_5() { return &___U3ClistU3E5__1_5; }
	inline void set_U3ClistU3E5__1_5(ArrayList_t2718874744 * value)
	{
		___U3ClistU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CiU3E5__2_6)); }
	inline int32_t get_U3CiU3E5__2_6() const { return ___U3CiU3E5__2_6; }
	inline int32_t* get_address_of_U3CiU3E5__2_6() { return &___U3CiU3E5__2_6; }
	inline void set_U3CiU3E5__2_6(int32_t value)
	{
		___U3CiU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__18_T986872067_H
#ifndef __STATICARRAYINITTYPESIZEU3D14_T3517563373_H
#define __STATICARRAYINITTYPESIZEU3D14_T3517563373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14
struct  __StaticArrayInitTypeSizeU3D14_t3517563373 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t3517563373__padding[14];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D14_T3517563373_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T2711125391_H
#define __STATICARRAYINITTYPESIZEU3D32_T2711125391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t2711125391 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t2711125391__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T2711125391_H
#ifndef ROOT_T720671714_H
#define ROOT_T720671714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Root
struct  Root_t720671714  : public AstNode_t2514041814
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOT_T720671714_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994318_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994318 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994318__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994318_H
#ifndef __STATICARRAYINITTYPESIZEU3D9_T3218278900_H
#define __STATICARRAYINITTYPESIZEU3D9_T3218278900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9
struct  __StaticArrayInitTypeSizeU3D9_t3218278900 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D9_t3218278900__padding[9];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D9_T3218278900_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T3217689075_H
#define __STATICARRAYINITTYPESIZEU3D6_T3217689075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_t3217689075 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t3217689075__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T3217689075_H
#ifndef __STATICARRAYINITTYPESIZEU3D3_T3217885684_H
#define __STATICARRAYINITTYPESIZEU3D3_T3217885684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3
struct  __StaticArrayInitTypeSizeU3D3_t3217885684 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_t3217885684__padding[3];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3_T3217885684_H
#ifndef FAMREQUEST_T3578860103_H
#define FAMREQUEST_T3578860103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMRequest
struct  FAMRequest_t3578860103 
{
public:
	// System.Int32 System.IO.FAMRequest::ReqNum
	int32_t ___ReqNum_0;

public:
	inline static int32_t get_offset_of_ReqNum_0() { return static_cast<int32_t>(offsetof(FAMRequest_t3578860103, ___ReqNum_0)); }
	inline int32_t get_ReqNum_0() const { return ___ReqNum_0; }
	inline int32_t* get_address_of_ReqNum_0() { return &___ReqNum_0; }
	inline void set_ReqNum_0(int32_t value)
	{
		___ReqNum_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMREQUEST_T3578860103_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_T3517366764_H
#define __STATICARRAYINITTYPESIZEU3D44_T3517366764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_t3517366764 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t3517366764__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_T3517366764_H
#ifndef FILTER_T1571657935_H
#define FILTER_T1571657935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Filter
struct  Filter_t1571657935  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Filter::input
	AstNode_t2514041814 * ___input_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Filter::condition
	AstNode_t2514041814 * ___condition_1;

public:
	inline static int32_t get_offset_of_input_0() { return static_cast<int32_t>(offsetof(Filter_t1571657935, ___input_0)); }
	inline AstNode_t2514041814 * get_input_0() const { return ___input_0; }
	inline AstNode_t2514041814 ** get_address_of_input_0() { return &___input_0; }
	inline void set_input_0(AstNode_t2514041814 * value)
	{
		___input_0 = value;
		Il2CppCodeGenWriteBarrier((&___input_0), value);
	}

	inline static int32_t get_offset_of_condition_1() { return static_cast<int32_t>(offsetof(Filter_t1571657935, ___condition_1)); }
	inline AstNode_t2514041814 * get_condition_1() const { return ___condition_1; }
	inline AstNode_t2514041814 ** get_address_of_condition_1() { return &___condition_1; }
	inline void set_condition_1(AstNode_t2514041814 * value)
	{
		___condition_1 = value;
		Il2CppCodeGenWriteBarrier((&___condition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTER_T1571657935_H
#ifndef BINHEXDECODER_T1474272384_H
#define BINHEXDECODER_T1474272384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexDecoder
struct  BinHexDecoder_t1474272384  : public IncrementalReadDecoder_t3011954239
{
public:
	// System.Byte[] System.Xml.BinHexDecoder::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 System.Xml.BinHexDecoder::curIndex
	int32_t ___curIndex_1;
	// System.Int32 System.Xml.BinHexDecoder::endIndex
	int32_t ___endIndex_2;
	// System.Boolean System.Xml.BinHexDecoder::hasHalfByteCached
	bool ___hasHalfByteCached_3;
	// System.Byte System.Xml.BinHexDecoder::cachedHalfByte
	uint8_t ___cachedHalfByte_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_curIndex_1() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___curIndex_1)); }
	inline int32_t get_curIndex_1() const { return ___curIndex_1; }
	inline int32_t* get_address_of_curIndex_1() { return &___curIndex_1; }
	inline void set_curIndex_1(int32_t value)
	{
		___curIndex_1 = value;
	}

	inline static int32_t get_offset_of_endIndex_2() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___endIndex_2)); }
	inline int32_t get_endIndex_2() const { return ___endIndex_2; }
	inline int32_t* get_address_of_endIndex_2() { return &___endIndex_2; }
	inline void set_endIndex_2(int32_t value)
	{
		___endIndex_2 = value;
	}

	inline static int32_t get_offset_of_hasHalfByteCached_3() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___hasHalfByteCached_3)); }
	inline bool get_hasHalfByteCached_3() const { return ___hasHalfByteCached_3; }
	inline bool* get_address_of_hasHalfByteCached_3() { return &___hasHalfByteCached_3; }
	inline void set_hasHalfByteCached_3(bool value)
	{
		___hasHalfByteCached_3 = value;
	}

	inline static int32_t get_offset_of_cachedHalfByte_4() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___cachedHalfByte_4)); }
	inline uint8_t get_cachedHalfByte_4() const { return ___cachedHalfByte_4; }
	inline uint8_t* get_address_of_cachedHalfByte_4() { return &___cachedHalfByte_4; }
	inline void set_cachedHalfByte_4(uint8_t value)
	{
		___cachedHalfByte_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXDECODER_T1474272384_H
#ifndef GROUP_T100818710_H
#define GROUP_T100818710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Group
struct  Group_t100818710  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Group::groupNode
	AstNode_t2514041814 * ___groupNode_0;

public:
	inline static int32_t get_offset_of_groupNode_0() { return static_cast<int32_t>(offsetof(Group_t100818710, ___groupNode_0)); }
	inline AstNode_t2514041814 * get_groupNode_0() const { return ___groupNode_0; }
	inline AstNode_t2514041814 ** get_address_of_groupNode_0() { return &___groupNode_0; }
	inline void set_groupNode_0(AstNode_t2514041814 * value)
	{
		___groupNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___groupNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T100818710_H
#ifndef XMLTEXTWRITERBASE64ENCODER_T4259465041_H
#define XMLTEXTWRITERBASE64ENCODER_T4259465041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriterBase64Encoder
struct  XmlTextWriterBase64Encoder_t4259465041  : public Base64Encoder_t3938083961
{
public:
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriterBase64Encoder::xmlTextEncoder
	XmlTextEncoder_t1632274355 * ___xmlTextEncoder_3;

public:
	inline static int32_t get_offset_of_xmlTextEncoder_3() { return static_cast<int32_t>(offsetof(XmlTextWriterBase64Encoder_t4259465041, ___xmlTextEncoder_3)); }
	inline XmlTextEncoder_t1632274355 * get_xmlTextEncoder_3() const { return ___xmlTextEncoder_3; }
	inline XmlTextEncoder_t1632274355 ** get_address_of_xmlTextEncoder_3() { return &___xmlTextEncoder_3; }
	inline void set_xmlTextEncoder_3(XmlTextEncoder_t1632274355 * value)
	{
		___xmlTextEncoder_3 = value;
		Il2CppCodeGenWriteBarrier((&___xmlTextEncoder_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITERBASE64ENCODER_T4259465041_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef __STATICARRAYINITTYPESIZEU3D256_T1757367633_H
#define __STATICARRAYINITTYPESIZEU3D256_T1757367633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct  __StaticArrayInitTypeSizeU3D256_t1757367633 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_t1757367633__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D256_T1757367633_H
#ifndef __STATICARRAYINITTYPESIZEU3D128_T531529102_H
#define __STATICARRAYINITTYPESIZEU3D128_T531529102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128
struct  __StaticArrayInitTypeSizeU3D128_t531529102 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t531529102__padding[128];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D128_T531529102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VARIABLE_T262588068_H
#define VARIABLE_T262588068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Variable
struct  Variable_t262588068  : public AstNode_t2514041814
{
public:
	// System.String MS.Internal.Xml.XPath.Variable::localname
	String_t* ___localname_0;
	// System.String MS.Internal.Xml.XPath.Variable::prefix
	String_t* ___prefix_1;

public:
	inline static int32_t get_offset_of_localname_0() { return static_cast<int32_t>(offsetof(Variable_t262588068, ___localname_0)); }
	inline String_t* get_localname_0() const { return ___localname_0; }
	inline String_t** get_address_of_localname_0() { return &___localname_0; }
	inline void set_localname_0(String_t* value)
	{
		___localname_0 = value;
		Il2CppCodeGenWriteBarrier((&___localname_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(Variable_t262588068, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T262588068_H
#ifndef XMLCHARTYPE_T2277243275_H
#define XMLCHARTYPE_T2277243275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t2277243275 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t4116647657* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275, ___charProperties_2)); }
	inline ByteU5BU5D_t4116647657* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t4116647657* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t2277243275_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t4116647657* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t4116647657* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t4116647657* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T2277243275_H
#ifndef XPATHNAVIGATOR_T787956054_H
#define XPATHNAVIGATOR_T787956054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t787956054  : public XPathItem_t4250588140
{
public:

public:
};

struct XPathNavigator_t787956054_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorKeyComparer System.Xml.XPath.XPathNavigator::comparer
	XPathNavigatorKeyComparer_t2518900029 * ___comparer_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::NodeTypeLetter
	CharU5BU5D_t3528271667* ___NodeTypeLetter_1;
	// System.Char[] System.Xml.XPath.XPathNavigator::UniqueIdTbl
	CharU5BU5D_t3528271667* ___UniqueIdTbl_2;
	// System.Int32[] System.Xml.XPath.XPathNavigator::ContentKindMasks
	Int32U5BU5D_t385246372* ___ContentKindMasks_3;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___comparer_0)); }
	inline XPathNavigatorKeyComparer_t2518900029 * get_comparer_0() const { return ___comparer_0; }
	inline XPathNavigatorKeyComparer_t2518900029 ** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(XPathNavigatorKeyComparer_t2518900029 * value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_0), value);
	}

	inline static int32_t get_offset_of_NodeTypeLetter_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___NodeTypeLetter_1)); }
	inline CharU5BU5D_t3528271667* get_NodeTypeLetter_1() const { return ___NodeTypeLetter_1; }
	inline CharU5BU5D_t3528271667** get_address_of_NodeTypeLetter_1() { return &___NodeTypeLetter_1; }
	inline void set_NodeTypeLetter_1(CharU5BU5D_t3528271667* value)
	{
		___NodeTypeLetter_1 = value;
		Il2CppCodeGenWriteBarrier((&___NodeTypeLetter_1), value);
	}

	inline static int32_t get_offset_of_UniqueIdTbl_2() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___UniqueIdTbl_2)); }
	inline CharU5BU5D_t3528271667* get_UniqueIdTbl_2() const { return ___UniqueIdTbl_2; }
	inline CharU5BU5D_t3528271667** get_address_of_UniqueIdTbl_2() { return &___UniqueIdTbl_2; }
	inline void set_UniqueIdTbl_2(CharU5BU5D_t3528271667* value)
	{
		___UniqueIdTbl_2 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdTbl_2), value);
	}

	inline static int32_t get_offset_of_ContentKindMasks_3() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___ContentKindMasks_3)); }
	inline Int32U5BU5D_t385246372* get_ContentKindMasks_3() const { return ___ContentKindMasks_3; }
	inline Int32U5BU5D_t385246372** get_address_of_ContentKindMasks_3() { return &___ContentKindMasks_3; }
	inline void set_ContentKindMasks_3(Int32U5BU5D_t385246372* value)
	{
		___ContentKindMasks_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentKindMasks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T787956054_H
#ifndef XPATHNODEREF_T3498189018_H
#define XPATHNODEREF_T3498189018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeRef
struct  XPathNodeRef_t3498189018 
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeRef::page
	XPathNodeU5BU5D_t47339301* ___page_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeRef::idx
	int32_t ___idx_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___page_0)); }
	inline XPathNodeU5BU5D_t47339301* get_page_0() const { return ___page_0; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(XPathNodeU5BU5D_t47339301* value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_pinvoke
{
	XPathNode_t2208072876_marshaled_pinvoke* ___page_0;
	int32_t ___idx_1;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_com
{
	XPathNode_t2208072876_marshaled_com* ___page_0;
	int32_t ___idx_1;
};
#endif // XPATHNODEREF_T3498189018_H
#ifndef XPATHNODE_T2208072876_H
#define XPATHNODE_T2208072876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNode
struct  XPathNode_t2208072876 
{
public:
	// MS.Internal.Xml.Cache.XPathNodeInfoAtom MS.Internal.Xml.Cache.XPathNode::info
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSibling
	uint16_t ___idxSibling_1;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxParent
	uint16_t ___idxParent_2;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSimilar
	uint16_t ___idxSimilar_3;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::posOffset
	uint16_t ___posOffset_4;
	// System.UInt32 MS.Internal.Xml.Cache.XPathNode::props
	uint32_t ___props_5;
	// System.String MS.Internal.Xml.Cache.XPathNode::value
	String_t* ___value_6;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___info_0)); }
	inline XPathNodeInfoAtom_t1760358141 * get_info_0() const { return ___info_0; }
	inline XPathNodeInfoAtom_t1760358141 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(XPathNodeInfoAtom_t1760358141 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier((&___info_0), value);
	}

	inline static int32_t get_offset_of_idxSibling_1() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxSibling_1)); }
	inline uint16_t get_idxSibling_1() const { return ___idxSibling_1; }
	inline uint16_t* get_address_of_idxSibling_1() { return &___idxSibling_1; }
	inline void set_idxSibling_1(uint16_t value)
	{
		___idxSibling_1 = value;
	}

	inline static int32_t get_offset_of_idxParent_2() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxParent_2)); }
	inline uint16_t get_idxParent_2() const { return ___idxParent_2; }
	inline uint16_t* get_address_of_idxParent_2() { return &___idxParent_2; }
	inline void set_idxParent_2(uint16_t value)
	{
		___idxParent_2 = value;
	}

	inline static int32_t get_offset_of_idxSimilar_3() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxSimilar_3)); }
	inline uint16_t get_idxSimilar_3() const { return ___idxSimilar_3; }
	inline uint16_t* get_address_of_idxSimilar_3() { return &___idxSimilar_3; }
	inline void set_idxSimilar_3(uint16_t value)
	{
		___idxSimilar_3 = value;
	}

	inline static int32_t get_offset_of_posOffset_4() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___posOffset_4)); }
	inline uint16_t get_posOffset_4() const { return ___posOffset_4; }
	inline uint16_t* get_address_of_posOffset_4() { return &___posOffset_4; }
	inline void set_posOffset_4(uint16_t value)
	{
		___posOffset_4 = value;
	}

	inline static int32_t get_offset_of_props_5() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___props_5)); }
	inline uint32_t get_props_5() const { return ___props_5; }
	inline uint32_t* get_address_of_props_5() { return &___props_5; }
	inline void set_props_5(uint32_t value)
	{
		___props_5 = value;
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t2208072876_marshaled_pinvoke
{
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	char* ___value_6;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t2208072876_marshaled_com
{
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	Il2CppChar* ___value_6;
};
#endif // XPATHNODE_T2208072876_H
#ifndef ERROREVENTARGS_T1584858912_H
#define ERROREVENTARGS_T1584858912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ErrorEventArgs
struct  ErrorEventArgs_t1584858912  : public EventArgs_t3591816995
{
public:
	// System.Exception System.IO.ErrorEventArgs::exception
	Exception_t * ___exception_1;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t1584858912, ___exception_1)); }
	inline Exception_t * get_exception_1() const { return ___exception_1; }
	inline Exception_t ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(Exception_t * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T1584858912_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef DESCRIPTIONATTRIBUTE_T874390736_H
#define DESCRIPTIONATTRIBUTE_T874390736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DescriptionAttribute
struct  DescriptionAttribute_t874390736  : public Attribute_t861562559
{
public:
	// System.String System.ComponentModel.DescriptionAttribute::description
	String_t* ___description_1;

public:
	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t874390736, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}
};

struct DescriptionAttribute_t874390736_StaticFields
{
public:
	// System.ComponentModel.DescriptionAttribute System.ComponentModel.DescriptionAttribute::Default
	DescriptionAttribute_t874390736 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t874390736_StaticFields, ___Default_0)); }
	inline DescriptionAttribute_t874390736 * get_Default_0() const { return ___Default_0; }
	inline DescriptionAttribute_t874390736 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(DescriptionAttribute_t874390736 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCRIPTIONATTRIBUTE_T874390736_H
#ifndef COMPONENT_T3620823400_H
#define COMPONENT_T3620823400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t3620823400  : public MarshalByRefObject_t2760389100
{
public:
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	RuntimeObject* ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_t1108123056 * ___events_3;

public:
	inline static int32_t get_offset_of_site_2() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___site_2)); }
	inline RuntimeObject* get_site_2() const { return ___site_2; }
	inline RuntimeObject** get_address_of_site_2() { return &___site_2; }
	inline void set_site_2(RuntimeObject* value)
	{
		___site_2 = value;
		Il2CppCodeGenWriteBarrier((&___site_2), value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___events_3)); }
	inline EventHandlerList_t1108123056 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_t1108123056 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier((&___events_3), value);
	}
};

struct Component_t3620823400_StaticFields
{
public:
	// System.Object System.ComponentModel.Component::EventDisposed
	RuntimeObject * ___EventDisposed_1;

public:
	inline static int32_t get_offset_of_EventDisposed_1() { return static_cast<int32_t>(offsetof(Component_t3620823400_StaticFields, ___EventDisposed_1)); }
	inline RuntimeObject * get_EventDisposed_1() const { return ___EventDisposed_1; }
	inline RuntimeObject ** get_address_of_EventDisposed_1() { return &___EventDisposed_1; }
	inline void set_EventDisposed_1(RuntimeObject * value)
	{
		___EventDisposed_1 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3620823400_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef UINTPTR_T_H
#define UINTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UIntPtr
struct  UIntPtr_t 
{
public:
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;

public:
	inline static int32_t get_offset_of__pointer_1() { return static_cast<int32_t>(offsetof(UIntPtr_t, ____pointer_1)); }
	inline void* get__pointer_1() const { return ____pointer_1; }
	inline void** get_address_of__pointer_1() { return &____pointer_1; }
	inline void set__pointer_1(void* value)
	{
		____pointer_1 = value;
	}
};

struct UIntPtr_t_StaticFields
{
public:
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(UIntPtr_t_StaticFields, ___Zero_0)); }
	inline uintptr_t get_Zero_0() const { return ___Zero_0; }
	inline uintptr_t* get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(uintptr_t value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTR_T_H
#ifndef EVENTTYPE_T2132206657_H
#define EVENTTYPE_T2132206657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemWatcher/EventType
struct  EventType_t2132206657 
{
public:
	// System.Int32 System.IO.FileSystemWatcher/EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_t2132206657, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T2132206657_H
#ifndef IODESCRIPTIONATTRIBUTE_T2336130459_H
#define IODESCRIPTIONATTRIBUTE_T2336130459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IODescriptionAttribute
struct  IODescriptionAttribute_t2336130459  : public DescriptionAttribute_t874390736
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IODESCRIPTIONATTRIBUTE_T2336130459_H
#ifndef INOTIFYMASK_T3323194736_H
#define INOTIFYMASK_T3323194736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyMask
struct  InotifyMask_t3323194736 
{
public:
	// System.UInt32 System.IO.InotifyMask::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InotifyMask_t3323194736, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INOTIFYMASK_T3323194736_H
#ifndef FILEACTION_T3839877343_H
#define FILEACTION_T3839877343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAction
struct  FileAction_t3839877343 
{
public:
	// System.Int32 System.IO.FileAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAction_t3839877343, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACTION_T3839877343_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef FAMCONNECTION_T1678998633_H
#define FAMCONNECTION_T1678998633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMConnection
struct  FAMConnection_t1678998633 
{
public:
	// System.Int32 System.IO.FAMConnection::FD
	int32_t ___FD_0;
	// System.IntPtr System.IO.FAMConnection::opaque
	intptr_t ___opaque_1;

public:
	inline static int32_t get_offset_of_FD_0() { return static_cast<int32_t>(offsetof(FAMConnection_t1678998633, ___FD_0)); }
	inline int32_t get_FD_0() const { return ___FD_0; }
	inline int32_t* get_address_of_FD_0() { return &___FD_0; }
	inline void set_FD_0(int32_t value)
	{
		___FD_0 = value;
	}

	inline static int32_t get_offset_of_opaque_1() { return static_cast<int32_t>(offsetof(FAMConnection_t1678998633, ___opaque_1)); }
	inline intptr_t get_opaque_1() const { return ___opaque_1; }
	inline intptr_t* get_address_of_opaque_1() { return &___opaque_1; }
	inline void set_opaque_1(intptr_t value)
	{
		___opaque_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMCONNECTION_T1678998633_H
#ifndef XPATHRESULTTYPE_T2828988488_H
#define XPATHRESULTTYPE_T2828988488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathResultType
struct  XPathResultType_t2828988488 
{
public:
	// System.Int32 System.Xml.XPath.XPathResultType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathResultType_t2828988488, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHRESULTTYPE_T2828988488_H
#ifndef XPATHNODETYPE_T3031007223_H
#define XPATHNODETYPE_T3031007223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_t3031007223 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNodeType_t3031007223, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_T3031007223_H
#ifndef NOTIFYFILTERS_T3825601669_H
#define NOTIFYFILTERS_T3825601669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NotifyFilters
struct  NotifyFilters_t3825601669 
{
public:
	// System.Int32 System.IO.NotifyFilters::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotifyFilters_t3825601669, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYFILTERS_T3825601669_H
#ifndef FAMCODES_T1537683418_H
#define FAMCODES_T1537683418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMCodes
struct  FAMCodes_t1537683418 
{
public:
	// System.Int32 System.IO.FAMCodes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FAMCodes_t1537683418, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMCODES_T1537683418_H
#ifndef FAMDATA_T1817296356_H
#define FAMDATA_T1817296356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMData
struct  FAMData_t1817296356  : public RuntimeObject
{
public:
	// System.IO.FileSystemWatcher System.IO.FAMData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.FAMData::Directory
	String_t* ___Directory_1;
	// System.String System.IO.FAMData::FileMask
	String_t* ___FileMask_2;
	// System.Boolean System.IO.FAMData::IncludeSubdirs
	bool ___IncludeSubdirs_3;
	// System.Boolean System.IO.FAMData::Enabled
	bool ___Enabled_4;
	// System.IO.FAMRequest System.IO.FAMData::Request
	FAMRequest_t3578860103  ___Request_5;
	// System.Collections.Hashtable System.IO.FAMData::SubDirs
	Hashtable_t1853889766 * ___SubDirs_6;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier((&___FSW_0), value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier((&___Directory_1), value);
	}

	inline static int32_t get_offset_of_FileMask_2() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___FileMask_2)); }
	inline String_t* get_FileMask_2() const { return ___FileMask_2; }
	inline String_t** get_address_of_FileMask_2() { return &___FileMask_2; }
	inline void set_FileMask_2(String_t* value)
	{
		___FileMask_2 = value;
		Il2CppCodeGenWriteBarrier((&___FileMask_2), value);
	}

	inline static int32_t get_offset_of_IncludeSubdirs_3() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___IncludeSubdirs_3)); }
	inline bool get_IncludeSubdirs_3() const { return ___IncludeSubdirs_3; }
	inline bool* get_address_of_IncludeSubdirs_3() { return &___IncludeSubdirs_3; }
	inline void set_IncludeSubdirs_3(bool value)
	{
		___IncludeSubdirs_3 = value;
	}

	inline static int32_t get_offset_of_Enabled_4() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___Enabled_4)); }
	inline bool get_Enabled_4() const { return ___Enabled_4; }
	inline bool* get_address_of_Enabled_4() { return &___Enabled_4; }
	inline void set_Enabled_4(bool value)
	{
		___Enabled_4 = value;
	}

	inline static int32_t get_offset_of_Request_5() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___Request_5)); }
	inline FAMRequest_t3578860103  get_Request_5() const { return ___Request_5; }
	inline FAMRequest_t3578860103 * get_address_of_Request_5() { return &___Request_5; }
	inline void set_Request_5(FAMRequest_t3578860103  value)
	{
		___Request_5 = value;
	}

	inline static int32_t get_offset_of_SubDirs_6() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___SubDirs_6)); }
	inline Hashtable_t1853889766 * get_SubDirs_6() const { return ___SubDirs_6; }
	inline Hashtable_t1853889766 ** get_address_of_SubDirs_6() { return &___SubDirs_6; }
	inline void set_SubDirs_6(Hashtable_t1853889766 * value)
	{
		___SubDirs_6 = value;
		Il2CppCodeGenWriteBarrier((&___SubDirs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMDATA_T1817296356_H
#ifndef ENTITYHANDLING_T1047276436_H
#define ENTITYHANDLING_T1047276436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t1047276436 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityHandling_t1047276436, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T1047276436_H
#ifndef DTDPROCESSING_T1163997051_H
#define DTDPROCESSING_T1163997051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdProcessing
struct  DtdProcessing_t1163997051 
{
public:
	// System.Int32 System.Xml.DtdProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DtdProcessing_t1163997051, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPROCESSING_T1163997051_H
#ifndef CONFORMANCELEVEL_T3899847875_H
#define CONFORMANCELEVEL_T3899847875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3899847875 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3899847875, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3899847875_H
#ifndef XPATHDOCUMENTNAVIGATOR_T2457178823_H
#define XPATHDOCUMENTNAVIGATOR_T2457178823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathDocumentNavigator
struct  XPathDocumentNavigator_t2457178823  : public XPathNavigator_t787956054
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageCurrent
	XPathNodeU5BU5D_t47339301* ___pageCurrent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageParent
	XPathNodeU5BU5D_t47339301* ___pageParent_5;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxCurrent
	int32_t ___idxCurrent_6;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxParent
	int32_t ___idxParent_7;

public:
	inline static int32_t get_offset_of_pageCurrent_4() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___pageCurrent_4)); }
	inline XPathNodeU5BU5D_t47339301* get_pageCurrent_4() const { return ___pageCurrent_4; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageCurrent_4() { return &___pageCurrent_4; }
	inline void set_pageCurrent_4(XPathNodeU5BU5D_t47339301* value)
	{
		___pageCurrent_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageCurrent_4), value);
	}

	inline static int32_t get_offset_of_pageParent_5() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___pageParent_5)); }
	inline XPathNodeU5BU5D_t47339301* get_pageParent_5() const { return ___pageParent_5; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageParent_5() { return &___pageParent_5; }
	inline void set_pageParent_5(XPathNodeU5BU5D_t47339301* value)
	{
		___pageParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageParent_5), value);
	}

	inline static int32_t get_offset_of_idxCurrent_6() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___idxCurrent_6)); }
	inline int32_t get_idxCurrent_6() const { return ___idxCurrent_6; }
	inline int32_t* get_address_of_idxCurrent_6() { return &___idxCurrent_6; }
	inline void set_idxCurrent_6(int32_t value)
	{
		___idxCurrent_6 = value;
	}

	inline static int32_t get_offset_of_idxParent_7() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___idxParent_7)); }
	inline int32_t get_idxParent_7() const { return ___idxParent_7; }
	inline int32_t* get_address_of_idxParent_7() { return &___idxParent_7; }
	inline void set_idxParent_7(int32_t value)
	{
		___idxParent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHDOCUMENTNAVIGATOR_T2457178823_H
#ifndef FILTERFLAGS_T1388790893_H
#define FILTERFLAGS_T1388790893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FilterFlags
struct  FilterFlags_t1388790893 
{
public:
	// System.UInt32 System.IO.FilterFlags::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterFlags_t1388790893, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERFLAGS_T1388790893_H
#ifndef COMPRESSIONMODE_T3714291783_H
#define COMPRESSIONMODE_T3714291783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.CompressionMode
struct  CompressionMode_t3714291783 
{
public:
	// System.Int32 System.IO.Compression.CompressionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMode_t3714291783, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMODE_T3714291783_H
#ifndef GZIPSTREAM_T3417139389_H
#define GZIPSTREAM_T3417139389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.GZipStream
struct  GZipStream_t3417139389  : public Stream_t1273022909
{
public:
	// System.IO.Compression.DeflateStream System.IO.Compression.GZipStream::_deflateStream
	DeflateStream_t4175168077 * ____deflateStream_4;

public:
	inline static int32_t get_offset_of__deflateStream_4() { return static_cast<int32_t>(offsetof(GZipStream_t3417139389, ____deflateStream_4)); }
	inline DeflateStream_t4175168077 * get__deflateStream_4() const { return ____deflateStream_4; }
	inline DeflateStream_t4175168077 ** get_address_of__deflateStream_4() { return &____deflateStream_4; }
	inline void set__deflateStream_4(DeflateStream_t4175168077 * value)
	{
		____deflateStream_4 = value;
		Il2CppCodeGenWriteBarrier((&____deflateStream_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPSTREAM_T3417139389_H
#ifndef AXISTYPE_T3322599580_H
#define AXISTYPE_T3322599580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis/AxisType
struct  AxisType_t3322599580 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Axis/AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_t3322599580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTYPE_T3322599580_H
#ifndef ASTTYPE_T3854428833_H
#define ASTTYPE_T3854428833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode/AstType
struct  AstType_t3854428833 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.AstNode/AstType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AstType_t3854428833, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTTYPE_T3854428833_H
#ifndef KQUEUEMONITOR_T3818269198_H
#define KQUEUEMONITOR_T3818269198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KqueueMonitor
struct  KqueueMonitor_t3818269198  : public RuntimeObject
{
public:
	// System.Int32 System.IO.KqueueMonitor::maxFds
	int32_t ___maxFds_2;
	// System.IO.FileSystemWatcher System.IO.KqueueMonitor::fsw
	FileSystemWatcher_t416760199 * ___fsw_3;
	// System.Int32 System.IO.KqueueMonitor::conn
	int32_t ___conn_4;
	// System.Threading.Thread System.IO.KqueueMonitor::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.KqueueMonitor::requestStop
	bool ___requestStop_6;
	// System.Threading.AutoResetEvent System.IO.KqueueMonitor::startedEvent
	AutoResetEvent_t1333520283 * ___startedEvent_7;
	// System.Boolean System.IO.KqueueMonitor::started
	bool ___started_8;
	// System.Boolean System.IO.KqueueMonitor::inDispatch
	bool ___inDispatch_9;
	// System.Exception System.IO.KqueueMonitor::exc
	Exception_t * ___exc_10;
	// System.Object System.IO.KqueueMonitor::stateLock
	RuntimeObject * ___stateLock_11;
	// System.Object System.IO.KqueueMonitor::connLock
	RuntimeObject * ___connLock_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.IO.PathData> System.IO.KqueueMonitor::pathsDict
	Dictionary_2_t3002451328 * ___pathsDict_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.IO.PathData> System.IO.KqueueMonitor::fdsDict
	Dictionary_2_t2105908360 * ___fdsDict_14;
	// System.String System.IO.KqueueMonitor::fixupPath
	String_t* ___fixupPath_15;
	// System.String System.IO.KqueueMonitor::fullPathNoLastSlash
	String_t* ___fullPathNoLastSlash_16;

public:
	inline static int32_t get_offset_of_maxFds_2() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___maxFds_2)); }
	inline int32_t get_maxFds_2() const { return ___maxFds_2; }
	inline int32_t* get_address_of_maxFds_2() { return &___maxFds_2; }
	inline void set_maxFds_2(int32_t value)
	{
		___maxFds_2 = value;
	}

	inline static int32_t get_offset_of_fsw_3() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fsw_3)); }
	inline FileSystemWatcher_t416760199 * get_fsw_3() const { return ___fsw_3; }
	inline FileSystemWatcher_t416760199 ** get_address_of_fsw_3() { return &___fsw_3; }
	inline void set_fsw_3(FileSystemWatcher_t416760199 * value)
	{
		___fsw_3 = value;
		Il2CppCodeGenWriteBarrier((&___fsw_3), value);
	}

	inline static int32_t get_offset_of_conn_4() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___conn_4)); }
	inline int32_t get_conn_4() const { return ___conn_4; }
	inline int32_t* get_address_of_conn_4() { return &___conn_4; }
	inline void set_conn_4(int32_t value)
	{
		___conn_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___thread_5), value);
	}

	inline static int32_t get_offset_of_requestStop_6() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___requestStop_6)); }
	inline bool get_requestStop_6() const { return ___requestStop_6; }
	inline bool* get_address_of_requestStop_6() { return &___requestStop_6; }
	inline void set_requestStop_6(bool value)
	{
		___requestStop_6 = value;
	}

	inline static int32_t get_offset_of_startedEvent_7() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___startedEvent_7)); }
	inline AutoResetEvent_t1333520283 * get_startedEvent_7() const { return ___startedEvent_7; }
	inline AutoResetEvent_t1333520283 ** get_address_of_startedEvent_7() { return &___startedEvent_7; }
	inline void set_startedEvent_7(AutoResetEvent_t1333520283 * value)
	{
		___startedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___startedEvent_7), value);
	}

	inline static int32_t get_offset_of_started_8() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___started_8)); }
	inline bool get_started_8() const { return ___started_8; }
	inline bool* get_address_of_started_8() { return &___started_8; }
	inline void set_started_8(bool value)
	{
		___started_8 = value;
	}

	inline static int32_t get_offset_of_inDispatch_9() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___inDispatch_9)); }
	inline bool get_inDispatch_9() const { return ___inDispatch_9; }
	inline bool* get_address_of_inDispatch_9() { return &___inDispatch_9; }
	inline void set_inDispatch_9(bool value)
	{
		___inDispatch_9 = value;
	}

	inline static int32_t get_offset_of_exc_10() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___exc_10)); }
	inline Exception_t * get_exc_10() const { return ___exc_10; }
	inline Exception_t ** get_address_of_exc_10() { return &___exc_10; }
	inline void set_exc_10(Exception_t * value)
	{
		___exc_10 = value;
		Il2CppCodeGenWriteBarrier((&___exc_10), value);
	}

	inline static int32_t get_offset_of_stateLock_11() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___stateLock_11)); }
	inline RuntimeObject * get_stateLock_11() const { return ___stateLock_11; }
	inline RuntimeObject ** get_address_of_stateLock_11() { return &___stateLock_11; }
	inline void set_stateLock_11(RuntimeObject * value)
	{
		___stateLock_11 = value;
		Il2CppCodeGenWriteBarrier((&___stateLock_11), value);
	}

	inline static int32_t get_offset_of_connLock_12() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___connLock_12)); }
	inline RuntimeObject * get_connLock_12() const { return ___connLock_12; }
	inline RuntimeObject ** get_address_of_connLock_12() { return &___connLock_12; }
	inline void set_connLock_12(RuntimeObject * value)
	{
		___connLock_12 = value;
		Il2CppCodeGenWriteBarrier((&___connLock_12), value);
	}

	inline static int32_t get_offset_of_pathsDict_13() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___pathsDict_13)); }
	inline Dictionary_2_t3002451328 * get_pathsDict_13() const { return ___pathsDict_13; }
	inline Dictionary_2_t3002451328 ** get_address_of_pathsDict_13() { return &___pathsDict_13; }
	inline void set_pathsDict_13(Dictionary_2_t3002451328 * value)
	{
		___pathsDict_13 = value;
		Il2CppCodeGenWriteBarrier((&___pathsDict_13), value);
	}

	inline static int32_t get_offset_of_fdsDict_14() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fdsDict_14)); }
	inline Dictionary_2_t2105908360 * get_fdsDict_14() const { return ___fdsDict_14; }
	inline Dictionary_2_t2105908360 ** get_address_of_fdsDict_14() { return &___fdsDict_14; }
	inline void set_fdsDict_14(Dictionary_2_t2105908360 * value)
	{
		___fdsDict_14 = value;
		Il2CppCodeGenWriteBarrier((&___fdsDict_14), value);
	}

	inline static int32_t get_offset_of_fixupPath_15() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fixupPath_15)); }
	inline String_t* get_fixupPath_15() const { return ___fixupPath_15; }
	inline String_t** get_address_of_fixupPath_15() { return &___fixupPath_15; }
	inline void set_fixupPath_15(String_t* value)
	{
		___fixupPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___fixupPath_15), value);
	}

	inline static int32_t get_offset_of_fullPathNoLastSlash_16() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fullPathNoLastSlash_16)); }
	inline String_t* get_fullPathNoLastSlash_16() const { return ___fullPathNoLastSlash_16; }
	inline String_t** get_address_of_fullPathNoLastSlash_16() { return &___fullPathNoLastSlash_16; }
	inline void set_fullPathNoLastSlash_16(String_t* value)
	{
		___fullPathNoLastSlash_16 = value;
		Il2CppCodeGenWriteBarrier((&___fullPathNoLastSlash_16), value);
	}
};

struct KqueueMonitor_t3818269198_StaticFields
{
public:
	// System.Boolean System.IO.KqueueMonitor::initialized
	bool ___initialized_0;
	// System.IO.kevent[] System.IO.KqueueMonitor::emptyEventList
	keventU5BU5D_t21523777* ___emptyEventList_1;

public:
	inline static int32_t get_offset_of_initialized_0() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198_StaticFields, ___initialized_0)); }
	inline bool get_initialized_0() const { return ___initialized_0; }
	inline bool* get_address_of_initialized_0() { return &___initialized_0; }
	inline void set_initialized_0(bool value)
	{
		___initialized_0 = value;
	}

	inline static int32_t get_offset_of_emptyEventList_1() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198_StaticFields, ___emptyEventList_1)); }
	inline keventU5BU5D_t21523777* get_emptyEventList_1() const { return ___emptyEventList_1; }
	inline keventU5BU5D_t21523777** get_address_of_emptyEventList_1() { return &___emptyEventList_1; }
	inline void set_emptyEventList_1(keventU5BU5D_t21523777* value)
	{
		___emptyEventList_1 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEventList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KQUEUEMONITOR_T3818269198_H
#ifndef DEFLATESTREAMNATIVE_T1405046456_H
#define DEFLATESTREAMNATIVE_T1405046456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStreamNative
struct  DeflateStreamNative_t1405046456  : public RuntimeObject
{
public:
	// System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite System.IO.Compression.DeflateStreamNative::feeder
	UnmanagedReadOrWrite_t1975956110 * ___feeder_0;
	// System.IO.Stream System.IO.Compression.DeflateStreamNative::base_stream
	Stream_t1273022909 * ___base_stream_1;
	// System.IntPtr System.IO.Compression.DeflateStreamNative::z_stream
	intptr_t ___z_stream_2;
	// System.Runtime.InteropServices.GCHandle System.IO.Compression.DeflateStreamNative::data
	GCHandle_t3351438187  ___data_3;
	// System.Boolean System.IO.Compression.DeflateStreamNative::disposed
	bool ___disposed_4;
	// System.Byte[] System.IO.Compression.DeflateStreamNative::io_buffer
	ByteU5BU5D_t4116647657* ___io_buffer_5;

public:
	inline static int32_t get_offset_of_feeder_0() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___feeder_0)); }
	inline UnmanagedReadOrWrite_t1975956110 * get_feeder_0() const { return ___feeder_0; }
	inline UnmanagedReadOrWrite_t1975956110 ** get_address_of_feeder_0() { return &___feeder_0; }
	inline void set_feeder_0(UnmanagedReadOrWrite_t1975956110 * value)
	{
		___feeder_0 = value;
		Il2CppCodeGenWriteBarrier((&___feeder_0), value);
	}

	inline static int32_t get_offset_of_base_stream_1() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___base_stream_1)); }
	inline Stream_t1273022909 * get_base_stream_1() const { return ___base_stream_1; }
	inline Stream_t1273022909 ** get_address_of_base_stream_1() { return &___base_stream_1; }
	inline void set_base_stream_1(Stream_t1273022909 * value)
	{
		___base_stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_1), value);
	}

	inline static int32_t get_offset_of_z_stream_2() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___z_stream_2)); }
	inline intptr_t get_z_stream_2() const { return ___z_stream_2; }
	inline intptr_t* get_address_of_z_stream_2() { return &___z_stream_2; }
	inline void set_z_stream_2(intptr_t value)
	{
		___z_stream_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___data_3)); }
	inline GCHandle_t3351438187  get_data_3() const { return ___data_3; }
	inline GCHandle_t3351438187 * get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(GCHandle_t3351438187  value)
	{
		___data_3 = value;
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}

	inline static int32_t get_offset_of_io_buffer_5() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___io_buffer_5)); }
	inline ByteU5BU5D_t4116647657* get_io_buffer_5() const { return ___io_buffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_io_buffer_5() { return &___io_buffer_5; }
	inline void set_io_buffer_5(ByteU5BU5D_t4116647657* value)
	{
		___io_buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___io_buffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAMNATIVE_T1405046456_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255364_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255364  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14 <PrivateImplementationDetails>::0283A6AF88802AB45989B29549915BEA0F6CD515
	__StaticArrayInitTypeSizeU3D14_t3517563373  ___0283A6AF88802AB45989B29549915BEA0F6CD515_0;
	// System.Int64 <PrivateImplementationDetails>::03F4297FCC30D0FD5E420E5D26E7FA711167C7EF
	int64_t ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9 <PrivateImplementationDetails>::1A39764B112685485A5BA7B2880D878B858C1A7A
	__StaticArrayInitTypeSizeU3D9_t3218278900  ___1A39764B112685485A5BA7B2880D878B858C1A7A_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::1A84029C80CB5518379F199F53FF08A7B764F8FD
	__StaticArrayInitTypeSizeU3D3_t3217885684  ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC
	__StaticArrayInitTypeSizeU3D12_t2710994318  ___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::59F5BD34B6C013DEACC784F69C67E95150033A84
	__StaticArrayInitTypeSizeU3D32_t2711125391  ___59F5BD34B6C013DEACC784F69C67E95150033A84_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C
	__StaticArrayInitTypeSizeU3D6_t3217689075  ___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9 <PrivateImplementationDetails>::6D49C9D487D7AD3491ECE08732D68A593CC2038D
	__StaticArrayInitTypeSizeU3D9_t3218278900  ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E
	__StaticArrayInitTypeSizeU3D128_t531529102  ___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3
	__StaticArrayInitTypeSizeU3D44_t3517366764  ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9;
	// System.Int64 <PrivateImplementationDetails>::98A44A6F8606AE6F23FE230286C1D6FBCC407226
	int64_t ___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536
	__StaticArrayInitTypeSizeU3D32_t2711125391  ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::CCEEADA43268372341F81AE0C9208C6856441C04
	__StaticArrayInitTypeSizeU3D128_t531529102  ___CCEEADA43268372341F81AE0C9208C6856441C04_12;
	// System.Int64 <PrivateImplementationDetails>::E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78
	int64_t ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::EC5842B3154E1AF94500B57220EB9F684BCCC42A
	__StaticArrayInitTypeSizeU3D32_t2711125391  ___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::EEAFE8C6E1AB017237567305EE925C976CDB6458
	__StaticArrayInitTypeSizeU3D256_t1757367633  ___EEAFE8C6E1AB017237567305EE925C976CDB6458_15;

public:
	inline static int32_t get_offset_of_U30283A6AF88802AB45989B29549915BEA0F6CD515_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___0283A6AF88802AB45989B29549915BEA0F6CD515_0)); }
	inline __StaticArrayInitTypeSizeU3D14_t3517563373  get_U30283A6AF88802AB45989B29549915BEA0F6CD515_0() const { return ___0283A6AF88802AB45989B29549915BEA0F6CD515_0; }
	inline __StaticArrayInitTypeSizeU3D14_t3517563373 * get_address_of_U30283A6AF88802AB45989B29549915BEA0F6CD515_0() { return &___0283A6AF88802AB45989B29549915BEA0F6CD515_0; }
	inline void set_U30283A6AF88802AB45989B29549915BEA0F6CD515_0(__StaticArrayInitTypeSizeU3D14_t3517563373  value)
	{
		___0283A6AF88802AB45989B29549915BEA0F6CD515_0 = value;
	}

	inline static int32_t get_offset_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1)); }
	inline int64_t get_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1() const { return ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1; }
	inline int64_t* get_address_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1() { return &___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1; }
	inline void set_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1(int64_t value)
	{
		___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1 = value;
	}

	inline static int32_t get_offset_of_U31A39764B112685485A5BA7B2880D878B858C1A7A_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___1A39764B112685485A5BA7B2880D878B858C1A7A_2)); }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900  get_U31A39764B112685485A5BA7B2880D878B858C1A7A_2() const { return ___1A39764B112685485A5BA7B2880D878B858C1A7A_2; }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900 * get_address_of_U31A39764B112685485A5BA7B2880D878B858C1A7A_2() { return &___1A39764B112685485A5BA7B2880D878B858C1A7A_2; }
	inline void set_U31A39764B112685485A5BA7B2880D878B858C1A7A_2(__StaticArrayInitTypeSizeU3D9_t3218278900  value)
	{
		___1A39764B112685485A5BA7B2880D878B858C1A7A_2 = value;
	}

	inline static int32_t get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885684  get_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() const { return ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885684 * get_address_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return &___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline void set_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(__StaticArrayInitTypeSizeU3D3_t3217885684  value)
	{
		___1A84029C80CB5518379F199F53FF08A7B764F8FD_3 = value;
	}

	inline static int32_t get_offset_of_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994318  get_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4() const { return ___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994318 * get_address_of_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4() { return &___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4; }
	inline void set_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4(__StaticArrayInitTypeSizeU3D12_t2710994318  value)
	{
		___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4 = value;
	}

	inline static int32_t get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___59F5BD34B6C013DEACC784F69C67E95150033A84_5)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125391  get_U359F5BD34B6C013DEACC784F69C67E95150033A84_5() const { return ___59F5BD34B6C013DEACC784F69C67E95150033A84_5; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125391 * get_address_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_5() { return &___59F5BD34B6C013DEACC784F69C67E95150033A84_5; }
	inline void set_U359F5BD34B6C013DEACC784F69C67E95150033A84_5(__StaticArrayInitTypeSizeU3D32_t2711125391  value)
	{
		___59F5BD34B6C013DEACC784F69C67E95150033A84_5 = value;
	}

	inline static int32_t get_offset_of_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6)); }
	inline __StaticArrayInitTypeSizeU3D6_t3217689075  get_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6() const { return ___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6; }
	inline __StaticArrayInitTypeSizeU3D6_t3217689075 * get_address_of_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6() { return &___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6; }
	inline void set_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6(__StaticArrayInitTypeSizeU3D6_t3217689075  value)
	{
		___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6 = value;
	}

	inline static int32_t get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7)); }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900  get_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7() const { return ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7; }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900 * get_address_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7() { return &___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7; }
	inline void set_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7(__StaticArrayInitTypeSizeU3D9_t3218278900  value)
	{
		___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7 = value;
	}

	inline static int32_t get_offset_of_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8)); }
	inline __StaticArrayInitTypeSizeU3D128_t531529102  get_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8() const { return ___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8; }
	inline __StaticArrayInitTypeSizeU3D128_t531529102 * get_address_of_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8() { return &___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8; }
	inline void set_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8(__StaticArrayInitTypeSizeU3D128_t531529102  value)
	{
		___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8 = value;
	}

	inline static int32_t get_offset_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9)); }
	inline __StaticArrayInitTypeSizeU3D44_t3517366764  get_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9() const { return ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9; }
	inline __StaticArrayInitTypeSizeU3D44_t3517366764 * get_address_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9() { return &___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9; }
	inline void set_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9(__StaticArrayInitTypeSizeU3D44_t3517366764  value)
	{
		___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9 = value;
	}

	inline static int32_t get_offset_of_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10)); }
	inline int64_t get_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10() const { return ___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10; }
	inline int64_t* get_address_of_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10() { return &___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10; }
	inline void set_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10(int64_t value)
	{
		___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10 = value;
	}

	inline static int32_t get_offset_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125391  get_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11() const { return ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125391 * get_address_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11() { return &___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11; }
	inline void set_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11(__StaticArrayInitTypeSizeU3D32_t2711125391  value)
	{
		___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11 = value;
	}

	inline static int32_t get_offset_of_CCEEADA43268372341F81AE0C9208C6856441C04_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___CCEEADA43268372341F81AE0C9208C6856441C04_12)); }
	inline __StaticArrayInitTypeSizeU3D128_t531529102  get_CCEEADA43268372341F81AE0C9208C6856441C04_12() const { return ___CCEEADA43268372341F81AE0C9208C6856441C04_12; }
	inline __StaticArrayInitTypeSizeU3D128_t531529102 * get_address_of_CCEEADA43268372341F81AE0C9208C6856441C04_12() { return &___CCEEADA43268372341F81AE0C9208C6856441C04_12; }
	inline void set_CCEEADA43268372341F81AE0C9208C6856441C04_12(__StaticArrayInitTypeSizeU3D128_t531529102  value)
	{
		___CCEEADA43268372341F81AE0C9208C6856441C04_12 = value;
	}

	inline static int32_t get_offset_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13)); }
	inline int64_t get_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13() const { return ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13; }
	inline int64_t* get_address_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13() { return &___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13; }
	inline void set_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13(int64_t value)
	{
		___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13 = value;
	}

	inline static int32_t get_offset_of_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125391  get_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14() const { return ___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125391 * get_address_of_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14() { return &___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14; }
	inline void set_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14(__StaticArrayInitTypeSizeU3D32_t2711125391  value)
	{
		___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14 = value;
	}

	inline static int32_t get_offset_of_EEAFE8C6E1AB017237567305EE925C976CDB6458_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___EEAFE8C6E1AB017237567305EE925C976CDB6458_15)); }
	inline __StaticArrayInitTypeSizeU3D256_t1757367633  get_EEAFE8C6E1AB017237567305EE925C976CDB6458_15() const { return ___EEAFE8C6E1AB017237567305EE925C976CDB6458_15; }
	inline __StaticArrayInitTypeSizeU3D256_t1757367633 * get_address_of_EEAFE8C6E1AB017237567305EE925C976CDB6458_15() { return &___EEAFE8C6E1AB017237567305EE925C976CDB6458_15; }
	inline void set_EEAFE8C6E1AB017237567305EE925C976CDB6458_15(__StaticArrayInitTypeSizeU3D256_t1757367633  value)
	{
		___EEAFE8C6E1AB017237567305EE925C976CDB6458_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255364_H
#ifndef FUNCTIONTYPE_T3319434782_H
#define FUNCTIONTYPE_T3319434782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Function/FunctionType
struct  FunctionType_t3319434782 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Function/FunctionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FunctionType_t3319434782, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONTYPE_T3319434782_H
#ifndef LEXKIND_T864578899_H
#define LEXKIND_T864578899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner/LexKind
struct  LexKind_t864578899 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner/LexKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LexKind_t864578899, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXKIND_T864578899_H
#ifndef EVENTFILTER_T2065690828_H
#define EVENTFILTER_T2065690828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EventFilter
struct  EventFilter_t2065690828 
{
public:
	// System.Int16 System.IO.EventFilter::value__
	int16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventFilter_t2065690828, ___value___2)); }
	inline int16_t get_value___2() const { return ___value___2; }
	inline int16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFILTER_T2065690828_H
#ifndef EVENTFLAGS_T1670942752_H
#define EVENTFLAGS_T1670942752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EventFlags
struct  EventFlags_t1670942752 
{
public:
	// System.UInt16 System.IO.EventFlags::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventFlags_t1670942752, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFLAGS_T1670942752_H
#ifndef OP_T2046805169_H
#define OP_T2046805169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator/Op
struct  Op_t2046805169 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Operator/Op::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Op_t2046805169, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OP_T2046805169_H
#ifndef TIMESPEC_T893749660_H
#define TIMESPEC_T893749660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.timespec
struct  timespec_t893749660 
{
public:
	// System.IntPtr System.IO.timespec::tv_sec
	intptr_t ___tv_sec_0;
	// System.IntPtr System.IO.timespec::tv_nsec
	intptr_t ___tv_nsec_1;

public:
	inline static int32_t get_offset_of_tv_sec_0() { return static_cast<int32_t>(offsetof(timespec_t893749660, ___tv_sec_0)); }
	inline intptr_t get_tv_sec_0() const { return ___tv_sec_0; }
	inline intptr_t* get_address_of_tv_sec_0() { return &___tv_sec_0; }
	inline void set_tv_sec_0(intptr_t value)
	{
		___tv_sec_0 = value;
	}

	inline static int32_t get_offset_of_tv_nsec_1() { return static_cast<int32_t>(offsetof(timespec_t893749660, ___tv_nsec_1)); }
	inline intptr_t get_tv_nsec_1() const { return ___tv_nsec_1; }
	inline intptr_t* get_address_of_tv_nsec_1() { return &___tv_nsec_1; }
	inline void set_tv_nsec_1(intptr_t value)
	{
		___tv_nsec_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPEC_T893749660_H
#ifndef OPCODE_T3581930920_H
#define OPCODE_T3581930920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2/OpCode
struct  OpCode_t3581930920 
{
public:
	// System.Int32 System.IO.SearchPattern2/OpCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OpCode_t3581930920, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T3581930920_H
#ifndef WATCHERCHANGETYPES_T673177441_H
#define WATCHERCHANGETYPES_T673177441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WatcherChangeTypes
struct  WatcherChangeTypes_t673177441 
{
public:
	// System.Int32 System.IO.WatcherChangeTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WatcherChangeTypes_t673177441, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATCHERCHANGETYPES_T673177441_H
#ifndef DEFLATESTREAM_T4175168077_H
#define DEFLATESTREAM_T4175168077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream
struct  DeflateStream_t4175168077  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.IO.Compression.DeflateStream::base_stream
	Stream_t1273022909 * ___base_stream_4;
	// System.IO.Compression.CompressionMode System.IO.Compression.DeflateStream::mode
	int32_t ___mode_5;
	// System.Boolean System.IO.Compression.DeflateStream::leaveOpen
	bool ___leaveOpen_6;
	// System.Boolean System.IO.Compression.DeflateStream::disposed
	bool ___disposed_7;
	// System.IO.Compression.DeflateStreamNative System.IO.Compression.DeflateStream::native
	DeflateStreamNative_t1405046456 * ___native_8;

public:
	inline static int32_t get_offset_of_base_stream_4() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___base_stream_4)); }
	inline Stream_t1273022909 * get_base_stream_4() const { return ___base_stream_4; }
	inline Stream_t1273022909 ** get_address_of_base_stream_4() { return &___base_stream_4; }
	inline void set_base_stream_4(Stream_t1273022909 * value)
	{
		___base_stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_4), value);
	}

	inline static int32_t get_offset_of_mode_5() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___mode_5)); }
	inline int32_t get_mode_5() const { return ___mode_5; }
	inline int32_t* get_address_of_mode_5() { return &___mode_5; }
	inline void set_mode_5(int32_t value)
	{
		___mode_5 = value;
	}

	inline static int32_t get_offset_of_leaveOpen_6() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___leaveOpen_6)); }
	inline bool get_leaveOpen_6() const { return ___leaveOpen_6; }
	inline bool* get_address_of_leaveOpen_6() { return &___leaveOpen_6; }
	inline void set_leaveOpen_6(bool value)
	{
		___leaveOpen_6 = value;
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_native_8() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___native_8)); }
	inline DeflateStreamNative_t1405046456 * get_native_8() const { return ___native_8; }
	inline DeflateStreamNative_t1405046456 ** get_address_of_native_8() { return &___native_8; }
	inline void set_native_8(DeflateStreamNative_t1405046456 * value)
	{
		___native_8 = value;
		Il2CppCodeGenWriteBarrier((&___native_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAM_T4175168077_H
#ifndef OP_T3134810481_H
#define OP_T3134810481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2/Op
struct  Op_t3134810481  : public RuntimeObject
{
public:
	// System.IO.SearchPattern2/OpCode System.IO.SearchPattern2/Op::Code
	int32_t ___Code_0;
	// System.String System.IO.SearchPattern2/Op::Argument
	String_t* ___Argument_1;
	// System.IO.SearchPattern2/Op System.IO.SearchPattern2/Op::Next
	Op_t3134810481 * ___Next_2;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Code_0)); }
	inline int32_t get_Code_0() const { return ___Code_0; }
	inline int32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(int32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Argument_1() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Argument_1)); }
	inline String_t* get_Argument_1() const { return ___Argument_1; }
	inline String_t** get_address_of_Argument_1() { return &___Argument_1; }
	inline void set_Argument_1(String_t* value)
	{
		___Argument_1 = value;
		Il2CppCodeGenWriteBarrier((&___Argument_1), value);
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Next_2)); }
	inline Op_t3134810481 * get_Next_2() const { return ___Next_2; }
	inline Op_t3134810481 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Op_t3134810481 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OP_T3134810481_H
#ifndef WAITFORCHANGEDRESULT_T3377826936_H
#define WAITFORCHANGEDRESULT_T3377826936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WaitForChangedResult
struct  WaitForChangedResult_t3377826936 
{
public:
	// System.IO.WatcherChangeTypes System.IO.WaitForChangedResult::changeType
	int32_t ___changeType_0;
	// System.String System.IO.WaitForChangedResult::name
	String_t* ___name_1;
	// System.String System.IO.WaitForChangedResult::oldName
	String_t* ___oldName_2;
	// System.Boolean System.IO.WaitForChangedResult::timedOut
	bool ___timedOut_3;

public:
	inline static int32_t get_offset_of_changeType_0() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___changeType_0)); }
	inline int32_t get_changeType_0() const { return ___changeType_0; }
	inline int32_t* get_address_of_changeType_0() { return &___changeType_0; }
	inline void set_changeType_0(int32_t value)
	{
		___changeType_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_oldName_2() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___oldName_2)); }
	inline String_t* get_oldName_2() const { return ___oldName_2; }
	inline String_t** get_address_of_oldName_2() { return &___oldName_2; }
	inline void set_oldName_2(String_t* value)
	{
		___oldName_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldName_2), value);
	}

	inline static int32_t get_offset_of_timedOut_3() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___timedOut_3)); }
	inline bool get_timedOut_3() const { return ___timedOut_3; }
	inline bool* get_address_of_timedOut_3() { return &___timedOut_3; }
	inline void set_timedOut_3(bool value)
	{
		___timedOut_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.WaitForChangedResult
struct WaitForChangedResult_t3377826936_marshaled_pinvoke
{
	int32_t ___changeType_0;
	char* ___name_1;
	char* ___oldName_2;
	int32_t ___timedOut_3;
};
// Native definition for COM marshalling of System.IO.WaitForChangedResult
struct WaitForChangedResult_t3377826936_marshaled_com
{
	int32_t ___changeType_0;
	Il2CppChar* ___name_1;
	Il2CppChar* ___oldName_2;
	int32_t ___timedOut_3;
};
#endif // WAITFORCHANGEDRESULT_T3377826936_H
#ifndef INOTIFYWATCHER_T2392648639_H
#define INOTIFYWATCHER_T2392648639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyWatcher
struct  InotifyWatcher_t2392648639  : public RuntimeObject
{
public:

public:
};

struct InotifyWatcher_t2392648639_StaticFields
{
public:
	// System.Boolean System.IO.InotifyWatcher::failed
	bool ___failed_0;
	// System.IO.InotifyWatcher System.IO.InotifyWatcher::instance
	InotifyWatcher_t2392648639 * ___instance_1;
	// System.Collections.Hashtable System.IO.InotifyWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.InotifyWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.IntPtr System.IO.InotifyWatcher::FD
	intptr_t ___FD_4;
	// System.Threading.Thread System.IO.InotifyWatcher::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean System.IO.InotifyWatcher::stop
	bool ___stop_6;
	// System.IO.InotifyMask System.IO.InotifyWatcher::Interesting
	uint32_t ___Interesting_7;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___instance_1)); }
	inline InotifyWatcher_t2392648639 * get_instance_1() const { return ___instance_1; }
	inline InotifyWatcher_t2392648639 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(InotifyWatcher_t2392648639 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier((&___requests_3), value);
	}

	inline static int32_t get_offset_of_FD_4() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___FD_4)); }
	inline intptr_t get_FD_4() const { return ___FD_4; }
	inline intptr_t* get_address_of_FD_4() { return &___FD_4; }
	inline void set_FD_4(intptr_t value)
	{
		___FD_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___thread_5), value);
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}

	inline static int32_t get_offset_of_Interesting_7() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___Interesting_7)); }
	inline uint32_t get_Interesting_7() const { return ___Interesting_7; }
	inline uint32_t* get_address_of_Interesting_7() { return &___Interesting_7; }
	inline void set_Interesting_7(uint32_t value)
	{
		___Interesting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INOTIFYWATCHER_T2392648639_H
#ifndef FUNCTION_T1283990952_H
#define FUNCTION_T1283990952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Function
struct  Function_t1283990952  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.Function::functionType
	int32_t ___functionType_0;
	// System.Collections.ArrayList MS.Internal.Xml.XPath.Function::argumentList
	ArrayList_t2718874744 * ___argumentList_1;
	// System.String MS.Internal.Xml.XPath.Function::name
	String_t* ___name_2;
	// System.String MS.Internal.Xml.XPath.Function::prefix
	String_t* ___prefix_3;

public:
	inline static int32_t get_offset_of_functionType_0() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___functionType_0)); }
	inline int32_t get_functionType_0() const { return ___functionType_0; }
	inline int32_t* get_address_of_functionType_0() { return &___functionType_0; }
	inline void set_functionType_0(int32_t value)
	{
		___functionType_0 = value;
	}

	inline static int32_t get_offset_of_argumentList_1() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___argumentList_1)); }
	inline ArrayList_t2718874744 * get_argumentList_1() const { return ___argumentList_1; }
	inline ArrayList_t2718874744 ** get_address_of_argumentList_1() { return &___argumentList_1; }
	inline void set_argumentList_1(ArrayList_t2718874744 * value)
	{
		___argumentList_1 = value;
		Il2CppCodeGenWriteBarrier((&___argumentList_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_prefix_3() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___prefix_3)); }
	inline String_t* get_prefix_3() const { return ___prefix_3; }
	inline String_t** get_address_of_prefix_3() { return &___prefix_3; }
	inline void set_prefix_3(String_t* value)
	{
		___prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_3), value);
	}
};

struct Function_t1283990952_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.Function::ReturnTypes
	XPathResultTypeU5BU5D_t1515527577* ___ReturnTypes_4;

public:
	inline static int32_t get_offset_of_ReturnTypes_4() { return static_cast<int32_t>(offsetof(Function_t1283990952_StaticFields, ___ReturnTypes_4)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_ReturnTypes_4() const { return ___ReturnTypes_4; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_ReturnTypes_4() { return &___ReturnTypes_4; }
	inline void set_ReturnTypes_4(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___ReturnTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ReturnTypes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTION_T1283990952_H
#ifndef FILESYSTEMEVENTARGS_T1603777841_H
#define FILESYSTEMEVENTARGS_T1603777841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemEventArgs
struct  FileSystemEventArgs_t1603777841  : public EventArgs_t3591816995
{
public:
	// System.IO.WatcherChangeTypes System.IO.FileSystemEventArgs::changeType
	int32_t ___changeType_1;
	// System.String System.IO.FileSystemEventArgs::directory
	String_t* ___directory_2;
	// System.String System.IO.FileSystemEventArgs::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_changeType_1() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___changeType_1)); }
	inline int32_t get_changeType_1() const { return ___changeType_1; }
	inline int32_t* get_address_of_changeType_1() { return &___changeType_1; }
	inline void set_changeType_1(int32_t value)
	{
		___changeType_1 = value;
	}

	inline static int32_t get_offset_of_directory_2() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___directory_2)); }
	inline String_t* get_directory_2() const { return ___directory_2; }
	inline String_t** get_address_of_directory_2() { return &___directory_2; }
	inline void set_directory_2(String_t* value)
	{
		___directory_2 = value;
		Il2CppCodeGenWriteBarrier((&___directory_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMEVENTARGS_T1603777841_H
#ifndef OPERAND_T3355154092_H
#define OPERAND_T3355154092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operand
struct  Operand_t3355154092  : public AstNode_t2514041814
{
public:
	// System.Xml.XPath.XPathResultType MS.Internal.Xml.XPath.Operand::type
	int32_t ___type_0;
	// System.Object MS.Internal.Xml.XPath.Operand::val
	RuntimeObject * ___val_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Operand_t3355154092, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(Operand_t3355154092, ___val_1)); }
	inline RuntimeObject * get_val_1() const { return ___val_1; }
	inline RuntimeObject ** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(RuntimeObject * value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERAND_T3355154092_H
#ifndef OPERATOR_T966760113_H
#define OPERATOR_T966760113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator
struct  Operator_t966760113  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.Operator/Op MS.Internal.Xml.XPath.Operator::opType
	int32_t ___opType_1;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd1
	AstNode_t2514041814 * ___opnd1_2;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd2
	AstNode_t2514041814 * ___opnd2_3;

public:
	inline static int32_t get_offset_of_opType_1() { return static_cast<int32_t>(offsetof(Operator_t966760113, ___opType_1)); }
	inline int32_t get_opType_1() const { return ___opType_1; }
	inline int32_t* get_address_of_opType_1() { return &___opType_1; }
	inline void set_opType_1(int32_t value)
	{
		___opType_1 = value;
	}

	inline static int32_t get_offset_of_opnd1_2() { return static_cast<int32_t>(offsetof(Operator_t966760113, ___opnd1_2)); }
	inline AstNode_t2514041814 * get_opnd1_2() const { return ___opnd1_2; }
	inline AstNode_t2514041814 ** get_address_of_opnd1_2() { return &___opnd1_2; }
	inline void set_opnd1_2(AstNode_t2514041814 * value)
	{
		___opnd1_2 = value;
		Il2CppCodeGenWriteBarrier((&___opnd1_2), value);
	}

	inline static int32_t get_offset_of_opnd2_3() { return static_cast<int32_t>(offsetof(Operator_t966760113, ___opnd2_3)); }
	inline AstNode_t2514041814 * get_opnd2_3() const { return ___opnd2_3; }
	inline AstNode_t2514041814 ** get_address_of_opnd2_3() { return &___opnd2_3; }
	inline void set_opnd2_3(AstNode_t2514041814 * value)
	{
		___opnd2_3 = value;
		Il2CppCodeGenWriteBarrier((&___opnd2_3), value);
	}
};

struct Operator_t966760113_StaticFields
{
public:
	// MS.Internal.Xml.XPath.Operator/Op[] MS.Internal.Xml.XPath.Operator::invertOp
	OpU5BU5D_t2837398892* ___invertOp_0;

public:
	inline static int32_t get_offset_of_invertOp_0() { return static_cast<int32_t>(offsetof(Operator_t966760113_StaticFields, ___invertOp_0)); }
	inline OpU5BU5D_t2837398892* get_invertOp_0() const { return ___invertOp_0; }
	inline OpU5BU5D_t2837398892** get_address_of_invertOp_0() { return &___invertOp_0; }
	inline void set_invertOp_0(OpU5BU5D_t2837398892* value)
	{
		___invertOp_0 = value;
		Il2CppCodeGenWriteBarrier((&___invertOp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_T966760113_H
#ifndef PARAMINFO_T1233379796_H
#define PARAMINFO_T1233379796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser/ParamInfo
struct  ParamInfo_t1233379796  : public RuntimeObject
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.XPathParser/ParamInfo::ftype
	int32_t ___ftype_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::minargs
	int32_t ___minargs_1;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::maxargs
	int32_t ___maxargs_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser/ParamInfo::argTypes
	XPathResultTypeU5BU5D_t1515527577* ___argTypes_3;

public:
	inline static int32_t get_offset_of_ftype_0() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___ftype_0)); }
	inline int32_t get_ftype_0() const { return ___ftype_0; }
	inline int32_t* get_address_of_ftype_0() { return &___ftype_0; }
	inline void set_ftype_0(int32_t value)
	{
		___ftype_0 = value;
	}

	inline static int32_t get_offset_of_minargs_1() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___minargs_1)); }
	inline int32_t get_minargs_1() const { return ___minargs_1; }
	inline int32_t* get_address_of_minargs_1() { return &___minargs_1; }
	inline void set_minargs_1(int32_t value)
	{
		___minargs_1 = value;
	}

	inline static int32_t get_offset_of_maxargs_2() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___maxargs_2)); }
	inline int32_t get_maxargs_2() const { return ___maxargs_2; }
	inline int32_t* get_address_of_maxargs_2() { return &___maxargs_2; }
	inline void set_maxargs_2(int32_t value)
	{
		___maxargs_2 = value;
	}

	inline static int32_t get_offset_of_argTypes_3() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___argTypes_3)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_argTypes_3() const { return ___argTypes_3; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_argTypes_3() { return &___argTypes_3; }
	inline void set_argTypes_3(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___argTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___argTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMINFO_T1233379796_H
#ifndef XPATHSCANNER_T3283201025_H
#define XPATHSCANNER_T3283201025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner
struct  XPathScanner_t3283201025  : public RuntimeObject
{
public:
	// System.String MS.Internal.Xml.XPath.XPathScanner::xpathExpr
	String_t* ___xpathExpr_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner::xpathExprIndex
	int32_t ___xpathExprIndex_1;
	// MS.Internal.Xml.XPath.XPathScanner/LexKind MS.Internal.Xml.XPath.XPathScanner::kind
	int32_t ___kind_2;
	// System.Char MS.Internal.Xml.XPath.XPathScanner::currentChar
	Il2CppChar ___currentChar_3;
	// System.String MS.Internal.Xml.XPath.XPathScanner::name
	String_t* ___name_4;
	// System.String MS.Internal.Xml.XPath.XPathScanner::prefix
	String_t* ___prefix_5;
	// System.String MS.Internal.Xml.XPath.XPathScanner::stringValue
	String_t* ___stringValue_6;
	// System.Double MS.Internal.Xml.XPath.XPathScanner::numberValue
	double ___numberValue_7;
	// System.Boolean MS.Internal.Xml.XPath.XPathScanner::canBeFunction
	bool ___canBeFunction_8;
	// System.Xml.XmlCharType MS.Internal.Xml.XPath.XPathScanner::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_9;

public:
	inline static int32_t get_offset_of_xpathExpr_0() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___xpathExpr_0)); }
	inline String_t* get_xpathExpr_0() const { return ___xpathExpr_0; }
	inline String_t** get_address_of_xpathExpr_0() { return &___xpathExpr_0; }
	inline void set_xpathExpr_0(String_t* value)
	{
		___xpathExpr_0 = value;
		Il2CppCodeGenWriteBarrier((&___xpathExpr_0), value);
	}

	inline static int32_t get_offset_of_xpathExprIndex_1() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___xpathExprIndex_1)); }
	inline int32_t get_xpathExprIndex_1() const { return ___xpathExprIndex_1; }
	inline int32_t* get_address_of_xpathExprIndex_1() { return &___xpathExprIndex_1; }
	inline void set_xpathExprIndex_1(int32_t value)
	{
		___xpathExprIndex_1 = value;
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_currentChar_3() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___currentChar_3)); }
	inline Il2CppChar get_currentChar_3() const { return ___currentChar_3; }
	inline Il2CppChar* get_address_of_currentChar_3() { return &___currentChar_3; }
	inline void set_currentChar_3(Il2CppChar value)
	{
		___currentChar_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_prefix_5() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___prefix_5)); }
	inline String_t* get_prefix_5() const { return ___prefix_5; }
	inline String_t** get_address_of_prefix_5() { return &___prefix_5; }
	inline void set_prefix_5(String_t* value)
	{
		___prefix_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_5), value);
	}

	inline static int32_t get_offset_of_stringValue_6() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___stringValue_6)); }
	inline String_t* get_stringValue_6() const { return ___stringValue_6; }
	inline String_t** get_address_of_stringValue_6() { return &___stringValue_6; }
	inline void set_stringValue_6(String_t* value)
	{
		___stringValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_6), value);
	}

	inline static int32_t get_offset_of_numberValue_7() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___numberValue_7)); }
	inline double get_numberValue_7() const { return ___numberValue_7; }
	inline double* get_address_of_numberValue_7() { return &___numberValue_7; }
	inline void set_numberValue_7(double value)
	{
		___numberValue_7 = value;
	}

	inline static int32_t get_offset_of_canBeFunction_8() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___canBeFunction_8)); }
	inline bool get_canBeFunction_8() const { return ___canBeFunction_8; }
	inline bool* get_address_of_canBeFunction_8() { return &___canBeFunction_8; }
	inline void set_canBeFunction_8(bool value)
	{
		___canBeFunction_8 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_9() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___xmlCharType_9)); }
	inline XmlCharType_t2277243275  get_xmlCharType_9() const { return ___xmlCharType_9; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_9() { return &___xmlCharType_9; }
	inline void set_xmlCharType_9(XmlCharType_t2277243275  value)
	{
		___xmlCharType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSCANNER_T3283201025_H
#ifndef FAMWATCHER_T3228827479_H
#define FAMWATCHER_T3228827479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMWatcher
struct  FAMWatcher_t3228827479  : public RuntimeObject
{
public:

public:
};

struct FAMWatcher_t3228827479_StaticFields
{
public:
	// System.Boolean System.IO.FAMWatcher::failed
	bool ___failed_0;
	// System.IO.FAMWatcher System.IO.FAMWatcher::instance
	FAMWatcher_t3228827479 * ___instance_1;
	// System.Collections.Hashtable System.IO.FAMWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.FAMWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.IO.FAMConnection System.IO.FAMWatcher::conn
	FAMConnection_t1678998633  ___conn_4;
	// System.Threading.Thread System.IO.FAMWatcher::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean System.IO.FAMWatcher::stop
	bool ___stop_6;
	// System.Boolean System.IO.FAMWatcher::use_gamin
	bool ___use_gamin_7;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___instance_1)); }
	inline FAMWatcher_t3228827479 * get_instance_1() const { return ___instance_1; }
	inline FAMWatcher_t3228827479 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(FAMWatcher_t3228827479 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier((&___requests_3), value);
	}

	inline static int32_t get_offset_of_conn_4() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___conn_4)); }
	inline FAMConnection_t1678998633  get_conn_4() const { return ___conn_4; }
	inline FAMConnection_t1678998633 * get_address_of_conn_4() { return &___conn_4; }
	inline void set_conn_4(FAMConnection_t1678998633  value)
	{
		___conn_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___thread_5), value);
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}

	inline static int32_t get_offset_of_use_gamin_7() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___use_gamin_7)); }
	inline bool get_use_gamin_7() const { return ___use_gamin_7; }
	inline bool* get_address_of_use_gamin_7() { return &___use_gamin_7; }
	inline void set_use_gamin_7(bool value)
	{
		___use_gamin_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMWATCHER_T3228827479_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef KEVENT_T2406656960_H
#define KEVENT_T2406656960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.kevent
struct  kevent_t2406656960 
{
public:
	// System.UIntPtr System.IO.kevent::ident
	uintptr_t ___ident_0;
	// System.IO.EventFilter System.IO.kevent::filter
	int16_t ___filter_1;
	// System.IO.EventFlags System.IO.kevent::flags
	uint16_t ___flags_2;
	// System.IO.FilterFlags System.IO.kevent::fflags
	uint32_t ___fflags_3;
	// System.IntPtr System.IO.kevent::data
	intptr_t ___data_4;
	// System.IntPtr System.IO.kevent::udata
	intptr_t ___udata_5;

public:
	inline static int32_t get_offset_of_ident_0() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___ident_0)); }
	inline uintptr_t get_ident_0() const { return ___ident_0; }
	inline uintptr_t* get_address_of_ident_0() { return &___ident_0; }
	inline void set_ident_0(uintptr_t value)
	{
		___ident_0 = value;
	}

	inline static int32_t get_offset_of_filter_1() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___filter_1)); }
	inline int16_t get_filter_1() const { return ___filter_1; }
	inline int16_t* get_address_of_filter_1() { return &___filter_1; }
	inline void set_filter_1(int16_t value)
	{
		___filter_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___flags_2)); }
	inline uint16_t get_flags_2() const { return ___flags_2; }
	inline uint16_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(uint16_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_fflags_3() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___fflags_3)); }
	inline uint32_t get_fflags_3() const { return ___fflags_3; }
	inline uint32_t* get_address_of_fflags_3() { return &___fflags_3; }
	inline void set_fflags_3(uint32_t value)
	{
		___fflags_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___data_4)); }
	inline intptr_t get_data_4() const { return ___data_4; }
	inline intptr_t* get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(intptr_t value)
	{
		___data_4 = value;
	}

	inline static int32_t get_offset_of_udata_5() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___udata_5)); }
	inline intptr_t get_udata_5() const { return ___udata_5; }
	inline intptr_t* get_address_of_udata_5() { return &___udata_5; }
	inline void set_udata_5(intptr_t value)
	{
		___udata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEVENT_T2406656960_H
#ifndef INOTIFYEVENT_T2637353984_H
#define INOTIFYEVENT_T2637353984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyEvent
struct  InotifyEvent_t2637353984 
{
public:
	// System.Int32 System.IO.InotifyEvent::WatchDescriptor
	int32_t ___WatchDescriptor_0;
	// System.IO.InotifyMask System.IO.InotifyEvent::Mask
	uint32_t ___Mask_1;
	// System.String System.IO.InotifyEvent::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_WatchDescriptor_0() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___WatchDescriptor_0)); }
	inline int32_t get_WatchDescriptor_0() const { return ___WatchDescriptor_0; }
	inline int32_t* get_address_of_WatchDescriptor_0() { return &___WatchDescriptor_0; }
	inline void set_WatchDescriptor_0(int32_t value)
	{
		___WatchDescriptor_0 = value;
	}

	inline static int32_t get_offset_of_Mask_1() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___Mask_1)); }
	inline uint32_t get_Mask_1() const { return ___Mask_1; }
	inline uint32_t* get_address_of_Mask_1() { return &___Mask_1; }
	inline void set_Mask_1(uint32_t value)
	{
		___Mask_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.InotifyEvent
struct InotifyEvent_t2637353984_marshaled_pinvoke
{
	int32_t ___WatchDescriptor_0;
	uint32_t ___Mask_1;
	char* ___Name_2;
};
// Native definition for COM marshalling of System.IO.InotifyEvent
struct InotifyEvent_t2637353984_marshaled_com
{
	int32_t ___WatchDescriptor_0;
	uint32_t ___Mask_1;
	Il2CppChar* ___Name_2;
};
#endif // INOTIFYEVENT_T2637353984_H
#ifndef AXIS_T4207104559_H
#define AXIS_T4207104559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis
struct  Axis_t4207104559  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.Axis/AxisType MS.Internal.Xml.XPath.Axis::axisType
	int32_t ___axisType_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Axis::input
	AstNode_t2514041814 * ___input_1;
	// System.String MS.Internal.Xml.XPath.Axis::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.XPath.Axis::name
	String_t* ___name_3;
	// System.Xml.XPath.XPathNodeType MS.Internal.Xml.XPath.Axis::nodeType
	int32_t ___nodeType_4;
	// System.Boolean MS.Internal.Xml.XPath.Axis::abbrAxis
	bool ___abbrAxis_5;
	// System.String MS.Internal.Xml.XPath.Axis::urn
	String_t* ___urn_6;

public:
	inline static int32_t get_offset_of_axisType_0() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___axisType_0)); }
	inline int32_t get_axisType_0() const { return ___axisType_0; }
	inline int32_t* get_address_of_axisType_0() { return &___axisType_0; }
	inline void set_axisType_0(int32_t value)
	{
		___axisType_0 = value;
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___input_1)); }
	inline AstNode_t2514041814 * get_input_1() const { return ___input_1; }
	inline AstNode_t2514041814 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(AstNode_t2514041814 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_abbrAxis_5() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___abbrAxis_5)); }
	inline bool get_abbrAxis_5() const { return ___abbrAxis_5; }
	inline bool* get_address_of_abbrAxis_5() { return &___abbrAxis_5; }
	inline void set_abbrAxis_5(bool value)
	{
		___abbrAxis_5 = value;
	}

	inline static int32_t get_offset_of_urn_6() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___urn_6)); }
	inline String_t* get_urn_6() const { return ___urn_6; }
	inline String_t** get_address_of_urn_6() { return &___urn_6; }
	inline void set_urn_6(String_t* value)
	{
		___urn_6 = value;
		Il2CppCodeGenWriteBarrier((&___urn_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T4207104559_H
#ifndef ERROREVENTHANDLER_T2621677363_H
#define ERROREVENTHANDLER_T2621677363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ErrorEventHandler
struct  ErrorEventHandler_t2621677363  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_T2621677363_H
#ifndef RENAMEDEVENTARGS_T2350765466_H
#define RENAMEDEVENTARGS_T2350765466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.RenamedEventArgs
struct  RenamedEventArgs_t2350765466  : public FileSystemEventArgs_t1603777841
{
public:
	// System.String System.IO.RenamedEventArgs::oldName
	String_t* ___oldName_4;
	// System.String System.IO.RenamedEventArgs::oldFullPath
	String_t* ___oldFullPath_5;

public:
	inline static int32_t get_offset_of_oldName_4() { return static_cast<int32_t>(offsetof(RenamedEventArgs_t2350765466, ___oldName_4)); }
	inline String_t* get_oldName_4() const { return ___oldName_4; }
	inline String_t** get_address_of_oldName_4() { return &___oldName_4; }
	inline void set_oldName_4(String_t* value)
	{
		___oldName_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldName_4), value);
	}

	inline static int32_t get_offset_of_oldFullPath_5() { return static_cast<int32_t>(offsetof(RenamedEventArgs_t2350765466, ___oldFullPath_5)); }
	inline String_t* get_oldFullPath_5() const { return ___oldFullPath_5; }
	inline String_t** get_address_of_oldFullPath_5() { return &___oldFullPath_5; }
	inline void set_oldFullPath_5(String_t* value)
	{
		___oldFullPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___oldFullPath_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENAMEDEVENTARGS_T2350765466_H
#ifndef RENAMEDEVENTHANDLER_T3047461033_H
#define RENAMEDEVENTHANDLER_T3047461033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.RenamedEventHandler
struct  RenamedEventHandler_t3047461033  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENAMEDEVENTHANDLER_T3047461033_H
#ifndef WRITEMETHOD_T2538911768_H
#define WRITEMETHOD_T2538911768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/WriteMethod
struct  WriteMethod_t2538911768  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEMETHOD_T2538911768_H
#ifndef READMETHOD_T893206259_H
#define READMETHOD_T893206259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/ReadMethod
struct  ReadMethod_t893206259  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READMETHOD_T893206259_H
#ifndef FILESYSTEMWATCHER_T416760199_H
#define FILESYSTEMWATCHER_T416760199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemWatcher
struct  FileSystemWatcher_t416760199  : public Component_t3620823400
{
public:
	// System.Boolean System.IO.FileSystemWatcher::enableRaisingEvents
	bool ___enableRaisingEvents_4;
	// System.String System.IO.FileSystemWatcher::filter
	String_t* ___filter_5;
	// System.Boolean System.IO.FileSystemWatcher::includeSubdirectories
	bool ___includeSubdirectories_6;
	// System.Int32 System.IO.FileSystemWatcher::internalBufferSize
	int32_t ___internalBufferSize_7;
	// System.IO.NotifyFilters System.IO.FileSystemWatcher::notifyFilter
	int32_t ___notifyFilter_8;
	// System.String System.IO.FileSystemWatcher::path
	String_t* ___path_9;
	// System.String System.IO.FileSystemWatcher::fullpath
	String_t* ___fullpath_10;
	// System.ComponentModel.ISynchronizeInvoke System.IO.FileSystemWatcher::synchronizingObject
	RuntimeObject* ___synchronizingObject_11;
	// System.IO.WaitForChangedResult System.IO.FileSystemWatcher::lastData
	WaitForChangedResult_t3377826936  ___lastData_12;
	// System.Boolean System.IO.FileSystemWatcher::waiting
	bool ___waiting_13;
	// System.IO.SearchPattern2 System.IO.FileSystemWatcher::pattern
	SearchPattern2_t2824637351 * ___pattern_14;
	// System.Boolean System.IO.FileSystemWatcher::disposed
	bool ___disposed_15;
	// System.String System.IO.FileSystemWatcher::mangledFilter
	String_t* ___mangledFilter_16;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Changed
	FileSystemEventHandler_t1806121106 * ___Changed_19;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Created
	FileSystemEventHandler_t1806121106 * ___Created_20;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Deleted
	FileSystemEventHandler_t1806121106 * ___Deleted_21;
	// System.IO.ErrorEventHandler System.IO.FileSystemWatcher::Error
	ErrorEventHandler_t2621677363 * ___Error_22;
	// System.IO.RenamedEventHandler System.IO.FileSystemWatcher::Renamed
	RenamedEventHandler_t3047461033 * ___Renamed_23;

public:
	inline static int32_t get_offset_of_enableRaisingEvents_4() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___enableRaisingEvents_4)); }
	inline bool get_enableRaisingEvents_4() const { return ___enableRaisingEvents_4; }
	inline bool* get_address_of_enableRaisingEvents_4() { return &___enableRaisingEvents_4; }
	inline void set_enableRaisingEvents_4(bool value)
	{
		___enableRaisingEvents_4 = value;
	}

	inline static int32_t get_offset_of_filter_5() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___filter_5)); }
	inline String_t* get_filter_5() const { return ___filter_5; }
	inline String_t** get_address_of_filter_5() { return &___filter_5; }
	inline void set_filter_5(String_t* value)
	{
		___filter_5 = value;
		Il2CppCodeGenWriteBarrier((&___filter_5), value);
	}

	inline static int32_t get_offset_of_includeSubdirectories_6() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___includeSubdirectories_6)); }
	inline bool get_includeSubdirectories_6() const { return ___includeSubdirectories_6; }
	inline bool* get_address_of_includeSubdirectories_6() { return &___includeSubdirectories_6; }
	inline void set_includeSubdirectories_6(bool value)
	{
		___includeSubdirectories_6 = value;
	}

	inline static int32_t get_offset_of_internalBufferSize_7() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___internalBufferSize_7)); }
	inline int32_t get_internalBufferSize_7() const { return ___internalBufferSize_7; }
	inline int32_t* get_address_of_internalBufferSize_7() { return &___internalBufferSize_7; }
	inline void set_internalBufferSize_7(int32_t value)
	{
		___internalBufferSize_7 = value;
	}

	inline static int32_t get_offset_of_notifyFilter_8() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___notifyFilter_8)); }
	inline int32_t get_notifyFilter_8() const { return ___notifyFilter_8; }
	inline int32_t* get_address_of_notifyFilter_8() { return &___notifyFilter_8; }
	inline void set_notifyFilter_8(int32_t value)
	{
		___notifyFilter_8 = value;
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___path_9)); }
	inline String_t* get_path_9() const { return ___path_9; }
	inline String_t** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(String_t* value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier((&___path_9), value);
	}

	inline static int32_t get_offset_of_fullpath_10() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___fullpath_10)); }
	inline String_t* get_fullpath_10() const { return ___fullpath_10; }
	inline String_t** get_address_of_fullpath_10() { return &___fullpath_10; }
	inline void set_fullpath_10(String_t* value)
	{
		___fullpath_10 = value;
		Il2CppCodeGenWriteBarrier((&___fullpath_10), value);
	}

	inline static int32_t get_offset_of_synchronizingObject_11() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___synchronizingObject_11)); }
	inline RuntimeObject* get_synchronizingObject_11() const { return ___synchronizingObject_11; }
	inline RuntimeObject** get_address_of_synchronizingObject_11() { return &___synchronizingObject_11; }
	inline void set_synchronizingObject_11(RuntimeObject* value)
	{
		___synchronizingObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___synchronizingObject_11), value);
	}

	inline static int32_t get_offset_of_lastData_12() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___lastData_12)); }
	inline WaitForChangedResult_t3377826936  get_lastData_12() const { return ___lastData_12; }
	inline WaitForChangedResult_t3377826936 * get_address_of_lastData_12() { return &___lastData_12; }
	inline void set_lastData_12(WaitForChangedResult_t3377826936  value)
	{
		___lastData_12 = value;
	}

	inline static int32_t get_offset_of_waiting_13() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___waiting_13)); }
	inline bool get_waiting_13() const { return ___waiting_13; }
	inline bool* get_address_of_waiting_13() { return &___waiting_13; }
	inline void set_waiting_13(bool value)
	{
		___waiting_13 = value;
	}

	inline static int32_t get_offset_of_pattern_14() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___pattern_14)); }
	inline SearchPattern2_t2824637351 * get_pattern_14() const { return ___pattern_14; }
	inline SearchPattern2_t2824637351 ** get_address_of_pattern_14() { return &___pattern_14; }
	inline void set_pattern_14(SearchPattern2_t2824637351 * value)
	{
		___pattern_14 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_14), value);
	}

	inline static int32_t get_offset_of_disposed_15() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___disposed_15)); }
	inline bool get_disposed_15() const { return ___disposed_15; }
	inline bool* get_address_of_disposed_15() { return &___disposed_15; }
	inline void set_disposed_15(bool value)
	{
		___disposed_15 = value;
	}

	inline static int32_t get_offset_of_mangledFilter_16() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___mangledFilter_16)); }
	inline String_t* get_mangledFilter_16() const { return ___mangledFilter_16; }
	inline String_t** get_address_of_mangledFilter_16() { return &___mangledFilter_16; }
	inline void set_mangledFilter_16(String_t* value)
	{
		___mangledFilter_16 = value;
		Il2CppCodeGenWriteBarrier((&___mangledFilter_16), value);
	}

	inline static int32_t get_offset_of_Changed_19() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Changed_19)); }
	inline FileSystemEventHandler_t1806121106 * get_Changed_19() const { return ___Changed_19; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Changed_19() { return &___Changed_19; }
	inline void set_Changed_19(FileSystemEventHandler_t1806121106 * value)
	{
		___Changed_19 = value;
		Il2CppCodeGenWriteBarrier((&___Changed_19), value);
	}

	inline static int32_t get_offset_of_Created_20() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Created_20)); }
	inline FileSystemEventHandler_t1806121106 * get_Created_20() const { return ___Created_20; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Created_20() { return &___Created_20; }
	inline void set_Created_20(FileSystemEventHandler_t1806121106 * value)
	{
		___Created_20 = value;
		Il2CppCodeGenWriteBarrier((&___Created_20), value);
	}

	inline static int32_t get_offset_of_Deleted_21() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Deleted_21)); }
	inline FileSystemEventHandler_t1806121106 * get_Deleted_21() const { return ___Deleted_21; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Deleted_21() { return &___Deleted_21; }
	inline void set_Deleted_21(FileSystemEventHandler_t1806121106 * value)
	{
		___Deleted_21 = value;
		Il2CppCodeGenWriteBarrier((&___Deleted_21), value);
	}

	inline static int32_t get_offset_of_Error_22() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Error_22)); }
	inline ErrorEventHandler_t2621677363 * get_Error_22() const { return ___Error_22; }
	inline ErrorEventHandler_t2621677363 ** get_address_of_Error_22() { return &___Error_22; }
	inline void set_Error_22(ErrorEventHandler_t2621677363 * value)
	{
		___Error_22 = value;
		Il2CppCodeGenWriteBarrier((&___Error_22), value);
	}

	inline static int32_t get_offset_of_Renamed_23() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Renamed_23)); }
	inline RenamedEventHandler_t3047461033 * get_Renamed_23() const { return ___Renamed_23; }
	inline RenamedEventHandler_t3047461033 ** get_address_of_Renamed_23() { return &___Renamed_23; }
	inline void set_Renamed_23(RenamedEventHandler_t3047461033 * value)
	{
		___Renamed_23 = value;
		Il2CppCodeGenWriteBarrier((&___Renamed_23), value);
	}
};

struct FileSystemWatcher_t416760199_StaticFields
{
public:
	// System.IO.IFileWatcher System.IO.FileSystemWatcher::watcher
	RuntimeObject* ___watcher_17;
	// System.Object System.IO.FileSystemWatcher::lockobj
	RuntimeObject * ___lockobj_18;

public:
	inline static int32_t get_offset_of_watcher_17() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199_StaticFields, ___watcher_17)); }
	inline RuntimeObject* get_watcher_17() const { return ___watcher_17; }
	inline RuntimeObject** get_address_of_watcher_17() { return &___watcher_17; }
	inline void set_watcher_17(RuntimeObject* value)
	{
		___watcher_17 = value;
		Il2CppCodeGenWriteBarrier((&___watcher_17), value);
	}

	inline static int32_t get_offset_of_lockobj_18() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199_StaticFields, ___lockobj_18)); }
	inline RuntimeObject * get_lockobj_18() const { return ___lockobj_18; }
	inline RuntimeObject ** get_address_of_lockobj_18() { return &___lockobj_18; }
	inline void set_lockobj_18(RuntimeObject * value)
	{
		___lockobj_18 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMWATCHER_T416760199_H
#ifndef FILESYSTEMEVENTHANDLER_T1806121106_H
#define FILESYSTEMEVENTHANDLER_T1806121106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemEventHandler
struct  FileSystemEventHandler_t1806121106  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMEVENTHANDLER_T1806121106_H
#ifndef UNMANAGEDREADORWRITE_T1975956110_H
#define UNMANAGEDREADORWRITE_T1975956110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite
struct  UnmanagedReadOrWrite_t1975956110  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMANAGEDREADORWRITE_T1975956110_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (DefaultWatcher_t2229106420), -1, sizeof(DefaultWatcher_t2229106420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2500[4] = 
{
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_instance_0(),
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_thread_1(),
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_watches_2(),
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_NoStringsArray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (ErrorEventArgs_t1584858912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	ErrorEventArgs_t1584858912::get_offset_of_exception_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (ErrorEventHandler_t2621677363), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (FAMConnection_t1678998633)+ sizeof (RuntimeObject), sizeof(FAMConnection_t1678998633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	FAMConnection_t1678998633::get_offset_of_FD_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FAMConnection_t1678998633::get_offset_of_opaque_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (FAMRequest_t3578860103)+ sizeof (RuntimeObject), sizeof(FAMRequest_t3578860103 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	FAMRequest_t3578860103::get_offset_of_ReqNum_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (FAMCodes_t1537683418)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[10] = 
{
	FAMCodes_t1537683418::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (FAMData_t1817296356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[7] = 
{
	FAMData_t1817296356::get_offset_of_FSW_0(),
	FAMData_t1817296356::get_offset_of_Directory_1(),
	FAMData_t1817296356::get_offset_of_FileMask_2(),
	FAMData_t1817296356::get_offset_of_IncludeSubdirs_3(),
	FAMData_t1817296356::get_offset_of_Enabled_4(),
	FAMData_t1817296356::get_offset_of_Request_5(),
	FAMData_t1817296356::get_offset_of_SubDirs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (FAMWatcher_t3228827479), -1, sizeof(FAMWatcher_t3228827479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[8] = 
{
	FAMWatcher_t3228827479_StaticFields::get_offset_of_failed_0(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_instance_1(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_watches_2(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_requests_3(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_conn_4(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_thread_5(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_stop_6(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_use_gamin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (FileAction_t3839877343)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[6] = 
{
	FileAction_t3839877343::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (FileSystemEventArgs_t1603777841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[3] = 
{
	FileSystemEventArgs_t1603777841::get_offset_of_changeType_1(),
	FileSystemEventArgs_t1603777841::get_offset_of_directory_2(),
	FileSystemEventArgs_t1603777841::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (FileSystemEventHandler_t1806121106), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (FileSystemWatcher_t416760199), -1, sizeof(FileSystemWatcher_t416760199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2511[20] = 
{
	FileSystemWatcher_t416760199::get_offset_of_enableRaisingEvents_4(),
	FileSystemWatcher_t416760199::get_offset_of_filter_5(),
	FileSystemWatcher_t416760199::get_offset_of_includeSubdirectories_6(),
	FileSystemWatcher_t416760199::get_offset_of_internalBufferSize_7(),
	FileSystemWatcher_t416760199::get_offset_of_notifyFilter_8(),
	FileSystemWatcher_t416760199::get_offset_of_path_9(),
	FileSystemWatcher_t416760199::get_offset_of_fullpath_10(),
	FileSystemWatcher_t416760199::get_offset_of_synchronizingObject_11(),
	FileSystemWatcher_t416760199::get_offset_of_lastData_12(),
	FileSystemWatcher_t416760199::get_offset_of_waiting_13(),
	FileSystemWatcher_t416760199::get_offset_of_pattern_14(),
	FileSystemWatcher_t416760199::get_offset_of_disposed_15(),
	FileSystemWatcher_t416760199::get_offset_of_mangledFilter_16(),
	FileSystemWatcher_t416760199_StaticFields::get_offset_of_watcher_17(),
	FileSystemWatcher_t416760199_StaticFields::get_offset_of_lockobj_18(),
	FileSystemWatcher_t416760199::get_offset_of_Changed_19(),
	FileSystemWatcher_t416760199::get_offset_of_Created_20(),
	FileSystemWatcher_t416760199::get_offset_of_Deleted_21(),
	FileSystemWatcher_t416760199::get_offset_of_Error_22(),
	FileSystemWatcher_t416760199::get_offset_of_Renamed_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (EventType_t2132206657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2512[4] = 
{
	EventType_t2132206657::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (IODescriptionAttribute_t2336130459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (InotifyMask_t3323194736)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2515[22] = 
{
	InotifyMask_t3323194736::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (InotifyEvent_t2637353984)+ sizeof (RuntimeObject), sizeof(InotifyEvent_t2637353984_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2516[3] = 
{
	InotifyEvent_t2637353984::get_offset_of_WatchDescriptor_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InotifyEvent_t2637353984::get_offset_of_Mask_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InotifyEvent_t2637353984::get_offset_of_Name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (ParentInotifyData_t1149622319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[4] = 
{
	ParentInotifyData_t1149622319::get_offset_of_IncludeSubdirs_0(),
	ParentInotifyData_t1149622319::get_offset_of_Enabled_1(),
	ParentInotifyData_t1149622319::get_offset_of_children_2(),
	ParentInotifyData_t1149622319::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (InotifyData_t2533354870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[3] = 
{
	InotifyData_t2533354870::get_offset_of_FSW_0(),
	InotifyData_t2533354870::get_offset_of_Directory_1(),
	InotifyData_t2533354870::get_offset_of_Watch_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (InotifyWatcher_t2392648639), -1, sizeof(InotifyWatcher_t2392648639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2519[8] = 
{
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_failed_0(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_instance_1(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_watches_2(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_requests_3(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_FD_4(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_thread_5(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_stop_6(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_Interesting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (U3CGetEnumeratorU3Ed__18_t986872067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[7] = 
{
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_source_3(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3E3__source_4(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3ClistU3E5__1_5(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CiU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (EventFlags_t1670942752)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[14] = 
{
	EventFlags_t1670942752::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (EventFilter_t2065690828)+ sizeof (RuntimeObject), sizeof(int16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[12] = 
{
	EventFilter_t2065690828::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (FilterFlags_t1388790893)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[42] = 
{
	FilterFlags_t1388790893::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (kevent_t2406656960)+ sizeof (RuntimeObject), sizeof(kevent_t2406656960 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2524[6] = 
{
	kevent_t2406656960::get_offset_of_ident_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_filter_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_flags_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_fflags_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_data_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_udata_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (timespec_t893749660)+ sizeof (RuntimeObject), sizeof(timespec_t893749660 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2525[2] = 
{
	timespec_t893749660::get_offset_of_tv_sec_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	timespec_t893749660::get_offset_of_tv_nsec_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (PathData_t3217195029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[3] = 
{
	PathData_t3217195029::get_offset_of_Path_0(),
	PathData_t3217195029::get_offset_of_IsDirectory_1(),
	PathData_t3217195029::get_offset_of_Fd_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (KqueueMonitor_t3818269198), -1, sizeof(KqueueMonitor_t3818269198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[17] = 
{
	KqueueMonitor_t3818269198_StaticFields::get_offset_of_initialized_0(),
	KqueueMonitor_t3818269198_StaticFields::get_offset_of_emptyEventList_1(),
	KqueueMonitor_t3818269198::get_offset_of_maxFds_2(),
	KqueueMonitor_t3818269198::get_offset_of_fsw_3(),
	KqueueMonitor_t3818269198::get_offset_of_conn_4(),
	KqueueMonitor_t3818269198::get_offset_of_thread_5(),
	KqueueMonitor_t3818269198::get_offset_of_requestStop_6(),
	KqueueMonitor_t3818269198::get_offset_of_startedEvent_7(),
	KqueueMonitor_t3818269198::get_offset_of_started_8(),
	KqueueMonitor_t3818269198::get_offset_of_inDispatch_9(),
	KqueueMonitor_t3818269198::get_offset_of_exc_10(),
	KqueueMonitor_t3818269198::get_offset_of_stateLock_11(),
	KqueueMonitor_t3818269198::get_offset_of_connLock_12(),
	KqueueMonitor_t3818269198::get_offset_of_pathsDict_13(),
	KqueueMonitor_t3818269198::get_offset_of_fdsDict_14(),
	KqueueMonitor_t3818269198::get_offset_of_fixupPath_15(),
	KqueueMonitor_t3818269198::get_offset_of_fullPathNoLastSlash_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (U3CU3Ec__DisplayClass11_0_t2361320332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	U3CU3Ec__DisplayClass11_0_t2361320332::get_offset_of_newFds_0(),
	U3CU3Ec__DisplayClass11_0_t2361320332::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass11_0_t2361320332::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (KeventWatcher_t3355132332), -1, sizeof(KeventWatcher_t3355132332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2529[3] = 
{
	KeventWatcher_t3355132332_StaticFields::get_offset_of_failed_0(),
	KeventWatcher_t3355132332_StaticFields::get_offset_of_instance_1(),
	KeventWatcher_t3355132332_StaticFields::get_offset_of_watches_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (NotifyFilters_t3825601669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2530[9] = 
{
	NotifyFilters_t3825601669::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (NullFileWatcher_t1718214284), -1, sizeof(NullFileWatcher_t1718214284_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2531[1] = 
{
	NullFileWatcher_t1718214284_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (RenamedEventArgs_t2350765466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[2] = 
{
	RenamedEventArgs_t2350765466::get_offset_of_oldName_4(),
	RenamedEventArgs_t2350765466::get_offset_of_oldFullPath_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (RenamedEventHandler_t3047461033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (SearchPattern2_t2824637351), -1, sizeof(SearchPattern2_t2824637351_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2534[6] = 
{
	SearchPattern2_t2824637351::get_offset_of_ops_0(),
	SearchPattern2_t2824637351::get_offset_of_ignore_1(),
	SearchPattern2_t2824637351::get_offset_of_hasWildcard_2(),
	SearchPattern2_t2824637351::get_offset_of_pattern_3(),
	SearchPattern2_t2824637351_StaticFields::get_offset_of_WildcardChars_4(),
	SearchPattern2_t2824637351_StaticFields::get_offset_of_InvalidChars_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (Op_t3134810481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[3] = 
{
	Op_t3134810481::get_offset_of_Code_0(),
	Op_t3134810481::get_offset_of_Argument_1(),
	Op_t3134810481::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (OpCode_t3581930920)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2536[6] = 
{
	OpCode_t3581930920::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (WaitForChangedResult_t3377826936)+ sizeof (RuntimeObject), sizeof(WaitForChangedResult_t3377826936_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	WaitForChangedResult_t3377826936::get_offset_of_changeType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WaitForChangedResult_t3377826936::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WaitForChangedResult_t3377826936::get_offset_of_oldName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WaitForChangedResult_t3377826936::get_offset_of_timedOut_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (WatcherChangeTypes_t673177441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[6] = 
{
	WatcherChangeTypes_t673177441::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (WindowsWatcher_t1958179572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (CompressionMode_t3714291783)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[3] = 
{
	CompressionMode_t3714291783::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (GZipStream_t3417139389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[1] = 
{
	GZipStream_t3417139389::get_offset_of__deflateStream_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (DeflateStream_t4175168077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[5] = 
{
	DeflateStream_t4175168077::get_offset_of_base_stream_4(),
	DeflateStream_t4175168077::get_offset_of_mode_5(),
	DeflateStream_t4175168077::get_offset_of_leaveOpen_6(),
	DeflateStream_t4175168077::get_offset_of_disposed_7(),
	DeflateStream_t4175168077::get_offset_of_native_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (ReadMethod_t893206259), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (WriteMethod_t2538911768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (DeflateStreamNative_t1405046456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[6] = 
{
	DeflateStreamNative_t1405046456::get_offset_of_feeder_0(),
	DeflateStreamNative_t1405046456::get_offset_of_base_stream_1(),
	DeflateStreamNative_t1405046456::get_offset_of_z_stream_2(),
	DeflateStreamNative_t1405046456::get_offset_of_data_3(),
	DeflateStreamNative_t1405046456::get_offset_of_disposed_4(),
	DeflateStreamNative_t1405046456::get_offset_of_io_buffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (UnmanagedReadOrWrite_t1975956110), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255364), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2547[16] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U30283A6AF88802AB45989B29549915BEA0F6CD515_0(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U31A39764B112685485A5BA7B2880D878B858C1A7A_2(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_5(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_CCEEADA43268372341F81AE0C9208C6856441C04_12(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_EEAFE8C6E1AB017237567305EE925C976CDB6458_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (__StaticArrayInitTypeSizeU3D3_t3217885684)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3_t3217885684 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (__StaticArrayInitTypeSizeU3D6_t3217689075)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t3217689075 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (__StaticArrayInitTypeSizeU3D9_t3218278900)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D9_t3218278900 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994318)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994318 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (__StaticArrayInitTypeSizeU3D14_t3517563373)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D14_t3517563373 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2711125391)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2711125391 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (__StaticArrayInitTypeSizeU3D44_t3517366764)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_t3517366764 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (__StaticArrayInitTypeSizeU3D128_t531529102)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D128_t531529102 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (__StaticArrayInitTypeSizeU3D256_t1757367633)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_t1757367633 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (U3CModuleU3E_t692745529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (SR_t167583545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (AstNode_t2514041814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (AstType_t3854428833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2560[10] = 
{
	AstType_t3854428833::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (Axis_t4207104559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[7] = 
{
	Axis_t4207104559::get_offset_of_axisType_0(),
	Axis_t4207104559::get_offset_of_input_1(),
	Axis_t4207104559::get_offset_of_prefix_2(),
	Axis_t4207104559::get_offset_of_name_3(),
	Axis_t4207104559::get_offset_of_nodeType_4(),
	Axis_t4207104559::get_offset_of_abbrAxis_5(),
	Axis_t4207104559::get_offset_of_urn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (AxisType_t3322599580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[15] = 
{
	AxisType_t3322599580::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (Filter_t1571657935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[2] = 
{
	Filter_t1571657935::get_offset_of_input_0(),
	Filter_t1571657935::get_offset_of_condition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (Function_t1283990952), -1, sizeof(Function_t1283990952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[5] = 
{
	Function_t1283990952::get_offset_of_functionType_0(),
	Function_t1283990952::get_offset_of_argumentList_1(),
	Function_t1283990952::get_offset_of_name_2(),
	Function_t1283990952::get_offset_of_prefix_3(),
	Function_t1283990952_StaticFields::get_offset_of_ReturnTypes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (FunctionType_t3319434782)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2565[29] = 
{
	FunctionType_t3319434782::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (Group_t100818710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[1] = 
{
	Group_t100818710::get_offset_of_groupNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (Operand_t3355154092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	Operand_t3355154092::get_offset_of_type_0(),
	Operand_t3355154092::get_offset_of_val_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (Operator_t966760113), -1, sizeof(Operator_t966760113_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[4] = 
{
	Operator_t966760113_StaticFields::get_offset_of_invertOp_0(),
	Operator_t966760113::get_offset_of_opType_1(),
	Operator_t966760113::get_offset_of_opnd1_2(),
	Operator_t966760113::get_offset_of_opnd2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (Op_t2046805169)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[16] = 
{
	Op_t2046805169::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (Root_t720671714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (Variable_t262588068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[2] = 
{
	Variable_t262588068::get_offset_of_localname_0(),
	Variable_t262588068::get_offset_of_prefix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (XPathParser_t618394529), -1, sizeof(XPathParser_t618394529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[13] = 
{
	XPathParser_t618394529::get_offset_of_scanner_0(),
	XPathParser_t618394529::get_offset_of_parseDepth_1(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray1_2(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray2_3(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray3_4(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray4_5(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray5_6(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray6_7(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray7_8(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray8_9(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray9_10(),
	XPathParser_t618394529_StaticFields::get_offset_of_functionTable_11(),
	XPathParser_t618394529_StaticFields::get_offset_of_AxesTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (ParamInfo_t1233379796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	ParamInfo_t1233379796::get_offset_of_ftype_0(),
	ParamInfo_t1233379796::get_offset_of_minargs_1(),
	ParamInfo_t1233379796::get_offset_of_maxargs_2(),
	ParamInfo_t1233379796::get_offset_of_argTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (XPathScanner_t3283201025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[10] = 
{
	XPathScanner_t3283201025::get_offset_of_xpathExpr_0(),
	XPathScanner_t3283201025::get_offset_of_xpathExprIndex_1(),
	XPathScanner_t3283201025::get_offset_of_kind_2(),
	XPathScanner_t3283201025::get_offset_of_currentChar_3(),
	XPathScanner_t3283201025::get_offset_of_name_4(),
	XPathScanner_t3283201025::get_offset_of_prefix_5(),
	XPathScanner_t3283201025::get_offset_of_stringValue_6(),
	XPathScanner_t3283201025::get_offset_of_numberValue_7(),
	XPathScanner_t3283201025::get_offset_of_canBeFunction_8(),
	XPathScanner_t3283201025::get_offset_of_xmlCharType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (LexKind_t864578899)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[32] = 
{
	LexKind_t864578899::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (XPathDocumentNavigator_t2457178823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[4] = 
{
	XPathDocumentNavigator_t2457178823::get_offset_of_pageCurrent_4(),
	XPathDocumentNavigator_t2457178823::get_offset_of_pageParent_5(),
	XPathDocumentNavigator_t2457178823::get_offset_of_idxCurrent_6(),
	XPathDocumentNavigator_t2457178823::get_offset_of_idxParent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (XPathNode_t2208072876)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[7] = 
{
	XPathNode_t2208072876::get_offset_of_info_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_idxSibling_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_idxParent_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_idxSimilar_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_posOffset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_props_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_value_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (XPathNodeRef_t3498189018)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[2] = 
{
	XPathNodeRef_t3498189018::get_offset_of_page_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNodeRef_t3498189018::get_offset_of_idx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (XPathNodeHelper_t2230825274), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (XPathNodePageInfo_t2343388010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[3] = 
{
	XPathNodePageInfo_t2343388010::get_offset_of_pageNum_0(),
	XPathNodePageInfo_t2343388010::get_offset_of_nodeCount_1(),
	XPathNodePageInfo_t2343388010::get_offset_of_pageNext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (XPathNodeInfoAtom_t1760358141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[13] = 
{
	XPathNodeInfoAtom_t1760358141::get_offset_of_localName_0(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_namespaceUri_1(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_prefix_2(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_baseUri_3(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageParent_4(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageSibling_5(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageSimilar_6(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_doc_7(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_lineNumBase_8(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_linePosBase_9(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_hashCode_10(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_localNameHash_11(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Res_t3627928856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Base64Encoder_t3938083961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[3] = 
{
	Base64Encoder_t3938083961::get_offset_of_leftOverBytes_0(),
	Base64Encoder_t3938083961::get_offset_of_leftOverBytesCount_1(),
	Base64Encoder_t3938083961::get_offset_of_charsLine_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (XmlTextWriterBase64Encoder_t4259465041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[1] = 
{
	XmlTextWriterBase64Encoder_t4259465041::get_offset_of_xmlTextEncoder_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (BinHexDecoder_t1474272384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[5] = 
{
	BinHexDecoder_t1474272384::get_offset_of_buffer_0(),
	BinHexDecoder_t1474272384::get_offset_of_curIndex_1(),
	BinHexDecoder_t1474272384::get_offset_of_endIndex_2(),
	BinHexDecoder_t1474272384::get_offset_of_hasHalfByteCached_3(),
	BinHexDecoder_t1474272384::get_offset_of_cachedHalfByte_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (BinHexEncoder_t1687308627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Bits_t3566938933), -1, sizeof(Bits_t3566938933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2587[5] = 
{
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0101010101010101_0(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0011001100110011_1(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0000111100001111_2(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0000000011111111_3(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_1111111111111111_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (BinaryCompatibility_t2660327299), -1, sizeof(BinaryCompatibility_t2660327299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2588[1] = 
{
	BinaryCompatibility_t2660327299_StaticFields::get_offset_of__targetsAtLeast_Desktop_V4_5_2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (ConformanceLevel_t3899847875)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2589[4] = 
{
	ConformanceLevel_t3899847875::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (DtdProcessing_t1163997051)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2590[4] = 
{
	DtdProcessing_t1163997051::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (EntityHandling_t1047276436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2591[3] = 
{
	EntityHandling_t1047276436::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
