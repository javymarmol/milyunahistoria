﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t508516997;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t3219022681;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;

extern RuntimeClass* PlayableAsset_t3219022681_il2cpp_TypeInfo_var;
extern const uint32_t PlayableDirector_get_playableAsset_m3386433828_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745539_H
#define U3CMODULEU3E_T692745539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745539 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745539_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef PROPERTYNAME_T3749835189_H
#define PROPERTYNAME_T3749835189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t3749835189 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t3749835189, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T3749835189_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DIRECTORWRAPMODE_T3150873520_H
#define DIRECTORWRAPMODE_T3150873520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DirectorWrapMode
struct  DirectorWrapMode_t3150873520 
{
public:
	// System.Int32 UnityEngine.Playables.DirectorWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectorWrapMode_t3150873520, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORWRAPMODE_T3150873520_H
#ifndef PLAYABLEGRAPH_T3515989261_H
#define PLAYABLEGRAPH_T3515989261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t3515989261 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t3515989261, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t3515989261, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T3515989261_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef PLAYABLEASSET_T3219022681_H
#define PLAYABLEASSET_T3219022681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t3219022681  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T3219022681_H
#ifndef PLAYABLEDIRECTOR_T508516997_H
#define PLAYABLEDIRECTOR_T508516997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableDirector
struct  PlayableDirector_t508516997  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEDIRECTOR_T508516997_H



// UnityEngine.Object UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_GetReferenceValue(UnityEngine.Playables.PlayableDirector,UnityEngine.PropertyName&,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1447819544 (RuntimeObject * __this /* static, unused */, PlayableDirector_t508516997 * ___self0, PropertyName_t3749835189 * ___id1, bool* ___idValid2, const RuntimeMethod* method);
// UnityEngine.Playables.DirectorWrapMode UnityEngine.Playables.PlayableDirector::GetWrapMode()
extern "C" IL2CPP_METHOD_ATTR int32_t PlayableDirector_GetWrapMode_m3827033998 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method);
// UnityEngine.ScriptableObject UnityEngine.Playables.PlayableDirector::GetPlayableAssetInternal()
extern "C" IL2CPP_METHOD_ATTR ScriptableObject_t2528358522 * PlayableDirector_GetPlayableAssetInternal_m2157181493 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableGraph UnityEngine.Playables.PlayableDirector::GetGraphHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableGraph_t3515989261  PlayableDirector_GetGraphHandle_m24850652 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::GetGraphHandle_Injected(UnityEngine.Playables.PlayableGraph&)
extern "C" IL2CPP_METHOD_ATTR void PlayableDirector_GetGraphHandle_Injected_m2307874625 (PlayableDirector_t508516997 * __this, PlayableGraph_t3515989261 * ___ret0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.ScriptableObject UnityEngine.Playables.PlayableDirector::GetPlayableAssetInternal()
extern "C" IL2CPP_METHOD_ATTR ScriptableObject_t2528358522 * PlayableDirector_GetPlayableAssetInternal_m2157181493 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	typedef ScriptableObject_t2528358522 * (*PlayableDirector_GetPlayableAssetInternal_m2157181493_ftn) (PlayableDirector_t508516997 *);
	static PlayableDirector_GetPlayableAssetInternal_m2157181493_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_GetPlayableAssetInternal_m2157181493_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::GetPlayableAssetInternal()");
	ScriptableObject_t2528358522 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::GetReferenceValue(UnityEngine.PropertyName,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * PlayableDirector_GetReferenceValue_m2151945686 (PlayableDirector_t508516997 * __this, PropertyName_t3749835189  ___id0, bool* ___idValid1, const RuntimeMethod* method)
{
	Object_t631007953 * V_0 = NULL;
	{
		bool* L_0 = ___idValid1;
		Object_t631007953 * L_1 = PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1447819544(NULL /*static, unused*/, __this, (&___id0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Object_t631007953 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_GetReferenceValue(UnityEngine.Playables.PlayableDirector,UnityEngine.PropertyName&,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1447819544 (RuntimeObject * __this /* static, unused */, PlayableDirector_t508516997 * ___self0, PropertyName_t3749835189 * ___id1, bool* ___idValid2, const RuntimeMethod* method)
{
	typedef Object_t631007953 * (*PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1447819544_ftn) (PlayableDirector_t508516997 *, PropertyName_t3749835189 *, bool*);
	static PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1447819544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_INTERNAL_CALL_GetReferenceValue_m1447819544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::INTERNAL_CALL_GetReferenceValue(UnityEngine.Playables.PlayableDirector,UnityEngine.PropertyName&,System.Boolean&)");
	Object_t631007953 * retVal = _il2cpp_icall_func(___self0, ___id1, ___idValid2);
	return retVal;
}
// UnityEngine.Object UnityEngine.Playables.PlayableDirector::GetGenericBinding(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * PlayableDirector_GetGenericBinding_m371850596 (PlayableDirector_t508516997 * __this, Object_t631007953 * ___key0, const RuntimeMethod* method)
{
	typedef Object_t631007953 * (*PlayableDirector_GetGenericBinding_m371850596_ftn) (PlayableDirector_t508516997 *, Object_t631007953 *);
	static PlayableDirector_GetGenericBinding_m371850596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_GetGenericBinding_m371850596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::GetGenericBinding(UnityEngine.Object)");
	Object_t631007953 * retVal = _il2cpp_icall_func(__this, ___key0);
	return retVal;
}
// UnityEngine.Playables.DirectorWrapMode UnityEngine.Playables.PlayableDirector::get_extrapolationMode()
extern "C" IL2CPP_METHOD_ATTR int32_t PlayableDirector_get_extrapolationMode_m2720668466 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayableDirector_GetWrapMode_m3827033998(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.PlayableAsset UnityEngine.Playables.PlayableDirector::get_playableAsset()
extern "C" IL2CPP_METHOD_ATTR PlayableAsset_t3219022681 * PlayableDirector_get_playableAsset_m3386433828 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableDirector_get_playableAsset_m3386433828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableAsset_t3219022681 * V_0 = NULL;
	{
		ScriptableObject_t2528358522 * L_0 = PlayableDirector_GetPlayableAssetInternal_m2157181493(__this, /*hidden argument*/NULL);
		V_0 = ((PlayableAsset_t3219022681 *)IsInstClass((RuntimeObject*)L_0, PlayableAsset_t3219022681_il2cpp_TypeInfo_var));
		goto IL_0012;
	}

IL_0012:
	{
		PlayableAsset_t3219022681 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.PlayableGraph UnityEngine.Playables.PlayableDirector::get_playableGraph()
extern "C" IL2CPP_METHOD_ATTR PlayableGraph_t3515989261  PlayableDirector_get_playableGraph_m1849498917 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	PlayableGraph_t3515989261  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableGraph_t3515989261  L_0 = PlayableDirector_GetGraphHandle_m24850652(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableGraph_t3515989261  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Playables.PlayableDirector::set_time(System.Double)
extern "C" IL2CPP_METHOD_ATTR void PlayableDirector_set_time_m344924007 (PlayableDirector_t508516997 * __this, double ___value0, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_set_time_m344924007_ftn) (PlayableDirector_t508516997 *, double);
	static PlayableDirector_set_time_m344924007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_set_time_m344924007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::set_time(System.Double)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Double UnityEngine.Playables.PlayableDirector::get_time()
extern "C" IL2CPP_METHOD_ATTR double PlayableDirector_get_time_m3083653627 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	typedef double (*PlayableDirector_get_time_m3083653627_ftn) (PlayableDirector_t508516997 *);
	static PlayableDirector_get_time_m3083653627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_get_time_m3083653627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::get_time()");
	double retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Playables.PlayableDirector::Evaluate()
extern "C" IL2CPP_METHOD_ATTR void PlayableDirector_Evaluate_m781969411 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Evaluate_m781969411_ftn) (PlayableDirector_t508516997 *);
	static PlayableDirector_Evaluate_m781969411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Evaluate_m781969411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Evaluate()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Playables.PlayableDirector::Play()
extern "C" IL2CPP_METHOD_ATTR void PlayableDirector_Play_m3047544225 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Play_m3047544225_ftn) (PlayableDirector_t508516997 *);
	static PlayableDirector_Play_m3047544225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Play_m3047544225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Play()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Playables.PlayableDirector::Pause()
extern "C" IL2CPP_METHOD_ATTR void PlayableDirector_Pause_m3831128627 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_Pause_m3831128627_ftn) (PlayableDirector_t508516997 *);
	static PlayableDirector_Pause_m3831128627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_Pause_m3831128627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::Pause()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Playables.DirectorWrapMode UnityEngine.Playables.PlayableDirector::GetWrapMode()
extern "C" IL2CPP_METHOD_ATTR int32_t PlayableDirector_GetWrapMode_m3827033998 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*PlayableDirector_GetWrapMode_m3827033998_ftn) (PlayableDirector_t508516997 *);
	static PlayableDirector_GetWrapMode_m3827033998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_GetWrapMode_m3827033998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::GetWrapMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Playables.PlayableGraph UnityEngine.Playables.PlayableDirector::GetGraphHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableGraph_t3515989261  PlayableDirector_GetGraphHandle_m24850652 (PlayableDirector_t508516997 * __this, const RuntimeMethod* method)
{
	PlayableGraph_t3515989261  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableDirector_GetGraphHandle_Injected_m2307874625(__this, (&V_0), /*hidden argument*/NULL);
		PlayableGraph_t3515989261  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Playables.PlayableDirector::GetGraphHandle_Injected(UnityEngine.Playables.PlayableGraph&)
extern "C" IL2CPP_METHOD_ATTR void PlayableDirector_GetGraphHandle_Injected_m2307874625 (PlayableDirector_t508516997 * __this, PlayableGraph_t3515989261 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*PlayableDirector_GetGraphHandle_Injected_m2307874625_ftn) (PlayableDirector_t508516997 *, PlayableGraph_t3515989261 *);
	static PlayableDirector_GetGraphHandle_Injected_m2307874625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableDirector_GetGraphHandle_Injected_m2307874625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableDirector::GetGraphHandle_Injected(UnityEngine.Playables.PlayableGraph&)");
	_il2cpp_icall_func(__this, ___ret0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
