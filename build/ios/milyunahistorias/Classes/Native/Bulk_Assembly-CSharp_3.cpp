﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// NAudio.Wave.WZT.WaveFormatExtraData
struct WaveFormatExtraData_t1489494699;
// NAudio.Wave.WZT.WaveFormat
struct WaveFormat_t2842237185;
// System.IO.BinaryReader
struct BinaryReader_t2428077293;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.IO.BinaryWriter
struct BinaryWriter_t3992595042;
// NAudio.Wave.WZT.WaveStream
struct WaveStream_t91297746;
// System.IO.Stream
struct Stream_t1273022909;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.String
struct String_t;
// PlayerData
struct PlayerData_t220878115;
// Quest
struct Quest_t3696879532;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// QuestManager
struct QuestManager_t588401851;
// CharacterBehavior
struct CharacterBehavior_t2392107484;
// SceneChanger
struct SceneChanger_t1033871796;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// QuestData
struct QuestData_t1216399150;
// QuestDataOld
struct QuestDataOld_t372650824;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Object
struct Object_t631007953;
// GameController
struct GameController_t2330501625;
// QuestScene
struct QuestScene_t3272400377;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// AudioRecordManager
struct AudioRecordManager_t3166594816;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// QuestTrigger
struct QuestTrigger_t3125687900;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// QuestTrigger/<DelayStartQuest>c__Iterator0
struct U3CDelayStartQuestU3Ec__Iterator0_t39561174;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// ResultQuestScene
struct ResultQuestScene_t315437199;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.IO.FileStream
struct FileStream_t4292183065;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t377119663;
// System.Text.Encoding
struct Encoding_t1523322056;
// DataController
struct DataController_t353634109;
// SettingsController
struct SettingsController_t1943457452;
// UIDisplayController
struct UIDisplayController_t1173465452;
// System.Action
struct Action_t1264377477;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UIPuzzleController
struct UIPuzzleController_t2836468369;
// UIPuzzleController/PuzzleControllerEventHandler
struct PuzzleControllerEventHandler_t963335703;
// System.Delegate
struct Delegate_t1188392813;
// UIPuzzleToken/PuzzleTokenEventHandler
struct PuzzleTokenEventHandler_t3807414068;
// UIPuzzleInsert/PuzzleInsertEventHandler
struct PuzzleInsertEventHandler_t3824397729;
// UIPuzzleController/<_StartLate>c__Iterator0
struct U3C_StartLateU3Ec__Iterator0_t3966377835;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<EPuzzleToken>
struct List_1_t3937307605;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.Collections.Generic.List`1<UIPuzzleToken>
struct List_1_t3134180627;
// UIPuzzleToken
struct UIPuzzleToken_t1662105885;
// UIPuzzleInsert
struct UIPuzzleInsert_t594790890;
// UIPuzzleController/<DelayCompleteQuest>c__Iterator1
struct U3CDelayCompleteQuestU3Ec__Iterator1_t479202429;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// Util.UtilThread
struct UtilThread_t4012287602;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2285235057;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// UIPuzzleToken[]
struct UIPuzzleTokenU5BU5D_t3919558096;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// EPuzzleToken[]
struct EPuzzleTokenU5BU5D_t1398254022;
// System.Text.Encoder
struct Encoder_t2198218980;
// System.Text.Decoder
struct Decoder_t2204182725;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Void
struct Void_t1185182177;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_t2755812594;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// GameData
struct GameData_t415813024;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// Quest[]
struct QuestU5BU5D_t1937508389;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.Dictionary`2<UnityEngine.AudioType,EAudioFileExtension>
struct Dictionary_2_t4114977226;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t2743564464;
// NAudio.Example.MP3EncoderWrapper
struct MP3EncoderWrapper_t279410081;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2297175928;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t1438173104;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t4040729994;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t2924027637;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3520241082;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t3270282352;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;

extern RuntimeClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern const uint32_t WaveFormatExtraData_t1489494699_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t WaveFormatExtraData_t1489494699_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t WaveFormatExtraData__ctor_m1274564551_MetadataUsageId;
extern const uint32_t WaveFormatExtraData__ctor_m92823382_MetadataUsageId;
extern RuntimeClass* Stream_t1273022909_il2cpp_TypeInfo_var;
extern const uint32_t WaveStream__ctor_m3424999445_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* WaveStream_SetLength_m903629300_RuntimeMethod_var;
extern String_t* _stringLiteral2192099496;
extern const uint32_t WaveStream_SetLength_m903629300_MetadataUsageId;
extern const RuntimeMethod* WaveStream_Write_m2960848016_RuntimeMethod_var;
extern String_t* _stringLiteral3455142558;
extern const uint32_t WaveStream_Write_m2960848016_MetadataUsageId;
extern RuntimeClass* TimeSpan_t881159249_il2cpp_TypeInfo_var;
extern const uint32_t WaveStream_get_CurrentTime_m1063384241_MetadataUsageId;
extern const uint32_t WaveStream_get_TotalTime_m2406271272_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var;
extern const uint32_t Quest__ctor_m430137165_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisCharacterBehavior_t2392107484_m3992520573_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497_RuntimeMethod_var;
extern String_t* _stringLiteral476364545;
extern String_t* _stringLiteral2580790064;
extern String_t* _stringLiteral1619350777;
extern String_t* _stringLiteral2129693546;
extern String_t* _stringLiteral1889752399;
extern String_t* _stringLiteral3267731041;
extern String_t* _stringLiteral2011406716;
extern const uint32_t Quest_StartQuest_m2018510914_MetadataUsageId;
extern const uint32_t QuestData__ctor_m3020203002_MetadataUsageId;
extern const uint32_t QuestDataOld__ctor_m1542677745_MetadataUsageId;
extern const uint32_t QuestManager__ctor_m3612029997_MetadataUsageId;
extern RuntimeClass* QuestManager_t588401851_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3878544753;
extern const uint32_t QuestManager_Awake_m1471503576_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisGameController_t2330501625_m3040890899_RuntimeMethod_var;
extern const uint32_t QuestManager_SaveData_m3571505125_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisAudioRecordManager_t3166594816_m2959838367_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisButton_t4055032469_m1515138076_RuntimeMethod_var;
extern String_t* _stringLiteral3454842868;
extern String_t* _stringLiteral3452614566;
extern const uint32_t QuestScene_Start_m4218677153_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var;
extern const uint32_t QuestScene_Update_m786752971_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern String_t* _stringLiteral2909207532;
extern String_t* _stringLiteral1853239370;
extern String_t* _stringLiteral2810867248;
extern String_t* _stringLiteral2756285813;
extern const uint32_t QuestScene_selectOption_m209496874_MetadataUsageId;
extern String_t* _stringLiteral1585816881;
extern const uint32_t QuestScene_selectStory_m3040738564_MetadataUsageId;
extern String_t* _stringLiteral2099292225;
extern const uint32_t QuestScene_StartActivity_m3349551200_MetadataUsageId;
extern String_t* _stringLiteral2582580331;
extern const uint32_t QuestScene_FormatTime_m3401522192_MetadataUsageId;
extern const uint32_t QuestScene_CompleteActivity_m3524613363_MetadataUsageId;
extern const uint32_t QuestScene_IncompleteActivity_m1001130087_MetadataUsageId;
extern const uint32_t QuestTrigger_Start_m1906426316_MetadataUsageId;
extern String_t* _stringLiteral2261822918;
extern const uint32_t QuestTrigger_OnTriggerEnter2D_m3562265525_MetadataUsageId;
extern RuntimeClass* U3CDelayStartQuestU3Ec__Iterator0_t39561174_il2cpp_TypeInfo_var;
extern const uint32_t QuestTrigger_DelayStartQuest_m4146236069_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayStartQuestU3Ec__Iterator0_MoveNext_m434194100_MetadataUsageId;
extern const RuntimeMethod* U3CDelayStartQuestU3Ec__Iterator0_Reset_m85328022_RuntimeMethod_var;
extern const uint32_t U3CDelayStartQuestU3Ec__Iterator0_Reset_m85328022_MetadataUsageId;
extern const uint32_t ResultQuestScene_Start_m2660546872_MetadataUsageId;
extern RuntimeClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2398230137;
extern const uint32_t SavWav_Save_m332214922_MetadataUsageId;
extern RuntimeClass* SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2869341516_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m964359627_RuntimeMethod_var;
extern const uint32_t SavWav_TrimSilence_m2368381276_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m718437397_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m4125879936_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveRange_m1420183523_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m571148082_RuntimeMethod_var;
extern String_t* _stringLiteral3240979319;
extern const uint32_t SavWav_TrimSilence_m3302723312_MetadataUsageId;
extern RuntimeClass* FileStream_t4292183065_il2cpp_TypeInfo_var;
extern const uint32_t SavWav_CreateEmpty_m194446383_MetadataUsageId;
extern RuntimeClass* Int16U5BU5D_t3686840178_il2cpp_TypeInfo_var;
extern RuntimeClass* BitConverter_t3118986983_il2cpp_TypeInfo_var;
extern const uint32_t SavWav_ConvertAndWrite_m4186062765_MetadataUsageId;
extern String_t* _stringLiteral1209311930;
extern String_t* _stringLiteral2834979077;
extern String_t* _stringLiteral731316992;
extern String_t* _stringLiteral2037252866;
extern const uint32_t SavWav_WriteHeader_m2208500169_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisDataController_t353634109_m3280844559_RuntimeMethod_var;
extern String_t* _stringLiteral3326552179;
extern const uint32_t SceneChanger_Start_m236998524_MetadataUsageId;
extern const uint32_t SettingsController_RestartData_m2218269148_MetadataUsageId;
extern String_t* _stringLiteral628085470;
extern const uint32_t SystemHelpers_FormatTimeSeconds_m797777047_MetadataUsageId;
extern RuntimeClass* Action_t1264377477_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UIDisplayController_RecordManager_OnStartEncoding_m3377201746_RuntimeMethod_var;
extern const RuntimeMethod* UIDisplayController_RecordManager_OnStopEncoding_m2462141832_RuntimeMethod_var;
extern const uint32_t UIDisplayController_OnEnable_m461706163_MetadataUsageId;
extern const uint32_t UIDisplayController_OnDisable_m2241095903_MetadataUsageId;
extern const uint32_t UIDisplayController_RecordManager_OnStopEncoding_m2462141832_MetadataUsageId;
extern RuntimeClass* UIPuzzleController_t2836468369_il2cpp_TypeInfo_var;
extern RuntimeClass* PuzzleControllerEventHandler_t963335703_il2cpp_TypeInfo_var;
extern const uint32_t UIPuzzleController_add_OnEndPuzzle_m603902049_MetadataUsageId;
extern const uint32_t UIPuzzleController_remove_OnEndPuzzle_m3057330886_MetadataUsageId;
extern RuntimeClass* PuzzleTokenEventHandler_t3807414068_il2cpp_TypeInfo_var;
extern RuntimeClass* PuzzleInsertEventHandler_t3824397729_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UIPuzzleController_UIPuzzleToken_OnDragEvent_m1610412114_RuntimeMethod_var;
extern const RuntimeMethod* UIPuzzleController_UIPuzzleInsert_OnPointerEvent_m249629649_RuntimeMethod_var;
extern const uint32_t UIPuzzleController_OnEnable_m4216670754_MetadataUsageId;
extern const uint32_t UIPuzzleController_OnDisable_m3910927490_MetadataUsageId;
extern const uint32_t UIPuzzleController_Start_m2120222110_MetadataUsageId;
extern RuntimeClass* U3C_StartLateU3Ec__Iterator0_t3966377835_il2cpp_TypeInfo_var;
extern const uint32_t UIPuzzleController__StartLate_m1048854624_MetadataUsageId;
extern const uint32_t UIPuzzleController_Update_m541428278_MetadataUsageId;
extern RuntimeClass* List_1_t899420910_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3937307605_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1536473967_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m28597759_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1242626717_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1524640104_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m200663048_RuntimeMethod_var;
extern const RuntimeMethod* SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547_RuntimeMethod_var;
extern const uint32_t UIPuzzleController_ShowIntroPanel_m1546992945_MetadataUsageId;
extern RuntimeClass* List_1_t3134180627_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3799958844_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisUIPuzzleToken_t1662105885_m4197970512_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3373879604_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m219509784_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisUIPuzzleInsert_t594790890_m1640620979_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3452984843_RuntimeMethod_var;
extern const uint32_t UIPuzzleController_ShowPuzzlePanel_m2130928474_MetadataUsageId;
extern String_t* _stringLiteral82719313;
extern const uint32_t UIPuzzleController_UIPuzzleToken_OnDragEvent_m1610412114_MetadataUsageId;
extern RuntimeClass* U3CDelayCompleteQuestU3Ec__Iterator1_t479202429_il2cpp_TypeInfo_var;
extern const uint32_t UIPuzzleController_DelayCompleteQuest_m3765199727_MetadataUsageId;
extern const uint32_t UIPuzzleController__cctor_m2123512877_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m1003748895_RuntimeMethod_var;
extern String_t* _stringLiteral3851960542;
extern const uint32_t U3C_StartLateU3Ec__Iterator0_MoveNext_m1211157992_MetadataUsageId;
extern const RuntimeMethod* U3C_StartLateU3Ec__Iterator0_Reset_m217901147_RuntimeMethod_var;
extern const uint32_t U3C_StartLateU3Ec__Iterator0_Reset_m217901147_MetadataUsageId;
extern String_t* _stringLiteral589998905;
extern const uint32_t U3CDelayCompleteQuestU3Ec__Iterator1_MoveNext_m3485536048_MetadataUsageId;
extern const RuntimeMethod* U3CDelayCompleteQuestU3Ec__Iterator1_Reset_m1169978582_RuntimeMethod_var;
extern const uint32_t U3CDelayCompleteQuestU3Ec__Iterator1_Reset_m1169978582_MetadataUsageId;
extern RuntimeClass* UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var;
extern const uint32_t UIPuzzleInsert_add_OnPointerEvent_m3287251477_MetadataUsageId;
extern const uint32_t UIPuzzleInsert_remove_OnPointerEvent_m414160073_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral452318740;
extern String_t* _stringLiteral2063885597;
extern const uint32_t UIPuzzleInsert_OnPointerEnter_m3558055084_MetadataUsageId;
extern const uint32_t UIPuzzleInsert_OnPointerExit_m3410566094_MetadataUsageId;
extern const uint32_t PuzzleInsertEventHandler_BeginInvoke_m43389714_MetadataUsageId;
extern RuntimeClass* UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var;
extern const uint32_t UIPuzzleToken_add_OnDragEvent_m3807187468_MetadataUsageId;
extern const uint32_t UIPuzzleToken_remove_OnDragEvent_m2242078314_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const uint32_t UIPuzzleToken_InitToken_m3887138451_MetadataUsageId;
extern const uint32_t UIPuzzleToken_Update_m1559844202_MetadataUsageId;
extern const uint32_t UIPuzzleToken_Match_m1213795857_MetadataUsageId;
extern const uint32_t UIPuzzleToken_OnBeginDrag_m1840991787_MetadataUsageId;
extern const uint32_t UIPuzzleToken_OnEndDrag_m2554922906_MetadataUsageId;
extern const uint32_t PuzzleTokenEventHandler_BeginInvoke_m2388511976_MetadataUsageId;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* ColorU5BU5D_t941916413_il2cpp_TypeInfo_var;
extern const uint32_t Tools2D_ResizeTexture_m3175497502_MetadataUsageId;
struct Exception_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;

struct ByteU5BU5D_t4116647657;
struct StringU5BU5D_t1281789340;
struct BooleanU5BU5D_t2897418192;
struct ObjectU5BU5D_t2843939325;
struct GameObjectU5BU5D_t3328599146;
struct ButtonU5BU5D_t2297175928;
struct QuestU5BU5D_t1937508389;
struct TextU5BU5D_t422084607;
struct SingleU5BU5D_t1444911251;
struct Int16U5BU5D_t3686840178;
struct DelegateU5BU5D_t1703627840;
struct ColorU5BU5D_t941916413;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef U3C_STARTLATEU3EC__ITERATOR0_T3966377835_H
#define U3C_STARTLATEU3EC__ITERATOR0_T3966377835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController/<_StartLate>c__Iterator0
struct  U3C_StartLateU3Ec__Iterator0_t3966377835  : public RuntimeObject
{
public:
	// System.Int32 UIPuzzleController/<_StartLate>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Int32 UIPuzzleController/<_StartLate>c__Iterator0::<j>__2
	int32_t ___U3CjU3E__2_1;
	// UIPuzzleController UIPuzzleController/<_StartLate>c__Iterator0::$this
	UIPuzzleController_t2836468369 * ___U24this_2;
	// System.Object UIPuzzleController/<_StartLate>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UIPuzzleController/<_StartLate>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UIPuzzleController/<_StartLate>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E__2_1() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U3CjU3E__2_1)); }
	inline int32_t get_U3CjU3E__2_1() const { return ___U3CjU3E__2_1; }
	inline int32_t* get_address_of_U3CjU3E__2_1() { return &___U3CjU3E__2_1; }
	inline void set_U3CjU3E__2_1(int32_t value)
	{
		___U3CjU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24this_2)); }
	inline UIPuzzleController_t2836468369 * get_U24this_2() const { return ___U24this_2; }
	inline UIPuzzleController_t2836468369 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UIPuzzleController_t2836468369 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3C_StartLateU3Ec__Iterator0_t3966377835, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_STARTLATEU3EC__ITERATOR0_T3966377835_H
#ifndef SYSTEMHELPERS_T2322995976_H
#define SYSTEMHELPERS_T2322995976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SystemHelpers
struct  SystemHelpers_t2322995976  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMHELPERS_T2322995976_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2285235057 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1188251036 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t3123823036 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___dataItem_10)); }
	inline CodePageDataItem_t2285235057 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2285235057 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2285235057 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoderFallback_13)); }
	inline EncoderFallback_t1188251036 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t1188251036 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoderFallback_14)); }
	inline DecoderFallback_t3123823036 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t3123823036 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t1523322056 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t1523322056 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t1523322056 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t1853889766 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t1523322056 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t1523322056 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t1523322056 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t1523322056 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t1523322056 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t1523322056 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t1523322056 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t1523322056 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t1523322056 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t1523322056 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t1523322056 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t1523322056 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t1523322056 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t1523322056 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_8)); }
	inline Hashtable_t1853889766 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t1853889766 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t1853889766 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef LIST_1_T2869341516_H
#define LIST_1_T2869341516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Single>
struct  List_1_t2869341516  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SingleU5BU5D_t1444911251* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____items_1)); }
	inline SingleU5BU5D_t1444911251* get__items_1() const { return ____items_1; }
	inline SingleU5BU5D_t1444911251** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SingleU5BU5D_t1444911251* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2869341516_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SingleU5BU5D_t1444911251* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2869341516_StaticFields, ____emptyArray_5)); }
	inline SingleU5BU5D_t1444911251* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SingleU5BU5D_t1444911251** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SingleU5BU5D_t1444911251* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2869341516_H
#ifndef SAVWAV_T2455245258_H
#define SAVWAV_T2455245258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavWav
struct  SavWav_t2455245258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVWAV_T2455245258_H
#ifndef U3CDELAYSTARTQUESTU3EC__ITERATOR0_T39561174_H
#define U3CDELAYSTARTQUESTU3EC__ITERATOR0_T39561174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestTrigger/<DelayStartQuest>c__Iterator0
struct  U3CDelayStartQuestU3Ec__Iterator0_t39561174  : public RuntimeObject
{
public:
	// QuestTrigger QuestTrigger/<DelayStartQuest>c__Iterator0::$this
	QuestTrigger_t3125687900 * ___U24this_0;
	// System.Object QuestTrigger/<DelayStartQuest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean QuestTrigger/<DelayStartQuest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 QuestTrigger/<DelayStartQuest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24this_0)); }
	inline QuestTrigger_t3125687900 * get_U24this_0() const { return ___U24this_0; }
	inline QuestTrigger_t3125687900 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(QuestTrigger_t3125687900 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayStartQuestU3Ec__Iterator0_t39561174, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYSTARTQUESTU3EC__ITERATOR0_T39561174_H
#ifndef QUESTDATAOLD_T372650824_H
#define QUESTDATAOLD_T372650824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestDataOld
struct  QuestDataOld_t372650824  : public RuntimeObject
{
public:
	// System.Int32 QuestDataOld::questID
	int32_t ___questID_0;
	// System.Boolean QuestDataOld::isActivity
	bool ___isActivity_1;
	// System.Boolean QuestDataOld::activityCompleted
	bool ___activityCompleted_2;
	// System.String[] QuestDataOld::textActivity
	StringU5BU5D_t1281789340* ___textActivity_3;
	// System.Boolean QuestDataOld::isSelectMultiple
	bool ___isSelectMultiple_4;
	// System.Int32 QuestDataOld::numberOptions
	int32_t ___numberOptions_5;
	// System.String[] QuestDataOld::options
	StringU5BU5D_t1281789340* ___options_6;
	// System.Int32 QuestDataOld::rigthOption
	int32_t ___rigthOption_7;
	// System.Boolean[] QuestDataOld::optionsSelected
	BooleanU5BU5D_t2897418192* ___optionsSelected_8;
	// System.Boolean QuestDataOld::isStory
	bool ___isStory_9;
	// System.Int32 QuestDataOld::numberOfStories
	int32_t ___numberOfStories_10;
	// System.String[] QuestDataOld::stories
	StringU5BU5D_t1281789340* ___stories_11;
	// System.Boolean[] QuestDataOld::storiesCompleted
	BooleanU5BU5D_t2897418192* ___storiesCompleted_12;
	// System.String QuestDataOld::sceneName
	String_t* ___sceneName_13;
	// System.String QuestDataOld::startText
	String_t* ___startText_14;
	// System.String QuestDataOld::completedText
	String_t* ___completedText_15;
	// System.String QuestDataOld::wrongText
	String_t* ___wrongText_16;

public:
	inline static int32_t get_offset_of_questID_0() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___questID_0)); }
	inline int32_t get_questID_0() const { return ___questID_0; }
	inline int32_t* get_address_of_questID_0() { return &___questID_0; }
	inline void set_questID_0(int32_t value)
	{
		___questID_0 = value;
	}

	inline static int32_t get_offset_of_isActivity_1() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___isActivity_1)); }
	inline bool get_isActivity_1() const { return ___isActivity_1; }
	inline bool* get_address_of_isActivity_1() { return &___isActivity_1; }
	inline void set_isActivity_1(bool value)
	{
		___isActivity_1 = value;
	}

	inline static int32_t get_offset_of_activityCompleted_2() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___activityCompleted_2)); }
	inline bool get_activityCompleted_2() const { return ___activityCompleted_2; }
	inline bool* get_address_of_activityCompleted_2() { return &___activityCompleted_2; }
	inline void set_activityCompleted_2(bool value)
	{
		___activityCompleted_2 = value;
	}

	inline static int32_t get_offset_of_textActivity_3() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___textActivity_3)); }
	inline StringU5BU5D_t1281789340* get_textActivity_3() const { return ___textActivity_3; }
	inline StringU5BU5D_t1281789340** get_address_of_textActivity_3() { return &___textActivity_3; }
	inline void set_textActivity_3(StringU5BU5D_t1281789340* value)
	{
		___textActivity_3 = value;
		Il2CppCodeGenWriteBarrier((&___textActivity_3), value);
	}

	inline static int32_t get_offset_of_isSelectMultiple_4() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___isSelectMultiple_4)); }
	inline bool get_isSelectMultiple_4() const { return ___isSelectMultiple_4; }
	inline bool* get_address_of_isSelectMultiple_4() { return &___isSelectMultiple_4; }
	inline void set_isSelectMultiple_4(bool value)
	{
		___isSelectMultiple_4 = value;
	}

	inline static int32_t get_offset_of_numberOptions_5() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___numberOptions_5)); }
	inline int32_t get_numberOptions_5() const { return ___numberOptions_5; }
	inline int32_t* get_address_of_numberOptions_5() { return &___numberOptions_5; }
	inline void set_numberOptions_5(int32_t value)
	{
		___numberOptions_5 = value;
	}

	inline static int32_t get_offset_of_options_6() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___options_6)); }
	inline StringU5BU5D_t1281789340* get_options_6() const { return ___options_6; }
	inline StringU5BU5D_t1281789340** get_address_of_options_6() { return &___options_6; }
	inline void set_options_6(StringU5BU5D_t1281789340* value)
	{
		___options_6 = value;
		Il2CppCodeGenWriteBarrier((&___options_6), value);
	}

	inline static int32_t get_offset_of_rigthOption_7() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___rigthOption_7)); }
	inline int32_t get_rigthOption_7() const { return ___rigthOption_7; }
	inline int32_t* get_address_of_rigthOption_7() { return &___rigthOption_7; }
	inline void set_rigthOption_7(int32_t value)
	{
		___rigthOption_7 = value;
	}

	inline static int32_t get_offset_of_optionsSelected_8() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___optionsSelected_8)); }
	inline BooleanU5BU5D_t2897418192* get_optionsSelected_8() const { return ___optionsSelected_8; }
	inline BooleanU5BU5D_t2897418192** get_address_of_optionsSelected_8() { return &___optionsSelected_8; }
	inline void set_optionsSelected_8(BooleanU5BU5D_t2897418192* value)
	{
		___optionsSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___optionsSelected_8), value);
	}

	inline static int32_t get_offset_of_isStory_9() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___isStory_9)); }
	inline bool get_isStory_9() const { return ___isStory_9; }
	inline bool* get_address_of_isStory_9() { return &___isStory_9; }
	inline void set_isStory_9(bool value)
	{
		___isStory_9 = value;
	}

	inline static int32_t get_offset_of_numberOfStories_10() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___numberOfStories_10)); }
	inline int32_t get_numberOfStories_10() const { return ___numberOfStories_10; }
	inline int32_t* get_address_of_numberOfStories_10() { return &___numberOfStories_10; }
	inline void set_numberOfStories_10(int32_t value)
	{
		___numberOfStories_10 = value;
	}

	inline static int32_t get_offset_of_stories_11() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___stories_11)); }
	inline StringU5BU5D_t1281789340* get_stories_11() const { return ___stories_11; }
	inline StringU5BU5D_t1281789340** get_address_of_stories_11() { return &___stories_11; }
	inline void set_stories_11(StringU5BU5D_t1281789340* value)
	{
		___stories_11 = value;
		Il2CppCodeGenWriteBarrier((&___stories_11), value);
	}

	inline static int32_t get_offset_of_storiesCompleted_12() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___storiesCompleted_12)); }
	inline BooleanU5BU5D_t2897418192* get_storiesCompleted_12() const { return ___storiesCompleted_12; }
	inline BooleanU5BU5D_t2897418192** get_address_of_storiesCompleted_12() { return &___storiesCompleted_12; }
	inline void set_storiesCompleted_12(BooleanU5BU5D_t2897418192* value)
	{
		___storiesCompleted_12 = value;
		Il2CppCodeGenWriteBarrier((&___storiesCompleted_12), value);
	}

	inline static int32_t get_offset_of_sceneName_13() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___sceneName_13)); }
	inline String_t* get_sceneName_13() const { return ___sceneName_13; }
	inline String_t** get_address_of_sceneName_13() { return &___sceneName_13; }
	inline void set_sceneName_13(String_t* value)
	{
		___sceneName_13 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_13), value);
	}

	inline static int32_t get_offset_of_startText_14() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___startText_14)); }
	inline String_t* get_startText_14() const { return ___startText_14; }
	inline String_t** get_address_of_startText_14() { return &___startText_14; }
	inline void set_startText_14(String_t* value)
	{
		___startText_14 = value;
		Il2CppCodeGenWriteBarrier((&___startText_14), value);
	}

	inline static int32_t get_offset_of_completedText_15() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___completedText_15)); }
	inline String_t* get_completedText_15() const { return ___completedText_15; }
	inline String_t** get_address_of_completedText_15() { return &___completedText_15; }
	inline void set_completedText_15(String_t* value)
	{
		___completedText_15 = value;
		Il2CppCodeGenWriteBarrier((&___completedText_15), value);
	}

	inline static int32_t get_offset_of_wrongText_16() { return static_cast<int32_t>(offsetof(QuestDataOld_t372650824, ___wrongText_16)); }
	inline String_t* get_wrongText_16() const { return ___wrongText_16; }
	inline String_t** get_address_of_wrongText_16() { return &___wrongText_16; }
	inline void set_wrongText_16(String_t* value)
	{
		___wrongText_16 = value;
		Il2CppCodeGenWriteBarrier((&___wrongText_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTDATAOLD_T372650824_H
#ifndef QUESTDATA_T1216399150_H
#define QUESTDATA_T1216399150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestData
struct  QuestData_t1216399150  : public RuntimeObject
{
public:
	// System.Int32 QuestData::optionSelected
	int32_t ___optionSelected_0;
	// System.Boolean[] QuestData::storiesCompleted
	BooleanU5BU5D_t2897418192* ___storiesCompleted_1;

public:
	inline static int32_t get_offset_of_optionSelected_0() { return static_cast<int32_t>(offsetof(QuestData_t1216399150, ___optionSelected_0)); }
	inline int32_t get_optionSelected_0() const { return ___optionSelected_0; }
	inline int32_t* get_address_of_optionSelected_0() { return &___optionSelected_0; }
	inline void set_optionSelected_0(int32_t value)
	{
		___optionSelected_0 = value;
	}

	inline static int32_t get_offset_of_storiesCompleted_1() { return static_cast<int32_t>(offsetof(QuestData_t1216399150, ___storiesCompleted_1)); }
	inline BooleanU5BU5D_t2897418192* get_storiesCompleted_1() const { return ___storiesCompleted_1; }
	inline BooleanU5BU5D_t2897418192** get_address_of_storiesCompleted_1() { return &___storiesCompleted_1; }
	inline void set_storiesCompleted_1(BooleanU5BU5D_t2897418192* value)
	{
		___storiesCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___storiesCompleted_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTDATA_T1216399150_H
#ifndef LIST_1_T899420910_H
#define LIST_1_T899420910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t899420910  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1718750761* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____items_1)); }
	inline Vector3U5BU5D_t1718750761* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1718750761* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t899420910_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t1718750761* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t899420910_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t1718750761* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t1718750761* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T899420910_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	ServerIdentity_t2342208608 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	ServerIdentity_t2342208608 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TOOLS2D_T1149803378_H
#define TOOLS2D_T1149803378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util.Tools2D
struct  Tools2D_t1149803378  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLS2D_T1149803378_H
#ifndef U3CDELAYCOMPLETEQUESTU3EC__ITERATOR1_T479202429_H
#define U3CDELAYCOMPLETEQUESTU3EC__ITERATOR1_T479202429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController/<DelayCompleteQuest>c__Iterator1
struct  U3CDelayCompleteQuestU3Ec__Iterator1_t479202429  : public RuntimeObject
{
public:
	// UIPuzzleController UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$this
	UIPuzzleController_t2836468369 * ___U24this_0;
	// System.Object UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UIPuzzleController/<DelayCompleteQuest>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24this_0)); }
	inline UIPuzzleController_t2836468369 * get_U24this_0() const { return ___U24this_0; }
	inline UIPuzzleController_t2836468369 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UIPuzzleController_t2836468369 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYCOMPLETEQUESTU3EC__ITERATOR1_T479202429_H
#ifndef LIST_1_T3134180627_H
#define LIST_1_T3134180627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UIPuzzleToken>
struct  List_1_t3134180627  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIPuzzleTokenU5BU5D_t3919558096* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3134180627, ____items_1)); }
	inline UIPuzzleTokenU5BU5D_t3919558096* get__items_1() const { return ____items_1; }
	inline UIPuzzleTokenU5BU5D_t3919558096** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIPuzzleTokenU5BU5D_t3919558096* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3134180627, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3134180627, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3134180627, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3134180627_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UIPuzzleTokenU5BU5D_t3919558096* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3134180627_StaticFields, ____emptyArray_5)); }
	inline UIPuzzleTokenU5BU5D_t3919558096* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UIPuzzleTokenU5BU5D_t3919558096** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UIPuzzleTokenU5BU5D_t3919558096* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3134180627_H
#ifndef LIST_1_T4142344393_H
#define LIST_1_T4142344393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct  List_1_t4142344393  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ImageU5BU5D_t2439009922* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4142344393, ____items_1)); }
	inline ImageU5BU5D_t2439009922* get__items_1() const { return ____items_1; }
	inline ImageU5BU5D_t2439009922** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ImageU5BU5D_t2439009922* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4142344393, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4142344393, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4142344393, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4142344393_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ImageU5BU5D_t2439009922* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4142344393_StaticFields, ____emptyArray_5)); }
	inline ImageU5BU5D_t2439009922* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ImageU5BU5D_t2439009922** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ImageU5BU5D_t2439009922* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4142344393_H
#ifndef LIST_1_T3937307605_H
#define LIST_1_T3937307605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<EPuzzleToken>
struct  List_1_t3937307605  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	EPuzzleTokenU5BU5D_t1398254022* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3937307605, ____items_1)); }
	inline EPuzzleTokenU5BU5D_t1398254022* get__items_1() const { return ____items_1; }
	inline EPuzzleTokenU5BU5D_t1398254022** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(EPuzzleTokenU5BU5D_t1398254022* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3937307605, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3937307605, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3937307605, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3937307605_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	EPuzzleTokenU5BU5D_t1398254022* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3937307605_StaticFields, ____emptyArray_5)); }
	inline EPuzzleTokenU5BU5D_t1398254022* get__emptyArray_5() const { return ____emptyArray_5; }
	inline EPuzzleTokenU5BU5D_t1398254022** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(EPuzzleTokenU5BU5D_t1398254022* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3937307605_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef BINARYWRITER_T3992595042_H
#define BINARYWRITER_T3992595042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.BinaryWriter
struct  BinaryWriter_t3992595042  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryWriter::OutStream
	Stream_t1273022909 * ___OutStream_1;
	// System.Byte[] System.IO.BinaryWriter::_buffer
	ByteU5BU5D_t4116647657* ____buffer_2;
	// System.Text.Encoding System.IO.BinaryWriter::_encoding
	Encoding_t1523322056 * ____encoding_3;
	// System.Text.Encoder System.IO.BinaryWriter::_encoder
	Encoder_t2198218980 * ____encoder_4;
	// System.Boolean System.IO.BinaryWriter::_leaveOpen
	bool ____leaveOpen_5;
	// System.Byte[] System.IO.BinaryWriter::_largeByteBuffer
	ByteU5BU5D_t4116647657* ____largeByteBuffer_6;
	// System.Int32 System.IO.BinaryWriter::_maxChars
	int32_t ____maxChars_7;

public:
	inline static int32_t get_offset_of_OutStream_1() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ___OutStream_1)); }
	inline Stream_t1273022909 * get_OutStream_1() const { return ___OutStream_1; }
	inline Stream_t1273022909 ** get_address_of_OutStream_1() { return &___OutStream_1; }
	inline void set_OutStream_1(Stream_t1273022909 * value)
	{
		___OutStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___OutStream_1), value);
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ____buffer_2)); }
	inline ByteU5BU5D_t4116647657* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t4116647657* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_2), value);
	}

	inline static int32_t get_offset_of__encoding_3() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ____encoding_3)); }
	inline Encoding_t1523322056 * get__encoding_3() const { return ____encoding_3; }
	inline Encoding_t1523322056 ** get_address_of__encoding_3() { return &____encoding_3; }
	inline void set__encoding_3(Encoding_t1523322056 * value)
	{
		____encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_3), value);
	}

	inline static int32_t get_offset_of__encoder_4() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ____encoder_4)); }
	inline Encoder_t2198218980 * get__encoder_4() const { return ____encoder_4; }
	inline Encoder_t2198218980 ** get_address_of__encoder_4() { return &____encoder_4; }
	inline void set__encoder_4(Encoder_t2198218980 * value)
	{
		____encoder_4 = value;
		Il2CppCodeGenWriteBarrier((&____encoder_4), value);
	}

	inline static int32_t get_offset_of__leaveOpen_5() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ____leaveOpen_5)); }
	inline bool get__leaveOpen_5() const { return ____leaveOpen_5; }
	inline bool* get_address_of__leaveOpen_5() { return &____leaveOpen_5; }
	inline void set__leaveOpen_5(bool value)
	{
		____leaveOpen_5 = value;
	}

	inline static int32_t get_offset_of__largeByteBuffer_6() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ____largeByteBuffer_6)); }
	inline ByteU5BU5D_t4116647657* get__largeByteBuffer_6() const { return ____largeByteBuffer_6; }
	inline ByteU5BU5D_t4116647657** get_address_of__largeByteBuffer_6() { return &____largeByteBuffer_6; }
	inline void set__largeByteBuffer_6(ByteU5BU5D_t4116647657* value)
	{
		____largeByteBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_6), value);
	}

	inline static int32_t get_offset_of__maxChars_7() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042, ____maxChars_7)); }
	inline int32_t get__maxChars_7() const { return ____maxChars_7; }
	inline int32_t* get_address_of__maxChars_7() { return &____maxChars_7; }
	inline void set__maxChars_7(int32_t value)
	{
		____maxChars_7 = value;
	}
};

struct BinaryWriter_t3992595042_StaticFields
{
public:
	// System.IO.BinaryWriter System.IO.BinaryWriter::Null
	BinaryWriter_t3992595042 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(BinaryWriter_t3992595042_StaticFields, ___Null_0)); }
	inline BinaryWriter_t3992595042 * get_Null_0() const { return ___Null_0; }
	inline BinaryWriter_t3992595042 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(BinaryWriter_t3992595042 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYWRITER_T3992595042_H
#ifndef BINARYREADER_T2428077293_H
#define BINARYREADER_T2428077293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.BinaryReader
struct  BinaryReader_t2428077293  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_t1273022909 * ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_t4116647657* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_t2204182725 * ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_t4116647657* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t3528271667* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t3528271667* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;

public:
	inline static int32_t get_offset_of_m_stream_0() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_stream_0)); }
	inline Stream_t1273022909 * get_m_stream_0() const { return ___m_stream_0; }
	inline Stream_t1273022909 ** get_address_of_m_stream_0() { return &___m_stream_0; }
	inline void set_m_stream_0(Stream_t1273022909 * value)
	{
		___m_stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_0), value);
	}

	inline static int32_t get_offset_of_m_buffer_1() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_m_buffer_1() const { return ___m_buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_buffer_1() { return &___m_buffer_1; }
	inline void set_m_buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___m_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_buffer_1), value);
	}

	inline static int32_t get_offset_of_m_decoder_2() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_decoder_2)); }
	inline Decoder_t2204182725 * get_m_decoder_2() const { return ___m_decoder_2; }
	inline Decoder_t2204182725 ** get_address_of_m_decoder_2() { return &___m_decoder_2; }
	inline void set_m_decoder_2(Decoder_t2204182725 * value)
	{
		___m_decoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_decoder_2), value);
	}

	inline static int32_t get_offset_of_m_charBytes_3() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_charBytes_3)); }
	inline ByteU5BU5D_t4116647657* get_m_charBytes_3() const { return ___m_charBytes_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_charBytes_3() { return &___m_charBytes_3; }
	inline void set_m_charBytes_3(ByteU5BU5D_t4116647657* value)
	{
		___m_charBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_charBytes_3), value);
	}

	inline static int32_t get_offset_of_m_singleChar_4() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_singleChar_4)); }
	inline CharU5BU5D_t3528271667* get_m_singleChar_4() const { return ___m_singleChar_4; }
	inline CharU5BU5D_t3528271667** get_address_of_m_singleChar_4() { return &___m_singleChar_4; }
	inline void set_m_singleChar_4(CharU5BU5D_t3528271667* value)
	{
		___m_singleChar_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_singleChar_4), value);
	}

	inline static int32_t get_offset_of_m_charBuffer_5() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_charBuffer_5)); }
	inline CharU5BU5D_t3528271667* get_m_charBuffer_5() const { return ___m_charBuffer_5; }
	inline CharU5BU5D_t3528271667** get_address_of_m_charBuffer_5() { return &___m_charBuffer_5; }
	inline void set_m_charBuffer_5(CharU5BU5D_t3528271667* value)
	{
		___m_charBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_charBuffer_5), value);
	}

	inline static int32_t get_offset_of_m_maxCharsSize_6() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_maxCharsSize_6)); }
	inline int32_t get_m_maxCharsSize_6() const { return ___m_maxCharsSize_6; }
	inline int32_t* get_address_of_m_maxCharsSize_6() { return &___m_maxCharsSize_6; }
	inline void set_m_maxCharsSize_6(int32_t value)
	{
		___m_maxCharsSize_6 = value;
	}

	inline static int32_t get_offset_of_m_2BytesPerChar_7() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_2BytesPerChar_7)); }
	inline bool get_m_2BytesPerChar_7() const { return ___m_2BytesPerChar_7; }
	inline bool* get_address_of_m_2BytesPerChar_7() { return &___m_2BytesPerChar_7; }
	inline void set_m_2BytesPerChar_7(bool value)
	{
		___m_2BytesPerChar_7 = value;
	}

	inline static int32_t get_offset_of_m_isMemoryStream_8() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_isMemoryStream_8)); }
	inline bool get_m_isMemoryStream_8() const { return ___m_isMemoryStream_8; }
	inline bool* get_address_of_m_isMemoryStream_8() { return &___m_isMemoryStream_8; }
	inline void set_m_isMemoryStream_8(bool value)
	{
		___m_isMemoryStream_8 = value;
	}

	inline static int32_t get_offset_of_m_leaveOpen_9() { return static_cast<int32_t>(offsetof(BinaryReader_t2428077293, ___m_leaveOpen_9)); }
	inline bool get_m_leaveOpen_9() const { return ___m_leaveOpen_9; }
	inline bool* get_address_of_m_leaveOpen_9() { return &___m_leaveOpen_9; }
	inline void set_m_leaveOpen_9(bool value)
	{
		___m_leaveOpen_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYREADER_T2428077293_H
#ifndef PLAYERDATA_T220878115_H
#define PLAYERDATA_T220878115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t220878115  : public RuntimeObject
{
public:
	// System.Int32 PlayerData::isFirstTime
	int32_t ___isFirstTime_0;
	// System.Int32 PlayerData::currentLevel
	int32_t ___currentLevel_1;
	// System.Int32 PlayerData::posLevelCharacter
	int32_t ___posLevelCharacter_2;
	// System.String PlayerData::characterName
	String_t* ___characterName_3;
	// System.Single PlayerData::posX
	float ___posX_4;
	// System.Single PlayerData::posY
	float ___posY_5;

public:
	inline static int32_t get_offset_of_isFirstTime_0() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___isFirstTime_0)); }
	inline int32_t get_isFirstTime_0() const { return ___isFirstTime_0; }
	inline int32_t* get_address_of_isFirstTime_0() { return &___isFirstTime_0; }
	inline void set_isFirstTime_0(int32_t value)
	{
		___isFirstTime_0 = value;
	}

	inline static int32_t get_offset_of_currentLevel_1() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___currentLevel_1)); }
	inline int32_t get_currentLevel_1() const { return ___currentLevel_1; }
	inline int32_t* get_address_of_currentLevel_1() { return &___currentLevel_1; }
	inline void set_currentLevel_1(int32_t value)
	{
		___currentLevel_1 = value;
	}

	inline static int32_t get_offset_of_posLevelCharacter_2() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___posLevelCharacter_2)); }
	inline int32_t get_posLevelCharacter_2() const { return ___posLevelCharacter_2; }
	inline int32_t* get_address_of_posLevelCharacter_2() { return &___posLevelCharacter_2; }
	inline void set_posLevelCharacter_2(int32_t value)
	{
		___posLevelCharacter_2 = value;
	}

	inline static int32_t get_offset_of_characterName_3() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___characterName_3)); }
	inline String_t* get_characterName_3() const { return ___characterName_3; }
	inline String_t** get_address_of_characterName_3() { return &___characterName_3; }
	inline void set_characterName_3(String_t* value)
	{
		___characterName_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_3), value);
	}

	inline static int32_t get_offset_of_posX_4() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___posX_4)); }
	inline float get_posX_4() const { return ___posX_4; }
	inline float* get_address_of_posX_4() { return &___posX_4; }
	inline void set_posX_4(float value)
	{
		___posX_4 = value;
	}

	inline static int32_t get_offset_of_posY_5() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___posY_5)); }
	inline float get_posY_5() const { return ___posY_5; }
	inline float* get_address_of_posY_5() { return &___posY_5; }
	inline void set_posY_5(float value)
	{
		___posY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T220878115_H
#ifndef INT16_T2552820387_H
#define INT16_T2552820387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t2552820387 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t2552820387, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T2552820387_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef UINT16_T2177724958_H
#define UINT16_T2177724958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t2177724958 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t2177724958, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T2177724958_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef EAUDIOCHANNEL_T4054551443_H
#define EAUDIOCHANNEL_T4054551443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EAudioChannel
struct  EAudioChannel_t4054551443 
{
public:
	// System.Int32 EAudioChannel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EAudioChannel_t4054551443, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAUDIOCHANNEL_T4054551443_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef AUDIOSPEAKERMODE_T101565544_H
#define AUDIOSPEAKERMODE_T101565544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSpeakerMode
struct  AudioSpeakerMode_t101565544 
{
public:
	// System.Int32 UnityEngine.AudioSpeakerMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioSpeakerMode_t101565544, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSPEAKERMODE_T101565544_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef AUDIOTYPE_T3170162477_H
#define AUDIOTYPE_T3170162477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioType
struct  AudioType_t3170162477 
{
public:
	// System.Int32 UnityEngine.AudioType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioType_t3170162477, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTYPE_T3170162477_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef IMAGEFILTERMODE_T35032559_H
#define IMAGEFILTERMODE_T35032559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util.ImageFilterMode
struct  ImageFilterMode_t35032559 
{
public:
	// System.Int32 Util.ImageFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageFilterMode_t35032559, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEFILTERMODE_T35032559_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef FILEACCESS_T1659085276_H
#define FILEACCESS_T1659085276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t1659085276 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t1659085276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T1659085276_H
#ifndef EPUZZLETOKEN_T2465232863_H
#define EPUZZLETOKEN_T2465232863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EPuzzleToken
struct  EPuzzleToken_t2465232863 
{
public:
	// System.Int32 EPuzzleToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EPuzzleToken_t2465232863, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPUZZLETOKEN_T2465232863_H
#ifndef EAUDIOSAMPLINGRATE_T3810640270_H
#define EAUDIOSAMPLINGRATE_T3810640270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EAudioSamplingRate
struct  EAudioSamplingRate_t3810640270 
{
public:
	// System.Int32 EAudioSamplingRate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EAudioSamplingRate_t3810640270, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAUDIOSAMPLINGRATE_T3810640270_H
#ifndef WAVESTREAM_T91297746_H
#define WAVESTREAM_T91297746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveStream
struct  WaveStream_t91297746  : public Stream_t1273022909
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVESTREAM_T91297746_H
#ifndef WAVEFORMATENCODING_T2321404235_H
#define WAVEFORMATENCODING_T2321404235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormatEncoding
struct  WaveFormatEncoding_t2321404235 
{
public:
	// System.UInt16 NAudio.Wave.WZT.WaveFormatEncoding::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaveFormatEncoding_t2321404235, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEFORMATENCODING_T2321404235_H
#ifndef FILEMODE_T1183438340_H
#define FILEMODE_T1183438340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileMode
struct  FileMode_t1183438340 
{
public:
	// System.Int32 System.IO.FileMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileMode_t1183438340, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEMODE_T1183438340_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_0)); }
	inline TimeSpan_t881159249  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_t881159249 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_t881159249  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_t881159249  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_t881159249  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_2)); }
	inline TimeSpan_t881159249  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_t881159249  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef FILEATTRIBUTES_T3417205536_H
#define FILEATTRIBUTES_T3417205536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t3417205536 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t3417205536, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T3417205536_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SEEKORIGIN_T1441174344_H
#define SEEKORIGIN_T1441174344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SeekOrigin
struct  SeekOrigin_t1441174344 
{
public:
	// System.Int32 System.IO.SeekOrigin::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SeekOrigin_t1441174344, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEKORIGIN_T1441174344_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AUDIOCONFIGURATION_T4040042187_H
#define AUDIOCONFIGURATION_T4040042187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioConfiguration
struct  AudioConfiguration_t4040042187 
{
public:
	// UnityEngine.AudioSpeakerMode UnityEngine.AudioConfiguration::speakerMode
	int32_t ___speakerMode_0;
	// System.Int32 UnityEngine.AudioConfiguration::dspBufferSize
	int32_t ___dspBufferSize_1;
	// System.Int32 UnityEngine.AudioConfiguration::sampleRate
	int32_t ___sampleRate_2;
	// System.Int32 UnityEngine.AudioConfiguration::numRealVoices
	int32_t ___numRealVoices_3;
	// System.Int32 UnityEngine.AudioConfiguration::numVirtualVoices
	int32_t ___numVirtualVoices_4;

public:
	inline static int32_t get_offset_of_speakerMode_0() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___speakerMode_0)); }
	inline int32_t get_speakerMode_0() const { return ___speakerMode_0; }
	inline int32_t* get_address_of_speakerMode_0() { return &___speakerMode_0; }
	inline void set_speakerMode_0(int32_t value)
	{
		___speakerMode_0 = value;
	}

	inline static int32_t get_offset_of_dspBufferSize_1() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___dspBufferSize_1)); }
	inline int32_t get_dspBufferSize_1() const { return ___dspBufferSize_1; }
	inline int32_t* get_address_of_dspBufferSize_1() { return &___dspBufferSize_1; }
	inline void set_dspBufferSize_1(int32_t value)
	{
		___dspBufferSize_1 = value;
	}

	inline static int32_t get_offset_of_sampleRate_2() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___sampleRate_2)); }
	inline int32_t get_sampleRate_2() const { return ___sampleRate_2; }
	inline int32_t* get_address_of_sampleRate_2() { return &___sampleRate_2; }
	inline void set_sampleRate_2(int32_t value)
	{
		___sampleRate_2 = value;
	}

	inline static int32_t get_offset_of_numRealVoices_3() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___numRealVoices_3)); }
	inline int32_t get_numRealVoices_3() const { return ___numRealVoices_3; }
	inline int32_t* get_address_of_numRealVoices_3() { return &___numRealVoices_3; }
	inline void set_numRealVoices_3(int32_t value)
	{
		___numRealVoices_3 = value;
	}

	inline static int32_t get_offset_of_numVirtualVoices_4() { return static_cast<int32_t>(offsetof(AudioConfiguration_t4040042187, ___numVirtualVoices_4)); }
	inline int32_t get_numVirtualVoices_4() const { return ___numVirtualVoices_4; }
	inline int32_t* get_address_of_numVirtualVoices_4() { return &___numVirtualVoices_4; }
	inline void set_numVirtualVoices_4(int32_t value)
	{
		___numVirtualVoices_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONFIGURATION_T4040042187_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef WAVEFORMAT_T2842237185_H
#define WAVEFORMAT_T2842237185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormat
#pragma pack(push, tp, 2)
struct  WaveFormat_t2842237185  : public RuntimeObject
{
public:
	// NAudio.Wave.WZT.WaveFormatEncoding NAudio.Wave.WZT.WaveFormat::waveFormatTag
	uint16_t ___waveFormatTag_0;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::channels
	int16_t ___channels_1;
	// System.Int32 NAudio.Wave.WZT.WaveFormat::sampleRate
	int32_t ___sampleRate_2;
	// System.Int32 NAudio.Wave.WZT.WaveFormat::averageBytesPerSecond
	int32_t ___averageBytesPerSecond_3;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::blockAlign
	int16_t ___blockAlign_4;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::bitsPerSample
	int16_t ___bitsPerSample_5;
	// System.Int16 NAudio.Wave.WZT.WaveFormat::extraSize
	int16_t ___extraSize_6;

public:
	inline static int32_t get_offset_of_waveFormatTag_0() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___waveFormatTag_0)); }
	inline uint16_t get_waveFormatTag_0() const { return ___waveFormatTag_0; }
	inline uint16_t* get_address_of_waveFormatTag_0() { return &___waveFormatTag_0; }
	inline void set_waveFormatTag_0(uint16_t value)
	{
		___waveFormatTag_0 = value;
	}

	inline static int32_t get_offset_of_channels_1() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___channels_1)); }
	inline int16_t get_channels_1() const { return ___channels_1; }
	inline int16_t* get_address_of_channels_1() { return &___channels_1; }
	inline void set_channels_1(int16_t value)
	{
		___channels_1 = value;
	}

	inline static int32_t get_offset_of_sampleRate_2() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___sampleRate_2)); }
	inline int32_t get_sampleRate_2() const { return ___sampleRate_2; }
	inline int32_t* get_address_of_sampleRate_2() { return &___sampleRate_2; }
	inline void set_sampleRate_2(int32_t value)
	{
		___sampleRate_2 = value;
	}

	inline static int32_t get_offset_of_averageBytesPerSecond_3() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___averageBytesPerSecond_3)); }
	inline int32_t get_averageBytesPerSecond_3() const { return ___averageBytesPerSecond_3; }
	inline int32_t* get_address_of_averageBytesPerSecond_3() { return &___averageBytesPerSecond_3; }
	inline void set_averageBytesPerSecond_3(int32_t value)
	{
		___averageBytesPerSecond_3 = value;
	}

	inline static int32_t get_offset_of_blockAlign_4() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___blockAlign_4)); }
	inline int16_t get_blockAlign_4() const { return ___blockAlign_4; }
	inline int16_t* get_address_of_blockAlign_4() { return &___blockAlign_4; }
	inline void set_blockAlign_4(int16_t value)
	{
		___blockAlign_4 = value;
	}

	inline static int32_t get_offset_of_bitsPerSample_5() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___bitsPerSample_5)); }
	inline int16_t get_bitsPerSample_5() const { return ___bitsPerSample_5; }
	inline int16_t* get_address_of_bitsPerSample_5() { return &___bitsPerSample_5; }
	inline void set_bitsPerSample_5(int16_t value)
	{
		___bitsPerSample_5 = value;
	}

	inline static int32_t get_offset_of_extraSize_6() { return static_cast<int32_t>(offsetof(WaveFormat_t2842237185, ___extraSize_6)); }
	inline int16_t get_extraSize_6() const { return ___extraSize_6; }
	inline int16_t* get_address_of_extraSize_6() { return &___extraSize_6; }
	inline void set_extraSize_6(int16_t value)
	{
		___extraSize_6 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.WaveFormat
#pragma pack(push, tp, 2)
struct WaveFormat_t2842237185_marshaled_pinvoke
{
	uint16_t ___waveFormatTag_0;
	int16_t ___channels_1;
	int32_t ___sampleRate_2;
	int32_t ___averageBytesPerSecond_3;
	int16_t ___blockAlign_4;
	int16_t ___bitsPerSample_5;
	int16_t ___extraSize_6;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.WaveFormat
#pragma pack(push, tp, 2)
struct WaveFormat_t2842237185_marshaled_com
{
	uint16_t ___waveFormatTag_0;
	int16_t ___channels_1;
	int32_t ___sampleRate_2;
	int32_t ___averageBytesPerSecond_3;
	int16_t ___blockAlign_4;
	int16_t ___bitsPerSample_5;
	int16_t ___extraSize_6;
};
#pragma pack(pop, tp)
#endif // WAVEFORMAT_T2842237185_H
#ifndef AUDIOCLIP_T3680889665_H
#define AUDIOCLIP_T3680889665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t3680889665  : public Object_t631007953
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1677636661 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1059417452 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t1677636661 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t1677636661 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t1677636661 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t1059417452 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t1059417452 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T3680889665_H
#ifndef FILESTREAM_T4292183065_H
#define FILESTREAM_T4292183065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_t4292183065  : public Stream_t1273022909
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_t4116647657* ___buf_6;
	// System.String System.IO.FileStream::name
	String_t* ___name_7;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_t2755812594 * ___safeHandle_8;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_9;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_10;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_11;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_12;
	// System.Boolean System.IO.FileStream::async
	bool ___async_13;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_14;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_15;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_16;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_17;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_18;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_19;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_20;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_6)); }
	inline ByteU5BU5D_t4116647657* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_t4116647657* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_safeHandle_8() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___safeHandle_8)); }
	inline SafeFileHandle_t2755812594 * get_safeHandle_8() const { return ___safeHandle_8; }
	inline SafeFileHandle_t2755812594 ** get_address_of_safeHandle_8() { return &___safeHandle_8; }
	inline void set_safeHandle_8(SafeFileHandle_t2755812594 * value)
	{
		___safeHandle_8 = value;
		Il2CppCodeGenWriteBarrier((&___safeHandle_8), value);
	}

	inline static int32_t get_offset_of_isExposed_9() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___isExposed_9)); }
	inline bool get_isExposed_9() const { return ___isExposed_9; }
	inline bool* get_address_of_isExposed_9() { return &___isExposed_9; }
	inline void set_isExposed_9(bool value)
	{
		___isExposed_9 = value;
	}

	inline static int32_t get_offset_of_append_startpos_10() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___append_startpos_10)); }
	inline int64_t get_append_startpos_10() const { return ___append_startpos_10; }
	inline int64_t* get_address_of_append_startpos_10() { return &___append_startpos_10; }
	inline void set_append_startpos_10(int64_t value)
	{
		___append_startpos_10 = value;
	}

	inline static int32_t get_offset_of_access_11() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___access_11)); }
	inline int32_t get_access_11() const { return ___access_11; }
	inline int32_t* get_address_of_access_11() { return &___access_11; }
	inline void set_access_11(int32_t value)
	{
		___access_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___owner_12)); }
	inline bool get_owner_12() const { return ___owner_12; }
	inline bool* get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(bool value)
	{
		___owner_12 = value;
	}

	inline static int32_t get_offset_of_async_13() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___async_13)); }
	inline bool get_async_13() const { return ___async_13; }
	inline bool* get_address_of_async_13() { return &___async_13; }
	inline void set_async_13(bool value)
	{
		___async_13 = value;
	}

	inline static int32_t get_offset_of_canseek_14() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___canseek_14)); }
	inline bool get_canseek_14() const { return ___canseek_14; }
	inline bool* get_address_of_canseek_14() { return &___canseek_14; }
	inline void set_canseek_14(bool value)
	{
		___canseek_14 = value;
	}

	inline static int32_t get_offset_of_anonymous_15() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___anonymous_15)); }
	inline bool get_anonymous_15() const { return ___anonymous_15; }
	inline bool* get_address_of_anonymous_15() { return &___anonymous_15; }
	inline void set_anonymous_15(bool value)
	{
		___anonymous_15 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_16() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_dirty_16)); }
	inline bool get_buf_dirty_16() const { return ___buf_dirty_16; }
	inline bool* get_address_of_buf_dirty_16() { return &___buf_dirty_16; }
	inline void set_buf_dirty_16(bool value)
	{
		___buf_dirty_16 = value;
	}

	inline static int32_t get_offset_of_buf_size_17() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_size_17)); }
	inline int32_t get_buf_size_17() const { return ___buf_size_17; }
	inline int32_t* get_address_of_buf_size_17() { return &___buf_size_17; }
	inline void set_buf_size_17(int32_t value)
	{
		___buf_size_17 = value;
	}

	inline static int32_t get_offset_of_buf_length_18() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_length_18)); }
	inline int32_t get_buf_length_18() const { return ___buf_length_18; }
	inline int32_t* get_address_of_buf_length_18() { return &___buf_length_18; }
	inline void set_buf_length_18(int32_t value)
	{
		___buf_length_18 = value;
	}

	inline static int32_t get_offset_of_buf_offset_19() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_offset_19)); }
	inline int32_t get_buf_offset_19() const { return ___buf_offset_19; }
	inline int32_t* get_address_of_buf_offset_19() { return &___buf_offset_19; }
	inline void set_buf_offset_19(int32_t value)
	{
		___buf_offset_19 = value;
	}

	inline static int32_t get_offset_of_buf_start_20() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_start_20)); }
	inline int64_t get_buf_start_20() const { return ___buf_start_20; }
	inline int64_t* get_address_of_buf_start_20() { return &___buf_start_20; }
	inline void set_buf_start_20(int64_t value)
	{
		___buf_start_20 = value;
	}
};

struct FileStream_t4292183065_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_t4116647657* ___buf_recycle_4;
	// System.Object System.IO.FileStream::buf_recycle_lock
	RuntimeObject * ___buf_recycle_lock_5;

public:
	inline static int32_t get_offset_of_buf_recycle_4() { return static_cast<int32_t>(offsetof(FileStream_t4292183065_StaticFields, ___buf_recycle_4)); }
	inline ByteU5BU5D_t4116647657* get_buf_recycle_4() const { return ___buf_recycle_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_recycle_4() { return &___buf_recycle_4; }
	inline void set_buf_recycle_4(ByteU5BU5D_t4116647657* value)
	{
		___buf_recycle_4 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_4), value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_5() { return static_cast<int32_t>(offsetof(FileStream_t4292183065_StaticFields, ___buf_recycle_lock_5)); }
	inline RuntimeObject * get_buf_recycle_lock_5() const { return ___buf_recycle_lock_5; }
	inline RuntimeObject ** get_address_of_buf_recycle_lock_5() { return &___buf_recycle_lock_5; }
	inline void set_buf_recycle_lock_5(RuntimeObject * value)
	{
		___buf_recycle_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_lock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_T4292183065_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MONOIOSTAT_T592533987_H
#define MONOIOSTAT_T592533987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t592533987 
{
public:
	// System.IO.FileAttributes System.IO.MonoIOStat::fileAttributes
	int32_t ___fileAttributes_0;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_1;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_2;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_3;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_4;

public:
	inline static int32_t get_offset_of_fileAttributes_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___fileAttributes_0)); }
	inline int32_t get_fileAttributes_0() const { return ___fileAttributes_0; }
	inline int32_t* get_address_of_fileAttributes_0() { return &___fileAttributes_0; }
	inline void set_fileAttributes_0(int32_t value)
	{
		___fileAttributes_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Length_1)); }
	inline int64_t get_Length_1() const { return ___Length_1; }
	inline int64_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int64_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_CreationTime_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___CreationTime_2)); }
	inline int64_t get_CreationTime_2() const { return ___CreationTime_2; }
	inline int64_t* get_address_of_CreationTime_2() { return &___CreationTime_2; }
	inline void set_CreationTime_2(int64_t value)
	{
		___CreationTime_2 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastAccessTime_3)); }
	inline int64_t get_LastAccessTime_3() const { return ___LastAccessTime_3; }
	inline int64_t* get_address_of_LastAccessTime_3() { return &___LastAccessTime_3; }
	inline void set_LastAccessTime_3(int64_t value)
	{
		___LastAccessTime_3 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastWriteTime_4)); }
	inline int64_t get_LastWriteTime_4() const { return ___LastWriteTime_4; }
	inline int64_t* get_address_of_LastWriteTime_4() { return &___LastWriteTime_4; }
	inline void set_LastWriteTime_4(int64_t value)
	{
		___LastWriteTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOIOSTAT_T592533987_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef WAVEFORMATEXTRADATA_T1489494699_H
#define WAVEFORMATEXTRADATA_T1489494699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NAudio.Wave.WZT.WaveFormatExtraData
struct  WaveFormatExtraData_t1489494699  : public WaveFormat_t2842237185
{
public:
	// System.Byte[] NAudio.Wave.WZT.WaveFormatExtraData::extraData
	ByteU5BU5D_t4116647657* ___extraData_7;

public:
	inline static int32_t get_offset_of_extraData_7() { return static_cast<int32_t>(offsetof(WaveFormatExtraData_t1489494699, ___extraData_7)); }
	inline ByteU5BU5D_t4116647657* get_extraData_7() const { return ___extraData_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_extraData_7() { return &___extraData_7; }
	inline void set_extraData_7(ByteU5BU5D_t4116647657* value)
	{
		___extraData_7 = value;
		Il2CppCodeGenWriteBarrier((&___extraData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NAudio.Wave.WZT.WaveFormatExtraData
#pragma pack(push, tp, 2)
struct WaveFormatExtraData_t1489494699_marshaled_pinvoke : public WaveFormat_t2842237185_marshaled_pinvoke
{
	uint8_t ___extraData_7[100];
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of NAudio.Wave.WZT.WaveFormatExtraData
#pragma pack(push, tp, 2)
struct WaveFormatExtraData_t1489494699_marshaled_com : public WaveFormat_t2842237185_marshaled_com
{
	uint8_t ___extraData_7[100];
};
#pragma pack(pop, tp)
#endif // WAVEFORMATEXTRADATA_T1489494699_H
#ifndef PUZZLETOKENEVENTHANDLER_T3807414068_H
#define PUZZLETOKENEVENTHANDLER_T3807414068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleToken/PuzzleTokenEventHandler
struct  PuzzleTokenEventHandler_t3807414068  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLETOKENEVENTHANDLER_T3807414068_H
#ifndef FILESYSTEMINFO_T3745885336_H
#define FILESYSTEMINFO_T3745885336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemInfo
struct  FileSystemInfo_t3745885336  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.MonoIOStat System.IO.FileSystemInfo::_data
	MonoIOStat_t592533987  ____data_1;
	// System.Int32 System.IO.FileSystemInfo::_dataInitialised
	int32_t ____dataInitialised_2;
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_3;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_4;
	// System.String System.IO.FileSystemInfo::_displayPath
	String_t* ____displayPath_5;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ____data_1)); }
	inline MonoIOStat_t592533987  get__data_1() const { return ____data_1; }
	inline MonoIOStat_t592533987 * get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(MonoIOStat_t592533987  value)
	{
		____data_1 = value;
	}

	inline static int32_t get_offset_of__dataInitialised_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ____dataInitialised_2)); }
	inline int32_t get__dataInitialised_2() const { return ____dataInitialised_2; }
	inline int32_t* get_address_of__dataInitialised_2() { return &____dataInitialised_2; }
	inline void set__dataInitialised_2(int32_t value)
	{
		____dataInitialised_2 = value;
	}

	inline static int32_t get_offset_of_FullPath_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___FullPath_3)); }
	inline String_t* get_FullPath_3() const { return ___FullPath_3; }
	inline String_t** get_address_of_FullPath_3() { return &___FullPath_3; }
	inline void set_FullPath_3(String_t* value)
	{
		___FullPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___FullPath_3), value);
	}

	inline static int32_t get_offset_of_OriginalPath_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___OriginalPath_4)); }
	inline String_t* get_OriginalPath_4() const { return ___OriginalPath_4; }
	inline String_t** get_address_of_OriginalPath_4() { return &___OriginalPath_4; }
	inline void set_OriginalPath_4(String_t* value)
	{
		___OriginalPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPath_4), value);
	}

	inline static int32_t get_offset_of__displayPath_5() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ____displayPath_5)); }
	inline String_t* get__displayPath_5() const { return ____displayPath_5; }
	inline String_t** get_address_of__displayPath_5() { return &____displayPath_5; }
	inline void set__displayPath_5(String_t* value)
	{
		____displayPath_5 = value;
		Il2CppCodeGenWriteBarrier((&____displayPath_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMINFO_T3745885336_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef PUZZLEINSERTEVENTHANDLER_T3824397729_H
#define PUZZLEINSERTEVENTHANDLER_T3824397729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleInsert/PuzzleInsertEventHandler
struct  PuzzleInsertEventHandler_t3824397729  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEINSERTEVENTHANDLER_T3824397729_H
#ifndef PUZZLECONTROLLEREVENTHANDLER_T963335703_H
#define PUZZLECONTROLLEREVENTHANDLER_T963335703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController/PuzzleControllerEventHandler
struct  PuzzleControllerEventHandler_t963335703  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLECONTROLLEREVENTHANDLER_T963335703_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef DIRECTORYINFO_T35957480_H
#define DIRECTORYINFO_T35957480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DirectoryInfo
struct  DirectoryInfo_t35957480  : public FileSystemInfo_t3745885336
{
public:
	// System.String System.IO.DirectoryInfo::current
	String_t* ___current_6;
	// System.String System.IO.DirectoryInfo::parent
	String_t* ___parent_7;

public:
	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___current_6)); }
	inline String_t* get_current_6() const { return ___current_6; }
	inline String_t** get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(String_t* value)
	{
		___current_6 = value;
		Il2CppCodeGenWriteBarrier((&___current_6), value);
	}

	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___parent_7)); }
	inline String_t* get_parent_7() const { return ___parent_7; }
	inline String_t** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(String_t* value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYINFO_T35957480_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIDISPLAYCONTROLLER_T1173465452_H
#define UIDISPLAYCONTROLLER_T1173465452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDisplayController
struct  UIDisplayController_t1173465452  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIDisplayController::encodingMessagePanel
	GameObject_t1113636619 * ___encodingMessagePanel_2;
	// UnityEngine.UI.Text UIDisplayController::elapsedTimeText
	Text_t1901882714 * ___elapsedTimeText_3;
	// UnityEngine.UI.Text UIDisplayController::timerText
	Text_t1901882714 * ___timerText_4;
	// UnityEngine.UI.Button UIDisplayController::stop
	Button_t4055032469 * ___stop_5;
	// UnityEngine.UI.Button UIDisplayController::start
	Button_t4055032469 * ___start_6;
	// AudioRecordManager UIDisplayController::recordManager
	AudioRecordManager_t3166594816 * ___recordManager_7;
	// SceneChanger UIDisplayController::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_8;
	// QuestManager UIDisplayController::manager
	QuestManager_t588401851 * ___manager_9;
	// Quest UIDisplayController::quest
	Quest_t3696879532 * ___quest_10;

public:
	inline static int32_t get_offset_of_encodingMessagePanel_2() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___encodingMessagePanel_2)); }
	inline GameObject_t1113636619 * get_encodingMessagePanel_2() const { return ___encodingMessagePanel_2; }
	inline GameObject_t1113636619 ** get_address_of_encodingMessagePanel_2() { return &___encodingMessagePanel_2; }
	inline void set_encodingMessagePanel_2(GameObject_t1113636619 * value)
	{
		___encodingMessagePanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___encodingMessagePanel_2), value);
	}

	inline static int32_t get_offset_of_elapsedTimeText_3() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___elapsedTimeText_3)); }
	inline Text_t1901882714 * get_elapsedTimeText_3() const { return ___elapsedTimeText_3; }
	inline Text_t1901882714 ** get_address_of_elapsedTimeText_3() { return &___elapsedTimeText_3; }
	inline void set_elapsedTimeText_3(Text_t1901882714 * value)
	{
		___elapsedTimeText_3 = value;
		Il2CppCodeGenWriteBarrier((&___elapsedTimeText_3), value);
	}

	inline static int32_t get_offset_of_timerText_4() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___timerText_4)); }
	inline Text_t1901882714 * get_timerText_4() const { return ___timerText_4; }
	inline Text_t1901882714 ** get_address_of_timerText_4() { return &___timerText_4; }
	inline void set_timerText_4(Text_t1901882714 * value)
	{
		___timerText_4 = value;
		Il2CppCodeGenWriteBarrier((&___timerText_4), value);
	}

	inline static int32_t get_offset_of_stop_5() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___stop_5)); }
	inline Button_t4055032469 * get_stop_5() const { return ___stop_5; }
	inline Button_t4055032469 ** get_address_of_stop_5() { return &___stop_5; }
	inline void set_stop_5(Button_t4055032469 * value)
	{
		___stop_5 = value;
		Il2CppCodeGenWriteBarrier((&___stop_5), value);
	}

	inline static int32_t get_offset_of_start_6() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___start_6)); }
	inline Button_t4055032469 * get_start_6() const { return ___start_6; }
	inline Button_t4055032469 ** get_address_of_start_6() { return &___start_6; }
	inline void set_start_6(Button_t4055032469 * value)
	{
		___start_6 = value;
		Il2CppCodeGenWriteBarrier((&___start_6), value);
	}

	inline static int32_t get_offset_of_recordManager_7() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___recordManager_7)); }
	inline AudioRecordManager_t3166594816 * get_recordManager_7() const { return ___recordManager_7; }
	inline AudioRecordManager_t3166594816 ** get_address_of_recordManager_7() { return &___recordManager_7; }
	inline void set_recordManager_7(AudioRecordManager_t3166594816 * value)
	{
		___recordManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___recordManager_7), value);
	}

	inline static int32_t get_offset_of_sceneChanger_8() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___sceneChanger_8)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_8() const { return ___sceneChanger_8; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_8() { return &___sceneChanger_8; }
	inline void set_sceneChanger_8(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_8 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_8), value);
	}

	inline static int32_t get_offset_of_manager_9() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___manager_9)); }
	inline QuestManager_t588401851 * get_manager_9() const { return ___manager_9; }
	inline QuestManager_t588401851 ** get_address_of_manager_9() { return &___manager_9; }
	inline void set_manager_9(QuestManager_t588401851 * value)
	{
		___manager_9 = value;
		Il2CppCodeGenWriteBarrier((&___manager_9), value);
	}

	inline static int32_t get_offset_of_quest_10() { return static_cast<int32_t>(offsetof(UIDisplayController_t1173465452, ___quest_10)); }
	inline Quest_t3696879532 * get_quest_10() const { return ___quest_10; }
	inline Quest_t3696879532 ** get_address_of_quest_10() { return &___quest_10; }
	inline void set_quest_10(Quest_t3696879532 * value)
	{
		___quest_10 = value;
		Il2CppCodeGenWriteBarrier((&___quest_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDISPLAYCONTROLLER_T1173465452_H
#ifndef DATACONTROLLER_T353634109_H
#define DATACONTROLLER_T353634109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataController
struct  DataController_t353634109  : public MonoBehaviour_t3962482529
{
public:
	// PlayerData DataController::playerData
	PlayerData_t220878115 * ___playerData_2;
	// GameData DataController::gameData
	GameData_t415813024 * ___gameData_3;
	// System.String DataController::gameDataFileName
	String_t* ___gameDataFileName_4;
	// System.String DataController::playerDataFileName
	String_t* ___playerDataFileName_5;
	// System.Int32 DataController::isFirstTime
	int32_t ___isFirstTime_6;
	// System.Int32 DataController::currentLevel
	int32_t ___currentLevel_7;
	// QuestManager DataController::manager
	QuestManager_t588401851 * ___manager_8;
	// Quest DataController::quest
	Quest_t3696879532 * ___quest_9;

public:
	inline static int32_t get_offset_of_playerData_2() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___playerData_2)); }
	inline PlayerData_t220878115 * get_playerData_2() const { return ___playerData_2; }
	inline PlayerData_t220878115 ** get_address_of_playerData_2() { return &___playerData_2; }
	inline void set_playerData_2(PlayerData_t220878115 * value)
	{
		___playerData_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerData_2), value);
	}

	inline static int32_t get_offset_of_gameData_3() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___gameData_3)); }
	inline GameData_t415813024 * get_gameData_3() const { return ___gameData_3; }
	inline GameData_t415813024 ** get_address_of_gameData_3() { return &___gameData_3; }
	inline void set_gameData_3(GameData_t415813024 * value)
	{
		___gameData_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameData_3), value);
	}

	inline static int32_t get_offset_of_gameDataFileName_4() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___gameDataFileName_4)); }
	inline String_t* get_gameDataFileName_4() const { return ___gameDataFileName_4; }
	inline String_t** get_address_of_gameDataFileName_4() { return &___gameDataFileName_4; }
	inline void set_gameDataFileName_4(String_t* value)
	{
		___gameDataFileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameDataFileName_4), value);
	}

	inline static int32_t get_offset_of_playerDataFileName_5() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___playerDataFileName_5)); }
	inline String_t* get_playerDataFileName_5() const { return ___playerDataFileName_5; }
	inline String_t** get_address_of_playerDataFileName_5() { return &___playerDataFileName_5; }
	inline void set_playerDataFileName_5(String_t* value)
	{
		___playerDataFileName_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerDataFileName_5), value);
	}

	inline static int32_t get_offset_of_isFirstTime_6() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___isFirstTime_6)); }
	inline int32_t get_isFirstTime_6() const { return ___isFirstTime_6; }
	inline int32_t* get_address_of_isFirstTime_6() { return &___isFirstTime_6; }
	inline void set_isFirstTime_6(int32_t value)
	{
		___isFirstTime_6 = value;
	}

	inline static int32_t get_offset_of_currentLevel_7() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___currentLevel_7)); }
	inline int32_t get_currentLevel_7() const { return ___currentLevel_7; }
	inline int32_t* get_address_of_currentLevel_7() { return &___currentLevel_7; }
	inline void set_currentLevel_7(int32_t value)
	{
		___currentLevel_7 = value;
	}

	inline static int32_t get_offset_of_manager_8() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___manager_8)); }
	inline QuestManager_t588401851 * get_manager_8() const { return ___manager_8; }
	inline QuestManager_t588401851 ** get_address_of_manager_8() { return &___manager_8; }
	inline void set_manager_8(QuestManager_t588401851 * value)
	{
		___manager_8 = value;
		Il2CppCodeGenWriteBarrier((&___manager_8), value);
	}

	inline static int32_t get_offset_of_quest_9() { return static_cast<int32_t>(offsetof(DataController_t353634109, ___quest_9)); }
	inline Quest_t3696879532 * get_quest_9() const { return ___quest_9; }
	inline Quest_t3696879532 ** get_address_of_quest_9() { return &___quest_9; }
	inline void set_quest_9(Quest_t3696879532 * value)
	{
		___quest_9 = value;
		Il2CppCodeGenWriteBarrier((&___quest_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACONTROLLER_T353634109_H
#ifndef SETTINGSCONTROLLER_T1943457452_H
#define SETTINGSCONTROLLER_T1943457452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsController
struct  SettingsController_t1943457452  : public MonoBehaviour_t3962482529
{
public:
	// GameController SettingsController::gameController
	GameController_t2330501625 * ___gameController_2;

public:
	inline static int32_t get_offset_of_gameController_2() { return static_cast<int32_t>(offsetof(SettingsController_t1943457452, ___gameController_2)); }
	inline GameController_t2330501625 * get_gameController_2() const { return ___gameController_2; }
	inline GameController_t2330501625 ** get_address_of_gameController_2() { return &___gameController_2; }
	inline void set_gameController_2(GameController_t2330501625 * value)
	{
		___gameController_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSCONTROLLER_T1943457452_H
#ifndef SCENECHANGER_T1033871796_H
#define SCENECHANGER_T1033871796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneChanger
struct  SceneChanger_t1033871796  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SceneChanger::buttonOk
	Button_t4055032469 * ___buttonOk_2;
	// UnityEngine.UI.Button SceneChanger::buttonWrong
	Button_t4055032469 * ___buttonWrong_3;
	// GameController SceneChanger::gameController
	GameController_t2330501625 * ___gameController_4;
	// DataController SceneChanger::dataController
	DataController_t353634109 * ___dataController_5;

public:
	inline static int32_t get_offset_of_buttonOk_2() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___buttonOk_2)); }
	inline Button_t4055032469 * get_buttonOk_2() const { return ___buttonOk_2; }
	inline Button_t4055032469 ** get_address_of_buttonOk_2() { return &___buttonOk_2; }
	inline void set_buttonOk_2(Button_t4055032469 * value)
	{
		___buttonOk_2 = value;
		Il2CppCodeGenWriteBarrier((&___buttonOk_2), value);
	}

	inline static int32_t get_offset_of_buttonWrong_3() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___buttonWrong_3)); }
	inline Button_t4055032469 * get_buttonWrong_3() const { return ___buttonWrong_3; }
	inline Button_t4055032469 ** get_address_of_buttonWrong_3() { return &___buttonWrong_3; }
	inline void set_buttonWrong_3(Button_t4055032469 * value)
	{
		___buttonWrong_3 = value;
		Il2CppCodeGenWriteBarrier((&___buttonWrong_3), value);
	}

	inline static int32_t get_offset_of_gameController_4() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___gameController_4)); }
	inline GameController_t2330501625 * get_gameController_4() const { return ___gameController_4; }
	inline GameController_t2330501625 ** get_address_of_gameController_4() { return &___gameController_4; }
	inline void set_gameController_4(GameController_t2330501625 * value)
	{
		___gameController_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_4), value);
	}

	inline static int32_t get_offset_of_dataController_5() { return static_cast<int32_t>(offsetof(SceneChanger_t1033871796, ___dataController_5)); }
	inline DataController_t353634109 * get_dataController_5() const { return ___dataController_5; }
	inline DataController_t353634109 ** get_address_of_dataController_5() { return &___dataController_5; }
	inline void set_dataController_5(DataController_t353634109 * value)
	{
		___dataController_5 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECHANGER_T1033871796_H
#ifndef CHARACTERBEHAVIOR_T2392107484_H
#define CHARACTERBEHAVIOR_T2392107484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterBehavior
struct  CharacterBehavior_t2392107484  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CharacterBehavior::gameStarted
	bool ___gameStarted_2;
	// System.Int32 CharacterBehavior::posLevelCharacter
	int32_t ___posLevelCharacter_3;
	// System.Int32 CharacterBehavior::level
	int32_t ___level_4;
	// System.Single CharacterBehavior::speed
	float ___speed_5;
	// System.Boolean CharacterBehavior::isMoving
	bool ___isMoving_6;
	// UnityEngine.Vector3 CharacterBehavior::initPos
	Vector3_t3722313464  ___initPos_8;
	// UnityEngine.Vector3 CharacterBehavior::lastPos
	Vector3_t3722313464  ___lastPos_9;
	// UnityEngine.Sprite[] CharacterBehavior::characteres
	SpriteU5BU5D_t2581906349* ___characteres_10;
	// System.String CharacterBehavior::characterSelected
	String_t* ___characterSelected_11;
	// DataController CharacterBehavior::dataController
	DataController_t353634109 * ___dataController_12;
	// GameController CharacterBehavior::gameController
	GameController_t2330501625 * ___gameController_13;

public:
	inline static int32_t get_offset_of_gameStarted_2() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___gameStarted_2)); }
	inline bool get_gameStarted_2() const { return ___gameStarted_2; }
	inline bool* get_address_of_gameStarted_2() { return &___gameStarted_2; }
	inline void set_gameStarted_2(bool value)
	{
		___gameStarted_2 = value;
	}

	inline static int32_t get_offset_of_posLevelCharacter_3() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___posLevelCharacter_3)); }
	inline int32_t get_posLevelCharacter_3() const { return ___posLevelCharacter_3; }
	inline int32_t* get_address_of_posLevelCharacter_3() { return &___posLevelCharacter_3; }
	inline void set_posLevelCharacter_3(int32_t value)
	{
		___posLevelCharacter_3 = value;
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___level_4)); }
	inline int32_t get_level_4() const { return ___level_4; }
	inline int32_t* get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(int32_t value)
	{
		___level_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_isMoving_6() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___isMoving_6)); }
	inline bool get_isMoving_6() const { return ___isMoving_6; }
	inline bool* get_address_of_isMoving_6() { return &___isMoving_6; }
	inline void set_isMoving_6(bool value)
	{
		___isMoving_6 = value;
	}

	inline static int32_t get_offset_of_initPos_8() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___initPos_8)); }
	inline Vector3_t3722313464  get_initPos_8() const { return ___initPos_8; }
	inline Vector3_t3722313464 * get_address_of_initPos_8() { return &___initPos_8; }
	inline void set_initPos_8(Vector3_t3722313464  value)
	{
		___initPos_8 = value;
	}

	inline static int32_t get_offset_of_lastPos_9() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___lastPos_9)); }
	inline Vector3_t3722313464  get_lastPos_9() const { return ___lastPos_9; }
	inline Vector3_t3722313464 * get_address_of_lastPos_9() { return &___lastPos_9; }
	inline void set_lastPos_9(Vector3_t3722313464  value)
	{
		___lastPos_9 = value;
	}

	inline static int32_t get_offset_of_characteres_10() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___characteres_10)); }
	inline SpriteU5BU5D_t2581906349* get_characteres_10() const { return ___characteres_10; }
	inline SpriteU5BU5D_t2581906349** get_address_of_characteres_10() { return &___characteres_10; }
	inline void set_characteres_10(SpriteU5BU5D_t2581906349* value)
	{
		___characteres_10 = value;
		Il2CppCodeGenWriteBarrier((&___characteres_10), value);
	}

	inline static int32_t get_offset_of_characterSelected_11() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___characterSelected_11)); }
	inline String_t* get_characterSelected_11() const { return ___characterSelected_11; }
	inline String_t** get_address_of_characterSelected_11() { return &___characterSelected_11; }
	inline void set_characterSelected_11(String_t* value)
	{
		___characterSelected_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterSelected_11), value);
	}

	inline static int32_t get_offset_of_dataController_12() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___dataController_12)); }
	inline DataController_t353634109 * get_dataController_12() const { return ___dataController_12; }
	inline DataController_t353634109 ** get_address_of_dataController_12() { return &___dataController_12; }
	inline void set_dataController_12(DataController_t353634109 * value)
	{
		___dataController_12 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_12), value);
	}

	inline static int32_t get_offset_of_gameController_13() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484, ___gameController_13)); }
	inline GameController_t2330501625 * get_gameController_13() const { return ___gameController_13; }
	inline GameController_t2330501625 ** get_address_of_gameController_13() { return &___gameController_13; }
	inline void set_gameController_13(GameController_t2330501625 * value)
	{
		___gameController_13 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_13), value);
	}
};

struct CharacterBehavior_t2392107484_StaticFields
{
public:
	// System.Boolean CharacterBehavior::playerCreated
	bool ___playerCreated_7;

public:
	inline static int32_t get_offset_of_playerCreated_7() { return static_cast<int32_t>(offsetof(CharacterBehavior_t2392107484_StaticFields, ___playerCreated_7)); }
	inline bool get_playerCreated_7() const { return ___playerCreated_7; }
	inline bool* get_address_of_playerCreated_7() { return &___playerCreated_7; }
	inline void set_playerCreated_7(bool value)
	{
		___playerCreated_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERBEHAVIOR_T2392107484_H
#ifndef QUESTMANAGER_T588401851_H
#define QUESTMANAGER_T588401851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestManager
struct  QuestManager_t588401851  : public MonoBehaviour_t3962482529
{
public:
	// Quest[] QuestManager::quests
	QuestU5BU5D_t1937508389* ___quests_2;
	// System.Boolean[] QuestManager::questCompleted
	BooleanU5BU5D_t2897418192* ___questCompleted_3;
	// System.Boolean QuestManager::isCompleted
	bool ___isCompleted_4;
	// System.Int32 QuestManager::questID
	int32_t ___questID_5;
	// System.Int32 QuestManager::storySelected
	int32_t ___storySelected_6;
	// GameController QuestManager::gameController
	GameController_t2330501625 * ___gameController_7;

public:
	inline static int32_t get_offset_of_quests_2() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___quests_2)); }
	inline QuestU5BU5D_t1937508389* get_quests_2() const { return ___quests_2; }
	inline QuestU5BU5D_t1937508389** get_address_of_quests_2() { return &___quests_2; }
	inline void set_quests_2(QuestU5BU5D_t1937508389* value)
	{
		___quests_2 = value;
		Il2CppCodeGenWriteBarrier((&___quests_2), value);
	}

	inline static int32_t get_offset_of_questCompleted_3() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___questCompleted_3)); }
	inline BooleanU5BU5D_t2897418192* get_questCompleted_3() const { return ___questCompleted_3; }
	inline BooleanU5BU5D_t2897418192** get_address_of_questCompleted_3() { return &___questCompleted_3; }
	inline void set_questCompleted_3(BooleanU5BU5D_t2897418192* value)
	{
		___questCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___questCompleted_3), value);
	}

	inline static int32_t get_offset_of_isCompleted_4() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___isCompleted_4)); }
	inline bool get_isCompleted_4() const { return ___isCompleted_4; }
	inline bool* get_address_of_isCompleted_4() { return &___isCompleted_4; }
	inline void set_isCompleted_4(bool value)
	{
		___isCompleted_4 = value;
	}

	inline static int32_t get_offset_of_questID_5() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___questID_5)); }
	inline int32_t get_questID_5() const { return ___questID_5; }
	inline int32_t* get_address_of_questID_5() { return &___questID_5; }
	inline void set_questID_5(int32_t value)
	{
		___questID_5 = value;
	}

	inline static int32_t get_offset_of_storySelected_6() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___storySelected_6)); }
	inline int32_t get_storySelected_6() const { return ___storySelected_6; }
	inline int32_t* get_address_of_storySelected_6() { return &___storySelected_6; }
	inline void set_storySelected_6(int32_t value)
	{
		___storySelected_6 = value;
	}

	inline static int32_t get_offset_of_gameController_7() { return static_cast<int32_t>(offsetof(QuestManager_t588401851, ___gameController_7)); }
	inline GameController_t2330501625 * get_gameController_7() const { return ___gameController_7; }
	inline GameController_t2330501625 ** get_address_of_gameController_7() { return &___gameController_7; }
	inline void set_gameController_7(GameController_t2330501625 * value)
	{
		___gameController_7 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_7), value);
	}
};

struct QuestManager_t588401851_StaticFields
{
public:
	// QuestManager QuestManager::_instance
	QuestManager_t588401851 * ____instance_8;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(QuestManager_t588401851_StaticFields, ____instance_8)); }
	inline QuestManager_t588401851 * get__instance_8() const { return ____instance_8; }
	inline QuestManager_t588401851 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(QuestManager_t588401851 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTMANAGER_T588401851_H
#ifndef QUEST_T3696879532_H
#define QUEST_T3696879532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest
struct  Quest_t3696879532  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Quest::questID
	int32_t ___questID_2;
	// QuestManager Quest::manager
	QuestManager_t588401851 * ___manager_3;
	// System.Boolean Quest::isActivity
	bool ___isActivity_4;
	// System.String[] Quest::textActivity
	StringU5BU5D_t1281789340* ___textActivity_5;
	// System.Boolean Quest::isSelectMultiple
	bool ___isSelectMultiple_6;
	// System.Int32 Quest::numberOptions
	int32_t ___numberOptions_7;
	// System.String[] Quest::options
	StringU5BU5D_t1281789340* ___options_8;
	// System.Int32 Quest::rigthOption
	int32_t ___rigthOption_9;
	// System.Int32 Quest::optionsSelected
	int32_t ___optionsSelected_10;
	// System.Boolean Quest::isStory
	bool ___isStory_11;
	// System.Int32 Quest::numberOfStories
	int32_t ___numberOfStories_12;
	// System.String[] Quest::stories
	StringU5BU5D_t1281789340* ___stories_13;
	// System.Boolean[] Quest::storiesCompleted
	BooleanU5BU5D_t2897418192* ___storiesCompleted_14;
	// System.Boolean Quest::isFinal
	bool ___isFinal_15;
	// CharacterBehavior Quest::characterBehavior
	CharacterBehavior_t2392107484 * ___characterBehavior_16;
	// SceneChanger Quest::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_17;
	// System.String Quest::sceneName
	String_t* ___sceneName_18;
	// System.String Quest::startText
	String_t* ___startText_19;
	// System.String Quest::completedText
	String_t* ___completedText_20;
	// System.String Quest::wrongText
	String_t* ___wrongText_21;

public:
	inline static int32_t get_offset_of_questID_2() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___questID_2)); }
	inline int32_t get_questID_2() const { return ___questID_2; }
	inline int32_t* get_address_of_questID_2() { return &___questID_2; }
	inline void set_questID_2(int32_t value)
	{
		___questID_2 = value;
	}

	inline static int32_t get_offset_of_manager_3() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___manager_3)); }
	inline QuestManager_t588401851 * get_manager_3() const { return ___manager_3; }
	inline QuestManager_t588401851 ** get_address_of_manager_3() { return &___manager_3; }
	inline void set_manager_3(QuestManager_t588401851 * value)
	{
		___manager_3 = value;
		Il2CppCodeGenWriteBarrier((&___manager_3), value);
	}

	inline static int32_t get_offset_of_isActivity_4() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isActivity_4)); }
	inline bool get_isActivity_4() const { return ___isActivity_4; }
	inline bool* get_address_of_isActivity_4() { return &___isActivity_4; }
	inline void set_isActivity_4(bool value)
	{
		___isActivity_4 = value;
	}

	inline static int32_t get_offset_of_textActivity_5() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___textActivity_5)); }
	inline StringU5BU5D_t1281789340* get_textActivity_5() const { return ___textActivity_5; }
	inline StringU5BU5D_t1281789340** get_address_of_textActivity_5() { return &___textActivity_5; }
	inline void set_textActivity_5(StringU5BU5D_t1281789340* value)
	{
		___textActivity_5 = value;
		Il2CppCodeGenWriteBarrier((&___textActivity_5), value);
	}

	inline static int32_t get_offset_of_isSelectMultiple_6() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isSelectMultiple_6)); }
	inline bool get_isSelectMultiple_6() const { return ___isSelectMultiple_6; }
	inline bool* get_address_of_isSelectMultiple_6() { return &___isSelectMultiple_6; }
	inline void set_isSelectMultiple_6(bool value)
	{
		___isSelectMultiple_6 = value;
	}

	inline static int32_t get_offset_of_numberOptions_7() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___numberOptions_7)); }
	inline int32_t get_numberOptions_7() const { return ___numberOptions_7; }
	inline int32_t* get_address_of_numberOptions_7() { return &___numberOptions_7; }
	inline void set_numberOptions_7(int32_t value)
	{
		___numberOptions_7 = value;
	}

	inline static int32_t get_offset_of_options_8() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___options_8)); }
	inline StringU5BU5D_t1281789340* get_options_8() const { return ___options_8; }
	inline StringU5BU5D_t1281789340** get_address_of_options_8() { return &___options_8; }
	inline void set_options_8(StringU5BU5D_t1281789340* value)
	{
		___options_8 = value;
		Il2CppCodeGenWriteBarrier((&___options_8), value);
	}

	inline static int32_t get_offset_of_rigthOption_9() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___rigthOption_9)); }
	inline int32_t get_rigthOption_9() const { return ___rigthOption_9; }
	inline int32_t* get_address_of_rigthOption_9() { return &___rigthOption_9; }
	inline void set_rigthOption_9(int32_t value)
	{
		___rigthOption_9 = value;
	}

	inline static int32_t get_offset_of_optionsSelected_10() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___optionsSelected_10)); }
	inline int32_t get_optionsSelected_10() const { return ___optionsSelected_10; }
	inline int32_t* get_address_of_optionsSelected_10() { return &___optionsSelected_10; }
	inline void set_optionsSelected_10(int32_t value)
	{
		___optionsSelected_10 = value;
	}

	inline static int32_t get_offset_of_isStory_11() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isStory_11)); }
	inline bool get_isStory_11() const { return ___isStory_11; }
	inline bool* get_address_of_isStory_11() { return &___isStory_11; }
	inline void set_isStory_11(bool value)
	{
		___isStory_11 = value;
	}

	inline static int32_t get_offset_of_numberOfStories_12() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___numberOfStories_12)); }
	inline int32_t get_numberOfStories_12() const { return ___numberOfStories_12; }
	inline int32_t* get_address_of_numberOfStories_12() { return &___numberOfStories_12; }
	inline void set_numberOfStories_12(int32_t value)
	{
		___numberOfStories_12 = value;
	}

	inline static int32_t get_offset_of_stories_13() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___stories_13)); }
	inline StringU5BU5D_t1281789340* get_stories_13() const { return ___stories_13; }
	inline StringU5BU5D_t1281789340** get_address_of_stories_13() { return &___stories_13; }
	inline void set_stories_13(StringU5BU5D_t1281789340* value)
	{
		___stories_13 = value;
		Il2CppCodeGenWriteBarrier((&___stories_13), value);
	}

	inline static int32_t get_offset_of_storiesCompleted_14() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___storiesCompleted_14)); }
	inline BooleanU5BU5D_t2897418192* get_storiesCompleted_14() const { return ___storiesCompleted_14; }
	inline BooleanU5BU5D_t2897418192** get_address_of_storiesCompleted_14() { return &___storiesCompleted_14; }
	inline void set_storiesCompleted_14(BooleanU5BU5D_t2897418192* value)
	{
		___storiesCompleted_14 = value;
		Il2CppCodeGenWriteBarrier((&___storiesCompleted_14), value);
	}

	inline static int32_t get_offset_of_isFinal_15() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___isFinal_15)); }
	inline bool get_isFinal_15() const { return ___isFinal_15; }
	inline bool* get_address_of_isFinal_15() { return &___isFinal_15; }
	inline void set_isFinal_15(bool value)
	{
		___isFinal_15 = value;
	}

	inline static int32_t get_offset_of_characterBehavior_16() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___characterBehavior_16)); }
	inline CharacterBehavior_t2392107484 * get_characterBehavior_16() const { return ___characterBehavior_16; }
	inline CharacterBehavior_t2392107484 ** get_address_of_characterBehavior_16() { return &___characterBehavior_16; }
	inline void set_characterBehavior_16(CharacterBehavior_t2392107484 * value)
	{
		___characterBehavior_16 = value;
		Il2CppCodeGenWriteBarrier((&___characterBehavior_16), value);
	}

	inline static int32_t get_offset_of_sceneChanger_17() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___sceneChanger_17)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_17() const { return ___sceneChanger_17; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_17() { return &___sceneChanger_17; }
	inline void set_sceneChanger_17(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_17), value);
	}

	inline static int32_t get_offset_of_sceneName_18() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___sceneName_18)); }
	inline String_t* get_sceneName_18() const { return ___sceneName_18; }
	inline String_t** get_address_of_sceneName_18() { return &___sceneName_18; }
	inline void set_sceneName_18(String_t* value)
	{
		___sceneName_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_18), value);
	}

	inline static int32_t get_offset_of_startText_19() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___startText_19)); }
	inline String_t* get_startText_19() const { return ___startText_19; }
	inline String_t** get_address_of_startText_19() { return &___startText_19; }
	inline void set_startText_19(String_t* value)
	{
		___startText_19 = value;
		Il2CppCodeGenWriteBarrier((&___startText_19), value);
	}

	inline static int32_t get_offset_of_completedText_20() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___completedText_20)); }
	inline String_t* get_completedText_20() const { return ___completedText_20; }
	inline String_t** get_address_of_completedText_20() { return &___completedText_20; }
	inline void set_completedText_20(String_t* value)
	{
		___completedText_20 = value;
		Il2CppCodeGenWriteBarrier((&___completedText_20), value);
	}

	inline static int32_t get_offset_of_wrongText_21() { return static_cast<int32_t>(offsetof(Quest_t3696879532, ___wrongText_21)); }
	inline String_t* get_wrongText_21() const { return ___wrongText_21; }
	inline String_t** get_address_of_wrongText_21() { return &___wrongText_21; }
	inline void set_wrongText_21(String_t* value)
	{
		___wrongText_21 = value;
		Il2CppCodeGenWriteBarrier((&___wrongText_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEST_T3696879532_H
#ifndef UIPUZZLETOKEN_T1662105885_H
#define UIPUZZLETOKEN_T1662105885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleToken
struct  UIPuzzleToken_t1662105885  : public MonoBehaviour_t3962482529
{
public:
	// EPuzzleToken UIPuzzleToken::token
	int32_t ___token_2;
	// System.Single UIPuzzleToken::moveSpeed
	float ___moveSpeed_3;
	// System.Boolean UIPuzzleToken::<IsGrabbable>k__BackingField
	bool ___U3CIsGrabbableU3Ek__BackingField_4;
	// System.Boolean UIPuzzleToken::matched
	bool ___matched_5;
	// UnityEngine.Vector3 UIPuzzleToken::<StartPosition>k__BackingField
	Vector3_t3722313464  ___U3CStartPositionU3Ek__BackingField_6;
	// UnityEngine.RectTransform UIPuzzleToken::rectTransform
	RectTransform_t3704657025 * ___rectTransform_8;
	// UnityEngine.Vector2 UIPuzzleToken::mousePosition
	Vector2_t2156229523  ___mousePosition_9;
	// System.Boolean UIPuzzleToken::restart
	bool ___restart_10;
	// System.Single UIPuzzleToken::deltaTime
	float ___deltaTime_11;
	// UnityEngine.Vector3 UIPuzzleToken::position
	Vector3_t3722313464  ___position_12;
	// System.Boolean UIPuzzleToken::dragged
	bool ___dragged_13;
	// UnityEngine.UI.Image UIPuzzleToken::image
	Image_t2670269651 * ___image_14;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___token_2)); }
	inline int32_t get_token_2() const { return ___token_2; }
	inline int32_t* get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(int32_t value)
	{
		___token_2 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_3() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___moveSpeed_3)); }
	inline float get_moveSpeed_3() const { return ___moveSpeed_3; }
	inline float* get_address_of_moveSpeed_3() { return &___moveSpeed_3; }
	inline void set_moveSpeed_3(float value)
	{
		___moveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsGrabbableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___U3CIsGrabbableU3Ek__BackingField_4)); }
	inline bool get_U3CIsGrabbableU3Ek__BackingField_4() const { return ___U3CIsGrabbableU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsGrabbableU3Ek__BackingField_4() { return &___U3CIsGrabbableU3Ek__BackingField_4; }
	inline void set_U3CIsGrabbableU3Ek__BackingField_4(bool value)
	{
		___U3CIsGrabbableU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_matched_5() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___matched_5)); }
	inline bool get_matched_5() const { return ___matched_5; }
	inline bool* get_address_of_matched_5() { return &___matched_5; }
	inline void set_matched_5(bool value)
	{
		___matched_5 = value;
	}

	inline static int32_t get_offset_of_U3CStartPositionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___U3CStartPositionU3Ek__BackingField_6)); }
	inline Vector3_t3722313464  get_U3CStartPositionU3Ek__BackingField_6() const { return ___U3CStartPositionU3Ek__BackingField_6; }
	inline Vector3_t3722313464 * get_address_of_U3CStartPositionU3Ek__BackingField_6() { return &___U3CStartPositionU3Ek__BackingField_6; }
	inline void set_U3CStartPositionU3Ek__BackingField_6(Vector3_t3722313464  value)
	{
		___U3CStartPositionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_rectTransform_8() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___rectTransform_8)); }
	inline RectTransform_t3704657025 * get_rectTransform_8() const { return ___rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_8() { return &___rectTransform_8; }
	inline void set_rectTransform_8(RectTransform_t3704657025 * value)
	{
		___rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_8), value);
	}

	inline static int32_t get_offset_of_mousePosition_9() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___mousePosition_9)); }
	inline Vector2_t2156229523  get_mousePosition_9() const { return ___mousePosition_9; }
	inline Vector2_t2156229523 * get_address_of_mousePosition_9() { return &___mousePosition_9; }
	inline void set_mousePosition_9(Vector2_t2156229523  value)
	{
		___mousePosition_9 = value;
	}

	inline static int32_t get_offset_of_restart_10() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___restart_10)); }
	inline bool get_restart_10() const { return ___restart_10; }
	inline bool* get_address_of_restart_10() { return &___restart_10; }
	inline void set_restart_10(bool value)
	{
		___restart_10 = value;
	}

	inline static int32_t get_offset_of_deltaTime_11() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___deltaTime_11)); }
	inline float get_deltaTime_11() const { return ___deltaTime_11; }
	inline float* get_address_of_deltaTime_11() { return &___deltaTime_11; }
	inline void set_deltaTime_11(float value)
	{
		___deltaTime_11 = value;
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___position_12)); }
	inline Vector3_t3722313464  get_position_12() const { return ___position_12; }
	inline Vector3_t3722313464 * get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(Vector3_t3722313464  value)
	{
		___position_12 = value;
	}

	inline static int32_t get_offset_of_dragged_13() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___dragged_13)); }
	inline bool get_dragged_13() const { return ___dragged_13; }
	inline bool* get_address_of_dragged_13() { return &___dragged_13; }
	inline void set_dragged_13(bool value)
	{
		___dragged_13 = value;
	}

	inline static int32_t get_offset_of_image_14() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885, ___image_14)); }
	inline Image_t2670269651 * get_image_14() const { return ___image_14; }
	inline Image_t2670269651 ** get_address_of_image_14() { return &___image_14; }
	inline void set_image_14(Image_t2670269651 * value)
	{
		___image_14 = value;
		Il2CppCodeGenWriteBarrier((&___image_14), value);
	}
};

struct UIPuzzleToken_t1662105885_StaticFields
{
public:
	// UIPuzzleToken/PuzzleTokenEventHandler UIPuzzleToken::OnDragEvent
	PuzzleTokenEventHandler_t3807414068 * ___OnDragEvent_7;

public:
	inline static int32_t get_offset_of_OnDragEvent_7() { return static_cast<int32_t>(offsetof(UIPuzzleToken_t1662105885_StaticFields, ___OnDragEvent_7)); }
	inline PuzzleTokenEventHandler_t3807414068 * get_OnDragEvent_7() const { return ___OnDragEvent_7; }
	inline PuzzleTokenEventHandler_t3807414068 ** get_address_of_OnDragEvent_7() { return &___OnDragEvent_7; }
	inline void set_OnDragEvent_7(PuzzleTokenEventHandler_t3807414068 * value)
	{
		___OnDragEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnDragEvent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPUZZLETOKEN_T1662105885_H
#ifndef QUESTTRIGGER_T3125687900_H
#define QUESTTRIGGER_T3125687900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestTrigger
struct  QuestTrigger_t3125687900  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager QuestTrigger::manager
	QuestManager_t588401851 * ___manager_2;
	// System.Int32 QuestTrigger::questID
	int32_t ___questID_3;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(QuestTrigger_t3125687900, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_questID_3() { return static_cast<int32_t>(offsetof(QuestTrigger_t3125687900, ___questID_3)); }
	inline int32_t get_questID_3() const { return ___questID_3; }
	inline int32_t* get_address_of_questID_3() { return &___questID_3; }
	inline void set_questID_3(int32_t value)
	{
		___questID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTTRIGGER_T3125687900_H
#ifndef GAMECONTROLLER_T2330501625_H
#define GAMECONTROLLER_T2330501625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t2330501625  : public MonoBehaviour_t3962482529
{
public:
	// DataController GameController::dataController
	DataController_t353634109 * ___dataController_2;
	// SceneChanger GameController::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_3;
	// QuestManager GameController::manager
	QuestManager_t588401851 * ___manager_4;
	// CharacterBehavior GameController::character
	CharacterBehavior_t2392107484 * ___character_5;

public:
	inline static int32_t get_offset_of_dataController_2() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___dataController_2)); }
	inline DataController_t353634109 * get_dataController_2() const { return ___dataController_2; }
	inline DataController_t353634109 ** get_address_of_dataController_2() { return &___dataController_2; }
	inline void set_dataController_2(DataController_t353634109 * value)
	{
		___dataController_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_2), value);
	}

	inline static int32_t get_offset_of_sceneChanger_3() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___sceneChanger_3)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_3() const { return ___sceneChanger_3; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_3() { return &___sceneChanger_3; }
	inline void set_sceneChanger_3(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_3 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_3), value);
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___manager_4)); }
	inline QuestManager_t588401851 * get_manager_4() const { return ___manager_4; }
	inline QuestManager_t588401851 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(QuestManager_t588401851 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}

	inline static int32_t get_offset_of_character_5() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___character_5)); }
	inline CharacterBehavior_t2392107484 * get_character_5() const { return ___character_5; }
	inline CharacterBehavior_t2392107484 ** get_address_of_character_5() { return &___character_5; }
	inline void set_character_5(CharacterBehavior_t2392107484 * value)
	{
		___character_5 = value;
		Il2CppCodeGenWriteBarrier((&___character_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROLLER_T2330501625_H
#ifndef UTILTHREAD_T4012287602_H
#define UTILTHREAD_T4012287602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util.UtilThread
struct  UtilThread_t4012287602  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILTHREAD_T4012287602_H
#ifndef AUDIORECORDMANAGER_T3166594816_H
#define AUDIORECORDMANAGER_T3166594816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecordManager
struct  AudioRecordManager_t3166594816  : public MonoBehaviour_t3962482529
{
public:
	// EAudioSamplingRate AudioRecordManager::samplingRate
	int32_t ___samplingRate_2;
	// System.Int32 AudioRecordManager::sampleRate
	int32_t ___sampleRate_3;
	// UnityEngine.AudioSource AudioRecordManager::_audioSource
	AudioSource_t3935305588 * ____audioSource_4;
	// System.Int32 AudioRecordManager::audioLength
	int32_t ___audioLength_5;
	// UnityEngine.AudioClip AudioRecordManager::_audioClip
	AudioClip_t3680889665 * ____audioClip_6;
	// System.Boolean AudioRecordManager::useMicrophone
	bool ___useMicrophone_7;
	// System.String AudioRecordManager::_selectDevice
	String_t* ____selectDevice_8;
	// System.String AudioRecordManager::fileName
	String_t* ___fileName_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.AudioType,EAudioFileExtension> AudioRecordManager::audioExtensions
	Dictionary_2_t4114977226 * ___audioExtensions_10;
	// UnityEngine.AudioType AudioRecordManager::selectedAudioType
	int32_t ___selectedAudioType_11;
	// System.String AudioRecordManager::_persistentDataPath
	String_t* ____persistentDataPath_12;
	// UnityEngine.Audio.AudioMixerGroup AudioRecordManager::_mixerGroupMicrophone
	AudioMixerGroup_t2743564464 * ____mixerGroupMicrophone_13;
	// UnityEngine.Audio.AudioMixerGroup AudioRecordManager::_mixerGroupMaster
	AudioMixerGroup_t2743564464 * ____mixerGroupMaster_14;
	// UnityEngine.AudioConfiguration AudioRecordManager::_audioConfiguration
	AudioConfiguration_t4040042187  ____audioConfiguration_15;
	// NAudio.Example.MP3EncoderWrapper AudioRecordManager::_mp3EncoderWrapper
	MP3EncoderWrapper_t279410081 * ____mp3EncoderWrapper_16;
	// EAudioChannel AudioRecordManager::channel
	int32_t ___channel_17;
	// System.Int32 AudioRecordManager::min
	int32_t ___min_18;
	// System.Int32 AudioRecordManager::max
	int32_t ___max_19;
	// System.Single AudioRecordManager::elapsedTime
	float ___elapsedTime_20;
	// System.Single AudioRecordManager::timer
	float ___timer_21;
	// System.Int32 AudioRecordManager::maxRecordTime
	int32_t ___maxRecordTime_22;
	// System.Boolean AudioRecordManager::isRecording
	bool ___isRecording_23;
	// System.Single AudioRecordManager::startRecordTime
	float ___startRecordTime_24;
	// System.Int32 AudioRecordManager::timeSpan
	int32_t ___timeSpan_25;
	// System.Action AudioRecordManager::OnStartEncoding
	Action_t1264377477 * ___OnStartEncoding_26;
	// System.Action AudioRecordManager::OnStopEncoding
	Action_t1264377477 * ___OnStopEncoding_27;
	// QuestManager AudioRecordManager::manager
	QuestManager_t588401851 * ___manager_28;
	// Quest AudioRecordManager::quest
	Quest_t3696879532 * ___quest_29;
	// UnityEngine.UI.Text AudioRecordManager::HistoryText
	Text_t1901882714 * ___HistoryText_30;
	// System.String AudioRecordManager::<ElapsedTimeTextWithFormat>k__BackingField
	String_t* ___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31;
	// System.String AudioRecordManager::<TimerTextWithFormat>k__BackingField
	String_t* ___U3CTimerTextWithFormatU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_samplingRate_2() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___samplingRate_2)); }
	inline int32_t get_samplingRate_2() const { return ___samplingRate_2; }
	inline int32_t* get_address_of_samplingRate_2() { return &___samplingRate_2; }
	inline void set_samplingRate_2(int32_t value)
	{
		___samplingRate_2 = value;
	}

	inline static int32_t get_offset_of_sampleRate_3() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___sampleRate_3)); }
	inline int32_t get_sampleRate_3() const { return ___sampleRate_3; }
	inline int32_t* get_address_of_sampleRate_3() { return &___sampleRate_3; }
	inline void set_sampleRate_3(int32_t value)
	{
		___sampleRate_3 = value;
	}

	inline static int32_t get_offset_of__audioSource_4() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____audioSource_4)); }
	inline AudioSource_t3935305588 * get__audioSource_4() const { return ____audioSource_4; }
	inline AudioSource_t3935305588 ** get_address_of__audioSource_4() { return &____audioSource_4; }
	inline void set__audioSource_4(AudioSource_t3935305588 * value)
	{
		____audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&____audioSource_4), value);
	}

	inline static int32_t get_offset_of_audioLength_5() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___audioLength_5)); }
	inline int32_t get_audioLength_5() const { return ___audioLength_5; }
	inline int32_t* get_address_of_audioLength_5() { return &___audioLength_5; }
	inline void set_audioLength_5(int32_t value)
	{
		___audioLength_5 = value;
	}

	inline static int32_t get_offset_of__audioClip_6() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____audioClip_6)); }
	inline AudioClip_t3680889665 * get__audioClip_6() const { return ____audioClip_6; }
	inline AudioClip_t3680889665 ** get_address_of__audioClip_6() { return &____audioClip_6; }
	inline void set__audioClip_6(AudioClip_t3680889665 * value)
	{
		____audioClip_6 = value;
		Il2CppCodeGenWriteBarrier((&____audioClip_6), value);
	}

	inline static int32_t get_offset_of_useMicrophone_7() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___useMicrophone_7)); }
	inline bool get_useMicrophone_7() const { return ___useMicrophone_7; }
	inline bool* get_address_of_useMicrophone_7() { return &___useMicrophone_7; }
	inline void set_useMicrophone_7(bool value)
	{
		___useMicrophone_7 = value;
	}

	inline static int32_t get_offset_of__selectDevice_8() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____selectDevice_8)); }
	inline String_t* get__selectDevice_8() const { return ____selectDevice_8; }
	inline String_t** get_address_of__selectDevice_8() { return &____selectDevice_8; }
	inline void set__selectDevice_8(String_t* value)
	{
		____selectDevice_8 = value;
		Il2CppCodeGenWriteBarrier((&____selectDevice_8), value);
	}

	inline static int32_t get_offset_of_fileName_9() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___fileName_9)); }
	inline String_t* get_fileName_9() const { return ___fileName_9; }
	inline String_t** get_address_of_fileName_9() { return &___fileName_9; }
	inline void set_fileName_9(String_t* value)
	{
		___fileName_9 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_9), value);
	}

	inline static int32_t get_offset_of_audioExtensions_10() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___audioExtensions_10)); }
	inline Dictionary_2_t4114977226 * get_audioExtensions_10() const { return ___audioExtensions_10; }
	inline Dictionary_2_t4114977226 ** get_address_of_audioExtensions_10() { return &___audioExtensions_10; }
	inline void set_audioExtensions_10(Dictionary_2_t4114977226 * value)
	{
		___audioExtensions_10 = value;
		Il2CppCodeGenWriteBarrier((&___audioExtensions_10), value);
	}

	inline static int32_t get_offset_of_selectedAudioType_11() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___selectedAudioType_11)); }
	inline int32_t get_selectedAudioType_11() const { return ___selectedAudioType_11; }
	inline int32_t* get_address_of_selectedAudioType_11() { return &___selectedAudioType_11; }
	inline void set_selectedAudioType_11(int32_t value)
	{
		___selectedAudioType_11 = value;
	}

	inline static int32_t get_offset_of__persistentDataPath_12() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____persistentDataPath_12)); }
	inline String_t* get__persistentDataPath_12() const { return ____persistentDataPath_12; }
	inline String_t** get_address_of__persistentDataPath_12() { return &____persistentDataPath_12; }
	inline void set__persistentDataPath_12(String_t* value)
	{
		____persistentDataPath_12 = value;
		Il2CppCodeGenWriteBarrier((&____persistentDataPath_12), value);
	}

	inline static int32_t get_offset_of__mixerGroupMicrophone_13() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____mixerGroupMicrophone_13)); }
	inline AudioMixerGroup_t2743564464 * get__mixerGroupMicrophone_13() const { return ____mixerGroupMicrophone_13; }
	inline AudioMixerGroup_t2743564464 ** get_address_of__mixerGroupMicrophone_13() { return &____mixerGroupMicrophone_13; }
	inline void set__mixerGroupMicrophone_13(AudioMixerGroup_t2743564464 * value)
	{
		____mixerGroupMicrophone_13 = value;
		Il2CppCodeGenWriteBarrier((&____mixerGroupMicrophone_13), value);
	}

	inline static int32_t get_offset_of__mixerGroupMaster_14() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____mixerGroupMaster_14)); }
	inline AudioMixerGroup_t2743564464 * get__mixerGroupMaster_14() const { return ____mixerGroupMaster_14; }
	inline AudioMixerGroup_t2743564464 ** get_address_of__mixerGroupMaster_14() { return &____mixerGroupMaster_14; }
	inline void set__mixerGroupMaster_14(AudioMixerGroup_t2743564464 * value)
	{
		____mixerGroupMaster_14 = value;
		Il2CppCodeGenWriteBarrier((&____mixerGroupMaster_14), value);
	}

	inline static int32_t get_offset_of__audioConfiguration_15() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____audioConfiguration_15)); }
	inline AudioConfiguration_t4040042187  get__audioConfiguration_15() const { return ____audioConfiguration_15; }
	inline AudioConfiguration_t4040042187 * get_address_of__audioConfiguration_15() { return &____audioConfiguration_15; }
	inline void set__audioConfiguration_15(AudioConfiguration_t4040042187  value)
	{
		____audioConfiguration_15 = value;
	}

	inline static int32_t get_offset_of__mp3EncoderWrapper_16() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ____mp3EncoderWrapper_16)); }
	inline MP3EncoderWrapper_t279410081 * get__mp3EncoderWrapper_16() const { return ____mp3EncoderWrapper_16; }
	inline MP3EncoderWrapper_t279410081 ** get_address_of__mp3EncoderWrapper_16() { return &____mp3EncoderWrapper_16; }
	inline void set__mp3EncoderWrapper_16(MP3EncoderWrapper_t279410081 * value)
	{
		____mp3EncoderWrapper_16 = value;
		Il2CppCodeGenWriteBarrier((&____mp3EncoderWrapper_16), value);
	}

	inline static int32_t get_offset_of_channel_17() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___channel_17)); }
	inline int32_t get_channel_17() const { return ___channel_17; }
	inline int32_t* get_address_of_channel_17() { return &___channel_17; }
	inline void set_channel_17(int32_t value)
	{
		___channel_17 = value;
	}

	inline static int32_t get_offset_of_min_18() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___min_18)); }
	inline int32_t get_min_18() const { return ___min_18; }
	inline int32_t* get_address_of_min_18() { return &___min_18; }
	inline void set_min_18(int32_t value)
	{
		___min_18 = value;
	}

	inline static int32_t get_offset_of_max_19() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___max_19)); }
	inline int32_t get_max_19() const { return ___max_19; }
	inline int32_t* get_address_of_max_19() { return &___max_19; }
	inline void set_max_19(int32_t value)
	{
		___max_19 = value;
	}

	inline static int32_t get_offset_of_elapsedTime_20() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___elapsedTime_20)); }
	inline float get_elapsedTime_20() const { return ___elapsedTime_20; }
	inline float* get_address_of_elapsedTime_20() { return &___elapsedTime_20; }
	inline void set_elapsedTime_20(float value)
	{
		___elapsedTime_20 = value;
	}

	inline static int32_t get_offset_of_timer_21() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___timer_21)); }
	inline float get_timer_21() const { return ___timer_21; }
	inline float* get_address_of_timer_21() { return &___timer_21; }
	inline void set_timer_21(float value)
	{
		___timer_21 = value;
	}

	inline static int32_t get_offset_of_maxRecordTime_22() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___maxRecordTime_22)); }
	inline int32_t get_maxRecordTime_22() const { return ___maxRecordTime_22; }
	inline int32_t* get_address_of_maxRecordTime_22() { return &___maxRecordTime_22; }
	inline void set_maxRecordTime_22(int32_t value)
	{
		___maxRecordTime_22 = value;
	}

	inline static int32_t get_offset_of_isRecording_23() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___isRecording_23)); }
	inline bool get_isRecording_23() const { return ___isRecording_23; }
	inline bool* get_address_of_isRecording_23() { return &___isRecording_23; }
	inline void set_isRecording_23(bool value)
	{
		___isRecording_23 = value;
	}

	inline static int32_t get_offset_of_startRecordTime_24() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___startRecordTime_24)); }
	inline float get_startRecordTime_24() const { return ___startRecordTime_24; }
	inline float* get_address_of_startRecordTime_24() { return &___startRecordTime_24; }
	inline void set_startRecordTime_24(float value)
	{
		___startRecordTime_24 = value;
	}

	inline static int32_t get_offset_of_timeSpan_25() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___timeSpan_25)); }
	inline int32_t get_timeSpan_25() const { return ___timeSpan_25; }
	inline int32_t* get_address_of_timeSpan_25() { return &___timeSpan_25; }
	inline void set_timeSpan_25(int32_t value)
	{
		___timeSpan_25 = value;
	}

	inline static int32_t get_offset_of_OnStartEncoding_26() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___OnStartEncoding_26)); }
	inline Action_t1264377477 * get_OnStartEncoding_26() const { return ___OnStartEncoding_26; }
	inline Action_t1264377477 ** get_address_of_OnStartEncoding_26() { return &___OnStartEncoding_26; }
	inline void set_OnStartEncoding_26(Action_t1264377477 * value)
	{
		___OnStartEncoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnStartEncoding_26), value);
	}

	inline static int32_t get_offset_of_OnStopEncoding_27() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___OnStopEncoding_27)); }
	inline Action_t1264377477 * get_OnStopEncoding_27() const { return ___OnStopEncoding_27; }
	inline Action_t1264377477 ** get_address_of_OnStopEncoding_27() { return &___OnStopEncoding_27; }
	inline void set_OnStopEncoding_27(Action_t1264377477 * value)
	{
		___OnStopEncoding_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnStopEncoding_27), value);
	}

	inline static int32_t get_offset_of_manager_28() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___manager_28)); }
	inline QuestManager_t588401851 * get_manager_28() const { return ___manager_28; }
	inline QuestManager_t588401851 ** get_address_of_manager_28() { return &___manager_28; }
	inline void set_manager_28(QuestManager_t588401851 * value)
	{
		___manager_28 = value;
		Il2CppCodeGenWriteBarrier((&___manager_28), value);
	}

	inline static int32_t get_offset_of_quest_29() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___quest_29)); }
	inline Quest_t3696879532 * get_quest_29() const { return ___quest_29; }
	inline Quest_t3696879532 ** get_address_of_quest_29() { return &___quest_29; }
	inline void set_quest_29(Quest_t3696879532 * value)
	{
		___quest_29 = value;
		Il2CppCodeGenWriteBarrier((&___quest_29), value);
	}

	inline static int32_t get_offset_of_HistoryText_30() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___HistoryText_30)); }
	inline Text_t1901882714 * get_HistoryText_30() const { return ___HistoryText_30; }
	inline Text_t1901882714 ** get_address_of_HistoryText_30() { return &___HistoryText_30; }
	inline void set_HistoryText_30(Text_t1901882714 * value)
	{
		___HistoryText_30 = value;
		Il2CppCodeGenWriteBarrier((&___HistoryText_30), value);
	}

	inline static int32_t get_offset_of_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31)); }
	inline String_t* get_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31() const { return ___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31; }
	inline String_t** get_address_of_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31() { return &___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31; }
	inline void set_U3CElapsedTimeTextWithFormatU3Ek__BackingField_31(String_t* value)
	{
		___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CElapsedTimeTextWithFormatU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CTimerTextWithFormatU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(AudioRecordManager_t3166594816, ___U3CTimerTextWithFormatU3Ek__BackingField_32)); }
	inline String_t* get_U3CTimerTextWithFormatU3Ek__BackingField_32() const { return ___U3CTimerTextWithFormatU3Ek__BackingField_32; }
	inline String_t** get_address_of_U3CTimerTextWithFormatU3Ek__BackingField_32() { return &___U3CTimerTextWithFormatU3Ek__BackingField_32; }
	inline void set_U3CTimerTextWithFormatU3Ek__BackingField_32(String_t* value)
	{
		___U3CTimerTextWithFormatU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTimerTextWithFormatU3Ek__BackingField_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIORECORDMANAGER_T3166594816_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef UIPUZZLECONTROLLER_T2836468369_H
#define UIPUZZLECONTROLLER_T2836468369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleController
struct  UIPuzzleController_t2836468369  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UIPuzzleController::images
	List_1_t4142344393 * ___images_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIPuzzleController::imagePositions
	List_1_t899420910 * ___imagePositions_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIPuzzleController::startImgPositions
	List_1_t899420910 * ___startImgPositions_5;
	// System.Collections.Generic.List`1<EPuzzleToken> UIPuzzleController::tokenOrder
	List_1_t3937307605 * ___tokenOrder_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UIPuzzleController::tokenImages
	List_1_t4142344393 * ___tokenImages_7;
	// System.Collections.Generic.List`1<UIPuzzleToken> UIPuzzleController::tokens
	List_1_t3134180627 * ___tokens_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIPuzzleController::tokenPositions
	List_1_t899420910 * ___tokenPositions_9;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UIPuzzleController::insertImages
	List_1_t4142344393 * ___insertImages_10;
	// System.Single UIPuzzleController::showTime
	float ___showTime_11;
	// System.Single UIPuzzleController::startTime
	float ___startTime_12;
	// System.Single UIPuzzleController::countdownTime
	float ___countdownTime_13;
	// UnityEngine.GameObject UIPuzzleController::introPanel
	GameObject_t1113636619 * ___introPanel_14;
	// UnityEngine.GameObject UIPuzzleController::puzzlePanel
	GameObject_t1113636619 * ___puzzlePanel_15;
	// UnityEngine.GameObject UIPuzzleController::congratsMessage
	GameObject_t1113636619 * ___congratsMessage_16;
	// UnityEngine.UI.Text UIPuzzleController::countdownText
	Text_t1901882714 * ___countdownText_17;
	// System.Boolean UIPuzzleController::bInit
	bool ___bInit_18;
	// System.Boolean UIPuzzleController::bTokenMatched
	bool ___bTokenMatched_19;
	// UnityEngine.Vector2 UIPuzzleController::matchedPosition
	Vector2_t2156229523  ___matchedPosition_20;
	// UIPuzzleToken UIPuzzleController::CurrentToken
	UIPuzzleToken_t1662105885 * ___CurrentToken_21;
	// System.Int32 UIPuzzleController::matchCount
	int32_t ___matchCount_22;
	// QuestManager UIPuzzleController::manager
	QuestManager_t588401851 * ___manager_24;

public:
	inline static int32_t get_offset_of_images_3() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___images_3)); }
	inline List_1_t4142344393 * get_images_3() const { return ___images_3; }
	inline List_1_t4142344393 ** get_address_of_images_3() { return &___images_3; }
	inline void set_images_3(List_1_t4142344393 * value)
	{
		___images_3 = value;
		Il2CppCodeGenWriteBarrier((&___images_3), value);
	}

	inline static int32_t get_offset_of_imagePositions_4() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___imagePositions_4)); }
	inline List_1_t899420910 * get_imagePositions_4() const { return ___imagePositions_4; }
	inline List_1_t899420910 ** get_address_of_imagePositions_4() { return &___imagePositions_4; }
	inline void set_imagePositions_4(List_1_t899420910 * value)
	{
		___imagePositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___imagePositions_4), value);
	}

	inline static int32_t get_offset_of_startImgPositions_5() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___startImgPositions_5)); }
	inline List_1_t899420910 * get_startImgPositions_5() const { return ___startImgPositions_5; }
	inline List_1_t899420910 ** get_address_of_startImgPositions_5() { return &___startImgPositions_5; }
	inline void set_startImgPositions_5(List_1_t899420910 * value)
	{
		___startImgPositions_5 = value;
		Il2CppCodeGenWriteBarrier((&___startImgPositions_5), value);
	}

	inline static int32_t get_offset_of_tokenOrder_6() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokenOrder_6)); }
	inline List_1_t3937307605 * get_tokenOrder_6() const { return ___tokenOrder_6; }
	inline List_1_t3937307605 ** get_address_of_tokenOrder_6() { return &___tokenOrder_6; }
	inline void set_tokenOrder_6(List_1_t3937307605 * value)
	{
		___tokenOrder_6 = value;
		Il2CppCodeGenWriteBarrier((&___tokenOrder_6), value);
	}

	inline static int32_t get_offset_of_tokenImages_7() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokenImages_7)); }
	inline List_1_t4142344393 * get_tokenImages_7() const { return ___tokenImages_7; }
	inline List_1_t4142344393 ** get_address_of_tokenImages_7() { return &___tokenImages_7; }
	inline void set_tokenImages_7(List_1_t4142344393 * value)
	{
		___tokenImages_7 = value;
		Il2CppCodeGenWriteBarrier((&___tokenImages_7), value);
	}

	inline static int32_t get_offset_of_tokens_8() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokens_8)); }
	inline List_1_t3134180627 * get_tokens_8() const { return ___tokens_8; }
	inline List_1_t3134180627 ** get_address_of_tokens_8() { return &___tokens_8; }
	inline void set_tokens_8(List_1_t3134180627 * value)
	{
		___tokens_8 = value;
		Il2CppCodeGenWriteBarrier((&___tokens_8), value);
	}

	inline static int32_t get_offset_of_tokenPositions_9() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___tokenPositions_9)); }
	inline List_1_t899420910 * get_tokenPositions_9() const { return ___tokenPositions_9; }
	inline List_1_t899420910 ** get_address_of_tokenPositions_9() { return &___tokenPositions_9; }
	inline void set_tokenPositions_9(List_1_t899420910 * value)
	{
		___tokenPositions_9 = value;
		Il2CppCodeGenWriteBarrier((&___tokenPositions_9), value);
	}

	inline static int32_t get_offset_of_insertImages_10() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___insertImages_10)); }
	inline List_1_t4142344393 * get_insertImages_10() const { return ___insertImages_10; }
	inline List_1_t4142344393 ** get_address_of_insertImages_10() { return &___insertImages_10; }
	inline void set_insertImages_10(List_1_t4142344393 * value)
	{
		___insertImages_10 = value;
		Il2CppCodeGenWriteBarrier((&___insertImages_10), value);
	}

	inline static int32_t get_offset_of_showTime_11() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___showTime_11)); }
	inline float get_showTime_11() const { return ___showTime_11; }
	inline float* get_address_of_showTime_11() { return &___showTime_11; }
	inline void set_showTime_11(float value)
	{
		___showTime_11 = value;
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___startTime_12)); }
	inline float get_startTime_12() const { return ___startTime_12; }
	inline float* get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(float value)
	{
		___startTime_12 = value;
	}

	inline static int32_t get_offset_of_countdownTime_13() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___countdownTime_13)); }
	inline float get_countdownTime_13() const { return ___countdownTime_13; }
	inline float* get_address_of_countdownTime_13() { return &___countdownTime_13; }
	inline void set_countdownTime_13(float value)
	{
		___countdownTime_13 = value;
	}

	inline static int32_t get_offset_of_introPanel_14() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___introPanel_14)); }
	inline GameObject_t1113636619 * get_introPanel_14() const { return ___introPanel_14; }
	inline GameObject_t1113636619 ** get_address_of_introPanel_14() { return &___introPanel_14; }
	inline void set_introPanel_14(GameObject_t1113636619 * value)
	{
		___introPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___introPanel_14), value);
	}

	inline static int32_t get_offset_of_puzzlePanel_15() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___puzzlePanel_15)); }
	inline GameObject_t1113636619 * get_puzzlePanel_15() const { return ___puzzlePanel_15; }
	inline GameObject_t1113636619 ** get_address_of_puzzlePanel_15() { return &___puzzlePanel_15; }
	inline void set_puzzlePanel_15(GameObject_t1113636619 * value)
	{
		___puzzlePanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___puzzlePanel_15), value);
	}

	inline static int32_t get_offset_of_congratsMessage_16() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___congratsMessage_16)); }
	inline GameObject_t1113636619 * get_congratsMessage_16() const { return ___congratsMessage_16; }
	inline GameObject_t1113636619 ** get_address_of_congratsMessage_16() { return &___congratsMessage_16; }
	inline void set_congratsMessage_16(GameObject_t1113636619 * value)
	{
		___congratsMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___congratsMessage_16), value);
	}

	inline static int32_t get_offset_of_countdownText_17() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___countdownText_17)); }
	inline Text_t1901882714 * get_countdownText_17() const { return ___countdownText_17; }
	inline Text_t1901882714 ** get_address_of_countdownText_17() { return &___countdownText_17; }
	inline void set_countdownText_17(Text_t1901882714 * value)
	{
		___countdownText_17 = value;
		Il2CppCodeGenWriteBarrier((&___countdownText_17), value);
	}

	inline static int32_t get_offset_of_bInit_18() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___bInit_18)); }
	inline bool get_bInit_18() const { return ___bInit_18; }
	inline bool* get_address_of_bInit_18() { return &___bInit_18; }
	inline void set_bInit_18(bool value)
	{
		___bInit_18 = value;
	}

	inline static int32_t get_offset_of_bTokenMatched_19() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___bTokenMatched_19)); }
	inline bool get_bTokenMatched_19() const { return ___bTokenMatched_19; }
	inline bool* get_address_of_bTokenMatched_19() { return &___bTokenMatched_19; }
	inline void set_bTokenMatched_19(bool value)
	{
		___bTokenMatched_19 = value;
	}

	inline static int32_t get_offset_of_matchedPosition_20() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___matchedPosition_20)); }
	inline Vector2_t2156229523  get_matchedPosition_20() const { return ___matchedPosition_20; }
	inline Vector2_t2156229523 * get_address_of_matchedPosition_20() { return &___matchedPosition_20; }
	inline void set_matchedPosition_20(Vector2_t2156229523  value)
	{
		___matchedPosition_20 = value;
	}

	inline static int32_t get_offset_of_CurrentToken_21() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___CurrentToken_21)); }
	inline UIPuzzleToken_t1662105885 * get_CurrentToken_21() const { return ___CurrentToken_21; }
	inline UIPuzzleToken_t1662105885 ** get_address_of_CurrentToken_21() { return &___CurrentToken_21; }
	inline void set_CurrentToken_21(UIPuzzleToken_t1662105885 * value)
	{
		___CurrentToken_21 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentToken_21), value);
	}

	inline static int32_t get_offset_of_matchCount_22() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___matchCount_22)); }
	inline int32_t get_matchCount_22() const { return ___matchCount_22; }
	inline int32_t* get_address_of_matchCount_22() { return &___matchCount_22; }
	inline void set_matchCount_22(int32_t value)
	{
		___matchCount_22 = value;
	}

	inline static int32_t get_offset_of_manager_24() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369, ___manager_24)); }
	inline QuestManager_t588401851 * get_manager_24() const { return ___manager_24; }
	inline QuestManager_t588401851 ** get_address_of_manager_24() { return &___manager_24; }
	inline void set_manager_24(QuestManager_t588401851 * value)
	{
		___manager_24 = value;
		Il2CppCodeGenWriteBarrier((&___manager_24), value);
	}
};

struct UIPuzzleController_t2836468369_StaticFields
{
public:
	// System.Int32 UIPuzzleController::TOTAL_TOKENS
	int32_t ___TOTAL_TOKENS_2;
	// UIPuzzleController/PuzzleControllerEventHandler UIPuzzleController::OnEndPuzzle
	PuzzleControllerEventHandler_t963335703 * ___OnEndPuzzle_23;

public:
	inline static int32_t get_offset_of_TOTAL_TOKENS_2() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369_StaticFields, ___TOTAL_TOKENS_2)); }
	inline int32_t get_TOTAL_TOKENS_2() const { return ___TOTAL_TOKENS_2; }
	inline int32_t* get_address_of_TOTAL_TOKENS_2() { return &___TOTAL_TOKENS_2; }
	inline void set_TOTAL_TOKENS_2(int32_t value)
	{
		___TOTAL_TOKENS_2 = value;
	}

	inline static int32_t get_offset_of_OnEndPuzzle_23() { return static_cast<int32_t>(offsetof(UIPuzzleController_t2836468369_StaticFields, ___OnEndPuzzle_23)); }
	inline PuzzleControllerEventHandler_t963335703 * get_OnEndPuzzle_23() const { return ___OnEndPuzzle_23; }
	inline PuzzleControllerEventHandler_t963335703 ** get_address_of_OnEndPuzzle_23() { return &___OnEndPuzzle_23; }
	inline void set_OnEndPuzzle_23(PuzzleControllerEventHandler_t963335703 * value)
	{
		___OnEndPuzzle_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnEndPuzzle_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPUZZLECONTROLLER_T2836468369_H
#ifndef RESULTQUESTSCENE_T315437199_H
#define RESULTQUESTSCENE_T315437199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultQuestScene
struct  ResultQuestScene_t315437199  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager ResultQuestScene::manager
	QuestManager_t588401851 * ___manager_2;
	// Quest ResultQuestScene::quest
	Quest_t3696879532 * ___quest_3;
	// UnityEngine.UI.Text ResultQuestScene::text
	Text_t1901882714 * ___text_4;
	// SceneChanger ResultQuestScene::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_5;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_quest_3() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___quest_3)); }
	inline Quest_t3696879532 * get_quest_3() const { return ___quest_3; }
	inline Quest_t3696879532 ** get_address_of_quest_3() { return &___quest_3; }
	inline void set_quest_3(Quest_t3696879532 * value)
	{
		___quest_3 = value;
		Il2CppCodeGenWriteBarrier((&___quest_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_sceneChanger_5() { return static_cast<int32_t>(offsetof(ResultQuestScene_t315437199, ___sceneChanger_5)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_5() const { return ___sceneChanger_5; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_5() { return &___sceneChanger_5; }
	inline void set_sceneChanger_5(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_5 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTQUESTSCENE_T315437199_H
#ifndef UIPUZZLEINSERT_T594790890_H
#define UIPUZZLEINSERT_T594790890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPuzzleInsert
struct  UIPuzzleInsert_t594790890  : public MonoBehaviour_t3962482529
{
public:
	// UIPuzzleController UIPuzzleInsert::puzzleController
	UIPuzzleController_t2836468369 * ___puzzleController_2;
	// EPuzzleToken UIPuzzleInsert::Token
	int32_t ___Token_3;
	// System.Boolean UIPuzzleInsert::debugLog
	bool ___debugLog_5;
	// System.Boolean UIPuzzleInsert::match
	bool ___match_6;

public:
	inline static int32_t get_offset_of_puzzleController_2() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___puzzleController_2)); }
	inline UIPuzzleController_t2836468369 * get_puzzleController_2() const { return ___puzzleController_2; }
	inline UIPuzzleController_t2836468369 ** get_address_of_puzzleController_2() { return &___puzzleController_2; }
	inline void set_puzzleController_2(UIPuzzleController_t2836468369 * value)
	{
		___puzzleController_2 = value;
		Il2CppCodeGenWriteBarrier((&___puzzleController_2), value);
	}

	inline static int32_t get_offset_of_Token_3() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___Token_3)); }
	inline int32_t get_Token_3() const { return ___Token_3; }
	inline int32_t* get_address_of_Token_3() { return &___Token_3; }
	inline void set_Token_3(int32_t value)
	{
		___Token_3 = value;
	}

	inline static int32_t get_offset_of_debugLog_5() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___debugLog_5)); }
	inline bool get_debugLog_5() const { return ___debugLog_5; }
	inline bool* get_address_of_debugLog_5() { return &___debugLog_5; }
	inline void set_debugLog_5(bool value)
	{
		___debugLog_5 = value;
	}

	inline static int32_t get_offset_of_match_6() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890, ___match_6)); }
	inline bool get_match_6() const { return ___match_6; }
	inline bool* get_address_of_match_6() { return &___match_6; }
	inline void set_match_6(bool value)
	{
		___match_6 = value;
	}
};

struct UIPuzzleInsert_t594790890_StaticFields
{
public:
	// UIPuzzleInsert/PuzzleInsertEventHandler UIPuzzleInsert::OnPointerEvent
	PuzzleInsertEventHandler_t3824397729 * ___OnPointerEvent_4;

public:
	inline static int32_t get_offset_of_OnPointerEvent_4() { return static_cast<int32_t>(offsetof(UIPuzzleInsert_t594790890_StaticFields, ___OnPointerEvent_4)); }
	inline PuzzleInsertEventHandler_t3824397729 * get_OnPointerEvent_4() const { return ___OnPointerEvent_4; }
	inline PuzzleInsertEventHandler_t3824397729 ** get_address_of_OnPointerEvent_4() { return &___OnPointerEvent_4; }
	inline void set_OnPointerEvent_4(PuzzleInsertEventHandler_t3824397729 * value)
	{
		___OnPointerEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPUZZLEINSERT_T594790890_H
#ifndef QUESTSCENE_T3272400377_H
#define QUESTSCENE_T3272400377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestScene
struct  QuestScene_t3272400377  : public MonoBehaviour_t3962482529
{
public:
	// QuestManager QuestScene::manager
	QuestManager_t588401851 * ___manager_2;
	// Quest QuestScene::quest
	Quest_t3696879532 * ___quest_3;
	// UnityEngine.UI.Text QuestScene::text
	Text_t1901882714 * ___text_4;
	// UnityEngine.UI.Button[] QuestScene::options
	ButtonU5BU5D_t2297175928* ___options_5;
	// UnityEngine.UI.Text[] QuestScene::optionsText
	TextU5BU5D_t422084607* ___optionsText_6;
	// AudioRecordManager QuestScene::audioManager
	AudioRecordManager_t3166594816 * ___audioManager_7;
	// System.Int32 QuestScene::idStory
	int32_t ___idStory_8;
	// UnityEngine.GameObject QuestScene::buttonStartRecord
	GameObject_t1113636619 * ___buttonStartRecord_9;
	// UnityEngine.GameObject QuestScene::buttonStopRecord
	GameObject_t1113636619 * ___buttonStopRecord_10;
	// System.Single QuestScene::elapsedTime
	float ___elapsedTime_11;
	// System.Single QuestScene::countdownTimeDefault
	float ___countdownTimeDefault_12;
	// System.Single QuestScene::countdownTime
	float ___countdownTime_13;
	// System.Boolean QuestScene::isactivityStarted
	bool ___isactivityStarted_14;
	// UnityEngine.UI.Text QuestScene::countDownText
	Text_t1901882714 * ___countDownText_15;
	// UnityEngine.GameObject QuestScene::startActivity
	GameObject_t1113636619 * ___startActivity_16;
	// UnityEngine.GameObject QuestScene::completeActivity
	GameObject_t1113636619 * ___completeActivity_17;
	// UnityEngine.GameObject QuestScene::incompleteActivity
	GameObject_t1113636619 * ___incompleteActivity_18;
	// SceneChanger QuestScene::sceneChanger
	SceneChanger_t1033871796 * ___sceneChanger_19;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___manager_2)); }
	inline QuestManager_t588401851 * get_manager_2() const { return ___manager_2; }
	inline QuestManager_t588401851 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(QuestManager_t588401851 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_quest_3() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___quest_3)); }
	inline Quest_t3696879532 * get_quest_3() const { return ___quest_3; }
	inline Quest_t3696879532 ** get_address_of_quest_3() { return &___quest_3; }
	inline void set_quest_3(Quest_t3696879532 * value)
	{
		___quest_3 = value;
		Il2CppCodeGenWriteBarrier((&___quest_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___options_5)); }
	inline ButtonU5BU5D_t2297175928* get_options_5() const { return ___options_5; }
	inline ButtonU5BU5D_t2297175928** get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(ButtonU5BU5D_t2297175928* value)
	{
		___options_5 = value;
		Il2CppCodeGenWriteBarrier((&___options_5), value);
	}

	inline static int32_t get_offset_of_optionsText_6() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___optionsText_6)); }
	inline TextU5BU5D_t422084607* get_optionsText_6() const { return ___optionsText_6; }
	inline TextU5BU5D_t422084607** get_address_of_optionsText_6() { return &___optionsText_6; }
	inline void set_optionsText_6(TextU5BU5D_t422084607* value)
	{
		___optionsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___optionsText_6), value);
	}

	inline static int32_t get_offset_of_audioManager_7() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___audioManager_7)); }
	inline AudioRecordManager_t3166594816 * get_audioManager_7() const { return ___audioManager_7; }
	inline AudioRecordManager_t3166594816 ** get_address_of_audioManager_7() { return &___audioManager_7; }
	inline void set_audioManager_7(AudioRecordManager_t3166594816 * value)
	{
		___audioManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioManager_7), value);
	}

	inline static int32_t get_offset_of_idStory_8() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___idStory_8)); }
	inline int32_t get_idStory_8() const { return ___idStory_8; }
	inline int32_t* get_address_of_idStory_8() { return &___idStory_8; }
	inline void set_idStory_8(int32_t value)
	{
		___idStory_8 = value;
	}

	inline static int32_t get_offset_of_buttonStartRecord_9() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___buttonStartRecord_9)); }
	inline GameObject_t1113636619 * get_buttonStartRecord_9() const { return ___buttonStartRecord_9; }
	inline GameObject_t1113636619 ** get_address_of_buttonStartRecord_9() { return &___buttonStartRecord_9; }
	inline void set_buttonStartRecord_9(GameObject_t1113636619 * value)
	{
		___buttonStartRecord_9 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStartRecord_9), value);
	}

	inline static int32_t get_offset_of_buttonStopRecord_10() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___buttonStopRecord_10)); }
	inline GameObject_t1113636619 * get_buttonStopRecord_10() const { return ___buttonStopRecord_10; }
	inline GameObject_t1113636619 ** get_address_of_buttonStopRecord_10() { return &___buttonStopRecord_10; }
	inline void set_buttonStopRecord_10(GameObject_t1113636619 * value)
	{
		___buttonStopRecord_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStopRecord_10), value);
	}

	inline static int32_t get_offset_of_elapsedTime_11() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___elapsedTime_11)); }
	inline float get_elapsedTime_11() const { return ___elapsedTime_11; }
	inline float* get_address_of_elapsedTime_11() { return &___elapsedTime_11; }
	inline void set_elapsedTime_11(float value)
	{
		___elapsedTime_11 = value;
	}

	inline static int32_t get_offset_of_countdownTimeDefault_12() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___countdownTimeDefault_12)); }
	inline float get_countdownTimeDefault_12() const { return ___countdownTimeDefault_12; }
	inline float* get_address_of_countdownTimeDefault_12() { return &___countdownTimeDefault_12; }
	inline void set_countdownTimeDefault_12(float value)
	{
		___countdownTimeDefault_12 = value;
	}

	inline static int32_t get_offset_of_countdownTime_13() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___countdownTime_13)); }
	inline float get_countdownTime_13() const { return ___countdownTime_13; }
	inline float* get_address_of_countdownTime_13() { return &___countdownTime_13; }
	inline void set_countdownTime_13(float value)
	{
		___countdownTime_13 = value;
	}

	inline static int32_t get_offset_of_isactivityStarted_14() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___isactivityStarted_14)); }
	inline bool get_isactivityStarted_14() const { return ___isactivityStarted_14; }
	inline bool* get_address_of_isactivityStarted_14() { return &___isactivityStarted_14; }
	inline void set_isactivityStarted_14(bool value)
	{
		___isactivityStarted_14 = value;
	}

	inline static int32_t get_offset_of_countDownText_15() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___countDownText_15)); }
	inline Text_t1901882714 * get_countDownText_15() const { return ___countDownText_15; }
	inline Text_t1901882714 ** get_address_of_countDownText_15() { return &___countDownText_15; }
	inline void set_countDownText_15(Text_t1901882714 * value)
	{
		___countDownText_15 = value;
		Il2CppCodeGenWriteBarrier((&___countDownText_15), value);
	}

	inline static int32_t get_offset_of_startActivity_16() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___startActivity_16)); }
	inline GameObject_t1113636619 * get_startActivity_16() const { return ___startActivity_16; }
	inline GameObject_t1113636619 ** get_address_of_startActivity_16() { return &___startActivity_16; }
	inline void set_startActivity_16(GameObject_t1113636619 * value)
	{
		___startActivity_16 = value;
		Il2CppCodeGenWriteBarrier((&___startActivity_16), value);
	}

	inline static int32_t get_offset_of_completeActivity_17() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___completeActivity_17)); }
	inline GameObject_t1113636619 * get_completeActivity_17() const { return ___completeActivity_17; }
	inline GameObject_t1113636619 ** get_address_of_completeActivity_17() { return &___completeActivity_17; }
	inline void set_completeActivity_17(GameObject_t1113636619 * value)
	{
		___completeActivity_17 = value;
		Il2CppCodeGenWriteBarrier((&___completeActivity_17), value);
	}

	inline static int32_t get_offset_of_incompleteActivity_18() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___incompleteActivity_18)); }
	inline GameObject_t1113636619 * get_incompleteActivity_18() const { return ___incompleteActivity_18; }
	inline GameObject_t1113636619 ** get_address_of_incompleteActivity_18() { return &___incompleteActivity_18; }
	inline void set_incompleteActivity_18(GameObject_t1113636619 * value)
	{
		___incompleteActivity_18 = value;
		Il2CppCodeGenWriteBarrier((&___incompleteActivity_18), value);
	}

	inline static int32_t get_offset_of_sceneChanger_19() { return static_cast<int32_t>(offsetof(QuestScene_t3272400377, ___sceneChanger_19)); }
	inline SceneChanger_t1033871796 * get_sceneChanger_19() const { return ___sceneChanger_19; }
	inline SceneChanger_t1033871796 ** get_address_of_sceneChanger_19() { return &___sceneChanger_19; }
	inline void set_sceneChanger_19(SceneChanger_t1033871796 * value)
	{
		___sceneChanger_19 = value;
		Il2CppCodeGenWriteBarrier((&___sceneChanger_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTSCENE_T3272400377_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef DROPDOWN_T2274391225_H
#define DROPDOWN_T2274391225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown
struct  Dropdown_t2274391225  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t3704657025 * ___m_Template_16;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t1901882714 * ___m_CaptionText_17;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t2670269651 * ___m_CaptionImage_18;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t1901882714 * ___m_ItemText_19;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t2670269651 * ___m_ItemImage_20;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_21;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t1438173104 * ___m_Options_22;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t4040729994 * ___m_OnValueChanged_23;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t1113636619 * ___m_Dropdown_24;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t1113636619 * ___m_Blocker_25;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t2924027637 * ___m_Items_26;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t3520241082 * ___m_AlphaTweenRunner_27;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_28;

public:
	inline static int32_t get_offset_of_m_Template_16() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Template_16)); }
	inline RectTransform_t3704657025 * get_m_Template_16() const { return ___m_Template_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_Template_16() { return &___m_Template_16; }
	inline void set_m_Template_16(RectTransform_t3704657025 * value)
	{
		___m_Template_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_16), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_17() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionText_17)); }
	inline Text_t1901882714 * get_m_CaptionText_17() const { return ___m_CaptionText_17; }
	inline Text_t1901882714 ** get_address_of_m_CaptionText_17() { return &___m_CaptionText_17; }
	inline void set_m_CaptionText_17(Text_t1901882714 * value)
	{
		___m_CaptionText_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_17), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_18() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionImage_18)); }
	inline Image_t2670269651 * get_m_CaptionImage_18() const { return ___m_CaptionImage_18; }
	inline Image_t2670269651 ** get_address_of_m_CaptionImage_18() { return &___m_CaptionImage_18; }
	inline void set_m_CaptionImage_18(Image_t2670269651 * value)
	{
		___m_CaptionImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_18), value);
	}

	inline static int32_t get_offset_of_m_ItemText_19() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemText_19)); }
	inline Text_t1901882714 * get_m_ItemText_19() const { return ___m_ItemText_19; }
	inline Text_t1901882714 ** get_address_of_m_ItemText_19() { return &___m_ItemText_19; }
	inline void set_m_ItemText_19(Text_t1901882714 * value)
	{
		___m_ItemText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_19), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_20() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemImage_20)); }
	inline Image_t2670269651 * get_m_ItemImage_20() const { return ___m_ItemImage_20; }
	inline Image_t2670269651 ** get_address_of_m_ItemImage_20() { return &___m_ItemImage_20; }
	inline void set_m_ItemImage_20(Image_t2670269651 * value)
	{
		___m_ItemImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_20), value);
	}

	inline static int32_t get_offset_of_m_Value_21() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Value_21)); }
	inline int32_t get_m_Value_21() const { return ___m_Value_21; }
	inline int32_t* get_address_of_m_Value_21() { return &___m_Value_21; }
	inline void set_m_Value_21(int32_t value)
	{
		___m_Value_21 = value;
	}

	inline static int32_t get_offset_of_m_Options_22() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Options_22)); }
	inline OptionDataList_t1438173104 * get_m_Options_22() const { return ___m_Options_22; }
	inline OptionDataList_t1438173104 ** get_address_of_m_Options_22() { return &___m_Options_22; }
	inline void set_m_Options_22(OptionDataList_t1438173104 * value)
	{
		___m_Options_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_22), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_OnValueChanged_23)); }
	inline DropdownEvent_t4040729994 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline DropdownEvent_t4040729994 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(DropdownEvent_t4040729994 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_24() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Dropdown_24)); }
	inline GameObject_t1113636619 * get_m_Dropdown_24() const { return ___m_Dropdown_24; }
	inline GameObject_t1113636619 ** get_address_of_m_Dropdown_24() { return &___m_Dropdown_24; }
	inline void set_m_Dropdown_24(GameObject_t1113636619 * value)
	{
		___m_Dropdown_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_24), value);
	}

	inline static int32_t get_offset_of_m_Blocker_25() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Blocker_25)); }
	inline GameObject_t1113636619 * get_m_Blocker_25() const { return ___m_Blocker_25; }
	inline GameObject_t1113636619 ** get_address_of_m_Blocker_25() { return &___m_Blocker_25; }
	inline void set_m_Blocker_25(GameObject_t1113636619 * value)
	{
		___m_Blocker_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_25), value);
	}

	inline static int32_t get_offset_of_m_Items_26() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Items_26)); }
	inline List_1_t2924027637 * get_m_Items_26() const { return ___m_Items_26; }
	inline List_1_t2924027637 ** get_address_of_m_Items_26() { return &___m_Items_26; }
	inline void set_m_Items_26(List_1_t2924027637 * value)
	{
		___m_Items_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_26), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_27() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_AlphaTweenRunner_27)); }
	inline TweenRunner_1_t3520241082 * get_m_AlphaTweenRunner_27() const { return ___m_AlphaTweenRunner_27; }
	inline TweenRunner_1_t3520241082 ** get_address_of_m_AlphaTweenRunner_27() { return &___m_AlphaTweenRunner_27; }
	inline void set_m_AlphaTweenRunner_27(TweenRunner_1_t3520241082 * value)
	{
		___m_AlphaTweenRunner_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_27), value);
	}

	inline static int32_t get_offset_of_validTemplate_28() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___validTemplate_28)); }
	inline bool get_validTemplate_28() const { return ___validTemplate_28; }
	inline bool* get_address_of_validTemplate_28() { return &___validTemplate_28; }
	inline void set_validTemplate_28(bool value)
	{
		___validTemplate_28 = value;
	}
};

struct Dropdown_t2274391225_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t3270282352 * ___s_NoOptionData_29;

public:
	inline static int32_t get_offset_of_s_NoOptionData_29() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225_StaticFields, ___s_NoOptionData_29)); }
	inline OptionData_t3270282352 * get_s_NoOptionData_29() const { return ___s_NoOptionData_29; }
	inline OptionData_t3270282352 ** get_address_of_s_NoOptionData_29() { return &___s_NoOptionData_29; }
	inline void set_s_NoOptionData_29(OptionData_t3270282352 * value)
	{
		___s_NoOptionData_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWN_T2274391225_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_16;

public:
	inline static int32_t get_offset_of_m_OnClick_16() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_16)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_16() const { return ___m_OnClick_16; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_16() { return &___m_OnClick_16; }
	inline void set_m_OnClick_16(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_39;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_39)); }
	inline bool get_m_Tracked_39() const { return ___m_Tracked_39; }
	inline bool* get_address_of_m_Tracked_39() { return &___m_Tracked_39; }
	inline void set_m_Tracked_39(bool value)
	{
		___m_Tracked_39 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_40;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_42;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_43;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_44;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_45;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_46;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_40() const { return ___s_VertScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_40() { return &___s_VertScratch_40; }
	inline void set_s_VertScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_40), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_41)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_41() const { return ___s_UVScratch_41; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_41() { return &___s_UVScratch_41; }
	inline void set_s_UVScratch_41(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_41), value);
	}

	inline static int32_t get_offset_of_s_Xy_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_42() const { return ___s_Xy_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_42() { return &___s_Xy_42; }
	inline void set_s_Xy_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_42), value);
	}

	inline static int32_t get_offset_of_s_Uv_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_43)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_43() const { return ___s_Uv_43; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_43() { return &___s_Uv_43; }
	inline void set_s_Uv_43(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_43), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_44)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_44() const { return ___m_TrackedTexturelessImages_44; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_44() { return &___m_TrackedTexturelessImages_44; }
	inline void set_m_TrackedTexturelessImages_44(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_44), value);
	}

	inline static int32_t get_offset_of_s_Initialized_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_45)); }
	inline bool get_s_Initialized_45() const { return ___s_Initialized_45; }
	inline bool* get_address_of_s_Initialized_45() { return &___s_Initialized_45; }
	inline void set_s_Initialized_45(bool value)
	{
		___s_Initialized_45 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_46)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_46() const { return ___U3CU3Ef__mgU24cache0_46; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_46() { return &___U3CU3Ef__mgU24cache0_46; }
	inline void set_U3CU3Ef__mgU24cache0_46(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t2897418192  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2297175928  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Button_t4055032469 * m_Items[1];

public:
	inline Button_t4055032469 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Button_t4055032469 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Button_t4055032469 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Button_t4055032469 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Button_t4055032469 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Button_t4055032469 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Quest[]
struct QuestU5BU5D_t1937508389  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Quest_t3696879532 * m_Items[1];

public:
	inline Quest_t3696879532 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Quest_t3696879532 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Quest_t3696879532 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Quest_t3696879532 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Quest_t3696879532 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Quest_t3696879532 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Text_t1901882714 * m_Items[1];

public:
	inline Text_t1901882714 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Text_t1901882714 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Text_t1901882714 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Text_t1901882714 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Text_t1901882714 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Text_t1901882714 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[]
struct SingleU5BU5D_t1444911251  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Int16[]
struct Int16U5BU5D_t3686840178  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1703627840  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t1188392813 * m_Items[1];

public:
	inline Delegate_t1188392813 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t1188392813 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m3345590611_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m964359627_gshared (List_1_t2869341516 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float List_1_get_Item_m718437397_gshared (List_1_t2869341516 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4125879936_gshared (List_1_t2869341516 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::RemoveRange(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveRange_m1420183523_gshared (List_1_t2869341516 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Single>::ToArray()
extern "C" IL2CPP_METHOD_ATTR SingleU5BU5D_t1444911251* List_1_ToArray_m571148082_gshared (List_1_t2869341516 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m1536473967_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<EPuzzleToken>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m28597759_gshared (List_1_t3937307605 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m1328026504_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m1524640104_gshared (List_1_t899420910 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  List_1_get_Item_m200663048_gshared (List_1_t899420910 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<E> SystemHelpers::ShuffleList<UnityEngine.Vector3>(System.Collections.Generic.List`1<E>)
extern "C" IL2CPP_METHOD_ATTR List_1_t899420910 * SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547_gshared (RuntimeObject * __this /* static, unused */, List_1_t899420910 * ___inputList0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<EPuzzleToken>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m3452984843_gshared (List_1_t3937307605 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<EPuzzleToken>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m1003748895_gshared (List_1_t3937307605 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void NAudio.Wave.WZT.WaveFormat::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaveFormat__ctor_m1746447265 (WaveFormat_t2842237185 * __this, const RuntimeMethod* method);
// System.Void NAudio.Wave.WZT.WaveFormat::.ctor(System.IO.BinaryReader)
extern "C" IL2CPP_METHOD_ATTR void WaveFormat__ctor_m1988710552 (WaveFormat_t2842237185 * __this, BinaryReader_t2428077293 * ___br0, const RuntimeMethod* method);
// System.Void NAudio.Wave.WZT.WaveFormatExtraData::ReadExtraData(System.IO.BinaryReader)
extern "C" IL2CPP_METHOD_ATTR void WaveFormatExtraData_ReadExtraData_m1454707470 (WaveFormatExtraData_t1489494699 * __this, BinaryReader_t2428077293 * ___reader0, const RuntimeMethod* method);
// System.Void NAudio.Wave.WZT.WaveFormat::Serialize(System.IO.BinaryWriter)
extern "C" IL2CPP_METHOD_ATTR void WaveFormat_Serialize_m3147009982 (WaveFormat_t2842237185 * __this, BinaryWriter_t3992595042 * ___writer0, const RuntimeMethod* method);
// System.Void System.IO.Stream::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Stream__ctor_m3881936881 (Stream_t1273022909 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Enter_m984175629 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, bool* p1, const RuntimeMethod* method);
// System.Int32 NAudio.Wave.WZT.WaveFormat::get_AverageBytesPerSecond()
extern "C" IL2CPP_METHOD_ATTR int32_t WaveFormat_get_AverageBytesPerSecond_m3143431325 (WaveFormat_t2842237185 * __this, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.TimeSpan System.TimeSpan::FromSeconds(System.Double)
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  TimeSpan_FromSeconds_m4219356874 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method);
// System.Double System.TimeSpan::get_TotalSeconds()
extern "C" IL2CPP_METHOD_ATTR double TimeSpan_get_TotalSeconds_m4083325051 (TimeSpan_t881159249 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<QuestManager>()
#define Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(__this /* static, unused */, method) ((  QuestManager_t588401851 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<CharacterBehavior>()
#define Object_FindObjectOfType_TisCharacterBehavior_t2392107484_m3992520573(__this /* static, unused */, method) ((  CharacterBehavior_t2392107484 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<SceneChanger>()
#define Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497(__this /* static, unused */, method) ((  SceneChanger_t1033871796 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void SceneChanger::ChangeSceneTo(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneChanger_ChangeSceneTo_m2636143807 (SceneChanger_t1033871796 * __this, String_t* ___sceneName0, const RuntimeMethod* method);
// System.Int32 CharacterBehavior::GetPos()
extern "C" IL2CPP_METHOD_ATTR int32_t CharacterBehavior_GetPos_m3016674883 (CharacterBehavior_t2392107484 * __this, const RuntimeMethod* method);
// System.Void CharacterBehavior::NextLevel()
extern "C" IL2CPP_METHOD_ATTR void CharacterBehavior_NextLevel_m3007643461 (CharacterBehavior_t2392107484 * __this, const RuntimeMethod* method);
// System.Void QuestManager::SaveData()
extern "C" IL2CPP_METHOD_ATTR void QuestManager_SaveData_m3571505125 (QuestManager_t588401851 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<GameController>()
#define Object_FindObjectOfType_TisGameController_t2330501625_m3040890899(__this /* static, unused */, method) ((  GameController_t2330501625 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.Void GameController::SaveProgress()
extern "C" IL2CPP_METHOD_ATTR void GameController_SaveProgress_m2661837419 (GameController_t2330501625 * __this, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C" IL2CPP_METHOD_ATTR Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Scene_get_name_m622963475 (Scene_t2348375561 * __this, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_Equals_m2270643605 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
extern "C" IL2CPP_METHOD_ATTR ColorBlock_t2139031574  Selectable_get_colors_m2213868400 (Selectable_t3250028441 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void ColorBlock_set_disabledColor_m2996580094 (ColorBlock_t2139031574 * __this, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
extern "C" IL2CPP_METHOD_ATTR void Selectable_set_colors_m1384394609 (Selectable_t3250028441 * __this, ColorBlock_t2139031574  p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Selectable_set_interactable_m3105888815 (Selectable_t3250028441 * __this, bool p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<AudioRecordManager>()
#define Object_FindObjectOfType_TisAudioRecordManager_t3166594816_m2959838367(__this /* static, unused */, method) ((  AudioRecordManager_t3166594816 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_get_gameObject_m3693461266 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t4055032469_m1515138076(__this, method) ((  Button_t4055032469 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3345590611_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m1874334613 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Void QuestScene::FinishActivity()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_FinishActivity_m545023592 (QuestScene_t3272400377 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t1901882714_m4196288697(__this, method) ((  Text_t1901882714 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.String QuestScene::FormatTime(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR String_t* QuestScene_FormatTime_m3401522192 (QuestScene_t3272400377 * __this, int32_t ___time0, bool ___countdownd1, const RuntimeMethod* method);
// System.Void QuestScene::selectStory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void QuestScene_selectStory_m3040738564 (QuestScene_t3272400377 * __this, int32_t ___id0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void Quest::QuestComplete()
extern "C" IL2CPP_METHOD_ATTR void Quest_QuestComplete_m1582949342 (Quest_t3696879532 * __this, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void AudioRecordManager::OnStartMicButtonClick()
extern "C" IL2CPP_METHOD_ATTR void AudioRecordManager_OnStartMicButtonClick_m1918784328 (AudioRecordManager_t3166594816 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.String UnityEngine.GameObject::get_tag()
extern "C" IL2CPP_METHOD_ATTR String_t* GameObject_get_tag_m3951609671 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator QuestTrigger::DelayStartQuest()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* QuestTrigger_DelayStartQuest_m4146236069 (QuestTrigger_t3125687900 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void QuestTrigger/<DelayStartQuest>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayStartQuestU3Ec__Iterator0__ctor_m861285505 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method);
// System.Void Quest::StartQuest()
extern "C" IL2CPP_METHOD_ATTR void Quest_StartQuest_m2018510914 (Quest_t3696879532 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogFormat(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR void Debug_LogFormat_m309087137 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// System.String System.IO.Path::GetDirectoryName(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Path_GetDirectoryName_m3496866581 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
extern "C" IL2CPP_METHOD_ATTR DirectoryInfo_t35957480 * Directory_CreateDirectory_m751642867 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.IO.FileStream SavWav::CreateEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR FileStream_t4292183065 * SavWav_CreateEmpty_m194446383 (RuntimeObject * __this /* static, unused */, String_t* ___filepath0, const RuntimeMethod* method);
// System.Void SavWav::ConvertAndWrite(System.IO.FileStream,UnityEngine.AudioClip,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SavWav_ConvertAndWrite_m4186062765 (RuntimeObject * __this /* static, unused */, FileStream_t4292183065 * ___fileStream0, AudioClip_t3680889665 * ___clip1, int32_t ___recordedSamples2, const RuntimeMethod* method);
// System.Void SavWav::WriteHeader(System.IO.FileStream,UnityEngine.AudioClip,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SavWav_WriteHeader_m2208500169 (RuntimeObject * __this /* static, unused */, FileStream_t4292183065 * ___fileStream0, AudioClip_t3680889665 * ___clip1, int32_t ___totalSamples2, const RuntimeMethod* method);
// System.Int32 UnityEngine.AudioClip::get_samples()
extern "C" IL2CPP_METHOD_ATTR int32_t AudioClip_get_samples_m1836473408 (AudioClip_t3680889665 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool AudioClip_GetData_m1251334845 (AudioClip_t3680889665 * __this, SingleU5BU5D_t1444911251* p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m964359627(__this, p0, method) ((  void (*) (List_1_t2869341516 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m964359627_gshared)(__this, p0, method)
// System.Int32 UnityEngine.AudioClip::get_channels()
extern "C" IL2CPP_METHOD_ATTR int32_t AudioClip_get_channels_m2392813414 (AudioClip_t3680889665 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AudioClip::get_frequency()
extern "C" IL2CPP_METHOD_ATTR int32_t AudioClip_get_frequency_m3907759635 (AudioClip_t3680889665 * __this, const RuntimeMethod* method);
// UnityEngine.AudioClip SavWav::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR AudioClip_t3680889665 * SavWav_TrimSilence_m1383616551 (RuntimeObject * __this /* static, unused */, List_1_t2869341516 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, const RuntimeMethod* method);
// UnityEngine.AudioClip SavWav::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR AudioClip_t3680889665 * SavWav_TrimSilence_m3302723312 (RuntimeObject * __this /* static, unused */, List_1_t2869341516 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, bool ____3D4, bool ___stream5, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
#define List_1_get_Item_m718437397(__this, p0, method) ((  float (*) (List_1_t2869341516 *, int32_t, const RuntimeMethod*))List_1_get_Item_m718437397_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
#define List_1_get_Count_m4125879936(__this, method) ((  int32_t (*) (List_1_t2869341516 *, const RuntimeMethod*))List_1_get_Count_m4125879936_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1420183523(__this, p0, p1, method) ((  void (*) (List_1_t2869341516 *, int32_t, int32_t, const RuntimeMethod*))List_1_RemoveRange_m1420183523_gshared)(__this, p0, p1, method)
// UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR AudioClip_t3680889665 * AudioClip_Create_m255819841 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, int32_t p2, int32_t p3, bool p4, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Single>::ToArray()
#define List_1_ToArray_m571148082(__this, method) ((  SingleU5BU5D_t1444911251* (*) (List_1_t2869341516 *, const RuntimeMethod*))List_1_ToArray_m571148082_gshared)(__this, method)
// System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool AudioClip_SetData_m313920037 (AudioClip_t3680889665 * __this, SingleU5BU5D_t1444911251* p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.IO.FileStream::.ctor(System.String,System.IO.FileMode)
extern "C" IL2CPP_METHOD_ATTR void FileStream__ctor_m2784380556 (FileStream_t4292183065 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.Int16)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* BitConverter_GetBytes_m658425501 (RuntimeObject * __this /* static, unused */, int16_t p0, const RuntimeMethod* method);
// System.Void System.Array::CopyTo(System.Array,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Array_CopyTo_m225704097 (RuntimeArray * __this, RuntimeArray * p0, int32_t p1, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C" IL2CPP_METHOD_ATTR Encoding_t1523322056 * Encoding_get_UTF8_m1008486739 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.Int64)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* BitConverter_GetBytes_m4144088731 (RuntimeObject * __this /* static, unused */, int64_t p0, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* BitConverter_GetBytes_m1040762521 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.UInt16)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* BitConverter_GetBytes_m2889824517 (RuntimeObject * __this /* static, unused */, uint16_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<DataController>()
#define Object_FindObjectOfType_TisDataController_t353634109_m3280844559(__this /* static, unused */, method) ((  DataController_t353634109 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void GameController::SelectCharacter(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameController_SelectCharacter_m3280008209 (GameController_t2330501625 * __this, String_t* ___type0, const RuntimeMethod* method);
// System.Void GameController::StartGame()
extern "C" IL2CPP_METHOD_ATTR void GameController_StartGame_m2172304344 (GameController_t2330501625 * __this, const RuntimeMethod* method);
// System.Void GameController::restarData()
extern "C" IL2CPP_METHOD_ATTR void GameController_restarData_m2034268093 (GameController_t2330501625 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m75143462 (Action_t1264377477 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void AudioRecordManager::add_OnStartEncoding(System.Action)
extern "C" IL2CPP_METHOD_ATTR void AudioRecordManager_add_OnStartEncoding_m1329063948 (AudioRecordManager_t3166594816 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method);
// System.Void AudioRecordManager::add_OnStopEncoding(System.Action)
extern "C" IL2CPP_METHOD_ATTR void AudioRecordManager_add_OnStopEncoding_m3752743452 (AudioRecordManager_t3166594816 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method);
// System.Void AudioRecordManager::remove_OnStartEncoding(System.Action)
extern "C" IL2CPP_METHOD_ATTR void AudioRecordManager_remove_OnStartEncoding_m3701522171 (AudioRecordManager_t3166594816 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method);
// System.Void AudioRecordManager::remove_OnStopEncoding(System.Action)
extern "C" IL2CPP_METHOD_ATTR void AudioRecordManager_remove_OnStopEncoding_m1703388006 (AudioRecordManager_t3166594816 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C" IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m1767405923 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.String AudioRecordManager::get_ElapsedTimeTextWithFormat()
extern "C" IL2CPP_METHOD_ATTR String_t* AudioRecordManager_get_ElapsedTimeTextWithFormat_m1083798450 (AudioRecordManager_t3166594816 * __this, const RuntimeMethod* method);
// System.String AudioRecordManager::get_TimerTextWithFormat()
extern "C" IL2CPP_METHOD_ATTR String_t* AudioRecordManager_get_TimerTextWithFormat_m2913629590 (AudioRecordManager_t3166594816 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.UI.Dropdown::get_value()
extern "C" IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m1555353112 (Dropdown_t2274391225 * __this, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Void UIPuzzleToken/PuzzleTokenEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void PuzzleTokenEventHandler__ctor_m94650657 (PuzzleTokenEventHandler_t3807414068 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UIPuzzleToken::add_OnDragEvent(UIPuzzleToken/PuzzleTokenEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_add_OnDragEvent_m3807187468 (RuntimeObject * __this /* static, unused */, PuzzleTokenEventHandler_t3807414068 * ___value0, const RuntimeMethod* method);
// System.Void UIPuzzleInsert/PuzzleInsertEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void PuzzleInsertEventHandler__ctor_m3630263791 (PuzzleInsertEventHandler_t3824397729 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UIPuzzleInsert::add_OnPointerEvent(UIPuzzleInsert/PuzzleInsertEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_add_OnPointerEvent_m3287251477 (RuntimeObject * __this /* static, unused */, PuzzleInsertEventHandler_t3824397729 * ___value0, const RuntimeMethod* method);
// System.Void UIPuzzleToken::remove_OnDragEvent(UIPuzzleToken/PuzzleTokenEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_remove_OnDragEvent_m2242078314 (RuntimeObject * __this /* static, unused */, PuzzleTokenEventHandler_t3807414068 * ___value0, const RuntimeMethod* method);
// System.Void UIPuzzleInsert::remove_OnPointerEvent(UIPuzzleInsert/PuzzleInsertEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_remove_OnPointerEvent_m414160073 (RuntimeObject * __this /* static, unused */, PuzzleInsertEventHandler_t3824397729 * ___value0, const RuntimeMethod* method);
// System.Void UIPuzzleController::ShowIntroPanel()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_ShowIntroPanel_m1546992945 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UIPuzzleController::_StartLate()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* UIPuzzleController__StartLate_m1048854624 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method);
// System.Void UIPuzzleController/<_StartLate>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3C_StartLateU3Ec__Iterator0__ctor_m3073728995 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_CeilToInt_m432108984 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.String SystemHelpers::FormatTimeSeconds(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* SystemHelpers_FormatTimeSeconds_m797777047 (RuntimeObject * __this /* static, unused */, int32_t ___time0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m1536473967(__this, method) ((  void (*) (List_1_t899420910 *, const RuntimeMethod*))List_1__ctor_m1536473967_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EPuzzleToken>::.ctor()
#define List_1__ctor_m28597759(__this, method) ((  void (*) (List_1_t3937307605 *, const RuntimeMethod*))List_1__ctor_m28597759_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<UnityEngine.UI.Image>::get_Item(System.Int32)
#define List_1_get_Item_m1242626717(__this, p0, method) ((  Image_t2670269651 * (*) (List_1_t4142344393 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method)
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t3704657025 * Graphic_get_rectTransform_m1167152468 (Graphic_t1660335611 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m1524640104(__this, p0, method) ((  void (*) (List_1_t899420910 *, Vector3_t3722313464 , const RuntimeMethod*))List_1_Add_m1524640104_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
#define List_1_get_Item_m200663048(__this, p0, method) ((  Vector3_t3722313464  (*) (List_1_t899420910 *, int32_t, const RuntimeMethod*))List_1_get_Item_m200663048_gshared)(__this, p0, method)
// System.Collections.Generic.List`1<E> SystemHelpers::ShuffleList<UnityEngine.Vector3>(System.Collections.Generic.List`1<E>)
#define SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547(__this /* static, unused */, ___inputList0, method) ((  List_1_t899420910 * (*) (RuntimeObject * /* static, unused */, List_1_t899420910 *, const RuntimeMethod*))SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547_gshared)(__this /* static, unused */, ___inputList0, method)
// System.Void System.Collections.Generic.List`1<UIPuzzleToken>::.ctor()
#define List_1__ctor_m3799958844(__this, method) ((  void (*) (List_1_t3134180627 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPuzzleToken>()
#define Component_GetComponent_TisUIPuzzleToken_t1662105885_m4197970512(__this, method) ((  UIPuzzleToken_t1662105885 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UIPuzzleToken>::Add(!0)
#define List_1_Add_m3373879604(__this, p0, method) ((  void (*) (List_1_t3134180627 *, UIPuzzleToken_t1662105885 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UIPuzzleToken>::get_Item(System.Int32)
#define List_1_get_Item_m219509784(__this, p0, method) ((  UIPuzzleToken_t1662105885 * (*) (List_1_t3134180627 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method)
// System.Void UIPuzzleToken::InitToken()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_InitToken_m3887138451 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UIPuzzleInsert>()
#define Component_GetComponent_TisUIPuzzleInsert_t594790890_m1640620979(__this, method) ((  UIPuzzleInsert_t594790890 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<EPuzzleToken>::get_Item(System.Int32)
#define List_1_get_Item_m3452984843(__this, p0, method) ((  int32_t (*) (List_1_t3937307605 *, int32_t, const RuntimeMethod*))List_1_get_Item_m3452984843_gshared)(__this, p0, method)
// System.Void UIPuzzleInsert::Init(EPuzzleToken)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_Init_m3966161147 (UIPuzzleInsert_t594790890 * __this, int32_t ___token0, const RuntimeMethod* method);
// System.Void UIPuzzleToken::Match(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_Match_m1213795857 (UIPuzzleToken_t1662105885 * __this, Vector2_t2156229523  ___position0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Collections.IEnumerator UIPuzzleController::DelayCompleteQuest()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* UIPuzzleController_DelayCompleteQuest_m3765199727 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method);
// System.Void UIPuzzleController/PuzzleControllerEventHandler::Invoke()
extern "C" IL2CPP_METHOD_ATTR void PuzzleControllerEventHandler_Invoke_m1864861298 (PuzzleControllerEventHandler_t963335703 * __this, const RuntimeMethod* method);
// System.Void UIPuzzleToken::RestartToken()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_RestartToken_m3577290980 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method);
// System.Void UIPuzzleController/<DelayCompleteQuest>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayCompleteQuestU3Ec__Iterator1__ctor_m852968702 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m4231250055 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<EPuzzleToken>::Add(!0)
#define List_1_Add_m1003748895(__this, p0, method) ((  void (*) (List_1_t3937307605 *, int32_t, const RuntimeMethod*))List_1_Add_m1003748895_gshared)(__this, p0, method)
// System.Single UnityEngine.Time::get_time()
extern "C" IL2CPP_METHOD_ATTR float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4227543964 (MonoBehaviour_t3962482529 * __this, String_t* p0, float p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void UIPuzzleInsert/PuzzleInsertEventHandler::Invoke(System.Boolean,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void PuzzleInsertEventHandler_Invoke_m1382497804 (PuzzleInsertEventHandler_t3824397729 * __this, bool ___match0, Vector2_t2156229523  ___position1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, method) ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UIPuzzleToken::set_StartPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_set_StartPosition_m1033295995 (UIPuzzleToken_t1662105885 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UIPuzzleToken::set_IsGrabbable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_set_IsGrabbable_m4025083784 (UIPuzzleToken_t1662105885 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UIPuzzleToken::get_StartPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  UIPuzzleToken_get_StartPosition_m2034065853 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  PointerEventData_get_position_m437660275 (PointerEventData_t3807901092 * __this, const RuntimeMethod* method);
// System.Void UIPuzzleToken/PuzzleTokenEventHandler::Invoke(UIPuzzleToken,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PuzzleTokenEventHandler_Invoke_m834018601 (PuzzleTokenEventHandler_t3807414068 * __this, UIPuzzleToken_t1662105885 * ___token0, bool ___dragged1, const RuntimeMethod* method);
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t941916413* Texture2D_GetPixels_m2757678651 (Texture2D_t3840446185 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m2862217990 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_Lerp_m973389909 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, float p2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Addition(UnityEngine.Color,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_op_Addition_m3293657895 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Division(UnityEngine.Color,System.Single)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_op_Division_m1074517668 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixels_m3008871897 (Texture2D_t3840446185 * __this, ColorU5BU5D_t941916413* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: NAudio.Wave.WZT.WaveFormatExtraData
extern "C" void WaveFormatExtraData_t1489494699_marshal_pinvoke(const WaveFormatExtraData_t1489494699& unmarshaled, WaveFormatExtraData_t1489494699_marshaled_pinvoke& marshaled)
{
	if (unmarshaled.get_extraData_7() != NULL)
	{
		if (100 > (unmarshaled.get_extraData_7())->max_length)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_exception("", "Type could not be marshaled because the length of an embedded array instance does not match the declared length in the layout."),NULL);
		}

		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(100); i++)
		{
			(marshaled.___extraData_7)[i] = (unmarshaled.get_extraData_7())->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	marshaled.___waveFormatTag_0 = unmarshaled.get_waveFormatTag_0();
	marshaled.___channels_1 = unmarshaled.get_channels_1();
	marshaled.___sampleRate_2 = unmarshaled.get_sampleRate_2();
	marshaled.___averageBytesPerSecond_3 = unmarshaled.get_averageBytesPerSecond_3();
	marshaled.___blockAlign_4 = unmarshaled.get_blockAlign_4();
	marshaled.___bitsPerSample_5 = unmarshaled.get_bitsPerSample_5();
	marshaled.___extraSize_6 = unmarshaled.get_extraSize_6();
}
extern "C" void WaveFormatExtraData_t1489494699_marshal_pinvoke_back(const WaveFormatExtraData_t1489494699_marshaled_pinvoke& marshaled, WaveFormatExtraData_t1489494699& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveFormatExtraData_t1489494699_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_extraData_7(reinterpret_cast<ByteU5BU5D_t4116647657*>(SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, 100)));
	for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(100); i++)
	{
		(unmarshaled.get_extraData_7())->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (marshaled.___extraData_7)[i]);
	}
	uint16_t unmarshaled_waveFormatTag_temp_1 = 0;
	unmarshaled_waveFormatTag_temp_1 = marshaled.___waveFormatTag_0;
	unmarshaled.set_waveFormatTag_0(unmarshaled_waveFormatTag_temp_1);
	int16_t unmarshaled_channels_temp_2 = 0;
	unmarshaled_channels_temp_2 = marshaled.___channels_1;
	unmarshaled.set_channels_1(unmarshaled_channels_temp_2);
	int32_t unmarshaled_sampleRate_temp_3 = 0;
	unmarshaled_sampleRate_temp_3 = marshaled.___sampleRate_2;
	unmarshaled.set_sampleRate_2(unmarshaled_sampleRate_temp_3);
	int32_t unmarshaled_averageBytesPerSecond_temp_4 = 0;
	unmarshaled_averageBytesPerSecond_temp_4 = marshaled.___averageBytesPerSecond_3;
	unmarshaled.set_averageBytesPerSecond_3(unmarshaled_averageBytesPerSecond_temp_4);
	int16_t unmarshaled_blockAlign_temp_5 = 0;
	unmarshaled_blockAlign_temp_5 = marshaled.___blockAlign_4;
	unmarshaled.set_blockAlign_4(unmarshaled_blockAlign_temp_5);
	int16_t unmarshaled_bitsPerSample_temp_6 = 0;
	unmarshaled_bitsPerSample_temp_6 = marshaled.___bitsPerSample_5;
	unmarshaled.set_bitsPerSample_5(unmarshaled_bitsPerSample_temp_6);
	int16_t unmarshaled_extraSize_temp_7 = 0;
	unmarshaled_extraSize_temp_7 = marshaled.___extraSize_6;
	unmarshaled.set_extraSize_6(unmarshaled_extraSize_temp_7);
}
// Conversion method for clean up from marshalling of: NAudio.Wave.WZT.WaveFormatExtraData
extern "C" void WaveFormatExtraData_t1489494699_marshal_pinvoke_cleanup(WaveFormatExtraData_t1489494699_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: NAudio.Wave.WZT.WaveFormatExtraData
extern "C" void WaveFormatExtraData_t1489494699_marshal_com(const WaveFormatExtraData_t1489494699& unmarshaled, WaveFormatExtraData_t1489494699_marshaled_com& marshaled)
{
	if (unmarshaled.get_extraData_7() != NULL)
	{
		if (100 > (unmarshaled.get_extraData_7())->max_length)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_exception("", "Type could not be marshaled because the length of an embedded array instance does not match the declared length in the layout."),NULL);
		}

		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(100); i++)
		{
			(marshaled.___extraData_7)[i] = (unmarshaled.get_extraData_7())->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	marshaled.___waveFormatTag_0 = unmarshaled.get_waveFormatTag_0();
	marshaled.___channels_1 = unmarshaled.get_channels_1();
	marshaled.___sampleRate_2 = unmarshaled.get_sampleRate_2();
	marshaled.___averageBytesPerSecond_3 = unmarshaled.get_averageBytesPerSecond_3();
	marshaled.___blockAlign_4 = unmarshaled.get_blockAlign_4();
	marshaled.___bitsPerSample_5 = unmarshaled.get_bitsPerSample_5();
	marshaled.___extraSize_6 = unmarshaled.get_extraSize_6();
}
extern "C" void WaveFormatExtraData_t1489494699_marshal_com_back(const WaveFormatExtraData_t1489494699_marshaled_com& marshaled, WaveFormatExtraData_t1489494699& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveFormatExtraData_t1489494699_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_extraData_7(reinterpret_cast<ByteU5BU5D_t4116647657*>(SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, 100)));
	for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(100); i++)
	{
		(unmarshaled.get_extraData_7())->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (marshaled.___extraData_7)[i]);
	}
	uint16_t unmarshaled_waveFormatTag_temp_1 = 0;
	unmarshaled_waveFormatTag_temp_1 = marshaled.___waveFormatTag_0;
	unmarshaled.set_waveFormatTag_0(unmarshaled_waveFormatTag_temp_1);
	int16_t unmarshaled_channels_temp_2 = 0;
	unmarshaled_channels_temp_2 = marshaled.___channels_1;
	unmarshaled.set_channels_1(unmarshaled_channels_temp_2);
	int32_t unmarshaled_sampleRate_temp_3 = 0;
	unmarshaled_sampleRate_temp_3 = marshaled.___sampleRate_2;
	unmarshaled.set_sampleRate_2(unmarshaled_sampleRate_temp_3);
	int32_t unmarshaled_averageBytesPerSecond_temp_4 = 0;
	unmarshaled_averageBytesPerSecond_temp_4 = marshaled.___averageBytesPerSecond_3;
	unmarshaled.set_averageBytesPerSecond_3(unmarshaled_averageBytesPerSecond_temp_4);
	int16_t unmarshaled_blockAlign_temp_5 = 0;
	unmarshaled_blockAlign_temp_5 = marshaled.___blockAlign_4;
	unmarshaled.set_blockAlign_4(unmarshaled_blockAlign_temp_5);
	int16_t unmarshaled_bitsPerSample_temp_6 = 0;
	unmarshaled_bitsPerSample_temp_6 = marshaled.___bitsPerSample_5;
	unmarshaled.set_bitsPerSample_5(unmarshaled_bitsPerSample_temp_6);
	int16_t unmarshaled_extraSize_temp_7 = 0;
	unmarshaled_extraSize_temp_7 = marshaled.___extraSize_6;
	unmarshaled.set_extraSize_6(unmarshaled_extraSize_temp_7);
}
// Conversion method for clean up from marshalling of: NAudio.Wave.WZT.WaveFormatExtraData
extern "C" void WaveFormatExtraData_t1489494699_marshal_com_cleanup(WaveFormatExtraData_t1489494699_marshaled_com& marshaled)
{
}
// System.Void NAudio.Wave.WZT.WaveFormatExtraData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaveFormatExtraData__ctor_m1274564551 (WaveFormatExtraData_t1489494699 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveFormatExtraData__ctor_m1274564551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private byte[] extraData = new byte[100];
		__this->set_extraData_7(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100))));
		// internal WaveFormatExtraData()
		WaveFormat__ctor_m1746447265(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NAudio.Wave.WZT.WaveFormatExtraData::.ctor(System.IO.BinaryReader)
extern "C" IL2CPP_METHOD_ATTR void WaveFormatExtraData__ctor_m92823382 (WaveFormatExtraData_t1489494699 * __this, BinaryReader_t2428077293 * ___reader0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveFormatExtraData__ctor_m92823382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private byte[] extraData = new byte[100];
		__this->set_extraData_7(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100))));
		// : base(reader)
		BinaryReader_t2428077293 * L_0 = ___reader0;
		WaveFormat__ctor_m1988710552(__this, L_0, /*hidden argument*/NULL);
		// ReadExtraData(reader);
		BinaryReader_t2428077293 * L_1 = ___reader0;
		// ReadExtraData(reader);
		WaveFormatExtraData_ReadExtraData_m1454707470(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Byte[] NAudio.Wave.WZT.WaveFormatExtraData::get_ExtraData()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* WaveFormatExtraData_get_ExtraData_m1974269035 (WaveFormatExtraData_t1489494699 * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_t4116647657* V_0 = NULL;
	{
		// public byte[] ExtraData { get { return extraData; } }
		ByteU5BU5D_t4116647657* L_0 = __this->get_extraData_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// public byte[] ExtraData { get { return extraData; } }
		ByteU5BU5D_t4116647657* L_1 = V_0;
		return L_1;
	}
}
// System.Void NAudio.Wave.WZT.WaveFormatExtraData::ReadExtraData(System.IO.BinaryReader)
extern "C" IL2CPP_METHOD_ATTR void WaveFormatExtraData_ReadExtraData_m1454707470 (WaveFormatExtraData_t1489494699 * __this, BinaryReader_t2428077293 * ___reader0, const RuntimeMethod* method)
{
	{
		// if (this.extraSize > 0)
		int16_t L_0 = ((WaveFormat_t2842237185 *)__this)->get_extraSize_6();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		// reader.Read(extraData, 0, extraSize);
		BinaryReader_t2428077293 * L_1 = ___reader0;
		ByteU5BU5D_t4116647657* L_2 = __this->get_extraData_7();
		int16_t L_3 = ((WaveFormat_t2842237185 *)__this)->get_extraSize_6();
		// reader.Read(extraData, 0, extraSize);
		NullCheck(L_1);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(25 /* System.Int32 System.IO.BinaryReader::Read(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, 0, L_3);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void NAudio.Wave.WZT.WaveFormatExtraData::Serialize(System.IO.BinaryWriter)
extern "C" IL2CPP_METHOD_ATTR void WaveFormatExtraData_Serialize_m1438573946 (WaveFormatExtraData_t1489494699 * __this, BinaryWriter_t3992595042 * ___writer0, const RuntimeMethod* method)
{
	{
		// base.Serialize(writer);
		BinaryWriter_t3992595042 * L_0 = ___writer0;
		// base.Serialize(writer);
		WaveFormat_Serialize_m3147009982(__this, L_0, /*hidden argument*/NULL);
		// if (extraSize > 0)
		int16_t L_1 = ((WaveFormat_t2842237185 *)__this)->get_extraSize_6();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		// writer.Write(extraData, 0, extraSize);
		BinaryWriter_t3992595042 * L_2 = ___writer0;
		ByteU5BU5D_t4116647657* L_3 = __this->get_extraData_7();
		int16_t L_4 = ((WaveFormat_t2842237185 *)__this)->get_extraSize_6();
		// writer.Write(extraData, 0, extraSize);
		NullCheck(L_2);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(10 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, L_4);
	}

IL_0029:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NAudio.Wave.WZT.WaveStream::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaveStream__ctor_m3424999445 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveStream__ctor_m3424999445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1273022909_il2cpp_TypeInfo_var);
		Stream__ctor_m3881936881(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NAudio.Wave.WZT.WaveStream::get_CanRead()
extern "C" IL2CPP_METHOD_ATTR bool WaveStream_get_CanRead_m2860274087 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public override bool CanRead { get { return true; } }
		V_0 = (bool)1;
		goto IL_0008;
	}

IL_0008:
	{
		// public override bool CanRead { get { return true; } }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Boolean NAudio.Wave.WZT.WaveStream::get_CanSeek()
extern "C" IL2CPP_METHOD_ATTR bool WaveStream_get_CanSeek_m4164170462 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public override bool CanSeek { get { return true; } }
		V_0 = (bool)1;
		goto IL_0008;
	}

IL_0008:
	{
		// public override bool CanSeek { get { return true; } }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Boolean NAudio.Wave.WZT.WaveStream::get_CanWrite()
extern "C" IL2CPP_METHOD_ATTR bool WaveStream_get_CanWrite_m2652788188 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public override bool CanWrite { get { return false; } }
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		// public override bool CanWrite { get { return false; } }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Void NAudio.Wave.WZT.WaveStream::Flush()
extern "C" IL2CPP_METHOD_ATTR void WaveStream_Flush_m104084886 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	{
		// public override void Flush() { }
		return;
	}
}
// System.Int64 NAudio.Wave.WZT.WaveStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C" IL2CPP_METHOD_ATTR int64_t WaveStream_Seek_m4211447414 (WaveStream_t91297746 * __this, int64_t ___offset0, int32_t ___origin1, const RuntimeMethod* method)
{
	int64_t V_0 = 0;
	{
		// if (origin == SeekOrigin.Begin)
		int32_t L_0 = ___origin1;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// Position = offset;
		int64_t L_1 = ___offset0;
		// Position = offset;
		VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, L_1);
		goto IL_003b;
	}

IL_0013:
	{
		// else if (origin == SeekOrigin.Current)
		int32_t L_2 = ___origin1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		// Position += offset;
		// Position += offset;
		int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, __this);
		int64_t L_4 = ___offset0;
		// Position += offset;
		VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, ((int64_t)il2cpp_codegen_add((int64_t)L_3, (int64_t)L_4)));
		goto IL_003b;
	}

IL_002d:
	{
		// Position = Length + offset;
		// Position = Length + offset;
		int64_t L_5 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, __this);
		int64_t L_6 = ___offset0;
		// Position = Length + offset;
		VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, ((int64_t)il2cpp_codegen_add((int64_t)L_5, (int64_t)L_6)));
	}

IL_003b:
	{
		// return Position;
		// return Position;
		int64_t L_7 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, __this);
		V_0 = L_7;
		goto IL_0047;
	}

IL_0047:
	{
		// }
		int64_t L_8 = V_0;
		return L_8;
	}
}
// System.Void NAudio.Wave.WZT.WaveStream::SetLength(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void WaveStream_SetLength_m903629300 (WaveStream_t91297746 * __this, int64_t ___length0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveStream_SetLength_m903629300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// throw new NotSupportedException("Can't set length of a WaveFormatString");
		// throw new NotSupportedException("Can't set length of a WaveFormatString");
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_0, _stringLiteral2192099496, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,WaveStream_SetLength_m903629300_RuntimeMethod_var);
	}
}
// System.Void NAudio.Wave.WZT.WaveStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WaveStream_Write_m2960848016 (WaveStream_t91297746 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveStream_Write_m2960848016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// throw new NotSupportedException("Can't write to a WaveFormatString");
		// throw new NotSupportedException("Can't write to a WaveFormatString");
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_0, _stringLiteral3455142558, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,WaveStream_Write_m2960848016_RuntimeMethod_var);
	}
}
// System.Int32 NAudio.Wave.WZT.WaveStream::get_BlockAlign()
extern "C" IL2CPP_METHOD_ATTR int32_t WaveStream_get_BlockAlign_m3039469763 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return WaveFormat.BlockAlign;
		// return WaveFormat.BlockAlign;
		WaveFormat_t2842237185 * L_0 = VirtFuncInvoker0< WaveFormat_t2842237185 * >::Invoke(32 /* NAudio.Wave.WZT.WaveFormat NAudio.Wave.WZT.WaveStream::get_WaveFormat() */, __this);
		// return WaveFormat.BlockAlign;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 NAudio.Wave.WZT.WaveFormat::get_BlockAlign() */, L_0);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void NAudio.Wave.WZT.WaveStream::Skip(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WaveStream_Skip_m3631272947 (WaveStream_t91297746 * __this, int32_t ___seconds0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	int64_t V_2 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// lock (this)
		V_0 = __this;
		V_1 = (bool)0;
	}

IL_0005:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = V_0;
			Monitor_Enter_m984175629(NULL /*static, unused*/, L_0, (&V_1), /*hidden argument*/NULL);
			// long newPosition = Position + WaveFormat.AverageBytesPerSecond * seconds;
			// long newPosition = Position + WaveFormat.AverageBytesPerSecond * seconds;
			int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, __this);
			// long newPosition = Position + WaveFormat.AverageBytesPerSecond * seconds;
			WaveFormat_t2842237185 * L_2 = VirtFuncInvoker0< WaveFormat_t2842237185 * >::Invoke(32 /* NAudio.Wave.WZT.WaveFormat NAudio.Wave.WZT.WaveStream::get_WaveFormat() */, __this);
			// long newPosition = Position + WaveFormat.AverageBytesPerSecond * seconds;
			NullCheck(L_2);
			int32_t L_3 = WaveFormat_get_AverageBytesPerSecond_m3143431325(L_2, /*hidden argument*/NULL);
			int32_t L_4 = ___seconds0;
			V_2 = ((int64_t)il2cpp_codegen_add((int64_t)L_1, (int64_t)(((int64_t)((int64_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_3, (int32_t)L_4)))))));
			// if (newPosition > Length)
			int64_t L_5 = V_2;
			// if (newPosition > Length)
			int64_t L_6 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, __this);
			if ((((int64_t)L_5) <= ((int64_t)L_6)))
			{
				goto IL_0041;
			}
		}

IL_0030:
		{
			// Position = Length;
			// Position = Length;
			int64_t L_7 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, __this);
			// Position = Length;
			VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, L_7);
			goto IL_005d;
		}

IL_0041:
		{
			// else if (newPosition < 0)
			int64_t L_8 = V_2;
			if ((((int64_t)L_8) >= ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_0056;
			}
		}

IL_0049:
		{
			// Position = 0;
			// Position = 0;
			VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, (((int64_t)((int64_t)0))));
			goto IL_005d;
		}

IL_0056:
		{
			// Position = newPosition;
			int64_t L_9 = V_2;
			// Position = newPosition;
			VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, L_9);
		}

IL_005d:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_006c;
			}
		}

IL_0066:
		{
			RuntimeObject * L_11 = V_0;
			Monitor_Exit_m3585316909(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		}

IL_006c:
		{
			IL2CPP_END_FINALLY(99)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006d:
	{
		// }
		return;
	}
}
// System.TimeSpan NAudio.Wave.WZT.WaveStream::get_CurrentTime()
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  WaveStream_get_CurrentTime_m1063384241 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveStream_get_CurrentTime_m1063384241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t881159249  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return TimeSpan.FromSeconds((double)Position / WaveFormat.AverageBytesPerSecond);
		// return TimeSpan.FromSeconds((double)Position / WaveFormat.AverageBytesPerSecond);
		int64_t L_0 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, __this);
		// return TimeSpan.FromSeconds((double)Position / WaveFormat.AverageBytesPerSecond);
		WaveFormat_t2842237185 * L_1 = VirtFuncInvoker0< WaveFormat_t2842237185 * >::Invoke(32 /* NAudio.Wave.WZT.WaveFormat NAudio.Wave.WZT.WaveStream::get_WaveFormat() */, __this);
		// return TimeSpan.FromSeconds((double)Position / WaveFormat.AverageBytesPerSecond);
		NullCheck(L_1);
		int32_t L_2 = WaveFormat_get_AverageBytesPerSecond_m3143431325(L_1, /*hidden argument*/NULL);
		// return TimeSpan.FromSeconds((double)Position / WaveFormat.AverageBytesPerSecond);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_3 = TimeSpan_FromSeconds_m4219356874(NULL /*static, unused*/, ((double)((double)(((double)((double)L_0)))/(double)(((double)((double)L_2))))), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0020;
	}

IL_0020:
	{
		// }
		TimeSpan_t881159249  L_4 = V_0;
		return L_4;
	}
}
// System.Void NAudio.Wave.WZT.WaveStream::set_CurrentTime(System.TimeSpan)
extern "C" IL2CPP_METHOD_ATTR void WaveStream_set_CurrentTime_m2344576605 (WaveStream_t91297746 * __this, TimeSpan_t881159249  ___value0, const RuntimeMethod* method)
{
	{
		// Position = (long) (value.TotalSeconds * WaveFormat.AverageBytesPerSecond);
		// Position = (long) (value.TotalSeconds * WaveFormat.AverageBytesPerSecond);
		double L_0 = TimeSpan_get_TotalSeconds_m4083325051((&___value0), /*hidden argument*/NULL);
		// Position = (long) (value.TotalSeconds * WaveFormat.AverageBytesPerSecond);
		WaveFormat_t2842237185 * L_1 = VirtFuncInvoker0< WaveFormat_t2842237185 * >::Invoke(32 /* NAudio.Wave.WZT.WaveFormat NAudio.Wave.WZT.WaveStream::get_WaveFormat() */, __this);
		// Position = (long) (value.TotalSeconds * WaveFormat.AverageBytesPerSecond);
		NullCheck(L_1);
		int32_t L_2 = WaveFormat_get_AverageBytesPerSecond_m3143431325(L_1, /*hidden argument*/NULL);
		// Position = (long) (value.TotalSeconds * WaveFormat.AverageBytesPerSecond);
		VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, __this, (((int64_t)((int64_t)((double)il2cpp_codegen_multiply((double)L_0, (double)(((double)((double)L_2)))))))));
		// }
		return;
	}
}
// System.TimeSpan NAudio.Wave.WZT.WaveStream::get_TotalTime()
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  WaveStream_get_TotalTime_m2406271272 (WaveStream_t91297746 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveStream_get_TotalTime_m2406271272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t881159249  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// return TimeSpan.FromSeconds((double) Length / WaveFormat.AverageBytesPerSecond);
		// return TimeSpan.FromSeconds((double) Length / WaveFormat.AverageBytesPerSecond);
		int64_t L_0 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, __this);
		// return TimeSpan.FromSeconds((double) Length / WaveFormat.AverageBytesPerSecond);
		WaveFormat_t2842237185 * L_1 = VirtFuncInvoker0< WaveFormat_t2842237185 * >::Invoke(32 /* NAudio.Wave.WZT.WaveFormat NAudio.Wave.WZT.WaveStream::get_WaveFormat() */, __this);
		// return TimeSpan.FromSeconds((double) Length / WaveFormat.AverageBytesPerSecond);
		NullCheck(L_1);
		int32_t L_2 = WaveFormat_get_AverageBytesPerSecond_m3143431325(L_1, /*hidden argument*/NULL);
		// return TimeSpan.FromSeconds((double) Length / WaveFormat.AverageBytesPerSecond);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_3 = TimeSpan_FromSeconds_m4219356874(NULL /*static, unused*/, ((double)((double)(((double)((double)L_0)))/(double)(((double)((double)L_2))))), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0020;
	}

IL_0020:
	{
		// }
		TimeSpan_t881159249  L_4 = V_0;
		return L_4;
	}
}
// System.Boolean NAudio.Wave.WZT.WaveStream::HasData(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool WaveStream_HasData_m1713719930 (WaveStream_t91297746 * __this, int32_t ___count0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return Position < Length;
		// return Position < Length;
		int64_t L_0 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, __this);
		// return Position < Length;
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, __this);
		V_0 = (bool)((((int64_t)L_0) < ((int64_t)L_1))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		// }
		bool L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerData__ctor_m2650193907 (PlayerData_t220878115 * __this, const RuntimeMethod* method)
{
	{
		// public PlayerData()
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// this.isFirstTime = 0;
		__this->set_isFirstTime_0(0);
		// this.currentLevel = 1;
		__this->set_currentLevel_1(1);
		// this.posLevelCharacter = 0;
		__this->set_posLevelCharacter_2(0);
		// this.posX = -17.37f;
		__this->set_posX_4((-17.37f));
		// this.posY = -9.5f;
		__this->set_posY_5((-9.5f));
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Quest::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Quest__ctor_m430137165 (Quest_t3696879532 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quest__ctor_m430137165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string[] textActivity =  new string[4];
		__this->set_textActivity_5(((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)4)));
		// public bool[] storiesCompleted = new bool[4];
		__this->set_storiesCompleted_14(((BooleanU5BU5D_t2897418192*)SZArrayNew(BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Quest::Start()
extern "C" IL2CPP_METHOD_ATTR void Quest_Start_m1997759511 (Quest_t3696879532 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Quest::Update()
extern "C" IL2CPP_METHOD_ATTR void Quest_Update_m2531889508 (Quest_t3696879532 * __this, const RuntimeMethod* method)
{
	{
		// if (isStory)
		bool L_0 = __this->get_isStory_11();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
	}

IL_000e:
	{
		// if (isActivity)
		bool L_1 = __this->get_isActivity_4();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void Quest::StartQuest()
extern "C" IL2CPP_METHOD_ATTR void Quest_StartQuest_m2018510914 (Quest_t3696879532 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quest_StartQuest_m2018510914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// manager = FindObjectOfType<QuestManager>();
		// manager = FindObjectOfType<QuestManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		QuestManager_t588401851 * L_0 = Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var);
		__this->set_manager_3(L_0);
		// characterBehavior = FindObjectOfType<CharacterBehavior>();
		// characterBehavior = FindObjectOfType<CharacterBehavior>();
		CharacterBehavior_t2392107484 * L_1 = Object_FindObjectOfType_TisCharacterBehavior_t2392107484_m3992520573(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisCharacterBehavior_t2392107484_m3992520573_RuntimeMethod_var);
		__this->set_characterBehavior_16(L_1);
		// sceneChanger = FindObjectOfType<SceneChanger>();
		// sceneChanger = FindObjectOfType<SceneChanger>();
		SceneChanger_t1033871796 * L_2 = Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497_RuntimeMethod_var);
		__this->set_sceneChanger_17(L_2);
		// Debug.Log("estar quest: " + questID + ", select: " + isSelectMultiple + ", Story: " + isStory + ", Reto: " + isActivity);
		ObjectU5BU5D_t2843939325* L_3 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral476364545);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral476364545);
		ObjectU5BU5D_t2843939325* L_4 = L_3;
		int32_t L_5 = __this->get_questID_2();
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral2580790064);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral2580790064);
		ObjectU5BU5D_t2843939325* L_9 = L_8;
		bool L_10 = __this->get_isSelectMultiple_6();
		bool L_11 = L_10;
		RuntimeObject * L_12 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_12);
		ObjectU5BU5D_t2843939325* L_13 = L_9;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1619350777);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1619350777);
		ObjectU5BU5D_t2843939325* L_14 = L_13;
		bool L_15 = __this->get_isStory_11();
		bool L_16 = L_15;
		RuntimeObject * L_17 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_17);
		ObjectU5BU5D_t2843939325* L_18 = L_14;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral2129693546);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral2129693546);
		ObjectU5BU5D_t2843939325* L_19 = L_18;
		bool L_20 = __this->get_isActivity_4();
		bool L_21 = L_20;
		RuntimeObject * L_22 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_22);
		String_t* L_23 = String_Concat_m2971454694(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		// Debug.Log("estar quest: " + questID + ", select: " + isSelectMultiple + ", Story: " + isStory + ", Reto: " + isActivity);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		// if (isSelectMultiple)
		bool L_24 = __this->get_isSelectMultiple_6();
		if (!L_24)
		{
			goto IL_00a7;
		}
	}
	{
		// this.sceneName = "SelectMultipleScene";
		__this->set_sceneName_18(_stringLiteral1889752399);
		goto IL_00dc;
	}

IL_00a7:
	{
		// }else if (isStory)
		bool L_25 = __this->get_isStory_11();
		if (!L_25)
		{
			goto IL_00c4;
		}
	}
	{
		// this.sceneName = "QuestionScene";
		__this->set_sceneName_18(_stringLiteral3267731041);
		goto IL_00dc;
	}

IL_00c4:
	{
		// else if (isActivity)
		bool L_26 = __this->get_isActivity_4();
		if (!L_26)
		{
			goto IL_00dc;
		}
	}
	{
		// this.sceneName = "RetoScene";
		__this->set_sceneName_18(_stringLiteral2011406716);
	}

IL_00dc:
	{
		// manager.questID = questID;
		QuestManager_t588401851 * L_27 = __this->get_manager_3();
		int32_t L_28 = __this->get_questID_2();
		NullCheck(L_27);
		L_27->set_questID_5(L_28);
		// sceneChanger.ChangeSceneTo(this.sceneName);
		SceneChanger_t1033871796 * L_29 = __this->get_sceneChanger_17();
		String_t* L_30 = __this->get_sceneName_18();
		// sceneChanger.ChangeSceneTo(this.sceneName);
		NullCheck(L_29);
		SceneChanger_ChangeSceneTo_m2636143807(L_29, L_30, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Quest::QuestComplete()
extern "C" IL2CPP_METHOD_ATTR void Quest_QuestComplete_m1582949342 (Quest_t3696879532 * __this, const RuntimeMethod* method)
{
	{
		// manager.questCompleted[questID] = true;
		QuestManager_t588401851 * L_0 = __this->get_manager_3();
		NullCheck(L_0);
		BooleanU5BU5D_t2897418192* L_1 = L_0->get_questCompleted_3();
		int32_t L_2 = __this->get_questID_2();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (bool)1);
		// if ((characterBehavior.GetPos() == characterBehavior.level - 1) && (characterBehavior.GetPos() == questID) && (characterBehavior.level < 30))
		CharacterBehavior_t2392107484 * L_3 = __this->get_characterBehavior_16();
		// if ((characterBehavior.GetPos() == characterBehavior.level - 1) && (characterBehavior.GetPos() == questID) && (characterBehavior.level < 30))
		NullCheck(L_3);
		int32_t L_4 = CharacterBehavior_GetPos_m3016674883(L_3, /*hidden argument*/NULL);
		CharacterBehavior_t2392107484 * L_5 = __this->get_characterBehavior_16();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_level_4();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1))))))
		{
			goto IL_0066;
		}
	}
	{
		CharacterBehavior_t2392107484 * L_7 = __this->get_characterBehavior_16();
		// if ((characterBehavior.GetPos() == characterBehavior.level - 1) && (characterBehavior.GetPos() == questID) && (characterBehavior.level < 30))
		NullCheck(L_7);
		int32_t L_8 = CharacterBehavior_GetPos_m3016674883(L_7, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_questID_2();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0066;
		}
	}
	{
		CharacterBehavior_t2392107484 * L_10 = __this->get_characterBehavior_16();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_level_4();
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)30))))
		{
			goto IL_0066;
		}
	}
	{
		// characterBehavior.NextLevel();
		CharacterBehavior_t2392107484 * L_12 = __this->get_characterBehavior_16();
		// characterBehavior.NextLevel();
		NullCheck(L_12);
		CharacterBehavior_NextLevel_m3007643461(L_12, /*hidden argument*/NULL);
	}

IL_0066:
	{
		// manager.SaveData();
		QuestManager_t588401851 * L_13 = __this->get_manager_3();
		// manager.SaveData();
		NullCheck(L_13);
		QuestManager_SaveData_m3571505125(L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Quest::CompleteStory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Quest_CompleteStory_m3949629842 (Quest_t3696879532 * __this, int32_t ___posStory0, const RuntimeMethod* method)
{
	{
		// this.storiesCompleted[posStory] = true;
		BooleanU5BU5D_t2897418192* L_0 = __this->get_storiesCompleted_14();
		int32_t L_1 = ___posStory0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void QuestData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QuestData__ctor_m3020203002 (QuestData_t1216399150 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestData__ctor_m3020203002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool[] storiesCompleted = new bool[4];
		__this->set_storiesCompleted_1(((BooleanU5BU5D_t2897418192*)SZArrayNew(BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var, (uint32_t)4)));
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void QuestDataOld::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QuestDataOld__ctor_m1542677745 (QuestDataOld_t372650824 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestDataOld__ctor_m1542677745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string[] textActivity = new string[4];
		__this->set_textActivity_3(((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)4)));
		// public bool[] optionsSelected = new bool[4];
		__this->set_optionsSelected_8(((BooleanU5BU5D_t2897418192*)SZArrayNew(BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var, (uint32_t)4)));
		// private bool[] storiesCompleted = new bool[4];
		__this->set_storiesCompleted_12(((BooleanU5BU5D_t2897418192*)SZArrayNew(BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var, (uint32_t)4)));
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void QuestManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QuestManager__ctor_m3612029997 (QuestManager_t588401851 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestManager__ctor_m3612029997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool[] questCompleted = new bool[30];
		__this->set_questCompleted_3(((BooleanU5BU5D_t2897418192*)SZArrayNew(BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30))));
		// public int questID = 0;
		__this->set_questID_5(0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void QuestManager_Awake_m1471503576 (QuestManager_t588401851 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestManager_Awake_m1471503576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3328599146* V_0 = NULL;
	{
		// _instance = this;
		((QuestManager_t588401851_StaticFields*)il2cpp_codegen_static_fields_for(QuestManager_t588401851_il2cpp_TypeInfo_var))->set__instance_8(__this);
		// GameObject[] objs = GameObject.FindGameObjectsWithTag("QuestManager");
		// GameObject[] objs = GameObject.FindGameObjectsWithTag("QuestManager");
		GameObjectU5BU5D_t3328599146* L_0 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral3878544753, /*hidden argument*/NULL);
		V_0 = L_0;
		// if (objs.Length > 1)
		GameObjectU5BU5D_t3328599146* L_1 = V_0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		// Destroy(this.gameObject);
		// Destroy(this.gameObject);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		// Destroy(this.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0028:
	{
		// DontDestroyOnLoad(this.gameObject);
		// DontDestroyOnLoad(this.gameObject);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		// DontDestroyOnLoad(this.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void QuestManager::Start()
extern "C" IL2CPP_METHOD_ATTR void QuestManager_Start_m4127225011 (QuestManager_t588401851 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void QuestManager::Update()
extern "C" IL2CPP_METHOD_ATTR void QuestManager_Update_m3391237575 (QuestManager_t588401851 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void QuestManager::SaveData()
extern "C" IL2CPP_METHOD_ATTR void QuestManager_SaveData_m3571505125 (QuestManager_t588401851 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestManager_SaveData_m3571505125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gameController = FindObjectOfType<GameController>();
		// gameController = FindObjectOfType<GameController>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameController_t2330501625 * L_0 = Object_FindObjectOfType_TisGameController_t2330501625_m3040890899(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisGameController_t2330501625_m3040890899_RuntimeMethod_var);
		__this->set_gameController_7(L_0);
		// gameController.SaveProgress();
		GameController_t2330501625 * L_1 = __this->get_gameController_7();
		// gameController.SaveProgress();
		NullCheck(L_1);
		GameController_SaveProgress_m2661837419(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void QuestScene::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QuestScene__ctor_m448717681 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	{
		// float countdownTimeDefault = 30;
		__this->set_countdownTimeDefault_12((30.0f));
		// bool isactivityStarted = false;
		__this->set_isactivityStarted_14((bool)0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestScene::Start()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_Start_m4218677153 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_Start_m4218677153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Button_t4055032469 * V_5 = NULL;
	ButtonU5BU5D_t2297175928* V_6 = NULL;
	int32_t V_7 = 0;
	ColorBlock_t2139031574  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Scene_t2348375561  V_9;
	memset(&V_9, 0, sizeof(V_9));
	int32_t V_10 = 0;
	Scene_t2348375561  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		// sceneChanger = FindObjectOfType<SceneChanger>();
		// sceneChanger = FindObjectOfType<SceneChanger>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		SceneChanger_t1033871796 * L_0 = Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497_RuntimeMethod_var);
		__this->set_sceneChanger_19(L_0);
		// manager = FindObjectOfType<QuestManager>();
		// manager = FindObjectOfType<QuestManager>();
		QuestManager_t588401851 * L_1 = Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var);
		__this->set_manager_2(L_1);
		// quest = manager.quests[manager.questID];
		QuestManager_t588401851 * L_2 = __this->get_manager_2();
		NullCheck(L_2);
		QuestU5BU5D_t1937508389* L_3 = L_2->get_quests_2();
		QuestManager_t588401851 * L_4 = __this->get_manager_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_questID_5();
		NullCheck(L_3);
		int32_t L_6 = L_5;
		Quest_t3696879532 * L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_quest_3(L_7);
		// text.text = quest.startText.Replace("\\n", "\n");
		Text_t1901882714 * L_8 = __this->get_text_4();
		Quest_t3696879532 * L_9 = __this->get_quest_3();
		NullCheck(L_9);
		String_t* L_10 = L_9->get_startText_19();
		// text.text = quest.startText.Replace("\\n", "\n");
		NullCheck(L_10);
		String_t* L_11 = String_Replace_m1273907647(L_10, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		// text.text = quest.startText.Replace("\\n", "\n");
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_11);
		// if (SceneManager.GetActiveScene().name.Equals("SelectMultipleScene"))
		Scene_t2348375561  L_12 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_12;
		// if (SceneManager.GetActiveScene().name.Equals("SelectMultipleScene"))
		String_t* L_13 = Scene_get_name_m622963475((&V_0), /*hidden argument*/NULL);
		// if (SceneManager.GetActiveScene().name.Equals("SelectMultipleScene"))
		NullCheck(L_13);
		bool L_14 = String_Equals_m2270643605(L_13, _stringLiteral1889752399, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_01d4;
		}
	}
	{
		// for (int i = 0; i < options.Length; i++)
		V_1 = 0;
		goto IL_0091;
	}

IL_007d:
	{
		// options[i].enabled = true;
		ButtonU5BU5D_t2297175928* L_15 = __this->get_options_5();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Button_t4055032469 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		// options[i].enabled = true;
		NullCheck(L_18);
		Behaviour_set_enabled_m20417929(L_18, (bool)1, /*hidden argument*/NULL);
		// for (int i = 0; i < options.Length; i++)
		int32_t L_19 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0091:
	{
		// for (int i = 0; i < options.Length; i++)
		int32_t L_20 = V_1;
		ButtonU5BU5D_t2297175928* L_21 = __this->get_options_5();
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))))
		{
			goto IL_007d;
		}
	}
	{
		// for (int i = 0; i < quest.options.Length; i++)
		V_2 = 0;
		goto IL_00c6;
	}

IL_00a6:
	{
		// optionsText[i].text = quest.options[i];
		TextU5BU5D_t422084607* L_22 = __this->get_optionsText_6();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		Text_t1901882714 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		Quest_t3696879532 * L_26 = __this->get_quest_3();
		NullCheck(L_26);
		StringU5BU5D_t1281789340* L_27 = L_26->get_options_8();
		int32_t L_28 = V_2;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		String_t* L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		// optionsText[i].text = quest.options[i];
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_30);
		// for (int i = 0; i < quest.options.Length; i++)
		int32_t L_31 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_00c6:
	{
		// for (int i = 0; i < quest.options.Length; i++)
		int32_t L_32 = V_2;
		Quest_t3696879532 * L_33 = __this->get_quest_3();
		NullCheck(L_33);
		StringU5BU5D_t1281789340* L_34 = L_33->get_options_8();
		NullCheck(L_34);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length)))))))
		{
			goto IL_00a6;
		}
	}
	{
		// if (quest.options.Length < 4)
		Quest_t3696879532 * L_35 = __this->get_quest_3();
		NullCheck(L_35);
		StringU5BU5D_t1281789340* L_36 = L_35->get_options_8();
		NullCheck(L_36);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_36)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_012f;
		}
	}
	{
		// for (int i = quest.options.Length; i < 4; i++)
		Quest_t3696879532 * L_37 = __this->get_quest_3();
		NullCheck(L_37);
		StringU5BU5D_t1281789340* L_38 = L_37->get_options_8();
		NullCheck(L_38);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_38)->max_length))));
		goto IL_0127;
	}

IL_0100:
	{
		// options[i].enabled = false;
		ButtonU5BU5D_t2297175928* L_39 = __this->get_options_5();
		int32_t L_40 = V_3;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		Button_t4055032469 * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		// options[i].enabled = false;
		NullCheck(L_42);
		Behaviour_set_enabled_m20417929(L_42, (bool)0, /*hidden argument*/NULL);
		// options[i].gameObject.SetActive(false);
		ButtonU5BU5D_t2297175928* L_43 = __this->get_options_5();
		int32_t L_44 = V_3;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		Button_t4055032469 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		// options[i].gameObject.SetActive(false);
		NullCheck(L_46);
		GameObject_t1113636619 * L_47 = Component_get_gameObject_m442555142(L_46, /*hidden argument*/NULL);
		// options[i].gameObject.SetActive(false);
		NullCheck(L_47);
		GameObject_SetActive_m796801857(L_47, (bool)0, /*hidden argument*/NULL);
		// for (int i = quest.options.Length; i < 4; i++)
		int32_t L_48 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)1));
	}

IL_0127:
	{
		// for (int i = quest.options.Length; i < 4; i++)
		int32_t L_49 = V_3;
		if ((((int32_t)L_49) < ((int32_t)4)))
		{
			goto IL_0100;
		}
	}
	{
	}

IL_012f:
	{
		// if (manager.questCompleted[manager.questID])
		QuestManager_t588401851 * L_50 = __this->get_manager_2();
		NullCheck(L_50);
		BooleanU5BU5D_t2897418192* L_51 = L_50->get_questCompleted_3();
		QuestManager_t588401851 * L_52 = __this->get_manager_2();
		NullCheck(L_52);
		int32_t L_53 = L_52->get_questID_5();
		NullCheck(L_51);
		int32_t L_54 = L_53;
		uint8_t L_55 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		if (!L_55)
		{
			goto IL_01cf;
		}
	}
	{
		// int i = 0;
		V_4 = 0;
		// foreach (Button button in options)
		ButtonU5BU5D_t2297175928* L_56 = __this->get_options_5();
		V_6 = L_56;
		V_7 = 0;
		goto IL_01c3;
	}

IL_0160:
	{
		// foreach (Button button in options)
		ButtonU5BU5D_t2297175928* L_57 = V_6;
		int32_t L_58 = V_7;
		NullCheck(L_57);
		int32_t L_59 = L_58;
		Button_t4055032469 * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		V_5 = L_60;
		// if(i == quest.rigthOption)
		int32_t L_61 = V_4;
		Quest_t3696879532 * L_62 = __this->get_quest_3();
		NullCheck(L_62);
		int32_t L_63 = L_62->get_rigthOption_9();
		if ((!(((uint32_t)L_61) == ((uint32_t)L_63))))
		{
			goto IL_01ae;
		}
	}
	{
		// ColorBlock cb = button.colors;
		Button_t4055032469 * L_64 = V_5;
		// ColorBlock cb = button.colors;
		NullCheck(L_64);
		ColorBlock_t2139031574  L_65 = Selectable_get_colors_m2213868400(L_64, /*hidden argument*/NULL);
		V_8 = L_65;
		// cb.disabledColor = new Color(0.25f, 0.78f, 0.54f, 1.00f); //Color4f(0.25, 0.78, 0.54, 1.00)
		// cb.disabledColor = new Color(0.25f, 0.78f, 0.54f, 1.00f); //Color4f(0.25, 0.78, 0.54, 1.00)
		Color_t2555686324  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Color__ctor_m2943235014((&L_66), (0.25f), (0.78f), (0.54f), (1.0f), /*hidden argument*/NULL);
		// cb.disabledColor = new Color(0.25f, 0.78f, 0.54f, 1.00f); //Color4f(0.25, 0.78, 0.54, 1.00)
		ColorBlock_set_disabledColor_m2996580094((&V_8), L_66, /*hidden argument*/NULL);
		// button.colors = cb;
		Button_t4055032469 * L_67 = V_5;
		ColorBlock_t2139031574  L_68 = V_8;
		// button.colors = cb;
		NullCheck(L_67);
		Selectable_set_colors_m1384394609(L_67, L_68, /*hidden argument*/NULL);
	}

IL_01ae:
	{
		// button.interactable = false;
		Button_t4055032469 * L_69 = V_5;
		// button.interactable = false;
		NullCheck(L_69);
		Selectable_set_interactable_m3105888815(L_69, (bool)0, /*hidden argument*/NULL);
		// i++;
		int32_t L_70 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_70, (int32_t)1));
		// foreach (Button button in options)
		int32_t L_71 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_71, (int32_t)1));
	}

IL_01c3:
	{
		int32_t L_72 = V_7;
		ButtonU5BU5D_t2297175928* L_73 = V_6;
		NullCheck(L_73);
		if ((((int32_t)L_72) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_73)->max_length)))))))
		{
			goto IL_0160;
		}
	}
	{
	}

IL_01cf:
	{
		// return;
		goto IL_0411;
	}

IL_01d4:
	{
		// if (SceneManager.GetActiveScene().name.Equals("QuestionScene"))
		Scene_t2348375561  L_74 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_74;
		// if (SceneManager.GetActiveScene().name.Equals("QuestionScene"))
		String_t* L_75 = Scene_get_name_m622963475((&V_9), /*hidden argument*/NULL);
		// if (SceneManager.GetActiveScene().name.Equals("QuestionScene"))
		NullCheck(L_75);
		bool L_76 = String_Equals_m2270643605(L_75, _stringLiteral3267731041, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_029d;
		}
	}
	{
		// if (quest.isSelectMultiple)
		Quest_t3696879532 * L_77 = __this->get_quest_3();
		NullCheck(L_77);
		bool L_78 = L_77->get_isSelectMultiple_6();
		if (!L_78)
		{
			goto IL_0229;
		}
	}
	{
		// text.text = quest.completedText.Replace("\\n", "\n");
		Text_t1901882714 * L_79 = __this->get_text_4();
		Quest_t3696879532 * L_80 = __this->get_quest_3();
		NullCheck(L_80);
		String_t* L_81 = L_80->get_completedText_20();
		// text.text = quest.completedText.Replace("\\n", "\n");
		NullCheck(L_81);
		String_t* L_82 = String_Replace_m1273907647(L_81, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		// text.text = quest.completedText.Replace("\\n", "\n");
		NullCheck(L_79);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_79, L_82);
	}

IL_0229:
	{
		// audioManager = FindObjectOfType<AudioRecordManager>();
		// audioManager = FindObjectOfType<AudioRecordManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		AudioRecordManager_t3166594816 * L_83 = Object_FindObjectOfType_TisAudioRecordManager_t3166594816_m2959838367(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisAudioRecordManager_t3166594816_m2959838367_RuntimeMethod_var);
		__this->set_audioManager_7(L_83);
		// for (int i = 0; i < quest.stories.Length; i++)
		V_10 = 0;
		goto IL_0284;
	}

IL_023c:
	{
		// optionsText[i].text = quest.stories[i];
		TextU5BU5D_t422084607* L_84 = __this->get_optionsText_6();
		int32_t L_85 = V_10;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		Text_t1901882714 * L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		Quest_t3696879532 * L_88 = __this->get_quest_3();
		NullCheck(L_88);
		StringU5BU5D_t1281789340* L_89 = L_88->get_stories_13();
		int32_t L_90 = V_10;
		NullCheck(L_89);
		int32_t L_91 = L_90;
		String_t* L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		// optionsText[i].text = quest.stories[i];
		NullCheck(L_87);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_87, L_92);
		// if (quest.storiesCompleted[i])
		Quest_t3696879532 * L_93 = __this->get_quest_3();
		NullCheck(L_93);
		BooleanU5BU5D_t2897418192* L_94 = L_93->get_storiesCompleted_14();
		int32_t L_95 = V_10;
		NullCheck(L_94);
		int32_t L_96 = L_95;
		uint8_t L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		if (!L_97)
		{
			goto IL_027d;
		}
	}
	{
		// options[i].interactable = false;
		ButtonU5BU5D_t2297175928* L_98 = __this->get_options_5();
		int32_t L_99 = V_10;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		Button_t4055032469 * L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		// options[i].interactable = false;
		NullCheck(L_101);
		Selectable_set_interactable_m3105888815(L_101, (bool)0, /*hidden argument*/NULL);
	}

IL_027d:
	{
		// for (int i = 0; i < quest.stories.Length; i++)
		int32_t L_102 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_102, (int32_t)1));
	}

IL_0284:
	{
		// for (int i = 0; i < quest.stories.Length; i++)
		int32_t L_103 = V_10;
		Quest_t3696879532 * L_104 = __this->get_quest_3();
		NullCheck(L_104);
		StringU5BU5D_t1281789340* L_105 = L_104->get_stories_13();
		NullCheck(L_105);
		if ((((int32_t)L_103) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_105)->max_length)))))))
		{
			goto IL_023c;
		}
	}
	{
		// return;
		goto IL_0411;
	}

IL_029d:
	{
		// if (SceneManager.GetActiveScene().name.Equals("RetoScene"))
		Scene_t2348375561  L_106 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_11 = L_106;
		// if (SceneManager.GetActiveScene().name.Equals("RetoScene"))
		String_t* L_107 = Scene_get_name_m622963475((&V_11), /*hidden argument*/NULL);
		// if (SceneManager.GetActiveScene().name.Equals("RetoScene"))
		NullCheck(L_107);
		bool L_108 = String_Equals_m2270643605(L_107, _stringLiteral2011406716, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_0411;
		}
	}
	{
		// if (quest.isSelectMultiple)
		Quest_t3696879532 * L_109 = __this->get_quest_3();
		NullCheck(L_109);
		bool L_110 = L_109->get_isSelectMultiple_6();
		if (!L_110)
		{
			goto IL_0330;
		}
	}
	{
		// if(quest.optionsSelected == 0)
		Quest_t3696879532 * L_111 = __this->get_quest_3();
		NullCheck(L_111);
		int32_t L_112 = L_111->get_optionsSelected_10();
		if (L_112)
		{
			goto IL_0308;
		}
	}
	{
		// text.text = quest.completedText.Replace("\\n", "\n");
		Text_t1901882714 * L_113 = __this->get_text_4();
		Quest_t3696879532 * L_114 = __this->get_quest_3();
		NullCheck(L_114);
		String_t* L_115 = L_114->get_completedText_20();
		// text.text = quest.completedText.Replace("\\n", "\n");
		NullCheck(L_115);
		String_t* L_116 = String_Replace_m1273907647(L_115, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		// text.text = quest.completedText.Replace("\\n", "\n");
		NullCheck(L_113);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_113, L_116);
		goto IL_032f;
	}

IL_0308:
	{
		// text.text = quest.wrongText.Replace("\\n", "\n");
		Text_t1901882714 * L_117 = __this->get_text_4();
		Quest_t3696879532 * L_118 = __this->get_quest_3();
		NullCheck(L_118);
		String_t* L_119 = L_118->get_wrongText_21();
		// text.text = quest.wrongText.Replace("\\n", "\n");
		NullCheck(L_119);
		String_t* L_120 = String_Replace_m1273907647(L_119, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		// text.text = quest.wrongText.Replace("\\n", "\n");
		NullCheck(L_117);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_117, L_120);
	}

IL_032f:
	{
	}

IL_0330:
	{
		// if (!manager.questCompleted[manager.questID])
		QuestManager_t588401851 * L_121 = __this->get_manager_2();
		NullCheck(L_121);
		BooleanU5BU5D_t2897418192* L_122 = L_121->get_questCompleted_3();
		QuestManager_t588401851 * L_123 = __this->get_manager_2();
		NullCheck(L_123);
		int32_t L_124 = L_123->get_questID_5();
		NullCheck(L_122);
		int32_t L_125 = L_124;
		uint8_t L_126 = (L_122)->GetAt(static_cast<il2cpp_array_size_t>(L_125));
		if (L_126)
		{
			goto IL_03b0;
		}
	}
	{
		// completeActivity.SetActive(false);
		GameObject_t1113636619 * L_127 = __this->get_completeActivity_17();
		// completeActivity.SetActive(false);
		NullCheck(L_127);
		GameObject_SetActive_m796801857(L_127, (bool)0, /*hidden argument*/NULL);
		// incompleteActivity.SetActive(false);
		GameObject_t1113636619 * L_128 = __this->get_incompleteActivity_18();
		// incompleteActivity.SetActive(false);
		NullCheck(L_128);
		GameObject_SetActive_m796801857(L_128, (bool)0, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(true);
		Text_t1901882714 * L_129 = __this->get_countDownText_15();
		// countDownText.gameObject.SetActive(true);
		NullCheck(L_129);
		GameObject_t1113636619 * L_130 = Component_get_gameObject_m442555142(L_129, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(true);
		NullCheck(L_130);
		GameObject_SetActive_m796801857(L_130, (bool)1, /*hidden argument*/NULL);
		// startActivity.gameObject.SetActive(true);
		GameObject_t1113636619 * L_131 = __this->get_startActivity_16();
		// startActivity.gameObject.SetActive(true);
		NullCheck(L_131);
		GameObject_t1113636619 * L_132 = GameObject_get_gameObject_m3693461266(L_131, /*hidden argument*/NULL);
		// startActivity.gameObject.SetActive(true);
		NullCheck(L_132);
		GameObject_SetActive_m796801857(L_132, (bool)1, /*hidden argument*/NULL);
		// if (quest.isFinal)
		Quest_t3696879532 * L_133 = __this->get_quest_3();
		NullCheck(L_133);
		bool L_134 = L_133->get_isFinal_15();
		if (!L_134)
		{
			goto IL_03aa;
		}
	}
	{
		// countDownText.gameObject.SetActive(false);
		Text_t1901882714 * L_135 = __this->get_countDownText_15();
		// countDownText.gameObject.SetActive(false);
		NullCheck(L_135);
		GameObject_t1113636619 * L_136 = Component_get_gameObject_m442555142(L_135, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(false);
		NullCheck(L_136);
		GameObject_SetActive_m796801857(L_136, (bool)0, /*hidden argument*/NULL);
	}

IL_03aa:
	{
		goto IL_040c;
	}

IL_03b0:
	{
		// completeActivity.SetActive(true);
		GameObject_t1113636619 * L_137 = __this->get_completeActivity_17();
		// completeActivity.SetActive(true);
		NullCheck(L_137);
		GameObject_SetActive_m796801857(L_137, (bool)1, /*hidden argument*/NULL);
		// completeActivity.GetComponent<Button>().interactable = false;
		GameObject_t1113636619 * L_138 = __this->get_completeActivity_17();
		// completeActivity.GetComponent<Button>().interactable = false;
		NullCheck(L_138);
		Button_t4055032469 * L_139 = GameObject_GetComponent_TisButton_t4055032469_m1515138076(L_138, /*hidden argument*/GameObject_GetComponent_TisButton_t4055032469_m1515138076_RuntimeMethod_var);
		// completeActivity.GetComponent<Button>().interactable = false;
		NullCheck(L_139);
		Selectable_set_interactable_m3105888815(L_139, (bool)0, /*hidden argument*/NULL);
		// completeActivity.transform.position = startActivity.transform.position;
		GameObject_t1113636619 * L_140 = __this->get_completeActivity_17();
		// completeActivity.transform.position = startActivity.transform.position;
		NullCheck(L_140);
		Transform_t3600365921 * L_141 = GameObject_get_transform_m1369836730(L_140, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_142 = __this->get_startActivity_16();
		// completeActivity.transform.position = startActivity.transform.position;
		NullCheck(L_142);
		Transform_t3600365921 * L_143 = GameObject_get_transform_m1369836730(L_142, /*hidden argument*/NULL);
		// completeActivity.transform.position = startActivity.transform.position;
		NullCheck(L_143);
		Vector3_t3722313464  L_144 = Transform_get_position_m36019626(L_143, /*hidden argument*/NULL);
		// completeActivity.transform.position = startActivity.transform.position;
		NullCheck(L_141);
		Transform_set_position_m3387557959(L_141, L_144, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(false);
		Text_t1901882714 * L_145 = __this->get_countDownText_15();
		// countDownText.gameObject.SetActive(false);
		NullCheck(L_145);
		GameObject_t1113636619 * L_146 = Component_get_gameObject_m442555142(L_145, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(false);
		NullCheck(L_146);
		GameObject_SetActive_m796801857(L_146, (bool)0, /*hidden argument*/NULL);
		// startActivity.SetActive(false);
		GameObject_t1113636619 * L_147 = __this->get_startActivity_16();
		// startActivity.SetActive(false);
		NullCheck(L_147);
		GameObject_SetActive_m796801857(L_147, (bool)0, /*hidden argument*/NULL);
	}

IL_040c:
	{
		// return;
		goto IL_0411;
	}

IL_0411:
	{
		// }
		return;
	}
}
// System.Void QuestScene::Update()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_Update_m786752971 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_Update_m786752971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (isactivityStarted)
		bool L_0 = __this->get_isactivityStarted_14();
		if (!L_0)
		{
			goto IL_006f;
		}
	}
	{
		// elapsedTime += Time.deltaTime;
		float L_1 = __this->get_elapsedTime_11();
		// elapsedTime += Time.deltaTime;
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_elapsedTime_11(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		// if (Mathf.RoundToInt(elapsedTime) > countdownTimeDefault)
		float L_3 = __this->get_elapsedTime_11();
		// if (Mathf.RoundToInt(elapsedTime) > countdownTimeDefault)
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_countdownTimeDefault_12();
		if ((!(((float)(((float)((float)L_4)))) > ((float)L_5))))
		{
			goto IL_0045;
		}
	}
	{
		// isactivityStarted = false;
		__this->set_isactivityStarted_14((bool)0);
		// FinishActivity();
		// FinishActivity();
		QuestScene_FinishActivity_m545023592(__this, /*hidden argument*/NULL);
	}

IL_0045:
	{
		// countDownText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime - countdownTime), true);
		Text_t1901882714 * L_6 = __this->get_countDownText_15();
		// countDownText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime - countdownTime), true);
		NullCheck(L_6);
		Text_t1901882714 * L_7 = Component_GetComponent_TisText_t1901882714_m4196288697(L_6, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		float L_8 = __this->get_elapsedTime_11();
		float L_9 = __this->get_countdownTime_13();
		// countDownText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime - countdownTime), true);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_subtract((float)L_8, (float)L_9)), /*hidden argument*/NULL);
		// countDownText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime - countdownTime), true);
		String_t* L_11 = QuestScene_FormatTime_m3401522192(__this, L_10, (bool)1, /*hidden argument*/NULL);
		// countDownText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime - countdownTime), true);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_11);
	}

IL_006f:
	{
		// }
		return;
	}
}
// System.Void QuestScene::selectOption(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void QuestScene_selectOption_m209496874 (QuestScene_t3272400377 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_selectOption_m209496874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// quest.optionsSelected = id;
		Quest_t3696879532 * L_0 = __this->get_quest_3();
		int32_t L_1 = ___id0;
		NullCheck(L_0);
		L_0->set_optionsSelected_10(L_1);
		// if (id == quest.rigthOption)
		int32_t L_2 = ___id0;
		Quest_t3696879532 * L_3 = __this->get_quest_3();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_rigthOption_9();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_00dd;
		}
	}
	{
		// if (quest.isActivity)
		Quest_t3696879532 * L_5 = __this->get_quest_3();
		NullCheck(L_5);
		bool L_6 = L_5->get_isActivity_4();
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("RetoScene");
		SceneChanger_t1033871796 * L_7 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("RetoScene");
		NullCheck(L_7);
		SceneChanger_ChangeSceneTo_m2636143807(L_7, _stringLiteral2011406716, /*hidden argument*/NULL);
		// return;
		goto IL_01ad;
	}

IL_0045:
	{
		// if (quest.isStory)
		Quest_t3696879532 * L_8 = __this->get_quest_3();
		NullCheck(L_8);
		bool L_9 = L_8->get_isStory_11();
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		// selectStory(0);
		// selectStory(0);
		QuestScene_selectStory_m3040738564(__this, 0, /*hidden argument*/NULL);
		// return;
		goto IL_01ad;
	}

IL_0062:
	{
		// options[id].GetComponent<Image>().color = new Color(0.25f, 0.78f, 0.54f, 1.00f);
		ButtonU5BU5D_t2297175928* L_10 = __this->get_options_5();
		int32_t L_11 = ___id0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Button_t4055032469 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		// options[id].GetComponent<Image>().color = new Color(0.25f, 0.78f, 0.54f, 1.00f);
		NullCheck(L_13);
		Image_t2670269651 * L_14 = Component_GetComponent_TisImage_t2670269651_m980647750(L_13, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		// options[id].GetComponent<Image>().color = new Color(0.25f, 0.78f, 0.54f, 1.00f);
		Color_t2555686324  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m2943235014((&L_15), (0.25f), (0.78f), (0.54f), (1.0f), /*hidden argument*/NULL);
		// options[id].GetComponent<Image>().color = new Color(0.25f, 0.78f, 0.54f, 1.00f);
		NullCheck(L_14);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_14, L_15);
		// quest.QuestComplete();
		Quest_t3696879532 * L_16 = __this->get_quest_3();
		// quest.QuestComplete();
		NullCheck(L_16);
		Quest_QuestComplete_m1582949342(L_16, /*hidden argument*/NULL);
		// if(quest.completedText.Length > 0)
		Quest_t3696879532 * L_17 = __this->get_quest_3();
		NullCheck(L_17);
		String_t* L_18 = L_17->get_completedText_20();
		// if(quest.completedText.Length > 0)
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m3847582255(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_00c5;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		SceneChanger_t1033871796 * L_20 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		NullCheck(L_20);
		SceneChanger_ChangeSceneTo_m2636143807(L_20, _stringLiteral2909207532, /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_00c5:
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_21 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_21);
		SceneChanger_ChangeSceneTo_m2636143807(L_21, _stringLiteral1853239370, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		goto IL_01ad;
	}

IL_00dd:
	{
		// Debug.Log("wrong answer, is activity: "+ quest.isActivity);
		Quest_t3696879532 * L_22 = __this->get_quest_3();
		NullCheck(L_22);
		bool L_23 = L_22->get_isActivity_4();
		bool L_24 = L_23;
		RuntimeObject * L_25 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_24);
		String_t* L_26 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2810867248, L_25, /*hidden argument*/NULL);
		// Debug.Log("wrong answer, is activity: "+ quest.isActivity);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		// if(quest.isActivity)
		Quest_t3696879532 * L_27 = __this->get_quest_3();
		NullCheck(L_27);
		bool L_28 = L_27->get_isActivity_4();
		if (!L_28)
		{
			goto IL_014e;
		}
	}
	{
		// Debug.Log("entro a quest.isActivity " + quest.questID);
		Quest_t3696879532 * L_29 = __this->get_quest_3();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_questID_2();
		int32_t L_31 = L_30;
		RuntimeObject * L_32 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_31);
		String_t* L_33 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2756285813, L_32, /*hidden argument*/NULL);
		// Debug.Log("entro a quest.isActivity " + quest.questID);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		// quest.rigthOption = id;
		Quest_t3696879532 * L_34 = __this->get_quest_3();
		int32_t L_35 = ___id0;
		NullCheck(L_34);
		L_34->set_rigthOption_9(L_35);
		// sceneChanger.ChangeSceneTo("RetoScene");
		SceneChanger_t1033871796 * L_36 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("RetoScene");
		NullCheck(L_36);
		SceneChanger_ChangeSceneTo_m2636143807(L_36, _stringLiteral2011406716, /*hidden argument*/NULL);
		// return;
		goto IL_01ad;
	}

IL_014e:
	{
		// if (quest.wrongText.Length > 0)
		Quest_t3696879532 * L_37 = __this->get_quest_3();
		NullCheck(L_37);
		String_t* L_38 = L_37->get_wrongText_21();
		// if (quest.wrongText.Length > 0)
		NullCheck(L_38);
		int32_t L_39 = String_get_Length_m3847582255(L_38, /*hidden argument*/NULL);
		if ((((int32_t)L_39) <= ((int32_t)0)))
		{
			goto IL_019a;
		}
	}
	{
		// Debug.Log("entro a quest.isActivity " + quest.questID);
		Quest_t3696879532 * L_40 = __this->get_quest_3();
		NullCheck(L_40);
		int32_t L_41 = L_40->get_questID_2();
		int32_t L_42 = L_41;
		RuntimeObject * L_43 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_42);
		String_t* L_44 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2756285813, L_43, /*hidden argument*/NULL);
		// Debug.Log("entro a quest.isActivity " + quest.questID);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		SceneChanger_t1033871796 * L_45 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		NullCheck(L_45);
		SceneChanger_ChangeSceneTo_m2636143807(L_45, _stringLiteral2909207532, /*hidden argument*/NULL);
		goto IL_01ac;
	}

IL_019a:
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_46 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_46);
		SceneChanger_ChangeSceneTo_m2636143807(L_46, _stringLiteral1853239370, /*hidden argument*/NULL);
	}

IL_01ac:
	{
	}

IL_01ad:
	{
		// }
		return;
	}
}
// System.Void QuestScene::selectStory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void QuestScene_selectStory_m3040738564 (QuestScene_t3272400377 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_selectStory_m3040738564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// manager.storySelected = id;
		QuestManager_t588401851 * L_0 = __this->get_manager_2();
		int32_t L_1 = ___id0;
		NullCheck(L_0);
		L_0->set_storySelected_6(L_1);
		// sceneChanger.ChangeSceneTo("AudioScene");
		SceneChanger_t1033871796 * L_2 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("AudioScene");
		NullCheck(L_2);
		SceneChanger_ChangeSceneTo_m2636143807(L_2, _stringLiteral1585816881, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void QuestScene::startRecord(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void QuestScene_startRecord_m2692324037 (QuestScene_t3272400377 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		// idStory = id;
		int32_t L_0 = ___id0;
		__this->set_idStory_8(L_0);
		// audioManager.OnStartMicButtonClick();
		AudioRecordManager_t3166594816 * L_1 = __this->get_audioManager_7();
		// audioManager.OnStartMicButtonClick();
		NullCheck(L_1);
		AudioRecordManager_OnStartMicButtonClick_m1918784328(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void QuestScene::stopRecord()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_stopRecord_m1421632293 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void QuestScene::StartActivity()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_StartActivity_m3349551200 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_StartActivity_m3349551200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (quest.isFinal)
		Quest_t3696879532 * L_0 = __this->get_quest_3();
		NullCheck(L_0);
		bool L_1 = L_0->get_isFinal_15();
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("PuzzleScene");
		SceneChanger_t1033871796 * L_2 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("PuzzleScene");
		NullCheck(L_2);
		SceneChanger_ChangeSceneTo_m2636143807(L_2, _stringLiteral2099292225, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// isactivityStarted = true;
		__this->set_isactivityStarted_14((bool)1);
		// elapsedTime = 0;
		__this->set_elapsedTime_11((0.0f));
		// countdownTime = countdownTimeDefault;
		float L_3 = __this->get_countdownTimeDefault_12();
		__this->set_countdownTime_13(L_3);
		// startActivity.SetActive(false);
		GameObject_t1113636619 * L_4 = __this->get_startActivity_16();
		// startActivity.SetActive(false);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void QuestScene::FinishActivity()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_FinishActivity_m545023592 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	{
		// completeActivity.SetActive(true);
		GameObject_t1113636619 * L_0 = __this->get_completeActivity_17();
		// completeActivity.SetActive(true);
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		// incompleteActivity.SetActive(true);
		GameObject_t1113636619 * L_1 = __this->get_incompleteActivity_18();
		// incompleteActivity.SetActive(true);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(false);
		Text_t1901882714 * L_2 = __this->get_countDownText_15();
		// countDownText.gameObject.SetActive(false);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		// countDownText.gameObject.SetActive(false);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String QuestScene::FormatTime(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR String_t* QuestScene_FormatTime_m3401522192 (QuestScene_t3272400377 * __this, int32_t ___time0, bool ___countdownd1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_FormatTime_m3401522192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	int32_t G_B3_0 = 0;
	{
		// int mins = time / 60;
		int32_t L_0 = ___time0;
		V_0 = ((int32_t)((int32_t)L_0/(int32_t)((int32_t)60)));
		// int secs = countdownd ? time % 60 * -1 : time % 60;
		bool L_1 = ___countdownd1;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_2 = ___time0;
		G_B3_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)((int32_t)L_2%(int32_t)((int32_t)60))), (int32_t)(-1)));
		goto IL_001b;
	}

IL_0017:
	{
		int32_t L_3 = ___time0;
		G_B3_0 = ((int32_t)((int32_t)L_3%(int32_t)((int32_t)60)));
	}

IL_001b:
	{
		V_1 = G_B3_0;
		// string timeFormat = string.Format("TIEMPO:  {0}:{1}", mins, secs);
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_5);
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_8);
		// string timeFormat = string.Format("TIEMPO:  {0}:{1}", mins, secs);
		String_t* L_10 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral2582580331, L_6, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		// return timeFormat;
		String_t* L_11 = V_2;
		V_3 = L_11;
		goto IL_003a;
	}

IL_003a:
	{
		// }
		String_t* L_12 = V_3;
		return L_12;
	}
}
// System.Void QuestScene::CompleteActivity()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_CompleteActivity_m3524613363 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_CompleteActivity_m3524613363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// quest.QuestComplete();
		Quest_t3696879532 * L_0 = __this->get_quest_3();
		// quest.QuestComplete();
		NullCheck(L_0);
		Quest_QuestComplete_m1582949342(L_0, /*hidden argument*/NULL);
		// if (quest.isSelectMultiple)
		Quest_t3696879532 * L_1 = __this->get_quest_3();
		NullCheck(L_1);
		bool L_2 = L_1->get_isSelectMultiple_6();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_3 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_3);
		SceneChanger_ChangeSceneTo_m2636143807(L_3, _stringLiteral1853239370, /*hidden argument*/NULL);
		// return;
		goto IL_0071;
	}

IL_0032:
	{
		// if (quest.completedText.Length > 0)
		Quest_t3696879532 * L_4 = __this->get_quest_3();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_completedText_20();
		// if (quest.completedText.Length > 0)
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m3847582255(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_005f;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		SceneChanger_t1033871796 * L_7 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		NullCheck(L_7);
		SceneChanger_ChangeSceneTo_m2636143807(L_7, _stringLiteral2909207532, /*hidden argument*/NULL);
		goto IL_0071;
	}

IL_005f:
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_8 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_8);
		SceneChanger_ChangeSceneTo_m2636143807(L_8, _stringLiteral1853239370, /*hidden argument*/NULL);
	}

IL_0071:
	{
		// }
		return;
	}
}
// System.Void QuestScene::IncompleteActivity()
extern "C" IL2CPP_METHOD_ATTR void QuestScene_IncompleteActivity_m1001130087 (QuestScene_t3272400377 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestScene_IncompleteActivity_m1001130087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (quest.isSelectMultiple)
		Quest_t3696879532 * L_0 = __this->get_quest_3();
		NullCheck(L_0);
		bool L_1 = L_0->get_isSelectMultiple_6();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_2 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_2);
		SceneChanger_ChangeSceneTo_m2636143807(L_2, _stringLiteral1853239370, /*hidden argument*/NULL);
		// return;
		goto IL_0066;
	}

IL_0027:
	{
		// if (quest.wrongText.Length > 0)
		Quest_t3696879532 * L_3 = __this->get_quest_3();
		NullCheck(L_3);
		String_t* L_4 = L_3->get_wrongText_21();
		// if (quest.wrongText.Length > 0)
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m3847582255(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		SceneChanger_t1033871796 * L_6 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		NullCheck(L_6);
		SceneChanger_ChangeSceneTo_m2636143807(L_6, _stringLiteral2909207532, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0054:
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_7 = __this->get_sceneChanger_19();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_7);
		SceneChanger_ChangeSceneTo_m2636143807(L_7, _stringLiteral1853239370, /*hidden argument*/NULL);
	}

IL_0066:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void QuestTrigger::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QuestTrigger__ctor_m1306723910 (QuestTrigger_t3125687900 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestTrigger::Start()
extern "C" IL2CPP_METHOD_ATTR void QuestTrigger_Start_m1906426316 (QuestTrigger_t3125687900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestTrigger_Start_m1906426316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// manager = FindObjectOfType<QuestManager>();
		// manager = FindObjectOfType<QuestManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		QuestManager_t588401851 * L_0 = Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var);
		__this->set_manager_2(L_0);
		// }
		return;
	}
}
// System.Void QuestTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void QuestTrigger_OnTriggerEnter2D_m3562265525 (QuestTrigger_t3125687900 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestTrigger_OnTriggerEnter2D_m3562265525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (collision.gameObject.tag.Equals("Player"))
		Collider2D_t2806799626 * L_0 = ___collision0;
		// if (collision.gameObject.tag.Equals("Player"))
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		// if (collision.gameObject.tag.Equals("Player"))
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m3951609671(L_1, /*hidden argument*/NULL);
		// if (collision.gameObject.tag.Equals("Player"))
		NullCheck(L_2);
		bool L_3 = String_Equals_m2270643605(L_2, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		// if (manager.questID == questID)
		QuestManager_t588401851 * L_4 = __this->get_manager_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_questID_5();
		int32_t L_6 = __this->get_questID_3();
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0038;
		}
	}
	{
		// return;
		goto IL_0048;
	}

IL_0038:
	{
		// StartCoroutine(DelayStartQuest());
		// StartCoroutine(DelayStartQuest());
		RuntimeObject* L_7 = QuestTrigger_DelayStartQuest_m4146236069(__this, /*hidden argument*/NULL);
		// StartCoroutine(DelayStartQuest());
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_7, /*hidden argument*/NULL);
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator QuestTrigger::DelayStartQuest()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* QuestTrigger_DelayStartQuest_m4146236069 (QuestTrigger_t3125687900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestTrigger_DelayStartQuest_m4146236069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDelayStartQuestU3Ec__Iterator0_t39561174 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CDelayStartQuestU3Ec__Iterator0_t39561174 * L_0 = (U3CDelayStartQuestU3Ec__Iterator0_t39561174 *)il2cpp_codegen_object_new(U3CDelayStartQuestU3Ec__Iterator0_t39561174_il2cpp_TypeInfo_var);
		U3CDelayStartQuestU3Ec__Iterator0__ctor_m861285505(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDelayStartQuestU3Ec__Iterator0_t39561174 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CDelayStartQuestU3Ec__Iterator0_t39561174 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_3 = V_1;
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void QuestTrigger/<DelayStartQuest>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayStartQuestU3Ec__Iterator0__ctor_m861285505 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean QuestTrigger/<DelayStartQuest>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CDelayStartQuestU3Ec__Iterator0_MoveNext_m434194100 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayStartQuestU3Ec__Iterator0_MoveNext_m434194100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0046;
			}
		}
	}
	{
		goto IL_006e;
	}

IL_0021:
	{
		// yield return new WaitForSeconds(0.5f);
		// yield return new WaitForSeconds(0.5f);
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0041:
	{
		goto IL_0070;
	}

IL_0046:
	{
		// manager.quests[questID].StartQuest();
		QuestTrigger_t3125687900 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		QuestManager_t588401851 * L_5 = L_4->get_manager_2();
		NullCheck(L_5);
		QuestU5BU5D_t1937508389* L_6 = L_5->get_quests_2();
		QuestTrigger_t3125687900 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_questID_3();
		NullCheck(L_6);
		int32_t L_9 = L_8;
		Quest_t3696879532 * L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		// manager.quests[questID].StartQuest();
		NullCheck(L_10);
		Quest_StartQuest_m2018510914(L_10, /*hidden argument*/NULL);
		// }
		__this->set_U24PC_3((-1));
	}

IL_006e:
	{
		return (bool)0;
	}

IL_0070:
	{
		return (bool)1;
	}
}
// System.Object QuestTrigger/<DelayStartQuest>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayStartQuestU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m700725668 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object QuestTrigger/<DelayStartQuest>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayStartQuestU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2600383256 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void QuestTrigger/<DelayStartQuest>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayStartQuestU3Ec__Iterator0_Dispose_m1942338222 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void QuestTrigger/<DelayStartQuest>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayStartQuestU3Ec__Iterator0_Reset_m85328022 (U3CDelayStartQuestU3Ec__Iterator0_t39561174 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayStartQuestU3Ec__Iterator0_Reset_m85328022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CDelayStartQuestU3Ec__Iterator0_Reset_m85328022_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ResultQuestScene::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ResultQuestScene__ctor_m756813208 (ResultQuestScene_t315437199 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ResultQuestScene::Start()
extern "C" IL2CPP_METHOD_ATTR void ResultQuestScene_Start_m2660546872 (ResultQuestScene_t315437199 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ResultQuestScene_Start_m2660546872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t1901882714 * G_B3_0 = NULL;
	Text_t1901882714 * G_B2_0 = NULL;
	String_t* G_B4_0 = NULL;
	Text_t1901882714 * G_B4_1 = NULL;
	Text_t1901882714 * G_B9_0 = NULL;
	Text_t1901882714 * G_B8_0 = NULL;
	String_t* G_B10_0 = NULL;
	Text_t1901882714 * G_B10_1 = NULL;
	{
		// sceneChanger = FindObjectOfType<SceneChanger>();
		// sceneChanger = FindObjectOfType<SceneChanger>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		SceneChanger_t1033871796 * L_0 = Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisSceneChanger_t1033871796_m2085528497_RuntimeMethod_var);
		__this->set_sceneChanger_5(L_0);
		// manager = FindObjectOfType<QuestManager>();
		// manager = FindObjectOfType<QuestManager>();
		QuestManager_t588401851 * L_1 = Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var);
		__this->set_manager_2(L_1);
		// quest = manager.quests[manager.questID];
		QuestManager_t588401851 * L_2 = __this->get_manager_2();
		NullCheck(L_2);
		QuestU5BU5D_t1937508389* L_3 = L_2->get_quests_2();
		QuestManager_t588401851 * L_4 = __this->get_manager_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_questID_5();
		NullCheck(L_3);
		int32_t L_6 = L_5;
		Quest_t3696879532 * L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_quest_3(L_7);
		// if (!quest.isStory)
		Quest_t3696879532 * L_8 = __this->get_quest_3();
		NullCheck(L_8);
		bool L_9 = L_8->get_isStory_11();
		if (L_9)
		{
			goto IL_00ab;
		}
	}
	{
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		Text_t1901882714 * L_10 = __this->get_text_4();
		QuestManager_t588401851 * L_11 = __this->get_manager_2();
		NullCheck(L_11);
		BooleanU5BU5D_t2897418192* L_12 = L_11->get_questCompleted_3();
		QuestManager_t588401851 * L_13 = __this->get_manager_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_questID_5();
		NullCheck(L_12);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		G_B2_0 = L_10;
		if (!L_16)
		{
			G_B3_0 = L_10;
			goto IL_0086;
		}
	}
	{
		Quest_t3696879532 * L_17 = __this->get_quest_3();
		NullCheck(L_17);
		String_t* L_18 = L_17->get_completedText_20();
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		NullCheck(L_18);
		String_t* L_19 = String_Replace_m1273907647(L_18, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		G_B4_0 = L_19;
		G_B4_1 = G_B2_0;
		goto IL_00a0;
	}

IL_0086:
	{
		Quest_t3696879532 * L_20 = __this->get_quest_3();
		NullCheck(L_20);
		String_t* L_21 = L_20->get_wrongText_21();
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		NullCheck(L_21);
		String_t* L_22 = String_Replace_m1273907647(L_21, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		G_B4_0 = L_22;
		G_B4_1 = G_B3_0;
	}

IL_00a0:
	{
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		NullCheck(G_B4_1);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B4_1, G_B4_0);
		goto IL_0159;
	}

IL_00ab:
	{
		// else if(quest.isStory && quest.isSelectMultiple)
		Quest_t3696879532 * L_23 = __this->get_quest_3();
		NullCheck(L_23);
		bool L_24 = L_23->get_isStory_11();
		if (!L_24)
		{
			goto IL_0132;
		}
	}
	{
		Quest_t3696879532 * L_25 = __this->get_quest_3();
		NullCheck(L_25);
		bool L_26 = L_25->get_isSelectMultiple_6();
		if (!L_26)
		{
			goto IL_0132;
		}
	}
	{
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		Text_t1901882714 * L_27 = __this->get_text_4();
		QuestManager_t588401851 * L_28 = __this->get_manager_2();
		NullCheck(L_28);
		BooleanU5BU5D_t2897418192* L_29 = L_28->get_questCompleted_3();
		QuestManager_t588401851 * L_30 = __this->get_manager_2();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_questID_5();
		NullCheck(L_29);
		int32_t L_32 = L_31;
		uint8_t L_33 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		G_B8_0 = L_27;
		if (!L_33)
		{
			G_B9_0 = L_27;
			goto IL_010d;
		}
	}
	{
		Quest_t3696879532 * L_34 = __this->get_quest_3();
		NullCheck(L_34);
		String_t* L_35 = L_34->get_completedText_20();
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		NullCheck(L_35);
		String_t* L_36 = String_Replace_m1273907647(L_35, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		G_B10_0 = L_36;
		G_B10_1 = G_B8_0;
		goto IL_0127;
	}

IL_010d:
	{
		Quest_t3696879532 * L_37 = __this->get_quest_3();
		NullCheck(L_37);
		String_t* L_38 = L_37->get_wrongText_21();
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		NullCheck(L_38);
		String_t* L_39 = String_Replace_m1273907647(L_38, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		G_B10_0 = L_39;
		G_B10_1 = G_B9_0;
	}

IL_0127:
	{
		// text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
		NullCheck(G_B10_1);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B10_1, G_B10_0);
		goto IL_0159;
	}

IL_0132:
	{
		// text.text = quest.completedText.Replace("\\n", "\n");
		Text_t1901882714 * L_40 = __this->get_text_4();
		Quest_t3696879532 * L_41 = __this->get_quest_3();
		NullCheck(L_41);
		String_t* L_42 = L_41->get_completedText_20();
		// text.text = quest.completedText.Replace("\\n", "\n");
		NullCheck(L_42);
		String_t* L_43 = String_Replace_m1273907647(L_42, _stringLiteral3454842868, _stringLiteral3452614566, /*hidden argument*/NULL);
		// text.text = quest.completedText.Replace("\\n", "\n");
		NullCheck(L_40);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_40, L_43);
	}

IL_0159:
	{
		// }
		return;
	}
}
// System.Void ResultQuestScene::Update()
extern "C" IL2CPP_METHOD_ATTR void ResultQuestScene_Update_m541932322 (ResultQuestScene_t315437199 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SavWav::Save(System.String,UnityEngine.AudioClip,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool SavWav_Save_m332214922 (RuntimeObject * __this /* static, unused */, String_t* ___filepath0, AudioClip_t3680889665 * ___clip1, int32_t ___samples2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavWav_Save_m332214922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FileStream_t4292183065 * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// Debug.LogFormat("SavWav:{0}", filepath);
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_1 = ___filepath0;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_1);
		// Debug.LogFormat("SavWav:{0}", filepath);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogFormat_m309087137(NULL /*static, unused*/, _stringLiteral2398230137, L_0, /*hidden argument*/NULL);
		// Directory.CreateDirectory(Path.GetDirectoryName(filepath));
		String_t* L_2 = ___filepath0;
		// Directory.CreateDirectory(Path.GetDirectoryName(filepath));
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// Directory.CreateDirectory(Path.GetDirectoryName(filepath));
		Directory_CreateDirectory_m751642867(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// using (var fileStream = CreateEmpty(filepath))
		String_t* L_4 = ___filepath0;
		// using (var fileStream = CreateEmpty(filepath))
		FileStream_t4292183065 * L_5 = SavWav_CreateEmpty_m194446383(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		// ConvertAndWrite(fileStream, clip, samples);
		FileStream_t4292183065 * L_6 = V_0;
		AudioClip_t3680889665 * L_7 = ___clip1;
		int32_t L_8 = ___samples2;
		// ConvertAndWrite(fileStream, clip, samples);
		SavWav_ConvertAndWrite_m4186062765(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		// WriteHeader(fileStream, clip);
		FileStream_t4292183065 * L_9 = V_0;
		AudioClip_t3680889665 * L_10 = ___clip1;
		// WriteHeader(fileStream, clip);
		SavWav_WriteHeader_m2208500169(NULL /*static, unused*/, L_9, L_10, (-1), /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x4C, FINALLY_003f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			FileStream_t4292183065 * L_11 = V_0;
			if (!L_11)
			{
				goto IL_004b;
			}
		}

IL_0045:
		{
			FileStream_t4292183065 * L_12 = V_0;
			// using (var fileStream = CreateEmpty(filepath))
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_12);
		}

IL_004b:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004c:
	{
		// return true; // TODO: return false if there's a failure saving the file
		V_1 = (bool)1;
		goto IL_0053;
	}

IL_0053:
	{
		// }
		bool L_13 = V_1;
		return L_13;
	}
}
// UnityEngine.AudioClip SavWav::TrimSilence(UnityEngine.AudioClip,System.Single)
extern "C" IL2CPP_METHOD_ATTR AudioClip_t3680889665 * SavWav_TrimSilence_m2368381276 (RuntimeObject * __this /* static, unused */, AudioClip_t3680889665 * ___clip0, float ___min1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavWav_TrimSilence_m2368381276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t1444911251* V_0 = NULL;
	AudioClip_t3680889665 * V_1 = NULL;
	{
		// var samples = new float[clip.samples];
		AudioClip_t3680889665 * L_0 = ___clip0;
		// var samples = new float[clip.samples];
		NullCheck(L_0);
		int32_t L_1 = AudioClip_get_samples_m1836473408(L_0, /*hidden argument*/NULL);
		V_0 = ((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)L_1));
		// clip.GetData(samples, 0);
		AudioClip_t3680889665 * L_2 = ___clip0;
		SingleU5BU5D_t1444911251* L_3 = V_0;
		// clip.GetData(samples, 0);
		NullCheck(L_2);
		AudioClip_GetData_m1251334845(L_2, L_3, 0, /*hidden argument*/NULL);
		// return TrimSilence(new List<float>(samples), min, clip.channels, clip.frequency);
		SingleU5BU5D_t1444911251* L_4 = V_0;
		// return TrimSilence(new List<float>(samples), min, clip.channels, clip.frequency);
		List_1_t2869341516 * L_5 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m964359627(L_5, (RuntimeObject*)(RuntimeObject*)L_4, /*hidden argument*/List_1__ctor_m964359627_RuntimeMethod_var);
		float L_6 = ___min1;
		AudioClip_t3680889665 * L_7 = ___clip0;
		// return TrimSilence(new List<float>(samples), min, clip.channels, clip.frequency);
		NullCheck(L_7);
		int32_t L_8 = AudioClip_get_channels_m2392813414(L_7, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_9 = ___clip0;
		// return TrimSilence(new List<float>(samples), min, clip.channels, clip.frequency);
		NullCheck(L_9);
		int32_t L_10 = AudioClip_get_frequency_m3907759635(L_9, /*hidden argument*/NULL);
		// return TrimSilence(new List<float>(samples), min, clip.channels, clip.frequency);
		AudioClip_t3680889665 * L_11 = SavWav_TrimSilence_m1383616551(NULL /*static, unused*/, L_5, L_6, L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_0034;
	}

IL_0034:
	{
		// }
		AudioClip_t3680889665 * L_12 = V_1;
		return L_12;
	}
}
// UnityEngine.AudioClip SavWav::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR AudioClip_t3680889665 * SavWav_TrimSilence_m1383616551 (RuntimeObject * __this /* static, unused */, List_1_t2869341516 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, const RuntimeMethod* method)
{
	AudioClip_t3680889665 * V_0 = NULL;
	{
		// return TrimSilence(samples, min, channels, hz, false, false);
		List_1_t2869341516 * L_0 = ___samples0;
		float L_1 = ___min1;
		int32_t L_2 = ___channels2;
		int32_t L_3 = ___hz3;
		// return TrimSilence(samples, min, channels, hz, false, false);
		AudioClip_t3680889665 * L_4 = SavWav_TrimSilence_m3302723312(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (bool)0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		// }
		AudioClip_t3680889665 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.AudioClip SavWav::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR AudioClip_t3680889665 * SavWav_TrimSilence_m3302723312 (RuntimeObject * __this /* static, unused */, List_1_t2869341516 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, bool ____3D4, bool ___stream5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavWav_TrimSilence_m3302723312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AudioClip_t3680889665 * V_1 = NULL;
	AudioClip_t3680889665 * V_2 = NULL;
	{
		// for (i = 0; i < samples.Count; i++)
		V_0 = 0;
		goto IL_0026;
	}

IL_0008:
	{
		// if (Mathf.Abs(samples[i]) > min)
		List_1_t2869341516 * L_0 = ___samples0;
		int32_t L_1 = V_0;
		// if (Mathf.Abs(samples[i]) > min)
		NullCheck(L_0);
		float L_2 = List_1_get_Item_m718437397(L_0, L_1, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		// if (Mathf.Abs(samples[i]) > min)
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		float L_4 = ___min1;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0021;
		}
	}
	{
		// break;
		goto IL_0032;
	}

IL_0021:
	{
		// for (i = 0; i < samples.Count; i++)
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0026:
	{
		// for (i = 0; i < samples.Count; i++)
		int32_t L_6 = V_0;
		List_1_t2869341516 * L_7 = ___samples0;
		// for (i = 0; i < samples.Count; i++)
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m4125879936(L_7, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0008;
		}
	}

IL_0032:
	{
		// samples.RemoveRange(0, i);
		List_1_t2869341516 * L_9 = ___samples0;
		int32_t L_10 = V_0;
		// samples.RemoveRange(0, i);
		NullCheck(L_9);
		List_1_RemoveRange_m1420183523(L_9, 0, L_10, /*hidden argument*/List_1_RemoveRange_m1420183523_RuntimeMethod_var);
		// for (i = samples.Count - 1; i > 0; i--)
		List_1_t2869341516 * L_11 = ___samples0;
		// for (i = samples.Count - 1; i > 0; i--)
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m4125879936(L_11, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
		goto IL_0066;
	}

IL_0048:
	{
		// if (Mathf.Abs(samples[i]) > min)
		List_1_t2869341516 * L_13 = ___samples0;
		int32_t L_14 = V_0;
		// if (Mathf.Abs(samples[i]) > min)
		NullCheck(L_13);
		float L_15 = List_1_get_Item_m718437397(L_13, L_14, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		// if (Mathf.Abs(samples[i]) > min)
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_16 = fabsf(L_15);
		float L_17 = ___min1;
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_0061;
		}
	}
	{
		// break;
		goto IL_006d;
	}

IL_0061:
	{
		// for (i = samples.Count - 1; i > 0; i--)
		int32_t L_18 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1));
	}

IL_0066:
	{
		// for (i = samples.Count - 1; i > 0; i--)
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) > ((int32_t)0)))
		{
			goto IL_0048;
		}
	}

IL_006d:
	{
		// samples.RemoveRange(i, samples.Count - i);
		List_1_t2869341516 * L_20 = ___samples0;
		int32_t L_21 = V_0;
		List_1_t2869341516 * L_22 = ___samples0;
		// samples.RemoveRange(i, samples.Count - i);
		NullCheck(L_22);
		int32_t L_23 = List_1_get_Count_m4125879936(L_22, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		int32_t L_24 = V_0;
		// samples.RemoveRange(i, samples.Count - i);
		NullCheck(L_20);
		List_1_RemoveRange_m1420183523(L_20, L_21, ((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)L_24)), /*hidden argument*/List_1_RemoveRange_m1420183523_RuntimeMethod_var);
		// AudioClip clip = AudioClip.Create("TempClip", samples.Count, channels, hz, stream);
		List_1_t2869341516 * L_25 = ___samples0;
		// AudioClip clip = AudioClip.Create("TempClip", samples.Count, channels, hz, stream);
		NullCheck(L_25);
		int32_t L_26 = List_1_get_Count_m4125879936(L_25, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		int32_t L_27 = ___channels2;
		int32_t L_28 = ___hz3;
		bool L_29 = ___stream5;
		// AudioClip clip = AudioClip.Create("TempClip", samples.Count, channels, hz, stream);
		AudioClip_t3680889665 * L_30 = AudioClip_Create_m255819841(NULL /*static, unused*/, _stringLiteral3240979319, L_26, L_27, L_28, L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		// clip.SetData(samples.ToArray(), 0);
		AudioClip_t3680889665 * L_31 = V_1;
		List_1_t2869341516 * L_32 = ___samples0;
		// clip.SetData(samples.ToArray(), 0);
		NullCheck(L_32);
		SingleU5BU5D_t1444911251* L_33 = List_1_ToArray_m571148082(L_32, /*hidden argument*/List_1_ToArray_m571148082_RuntimeMethod_var);
		// clip.SetData(samples.ToArray(), 0);
		NullCheck(L_31);
		AudioClip_SetData_m313920037(L_31, L_33, 0, /*hidden argument*/NULL);
		// return clip;
		AudioClip_t3680889665 * L_34 = V_1;
		V_2 = L_34;
		goto IL_00a6;
	}

IL_00a6:
	{
		// }
		AudioClip_t3680889665 * L_35 = V_2;
		return L_35;
	}
}
// System.IO.FileStream SavWav::CreateEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR FileStream_t4292183065 * SavWav_CreateEmpty_m194446383 (RuntimeObject * __this /* static, unused */, String_t* ___filepath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavWav_CreateEmpty_m194446383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FileStream_t4292183065 * V_0 = NULL;
	uint8_t V_1 = 0x0;
	int32_t V_2 = 0;
	FileStream_t4292183065 * V_3 = NULL;
	{
		// var fileStream = new FileStream(filepath, FileMode.Create);
		String_t* L_0 = ___filepath0;
		// var fileStream = new FileStream(filepath, FileMode.Create);
		FileStream_t4292183065 * L_1 = (FileStream_t4292183065 *)il2cpp_codegen_object_new(FileStream_t4292183065_il2cpp_TypeInfo_var);
		FileStream__ctor_m2784380556(L_1, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		// byte emptyByte = new byte();
		V_1 = (uint8_t)0;
		// for (int i = 0; i < HEADER_SIZE; i++) //preparing the header
		V_2 = 0;
		goto IL_001f;
	}

IL_0012:
	{
		// fileStream.WriteByte(emptyByte);
		FileStream_t4292183065 * L_2 = V_0;
		uint8_t L_3 = V_1;
		// fileStream.WriteByte(emptyByte);
		NullCheck(L_2);
		VirtActionInvoker1< uint8_t >::Invoke(29 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_2, L_3);
		// for (int i = 0; i < HEADER_SIZE; i++) //preparing the header
		int32_t L_4 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_001f:
	{
		// for (int i = 0; i < HEADER_SIZE; i++) //preparing the header
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)44))))
		{
			goto IL_0012;
		}
	}
	{
		// return fileStream;
		FileStream_t4292183065 * L_6 = V_0;
		V_3 = L_6;
		goto IL_002e;
	}

IL_002e:
	{
		// }
		FileStream_t4292183065 * L_7 = V_3;
		return L_7;
	}
}
// System.Void SavWav::ConvertAndWrite(System.IO.FileStream,UnityEngine.AudioClip,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SavWav_ConvertAndWrite_m4186062765 (RuntimeObject * __this /* static, unused */, FileStream_t4292183065 * ___fileStream0, AudioClip_t3680889665 * ___clip1, int32_t ___recordedSamples2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavWav_ConvertAndWrite_m4186062765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t1444911251* V_0 = NULL;
	Int16U5BU5D_t3686840178* V_1 = NULL;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	ByteU5BU5D_t4116647657* V_5 = NULL;
	SingleU5BU5D_t1444911251* G_B3_0 = NULL;
	{
		// float[] samples = recordedSamples == -1 ? new float[clip.samples] : new float[recordedSamples];
		int32_t L_0 = ___recordedSamples2;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0018;
		}
	}
	{
		AudioClip_t3680889665 * L_1 = ___clip1;
		// float[] samples = recordedSamples == -1 ? new float[clip.samples] : new float[recordedSamples];
		NullCheck(L_1);
		int32_t L_2 = AudioClip_get_samples_m1836473408(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)L_2));
		goto IL_001e;
	}

IL_0018:
	{
		int32_t L_3 = ___recordedSamples2;
		G_B3_0 = ((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)L_3));
	}

IL_001e:
	{
		V_0 = G_B3_0;
		// clip.GetData(samples, 0);
		AudioClip_t3680889665 * L_4 = ___clip1;
		SingleU5BU5D_t1444911251* L_5 = V_0;
		// clip.GetData(samples, 0);
		NullCheck(L_4);
		AudioClip_GetData_m1251334845(L_4, L_5, 0, /*hidden argument*/NULL);
		// Int16[] intData = new Int16[samples.Length];
		SingleU5BU5D_t1444911251* L_6 = V_0;
		NullCheck(L_6);
		V_1 = ((Int16U5BU5D_t3686840178*)SZArrayNew(Int16U5BU5D_t3686840178_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))));
		// Byte[] bytesData = new Byte[samples.Length * 2];
		SingleU5BU5D_t1444911251* L_7 = V_0;
		NullCheck(L_7);
		V_2 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))), (int32_t)2))));
		// float rescaleFactor = 32767; //to convert float to Int16
		V_3 = (32767.0f);
		// for (int i = 0; i < samples.Length; i++)
		V_4 = 0;
		goto IL_007c;
	}

IL_004a:
	{
		// intData[i] = (short)(samples[i] * rescaleFactor);
		Int16U5BU5D_t3686840178* L_8 = V_1;
		int32_t L_9 = V_4;
		SingleU5BU5D_t1444911251* L_10 = V_0;
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		float L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		float L_14 = V_3;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int16_t)(((int16_t)((int16_t)((float)il2cpp_codegen_multiply((float)L_13, (float)L_14))))));
		// Byte[] byteArr = new Byte[2];
		V_5 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)2));
		// byteArr = BitConverter.GetBytes(intData[i]);
		Int16U5BU5D_t3686840178* L_15 = V_1;
		int32_t L_16 = V_4;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int16_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		// byteArr = BitConverter.GetBytes(intData[i]);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_19 = BitConverter_GetBytes_m658425501(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		// byteArr.CopyTo(bytesData, i * 2);
		ByteU5BU5D_t4116647657* L_20 = V_5;
		ByteU5BU5D_t4116647657* L_21 = V_2;
		int32_t L_22 = V_4;
		// byteArr.CopyTo(bytesData, i * 2);
		NullCheck((RuntimeArray *)(RuntimeArray *)L_20);
		Array_CopyTo_m225704097((RuntimeArray *)(RuntimeArray *)L_20, (RuntimeArray *)(RuntimeArray *)L_21, ((int32_t)il2cpp_codegen_multiply((int32_t)L_22, (int32_t)2)), /*hidden argument*/NULL);
		// for (int i = 0; i < samples.Length; i++)
		int32_t L_23 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_007c:
	{
		// for (int i = 0; i < samples.Length; i++)
		int32_t L_24 = V_4;
		SingleU5BU5D_t1444911251* L_25 = V_0;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_004a;
		}
	}
	{
		// fileStream.Write(bytesData, 0, bytesData.Length);
		FileStream_t4292183065 * L_26 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_27 = V_2;
		ByteU5BU5D_t4116647657* L_28 = V_2;
		NullCheck(L_28);
		// fileStream.Write(bytesData, 0, bytesData.Length);
		NullCheck(L_26);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_26, L_27, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))));
		// }
		return;
	}
}
// System.Void SavWav::WriteHeader(System.IO.FileStream,UnityEngine.AudioClip,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SavWav_WriteHeader_m2208500169 (RuntimeObject * __this /* static, unused */, FileStream_t4292183065 * ___fileStream0, AudioClip_t3680889665 * ___clip1, int32_t ___totalSamples2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavWav_WriteHeader_m2208500169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t4116647657* V_3 = NULL;
	ByteU5BU5D_t4116647657* V_4 = NULL;
	ByteU5BU5D_t4116647657* V_5 = NULL;
	ByteU5BU5D_t4116647657* V_6 = NULL;
	ByteU5BU5D_t4116647657* V_7 = NULL;
	uint16_t V_8 = 0;
	ByteU5BU5D_t4116647657* V_9 = NULL;
	ByteU5BU5D_t4116647657* V_10 = NULL;
	ByteU5BU5D_t4116647657* V_11 = NULL;
	ByteU5BU5D_t4116647657* V_12 = NULL;
	uint16_t V_13 = 0;
	uint16_t V_14 = 0;
	ByteU5BU5D_t4116647657* V_15 = NULL;
	ByteU5BU5D_t4116647657* V_16 = NULL;
	ByteU5BU5D_t4116647657* V_17 = NULL;
	int32_t G_B3_0 = 0;
	{
		// int hz = clip.frequency;
		AudioClip_t3680889665 * L_0 = ___clip1;
		// int hz = clip.frequency;
		NullCheck(L_0);
		int32_t L_1 = AudioClip_get_frequency_m3907759635(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// int channels = clip.channels;
		AudioClip_t3680889665 * L_2 = ___clip1;
		// int channels = clip.channels;
		NullCheck(L_2);
		int32_t L_3 = AudioClip_get_channels_m2392813414(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// int samples = totalSamples == -1 ? clip.samples : totalSamples;
		int32_t L_4 = ___totalSamples2;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0021;
		}
	}
	{
		AudioClip_t3680889665 * L_5 = ___clip1;
		// int samples = totalSamples == -1 ? clip.samples : totalSamples;
		NullCheck(L_5);
		int32_t L_6 = AudioClip_get_samples_m1836473408(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		goto IL_0022;
	}

IL_0021:
	{
		int32_t L_7 = ___totalSamples2;
		G_B3_0 = L_7;
	}

IL_0022:
	{
		V_2 = G_B3_0;
		// fileStream.Seek(0, SeekOrigin.Begin);
		FileStream_t4292183065 * L_8 = ___fileStream0;
		// fileStream.Seek(0, SeekOrigin.Begin);
		NullCheck(L_8);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(24 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_8, (((int64_t)((int64_t)0))), 0);
		// Byte[] riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
		Encoding_t1523322056 * L_9 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Byte[] riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
		NullCheck(L_9);
		ByteU5BU5D_t4116647657* L_10 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(17 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, _stringLiteral1209311930);
		V_3 = L_10;
		// fileStream.Write(riff, 0, 4);
		FileStream_t4292183065 * L_11 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_12 = V_3;
		// fileStream.Write(riff, 0, 4);
		NullCheck(L_11);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_11, L_12, 0, 4);
		// Byte[] chunkSize = BitConverter.GetBytes(fileStream.Length - 8);
		FileStream_t4292183065 * L_13 = ___fileStream0;
		// Byte[] chunkSize = BitConverter.GetBytes(fileStream.Length - 8);
		NullCheck(L_13);
		int64_t L_14 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_13);
		// Byte[] chunkSize = BitConverter.GetBytes(fileStream.Length - 8);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_15 = BitConverter_GetBytes_m4144088731(NULL /*static, unused*/, ((int64_t)il2cpp_codegen_subtract((int64_t)L_14, (int64_t)(((int64_t)((int64_t)8))))), /*hidden argument*/NULL);
		V_4 = L_15;
		// fileStream.Write(chunkSize, 0, 4);
		FileStream_t4292183065 * L_16 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_17 = V_4;
		// fileStream.Write(chunkSize, 0, 4);
		NullCheck(L_16);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_16, L_17, 0, 4);
		// Byte[] wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
		Encoding_t1523322056 * L_18 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Byte[] wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
		NullCheck(L_18);
		ByteU5BU5D_t4116647657* L_19 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(17 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_18, _stringLiteral2834979077);
		V_5 = L_19;
		// fileStream.Write(wave, 0, 4);
		FileStream_t4292183065 * L_20 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_21 = V_5;
		// fileStream.Write(wave, 0, 4);
		NullCheck(L_20);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, 4);
		// Byte[] fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
		Encoding_t1523322056 * L_22 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Byte[] fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
		NullCheck(L_22);
		ByteU5BU5D_t4116647657* L_23 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(17 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_22, _stringLiteral731316992);
		V_6 = L_23;
		// fileStream.Write(fmt, 0, 4);
		FileStream_t4292183065 * L_24 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_25 = V_6;
		// fileStream.Write(fmt, 0, 4);
		NullCheck(L_24);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_24, L_25, 0, 4);
		// Byte[] subChunk1 = BitConverter.GetBytes(16);
		// Byte[] subChunk1 = BitConverter.GetBytes(16);
		ByteU5BU5D_t4116647657* L_26 = BitConverter_GetBytes_m1040762521(NULL /*static, unused*/, ((int32_t)16), /*hidden argument*/NULL);
		V_7 = L_26;
		// fileStream.Write(subChunk1, 0, 4);
		FileStream_t4292183065 * L_27 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_28 = V_7;
		// fileStream.Write(subChunk1, 0, 4);
		NullCheck(L_27);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_27, L_28, 0, 4);
		// UInt16 one = 1;
		V_8 = (uint16_t)1;
		// Byte[] audioFormat = BitConverter.GetBytes(one);
		uint16_t L_29 = V_8;
		// Byte[] audioFormat = BitConverter.GetBytes(one);
		ByteU5BU5D_t4116647657* L_30 = BitConverter_GetBytes_m2889824517(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_9 = L_30;
		// fileStream.Write(audioFormat, 0, 2);
		FileStream_t4292183065 * L_31 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_32 = V_9;
		// fileStream.Write(audioFormat, 0, 2);
		NullCheck(L_31);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_31, L_32, 0, 2);
		// Byte[] numChannels = BitConverter.GetBytes(channels);
		int32_t L_33 = V_1;
		// Byte[] numChannels = BitConverter.GetBytes(channels);
		ByteU5BU5D_t4116647657* L_34 = BitConverter_GetBytes_m1040762521(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		V_10 = L_34;
		// fileStream.Write(numChannels, 0, 2);
		FileStream_t4292183065 * L_35 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_36 = V_10;
		// fileStream.Write(numChannels, 0, 2);
		NullCheck(L_35);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_35, L_36, 0, 2);
		// Byte[] sampleRate = BitConverter.GetBytes(hz);
		int32_t L_37 = V_0;
		// Byte[] sampleRate = BitConverter.GetBytes(hz);
		ByteU5BU5D_t4116647657* L_38 = BitConverter_GetBytes_m1040762521(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		// fileStream.Write(sampleRate, 0, 4);
		FileStream_t4292183065 * L_39 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_40 = V_11;
		// fileStream.Write(sampleRate, 0, 4);
		NullCheck(L_39);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_39, L_40, 0, 4);
		// Byte[] byteRate = BitConverter.GetBytes(hz * channels * 2);
		int32_t L_41 = V_0;
		int32_t L_42 = V_1;
		// Byte[] byteRate = BitConverter.GetBytes(hz * channels * 2);
		ByteU5BU5D_t4116647657* L_43 = BitConverter_GetBytes_m1040762521(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_41, (int32_t)L_42)), (int32_t)2)), /*hidden argument*/NULL);
		V_12 = L_43;
		// fileStream.Write(byteRate, 0, 4);
		FileStream_t4292183065 * L_44 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_45 = V_12;
		// fileStream.Write(byteRate, 0, 4);
		NullCheck(L_44);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_44, L_45, 0, 4);
		// UInt16 blockAlign = (ushort)(channels * 2);
		int32_t L_46 = V_1;
		V_13 = (uint16_t)(((int32_t)((uint16_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_46, (int32_t)2)))));
		// fileStream.Write(BitConverter.GetBytes(blockAlign), 0, 2);
		FileStream_t4292183065 * L_47 = ___fileStream0;
		uint16_t L_48 = V_13;
		// fileStream.Write(BitConverter.GetBytes(blockAlign), 0, 2);
		ByteU5BU5D_t4116647657* L_49 = BitConverter_GetBytes_m2889824517(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		// fileStream.Write(BitConverter.GetBytes(blockAlign), 0, 2);
		NullCheck(L_47);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_47, L_49, 0, 2);
		// UInt16 bps = 16; // PCM 16 bps
		V_14 = (uint16_t)((int32_t)16);
		// Byte[] bitsPerSample = BitConverter.GetBytes(bps);
		uint16_t L_50 = V_14;
		// Byte[] bitsPerSample = BitConverter.GetBytes(bps);
		ByteU5BU5D_t4116647657* L_51 = BitConverter_GetBytes_m2889824517(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		V_15 = L_51;
		// fileStream.Write(bitsPerSample, 0, 2);
		FileStream_t4292183065 * L_52 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_53 = V_15;
		// fileStream.Write(bitsPerSample, 0, 2);
		NullCheck(L_52);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_52, L_53, 0, 2);
		// Byte[] datastring = System.Text.Encoding.UTF8.GetBytes("data");
		Encoding_t1523322056 * L_54 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Byte[] datastring = System.Text.Encoding.UTF8.GetBytes("data");
		NullCheck(L_54);
		ByteU5BU5D_t4116647657* L_55 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(17 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_54, _stringLiteral2037252866);
		V_16 = L_55;
		// fileStream.Write(datastring, 0, 4);
		FileStream_t4292183065 * L_56 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_57 = V_16;
		// fileStream.Write(datastring, 0, 4);
		NullCheck(L_56);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_56, L_57, 0, 4);
		// Byte[] subChunk2 = BitConverter.GetBytes(samples * channels * 2);
		int32_t L_58 = V_2;
		int32_t L_59 = V_1;
		// Byte[] subChunk2 = BitConverter.GetBytes(samples * channels * 2);
		ByteU5BU5D_t4116647657* L_60 = BitConverter_GetBytes_m1040762521(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_58, (int32_t)L_59)), (int32_t)2)), /*hidden argument*/NULL);
		V_17 = L_60;
		// fileStream.Write(subChunk2, 0, 4);
		FileStream_t4292183065 * L_61 = ___fileStream0;
		ByteU5BU5D_t4116647657* L_62 = V_17;
		// fileStream.Write(subChunk2, 0, 4);
		NullCheck(L_61);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(28 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_61, L_62, 0, 4);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SceneChanger::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SceneChanger__ctor_m2898256174 (SceneChanger_t1033871796 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneChanger::Start()
extern "C" IL2CPP_METHOD_ATTR void SceneChanger_Start_m236998524 (SceneChanger_t1033871796 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneChanger_Start_m236998524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gameController = FindObjectOfType<GameController>();
		// gameController = FindObjectOfType<GameController>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameController_t2330501625 * L_0 = Object_FindObjectOfType_TisGameController_t2330501625_m3040890899(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisGameController_t2330501625_m3040890899_RuntimeMethod_var);
		__this->set_gameController_4(L_0);
		// dataController = FindObjectOfType<DataController>();
		// dataController = FindObjectOfType<DataController>();
		DataController_t353634109 * L_1 = Object_FindObjectOfType_TisDataController_t353634109_m3280844559(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisDataController_t353634109_m3280844559_RuntimeMethod_var);
		__this->set_dataController_5(L_1);
		// if(dataController.playerData.isFirstTime == 1)
		DataController_t353634109 * L_2 = __this->get_dataController_5();
		NullCheck(L_2);
		PlayerData_t220878115 * L_3 = L_2->get_playerData_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_isFirstTime_0();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}
	{
		// if (buttonOk)
		Button_t4055032469 * L_5 = __this->get_buttonOk_2();
		// if (buttonOk)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		// Debug.Log("boton Ok");
		// Debug.Log("boton Ok");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3326552179, /*hidden argument*/NULL);
		// buttonOk.gameObject.SetActive(false);
		Button_t4055032469 * L_7 = __this->get_buttonOk_2();
		// buttonOk.gameObject.SetActive(false);
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(L_7, /*hidden argument*/NULL);
		// buttonOk.gameObject.SetActive(false);
		NullCheck(L_8);
		GameObject_SetActive_m796801857(L_8, (bool)0, /*hidden argument*/NULL);
	}

IL_005b:
	{
	}

IL_005c:
	{
		// }
		return;
	}
}
// System.Void SceneChanger::ChangeSceneTo(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneChanger_ChangeSceneTo_m2636143807 (SceneChanger_t1033871796 * __this, String_t* ___sceneName0, const RuntimeMethod* method)
{
	{
		// SceneManager.LoadScene(sceneName);
		String_t* L_0 = ___sceneName0;
		// SceneManager.LoadScene(sceneName);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SceneChanger::SelectCharacter(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneChanger_SelectCharacter_m2321949008 (SceneChanger_t1033871796 * __this, String_t* ___type0, const RuntimeMethod* method)
{
	{
		// gameController.SelectCharacter(type);
		GameController_t2330501625 * L_0 = __this->get_gameController_4();
		String_t* L_1 = ___type0;
		// gameController.SelectCharacter(type);
		NullCheck(L_0);
		GameController_SelectCharacter_m3280008209(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SceneChanger::StartGame()
extern "C" IL2CPP_METHOD_ATTR void SceneChanger_StartGame_m57266850 (SceneChanger_t1033871796 * __this, const RuntimeMethod* method)
{
	{
		// gameController.StartGame();
		GameController_t2330501625 * L_0 = __this->get_gameController_4();
		// gameController.StartGame();
		NullCheck(L_0);
		GameController_StartGame_m2172304344(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SettingsController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SettingsController__ctor_m3197295816 (SettingsController_t1943457452 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SettingsController::RestartData()
extern "C" IL2CPP_METHOD_ATTR void SettingsController_RestartData_m2218269148 (SettingsController_t1943457452 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsController_RestartData_m2218269148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gameController = FindObjectOfType<GameController>();
		// gameController = FindObjectOfType<GameController>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameController_t2330501625 * L_0 = Object_FindObjectOfType_TisGameController_t2330501625_m3040890899(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisGameController_t2330501625_m3040890899_RuntimeMethod_var);
		__this->set_gameController_2(L_0);
		// gameController.restarData();
		GameController_t2330501625 * L_1 = __this->get_gameController_2();
		// gameController.restarData();
		NullCheck(L_1);
		GameController_restarData_m2034268093(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SystemHelpers::FormatTimeSeconds(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* SystemHelpers_FormatTimeSeconds_m797777047 (RuntimeObject * __this /* static, unused */, int32_t ___time0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SystemHelpers_FormatTimeSeconds_m797777047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		// int mins = time / 60;
		int32_t L_0 = ___time0;
		V_0 = ((int32_t)((int32_t)L_0/(int32_t)((int32_t)60)));
		// int secs = time % 60;
		int32_t L_1 = ___time0;
		V_1 = ((int32_t)((int32_t)L_1%(int32_t)((int32_t)60)));
		// return string.Format("{0}", secs);
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_3);
		// return string.Format("{0}", secs);
		String_t* L_5 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral628085470, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0021;
	}

IL_0021:
	{
		// }
		String_t* L_6 = V_2;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIDisplayController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController__ctor_m1722786891 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDisplayController::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_OnEnable_m461706163 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDisplayController_OnEnable_m461706163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// recordManager.OnStartEncoding += RecordManager_OnStartEncoding;
		AudioRecordManager_t3166594816 * L_0 = __this->get_recordManager_7();
		intptr_t L_1 = (intptr_t)UIDisplayController_RecordManager_OnStartEncoding_m3377201746_RuntimeMethod_var;
		Action_t1264377477 * L_2 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m75143462(L_2, __this, L_1, /*hidden argument*/NULL);
		// recordManager.OnStartEncoding += RecordManager_OnStartEncoding;
		NullCheck(L_0);
		AudioRecordManager_add_OnStartEncoding_m1329063948(L_0, L_2, /*hidden argument*/NULL);
		// recordManager.OnStopEncoding += RecordManager_OnStopEncoding;
		AudioRecordManager_t3166594816 * L_3 = __this->get_recordManager_7();
		intptr_t L_4 = (intptr_t)UIDisplayController_RecordManager_OnStopEncoding_m2462141832_RuntimeMethod_var;
		Action_t1264377477 * L_5 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m75143462(L_5, __this, L_4, /*hidden argument*/NULL);
		// recordManager.OnStopEncoding += RecordManager_OnStopEncoding;
		NullCheck(L_3);
		AudioRecordManager_add_OnStopEncoding_m3752743452(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIDisplayController::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_OnDisable_m2241095903 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDisplayController_OnDisable_m2241095903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// recordManager.OnStartEncoding -= RecordManager_OnStartEncoding;
		AudioRecordManager_t3166594816 * L_0 = __this->get_recordManager_7();
		intptr_t L_1 = (intptr_t)UIDisplayController_RecordManager_OnStartEncoding_m3377201746_RuntimeMethod_var;
		Action_t1264377477 * L_2 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m75143462(L_2, __this, L_1, /*hidden argument*/NULL);
		// recordManager.OnStartEncoding -= RecordManager_OnStartEncoding;
		NullCheck(L_0);
		AudioRecordManager_remove_OnStartEncoding_m3701522171(L_0, L_2, /*hidden argument*/NULL);
		// recordManager.OnStopEncoding -= RecordManager_OnStopEncoding;
		AudioRecordManager_t3166594816 * L_3 = __this->get_recordManager_7();
		intptr_t L_4 = (intptr_t)UIDisplayController_RecordManager_OnStopEncoding_m2462141832_RuntimeMethod_var;
		Action_t1264377477 * L_5 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m75143462(L_5, __this, L_4, /*hidden argument*/NULL);
		// recordManager.OnStopEncoding -= RecordManager_OnStopEncoding;
		NullCheck(L_3);
		AudioRecordManager_remove_OnStopEncoding_m1703388006(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIDisplayController::Update()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_Update_m2592220395 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	{
		// if (recordManager.isRecording)
		AudioRecordManager_t3166594816 * L_0 = __this->get_recordManager_7();
		NullCheck(L_0);
		bool L_1 = L_0->get_isRecording_23();
		if (!L_1)
		{
			goto IL_008f;
		}
	}
	{
		// if (!elapsedTimeText.gameObject.activeSelf)
		Text_t1901882714 * L_2 = __this->get_elapsedTimeText_3();
		// if (!elapsedTimeText.gameObject.activeSelf)
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		// if (!elapsedTimeText.gameObject.activeSelf)
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m1767405923(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		// elapsedTimeText.gameObject.SetActive(true);
		Text_t1901882714 * L_5 = __this->get_elapsedTimeText_3();
		// elapsedTimeText.gameObject.SetActive(true);
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		// elapsedTimeText.gameObject.SetActive(true);
		NullCheck(L_6);
		GameObject_SetActive_m796801857(L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_003a:
	{
		// if (!timerText.gameObject.activeSelf)
		Text_t1901882714 * L_7 = __this->get_timerText_4();
		// if (!timerText.gameObject.activeSelf)
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(L_7, /*hidden argument*/NULL);
		// if (!timerText.gameObject.activeSelf)
		NullCheck(L_8);
		bool L_9 = GameObject_get_activeSelf_m1767405923(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0062;
		}
	}
	{
		// timerText.gameObject.SetActive(true);
		Text_t1901882714 * L_10 = __this->get_timerText_4();
		// timerText.gameObject.SetActive(true);
		NullCheck(L_10);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
		// timerText.gameObject.SetActive(true);
		NullCheck(L_11);
		GameObject_SetActive_m796801857(L_11, (bool)1, /*hidden argument*/NULL);
	}

IL_0062:
	{
		// elapsedTimeText.text = recordManager.ElapsedTimeTextWithFormat;
		Text_t1901882714 * L_12 = __this->get_elapsedTimeText_3();
		AudioRecordManager_t3166594816 * L_13 = __this->get_recordManager_7();
		// elapsedTimeText.text = recordManager.ElapsedTimeTextWithFormat;
		NullCheck(L_13);
		String_t* L_14 = AudioRecordManager_get_ElapsedTimeTextWithFormat_m1083798450(L_13, /*hidden argument*/NULL);
		// elapsedTimeText.text = recordManager.ElapsedTimeTextWithFormat;
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_14);
		// timerText.text = recordManager.TimerTextWithFormat;
		Text_t1901882714 * L_15 = __this->get_timerText_4();
		AudioRecordManager_t3166594816 * L_16 = __this->get_recordManager_7();
		// timerText.text = recordManager.TimerTextWithFormat;
		NullCheck(L_16);
		String_t* L_17 = AudioRecordManager_get_TimerTextWithFormat_m2913629590(L_16, /*hidden argument*/NULL);
		// timerText.text = recordManager.TimerTextWithFormat;
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_17);
	}

IL_008f:
	{
		// }
		return;
	}
}
// System.Void UIDisplayController::RecordManager_OnStartEncoding()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_RecordManager_OnStartEncoding_m3377201746 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	{
		// encodingMessagePanel.SetActive(true);
		GameObject_t1113636619 * L_0 = __this->get_encodingMessagePanel_2();
		// encodingMessagePanel.SetActive(true);
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIDisplayController::RecordManager_OnStopEncoding()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_RecordManager_OnStopEncoding_m2462141832 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDisplayController_RecordManager_OnStopEncoding_m2462141832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// manager = FindObjectOfType<QuestManager>();
		// manager = FindObjectOfType<QuestManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		QuestManager_t588401851 * L_0 = Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var);
		__this->set_manager_9(L_0);
		// quest = manager.quests[manager.questID];
		QuestManager_t588401851 * L_1 = __this->get_manager_9();
		NullCheck(L_1);
		QuestU5BU5D_t1937508389* L_2 = L_1->get_quests_2();
		QuestManager_t588401851 * L_3 = __this->get_manager_9();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_questID_5();
		NullCheck(L_2);
		int32_t L_5 = L_4;
		Quest_t3696879532 * L_6 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_quest_10(L_6);
		// encodingMessagePanel.SetActive(false);
		GameObject_t1113636619 * L_7 = __this->get_encodingMessagePanel_2();
		// encodingMessagePanel.SetActive(false);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		// if (!quest.isSelectMultiple)
		Quest_t3696879532 * L_8 = __this->get_quest_10();
		NullCheck(L_8);
		bool L_9 = L_8->get_isSelectMultiple_6();
		if (L_9)
		{
			goto IL_008a;
		}
	}
	{
		// if (quest.completedText.Length > 0)
		Quest_t3696879532 * L_10 = __this->get_quest_10();
		NullCheck(L_10);
		String_t* L_11 = L_10->get_completedText_20();
		// if (quest.completedText.Length > 0)
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m3847582255(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		SceneChanger_t1033871796 * L_13 = __this->get_sceneChanger_8();
		// sceneChanger.ChangeSceneTo("RightAnswerScene");
		NullCheck(L_13);
		SceneChanger_ChangeSceneTo_m2636143807(L_13, _stringLiteral2909207532, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_0073:
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_14 = __this->get_sceneChanger_8();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_14);
		SceneChanger_ChangeSceneTo_m2636143807(L_14, _stringLiteral1853239370, /*hidden argument*/NULL);
	}

IL_0085:
	{
		// return;
		goto IL_009a;
	}

IL_008a:
	{
		// sceneChanger.ChangeSceneTo("MainScene");
		SceneChanger_t1033871796 * L_15 = __this->get_sceneChanger_8();
		// sceneChanger.ChangeSceneTo("MainScene");
		NullCheck(L_15);
		SceneChanger_ChangeSceneTo_m2636143807(L_15, _stringLiteral1853239370, /*hidden argument*/NULL);
	}

IL_009a:
	{
		// }
		return;
	}
}
// System.Void UIDisplayController::EncoderDropdownValueChanged(UnityEngine.UI.Dropdown)
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_EncoderDropdownValueChanged_m2554422969 (UIDisplayController_t1173465452 * __this, Dropdown_t2274391225 * ___dropdown0, const RuntimeMethod* method)
{
	{
		// if (dropdown.value == 0)
		Dropdown_t2274391225 * L_0 = ___dropdown0;
		// if (dropdown.value == 0)
		NullCheck(L_0);
		int32_t L_1 = Dropdown_get_value_m1555353112(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		// recordManager.selectedAudioType = AudioType.MPEG;
		AudioRecordManager_t3166594816 * L_2 = __this->get_recordManager_7();
		NullCheck(L_2);
		L_2->set_selectedAudioType_11(((int32_t)13));
		goto IL_003b;
	}

IL_0020:
	{
		// else if (dropdown.value == 1)
		Dropdown_t2274391225 * L_3 = ___dropdown0;
		// else if (dropdown.value == 1)
		NullCheck(L_3);
		int32_t L_4 = Dropdown_get_value_m1555353112(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_003b;
		}
	}
	{
		// recordManager.selectedAudioType = AudioType.OGGVORBIS;
		AudioRecordManager_t3166594816 * L_5 = __this->get_recordManager_7();
		NullCheck(L_5);
		L_5->set_selectedAudioType_11(((int32_t)14));
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void UIDisplayController::OnStartButtonCLick()
extern "C" IL2CPP_METHOD_ATTR void UIDisplayController_OnStartButtonCLick_m1843070903 (UIDisplayController_t1173465452 * __this, const RuntimeMethod* method)
{
	{
		// start.gameObject.SetActive(false);
		Button_t4055032469 * L_0 = __this->get_start_6();
		// start.gameObject.SetActive(false);
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		// start.gameObject.SetActive(false);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		// stop.gameObject.SetActive(true);
		Button_t4055032469 * L_2 = __this->get_stop_5();
		// stop.gameObject.SetActive(true);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		// stop.gameObject.SetActive(true);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPuzzleController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController__ctor_m1077891668 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	{
		// public float showTime = 10;
		__this->set_showTime_11((10.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPuzzleController::add_OnEndPuzzle(UIPuzzleController/PuzzleControllerEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_add_OnEndPuzzle_m603902049 (RuntimeObject * __this /* static, unused */, PuzzleControllerEventHandler_t963335703 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_add_OnEndPuzzle_m603902049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PuzzleControllerEventHandler_t963335703 * V_0 = NULL;
	PuzzleControllerEventHandler_t963335703 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		PuzzleControllerEventHandler_t963335703 * L_0 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_OnEndPuzzle_23();
		V_0 = L_0;
	}

IL_0006:
	{
		PuzzleControllerEventHandler_t963335703 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		PuzzleControllerEventHandler_t963335703 * L_2 = V_1;
		PuzzleControllerEventHandler_t963335703 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		PuzzleControllerEventHandler_t963335703 * L_5 = V_0;
		PuzzleControllerEventHandler_t963335703 * L_6 = InterlockedCompareExchangeImpl<PuzzleControllerEventHandler_t963335703 *>((((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_address_of_OnEndPuzzle_23()), ((PuzzleControllerEventHandler_t963335703 *)CastclassSealed((RuntimeObject*)L_4, PuzzleControllerEventHandler_t963335703_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		PuzzleControllerEventHandler_t963335703 * L_7 = V_0;
		PuzzleControllerEventHandler_t963335703 * L_8 = V_1;
		if ((!(((RuntimeObject*)(PuzzleControllerEventHandler_t963335703 *)L_7) == ((RuntimeObject*)(PuzzleControllerEventHandler_t963335703 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UIPuzzleController::remove_OnEndPuzzle(UIPuzzleController/PuzzleControllerEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_remove_OnEndPuzzle_m3057330886 (RuntimeObject * __this /* static, unused */, PuzzleControllerEventHandler_t963335703 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_remove_OnEndPuzzle_m3057330886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PuzzleControllerEventHandler_t963335703 * V_0 = NULL;
	PuzzleControllerEventHandler_t963335703 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		PuzzleControllerEventHandler_t963335703 * L_0 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_OnEndPuzzle_23();
		V_0 = L_0;
	}

IL_0006:
	{
		PuzzleControllerEventHandler_t963335703 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		PuzzleControllerEventHandler_t963335703 * L_2 = V_1;
		PuzzleControllerEventHandler_t963335703 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		PuzzleControllerEventHandler_t963335703 * L_5 = V_0;
		PuzzleControllerEventHandler_t963335703 * L_6 = InterlockedCompareExchangeImpl<PuzzleControllerEventHandler_t963335703 *>((((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_address_of_OnEndPuzzle_23()), ((PuzzleControllerEventHandler_t963335703 *)CastclassSealed((RuntimeObject*)L_4, PuzzleControllerEventHandler_t963335703_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		PuzzleControllerEventHandler_t963335703 * L_7 = V_0;
		PuzzleControllerEventHandler_t963335703 * L_8 = V_1;
		if ((!(((RuntimeObject*)(PuzzleControllerEventHandler_t963335703 *)L_7) == ((RuntimeObject*)(PuzzleControllerEventHandler_t963335703 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UIPuzzleController::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_OnEnable_m4216670754 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_OnEnable_m4216670754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UIPuzzleToken.OnDragEvent += UIPuzzleToken_OnDragEvent;
		intptr_t L_0 = (intptr_t)UIPuzzleController_UIPuzzleToken_OnDragEvent_m1610412114_RuntimeMethod_var;
		PuzzleTokenEventHandler_t3807414068 * L_1 = (PuzzleTokenEventHandler_t3807414068 *)il2cpp_codegen_object_new(PuzzleTokenEventHandler_t3807414068_il2cpp_TypeInfo_var);
		PuzzleTokenEventHandler__ctor_m94650657(L_1, __this, L_0, /*hidden argument*/NULL);
		// UIPuzzleToken.OnDragEvent += UIPuzzleToken_OnDragEvent;
		UIPuzzleToken_add_OnDragEvent_m3807187468(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// UIPuzzleInsert.OnPointerEvent += UIPuzzleInsert_OnPointerEvent;
		intptr_t L_2 = (intptr_t)UIPuzzleController_UIPuzzleInsert_OnPointerEvent_m249629649_RuntimeMethod_var;
		PuzzleInsertEventHandler_t3824397729 * L_3 = (PuzzleInsertEventHandler_t3824397729 *)il2cpp_codegen_object_new(PuzzleInsertEventHandler_t3824397729_il2cpp_TypeInfo_var);
		PuzzleInsertEventHandler__ctor_m3630263791(L_3, __this, L_2, /*hidden argument*/NULL);
		// UIPuzzleInsert.OnPointerEvent += UIPuzzleInsert_OnPointerEvent;
		UIPuzzleInsert_add_OnPointerEvent_m3287251477(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPuzzleController::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_OnDisable_m3910927490 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_OnDisable_m3910927490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UIPuzzleToken.OnDragEvent -= UIPuzzleToken_OnDragEvent;
		intptr_t L_0 = (intptr_t)UIPuzzleController_UIPuzzleToken_OnDragEvent_m1610412114_RuntimeMethod_var;
		PuzzleTokenEventHandler_t3807414068 * L_1 = (PuzzleTokenEventHandler_t3807414068 *)il2cpp_codegen_object_new(PuzzleTokenEventHandler_t3807414068_il2cpp_TypeInfo_var);
		PuzzleTokenEventHandler__ctor_m94650657(L_1, __this, L_0, /*hidden argument*/NULL);
		// UIPuzzleToken.OnDragEvent -= UIPuzzleToken_OnDragEvent;
		UIPuzzleToken_remove_OnDragEvent_m2242078314(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// UIPuzzleInsert.OnPointerEvent -= UIPuzzleInsert_OnPointerEvent;
		intptr_t L_2 = (intptr_t)UIPuzzleController_UIPuzzleInsert_OnPointerEvent_m249629649_RuntimeMethod_var;
		PuzzleInsertEventHandler_t3824397729 * L_3 = (PuzzleInsertEventHandler_t3824397729 *)il2cpp_codegen_object_new(PuzzleInsertEventHandler_t3824397729_il2cpp_TypeInfo_var);
		PuzzleInsertEventHandler__ctor_m3630263791(L_3, __this, L_2, /*hidden argument*/NULL);
		// UIPuzzleInsert.OnPointerEvent -= UIPuzzleInsert_OnPointerEvent;
		UIPuzzleInsert_remove_OnPointerEvent_m414160073(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPuzzleController::Start()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_Start_m2120222110 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_Start_m2120222110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// matchCount = 0;
		__this->set_matchCount_22(0);
		// manager = FindObjectOfType<QuestManager>();
		// manager = FindObjectOfType<QuestManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		QuestManager_t588401851 * L_0 = Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisQuestManager_t588401851_m3444047686_RuntimeMethod_var);
		__this->set_manager_24(L_0);
		// ShowIntroPanel();
		// ShowIntroPanel();
		UIPuzzleController_ShowIntroPanel_m1546992945(__this, /*hidden argument*/NULL);
		// StartCoroutine(_StartLate());
		// StartCoroutine(_StartLate());
		RuntimeObject* L_1 = UIPuzzleController__StartLate_m1048854624(__this, /*hidden argument*/NULL);
		// StartCoroutine(_StartLate());
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator UIPuzzleController::_StartLate()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* UIPuzzleController__StartLate_m1048854624 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController__StartLate_m1048854624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3C_StartLateU3Ec__Iterator0_t3966377835 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3C_StartLateU3Ec__Iterator0_t3966377835 * L_0 = (U3C_StartLateU3Ec__Iterator0_t3966377835 *)il2cpp_codegen_object_new(U3C_StartLateU3Ec__Iterator0_t3966377835_il2cpp_TypeInfo_var);
		U3C_StartLateU3Ec__Iterator0__ctor_m3073728995(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3C_StartLateU3Ec__Iterator0_t3966377835 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3C_StartLateU3Ec__Iterator0_t3966377835 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_3 = V_1;
		return L_3;
	}
}
// System.Void UIPuzzleController::Update()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_Update_m541428278 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_Update_m541428278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!bInit) return;
		bool L_0 = __this->get_bInit_18();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (!bInit) return;
		goto IL_0050;
	}

IL_0011:
	{
		// if (countdownTime > 0)
		float L_1 = __this->get_countdownTime_13();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0050;
		}
	}
	{
		// countdownTime -= Time.deltaTime;
		float L_2 = __this->get_countdownTime_13();
		// countdownTime -= Time.deltaTime;
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_countdownTime_13(((float)il2cpp_codegen_subtract((float)L_2, (float)L_3)));
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		Text_t1901882714 * L_4 = __this->get_countdownText_17();
		float L_5 = __this->get_countdownTime_13();
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_6 = Mathf_CeilToInt_m432108984(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		String_t* L_7 = SystemHelpers_FormatTimeSeconds_m797777047(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_7);
	}

IL_0050:
	{
		// }
		return;
	}
}
// System.Void UIPuzzleController::ShowIntroPanel()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_ShowIntroPanel_m1546992945 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_ShowIntroPanel_m1546992945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t899420910 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// startImgPositions = new List<Vector3>();
		// startImgPositions = new List<Vector3>();
		List_1_t899420910 * L_0 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m1536473967(L_0, /*hidden argument*/List_1__ctor_m1536473967_RuntimeMethod_var);
		__this->set_startImgPositions_5(L_0);
		// tokenOrder = new List<EPuzzleToken>();
		// tokenOrder = new List<EPuzzleToken>();
		List_1_t3937307605 * L_1 = (List_1_t3937307605 *)il2cpp_codegen_object_new(List_1_t3937307605_il2cpp_TypeInfo_var);
		List_1__ctor_m28597759(L_1, /*hidden argument*/List_1__ctor_m28597759_RuntimeMethod_var);
		__this->set_tokenOrder_6(L_1);
		// List<Vector3> copyList = new List<Vector3>();
		List_1_t899420910 * L_2 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m1536473967(L_2, /*hidden argument*/List_1__ctor_m1536473967_RuntimeMethod_var);
		V_0 = L_2;
		// for (int i = 0; i < TOTAL_TOKENS; i++)
		V_1 = 0;
		goto IL_005d;
	}

IL_0024:
	{
		// startImgPositions.Add(images[i].rectTransform.position);
		List_1_t899420910 * L_3 = __this->get_startImgPositions_5();
		List_1_t4142344393 * L_4 = __this->get_images_3();
		int32_t L_5 = V_1;
		// startImgPositions.Add(images[i].rectTransform.position);
		NullCheck(L_4);
		Image_t2670269651 * L_6 = List_1_get_Item_m1242626717(L_4, L_5, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// startImgPositions.Add(images[i].rectTransform.position);
		NullCheck(L_6);
		RectTransform_t3704657025 * L_7 = Graphic_get_rectTransform_m1167152468(L_6, /*hidden argument*/NULL);
		// startImgPositions.Add(images[i].rectTransform.position);
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		// startImgPositions.Add(images[i].rectTransform.position);
		NullCheck(L_3);
		List_1_Add_m1524640104(L_3, L_8, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		// copyList.Add(startImgPositions[i]);
		List_1_t899420910 * L_9 = V_0;
		List_1_t899420910 * L_10 = __this->get_startImgPositions_5();
		int32_t L_11 = V_1;
		// copyList.Add(startImgPositions[i]);
		NullCheck(L_10);
		Vector3_t3722313464  L_12 = List_1_get_Item_m200663048(L_10, L_11, /*hidden argument*/List_1_get_Item_m200663048_RuntimeMethod_var);
		// copyList.Add(startImgPositions[i]);
		NullCheck(L_9);
		List_1_Add_m1524640104(L_9, L_12, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		// for (int i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_005d:
	{
		// for (int i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_15 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0024;
		}
	}
	{
		// imagePositions = SystemHelpers.ShuffleList(copyList);
		List_1_t899420910 * L_16 = V_0;
		// imagePositions = SystemHelpers.ShuffleList(copyList);
		List_1_t899420910 * L_17 = SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547(NULL /*static, unused*/, L_16, /*hidden argument*/SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547_RuntimeMethod_var);
		__this->set_imagePositions_4(L_17);
		// }
		return;
	}
}
// System.Void UIPuzzleController::ShowPuzzlePanel()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_ShowPuzzlePanel_m2130928474 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_ShowPuzzlePanel_m2130928474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t899420910 * V_1 = NULL;
	List_1_t899420910 * V_2 = NULL;
	{
		// introPanel.SetActive(false);
		GameObject_t1113636619 * L_0 = __this->get_introPanel_14();
		// introPanel.SetActive(false);
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		// puzzlePanel.SetActive(true);
		GameObject_t1113636619 * L_1 = __this->get_puzzlePanel_15();
		// puzzlePanel.SetActive(true);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		// List<Vector3> startTokenPositions = new List<Vector3>();
		List_1_t899420910 * L_2 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m1536473967(L_2, /*hidden argument*/List_1__ctor_m1536473967_RuntimeMethod_var);
		V_1 = L_2;
		// List<Vector3> copyList = new List<Vector3>();
		List_1_t899420910 * L_3 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m1536473967(L_3, /*hidden argument*/List_1__ctor_m1536473967_RuntimeMethod_var);
		V_2 = L_3;
		// for (i = 0; i < TOTAL_TOKENS; i++)
		V_0 = 0;
		goto IL_005b;
	}

IL_002c:
	{
		// startTokenPositions.Add(tokenImages[i].rectTransform.position);
		List_1_t899420910 * L_4 = V_1;
		List_1_t4142344393 * L_5 = __this->get_tokenImages_7();
		int32_t L_6 = V_0;
		// startTokenPositions.Add(tokenImages[i].rectTransform.position);
		NullCheck(L_5);
		Image_t2670269651 * L_7 = List_1_get_Item_m1242626717(L_5, L_6, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// startTokenPositions.Add(tokenImages[i].rectTransform.position);
		NullCheck(L_7);
		RectTransform_t3704657025 * L_8 = Graphic_get_rectTransform_m1167152468(L_7, /*hidden argument*/NULL);
		// startTokenPositions.Add(tokenImages[i].rectTransform.position);
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		// startTokenPositions.Add(tokenImages[i].rectTransform.position);
		NullCheck(L_4);
		List_1_Add_m1524640104(L_4, L_9, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		// copyList.Add(startTokenPositions[i]);
		List_1_t899420910 * L_10 = V_2;
		List_1_t899420910 * L_11 = V_1;
		int32_t L_12 = V_0;
		// copyList.Add(startTokenPositions[i]);
		NullCheck(L_11);
		Vector3_t3722313464  L_13 = List_1_get_Item_m200663048(L_11, L_12, /*hidden argument*/List_1_get_Item_m200663048_RuntimeMethod_var);
		// copyList.Add(startTokenPositions[i]);
		NullCheck(L_10);
		List_1_Add_m1524640104(L_10, L_13, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_005b:
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_16 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_002c;
		}
	}
	{
		// tokenPositions = SystemHelpers.ShuffleList(copyList);
		List_1_t899420910 * L_17 = V_2;
		// tokenPositions = SystemHelpers.ShuffleList(copyList);
		List_1_t899420910 * L_18 = SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547(NULL /*static, unused*/, L_17, /*hidden argument*/SystemHelpers_ShuffleList_TisVector3_t3722313464_m2108255547_RuntimeMethod_var);
		__this->set_tokenPositions_9(L_18);
		// tokens = new List<UIPuzzleToken>();
		// tokens = new List<UIPuzzleToken>();
		List_1_t3134180627 * L_19 = (List_1_t3134180627 *)il2cpp_codegen_object_new(List_1_t3134180627_il2cpp_TypeInfo_var);
		List_1__ctor_m3799958844(L_19, /*hidden argument*/List_1__ctor_m3799958844_RuntimeMethod_var);
		__this->set_tokens_8(L_19);
		// for (i = 0; i < TOTAL_TOKENS; i++)
		V_0 = 0;
		goto IL_00fb;
	}

IL_0084:
	{
		// tokenImages[i].rectTransform.position = tokenPositions[i];
		List_1_t4142344393 * L_20 = __this->get_tokenImages_7();
		int32_t L_21 = V_0;
		// tokenImages[i].rectTransform.position = tokenPositions[i];
		NullCheck(L_20);
		Image_t2670269651 * L_22 = List_1_get_Item_m1242626717(L_20, L_21, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// tokenImages[i].rectTransform.position = tokenPositions[i];
		NullCheck(L_22);
		RectTransform_t3704657025 * L_23 = Graphic_get_rectTransform_m1167152468(L_22, /*hidden argument*/NULL);
		List_1_t899420910 * L_24 = __this->get_tokenPositions_9();
		int32_t L_25 = V_0;
		// tokenImages[i].rectTransform.position = tokenPositions[i];
		NullCheck(L_24);
		Vector3_t3722313464  L_26 = List_1_get_Item_m200663048(L_24, L_25, /*hidden argument*/List_1_get_Item_m200663048_RuntimeMethod_var);
		// tokenImages[i].rectTransform.position = tokenPositions[i];
		NullCheck(L_23);
		Transform_set_position_m3387557959(L_23, L_26, /*hidden argument*/NULL);
		// tokens.Add(tokenImages[i].GetComponent<UIPuzzleToken>());
		List_1_t3134180627 * L_27 = __this->get_tokens_8();
		List_1_t4142344393 * L_28 = __this->get_tokenImages_7();
		int32_t L_29 = V_0;
		// tokens.Add(tokenImages[i].GetComponent<UIPuzzleToken>());
		NullCheck(L_28);
		Image_t2670269651 * L_30 = List_1_get_Item_m1242626717(L_28, L_29, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// tokens.Add(tokenImages[i].GetComponent<UIPuzzleToken>());
		NullCheck(L_30);
		UIPuzzleToken_t1662105885 * L_31 = Component_GetComponent_TisUIPuzzleToken_t1662105885_m4197970512(L_30, /*hidden argument*/Component_GetComponent_TisUIPuzzleToken_t1662105885_m4197970512_RuntimeMethod_var);
		// tokens.Add(tokenImages[i].GetComponent<UIPuzzleToken>());
		NullCheck(L_27);
		List_1_Add_m3373879604(L_27, L_31, /*hidden argument*/List_1_Add_m3373879604_RuntimeMethod_var);
		// tokens[i].InitToken();
		List_1_t3134180627 * L_32 = __this->get_tokens_8();
		int32_t L_33 = V_0;
		// tokens[i].InitToken();
		NullCheck(L_32);
		UIPuzzleToken_t1662105885 * L_34 = List_1_get_Item_m219509784(L_32, L_33, /*hidden argument*/List_1_get_Item_m219509784_RuntimeMethod_var);
		// tokens[i].InitToken();
		NullCheck(L_34);
		UIPuzzleToken_InitToken_m3887138451(L_34, /*hidden argument*/NULL);
		// insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokenOrder[i]);
		List_1_t4142344393 * L_35 = __this->get_insertImages_10();
		int32_t L_36 = V_0;
		// insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokenOrder[i]);
		NullCheck(L_35);
		Image_t2670269651 * L_37 = List_1_get_Item_m1242626717(L_35, L_36, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokenOrder[i]);
		NullCheck(L_37);
		UIPuzzleInsert_t594790890 * L_38 = Component_GetComponent_TisUIPuzzleInsert_t594790890_m1640620979(L_37, /*hidden argument*/Component_GetComponent_TisUIPuzzleInsert_t594790890_m1640620979_RuntimeMethod_var);
		List_1_t3937307605 * L_39 = __this->get_tokenOrder_6();
		int32_t L_40 = V_0;
		// insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokenOrder[i]);
		NullCheck(L_39);
		int32_t L_41 = List_1_get_Item_m3452984843(L_39, L_40, /*hidden argument*/List_1_get_Item_m3452984843_RuntimeMethod_var);
		// insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokenOrder[i]);
		NullCheck(L_38);
		UIPuzzleInsert_Init_m3966161147(L_38, L_41, /*hidden argument*/NULL);
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_42 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_00fb:
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_44 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_0084;
		}
	}
	{
		// }
		return;
	}
}
// System.Void UIPuzzleController::UIPuzzleToken_OnDragEvent(UIPuzzleToken,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_UIPuzzleToken_OnDragEvent_m1610412114 (UIPuzzleController_t2836468369 * __this, UIPuzzleToken_t1662105885 * ___token0, bool ___dragged1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_UIPuzzleToken_OnDragEvent_m1610412114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (dragged)
		bool L_0 = ___dragged1;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// CurrentToken = token;
		UIPuzzleToken_t1662105885 * L_1 = ___token0;
		__this->set_CurrentToken_21(L_1);
		goto IL_00ba;
	}

IL_0015:
	{
		// if (bTokenMatched)
		bool L_2 = __this->get_bTokenMatched_19();
		if (!L_2)
		{
			goto IL_00a5;
		}
	}
	{
		// CurrentToken.Match(matchedPosition);
		UIPuzzleToken_t1662105885 * L_3 = __this->get_CurrentToken_21();
		Vector2_t2156229523  L_4 = __this->get_matchedPosition_20();
		// CurrentToken.Match(matchedPosition);
		NullCheck(L_3);
		UIPuzzleToken_Match_m1213795857(L_3, L_4, /*hidden argument*/NULL);
		// matchCount++;
		int32_t L_5 = __this->get_matchCount_22();
		__this->set_matchCount_22(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		// if (matchCount == TOTAL_TOKENS)
		int32_t L_6 = __this->get_matchCount_22();
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_7 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_009f;
		}
	}
	{
		// if (congratsMessage != null)
		GameObject_t1113636619 * L_8 = __this->get_congratsMessage_16();
		// if (congratsMessage != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0071;
		}
	}
	{
		// congratsMessage.SetActive(true);
		GameObject_t1113636619 * L_10 = __this->get_congratsMessage_16();
		// congratsMessage.SetActive(true);
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
	}

IL_0071:
	{
		// Debug.Log("PuzzleController: Solved!");
		// Debug.Log("PuzzleController: Solved!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral82719313, /*hidden argument*/NULL);
		// StartCoroutine(DelayCompleteQuest());
		// StartCoroutine(DelayCompleteQuest());
		RuntimeObject* L_11 = UIPuzzleController_DelayCompleteQuest_m3765199727(__this, /*hidden argument*/NULL);
		// StartCoroutine(DelayCompleteQuest());
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_11, /*hidden argument*/NULL);
		// OnEndPuzzle?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		PuzzleControllerEventHandler_t963335703 * L_12 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_OnEndPuzzle_23();
		if (L_12)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_009e;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		PuzzleControllerEventHandler_t963335703 * L_13 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_OnEndPuzzle_23();
		// OnEndPuzzle?.Invoke();
		NullCheck(L_13);
		PuzzleControllerEventHandler_Invoke_m1864861298(L_13, /*hidden argument*/NULL);
	}

IL_009e:
	{
	}

IL_009f:
	{
		goto IL_00b2;
	}

IL_00a5:
	{
		// CurrentToken.RestartToken();
		UIPuzzleToken_t1662105885 * L_14 = __this->get_CurrentToken_21();
		// CurrentToken.RestartToken();
		NullCheck(L_14);
		UIPuzzleToken_RestartToken_m3577290980(L_14, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		// CurrentToken = null;
		__this->set_CurrentToken_21((UIPuzzleToken_t1662105885 *)NULL);
	}

IL_00ba:
	{
		// }
		return;
	}
}
// System.Void UIPuzzleController::UIPuzzleInsert_OnPointerEvent(System.Boolean,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController_UIPuzzleInsert_OnPointerEvent_m249629649 (UIPuzzleController_t2836468369 * __this, bool ___match0, Vector2_t2156229523  ___position1, const RuntimeMethod* method)
{
	{
		// bTokenMatched = match;
		bool L_0 = ___match0;
		__this->set_bTokenMatched_19(L_0);
		// matchedPosition = position;
		Vector2_t2156229523  L_1 = ___position1;
		__this->set_matchedPosition_20(L_1);
		// }
		return;
	}
}
// System.Collections.IEnumerator UIPuzzleController::DelayCompleteQuest()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* UIPuzzleController_DelayCompleteQuest_m3765199727 (UIPuzzleController_t2836468369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController_DelayCompleteQuest_m3765199727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * L_0 = (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 *)il2cpp_codegen_object_new(U3CDelayCompleteQuestU3Ec__Iterator1_t479202429_il2cpp_TypeInfo_var);
		U3CDelayCompleteQuestU3Ec__Iterator1__ctor_m852968702(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_3 = V_1;
		return L_3;
	}
}
// System.Void UIPuzzleController::.cctor()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleController__cctor_m2123512877 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleController__cctor_m2123512877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static int TOTAL_TOKENS = 6;
		((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->set_TOTAL_TOKENS_2(6);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPuzzleController/<_StartLate>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3C_StartLateU3Ec__Iterator0__ctor_m3073728995 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIPuzzleController/<_StartLate>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3C_StartLateU3Ec__Iterator0_MoveNext_m1211157992 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3C_StartLateU3Ec__Iterator0_MoveNext_m1211157992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_003d;
			}
		}
	}
	{
		goto IL_0202;
	}

IL_0021:
	{
		// yield return null;
		__this->set_U24current_3(NULL);
		bool L_2 = __this->get_U24disposing_4();
		if (L_2)
		{
			goto IL_0038;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0038:
	{
		goto IL_0204;
	}

IL_003d:
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		__this->set_U3CiU3E__1_0(0);
		goto IL_00ab;
	}

IL_0049:
	{
		// images[i].rectTransform.position = imagePositions[i];
		UIPuzzleController_t2836468369 * L_3 = __this->get_U24this_2();
		NullCheck(L_3);
		List_1_t4142344393 * L_4 = L_3->get_images_3();
		int32_t L_5 = __this->get_U3CiU3E__1_0();
		// images[i].rectTransform.position = imagePositions[i];
		NullCheck(L_4);
		Image_t2670269651 * L_6 = List_1_get_Item_m1242626717(L_4, L_5, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// images[i].rectTransform.position = imagePositions[i];
		NullCheck(L_6);
		RectTransform_t3704657025 * L_7 = Graphic_get_rectTransform_m1167152468(L_6, /*hidden argument*/NULL);
		UIPuzzleController_t2836468369 * L_8 = __this->get_U24this_2();
		NullCheck(L_8);
		List_1_t899420910 * L_9 = L_8->get_imagePositions_4();
		int32_t L_10 = __this->get_U3CiU3E__1_0();
		// images[i].rectTransform.position = imagePositions[i];
		NullCheck(L_9);
		Vector3_t3722313464  L_11 = List_1_get_Item_m200663048(L_9, L_10, /*hidden argument*/List_1_get_Item_m200663048_RuntimeMethod_var);
		// images[i].rectTransform.position = imagePositions[i];
		NullCheck(L_7);
		Transform_set_position_m3387557959(L_7, L_11, /*hidden argument*/NULL);
		// images[i].enabled = true;
		UIPuzzleController_t2836468369 * L_12 = __this->get_U24this_2();
		NullCheck(L_12);
		List_1_t4142344393 * L_13 = L_12->get_images_3();
		int32_t L_14 = __this->get_U3CiU3E__1_0();
		// images[i].enabled = true;
		NullCheck(L_13);
		Image_t2670269651 * L_15 = List_1_get_Item_m1242626717(L_13, L_14, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// images[i].enabled = true;
		NullCheck(L_15);
		Behaviour_set_enabled_m20417929(L_15, (bool)1, /*hidden argument*/NULL);
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_16 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1)));
	}

IL_00ab:
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_17 = __this->get_U3CiU3E__1_0();
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_18 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0049;
		}
	}
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		__this->set_U3CiU3E__1_0(0);
		goto IL_0179;
	}

IL_00c7:
	{
		// for (j = 0; j < TOTAL_TOKENS; j++)
		__this->set_U3CjU3E__2_1(0);
		goto IL_015a;
	}

IL_00d4:
	{
		// if (startImgPositions[i] == images[j].rectTransform.position)
		UIPuzzleController_t2836468369 * L_19 = __this->get_U24this_2();
		NullCheck(L_19);
		List_1_t899420910 * L_20 = L_19->get_startImgPositions_5();
		int32_t L_21 = __this->get_U3CiU3E__1_0();
		// if (startImgPositions[i] == images[j].rectTransform.position)
		NullCheck(L_20);
		Vector3_t3722313464  L_22 = List_1_get_Item_m200663048(L_20, L_21, /*hidden argument*/List_1_get_Item_m200663048_RuntimeMethod_var);
		UIPuzzleController_t2836468369 * L_23 = __this->get_U24this_2();
		NullCheck(L_23);
		List_1_t4142344393 * L_24 = L_23->get_images_3();
		int32_t L_25 = __this->get_U3CjU3E__2_1();
		// if (startImgPositions[i] == images[j].rectTransform.position)
		NullCheck(L_24);
		Image_t2670269651 * L_26 = List_1_get_Item_m1242626717(L_24, L_25, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// if (startImgPositions[i] == images[j].rectTransform.position)
		NullCheck(L_26);
		RectTransform_t3704657025 * L_27 = Graphic_get_rectTransform_m1167152468(L_26, /*hidden argument*/NULL);
		// if (startImgPositions[i] == images[j].rectTransform.position)
		NullCheck(L_27);
		Vector3_t3722313464  L_28 = Transform_get_position_m36019626(L_27, /*hidden argument*/NULL);
		// if (startImgPositions[i] == images[j].rectTransform.position)
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		bool L_29 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_22, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_014b;
		}
	}
	{
		// tokenOrder.Add(images[j].GetComponent<UIPuzzleToken>().token);
		UIPuzzleController_t2836468369 * L_30 = __this->get_U24this_2();
		NullCheck(L_30);
		List_1_t3937307605 * L_31 = L_30->get_tokenOrder_6();
		UIPuzzleController_t2836468369 * L_32 = __this->get_U24this_2();
		NullCheck(L_32);
		List_1_t4142344393 * L_33 = L_32->get_images_3();
		int32_t L_34 = __this->get_U3CjU3E__2_1();
		// tokenOrder.Add(images[j].GetComponent<UIPuzzleToken>().token);
		NullCheck(L_33);
		Image_t2670269651 * L_35 = List_1_get_Item_m1242626717(L_33, L_34, /*hidden argument*/List_1_get_Item_m1242626717_RuntimeMethod_var);
		// tokenOrder.Add(images[j].GetComponent<UIPuzzleToken>().token);
		NullCheck(L_35);
		UIPuzzleToken_t1662105885 * L_36 = Component_GetComponent_TisUIPuzzleToken_t1662105885_m4197970512(L_35, /*hidden argument*/Component_GetComponent_TisUIPuzzleToken_t1662105885_m4197970512_RuntimeMethod_var);
		NullCheck(L_36);
		int32_t L_37 = L_36->get_token_2();
		// tokenOrder.Add(images[j].GetComponent<UIPuzzleToken>().token);
		NullCheck(L_31);
		List_1_Add_m1003748895(L_31, L_37, /*hidden argument*/List_1_Add_m1003748895_RuntimeMethod_var);
		// break;
		goto IL_016a;
	}

IL_014b:
	{
		// for (j = 0; j < TOTAL_TOKENS; j++)
		int32_t L_38 = __this->get_U3CjU3E__2_1();
		__this->set_U3CjU3E__2_1(((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1)));
	}

IL_015a:
	{
		// for (j = 0; j < TOTAL_TOKENS; j++)
		int32_t L_39 = __this->get_U3CjU3E__2_1();
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_40 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_00d4;
		}
	}

IL_016a:
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_41 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1)));
	}

IL_0179:
	{
		// for (i = 0; i < TOTAL_TOKENS; i++)
		int32_t L_42 = __this->get_U3CiU3E__1_0();
		IL2CPP_RUNTIME_CLASS_INIT(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var);
		int32_t L_43 = ((UIPuzzleController_t2836468369_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleController_t2836468369_il2cpp_TypeInfo_var))->get_TOTAL_TOKENS_2();
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_00c7;
		}
	}
	{
		// startTime = Time.time;
		UIPuzzleController_t2836468369 * L_44 = __this->get_U24this_2();
		// startTime = Time.time;
		float L_45 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		L_44->set_startTime_12(L_45);
		// countdownTime = showTime;
		UIPuzzleController_t2836468369 * L_46 = __this->get_U24this_2();
		UIPuzzleController_t2836468369 * L_47 = __this->get_U24this_2();
		NullCheck(L_47);
		float L_48 = L_47->get_showTime_11();
		NullCheck(L_46);
		L_46->set_countdownTime_13(L_48);
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		UIPuzzleController_t2836468369 * L_49 = __this->get_U24this_2();
		NullCheck(L_49);
		Text_t1901882714 * L_50 = L_49->get_countdownText_17();
		UIPuzzleController_t2836468369 * L_51 = __this->get_U24this_2();
		NullCheck(L_51);
		float L_52 = L_51->get_countdownTime_13();
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_53 = Mathf_CeilToInt_m432108984(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		String_t* L_54 = SystemHelpers_FormatTimeSeconds_m797777047(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		// countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
		NullCheck(L_50);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_50, L_54);
		// bInit = true;
		UIPuzzleController_t2836468369 * L_55 = __this->get_U24this_2();
		NullCheck(L_55);
		L_55->set_bInit_18((bool)1);
		// Invoke("ShowPuzzlePanel", countdownTime);
		UIPuzzleController_t2836468369 * L_56 = __this->get_U24this_2();
		UIPuzzleController_t2836468369 * L_57 = __this->get_U24this_2();
		NullCheck(L_57);
		float L_58 = L_57->get_countdownTime_13();
		// Invoke("ShowPuzzlePanel", countdownTime);
		NullCheck(L_56);
		MonoBehaviour_Invoke_m4227543964(L_56, _stringLiteral3851960542, L_58, /*hidden argument*/NULL);
		// }
		__this->set_U24PC_5((-1));
	}

IL_0202:
	{
		return (bool)0;
	}

IL_0204:
	{
		return (bool)1;
	}
}
// System.Object UIPuzzleController/<_StartLate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3C_StartLateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2287487367 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UIPuzzleController/<_StartLate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3C_StartLateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1079970297 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UIPuzzleController/<_StartLate>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3C_StartLateU3Ec__Iterator0_Dispose_m3032292592 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UIPuzzleController/<_StartLate>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3C_StartLateU3Ec__Iterator0_Reset_m217901147 (U3C_StartLateU3Ec__Iterator0_t3966377835 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3C_StartLateU3Ec__Iterator0_Reset_m217901147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3C_StartLateU3Ec__Iterator0_Reset_m217901147_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPuzzleController/<DelayCompleteQuest>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayCompleteQuestU3Ec__Iterator1__ctor_m852968702 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIPuzzleController/<DelayCompleteQuest>c__Iterator1::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CDelayCompleteQuestU3Ec__Iterator1_MoveNext_m3485536048 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayCompleteQuestU3Ec__Iterator1_MoveNext_m3485536048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0046;
			}
		}
	}
	{
		goto IL_007d;
	}

IL_0021:
	{
		// yield return new WaitForSeconds(1f);
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0041:
	{
		goto IL_007f;
	}

IL_0046:
	{
		// manager.quests[manager.questID].QuestComplete();
		UIPuzzleController_t2836468369 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		QuestManager_t588401851 * L_5 = L_4->get_manager_24();
		NullCheck(L_5);
		QuestU5BU5D_t1937508389* L_6 = L_5->get_quests_2();
		UIPuzzleController_t2836468369 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		QuestManager_t588401851 * L_8 = L_7->get_manager_24();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_questID_5();
		NullCheck(L_6);
		int32_t L_10 = L_9;
		Quest_t3696879532 * L_11 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		// manager.quests[manager.questID].QuestComplete();
		NullCheck(L_11);
		Quest_QuestComplete_m1582949342(L_11, /*hidden argument*/NULL);
		// SceneManager.LoadScene("EndGameScene");
		// SceneManager.LoadScene("EndGameScene");
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral589998905, /*hidden argument*/NULL);
		// }
		__this->set_U24PC_3((-1));
	}

IL_007d:
	{
		return (bool)0;
	}

IL_007f:
	{
		return (bool)1;
	}
}
// System.Object UIPuzzleController/<DelayCompleteQuest>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayCompleteQuestU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4153623137 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UIPuzzleController/<DelayCompleteQuest>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayCompleteQuestU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3138117981 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UIPuzzleController/<DelayCompleteQuest>c__Iterator1::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayCompleteQuestU3Ec__Iterator1_Dispose_m3575658576 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UIPuzzleController/<DelayCompleteQuest>c__Iterator1::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CDelayCompleteQuestU3Ec__Iterator1_Reset_m1169978582 (U3CDelayCompleteQuestU3Ec__Iterator1_t479202429 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayCompleteQuestU3Ec__Iterator1_Reset_m1169978582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CDelayCompleteQuestU3Ec__Iterator1_Reset_m1169978582_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_PuzzleControllerEventHandler_t963335703 (PuzzleControllerEventHandler_t963335703 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UIPuzzleController/PuzzleControllerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void PuzzleControllerEventHandler__ctor_m3580259097 (PuzzleControllerEventHandler_t963335703 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIPuzzleController/PuzzleControllerEventHandler::Invoke()
extern "C" IL2CPP_METHOD_ATTR void PuzzleControllerEventHandler_Invoke_m1864861298 (PuzzleControllerEventHandler_t963335703 * __this, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UIPuzzleController/PuzzleControllerEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* PuzzleControllerEventHandler_BeginInvoke_m2953460245 (PuzzleControllerEventHandler_t963335703 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UIPuzzleController/PuzzleControllerEventHandler::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void PuzzleControllerEventHandler_EndInvoke_m1147716367 (PuzzleControllerEventHandler_t963335703 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPuzzleInsert::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert__ctor_m2209221095 (UIPuzzleInsert_t594790890 * __this, const RuntimeMethod* method)
{
	{
		// public bool debugLog = false;
		__this->set_debugLog_5((bool)0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPuzzleInsert::add_OnPointerEvent(UIPuzzleInsert/PuzzleInsertEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_add_OnPointerEvent_m3287251477 (RuntimeObject * __this /* static, unused */, PuzzleInsertEventHandler_t3824397729 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleInsert_add_OnPointerEvent_m3287251477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PuzzleInsertEventHandler_t3824397729 * V_0 = NULL;
	PuzzleInsertEventHandler_t3824397729 * V_1 = NULL;
	{
		PuzzleInsertEventHandler_t3824397729 * L_0 = ((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_OnPointerEvent_4();
		V_0 = L_0;
	}

IL_0006:
	{
		PuzzleInsertEventHandler_t3824397729 * L_1 = V_0;
		V_1 = L_1;
		PuzzleInsertEventHandler_t3824397729 * L_2 = V_1;
		PuzzleInsertEventHandler_t3824397729 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		PuzzleInsertEventHandler_t3824397729 * L_5 = V_0;
		PuzzleInsertEventHandler_t3824397729 * L_6 = InterlockedCompareExchangeImpl<PuzzleInsertEventHandler_t3824397729 *>((((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_address_of_OnPointerEvent_4()), ((PuzzleInsertEventHandler_t3824397729 *)CastclassSealed((RuntimeObject*)L_4, PuzzleInsertEventHandler_t3824397729_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		PuzzleInsertEventHandler_t3824397729 * L_7 = V_0;
		PuzzleInsertEventHandler_t3824397729 * L_8 = V_1;
		if ((!(((RuntimeObject*)(PuzzleInsertEventHandler_t3824397729 *)L_7) == ((RuntimeObject*)(PuzzleInsertEventHandler_t3824397729 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UIPuzzleInsert::remove_OnPointerEvent(UIPuzzleInsert/PuzzleInsertEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_remove_OnPointerEvent_m414160073 (RuntimeObject * __this /* static, unused */, PuzzleInsertEventHandler_t3824397729 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleInsert_remove_OnPointerEvent_m414160073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PuzzleInsertEventHandler_t3824397729 * V_0 = NULL;
	PuzzleInsertEventHandler_t3824397729 * V_1 = NULL;
	{
		PuzzleInsertEventHandler_t3824397729 * L_0 = ((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_OnPointerEvent_4();
		V_0 = L_0;
	}

IL_0006:
	{
		PuzzleInsertEventHandler_t3824397729 * L_1 = V_0;
		V_1 = L_1;
		PuzzleInsertEventHandler_t3824397729 * L_2 = V_1;
		PuzzleInsertEventHandler_t3824397729 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		PuzzleInsertEventHandler_t3824397729 * L_5 = V_0;
		PuzzleInsertEventHandler_t3824397729 * L_6 = InterlockedCompareExchangeImpl<PuzzleInsertEventHandler_t3824397729 *>((((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_address_of_OnPointerEvent_4()), ((PuzzleInsertEventHandler_t3824397729 *)CastclassSealed((RuntimeObject*)L_4, PuzzleInsertEventHandler_t3824397729_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		PuzzleInsertEventHandler_t3824397729 * L_7 = V_0;
		PuzzleInsertEventHandler_t3824397729 * L_8 = V_1;
		if ((!(((RuntimeObject*)(PuzzleInsertEventHandler_t3824397729 *)L_7) == ((RuntimeObject*)(PuzzleInsertEventHandler_t3824397729 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UIPuzzleInsert::Start()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_Start_m399829981 (UIPuzzleInsert_t594790890 * __this, const RuntimeMethod* method)
{
	{
		// match = false;
		__this->set_match_6((bool)0);
		// }
		return;
	}
}
// System.Void UIPuzzleInsert::Init(EPuzzleToken)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_Init_m3966161147 (UIPuzzleInsert_t594790890 * __this, int32_t ___token0, const RuntimeMethod* method)
{
	{
		// Token = token;
		int32_t L_0 = ___token0;
		__this->set_Token_3(L_0);
		// }
		return;
	}
}
// System.Void UIPuzzleInsert::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_OnPointerEnter_m3558055084 (UIPuzzleInsert_t594790890 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleInsert_OnPointerEnter_m3558055084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (debugLog) { Debug.LogFormat("Enter! {0}", transform.name); }
		bool L_0 = __this->get_debugLog_5();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		// if (debugLog) { Debug.LogFormat("Enter! {0}", transform.name); }
		ObjectU5BU5D_t2843939325* L_1 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		// if (debugLog) { Debug.LogFormat("Enter! {0}", transform.name); }
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// if (debugLog) { Debug.LogFormat("Enter! {0}", transform.name); }
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m4211327027(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		// if (debugLog) { Debug.LogFormat("Enter! {0}", transform.name); }
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogFormat_m309087137(NULL /*static, unused*/, _stringLiteral452318740, L_1, /*hidden argument*/NULL);
	}

IL_002c:
	{
		// if (puzzleController.CurrentToken == null) return;
		UIPuzzleController_t2836468369 * L_4 = __this->get_puzzleController_2();
		NullCheck(L_4);
		UIPuzzleToken_t1662105885 * L_5 = L_4->get_CurrentToken_21();
		// if (puzzleController.CurrentToken == null) return;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		// if (puzzleController.CurrentToken == null) return;
		goto IL_00c2;
	}

IL_0047:
	{
		// if (puzzleController.CurrentToken.token == Token)
		UIPuzzleController_t2836468369 * L_7 = __this->get_puzzleController_2();
		NullCheck(L_7);
		UIPuzzleToken_t1662105885 * L_8 = L_7->get_CurrentToken_21();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_token_2();
		int32_t L_10 = __this->get_Token_3();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_00c2;
		}
	}
	{
		// if (debugLog) { Debug.LogFormat("Enter match! {0}", transform.name); }
		bool L_11 = __this->get_debugLog_5();
		if (!L_11)
		{
			goto IL_008e;
		}
	}
	{
		// if (debugLog) { Debug.LogFormat("Enter match! {0}", transform.name); }
		ObjectU5BU5D_t2843939325* L_12 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		// if (debugLog) { Debug.LogFormat("Enter match! {0}", transform.name); }
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// if (debugLog) { Debug.LogFormat("Enter match! {0}", transform.name); }
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m4211327027(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_14);
		// if (debugLog) { Debug.LogFormat("Enter match! {0}", transform.name); }
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogFormat_m309087137(NULL /*static, unused*/, _stringLiteral2063885597, L_12, /*hidden argument*/NULL);
	}

IL_008e:
	{
		// match = true;
		__this->set_match_6((bool)1);
		// OnPointerEvent?.Invoke(match, transform.position);
		PuzzleInsertEventHandler_t3824397729 * L_15 = ((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_OnPointerEvent_4();
		if (L_15)
		{
			goto IL_00a1;
		}
	}
	{
		goto IL_00c1;
	}

IL_00a1:
	{
		PuzzleInsertEventHandler_t3824397729 * L_16 = ((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_OnPointerEvent_4();
		bool L_17 = __this->get_match_6();
		// OnPointerEvent?.Invoke(match, transform.position);
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// OnPointerEvent?.Invoke(match, transform.position);
		NullCheck(L_18);
		Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
		// OnPointerEvent?.Invoke(match, transform.position);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_20 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		// OnPointerEvent?.Invoke(match, transform.position);
		NullCheck(L_16);
		PuzzleInsertEventHandler_Invoke_m1382497804(L_16, L_17, L_20, /*hidden argument*/NULL);
	}

IL_00c1:
	{
	}

IL_00c2:
	{
		// }
		return;
	}
}
// System.Void UIPuzzleInsert::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleInsert_OnPointerExit_m3410566094 (UIPuzzleInsert_t594790890 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleInsert_OnPointerExit_m3410566094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// match = false;
		__this->set_match_6((bool)0);
		// OnPointerEvent?.Invoke(match, transform.position);
		PuzzleInsertEventHandler_t3824397729 * L_0 = ((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_OnPointerEvent_4();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0034;
	}

IL_0014:
	{
		PuzzleInsertEventHandler_t3824397729 * L_1 = ((UIPuzzleInsert_t594790890_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleInsert_t594790890_il2cpp_TypeInfo_var))->get_OnPointerEvent_4();
		bool L_2 = __this->get_match_6();
		// OnPointerEvent?.Invoke(match, transform.position);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// OnPointerEvent?.Invoke(match, transform.position);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		// OnPointerEvent?.Invoke(match, transform.position);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// OnPointerEvent?.Invoke(match, transform.position);
		NullCheck(L_1);
		PuzzleInsertEventHandler_Invoke_m1382497804(L_1, L_2, L_5, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_PuzzleInsertEventHandler_t3824397729 (PuzzleInsertEventHandler_t3824397729 * __this, bool ___match0, Vector2_t2156229523  ___position1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(int32_t, Vector2_t2156229523 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___match0), ___position1);

}
// System.Void UIPuzzleInsert/PuzzleInsertEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void PuzzleInsertEventHandler__ctor_m3630263791 (PuzzleInsertEventHandler_t3824397729 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIPuzzleInsert/PuzzleInsertEventHandler::Invoke(System.Boolean,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void PuzzleInsertEventHandler_Invoke_m1382497804 (PuzzleInsertEventHandler_t3824397729 * __this, bool ___match0, Vector2_t2156229523  ___position1, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, bool, Vector2_t2156229523 , const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___match0, ___position1, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, bool, Vector2_t2156229523 , const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___match0, ___position1, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< bool, Vector2_t2156229523  >::Invoke(targetMethod, targetThis, ___match0, ___position1);
							else
								GenericVirtActionInvoker2< bool, Vector2_t2156229523  >::Invoke(targetMethod, targetThis, ___match0, ___position1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< bool, Vector2_t2156229523  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___match0, ___position1);
							else
								VirtActionInvoker2< bool, Vector2_t2156229523  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___match0, ___position1);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, bool, Vector2_t2156229523 , const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___match0, ___position1, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, bool, Vector2_t2156229523 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___match0, ___position1, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, bool, Vector2_t2156229523 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___match0, ___position1, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< bool, Vector2_t2156229523  >::Invoke(targetMethod, targetThis, ___match0, ___position1);
						else
							GenericVirtActionInvoker2< bool, Vector2_t2156229523  >::Invoke(targetMethod, targetThis, ___match0, ___position1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< bool, Vector2_t2156229523  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___match0, ___position1);
						else
							VirtActionInvoker2< bool, Vector2_t2156229523  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___match0, ___position1);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, bool, Vector2_t2156229523 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___match0, ___position1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UIPuzzleInsert/PuzzleInsertEventHandler::BeginInvoke(System.Boolean,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* PuzzleInsertEventHandler_BeginInvoke_m43389714 (PuzzleInsertEventHandler_t3824397729 * __this, bool ___match0, Vector2_t2156229523  ___position1, AsyncCallback_t3962456242 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleInsertEventHandler_BeginInvoke_m43389714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___match0);
	__d_args[1] = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &___position1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UIPuzzleInsert/PuzzleInsertEventHandler::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void PuzzleInsertEventHandler_EndInvoke_m853362498 (PuzzleInsertEventHandler_t3824397729 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPuzzleToken::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken__ctor_m3726069491 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method)
{
	{
		// public float moveSpeed = 8;
		__this->set_moveSpeed_3((8.0f));
		// public bool IsGrabbable { get; private set; } = false;
		__this->set_U3CIsGrabbableU3Ek__BackingField_4((bool)0);
		// bool matched = false;
		__this->set_matched_5((bool)0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIPuzzleToken::get_IsGrabbable()
extern "C" IL2CPP_METHOD_ATTR bool UIPuzzleToken_get_IsGrabbable_m1400245146 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public bool IsGrabbable { get; private set; } = false;
		bool L_0 = __this->get_U3CIsGrabbableU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UIPuzzleToken::set_IsGrabbable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_set_IsGrabbable_m4025083784 (UIPuzzleToken_t1662105885 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsGrabbable { get; private set; } = false;
		bool L_0 = ___value0;
		__this->set_U3CIsGrabbableU3Ek__BackingField_4(L_0);
		return;
	}
}
// UnityEngine.Vector3 UIPuzzleToken::get_StartPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  UIPuzzleToken_get_StartPosition_m2034065853 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// public Vector3 StartPosition { get; private set; }
		Vector3_t3722313464  L_0 = __this->get_U3CStartPositionU3Ek__BackingField_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// System.Void UIPuzzleToken::set_StartPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_set_StartPosition_m1033295995 (UIPuzzleToken_t1662105885 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector3 StartPosition { get; private set; }
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_U3CStartPositionU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void UIPuzzleToken::add_OnDragEvent(UIPuzzleToken/PuzzleTokenEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_add_OnDragEvent_m3807187468 (RuntimeObject * __this /* static, unused */, PuzzleTokenEventHandler_t3807414068 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_add_OnDragEvent_m3807187468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PuzzleTokenEventHandler_t3807414068 * V_0 = NULL;
	PuzzleTokenEventHandler_t3807414068 * V_1 = NULL;
	{
		PuzzleTokenEventHandler_t3807414068 * L_0 = ((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_OnDragEvent_7();
		V_0 = L_0;
	}

IL_0006:
	{
		PuzzleTokenEventHandler_t3807414068 * L_1 = V_0;
		V_1 = L_1;
		PuzzleTokenEventHandler_t3807414068 * L_2 = V_1;
		PuzzleTokenEventHandler_t3807414068 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		PuzzleTokenEventHandler_t3807414068 * L_5 = V_0;
		PuzzleTokenEventHandler_t3807414068 * L_6 = InterlockedCompareExchangeImpl<PuzzleTokenEventHandler_t3807414068 *>((((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_address_of_OnDragEvent_7()), ((PuzzleTokenEventHandler_t3807414068 *)CastclassSealed((RuntimeObject*)L_4, PuzzleTokenEventHandler_t3807414068_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		PuzzleTokenEventHandler_t3807414068 * L_7 = V_0;
		PuzzleTokenEventHandler_t3807414068 * L_8 = V_1;
		if ((!(((RuntimeObject*)(PuzzleTokenEventHandler_t3807414068 *)L_7) == ((RuntimeObject*)(PuzzleTokenEventHandler_t3807414068 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UIPuzzleToken::remove_OnDragEvent(UIPuzzleToken/PuzzleTokenEventHandler)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_remove_OnDragEvent_m2242078314 (RuntimeObject * __this /* static, unused */, PuzzleTokenEventHandler_t3807414068 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_remove_OnDragEvent_m2242078314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PuzzleTokenEventHandler_t3807414068 * V_0 = NULL;
	PuzzleTokenEventHandler_t3807414068 * V_1 = NULL;
	{
		PuzzleTokenEventHandler_t3807414068 * L_0 = ((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_OnDragEvent_7();
		V_0 = L_0;
	}

IL_0006:
	{
		PuzzleTokenEventHandler_t3807414068 * L_1 = V_0;
		V_1 = L_1;
		PuzzleTokenEventHandler_t3807414068 * L_2 = V_1;
		PuzzleTokenEventHandler_t3807414068 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		PuzzleTokenEventHandler_t3807414068 * L_5 = V_0;
		PuzzleTokenEventHandler_t3807414068 * L_6 = InterlockedCompareExchangeImpl<PuzzleTokenEventHandler_t3807414068 *>((((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_address_of_OnDragEvent_7()), ((PuzzleTokenEventHandler_t3807414068 *)CastclassSealed((RuntimeObject*)L_4, PuzzleTokenEventHandler_t3807414068_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		PuzzleTokenEventHandler_t3807414068 * L_7 = V_0;
		PuzzleTokenEventHandler_t3807414068 * L_8 = V_1;
		if ((!(((RuntimeObject*)(PuzzleTokenEventHandler_t3807414068 *)L_7) == ((RuntimeObject*)(PuzzleTokenEventHandler_t3807414068 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UIPuzzleToken::InitToken()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_InitToken_m3887138451 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_InitToken_m3887138451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rectTransform = GetComponent<RectTransform>();
		// rectTransform = GetComponent<RectTransform>();
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_rectTransform_8(L_0);
		// StartPosition = transform.position;
		// StartPosition = transform.position;
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// StartPosition = transform.position;
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		// StartPosition = transform.position;
		UIPuzzleToken_set_StartPosition_m1033295995(__this, L_2, /*hidden argument*/NULL);
		// IsGrabbable = true;
		// IsGrabbable = true;
		UIPuzzleToken_set_IsGrabbable_m4025083784(__this, (bool)1, /*hidden argument*/NULL);
		// image = GetComponent<Image>();
		// image = GetComponent<Image>();
		Image_t2670269651 * L_3 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_image_14(L_3);
		// }
		return;
	}
}
// System.Void UIPuzzleToken::Update()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_Update_m1559844202 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_Update_m1559844202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// if (matched) return;
		bool L_0 = __this->get_matched_5();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (matched) return;
		goto IL_00d1;
	}

IL_0011:
	{
		// deltaTime = Time.deltaTime;
		// deltaTime = Time.deltaTime;
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_deltaTime_11(L_1);
		// if (restart)
		bool L_2 = __this->get_restart_10();
		if (!L_2)
		{
			goto IL_00ae;
		}
	}
	{
		// float step = moveSpeed * deltaTime;
		float L_3 = __this->get_moveSpeed_3();
		float L_4 = __this->get_deltaTime_11();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_3, (float)L_4));
		// position = Vector3.Lerp(transform.position, StartPosition, step);
		// position = Vector3.Lerp(transform.position, StartPosition, step);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// position = Vector3.Lerp(transform.position, StartPosition, step);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		// position = Vector3.Lerp(transform.position, StartPosition, step);
		Vector3_t3722313464  L_7 = UIPuzzleToken_get_StartPosition_m2034065853(__this, /*hidden argument*/NULL);
		float L_8 = V_0;
		// position = Vector3.Lerp(transform.position, StartPosition, step);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_position_12(L_9);
		// transform.position = position;
		// transform.position = position;
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = __this->get_position_12();
		// transform.position = position;
		NullCheck(L_10);
		Transform_set_position_m3387557959(L_10, L_11, /*hidden argument*/NULL);
		// if ((transform.position - StartPosition).magnitude < 0.1f)
		// if ((transform.position - StartPosition).magnitude < 0.1f)
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// if ((transform.position - StartPosition).magnitude < 0.1f)
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		// if ((transform.position - StartPosition).magnitude < 0.1f)
		Vector3_t3722313464  L_14 = UIPuzzleToken_get_StartPosition_m2034065853(__this, /*hidden argument*/NULL);
		// if ((transform.position - StartPosition).magnitude < 0.1f)
		Vector3_t3722313464  L_15 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		// if ((transform.position - StartPosition).magnitude < 0.1f)
		float L_16 = Vector3_get_magnitude_m27958459((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_16) < ((float)(0.1f)))))
		{
			goto IL_00ad;
		}
	}
	{
		// transform.position = StartPosition;
		// transform.position = StartPosition;
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		// transform.position = StartPosition;
		Vector3_t3722313464  L_18 = UIPuzzleToken_get_StartPosition_m2034065853(__this, /*hidden argument*/NULL);
		// transform.position = StartPosition;
		NullCheck(L_17);
		Transform_set_position_m3387557959(L_17, L_18, /*hidden argument*/NULL);
		// restart = false;
		__this->set_restart_10((bool)0);
		// IsGrabbable = true;
		// IsGrabbable = true;
		UIPuzzleToken_set_IsGrabbable_m4025083784(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_00ad:
	{
	}

IL_00ae:
	{
		// if(dragged)
		bool L_19 = __this->get_dragged_13();
		if (!L_19)
		{
			goto IL_00d1;
		}
	}
	{
		// rectTransform.position = mousePosition;
		RectTransform_t3704657025 * L_20 = __this->get_rectTransform_8();
		Vector2_t2156229523  L_21 = __this->get_mousePosition_9();
		// rectTransform.position = mousePosition;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_22 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		// rectTransform.position = mousePosition;
		NullCheck(L_20);
		Transform_set_position_m3387557959(L_20, L_22, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		// }
		return;
	}
}
// System.Void UIPuzzleToken::RestartToken()
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_RestartToken_m3577290980 (UIPuzzleToken_t1662105885 * __this, const RuntimeMethod* method)
{
	{
		// IsGrabbable = false;
		// IsGrabbable = false;
		UIPuzzleToken_set_IsGrabbable_m4025083784(__this, (bool)0, /*hidden argument*/NULL);
		// restart = true;
		__this->set_restart_10((bool)1);
		// }
		return;
	}
}
// System.Void UIPuzzleToken::Match(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_Match_m1213795857 (UIPuzzleToken_t1662105885 * __this, Vector2_t2156229523  ___position0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_Match_m1213795857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// matched = true;
		__this->set_matched_5((bool)1);
		// image.color = Color.black;
		Image_t2670269651 * L_0 = __this->get_image_14();
		// image.color = Color.black;
		Color_t2555686324  L_1 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		// image.color = Color.black;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		// rectTransform.position = position;
		RectTransform_t3704657025 * L_2 = __this->get_rectTransform_8();
		Vector2_t2156229523  L_3 = ___position0;
		// rectTransform.position = position;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// rectTransform.position = position;
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPuzzleToken::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_OnDrag_m2089967016 (UIPuzzleToken_t1662105885 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		// mousePosition = eventData.position;
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		// mousePosition = eventData.position;
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = PointerEventData_get_position_m437660275(L_0, /*hidden argument*/NULL);
		__this->set_mousePosition_9(L_1);
		// }
		return;
	}
}
// System.Void UIPuzzleToken::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_OnBeginDrag_m1840991787 (UIPuzzleToken_t1662105885 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_OnBeginDrag_m1840991787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnDragEvent?.Invoke(this, true);
		PuzzleTokenEventHandler_t3807414068 * L_0 = ((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_OnDragEvent_7();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		goto IL_0019;
	}

IL_000d:
	{
		PuzzleTokenEventHandler_t3807414068 * L_1 = ((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_OnDragEvent_7();
		// OnDragEvent?.Invoke(this, true);
		NullCheck(L_1);
		PuzzleTokenEventHandler_Invoke_m834018601(L_1, __this, (bool)1, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// dragged = true;
		__this->set_dragged_13((bool)1);
		// }
		return;
	}
}
// System.Void UIPuzzleToken::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void UIPuzzleToken_OnEndDrag_m2554922906 (UIPuzzleToken_t1662105885 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPuzzleToken_OnEndDrag_m2554922906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnDragEvent?.Invoke(this, false);
		PuzzleTokenEventHandler_t3807414068 * L_0 = ((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_OnDragEvent_7();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		goto IL_0019;
	}

IL_000d:
	{
		PuzzleTokenEventHandler_t3807414068 * L_1 = ((UIPuzzleToken_t1662105885_StaticFields*)il2cpp_codegen_static_fields_for(UIPuzzleToken_t1662105885_il2cpp_TypeInfo_var))->get_OnDragEvent_7();
		// OnDragEvent?.Invoke(this, false);
		NullCheck(L_1);
		PuzzleTokenEventHandler_Invoke_m834018601(L_1, __this, (bool)0, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// dragged = false;
		__this->set_dragged_13((bool)0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPuzzleToken/PuzzleTokenEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void PuzzleTokenEventHandler__ctor_m94650657 (PuzzleTokenEventHandler_t3807414068 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIPuzzleToken/PuzzleTokenEventHandler::Invoke(UIPuzzleToken,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PuzzleTokenEventHandler_Invoke_m834018601 (PuzzleTokenEventHandler_t3807414068 * __this, UIPuzzleToken_t1662105885 * ___token0, bool ___dragged1, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___token0, ___dragged1, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___token0, ___dragged1, targetMethod);
					}
				}
			}
			else
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(targetMethod, targetThis, ___token0, ___dragged1);
							else
								GenericVirtActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(targetMethod, targetThis, ___token0, ___dragged1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___token0, ___dragged1);
							else
								VirtActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___token0, ___dragged1);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___token0, ___dragged1, targetMethod);
					}
				}
				else
				{
					// open
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< bool >::Invoke(targetMethod, ___token0, ___dragged1);
							else
								GenericVirtActionInvoker1< bool >::Invoke(targetMethod, ___token0, ___dragged1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___token0, ___dragged1);
							else
								VirtActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___token0, ___dragged1);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___token0, ___dragged1, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___token0, ___dragged1, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___token0, ___dragged1, targetMethod);
				}
			}
		}
		else
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(targetMethod, targetThis, ___token0, ___dragged1);
						else
							GenericVirtActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(targetMethod, targetThis, ___token0, ___dragged1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___token0, ___dragged1);
						else
							VirtActionInvoker2< UIPuzzleToken_t1662105885 *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___token0, ___dragged1);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___token0, ___dragged1, targetMethod);
				}
			}
			else
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< bool >::Invoke(targetMethod, ___token0, ___dragged1);
						else
							GenericVirtActionInvoker1< bool >::Invoke(targetMethod, ___token0, ___dragged1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___token0, ___dragged1);
						else
							VirtActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___token0, ___dragged1);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (UIPuzzleToken_t1662105885 *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___token0, ___dragged1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UIPuzzleToken/PuzzleTokenEventHandler::BeginInvoke(UIPuzzleToken,System.Boolean,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* PuzzleTokenEventHandler_BeginInvoke_m2388511976 (PuzzleTokenEventHandler_t3807414068 * __this, UIPuzzleToken_t1662105885 * ___token0, bool ___dragged1, AsyncCallback_t3962456242 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleTokenEventHandler_BeginInvoke_m2388511976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___token0;
	__d_args[1] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___dragged1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UIPuzzleToken/PuzzleTokenEventHandler::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void PuzzleTokenEventHandler_EndInvoke_m3431210348 (PuzzleTokenEventHandler_t3807414068 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Texture2D Util.Tools2D::ResizeTexture(UnityEngine.Texture2D,Util.ImageFilterMode,System.Single)
extern "C" IL2CPP_METHOD_ATTR Texture2D_t3840446185 * Tools2D_ResizeTexture_m3175497502 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * ___pSource0, int32_t ___pFilterMode1, float ___pScale2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools2D_ResizeTexture_m3175497502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t941916413* V_1 = NULL;
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Texture2D_t3840446185 * V_5 = NULL;
	int32_t V_6 = 0;
	ColorU5BU5D_t941916413* V_7 = NULL;
	Vector2_t2156229523  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2156229523  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	int32_t V_12 = 0;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	Vector4_t3319028937  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Color_t2555686324  V_24;
	memset(&V_24, 0, sizeof(V_24));
	float V_25 = 0.0f;
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	Texture2D_t3840446185 * V_28 = NULL;
	{
		// Color[] aSourceColor = pSource.GetPixels(0);
		Texture2D_t3840446185 * L_0 = ___pSource0;
		// Color[] aSourceColor = pSource.GetPixels(0);
		NullCheck(L_0);
		ColorU5BU5D_t941916413* L_1 = Texture2D_GetPixels_m2757678651(L_0, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		// Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);
		Texture2D_t3840446185 * L_2 = ___pSource0;
		// Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_t3840446185 * L_4 = ___pSource0;
		// Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		// Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);
		Vector2__ctor_m3970636864((&V_2), (((float)((float)L_3))), (((float)((float)L_5))), /*hidden argument*/NULL);
		// float xWidth = Mathf.RoundToInt((float)pSource.width * pScale);
		Texture2D_t3840446185 * L_6 = ___pSource0;
		// float xWidth = Mathf.RoundToInt((float)pSource.width * pScale);
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		float L_8 = ___pScale2;
		// float xWidth = Mathf.RoundToInt((float)pSource.width * pScale);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_7))), (float)L_8)), /*hidden argument*/NULL);
		V_3 = (((float)((float)L_9)));
		// float xHeight = Mathf.RoundToInt((float)pSource.height * pScale);
		Texture2D_t3840446185 * L_10 = ___pSource0;
		// float xHeight = Mathf.RoundToInt((float)pSource.height * pScale);
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_10);
		float L_12 = ___pScale2;
		// float xHeight = Mathf.RoundToInt((float)pSource.height * pScale);
		int32_t L_13 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_11))), (float)L_12)), /*hidden argument*/NULL);
		V_4 = (((float)((float)L_13)));
		// Texture2D oNewTex = new Texture2D((int)xWidth, (int)xHeight, TextureFormat.RGBA32, false);
		float L_14 = V_3;
		float L_15 = V_4;
		// Texture2D oNewTex = new Texture2D((int)xWidth, (int)xHeight, TextureFormat.RGBA32, false);
		Texture2D_t3840446185 * L_16 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2862217990(L_16, (((int32_t)((int32_t)L_14))), (((int32_t)((int32_t)L_15))), 4, (bool)0, /*hidden argument*/NULL);
		V_5 = L_16;
		// int xLength = (int)xWidth * (int)xHeight;
		float L_17 = V_3;
		float L_18 = V_4;
		V_6 = ((int32_t)il2cpp_codegen_multiply((int32_t)(((int32_t)((int32_t)L_17))), (int32_t)(((int32_t)((int32_t)L_18)))));
		// Color[] aColor = new Color[xLength];
		int32_t L_19 = V_6;
		V_7 = ((ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)L_19));
		// Vector2 vPixelSize = new Vector2(vSourceSize.x / xWidth, vSourceSize.y / xHeight);
		float L_20 = (&V_2)->get_x_0();
		float L_21 = V_3;
		float L_22 = (&V_2)->get_y_1();
		float L_23 = V_4;
		// Vector2 vPixelSize = new Vector2(vSourceSize.x / xWidth, vSourceSize.y / xHeight);
		Vector2__ctor_m3970636864((&V_8), ((float)((float)L_20/(float)L_21)), ((float)((float)L_22/(float)L_23)), /*hidden argument*/NULL);
		// Vector2 vCenter = new Vector2();
		il2cpp_codegen_initobj((&V_9), sizeof(Vector2_t2156229523 ));
		// for (i = 0; i < xLength; i++)
		V_0 = 0;
		goto IL_0385;
	}

IL_0087:
	{
		// float xX = (float)i % xWidth;
		int32_t L_24 = V_0;
		float L_25 = V_3;
		V_10 = (fmodf((((float)((float)L_24))), L_25));
		// float xY = Mathf.Floor((float)i / xWidth);
		int32_t L_26 = V_0;
		float L_27 = V_3;
		// float xY = Mathf.Floor((float)i / xWidth);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_28 = floorf(((float)((float)(((float)((float)L_26)))/(float)L_27)));
		V_11 = L_28;
		// vCenter.x = (xX / xWidth) * vSourceSize.x;
		float L_29 = V_10;
		float L_30 = V_3;
		float L_31 = (&V_2)->get_x_0();
		(&V_9)->set_x_0(((float)il2cpp_codegen_multiply((float)((float)((float)L_29/(float)L_30)), (float)L_31)));
		// vCenter.y = (xY / xHeight) * vSourceSize.y;
		float L_32 = V_11;
		float L_33 = V_4;
		float L_34 = (&V_2)->get_y_1();
		(&V_9)->set_y_1(((float)il2cpp_codegen_multiply((float)((float)((float)L_32/(float)L_33)), (float)L_34)));
		// if (pFilterMode == ImageFilterMode.Nearest)
		int32_t L_35 = ___pFilterMode1;
		if (L_35)
		{
			goto IL_0127;
		}
	}
	{
		// vCenter.x = Mathf.Round(vCenter.x);
		float L_36 = (&V_9)->get_x_0();
		// vCenter.x = Mathf.Round(vCenter.x);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_37 = bankers_roundf(L_36);
		(&V_9)->set_x_0(L_37);
		// vCenter.y = Mathf.Round(vCenter.y);
		float L_38 = (&V_9)->get_y_1();
		// vCenter.y = Mathf.Round(vCenter.y);
		float L_39 = bankers_roundf(L_38);
		(&V_9)->set_y_1(L_39);
		// int xSourceIndex = (int)((vCenter.y * vSourceSize.x) + vCenter.x);
		float L_40 = (&V_9)->get_y_1();
		float L_41 = (&V_2)->get_x_0();
		float L_42 = (&V_9)->get_x_0();
		V_12 = (((int32_t)((int32_t)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_40, (float)L_41)), (float)L_42)))));
		// aColor[i] = aSourceColor[xSourceIndex];
		ColorU5BU5D_t941916413* L_43 = V_7;
		int32_t L_44 = V_0;
		NullCheck(L_43);
		ColorU5BU5D_t941916413* L_45 = V_1;
		int32_t L_46 = V_12;
		NullCheck(L_45);
		*(Color_t2555686324 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44))) = (*(Color_t2555686324 *)((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46))));
		goto IL_0380;
	}

IL_0127:
	{
		// else if (pFilterMode == ImageFilterMode.Bilinear)
		int32_t L_47 = ___pFilterMode1;
		if ((!(((uint32_t)L_47) == ((uint32_t)1))))
		{
			goto IL_0247;
		}
	}
	{
		// float xRatioX = vCenter.x - Mathf.Floor(vCenter.x);
		float L_48 = (&V_9)->get_x_0();
		float L_49 = (&V_9)->get_x_0();
		// float xRatioX = vCenter.x - Mathf.Floor(vCenter.x);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_50 = floorf(L_49);
		V_13 = ((float)il2cpp_codegen_subtract((float)L_48, (float)L_50));
		// float xRatioY = vCenter.y - Mathf.Floor(vCenter.y);
		float L_51 = (&V_9)->get_y_1();
		float L_52 = (&V_9)->get_y_1();
		// float xRatioY = vCenter.y - Mathf.Floor(vCenter.y);
		float L_53 = floorf(L_52);
		V_14 = ((float)il2cpp_codegen_subtract((float)L_51, (float)L_53));
		// int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
		float L_54 = (&V_9)->get_y_1();
		// int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
		float L_55 = floorf(L_54);
		float L_56 = (&V_2)->get_x_0();
		float L_57 = (&V_9)->get_x_0();
		// int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
		float L_58 = floorf(L_57);
		V_15 = (((int32_t)((int32_t)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_55, (float)L_56)), (float)L_58)))));
		// int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
		float L_59 = (&V_9)->get_y_1();
		// int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
		float L_60 = floorf(L_59);
		float L_61 = (&V_2)->get_x_0();
		float L_62 = (&V_9)->get_x_0();
		// int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
		float L_63 = ceilf(L_62);
		V_16 = (((int32_t)((int32_t)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_60, (float)L_61)), (float)L_63)))));
		// int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
		float L_64 = (&V_9)->get_y_1();
		// int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
		float L_65 = ceilf(L_64);
		float L_66 = (&V_2)->get_x_0();
		float L_67 = (&V_9)->get_x_0();
		// int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
		float L_68 = floorf(L_67);
		V_17 = (((int32_t)((int32_t)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_65, (float)L_66)), (float)L_68)))));
		// int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
		float L_69 = (&V_9)->get_y_1();
		// int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
		float L_70 = ceilf(L_69);
		float L_71 = (&V_2)->get_x_0();
		float L_72 = (&V_9)->get_x_0();
		// int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
		float L_73 = ceilf(L_72);
		V_18 = (((int32_t)((int32_t)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_70, (float)L_71)), (float)L_73)))));
		// aColor[i] = Color.Lerp(
		ColorU5BU5D_t941916413* L_74 = V_7;
		int32_t L_75 = V_0;
		NullCheck(L_74);
		ColorU5BU5D_t941916413* L_76 = V_1;
		int32_t L_77 = V_15;
		NullCheck(L_76);
		ColorU5BU5D_t941916413* L_78 = V_1;
		int32_t L_79 = V_16;
		NullCheck(L_78);
		float L_80 = V_13;
		// Color.Lerp(aSourceColor[xIndexTL], aSourceColor[xIndexTR], xRatioX),
		Color_t2555686324  L_81 = Color_Lerp_m973389909(NULL /*static, unused*/, (*(Color_t2555686324 *)((L_76)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_77)))), (*(Color_t2555686324 *)((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_79)))), L_80, /*hidden argument*/NULL);
		ColorU5BU5D_t941916413* L_82 = V_1;
		int32_t L_83 = V_17;
		NullCheck(L_82);
		ColorU5BU5D_t941916413* L_84 = V_1;
		int32_t L_85 = V_18;
		NullCheck(L_84);
		float L_86 = V_13;
		// Color.Lerp(aSourceColor[xIndexBL], aSourceColor[xIndexBR], xRatioX),
		Color_t2555686324  L_87 = Color_Lerp_m973389909(NULL /*static, unused*/, (*(Color_t2555686324 *)((L_82)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_83)))), (*(Color_t2555686324 *)((L_84)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_85)))), L_86, /*hidden argument*/NULL);
		float L_88 = V_14;
		// aColor[i] = Color.Lerp(
		Color_t2555686324  L_89 = Color_Lerp_m973389909(NULL /*static, unused*/, L_81, L_87, L_88, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_74)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_75))) = L_89;
		goto IL_0380;
	}

IL_0247:
	{
		// else if (pFilterMode == ImageFilterMode.Average)
		int32_t L_90 = ___pFilterMode1;
		if ((!(((uint32_t)L_90) == ((uint32_t)2))))
		{
			goto IL_0380;
		}
	}
	{
		// int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
		float L_91 = (&V_9)->get_x_0();
		float L_92 = (&V_8)->get_x_0();
		// int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_93 = floorf(((float)il2cpp_codegen_subtract((float)L_91, (float)((float)il2cpp_codegen_multiply((float)L_92, (float)(0.5f))))));
		// int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
		float L_94 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_93, (0.0f), /*hidden argument*/NULL);
		V_19 = (((int32_t)((int32_t)L_94)));
		// int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
		float L_95 = (&V_9)->get_x_0();
		float L_96 = (&V_8)->get_x_0();
		// int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
		float L_97 = ceilf(((float)il2cpp_codegen_add((float)L_95, (float)((float)il2cpp_codegen_multiply((float)L_96, (float)(0.5f))))));
		float L_98 = (&V_2)->get_x_0();
		// int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
		float L_99 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_97, L_98, /*hidden argument*/NULL);
		V_20 = (((int32_t)((int32_t)L_99)));
		// int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
		float L_100 = (&V_9)->get_y_1();
		float L_101 = (&V_8)->get_y_1();
		// int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
		float L_102 = floorf(((float)il2cpp_codegen_subtract((float)L_100, (float)((float)il2cpp_codegen_multiply((float)L_101, (float)(0.5f))))));
		// int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
		float L_103 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_102, (0.0f), /*hidden argument*/NULL);
		V_21 = (((int32_t)((int32_t)L_103)));
		// int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);
		float L_104 = (&V_9)->get_y_1();
		float L_105 = (&V_8)->get_y_1();
		// int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);
		float L_106 = ceilf(((float)il2cpp_codegen_add((float)L_104, (float)((float)il2cpp_codegen_multiply((float)L_105, (float)(0.5f))))));
		float L_107 = (&V_2)->get_y_1();
		// int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);
		float L_108 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_106, L_107, /*hidden argument*/NULL);
		V_22 = (((int32_t)((int32_t)L_108)));
		// Vector4 oColorTotal = new Vector4();
		il2cpp_codegen_initobj((&V_23), sizeof(Vector4_t3319028937 ));
		// Color oColorTemp = new Color();
		il2cpp_codegen_initobj((&V_24), sizeof(Color_t2555686324 ));
		// float xGridCount = 0;
		V_25 = (0.0f);
		// for (int iy = xYFrom; iy < xYTo; iy++)
		int32_t L_109 = V_21;
		V_26 = L_109;
		goto IL_035f;
	}

IL_030f:
	{
		// for (int ix = xXFrom; ix < xXTo; ix++)
		int32_t L_110 = V_19;
		V_27 = L_110;
		goto IL_034f;
	}

IL_0319:
	{
		// oColorTemp += aSourceColor[(int)(((float)iy * vSourceSize.x) + ix)];
		Color_t2555686324  L_111 = V_24;
		ColorU5BU5D_t941916413* L_112 = V_1;
		int32_t L_113 = V_26;
		float L_114 = (&V_2)->get_x_0();
		int32_t L_115 = V_27;
		NullCheck(L_112);
		// oColorTemp += aSourceColor[(int)(((float)iy * vSourceSize.x) + ix)];
		Color_t2555686324  L_116 = Color_op_Addition_m3293657895(NULL /*static, unused*/, L_111, (*(Color_t2555686324 *)((L_112)->GetAddressAt(static_cast<il2cpp_array_size_t>((((int32_t)((int32_t)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_113))), (float)L_114)), (float)(((float)((float)L_115)))))))))))), /*hidden argument*/NULL);
		V_24 = L_116;
		// xGridCount++;
		float L_117 = V_25;
		V_25 = ((float)il2cpp_codegen_add((float)L_117, (float)(1.0f)));
		// for (int ix = xXFrom; ix < xXTo; ix++)
		int32_t L_118 = V_27;
		V_27 = ((int32_t)il2cpp_codegen_add((int32_t)L_118, (int32_t)1));
	}

IL_034f:
	{
		// for (int ix = xXFrom; ix < xXTo; ix++)
		int32_t L_119 = V_27;
		int32_t L_120 = V_20;
		if ((((int32_t)L_119) < ((int32_t)L_120)))
		{
			goto IL_0319;
		}
	}
	{
		// for (int iy = xYFrom; iy < xYTo; iy++)
		int32_t L_121 = V_26;
		V_26 = ((int32_t)il2cpp_codegen_add((int32_t)L_121, (int32_t)1));
	}

IL_035f:
	{
		// for (int iy = xYFrom; iy < xYTo; iy++)
		int32_t L_122 = V_26;
		int32_t L_123 = V_22;
		if ((((int32_t)L_122) < ((int32_t)L_123)))
		{
			goto IL_030f;
		}
	}
	{
		// aColor[i] = oColorTemp / (float)xGridCount;
		ColorU5BU5D_t941916413* L_124 = V_7;
		int32_t L_125 = V_0;
		NullCheck(L_124);
		Color_t2555686324  L_126 = V_24;
		float L_127 = V_25;
		// aColor[i] = oColorTemp / (float)xGridCount;
		Color_t2555686324  L_128 = Color_op_Division_m1074517668(NULL /*static, unused*/, L_126, (((float)((float)L_127))), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_124)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_125))) = L_128;
	}

IL_0380:
	{
		// for (i = 0; i < xLength; i++)
		int32_t L_129 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_129, (int32_t)1));
	}

IL_0385:
	{
		// for (i = 0; i < xLength; i++)
		int32_t L_130 = V_0;
		int32_t L_131 = V_6;
		if ((((int32_t)L_130) < ((int32_t)L_131)))
		{
			goto IL_0087;
		}
	}
	{
		// oNewTex.SetPixels(aColor);
		Texture2D_t3840446185 * L_132 = V_5;
		ColorU5BU5D_t941916413* L_133 = V_7;
		// oNewTex.SetPixels(aColor);
		NullCheck(L_132);
		Texture2D_SetPixels_m3008871897(L_132, L_133, /*hidden argument*/NULL);
		// oNewTex.Apply();
		Texture2D_t3840446185 * L_134 = V_5;
		// oNewTex.Apply();
		NullCheck(L_134);
		Texture2D_Apply_m2271746283(L_134, /*hidden argument*/NULL);
		// return oNewTex;
		Texture2D_t3840446185 * L_135 = V_5;
		V_28 = L_135;
		goto IL_03a6;
	}

IL_03a6:
	{
		// }
		Texture2D_t3840446185 * L_136 = V_28;
		return L_136;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Util.UtilThread::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UtilThread__ctor_m4066518524 (UtilThread_t4012287602 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Util.UtilThread::Start()
extern "C" IL2CPP_METHOD_ATTR void UtilThread_Start_m661257546 (UtilThread_t4012287602 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Util.UtilThread::Update()
extern "C" IL2CPP_METHOD_ATTR void UtilThread_Update_m635515079 (UtilThread_t4012287602 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
