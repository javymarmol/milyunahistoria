﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestDataOld {

    public int questID;

    public bool isActivity;
    private bool activityCompleted;
    public string[] textActivity = new string[4];

    public bool isSelectMultiple;
    public int numberOptions;
    public string[] options;
    public int rigthOption;
    public bool[] optionsSelected = new bool[4];

    public bool isStory;
    public int numberOfStories;
    public string[] stories;
    private bool[] storiesCompleted = new bool[4];

    private string sceneName;

    public string startText, completedText, wrongText;
}
