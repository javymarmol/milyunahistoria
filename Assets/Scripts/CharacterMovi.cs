﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovi : MonoBehaviour {
    public int position = 0;
    readonly float speed = (float)0.1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(Input.mousePosition);
        //capturar posicion del puntero
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //definimos movimineto solamente en Y, que no de pase de la scene
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(mousePos.y,-3.02f,3.13f), transform.position.z);


	}

    public void ChangePosition(Transform point)
    {
        float diffX = CalculateDiference(transform.position.x, point.position.x);
        float diffY = CalculateDiference(transform.position.y, point.position.y);
        if (diffX < diffY)
        {
            if(transform.position.x < point.position.x)
            {
                transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
            }

            if (transform.position.y < point.position.y)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + speed, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);
            }
        }
        else
        {
            if (transform.position.y < point.position.y)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + speed, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);
            }

            if (transform.position.x < point.position.x)
            {
                transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
            }

        }
    }

    float CalculateDiference(float a, float b)
    {
        if(a<0 && b < 0)
        {
            return -1 * (a + b);
        }else if(a >= 0 && b >= 0)
        {
            return a - b;
        }
        else
        {
            return a + b;
        }

    }
}
