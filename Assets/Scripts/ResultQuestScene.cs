﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultQuestScene : MonoBehaviour {


    private QuestManager manager;
    private Quest quest;
    public Text text;
    SceneChanger sceneChanger;

    // Use this for initialization
    void Start () {
        sceneChanger = FindObjectOfType<SceneChanger>();
        manager = FindObjectOfType<QuestManager>();
        quest = manager.quests[manager.questID];
        if (!quest.isStory)
        {
            text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
        }
        else if(quest.isStory && quest.isSelectMultiple)
        {
            text.text = (manager.questCompleted[manager.questID] == true) ? quest.completedText.Replace("\\n", "\n") : quest.wrongText.Replace("\\n", "\n");
        }
        else
        {
            text.text = quest.completedText.Replace("\\n", "\n");
        }
        //Debug.Log("quest: " + manager.questID + " quest completed: " +
        //manager.questCompleted[manager.questID] + "textcompl: " + quest.completedText + "textwrong" + quest.wrongText);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
