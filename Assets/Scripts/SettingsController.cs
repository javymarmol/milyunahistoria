﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsController : MonoBehaviour {

    private GameController gameController;

    public void RestartData()
    {

        gameController = FindObjectOfType<GameController>();
        gameController.restarData();
    }
}
