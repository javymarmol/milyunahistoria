﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestManager : MonoBehaviour {

    public Quest[] quests;
    public bool[] questCompleted = new bool[30];
    public bool isCompleted;
    public int questID = 0;
    public int storySelected;

    private GameController gameController;

    public static QuestManager _instance;

    private void Awake()
    {
        _instance = this;
        GameObject[] objs = GameObject.FindGameObjectsWithTag("QuestManager");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }



    // Use this for initialization
    void Start () {
        //questCompleted = new bool[quests.Length];	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SaveData()
    {
        gameController = FindObjectOfType<GameController>();
        gameController.SaveProgress();
    }



}
