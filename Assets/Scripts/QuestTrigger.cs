﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class QuestTrigger : MonoBehaviour {

    private QuestManager manager;
    public int questID;

	// Use this for initialization
	void Start () {
        manager = FindObjectOfType<QuestManager>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("OnTriggerEnter2D " + questID + ", questCompleted: " + manager.questCompleted.Length);

        if (collision.gameObject.tag.Equals("Player"))
        {
            //if (!manager.questCompleted[questID])
            //{

            if (manager.questID == questID)
            {
                return;
            }
            else
            {
                //manager.quests[questID].StartQuest();
                StartCoroutine(DelayStartQuest());
            }
            //manager.quests[questID].StartQuest();
            //}
        }
    }

    IEnumerator DelayStartQuest()
    {
        yield return new WaitForSeconds(0.5f); 
        manager.quests[questID].StartQuest();
    }
}
