﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestScene : MonoBehaviour {

    private QuestManager manager;
    private Quest quest;
    public Text text;

    public Button[] options;
    public Text[] optionsText;

    //AudioManager
    private AudioRecordManager audioManager;
    private int idStory;

    public GameObject buttonStartRecord;
    public GameObject buttonStopRecord;

    //time variables
    float elapsedTime;
    float countdownTimeDefault = 30;
    float countdownTime;
    bool isactivityStarted = false;
    public Text countDownText;

    public GameObject startActivity;
    public GameObject completeActivity;
    public GameObject incompleteActivity;

    SceneChanger sceneChanger;

    // Use this for initialization
    void Start () {
        sceneChanger = FindObjectOfType<SceneChanger>();
        manager = FindObjectOfType<QuestManager>();
        quest = manager.quests[manager.questID];
        text.text = quest.startText.Replace("\\n", "\n");
        //Debug.Log("sceneNaame: " + SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name.Equals("SelectMultipleScene"))
        {
            for (int i = 0; i < options.Length; i++)
            {
                options[i].enabled = true;
            }
            for (int i = 0; i < quest.options.Length; i++)
            {
                optionsText[i].text = quest.options[i];
            }
            if (quest.options.Length < 4)
            {
                for (int i = quest.options.Length; i < 4; i++)
                {

                    options[i].enabled = false;
                    options[i].gameObject.SetActive(false);
                }
            }
            if (manager.questCompleted[manager.questID])
            {
                int i = 0;
                foreach (Button button in options)
                {
                    //button.gameObject.SetActive(false);
                    if(i == quest.rigthOption)
                    {
                        //Changes the button's Disabled color to the new color.
                        ColorBlock cb = button.colors;
                        cb.disabledColor = new Color(0.25f, 0.78f, 0.54f, 1.00f); //Color4f(0.25, 0.78, 0.54, 1.00)
                        //cb.disabledColor = Color.green;
                        button.colors = cb;
                        //button.colors.pressedColor = Color.green;
                        //button.GetComponent<Image>().color = Color.green;
                    }
                    button.interactable = false;
                    i++;
                }
            }
            return;
        }
        if (SceneManager.GetActiveScene().name.Equals("QuestionScene"))
        {
            if (quest.isSelectMultiple)
            {

                    text.text = quest.completedText.Replace("\\n", "\n");
                
            }

            audioManager = FindObjectOfType<AudioRecordManager>();

            for (int i = 0; i < quest.stories.Length; i++)
            {
                optionsText[i].text = quest.stories[i];
                if (quest.storiesCompleted[i])
                {
                    options[i].interactable = false;
                }
            }
            return;
        }
        if (SceneManager.GetActiveScene().name.Equals("RetoScene"))
        {
            if (quest.isSelectMultiple)
            {
                if(quest.optionsSelected == 0)
                {
                    text.text = quest.completedText.Replace("\\n", "\n");
                }
                else
                {
                    text.text = quest.wrongText.Replace("\\n", "\n");
                }
            }
            if (!manager.questCompleted[manager.questID])
            {
                completeActivity.SetActive(false);
                incompleteActivity.SetActive(false);
                countDownText.gameObject.SetActive(true);
                startActivity.gameObject.SetActive(true);

                if (quest.isFinal)
                {
                    countDownText.gameObject.SetActive(false);
                }
            }
            else
            {
                completeActivity.SetActive(true);
                completeActivity.GetComponent<Button>().interactable = false;
                completeActivity.transform.position = startActivity.transform.position;
                countDownText.gameObject.SetActive(false);
                startActivity.SetActive(false);

            }
            return;
        }
    }

    public void Update()
    {
        if (isactivityStarted)
        {
            elapsedTime += Time.deltaTime;
            if (Mathf.RoundToInt(elapsedTime) > countdownTimeDefault)
            {
                isactivityStarted = false;
                FinishActivity();
            }
            //elapsedTimeText.SetActive(true);
            //elapsedTimeText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime), false);
            //countDownText.SetActive(true);
            countDownText.GetComponent<Text>().text = FormatTime(Mathf.RoundToInt(elapsedTime - countdownTime), true);
        }
    }


    public void selectOption(int id)
    {
        quest.optionsSelected = id;
        if (id == quest.rigthOption)
        {
            if (quest.isActivity)
            {
                sceneChanger.ChangeSceneTo("RetoScene");
                return;
            }
            if (quest.isStory)
            {
                //manager.storySelected = 0;
                selectStory(0);
                return;
            }
            //options[id].GetComponent<Image>().color = Color.green;
            options[id].GetComponent<Image>().color = new Color(0.25f, 0.78f, 0.54f, 1.00f);
            quest.QuestComplete();
            if(quest.completedText.Length > 0)
            {
                sceneChanger.ChangeSceneTo("RightAnswerScene");
            }
            else
            {
                sceneChanger.ChangeSceneTo("MainScene");
            }

        }
        else
        {
            //options[id].GetComponent<Image>().color = Color.red;
            Debug.Log("wrong answer, is activity: "+ quest.isActivity);
            if(quest.isActivity)
            {
                Debug.Log("entro a quest.isActivity " + quest.questID);

                quest.rigthOption = id;
                sceneChanger.ChangeSceneTo("RetoScene");
                return;
            }

            //options[id].interactable = false;
            if (quest.wrongText.Length > 0)
            {
                Debug.Log("entro a quest.isActivity " + quest.questID);

                sceneChanger.ChangeSceneTo("RightAnswerScene");
            }
            else
            {
                sceneChanger.ChangeSceneTo("MainScene");
            }

        }
    }

    public void selectStory(int id)
    {
        manager.storySelected = id;
        sceneChanger.ChangeSceneTo("AudioScene");
    }

    public void startRecord(int id)
    {
        idStory = id;
        audioManager.OnStartMicButtonClick();
    }

    public void stopRecord()
    {
        //audioManager.StopRecord(manager.questID + " " + idStory);
    }

    public void StartActivity()
    {
        if (quest.isFinal)
        {
            sceneChanger.ChangeSceneTo("PuzzleScene");
        }
        isactivityStarted = true;
        elapsedTime = 0;
        countdownTime = countdownTimeDefault;
        startActivity.SetActive(false);
    }

    public void FinishActivity()
    {
        //startActivity.SetActive(false);
        completeActivity.SetActive(true);
        incompleteActivity.SetActive(true);
        countDownText.gameObject.SetActive(false);
    }

    private string FormatTime(int time, bool countdownd)
    {
        int mins = time / 60;
        int secs = countdownd ? time % 60 * -1 : time % 60;
        string timeFormat = string.Format("TIEMPO:  {0}:{1}", mins, secs);
        //Debug.Log(timeFormat);
        return timeFormat;
    }

    public void CompleteActivity()
    {
        quest.QuestComplete();
        if (quest.isSelectMultiple)
        {
            sceneChanger.ChangeSceneTo("MainScene");
            return;
        }
        if (quest.completedText.Length > 0)
        {
            sceneChanger.ChangeSceneTo("RightAnswerScene");
        }
        else
        {
            sceneChanger.ChangeSceneTo("MainScene");
        }
    }
    public void IncompleteActivity()
    {
        if (quest.isSelectMultiple)
        {
            sceneChanger.ChangeSceneTo("MainScene");
            return;
        }
        if (quest.wrongText.Length > 0)
        {
            sceneChanger.ChangeSceneTo("RightAnswerScene");
        }
        else
        {
            sceneChanger.ChangeSceneTo("MainScene");
        }
    }

}
