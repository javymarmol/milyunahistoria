﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LitJson;

public class DataController : MonoBehaviour {

    public PlayerData playerData;
    public GameData gameData;
    //public QuestManager gameDataManager;
    private readonly string gameDataFileName = "game.json";
    private readonly string playerDataFileName = "player.json";
    private int isFirstTime;
    private int currentLevel;

    //quest scripts

    private QuestManager manager;
    private Quest quest;

    void Awake ()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("DataController");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        //LoadPlayerData();
        //LoadGameData();
	}

    public void LoadPlayerData()
    {
        /*
        playerData = new PlayerData();

        if (PlayerPrefs.HasKey("isFirstTime")){
            playerData.isFirstTime = PlayerPrefs.GetInt("isFirstTime");
        }
        if (PlayerPrefs.HasKey("currentLevel"))
        {
            playerData.isFirstTime = PlayerPrefs.GetInt("currentLevel");
        }
        */
        string filePath = Path.Combine(Application.persistentDataPath, playerDataFileName);
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            PlayerData loadedData = JsonUtility.FromJson<PlayerData>(dataAsJson);

            playerData = loadedData ?? new PlayerData();

            //playerData.isFirstTime = (loadedData != null) ? loadedData.isFirstTime : 0;
            //playerData.currentLevel = (loadedData != null) ? loadedData.currentLevel : 1; 
            //playerData.posLevelCharacter = (loadedData != null) ? loadedData.posLevelCharacter : 0;
        }
        else
        {

            Debug.Log("No found file Player!!!");
            playerData = new PlayerData();
            using (FileStream fs =  File.Create(filePath))
            {
                fs.WriteByte(new byte());
            }

            File.WriteAllText(filePath, JsonUtility.ToJson(playerData));

        }
    }

    public void SavePlayerData()
    {
        //PlayerPrefs.SetInt("isFirstTime", playerData.isFirstTime);
        //PlayerPrefs.SetInt("currentLevel", playerData.currentLevel);
        string filePath = Path.Combine(Application.persistentDataPath, playerDataFileName);
        File.WriteAllText(filePath, JsonUtility.ToJson(playerData));
    }

    public void DeletePlayerData()
    {
        string filePath = Path.Combine(Application.persistentDataPath, playerDataFileName);
        File.Delete(filePath);
    }

    public void IsFirstTime(int firstTime)
    {
        if(firstTime > 0)
        {
            playerData.isFirstTime = firstTime;
            SavePlayerData();
        }
    }

    public int GetFirstTime()
    {
        return playerData.isFirstTime;
    }

    public void CurrentLevel(int currentLevel)
    {
        playerData.currentLevel = currentLevel;
        SavePlayerData();
    }

    public int GetCurrenLevel()
    {
        return playerData.currentLevel;
    }


    private void LoadGameSettings()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            PlayerData loadedData = JsonUtility.FromJson<PlayerData>(dataAsJson);

            isFirstTime = loadedData.isFirstTime;
            currentLevel = loadedData.currentLevel;
        }
        else
        {
            Debug.LogError("No found file!!!");
            playerData = new PlayerData();
        }
    }

    public void LoadGameData()
    {
        string filePath = Path.Combine(Application.persistentDataPath, gameDataFileName);
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            //QuestData[] loadedData = JsonUtility.FromJson<QuestData[]>(dataAsJson);
            GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);

            //gameData = loadedData;
            gameData = loadedData ?? new GameData();
        }
        else
        {

            Debug.Log("No found file gameData!!!");
            gameData = new GameData();
            //gameDataManager = FindObjectOfType<QuestManager>();
            using (FileStream fs = File.Create(filePath))
            {
                fs.WriteByte(new byte());
            }

            File.WriteAllText(filePath, JsonUtility.ToJson(gameData));

        }
    }

    public void SaveGameData()
    {
        string filePath = Path.Combine(Application.persistentDataPath, gameDataFileName);
        File.WriteAllText(filePath, JsonUtility.ToJson(gameData));
    }

    public void DeleteGameData()
    {
        string filePath = Path.Combine(Application.persistentDataPath, gameDataFileName);
        File.Delete(filePath);

        manager = FindObjectOfType<QuestManager>();
        manager.questCompleted = new bool[30];
        Debug.Log(manager.quests.Length);
        foreach (Quest q in manager.quests)
        {
            q.storiesCompleted = new bool[4];
            q.optionsSelected = -1;
            if (q.isStory)
            {
                int i = 0;
                foreach (string story in q.stories)
                {
                    string nameStory = "story_" + q.questID + "_" + i;
                    string file = Path.Combine(Application.persistentDataPath, nameStory + ".mp3");
                    if (File.Exists(file))
                    {

                        File.Delete(file);
                    }
                    i++;
                }
            }

        }
    }


}
