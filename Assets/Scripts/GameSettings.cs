﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour {

    public Question[] questions = {
        new Question("audio", 0, 
            "Punto de partida del viaje y, según los antiguos pobladores de la región, el centro del origen del universo. El acceso está restringido, excepto para los sabios que todavía quedan en la región.\r\nAcacio: Despierta, despierta, amigo. Siento que alguien se acerca. ¿Que cómo lo sé? Escuché algo o alguien moviéndose. Y, no sé, solo lo siento. Están llegando. Son varios y nos están buscando. Espero que sean bondadosos con nosotros.\r\nVoz: Oigan, ustedes dos. ¿Quiénes son?\r\nAcacio: Solo somos viajeros, peregrinos. Estamos en busca de un transporte para mi amigo, para que vuelva a casa. También, buscamos conocimiento. ¿Quiénes son ustedes? ¿Por qué no revelan su rostro?\r\nVoz: Preferimos permanecer escondidos. Lo único que tienen que saber es que estamos unidos a esta tierra y buscamos alguna retribución de ustedes. Finalmente, están usando esta tierra para dormir y comer sin permiso de los dioses.\r\nAcacio: Está bien, ¿qué quieren a cambio?\r\nVoz: Somos guardianes y esta es nuestra casa, nuestro hogar desde hace mucho. Vivimos en este, el origen del universo y de todo lo que existe desde hace ya milenios. Queremos saber sobre la casa de tu infancia, viajero, sí tú, el que no ha hablado todavía. ¿En qué tierra construyeron tu hogar? ¿Qué recuerdos tienes de ella?\r\n(Luego de la respuesta)\r\nVoz: Es realmente una historia maravillosa, viajero. Has agradado a los dioses y seguramente tendrás buena fortuna en lo que te queda de camino. Los dejaremos descansar. \r\nAcacio: Esperen, ¿quiénes son? Ya nos pueden decir.\r\nVoz: Somos el espíritu de este desierto. Nuestro hogar son las piedras y la arena de esta región. Nos preguntabas por qué no mostramos nuestro rostro. Es sencillamente porque no tenemos rostro. Somos el cielo y las plantas que habitan por aquí. Hasta luego, queridos amigos. Tomen esta pieza para que nos recuerden durante su viaje.\r\n",
            null, 0, null, "Voz: Es realmente una historia maravillosa, viajero. Has agradado a los dioses y seguramente tendrás buena fortuna en lo que te queda de camino. Los dejaremos descansar. \r\nAcacio: Esperen, ¿quiénes son? Ya nos pueden decir.\r\nVoz: Somos el espíritu de este desierto. Nuestro hogar son las piedras y la arena de esta región. Nos preguntabas por qué no mostramos nuestro rostro. Es sencillamente porque no tenemos rostro. Somos el cielo y las plantas que habitan por aquí. Hasta luego, queridos amigos. Tomen esta pieza para que nos recuerden durante su viaje.\r\n"
            ),
        new Question("audio", 1,
            "Un pequeño bosque en el camino. Un oasis en medio del desierto aprovechado por los viajeros para descansar. Sin embargo, allí habita el gobernante Gerardo Guerrero Torres, quien siempre está satisfecho de escuchar una buena historia, y formula acertijos imposibles a los que se acercan.\r\n\r\nGerardo: ¡Bienvenidos, viajeros! ¿Qué vienen a hacer a mi oasis? Quieren descansar, por supuesto, y supongo que beber un poco de agua. Bien, siempre disfruto una buena historia. No pediría nada, pero ustedes se ven como gente interesante. \r\n¿Cómo fue tu primer día de colegio, sabio amigo? Siempre es interesante saber cómo empiezan las grandes aventuras intelectuales.\r\n(Luego de la respuesta)\r\nGerardo: Tu historia es de mi agrado. Puedes quedarte aquí tan sólo por una noche. Si tienes más historias, puedes quedarte más tiempo. Espero que tu estadía sea agradable.\r\n"
            ),
        new Question("multiple", 2,
            "Es la segunda ciudad en importancia y tamaño de la región. No obstante, la mayoría de los pobladores han migrado a causa de una fuerte sequía que azota a la parte occidental de la región. Algunos mendigos piden comida y consejos a los viajeros.\r\n\r\nAcacio: Umare parece ser un buen punto para dormir. No creo que encontremos comida aquí. La sequía dejó esta pobre tierra devastada. Cuando era niño tuve la oportunidad de venir aquí y ver los portentosos terrenos agrícolas que eran la maravilla de toda la región. Es una lástima.\r\nMendigo: Oigan, ustedes, por todos los cielos, ¿tienen algo de comer? Soy anciano y no tengo fuerzas para salir de Umare, pero aún puedo comer, y sí que me gusta comer. Qué delicia. Un buen chivo guisado. No. No, mejor aún, una buena ollada de caldo de chivo. Sí. Con aguadepanela con limón. Y de postre un dulce de guayabas. Y luego, otra vez, un buen pedazo de pollo guisado…\r\nAcacio: ¿Qué quieres, amigo?\r\nMendigo: Perdón, tiendo a irme por las ramas. Es como cuando era niño. Intentaba escalar un árbol, y terminaba subiendo a todos los que habían alrededor. Así, me quedaba horas jugando entre los árboles y…\r\nAcacio: Lo hiciste de nuevo. Ya sabemos que quieres comida. La verdad es que no tenemos. Estamos en un largo viaje.\r\nMendigo: Bien, sólo tengo una pregunta, entonces, un acertijo si se quiere. Además de comer y de divagar, me encanta poner a pensar a mis buenos amigos ¿Cómo saldrías de un foso de arenas movedizas?\r\n",
            new string[] { "a.\tBuscarías un palo largo para apoyarte.", "b.\tIntentarías patalear muy rápido solo con las piernas.", "c.\tBracearías suave y lentamente hacia la orilla, hasta que te puedas agarrar a algo firme.", "d.\tComenzarías a rezar para que sucediera un milagro." },
            3, "Mendigo: Creo que te equivocas. ¿Te equivocas? Sí, estoy seguro de que te equivocas. Sí, te equivocas. Lo siento, amigo. Inténtalo una vez más. Apuesto que esta vez tendrás más suerte.",
            "Mendigo: Excelente, veo que eres sabio. Savia. Sábila. Savia de sábila. Tengo que sacar savia del árbol en el que duermo. Seguramente has escuchado de las propiedades increíbles de la sábila. Bueno, no tengo que decirlo, eres un sabio. Savia. Sábila. Las propiedades de la sábila. A pesar de ser un mendigo, cuido mucho de mi salud. Soy, de hecho, experto en…\r\n\r\nAcacio:  Ya, ya… déjanos seguir con nuestro camino.\r\n"
            )
    };

    public class Question
    {
        public string type;
        public int index;
        public string text;
        public string[] options;
        public string answer;
        public string responseAnswerWrong = null;
        public string responseAnsweRight = null;
        public int answerRight;
        public bool completed = false;

        public Question(string type, int index, string text, string[] options = null, int answerRight = 0, string responseAnswerWrong = null, string responseAnsweRight = null)
        {
            this.type = type;
            this.index = index;
            this.options = options;
            this.answerRight = answerRight;
            this.responseAnswerWrong = responseAnswerWrong;
            this.responseAnsweRight = responseAnsweRight;
        }
    }
}
