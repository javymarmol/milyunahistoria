﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData  {

    public bool[] questCompleted;
    public QuestData[] questDatas;

    public GameData()
    {
        questCompleted = new bool[30];
        questDatas = new QuestData[30];
    }

}

[System.Serializable]
public class QuestData
{
    public int optionSelected;
    public bool[] storiesCompleted = new bool[4];
}
