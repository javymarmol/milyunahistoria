﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInteractions : MonoBehaviour {

    public SceneChanger sceneChanger;
    GameObject person;

    public void UpdatePositionCharacter(GameObject level)
    {
        Debug.Log("pressed");
        person = GameObject.Find("Personaje1");
        person.GetComponent<CharacterBehavior>().ChangePosition(this.transform, level.GetComponent<LevelBehavior>().index);
    }
}
