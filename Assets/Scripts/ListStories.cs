﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class ListStories : MonoBehaviour {

    QuestManager manager;
    AudioRecordManager audioRecordManager;
    public Transform contentPanel;
    public GameObject panelPrefab;
    GameObject panel;
    int count = 1;


    // Use this for initialization
    void Start () {
        manager = FindObjectOfType<QuestManager>();
        audioRecordManager = FindObjectOfType<AudioRecordManager>();
        Debug.Log(manager.quests.Length);
        foreach (Quest q in manager.quests)
        {
            if (q.isStory)
            {
                int i = 0;
                foreach (string story in q.stories)
                {
                    string nameStory = "story_" + q.questID + "_" + i;
                    string filePath = Path.Combine(Application.persistentDataPath, nameStory + ".mp3");
                    //Debug.Log("filePath quesst: " + filePath);
                    if (File.Exists(filePath))
                    {
                        Debug.Log("Entro quest: " + q.questID + " story:" + i);
                        panel = Instantiate(panelPrefab, contentPanel);
                        string text = story;

                        if(text.Length > 30)
                        {
                            text = string.Concat(text.Substring(0, 30), " ...");
                        }

                        panel.transform.GetChild(0).GetComponent<Text>().text = text;
                        Button btnShare = panel.transform.GetChild(1).GetComponent<Button>();
                        btnShare.onClick.AddListener(() => audioRecordManager.ShareAudio(nameStory));
                        Button btnPLay = panel.transform.GetChild(2).GetComponent<Button>();
                        btnPLay.onClick.AddListener(() => audioRecordManager.OnPlayAudioButtonClick(nameStory));
                        //panel.transform.GetChild(0).GetComponent<Text>().text = story;
                        count++;

                        int childCount = contentPanel.transform.childCount;
                        Vector2 size = contentPanel.transform.GetComponent<RectTransform>().sizeDelta;
                        size.y = (100 + 10) * (childCount);

                        contentPanel.transform.GetComponent<RectTransform>().sizeDelta = size;
                    }
                    i++;
                }
            }

        }
    }


}
