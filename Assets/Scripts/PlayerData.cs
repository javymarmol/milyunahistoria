﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {

    public int isFirstTime;
    public int currentLevel;
    public int posLevelCharacter;
    public string characterName;
    public float posX;
    public float posY;

    public PlayerData()
    {
        this.isFirstTime = 0;
        this.currentLevel = 1;
        this.posLevelCharacter = 0;
        this.posX = -17.37f;
        this.posY = -9.5f;
    }


}
