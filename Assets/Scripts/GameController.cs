﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    DataController dataController;
    SceneChanger sceneChanger;
    QuestManager manager;
    CharacterBehavior character;

    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameController");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        //LoadPlayerData();
        //LoadGameData();
    }

    // Use this for initialization
    void Start () {
        sceneChanger = FindObjectOfType<SceneChanger>();
        dataController = FindObjectOfType<DataController>();
        //LoadGame();
    }

    public void LoadGame()
    {
        dataController.LoadPlayerData();
        dataController.LoadGameData();
        manager = FindObjectOfType<QuestManager>();
        manager.questCompleted = dataController.gameData.questCompleted;

        for (int i = 0; i < dataController.gameData.questDatas.Length; i++)
        {
            manager.quests[i].optionsSelected = dataController.gameData.questDatas[i].optionSelected;
            manager.quests[i].storiesCompleted = dataController.gameData.questDatas[i].storiesCompleted;
        }

        if (GameObject.FindWithTag("Player"))
        {
            character = FindObjectOfType<CharacterBehavior>();
            character.posLevelCharacter = dataController.playerData.posLevelCharacter;
            character.level = dataController.playerData.currentLevel;
            character.gameObject.GetComponent<SpriteRenderer>().sprite = (dataController.playerData.characterName == "Male") ? character.characteres[0] : character.characteres[1];
            character.transform.position = new Vector3(dataController.playerData.posX, dataController.playerData.posY, 0f);

        }
    }


    public void StartGame()
    {
        LoadGame();
        if (dataController.playerData.isFirstTime == 0)
        {
            sceneChanger.ChangeSceneTo("IntroScene");
        }
        else
        {
            sceneChanger.ChangeSceneTo("MainScene");
        }
    }

    public void SelectCharacter(string type)
    {
        dataController.playerData.characterName = type;
        dataController.playerData.isFirstTime = 1;
        dataController.playerData.posLevelCharacter = 0;
        dataController.SavePlayerData();
        if (GameObject.FindWithTag("Player"))
        {
            GameObject.FindWithTag("Player").GetComponent<CharacterBehavior>().posLevelCharacter = 0;
            GameObject.FindWithTag("Player").GetComponent<CharacterBehavior>().level = 1;
        }
        sceneChanger.ChangeSceneTo("MainScene");
    }

    public void SaveProgress()
    {
        dataController.gameData.questCompleted = manager.questCompleted;
        foreach(Quest q in manager.quests)
        {
            dataController.gameData.questDatas[q.questID].optionSelected = q.optionsSelected;
            dataController.gameData.questDatas[q.questID].storiesCompleted = q.storiesCompleted;
        }
        dataController.SaveGameData();
        SavePlayerProgress();
        //dataController.gameData = (QuestData) quests;

    }

    public void SavePlayerProgress()
    {
        character = FindObjectOfType<CharacterBehavior>();
        dataController.playerData.currentLevel = character.level;
        dataController.playerData.posLevelCharacter = character.posLevelCharacter;
        dataController.SavePlayerData();
    }

    public void restarData()
    {
        dataController.DeletePlayerData();
        dataController.DeleteGameData();
        LoadGame();
    }
}
