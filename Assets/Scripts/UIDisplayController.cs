﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDisplayController : MonoBehaviour
{
    public GameObject encodingMessagePanel;
    public Text elapsedTimeText;
    public Text timerText;
    public Button stop, start;
    //public Dropdown encoderDropdown;

    public AudioRecordManager recordManager;
    public SceneChanger sceneChanger;

    QuestManager manager;
    Quest quest;

    void OnEnable()
    {
        recordManager.OnStartEncoding += RecordManager_OnStartEncoding;
        recordManager.OnStopEncoding += RecordManager_OnStopEncoding;

        //encoderDropdown.onValueChanged.AddListener(delegate { EncoderDropdownValueChanged(encoderDropdown); });
    }

    private void OnDisable()
    {
        recordManager.OnStartEncoding -= RecordManager_OnStartEncoding;
        recordManager.OnStopEncoding -= RecordManager_OnStopEncoding;
    }

    void Update()
    {
        if (recordManager.isRecording)
        {

            if (!elapsedTimeText.gameObject.activeSelf)
            {
                elapsedTimeText.gameObject.SetActive(true);
            }

            if (!timerText.gameObject.activeSelf)
            {
                timerText.gameObject.SetActive(true);
            }
            elapsedTimeText.text = recordManager.ElapsedTimeTextWithFormat;
            timerText.text = recordManager.TimerTextWithFormat;
        }
    }

    private void RecordManager_OnStartEncoding()
    {
        encodingMessagePanel.SetActive(true);
    }

    private void RecordManager_OnStopEncoding()
    {
        manager = FindObjectOfType<QuestManager>();
        quest = manager.quests[manager.questID];
        encodingMessagePanel.SetActive(false);
        if (!quest.isSelectMultiple)
        {
            if (quest.completedText.Length > 0)
            {
                sceneChanger.ChangeSceneTo("RightAnswerScene");
            }
            else
            {
                sceneChanger.ChangeSceneTo("MainScene");
            }
            return;
        }
        sceneChanger.ChangeSceneTo("MainScene");
    }

    private void EncoderDropdownValueChanged(Dropdown dropdown)
    {
        if (dropdown.value == 0)
        {
            recordManager.selectedAudioType = AudioType.MPEG;
        }
        else if (dropdown.value == 1)
        {
            recordManager.selectedAudioType = AudioType.OGGVORBIS;
        }
    }

    public void OnStartButtonCLick()
    {
        start.gameObject.SetActive(false);
        stop.gameObject.SetActive(true);
    }
}
