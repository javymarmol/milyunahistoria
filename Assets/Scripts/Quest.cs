﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest : MonoBehaviour {

    public int questID;
    public QuestManager manager;

    public bool isActivity;
    public string[] textActivity =  new string[4];

    public bool isSelectMultiple;
    public int numberOptions;
    public string[] options;
    public int rigthOption;
    public int optionsSelected;

    public bool isStory;
    public int numberOfStories;
    public string[] stories;
    public bool[] storiesCompleted = new bool[4];

    public bool isFinal;

    private CharacterBehavior characterBehavior;

    private SceneChanger sceneChanger;
    private string sceneName;

    public string startText, completedText, wrongText;

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update () {
        if (isStory)
        {
            //storiesCompleted++;
            /*if(storiesCompleted >= numberOfStories)
            {
                QuestComplete();
            }*/
        }
        if (isActivity)
        {
           /* if (manager.isCompleted)
            {
                activityCompleted = true;
                QuestComplete();
            }*/
        }
    }

    public void StartQuest()
    {

        manager = FindObjectOfType<QuestManager>();
        characterBehavior = FindObjectOfType<CharacterBehavior>();
        sceneChanger = FindObjectOfType<SceneChanger>();
        //gameController = FindObjectOfType<GameController>();
        //sceneChanger.ChangeSceneTo("QuestionScene");
        //characterBehavior.NextLevel();
        //Debug.Log("estar quest");

        //bool v = (characterBehavior.GetPos() == questID);


        Debug.Log("estar quest: " + questID + ", select: " + isSelectMultiple + ", Story: " + isStory + ", Reto: " + isActivity);

        if (isSelectMultiple)
        {
            //options = new string[numberOptions];
            this.sceneName = "SelectMultipleScene";
            //Debug.Log("estar quest sceneName: " + this.sceneName);
            //return;
        }else if (isStory)
        {
            //stories = new string[numberOfStories];
            this.sceneName = "QuestionScene";
            //Debug.Log("estar quest sceneName: " + this.sceneName);
            //return;
        }
        else if (isActivity)
        {
            this.sceneName = "RetoScene";
            //Debug.Log("estar quest sceneName: " + this.sceneName);
            //return;
        }
        /*if (questID == 0)
        {
            sceneName = "PuzzleScene";
        }*/

        //Debug.Log("estar quest sceneName: " + this.sceneName);
        manager.questID = questID;
        sceneChanger.ChangeSceneTo(this.sceneName);
    }

    public void QuestComplete()
    {
        manager.questCompleted[questID] = true;
        if ((characterBehavior.GetPos() == characterBehavior.level - 1) && (characterBehavior.GetPos() == questID) && (characterBehavior.level < 30))
        {
            characterBehavior.NextLevel();
        }
        manager.SaveData();

    }

    public void CompleteStory(int posStory)
    {
        this.storiesCompleted[posStory] = true;
    }
}
