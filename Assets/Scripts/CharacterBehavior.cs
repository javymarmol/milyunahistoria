﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBehavior : MonoBehaviour {

    public bool gameStarted = false;
    public int posLevelCharacter = 0;
    public int level = 1;
    readonly float speed = 0.1f;
    bool isMoving = false;
    public static bool playerCreated;
    private Vector3 initPos = new Vector3(-17.37f, -9.5f, 0f);
    private Vector3 lastPos;
    public Sprite[] characteres;
    private string characterSelected;

    DataController dataController;
    GameController gameController;

    // Use this for initialization
    void Start () {
        dataController = FindObjectOfType<DataController>();
        if (!playerCreated)
        {
            playerCreated = true;
            DontDestroyOnLoad(this.transform.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        this.level = dataController.playerData.currentLevel;
        //this.characterSelected = dataController.playerData.characterName;
        this.gameStarted = (dataController.playerData.isFirstTime == 1) ? true : false;
        if(initPos.x != dataController.playerData.posX && initPos.y != dataController.playerData.posY)
            this.transform.position = new Vector3(dataController.playerData.posX + 1f, dataController.playerData.posY + 1f, 0);

        gameObject.GetComponent<SpriteRenderer>().sprite = (this.characterSelected == "Male") ? characteres[0] : characteres[1];
    }
	
	// Update is called once per frame
	void Update () {
        if(this.characterSelected != dataController.playerData.characterName)
        {
            gameStarted = true;
            //this.transform.position = new Vector3(dataController.playerData.posX + 1f, dataController.playerData.posY + 1f, 0); ;
            this.characterSelected = dataController.playerData.characterName;
            gameObject.GetComponent<SpriteRenderer>().sprite = (this.characterSelected == "Male") ? characteres[0] : characteres[1];
        }
    }


    private void Awake()
    {
        //DontDestroyOnLoad(transform.gameObject);

        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }


    public void NextLevel()
    {
        level++;
    }

    public int GetLevel()
    {
        return level;
    }

    public int GetPos()
    {
        return posLevelCharacter;
    }

    public void ChangePosition(Transform point, int posLevel)
    {
        //Debug.Log(point.position);
        //Debug.Log(posLevel);
        lastPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        posLevelCharacter = posLevel;

        gameController = FindObjectOfType<GameController>();
        dataController.playerData.posX = point.position.x;
        dataController.playerData.posY = point.position.y;
        gameController.SavePlayerProgress();
        StartCoroutine(MoveToPos(transform, new Vector3(point.position.x, point.position.y, transform.position.z), 1.0f));


        //transform.position = new Vector3(point.position.x, point.position.y, transform.position.y);
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(8, 8);
    }

    public void ChangePosition2(Transform point, int posLevel)
    {
        posLevelCharacter = posLevel - 1;
        float diffX = CalculateDiference(transform.position.x, point.position.x);
        float diffY = CalculateDiference(transform.position.y, point.position.y);
        if (diffX < diffY)
        {
            if (transform.position.x < point.position.x)
            {
                //StartCoroutine(moveToPos(transform, new Vector3(Mathf.Clamp(transform.position.x + speed, -2.7f, 4.8f), transform.position.y, transform.position.z), 1.0f));
                //transform.position = new Vector3(Mathf.Clamp(transform.position.x + speed, -2.7f, 4.8f), transform.position.y, transform.position.z);
            }
            else
            {

                transform.position = new Vector3(Mathf.Clamp(transform.position.x - speed, -2.7f, 4.8f), transform.position.y, transform.position.z);
            }

            if (transform.position.y < point.position.y)
            {
                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y + speed, -2.7f, 4.8f), transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y - speed, -2.7f, 4.8f), transform.position.z);
            }
        }
        else
        {
            if (transform.position.y < point.position.y)
            {
                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y + speed, -2.7f, 4.8f), transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y - speed, -2.7f, 4.8f), transform.position.z);
            }

            if (transform.position.x < point.position.x)
            {
                transform.position = new Vector3(Mathf.Clamp(transform.position.x + speed, -2.7f, 4.8f), transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(Mathf.Clamp(transform.position.x - speed, -2.7f, 4.8f), transform.position.y, transform.position.z);
            }

        }
    }

    IEnumerator MoveToPos(Transform fromPosition, Vector3 toPosition, float duration)
    {
        //Make sure there is only one instance of this function running
        if (isMoving)
        {
            yield break; ///exit if this is still running
        }
        isMoving = true;

        float counter = 0;

        //Get the current position of the object to be moved
        Vector3 startPos = fromPosition.position;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            fromPosition.position = Vector3.Lerp(startPos, toPosition, counter / duration);
            yield return null;
        }

        isMoving = false;
    }

    float CalculateDiference(float a, float b)
    {
        if (a < 0 && b < 0)
        {
            return Mathf.Abs(a - b);
        }
        else if (a >= 0 && b >= 0)
        {
            return a - b;
        }
        else
        {
            return a + b;
        }

    }

}
