﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBehavior : MonoBehaviour {

    public int index;
    GameObject person;
    public bool completed;
    bool active = false;
    public Sprite[] buttons;
    private SpriteRenderer spriteRenderer;
    private QuestManager questManager;
    Transform level;
    //public SceneChanger sceneChanger;
    //public string scene;

    // Use this for initialization
    void Start () {
        person = GameObject.Find("Personaje1");
        spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = buttons[0];
        questManager = FindObjectOfType<QuestManager>();
    }

    // Update is called once per frame
    void Update () {
        if (!active)
        {
            if(person.GetComponent<CharacterBehavior>().GetLevel()-1 >= index)
            {
                ChangeSprite();
                active = true;
            }
        }
        else
        {
            if (IsTouched())
            {
                if(this.tag == "Next")
                {
                   level = GameObject.Find("level20").transform;
                    person.transform.position = new Vector3(level.position.x + 1f, level.position.y + 1f, 0);
                    person.GetComponent<CharacterBehavior>().posLevelCharacter = 19;
                }
                else if(this.tag == "Previous")
                {
                    level = GameObject.Find("level19").transform;
                    person.transform.position = new Vector3(level.position.x + 1f, level.position.y + 1f, 0);
                    person.GetComponent<CharacterBehavior>().posLevelCharacter = 18;
                }
                else
                {
                    level = this.transform;
                    UpdatePositionCharacter(level);
                    if (questManager.questID == index)
                    {
                        questManager.quests[index].StartQuest();
                    }
                }

            }

        }
    }

    //cuando es presionado
    public bool IsTouched()
    {



        bool result = false;


        if (Input.GetMouseButtonUp(0))
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos = new Vector2(wp.x, wp.y);

            if (this.gameObject.GetComponent<Collider2D>().OverlapPoint(mousePos))
            {

                result = true;
            }
        }
        return result;
    }

    void ChangeSprite()
    {
        spriteRenderer.sprite = buttons[1];
    }

    public void UpdatePositionCharacter(Transform level)
    {
        person.GetComponent<CharacterBehavior>().ChangePosition(level, index);
    }


}
