﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Networking;
using UnityEngine.UI;

using NAudio.Example;

//using OggVorbisEncoder.Example;

[RequireComponent(typeof(AudioSource))]
//[RequireComponent(typeof(OggVorbisEncoderWrapper))]
[RequireComponent(typeof(MP3EncoderWrapper))]
public class AudioRecordManager : MonoBehaviour
{
    
    #region Fields, Properties
    // constants for the wave file header
    //private const int HEADER_SIZE = 44;
    //private const short BITS_PER_SAMPLE = 16;
    //private const int SAMPLE_RATE = 44100;

    public EAudioSamplingRate samplingRate = EAudioSamplingRate.ASR_16000;
    private int sampleRate;
    // the number of audio channels in the output file
    //private int channels = 2;

    AudioSource _audioSource;

    // microphone input
    public int audioLength = 10;
    public AudioClip _audioClip;
    public bool useMicrophone = true;
    public string _selectDevice;
    public string fileName = "audio";

    public readonly Dictionary<AudioType, EAudioFileExtension> audioExtensions = 
        new Dictionary<AudioType, EAudioFileExtension>()
    {
        { AudioType.WAV, EAudioFileExtension.wav },
        { AudioType.OGGVORBIS, EAudioFileExtension.ogg },
        { AudioType.MPEG, EAudioFileExtension.mp3 }
    };

    public AudioType selectedAudioType = AudioType.MPEG;

    private string _persistentDataPath;
    public AudioMixerGroup _mixerGroupMicrophone, _mixerGroupMaster;
    AudioConfiguration _audioConfiguration;

    //OggVorbisEncoderWrapper _oggEncoderWrapper;
    MP3EncoderWrapper _mp3EncoderWrapper;


    // stereo channels
    public EAudioChannel channel = new EAudioChannel();


    // values min and max for mobile solved bug audio
    int min = 0, max = 0;
    float elapsedTime;
    float timer;
    /// <summary>
    /// Maximum recording time in seconds
    /// </summary>
    public int maxRecordTime = 600;

    /// <summary>
    /// Is the device currently recording with the micrphone?
    /// </summary>
    public bool isRecording;
    float startRecordTime;
    int timeSpan;

    #endregion

    public event Action OnStartEncoding;
    //public event Action<int> OnInt;
    public event Action OnStopEncoding;

    //quest scripts

    private QuestManager manager;
    private Quest quest;

    //UI Object
    public Text HistoryText;


    void Awake()
    {

        //_oggEncoderWrapper = GetComponent<OggVorbisEncoderWrapper>();
        _mp3EncoderWrapper = GetComponent<MP3EncoderWrapper>();
        _audioSource = GetComponent<AudioSource>();
        _audioConfiguration = AudioSettings.GetConfiguration();
        sampleRate = (int)samplingRate;
        _audioConfiguration.sampleRate = sampleRate;
        AudioSettings.Reset(_audioConfiguration);
        Debug.LogFormat("AudioRecordManager: AudioSettings.outputSampleRate = {0}Hz", AudioSettings.outputSampleRate);

        manager = FindObjectOfType<QuestManager>();
        if(manager.questID != -1)
        {
            quest = manager.quests[manager.questID];
       }
        //AudioSettings.outputSampleRate = (int)samplingRate;
        //sampleRate = AudioSettings.outputSampleRate;
    }

    void Start()
    {
        //_audioBand = new float[64];
        //_audioBandBuffer = new float[64];
        //_audioBand64 = new float[64];
        //_audioBandBuffer64 = new float[64];
        //AudioProfile(_audioProfile);

        _persistentDataPath = Application.persistentDataPath;
        if(HistoryText)
            HistoryText.text = quest.stories[manager.storySelected];

        // microphone input

        if (useMicrophone)
        {
            if (Microphone.devices.Length > 0)
            {
                // get values for solved bug mic android 
                Microphone.GetDeviceCaps(Microphone.devices[0], out min, out max);

                _selectDevice = Microphone.devices[0].ToString();
            }
            else
            {
                useMicrophone = false;
            }
        }
        if (!useMicrophone)
        {
            _audioSource.outputAudioMixerGroup = _mixerGroupMaster;
            _audioSource.clip = _audioClip;
        }

        //_audioSource.Play();
    }

    public string ElapsedTimeTextWithFormat { get; private set; }
    public string TimerTextWithFormat { get; private set; }

    public void Update()
    {
        if (isRecording)
        {
            elapsedTime += Time.deltaTime;
            timer -= Time.deltaTime;
            if (Mathf.RoundToInt(elapsedTime) > maxRecordTime)
            {
                isRecording = false;
                OnStopMicButtonClick();
            }
            ElapsedTimeTextWithFormat = FormatTime(Mathf.RoundToInt(elapsedTime));
            TimerTextWithFormat = FormatTime(Mathf.RoundToInt(timer));
        }
    }

    private string FormatTime(int time)
    {
        int mins = time / 60;
        int secs = time % 60;
        string timeFormat = string.Format("{0}:{1}", mins, secs);
        return timeFormat;
    }

    // method for record audio
    public void OnStartMicButtonClick()
    {
        _audioSource.outputAudioMixerGroup = _mixerGroupMicrophone;
        StartCoroutine(_AudioRecordWithAuthorization());
    }

    IEnumerator _AudioRecordWithAuthorization()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);

        if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
            yield return null;

        //if (max > 0)
        //{
        //    //solved bug for andorid
        //    //_audioSource.clip = Microphone.Start(_selectDevice, false, audioLength, max);
        //    sampleRate = max;
        //}
        //else
        //{
        //    sampleRate = AudioSettings.outputSampleRate;
        //}

        _audioSource.clip = Microphone.Start(_selectDevice, false, maxRecordTime, sampleRate);

        Debug.LogFormat("AudioRecordManager: Start recording >>>\nchannels={0}, samples={1}, f={2}", _audioSource.clip.channels, _audioSource.clip.samples, _audioSource.clip.frequency);
        startRecordTime = Time.time;
        isRecording = true;
        timer = maxRecordTime;
        elapsedTime = 0;
    }

    public void OnStopMicButtonClick()
    {
        isRecording = false;

        Debug.Log("AudioRecordManager: Stop Mic Session.");
        timeSpan = Mathf.RoundToInt(Time.time - startRecordTime);
        int samples = timeSpan * sampleRate;
        Microphone.End(_selectDevice);
        SavWav.Save(Path.Combine(_persistentDataPath, GetFileName(EAudioFileExtension.wav)), _audioSource.clip, samples);
        Debug.LogFormat("AudioRecordManager: WAV Audio File saved at {0}", Path.Combine(_persistentDataPath, GetFileName(EAudioFileExtension.wav)));

        OnEncodeAudioButtonClick();
    }

    public void OnEncodeAudioButtonClick()
    {
        switch (selectedAudioType)
        {
            case AudioType.UNKNOWN:
                break;
            case AudioType.ACC:
                break;
            case AudioType.AIFF:
                break;
            case AudioType.IT:
                break;
            case AudioType.MOD:
                break;
            case AudioType.MPEG:
                StartCoroutine(_Mp3FileEnconding());
                break;
            case AudioType.OGGVORBIS:
                //StartCoroutine(_OggFileEnconding());
                break;
            case AudioType.S3M:
                break;
            case AudioType.WAV:
                break;
            case AudioType.XM:
                break;
            case AudioType.XMA:
                break;
            case AudioType.VAG:
                break;
            case AudioType.AUDIOQUEUE:
                break;
        }
    }

    IEnumerator _Mp3FileEnconding()
    {
        //if (OnStartEncoding != null) OnStopEncoding();
        OnStartEncoding?.Invoke();

        yield return new WaitForSeconds(0.5f);
        string input = Path.Combine(_persistentDataPath, GetFileName(EAudioFileExtension.wav));
        //string output = Path.Combine(_persistentDataPath, GetFileName(EAudioFileExtension.mp3));
        string output = Path.Combine(_persistentDataPath, string.Concat("story_", quest.questID, "_", manager.storySelected, ".mp3"));
        //manager.quests[manager.questID].QuestComplete();
        quest.CompleteStory(manager.storySelected);
        quest.QuestComplete();
        _mp3EncoderWrapper.WaveToMP3(input, output, NAudio.Lame.LAMEPreset.ABR_128, UnlockUI);
    }

    /*IEnumerator _OggFileEnconding()
    {
        OnStartEncoding?.Invoke();

        yield return new WaitForSeconds(0.5f);
        string input = Path.Combine(_persistentDataPath, GetFileName(EAudioFileExtension.wav));
        string output = Path.Combine(_persistentDataPath, GetFileName(EAudioFileExtension.ogg));
        //_oggEncoderWrapper.EncodeOgg(input, output, UnlockUI);
    }*/

    private void UnlockUI()
    {
        OnStopEncoding?.Invoke();
    }

    public void OnPlayAudioButtonClick(string nameAudio)
    {
        Debug.Log("AudioRecordManager: init coroutine playing... ");
        _audioSource.outputAudioMixerGroup = _mixerGroupMaster;

        StartCoroutine(LoadAudio(selectedAudioType, nameAudio, OnAudioFileLoaded));
    }

    private IEnumerator LoadAudio(AudioType audioType, string file, System.Action<bool, AudioClip> OnComplete)
    {
        switch (audioType)
        {
            case AudioType.ACC:
                break;
            case AudioType.MPEG:
                file += ".mp3";
                break;
            case AudioType.OGGVORBIS:
                file += GetFileName(EAudioFileExtension.ogg);
                break;
            case AudioType.WAV:
                file += GetFileName(EAudioFileExtension.wav);
                break;
        }
        string path = string.Concat("file:///", Path.Combine(_persistentDataPath, file));
        Debug.LogFormat("AudioRecordManager: trying to load file at {0} ", path);
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(path, audioType))
        {
            yield return www.SendWebRequest();

            Debug.Log("AudioRecordManager: done loading.");

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                OnComplete?.Invoke(false, null);
            }
            else
            {
                _audioClip = DownloadHandlerAudioClip.GetContent(www);
                OnComplete?.Invoke(true, _audioClip);
            }
        }
    }

    public void ShareAudio(string nameAudio)
    {

        string screenShotPath = Path.Combine(Application.persistentDataPath, nameAudio + ".mp3");

        Debug.Log("sharing...: " + screenShotPath);
        //if (File.Exists(screenShotPath)) File.Delete(screenShotPath);

        //Application.CaptureScreenshot(ScreenshotName);

        StartCoroutine(delayedShare(screenShotPath));
    }    

    //CaptureScreenshot runs asynchronously, so you'll need to either capture the screenshot early and wait a fixed time
    //for it to save, or set a unique image name and check if the file has been created yet before sharing.
    IEnumerator delayedShare(string screenShotPath, string text = "")
    {
        while (!File.Exists(screenShotPath))
        {
            yield return null;
        }

        NativeShare.Share(text, screenShotPath, "", "", "audio/mpeg", true, "");
    }


    private void OnAudioFileLoaded(bool success, AudioClip clip)
    {
        //_audioSource.Stop();
        if (success && clip != null)
        {
            Debug.LogFormat("AudioRecordManager: audio loaded success = {0}", success);
            _audioSource.clip = clip;
            _audioSource.clip.name = "LoadedAudio";
            _audioSource.Play();
        }
    }

    public string GetFileName(EAudioFileExtension extension)
    {
        return string.Concat(fileName, ".", extension.ToString());
    }
}

public enum EAudioFileExtension
{
    wav,
    ogg,
    mp3,
}

public enum EAudioSamplingRate
{
    ASR_8000 = 8000,
    ASR_16000 = 16000,
    ASR_22050 = 22050,
    ASR_44100 = 44100,
}

public enum EAudioChannel
{
    Left = 0,
    Right = 1,
    Stereo = 2
}