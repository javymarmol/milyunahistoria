﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class UIPuzzleInsert : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public UIPuzzleController puzzleController;
    public EPuzzleToken Token;// { get; private set; }

    public delegate void PuzzleInsertEventHandler(bool match, Vector2 position);
    public static event PuzzleInsertEventHandler OnPointerEvent;

    public bool debugLog = false;

    bool match;

    private void Start()
    {
        match = false;
    }

    public void Init(EPuzzleToken token)
    {
        Token = token;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (debugLog) { Debug.LogFormat("Enter! {0}", transform.name); }
        if (puzzleController.CurrentToken == null) return;
        if (puzzleController.CurrentToken.token == Token)
        {
            if (debugLog) { Debug.LogFormat("Enter match! {0}", transform.name); }
            match = true;
            OnPointerEvent?.Invoke(match, transform.position);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        match = false;
        OnPointerEvent?.Invoke(match, transform.position);
    }

    //private void Update()
    //{
    //    if (Input.GetMouseButtonUp(0))
    //    {
    //        if (match)
    //        {
    //            Debug.LogFormat("Match! {0}", transform.name);
    //        }
    //    }
    //}
}