﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class UIPuzzleToken : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public EPuzzleToken token;
    [Range(0, 20)]
    public float moveSpeed = 8;
    public bool IsGrabbable { get; private set; } = false;
    bool matched = false;
    public Vector3 StartPosition { get; private set; }

    public delegate void PuzzleTokenEventHandler(UIPuzzleToken token, bool dragged);
    public static event PuzzleTokenEventHandler OnDragEvent;

    private RectTransform rectTransform;
    Vector2 mousePosition;

    bool restart;
    float deltaTime;
    Vector3 position;
    bool dragged;
    Image image;

    public void InitToken()
    {
        rectTransform = GetComponent<RectTransform>();
        StartPosition = transform.position;
        IsGrabbable = true;
        image = GetComponent<Image>();
    }

    private void Update()
    {
        if (matched) return;

        deltaTime = Time.deltaTime;
        if (restart)
        {
            float step = moveSpeed * deltaTime;
            position = Vector3.Lerp(transform.position, StartPosition, step);

            transform.position = position;

            if ((transform.position - StartPosition).magnitude < 0.1f)
            {
                transform.position = StartPosition;
                restart = false;
                IsGrabbable = true;
            }
        }

        if(dragged)
        {
            rectTransform.position = mousePosition;
        }
    }

    public void RestartToken()
    {
        IsGrabbable = false;
        restart = true;
    }

    public void Match(Vector2 position)
    {
        matched = true;
        image.color = Color.black;
        rectTransform.position = position;
    }

    public void OnDrag(PointerEventData eventData)
    {      
        mousePosition = eventData.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        OnDragEvent?.Invoke(this, true);
        dragged = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        OnDragEvent?.Invoke(this, false);
        dragged = false;
    }
}
