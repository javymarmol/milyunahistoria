﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIPuzzleController : MonoBehaviour
{
    public static int TOTAL_TOKENS = 6;
    public List<Image> images;
    private List<Vector3> imagePositions;
    private List<Vector3> startImgPositions;
    public List<EPuzzleToken> tokenOrder;

    public List<Image> tokenImages;
    private List<UIPuzzleToken> tokens;
    private List<Vector3> tokenPositions;

    public List<Image> insertImages;

    [Range(3, 15)]
    public float showTime = 10;
    float startTime;
    float countdownTime;

    public GameObject introPanel;
    public GameObject puzzlePanel;
    public GameObject congratsMessage;

    public Text countdownText;
    bool bInit;
    bool bTokenMatched;
    Vector2 matchedPosition;
    public UIPuzzleToken CurrentToken;
    int matchCount;

    public delegate void PuzzleControllerEventHandler();
    public static event PuzzleControllerEventHandler OnEndPuzzle;
    QuestManager manager;

    private void OnEnable()
    {
        UIPuzzleToken.OnDragEvent += UIPuzzleToken_OnDragEvent;
        UIPuzzleInsert.OnPointerEvent += UIPuzzleInsert_OnPointerEvent;
    }

    private void OnDisable()
    {
        UIPuzzleToken.OnDragEvent -= UIPuzzleToken_OnDragEvent;
        UIPuzzleInsert.OnPointerEvent -= UIPuzzleInsert_OnPointerEvent;
    }

    void Start()
    {
        matchCount = 0;

        manager = FindObjectOfType<QuestManager>();
        ShowIntroPanel();

        StartCoroutine(_StartLate());
    }

    IEnumerator _StartLate()
    {
        yield return null;
        int i, j;

        for (i = 0; i < TOTAL_TOKENS; i++)
        {
            images[i].rectTransform.position = imagePositions[i];
            images[i].enabled = true;
        }

        for (i = 0; i < TOTAL_TOKENS; i++)
        {
            //Debug.LogFormat("INDEX={0}", i);
            //Debug.LogFormat("POS={0}", startImgPositions[i]);
            for (j = 0; j < TOTAL_TOKENS; j++)
            {
                //Debug.Log(images[j].rectTransform.position);
                if (startImgPositions[i] == images[j].rectTransform.position)
                {
                    tokenOrder.Add(images[j].GetComponent<UIPuzzleToken>().token);
                    break;
                }
            }
        }

        startTime = Time.time;
        countdownTime = showTime;
        countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
        bInit = true;
        Invoke("ShowPuzzlePanel", countdownTime);
    }

    void Update()
    {
        if (!bInit) return;

        if (countdownTime > 0)
        {
            countdownTime -= Time.deltaTime;
            countdownText.text = SystemHelpers.FormatTimeSeconds(Mathf.CeilToInt(countdownTime));
        }
    }

    void ShowIntroPanel()
    {
        startImgPositions = new List<Vector3>();
        tokenOrder = new List<EPuzzleToken>();
        List<Vector3> copyList = new List<Vector3>();
        for (int i = 0; i < TOTAL_TOKENS; i++)
        {
            startImgPositions.Add(images[i].rectTransform.position);
            copyList.Add(startImgPositions[i]);
        }

        imagePositions = SystemHelpers.ShuffleList(copyList);
    }

    void ShowPuzzlePanel()
    {
        int i;
        introPanel.SetActive(false);
        puzzlePanel.SetActive(true);

        List<Vector3> startTokenPositions = new List<Vector3>();
        List<Vector3> copyList = new List<Vector3>();
        for (i = 0; i < TOTAL_TOKENS; i++)
        {
            startTokenPositions.Add(tokenImages[i].rectTransform.position);
            copyList.Add(startTokenPositions[i]);
        }
        tokenPositions = SystemHelpers.ShuffleList(copyList);

        tokens = new List<UIPuzzleToken>();
        for (i = 0; i < TOTAL_TOKENS; i++)
        {
            tokenImages[i].rectTransform.position = tokenPositions[i];
            tokens.Add(tokenImages[i].GetComponent<UIPuzzleToken>());
            tokens[i].InitToken();
            //insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokens[i].token);
            insertImages[i].GetComponent<UIPuzzleInsert>().Init(tokenOrder[i]);
        }
    }

    private void UIPuzzleToken_OnDragEvent(UIPuzzleToken token, bool dragged)
    {
        if (dragged)
        {
            CurrentToken = token;
        }
        else
        {
            if (bTokenMatched)
            {
                CurrentToken.Match(matchedPosition);
                matchCount++;

                if (matchCount == TOTAL_TOKENS)
                {
                    if (congratsMessage != null)
                    {
                        congratsMessage.SetActive(true);
                    }
                    // TODO:
                    Debug.Log("PuzzleController: Solved!");
                    StartCoroutine(DelayCompleteQuest());
                    OnEndPuzzle?.Invoke();
                    // Capture this event to do what you want :)
                }
            }
            else
            {
                CurrentToken.RestartToken();
            }
            CurrentToken = null;
        }
    }

    private void UIPuzzleInsert_OnPointerEvent(bool match, Vector2 position)
    {
        bTokenMatched = match;
        matchedPosition = position;
    }

    IEnumerator DelayCompleteQuest()
    {
        yield return new WaitForSeconds(1f);
        manager.quests[manager.questID].QuestComplete();
        SceneManager.LoadScene("EndGameScene");
    }
}

public enum EPuzzleToken
{
    None,
    Square,
    Circle,
    Diamond,
    Ellipse,
    Hexagon,
}