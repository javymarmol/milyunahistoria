﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour
{

    public Button buttonOk;
    public Button buttonWrong;

    private GameController gameController;
    private DataController dataController;

    private void Start()
    {

        gameController = FindObjectOfType<GameController>();
        dataController = FindObjectOfType<DataController>();
        if(dataController.playerData.isFirstTime == 1)
        {
            if (buttonOk)
            {
                Debug.Log("boton Ok");
                buttonOk.gameObject.SetActive(false);
            }
        }
    }

    public void ChangeSceneTo(string sceneName)
    {
        /*switch (sceneName)
        {
            case "MainScene":
                if (GameObject.FindWithTag("Player"))
                {
                    GameObject.FindWithTag("Player").GetComponent<SpriteRenderer>().enabled = true;
                }
                break;
            default:
                if (GameObject.FindWithTag("Player"))
                {
                    GameObject.FindWithTag("Player").GetComponent<SpriteRenderer>().enabled = false;
                }
                break;

        }*/

        SceneManager.LoadScene(sceneName);
    }

    public void SelectCharacter(string type)
    {
        gameController.SelectCharacter(type);
    }

    public void StartGame()
    {
        gameController.StartGame();
    }
}
