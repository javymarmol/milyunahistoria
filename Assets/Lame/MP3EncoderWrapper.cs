﻿using UnityEngine;
using System;
using System.Collections;
using NAudio.Lame;
using NAudio.Wave.WZT;

namespace NAudio.Example
{
    public class MP3EncoderWrapper : MonoBehaviour
    {
        public void WaveToMP3(string waveFileName, string mp3FileName, LAMEPreset bitRate = LAMEPreset.ABR_128, Action OnComplete = null)
        {
            StartCoroutine(_WaveToMP3(waveFileName, mp3FileName, bitRate, OnComplete));
        }

        public IEnumerator _WaveToMP3(string waveFileName, string mp3FileName, LAMEPreset bitRate = LAMEPreset.ABR_128, Action OnComplete= null)
        {
            using (var reader = new WaveFileReader(waveFileName))
            {
                using (var writer = new LameMP3FileWriter(mp3FileName, reader.WaveFormat, bitRate))
                {
                    yield return null;
                    reader.CopyTo(writer);
                }
            }
            yield return null;
            OnComplete?.Invoke();
        }
    }
}